﻿using System;
using CookBook.HISScheduler.Jobs.Contracts;
using System.Configuration;
using Microsoft.Practices.Unity;
using System.ComponentModel.Composition;
using System.ComponentModel;
using CookBook.Aspects.Factory;
using CookBook.Aspects.Constants;
using CookBook.HISScheduler.Jobs.Utilities;
using Newtonsoft.Json;
using System.Net;
using System.Linq;
using System.IO;
using CookBook.Aspects.Utils;
using System.Collections.Generic;
using CookBook.Data.MasterData;
using System.Net.Http;
using System.Net.Http.Headers;
using CookBook.Data.Request;

namespace CookBook.HISScheduler.Jobs.Implementation
{
    [Export(typeof(ICompassHISService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CompassHISServiceManager : ICompassHISService
    {
       
        public void ExecuteAllHospitalAPIs()
        {
            string hospitalAPIs = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("hospitalAPIs"), string.Empty);
            if (!string.IsNullOrEmpty(hospitalAPIs))
            {
                var hospitalList = hospitalAPIs.Split(',');
                if(hospitalList != null && hospitalList.Count() > 0)
                {
                    foreach(var hapi in hospitalList)
                    {
                        HospitalBedDataPushtoDB(hapi);
                        HospitalUserDataPushtoDB(hapi);
                        HospitalPatientDataPushtoDB(hapi);
                    }
                }
            }
        }
        
        #region Kondapur Hospital Public Methods

        /// <summary>
        /// PushNutritionData
        /// </summary>

        public void HospitalPatientDataPushtoDB(string hapi)
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory(""+hapi+" - Calling method to Send HospitalPatientDataPushtoDB", LogTraceCategoryNames.Tracing);
                HospitalDataPushtoDBAsync(hapi);
                LogTraceFactory.WriteLogWithCategory("" + hapi + " - Calling method to Send HospitalPatientDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        public void HospitalBedDataPushtoDB(string hapi)
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory("" + hapi + " -  method to Send HospitalBedDataPushtoDB", LogTraceCategoryNames.Tracing);
                HospitalBedMasterDataPushtoDB(hapi);
                LogTraceFactory.WriteLogWithCategory("" + hapi + " - Calling method to Send HospitalBedDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        public void HospitalUserDataPushtoDB(string hapi)
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory("" + hapi + " - Calling method to Send HospitalUserDataPushtoDB", LogTraceCategoryNames.Tracing);
                HospitalUserMasterDataPushtoDB(hapi);
                LogTraceFactory.WriteLogWithCategory("" + hapi + " - Calling method to Send HospitalUserDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        private void HospitalDataPushtoDBAsync(string hapi)
        {
            try
            {
                var httpClient = new HttpClient();
                // Set basic authentication credentials
                string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("PatientAPIUrl"), string.Empty);
                string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
                string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
                string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings(""+hapi+"OrgCode"), string.Empty);
                string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("" + hapi + "SiteCode"), string.Empty);
                string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("" + hapi + "DBName"), string.Empty);
                string isStartDatefromAppConfig = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("isStartDatefromAppConfig"), string.Empty);
                var startDate = DateTime.Now.AddMinutes(-15).ToShortDateString();
                if (isStartDatefromAppConfig.ToLower() == "true")
                {
                    startDate = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("StartDate"), string.Empty);
                }
                var endDate = DateTime.Now.ToShortDateString();
                // var startDate = DateTime.Now.AddDays(-60).ToShortDateString();
                var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

                var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Deserialize the JSON response
                    var jsonResponse = response.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<List<HISPatientProfileData>>(jsonResponse);
                    RequestData requestData = new RequestData();
                    requestData.SiteCode = siteCode;
                    requestData.SessionSectorName = dbName;
                    ServiceHelper.GetCookBookService().SaveHISPatientProfileMaster(requestData, data.ToArray());
                    LogTraceFactory.WriteLogWithCategory("" + hapi + " - HospitalDataPushtoDBAsync:Data saved successfully", LogTraceCategoryNames.Tracing);
                }
                else
                {
                    LogTraceFactory.WriteLogWithCategory("" + hapi + " - HospitalDataPushtoDBAsync:" + response.StatusCode + "", LogTraceCategoryNames.Tracing);
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("" + hapi + " - HospitalDataPushtoDBAsync: " + ex.ToString() + "", LogTraceCategoryNames.Tracing);
            }
        }

        private void HospitalBedMasterDataPushtoDB(string hapi)
        {
            try
            {
                var httpClient = new HttpClient();
                // Set basic authentication credentials
                string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("BEDAPIUrl"), string.Empty);
                string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
                string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
                string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("" + hapi + "OrgCode"), string.Empty);
                string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("" + hapi + "SiteCode"), string.Empty);
                string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("" + hapi + "DBName"), string.Empty);
                var startDate = DateTime.Now.AddMinutes(-15).ToShortDateString();
                string isStartDatefromAppConfig = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("isStartDatefromAppConfig"), string.Empty);
                if (isStartDatefromAppConfig.ToLower() == "true")
                {
                    startDate = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("StartDate"), string.Empty);
                }
                var endDate = DateTime.Now.ToShortDateString();
                var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

                var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Deserialize the JSON response
                    var jsonResponse = response.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<List<HISBedMasterData>>(jsonResponse);
                    RequestData requestData = new RequestData();
                    requestData.SiteCode = siteCode;
                    requestData.SessionSectorName = dbName;
                    LogTraceFactory.WriteLogWithCategory("" + hapi + " - HospitalBedMasterDataPushtoDB:" + requestData.SessionSectorName + " sitecode"
                        + requestData.SiteCode + "", LogTraceCategoryNames.Tracing);
                    ServiceHelper.GetCookBookService().SaveHISBedMasterData(requestData, data.ToArray());
                    LogTraceFactory.WriteLogWithCategory("" + hapi + " - HospitalBedMasterDataPushtoDB:Data saved successfully", LogTraceCategoryNames.Tracing);
                }
                else
                {
                    LogTraceFactory.WriteLogWithCategory("" + hapi + " - :" + response.StatusCode + "", LogTraceCategoryNames.Tracing);
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("" + hapi + " - HospitalBedMasterDataPushtoDB: " + ex.ToString() + "", LogTraceCategoryNames.Tracing);
            }
        }

        private void HospitalUserMasterDataPushtoDB(string hapi)
        {
            try
            {
                var httpClient = new HttpClient();
                // Set basic authentication credentials
                string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserAPIUrl"), string.Empty);
                string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
                string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
                string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("" + hapi + "OrgCode"), string.Empty);
                string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("" + hapi + "SiteCode"), string.Empty);
                string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("" + hapi + "DBName"), string.Empty);
                var startDate = DateTime.Now.AddMinutes(-15).ToShortDateString();
                string isStartDatefromAppConfig = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("isStartDatefromAppConfig"), string.Empty);
                if (isStartDatefromAppConfig.ToLower() == "true")
                {
                    startDate = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("StartDate"), string.Empty);
                }
                var endDate = DateTime.Now.ToShortDateString();
                var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

                var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Deserialize the JSON response
                    var jsonResponse = response.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<List<HISUserData>>(jsonResponse);
                    RequestData requestData = new RequestData();
                    requestData.SiteCode = siteCode;
                    requestData.SessionSectorName = dbName;
                    ServiceHelper.GetCookBookService().SaveHISUserData(requestData, data.ToArray());
                    LogTraceFactory.WriteLogWithCategory("" + hapi + " - HospitalUserMasterDataPushtoDB:Data saved successfully", LogTraceCategoryNames.Tracing);
                }
                else
                {
                    LogTraceFactory.WriteLogWithCategory("" + hapi + " - HospitalUserMasterDataPushtoDB:" + response.StatusCode + "", LogTraceCategoryNames.Tracing);
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("" + hapi + " - HospitalUserMasterDataPushtoDB: " + ex.ToString() + "", LogTraceCategoryNames.Tracing);
            }
        }

        #endregion


        #region HN Hospital Public Methods

        /// <summary>
        /// PushNutritionData
        /// </summary>

        public void HNHospitalPatientDataPushtoDB()
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory("HN - Calling method to Send HospitalPatientDataPushtoDB", LogTraceCategoryNames.Tracing);
                HNHospitalDataPushtoDBAsync();
                LogTraceFactory.WriteLogWithCategory("HN - Calling method to Send HospitalPatientDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        public void HNHospitalBedDataPushtoDB()
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory("HN -  method to Send HospitalBedDataPushtoDB", LogTraceCategoryNames.Tracing);
                HNHospitalBedMasterDataPushtoDB();
                LogTraceFactory.WriteLogWithCategory("HN - Calling method to Send HospitalBedDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        public void HNHospitalUserDataPushtoDB()
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory("HN - Calling method to Send HospitalUserDataPushtoDB", LogTraceCategoryNames.Tracing);
                HNHospitalUserMasterDataPushtoDB();
                LogTraceFactory.WriteLogWithCategory("HN - Calling method to Send HospitalUserDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        private void HNHospitalDataPushtoDBAsync()
        {
            try
            {
                var httpClient = new HttpClient();
                // Set basic authentication credentials
                string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("PatientAPIUrl"), string.Empty);
                string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
                string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
                string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("HNOrgCode"), string.Empty);
                string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("HNSiteCode"), string.Empty);
                string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("HNDBName"), string.Empty);
                string isStartDatefromAppConfig = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("isStartDatefromAppConfig"), string.Empty);
                var startDate = DateTime.Now.AddMinutes(-15).ToShortDateString();
                if (isStartDatefromAppConfig.ToLower() == "true")
                {
                    startDate = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("StartDate"), string.Empty);
                }
                var endDate = DateTime.Now.ToShortDateString();
                // var startDate = DateTime.Now.AddDays(-60).ToShortDateString();
                var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

                var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Deserialize the JSON response
                    var jsonResponse = response.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<List<HISPatientProfileData>>(jsonResponse);
                    RequestData requestData = new RequestData();
                    requestData.SiteCode = siteCode;
                    requestData.SessionSectorName = dbName;
                    ServiceHelper.GetCookBookService().SaveHISPatientProfileMaster(requestData, data.ToArray());
                    LogTraceFactory.WriteLogWithCategory("HN - HospitalDataPushtoDBAsync:Data saved successfully", LogTraceCategoryNames.Tracing);
                }
                else
                {
                    LogTraceFactory.WriteLogWithCategory("HN - HospitalDataPushtoDBAsync:" + response.StatusCode + "", LogTraceCategoryNames.Tracing);
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("HN - HospitalDataPushtoDBAsync: " + ex.ToString() + "", LogTraceCategoryNames.Tracing);
            }
        }

        private void HNHospitalBedMasterDataPushtoDB()
        {
            try
            {
                var httpClient = new HttpClient();
                // Set basic authentication credentials
                string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("BEDAPIUrl"), string.Empty);
                string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
                string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
                string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("HNOrgCode"), string.Empty);
                string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("HNSiteCode"), string.Empty);
                string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("HNDBName"), string.Empty);
                var endDate = DateTime.Now.ToShortDateString();
                var startDate = DateTime.Now.AddMinutes(-15).ToShortDateString();
                var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

                var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Deserialize the JSON response
                    var jsonResponse = response.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<List<HISBedMasterData>>(jsonResponse);
                    RequestData requestData = new RequestData();
                    requestData.SiteCode = siteCode;
                    requestData.SessionSectorName = dbName;
                    LogTraceFactory.WriteLogWithCategory("HN - HospitalBedMasterDataPushtoDB:" + requestData.SessionSectorName + " sitecode"
                        + requestData.SiteCode + "", LogTraceCategoryNames.Tracing);
                    ServiceHelper.GetCookBookService().SaveHISBedMasterData(requestData, data.ToArray());
                    LogTraceFactory.WriteLogWithCategory("HN - HospitalBedMasterDataPushtoDB:Data saved successfully", LogTraceCategoryNames.Tracing);
                }
                else
                {
                    LogTraceFactory.WriteLogWithCategory("HN - :" + response.StatusCode + "", LogTraceCategoryNames.Tracing);
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("HN - HospitalBedMasterDataPushtoDB: " + ex.ToString() + "", LogTraceCategoryNames.Tracing);
            }
        }

        private void HNHospitalUserMasterDataPushtoDB()
        {
            try
            {
                var httpClient = new HttpClient();
                // Set basic authentication credentials
                string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserAPIUrl"), string.Empty);
                string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
                string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
                string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("HNOrgCode"), string.Empty);
                string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("HNSiteCode"), string.Empty);
                string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("HNDBName"), string.Empty);
                var startDate = DateTime.Now.AddMinutes(-15).ToShortDateString();
                string isStartDatefromAppConfig = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("isStartDatefromAppConfig"), string.Empty);
                if (isStartDatefromAppConfig.ToLower() == "true")
                {
                    startDate = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("StartDate"), string.Empty);
                }
                var endDate = DateTime.Now.ToShortDateString();
                var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

                var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Deserialize the JSON response
                    var jsonResponse = response.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<List<HISUserData>>(jsonResponse);
                    RequestData requestData = new RequestData();
                    requestData.SiteCode = siteCode;
                    requestData.SessionSectorName = dbName;
                    ServiceHelper.GetCookBookService().SaveHISUserData(requestData, data.ToArray());
                    LogTraceFactory.WriteLogWithCategory("HN - HospitalUserMasterDataPushtoDB:Data saved successfully", LogTraceCategoryNames.Tracing);
                }
                else
                {
                    LogTraceFactory.WriteLogWithCategory("HN - HospitalUserMasterDataPushtoDB:" + response.StatusCode + "", LogTraceCategoryNames.Tracing);
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("HN - HospitalUserMasterDataPushtoDB: " + ex.ToString() + "", LogTraceCategoryNames.Tracing);
            }
        }

        #endregion

        #region R10 Hospital Public Methods

        /// <summary>
        /// PushNutritionData
        /// </summary>

        public void R10HospitalPatientDataPushtoDB()
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory("R10 - Calling method to Send HospitalPatientDataPushtoDB", LogTraceCategoryNames.Tracing);
                R10HospitalDataPushtoDBAsync();
                LogTraceFactory.WriteLogWithCategory("R10 - Calling method to Send HospitalPatientDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        public void R10HospitalBedDataPushtoDB()
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory("R10 -  method to Send HospitalBedDataPushtoDB", LogTraceCategoryNames.Tracing);
                R10HospitalBedMasterDataPushtoDB();
                LogTraceFactory.WriteLogWithCategory("R10 - Calling method to Send HospitalBedDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        public void R10HospitalUserDataPushtoDB()
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory("R10 - Calling method to Send HospitalUserDataPushtoDB", LogTraceCategoryNames.Tracing);
                R10HospitalUserMasterDataPushtoDB();
                LogTraceFactory.WriteLogWithCategory("R10 - Calling method to Send HospitalUserDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        private void R10HospitalDataPushtoDBAsync()
        {
            try
            {
                var httpClient = new HttpClient();
                // Set basic authentication credentials
                string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("PatientAPIUrl"), string.Empty);
                string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
                string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
                string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("R10OrgCode"), string.Empty);
                string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("R10SiteCode"), string.Empty);
                string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("R10DBName"), string.Empty);
                string isStartDatefromAppConfig = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("isStartDatefromAppConfig"), string.Empty);
                var startDate = DateTime.Now.AddMinutes(-15).ToShortDateString();
                if (isStartDatefromAppConfig.ToLower() == "true")
                {
                    startDate = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("StartDate"), string.Empty);
                }
                var endDate = DateTime.Now.ToShortDateString();
                // var startDate = DateTime.Now.AddDays(-60).ToShortDateString();
                var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

                var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Deserialize the JSON response
                    var jsonResponse = response.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<List<HISPatientProfileData>>(jsonResponse);
                    RequestData requestData = new RequestData();
                    requestData.SiteCode = siteCode;
                    requestData.SessionSectorName = dbName;
                    ServiceHelper.GetCookBookService().SaveHISPatientProfileMaster(requestData, data.ToArray());
                    LogTraceFactory.WriteLogWithCategory("R10 - HospitalDataPushtoDBAsync:Data saved successfully", LogTraceCategoryNames.Tracing);
                }
                else
                {
                    LogTraceFactory.WriteLogWithCategory("R10 - HospitalDataPushtoDBAsync:" + response.StatusCode + "", LogTraceCategoryNames.Tracing);
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("R10 - HospitalDataPushtoDBAsync: " + ex.ToString() + "", LogTraceCategoryNames.Tracing);
            }
        }

        private void R10HospitalBedMasterDataPushtoDB()
        {
            try
            {
                var httpClient = new HttpClient();
                // Set basic authentication credentials
                string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("BEDAPIUrl"), string.Empty);
                string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
                string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
                string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("R10OrgCode"), string.Empty);
                string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("R10SiteCode"), string.Empty);
                string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("R10DBName"), string.Empty);
                var endDate = DateTime.Now.ToShortDateString();
                var startDate = DateTime.Now.AddMinutes(-15).ToShortDateString();
                var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

                var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Deserialize the JSON response
                    var jsonResponse = response.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<List<HISBedMasterData>>(jsonResponse);
                    RequestData requestData = new RequestData();
                    requestData.SiteCode = siteCode;
                    requestData.SessionSectorName = dbName;
                    LogTraceFactory.WriteLogWithCategory("R10 - HospitalBedMasterDataPushtoDB:" + requestData.SessionSectorName + " sitecode"
                        + requestData.SiteCode + "", LogTraceCategoryNames.Tracing);
                    ServiceHelper.GetCookBookService().SaveHISBedMasterData(requestData, data.ToArray());
                    LogTraceFactory.WriteLogWithCategory("R10 - HospitalBedMasterDataPushtoDB:Data saved successfully", LogTraceCategoryNames.Tracing);
                }
                else
                {
                    LogTraceFactory.WriteLogWithCategory("R10 - :" + response.StatusCode + "", LogTraceCategoryNames.Tracing);
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("R10 - HospitalBedMasterDataPushtoDB: " + ex.ToString() + "", LogTraceCategoryNames.Tracing);
            }
        }

        private void R10HospitalUserMasterDataPushtoDB()
        {
            try
            {
                var httpClient = new HttpClient();
                // Set basic authentication credentials
                string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserAPIUrl"), string.Empty);
                string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
                string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
                string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("R10OrgCode"), string.Empty);
                string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("R10SiteCode"), string.Empty);
                string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("R10DBName"), string.Empty);
                var startDate = DateTime.Now.AddMinutes(-15).ToShortDateString();
                string isStartDatefromAppConfig = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("isStartDatefromAppConfig"), string.Empty);
                if (isStartDatefromAppConfig.ToLower() == "true")
                {
                    startDate = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("StartDate"), string.Empty);
                }
                var endDate = DateTime.Now.ToShortDateString();
                var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

                var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Deserialize the JSON response
                    var jsonResponse = response.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<List<HISUserData>>(jsonResponse);
                    RequestData requestData = new RequestData();
                    requestData.SiteCode = siteCode;
                    requestData.SessionSectorName = dbName;
                    ServiceHelper.GetCookBookService().SaveHISUserData(requestData, data.ToArray());
                    LogTraceFactory.WriteLogWithCategory("R10 - HospitalUserMasterDataPushtoDB:Data saved successfully", LogTraceCategoryNames.Tracing);
                }
                else
                {
                    LogTraceFactory.WriteLogWithCategory("R10 - HospitalUserMasterDataPushtoDB:" + response.StatusCode + "", LogTraceCategoryNames.Tracing);
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("R10 - HospitalUserMasterDataPushtoDB: " + ex.ToString() + "", LogTraceCategoryNames.Tracing);
            }
        }

        #endregion



    }
}
