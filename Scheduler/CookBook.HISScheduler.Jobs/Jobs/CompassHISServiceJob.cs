﻿using CookBook.Aspects.Constants;
using CookBook.Aspects.Factory;
using CookBook.HISScheduler.Jobs.Contracts;
using CookBook.HISScheduler.Jobs.Implementation;
using Quartz;

namespace CookBook.HISScheduler.Jobs.Jobs
{
    public class CompassHISServiceJob : IJob
    {
        #region Private Members


        private ICompassHISService cookBookServiceInstance = null;
        #endregion

        #region Properties

        /// <summary>
        /// Property to get claim process service business manager instance
        /// </summary>
        private ICompassHISService CookBookServiceInstance
        {
            get
            {
                if (cookBookServiceInstance == null)
                {
                    cookBookServiceInstance = new CompassHISServiceManager();
                }
                return cookBookServiceInstance;
            }
        }

        #endregion 

        #region Public Methods

        /// <summary>
        /// Method to override Quartz Job - Execute method 
        /// </summary>
        /// <param name="context">Job Execution Context</param>
        public void Execute(IJobExecutionContext context)
        {
            #region Kondapur
            LogTraceFactory.WriteLogWithCategory("Job HIS Execution begins", LogTraceCategoryNames.Tracing);
            CookBookServiceInstance.ExecuteAllHospitalAPIs();
            LogTraceFactory.WriteLogWithCategory("Job HIS Execution Ends", LogTraceCategoryNames.Tracing);

            #endregion


            //#region HN
            //LogTraceFactory.WriteLogWithCategory("HN -  Job HIS Execution begins", LogTraceCategoryNames.Tracing);
            //CookBookServiceInstance.HNHospitalBedDataPushtoDB();
            //CookBookServiceInstance.HNHospitalUserDataPushtoDB();
            //CookBookServiceInstance.HNHospitalPatientDataPushtoDB();
            //LogTraceFactory.WriteLogWithCategory("HN - Job HIS Execution Ends", LogTraceCategoryNames.Tracing);

            //#endregion


            //#region R10
            //LogTraceFactory.WriteLogWithCategory("R10 -  Job HIS Execution begins", LogTraceCategoryNames.Tracing);
            //CookBookServiceInstance.R10HospitalBedDataPushtoDB();
            //CookBookServiceInstance.R10HospitalUserDataPushtoDB();
            //CookBookServiceInstance.R10HospitalPatientDataPushtoDB();
            //LogTraceFactory.WriteLogWithCategory("R10 - Job HIS Execution Ends", LogTraceCategoryNames.Tracing);

            //#endregion


            //#region 
            //LogTraceFactory.WriteLogWithCategory("R10 -  Job HIS Execution begins", LogTraceCategoryNames.Tracing);
            //CookBookServiceInstance.R10HospitalBedDataPushtoDB();
            //CookBookServiceInstance.R10HospitalUserDataPushtoDB();
            //CookBookServiceInstance.R10HospitalPatientDataPushtoDB();
            //LogTraceFactory.WriteLogWithCategory("R10 - Job HIS Execution Ends", LogTraceCategoryNames.Tracing);

            //#endregion


        }

        #endregion
    }
}