﻿using CookBook.HISScheduler.Jobs.ServiceCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.HISScheduler.Jobs
{
    public class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new CompassHISService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
