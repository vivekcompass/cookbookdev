﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.HISScheduler.Jobs.Contracts
{
    /// <summary>
    /// Interface to keep all the methods, fields related to notifications
    /// </summary>
    public interface ICompassHISService
    {
        #region Kondapur
        /// <summary>
        /// Interface to send Notifications data to the respective Cob
        /// </summary>
        void ExecuteAllHospitalAPIs();
        void HospitalPatientDataPushtoDB(string hapi);
        void HospitalBedDataPushtoDB(string hapi);
        void HospitalUserDataPushtoDB(string hapi);
        #endregion

        #region HN
        /// <summary>
        /// Interface to send Notifications data to the respective Cob
        /// </summary>
        void HNHospitalPatientDataPushtoDB();
        void HNHospitalBedDataPushtoDB();
        void HNHospitalUserDataPushtoDB();
        #endregion

        #region R10
        /// <summary>
        /// Interface to send Notifications data to the respective Cob
        /// </summary>
        void R10HospitalPatientDataPushtoDB();
        void R10HospitalBedDataPushtoDB();
        void R10HospitalUserDataPushtoDB();
        #endregion
    }
}
