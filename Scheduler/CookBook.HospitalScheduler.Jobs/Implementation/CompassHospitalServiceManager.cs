﻿using System;
using CookBook.HospitalScheduler.Jobs.Contracts;
//using CookBook.APLScheduler.Utilities;
using System.Configuration;
using Microsoft.Practices.Unity;
using System.ComponentModel.Composition;
using System.ComponentModel;
using CookBook.Aspects.Factory;
using CookBook.Aspects.Constants;
using CookBook.HospitalScheduler.Jobs.Utilities;
using CookBook.Aspects.Utils;
using System.Collections.Generic;
using CookBook.Data.MasterData;
using System.Net.Http;
using System.Net.Http.Headers;
using CookBook.Data.Request;
using Newtonsoft.Json;

namespace CookBook.HospitalScheduler.Jobs.Implementation
{
    [Export(typeof(ICompassHospitalService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CompassHospitalServiceManager : ICompassHospitalService
    {
        #region Public Methods

        /// <summary>
        /// PushNutritionData
        /// </summary>

        public void HospitalPatientDataPushtoDB()
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory("Calling method to Send HospitalPatientDataPushtoDB", LogTraceCategoryNames.Tracing);
                HospitalPatientDataPushtoDB();
                LogTraceFactory.WriteLogWithCategory("Calling method to Send HospitalPatientDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        public void HospitalBedDataPushtoDB()
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory("Calling method to Send HospitalBedDataPushtoDB", LogTraceCategoryNames.Tracing);
                HospitalBedDataPushtoDB();
                LogTraceFactory.WriteLogWithCategory("Calling method to Send HospitalBedDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        public void HospitalUserDataPushtoDB()
        {
            try
            {
                LogTraceFactory.WriteLogWithCategory("Calling method to Send HospitalUserDataPushtoDB", LogTraceCategoryNames.Tracing);
                HospitalUserDataPushtoDB();
                LogTraceFactory.WriteLogWithCategory("Calling method to Send HospitalUserDataPushtoDB Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        #endregion


        #region Helpder Methods

        /// <summary>
        /// 
        /// </summary>
        private void HospitalDataPushtoDBAsync()
        {

            var httpClient = new HttpClient();
            // Set basic authentication credentials
            string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("PatientAPIUrl"), string.Empty);
            string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
            string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
            string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("OrgCode"), string.Empty);
            string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("SiteCode"), string.Empty);
            string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("DBName"), string.Empty);
            var endDate = DateTime.Now.ToShortDateString();
            var startDate = DateTime.Now.AddDays(-30).ToShortDateString();
            var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

            var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the JSON response
                var jsonResponse = response.Content.ReadAsStringAsync().Result;
                var data = JsonConvert.DeserializeObject<List<HISPatientProfileData>>(jsonResponse);
                RequestData requestData = new RequestData();
                requestData.SiteCode = siteCode;
                requestData.SessionSectorName = dbName;
                //ServiceHelper.GetCookBookService().SaveHISPatientProfileMaster(requestData, data.ToArray());
                Console.WriteLine("Data saved successfully.");
            }
            else
            {
                Console.WriteLine($"Failed to call API. Status code: {response.StatusCode}");
            }
        }

        private void HospitalBedMasterDataPushtoDB()
        {

            var httpClient = new HttpClient();
            // Set basic authentication credentials
            string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("BEDAPIUrl"), string.Empty);
            string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
            string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
            string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("OrgCode"), string.Empty);
            string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("SiteCode"), string.Empty);
            string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("DBName"), string.Empty);
            var endDate = DateTime.Now.ToShortDateString();
            var startDate = DateTime.Now.AddDays(-10).ToShortDateString();
            var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

            var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the JSON response
                var jsonResponse = response.Content.ReadAsStringAsync().Result;
                var data = JsonConvert.DeserializeObject<List<HISBedMasterData>>(jsonResponse);
                RequestData requestData = new RequestData();
                requestData.SiteCode = siteCode;
                requestData.SessionSectorName = dbName;
                //ServiceHelper.GetCookBookService().SaveHISBedMasterData(requestData, data.ToArray());
                Console.WriteLine("Data saved successfully.");
            }
            else
            {
                Console.WriteLine($"Failed to call API. Status code: {response.StatusCode}");
            }
        }

        private void HospitalUserMasterDataPushtoDB()
        {

            var httpClient = new HttpClient();
            // Set basic authentication credentials
            string apiUrl = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserAPIUrl"), string.Empty);
            string username = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("UserName"), string.Empty);
            string password = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("Password"), string.Empty);
            string orgCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("OrgCode"), string.Empty);
            string siteCode = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("SiteCode"), string.Empty);
            string dbName = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("DBName"), string.Empty);
            var startDate = DateTime.Now.AddDays(-100).ToShortDateString();
            var endDate = DateTime.Now.ToShortDateString();
            var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

            var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the JSON response
                var jsonResponse = response.Content.ReadAsStringAsync().Result;
                var data = JsonConvert.DeserializeObject<List<HISUserData>>(jsonResponse);
                RequestData requestData = new RequestData();
                requestData.SiteCode = siteCode;
                requestData.SessionSectorName = dbName;
                //ServiceHelper.GetCookBookService().SaveHISUserData(requestData, data.ToArray());
                Console.WriteLine("Data saved successfully.");
            }
            else
            {
                Console.WriteLine($"Failed to call API. Status code: {response.StatusCode}");
            }
        }



        #endregion
    }
}
