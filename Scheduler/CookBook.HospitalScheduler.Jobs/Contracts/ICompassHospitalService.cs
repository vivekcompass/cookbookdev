﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.HospitalScheduler.Jobs.Contracts
{
    /// <summary>
    /// Interface to keep all the methods, fields related to notifications
    /// </summary>
    public interface ICompassHospitalService
    {
        /// <summary>
        /// Interface to send Notifications data to the respective Cob
        /// </summary>
        void HospitalPatientDataPushtoDB();
        void HospitalBedDataPushtoDB();
        void HospitalUserDataPushtoDB();
    }
}
