﻿using CookBook.Aspects.Constants;
using CookBook.Aspects.Factory;
using CookBook.HospitalScheduler.Jobs.Contracts;
using CookBook.HospitalScheduler.Jobs.Implementation;
using Quartz;

namespace CookBook.HospitalScheduler.Jobs
{
    public class CompassHospitalServiceJob : IJob
    {
        #region Private Members


        private ICompassHospitalService cookBookServiceInstance = null;
        #endregion

        #region Properties

        /// <summary>
        /// Property to get claim process service business manager instance
        /// </summary>
        private ICompassHospitalService CookBookServiceInstance
        {
            get
            {
                if (cookBookServiceInstance == null)
                {
                    cookBookServiceInstance = new CompassHospitalServiceManager();
                }
                return cookBookServiceInstance;
            }
        }

        #endregion 

        #region Public Methods

        /// <summary>
        /// Method to override Quartz Job - Execute method 
        /// </summary>
        /// <param name="context">Job Execution Context</param>
        public void Execute(IJobExecutionContext context)
        {
            LogTraceFactory.WriteLogWithCategory("Job HIS Execution begins", LogTraceCategoryNames.Tracing);
            CookBookServiceInstance.HospitalPatientDataPushtoDB();
            LogTraceFactory.WriteLogWithCategory("Job HIS Execution Ends", LogTraceCategoryNames.Tracing);
        }

        #endregion
    }
}