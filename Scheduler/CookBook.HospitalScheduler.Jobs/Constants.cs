﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.HospitalScheduler.Jobs
{
    public class Constants
    {
        public const string COOKBOOKSERVICE_ENDPOINTKEY = "CookBookServiceEndPoint";
        public const string ApplicationUrl = "ApplicationUrl";
        public const string COOKBOOKSERVICE_ENDPOINT = "Web";
        public const string TEST_USER = "TestUser";
        public const string ProductionPlanFolder = "ProductionPlanFolder";
        public const string ProductionPlanExcelFile = "ProductionPlanExcelFile";
        public const string ProductionPlanSheetName = "ProductionPlanSheetName";
    }
}
