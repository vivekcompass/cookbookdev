﻿using System;
using CookBook.ChartwellsScheduler.Jobs.Contracts;
using System.Configuration;
using Microsoft.Practices.Unity;
using System.ComponentModel.Composition;
using System.ComponentModel;
using CookBook.Aspects.Factory;
using CookBook.Aspects.Constants;
using CookBook.ChartwellsScheduler.Jobs.Utilities;
using Newtonsoft.Json;
using System.Net;
using System.Linq;
using System.IO;
using CookBook.ChartwellsScheduler.Jobs.CookBookService;
using CookBook.Aspects.Utils;
using System.Collections.Generic;
using CookBook.Data.MasterData;

namespace CookBook.ChartwellsScheduler.Jobs.Implementation
{
    [Export(typeof(ICompassChartwellsService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CompassChartwellsServiceManager : ICompassChartwellsService
    {
        #region Public Methods

        /// <summary>
        /// PushNutritionData
        /// </summary>
        public void PushNutritionDataToEducation()
        {
            try
            {                
                LogTraceFactory.WriteLogWithCategory("Calling method to Send Pending NutritionData", LogTraceCategoryNames.Tracing);
                PushNutritionDataFoodBookEducation();
                LogTraceFactory.WriteLogWithCategory("Calling method to Send Pending NutritionData Completed", LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        #endregion

        #region Helpder Methods

        /// <summary>
        /// 
        /// </summary>
        private void PushNutritionDataFoodBookEducation()
        {
            try
            {
                var token = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("APIToken"), string.Empty);
                string URL = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("APIUrl"), string.Empty);
                var data = ServiceHelper.GetCookBookService().GetFoodBookNutritionDataList(AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("EducationSectorCode"), string.Empty));
                List<FoodBookNutritionDataRead> _output = new List<FoodBookNutritionDataRead>();
                if (data != null && data.Count() > 0)
                {
                    foreach (var food in data)
                    {
                        var fobj = new FoodBookNutritionDataRead();
                        fobj.Allergens = food.Allergens;
                        fobj.ArticleNumber = food.ArticleNumber;
                        fobj.HealthTags = food.HealthTags;
                        fobj.Ingredients = food.Ingredients.Replace(",", "");
                        // fobj.Nutrition = food.Nutrition;
                        fobj.Nutrition = food.Nutrition.Replace("ElementName", "name");
                        fobj.Nutrition = fobj.Nutrition.Replace("UOMName", "uom");
                        fobj.Nutrition = fobj.Nutrition.Replace("Quantity", "value");
                        fobj.PortionSize = food.PortionSize;
                        fobj.PortionUOM = food.PortionUOM;
                        fobj.SiteCode = food.SiteCode;
                        fobj.Details = new List<DishDetailsArray>();
                        var dArray = JsonConvert.DeserializeObject<List<DishDetails>>(food.Details);
                        if (dArray != null && dArray.Count() > 0)
                        {
                            foreach (var dobj in dArray)
                            {
                                DishDetailsArray dd = new DishDetailsArray();
                                dd.DishName = dobj.DishName;
                                dd.DishCode = dobj.DishCode;
                                dd.HealthTags = dobj.HealthTags;
                                //  dd.Ingredients = dobj.Ingredients;
                                dd.Allergens = JsonConvert.DeserializeObject<List<AllergenJson>>(dobj.Allergens);
                                dd.Nutrition = JsonConvert.DeserializeObject<List<NutritionJson>>(dobj.Nutrition);
                                fobj.Details.Add(dd);
                            }
                        }
                        _output.Add(fobj);
                    }
                }
                var total = _output.Count();
                var pageSize = AppUtil.ConvertToIntValue(XmlTextSerializer.GetAppSettings("RecordPageSize"), 0); 
                var loopCount = 1;
                var pageLeft = total - pageSize * loopCount;
                while (pageLeft > 0)
                {
                    var skip = pageSize * (loopCount - 1);
                    if (loopCount > 1)
                    {
                        pageLeft = total - pageSize * loopCount;
                    }
                    var pdata = _output.Skip(skip).Take(pageSize).ToArray();
                    var output = JsonConvert.SerializeObject(pdata);
                    string DATA = @"" + output + "";
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                    request.Method = "POST";
                    request.ContentType = "application/json";
                    request.Headers.Add("token", token);
                    request.ContentLength = DATA.Length;
                    StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                    requestWriter.Write(DATA);
                    requestWriter.Close();
                    try
                    {
                        WebResponse webResponse = request.GetResponse();
                        using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string response = responseReader.ReadToEnd();
                            Console.Out.WriteLine(response);
                            loopCount++;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
                    }
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }
        }

        #endregion
    }
}
