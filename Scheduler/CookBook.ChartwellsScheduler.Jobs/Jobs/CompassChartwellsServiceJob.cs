﻿using CookBook.Aspects.Constants;
using CookBook.Aspects.Factory;
using CookBook.ChartwellsScheduler.Jobs.Contracts;
using CookBook.ChartwellsScheduler.Jobs.Implementation;
using Quartz;

namespace CookBook.ChartwellsScheduler.Jobs.Jobs
{
    public class CompassChartwellsServiceJob : IJob
    {
        #region Private Members


        private ICompassChartwellsService cookBookServiceInstance = null;
        #endregion

        #region Properties

        /// <summary>
        /// Property to get claim process service business manager instance
        /// </summary>
        private ICompassChartwellsService CookBookServiceInstance
        {
            get
            {
                if (cookBookServiceInstance == null)
                {
                    cookBookServiceInstance = new CompassChartwellsServiceManager();
                }
                return cookBookServiceInstance;
            }
        }

        #endregion 

        #region Public Methods

        /// <summary>
        /// Method to override Quartz Job - Execute method 
        /// </summary>
        /// <param name="context">Job Execution Context</param>
        public void Execute(IJobExecutionContext context)
        {
            LogTraceFactory.WriteLogWithCategory("Job Chartwells Execution begins", LogTraceCategoryNames.Tracing);
            CookBookServiceInstance.PushNutritionDataToEducation();
            LogTraceFactory.WriteLogWithCategory("Job Chartwells Execution Ends", LogTraceCategoryNames.Tracing);
        }

        #endregion
    }
}