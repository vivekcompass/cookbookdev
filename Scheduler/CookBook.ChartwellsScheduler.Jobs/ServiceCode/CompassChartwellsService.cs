﻿using CookBook.ChartwellsScheduler.Jobs.Jobs;
using Quartz;
using Quartz.Impl;
using System.ServiceProcess;
using CookBook.ChartwellsScheduler.Jobs.Utilities;
using System.Diagnostics;
using CookBook.Aspects.Factory;
using CookBook.Aspects.Constants;
using CookBook.Aspects.Utils;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System;

namespace CookBook.ChartwellsScheduler.Jobs.ServiceCode
{
    partial class CompassChartwellsService : ServiceBase
    {
        #region Constructor
        /// <summary>
        /// COB Service contructor
        /// </summary>
        public CompassChartwellsService()
        {
            InitializeComponent();
        }
        #endregion

        #region Protected Methods/Event Handlers
        /// <summary>
        /// Method to call when Cob Service starts
        /// </summary>L
        /// <param name="args">Arguments</param>
        protected override void OnStart(string[] args)
        {
#if (DEBUG)
            Debugger.Launch();
#endif

            InitializeLoggingAndExceptionHandlingSettings();
            LogTraceFactory.WriteLogWithCategory("Initialized Logging And Exception Handling Settings",LogTraceCategoryNames.Tracing);
            int i;
            InitializeQuartzSettings();
            LogTraceFactory.WriteLogWithCategory("Initialized Quartz Settings", LogTraceCategoryNames.Tracing);
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Initialize Quartz Settings
        /// </summary>
        private void InitializeQuartzSettings()
        {

            bool runHourlyBasis = AppUtil.ConvertToBoolean(XmlTextSerializer.GetAppSettings("IsNutritionDataRun"), false);
            ExceptionFactory.AppExceptionManager.Process(() =>
            {
                //Construct job detail, scheduler factory and get scheduler 
                ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
                IScheduler scheduler = schedulerFactory.GetScheduler();
                // construct job info
                IJobDetail jobDetail = JobBuilder.Create<CompassChartwellsServiceJob>().WithIdentity("CompassSchedulerJobName").Build();

                int runningTimeInMinImm = 0;
                runningTimeInMinImm = AppUtil.ConvertToIntValue(XmlTextSerializer.GetAppSettings("NutritionDataRunningTimeMinuteImm"), 0);

                int runningTimeInMin = 0;
                int runningTimeInHours = 0;
                string runningTimeInDay = string.Empty;
                runningTimeInMin = AppUtil.ConvertToIntValue(XmlTextSerializer.GetAppSettings("NutritionDataRunningTimeMinute"), 0);
                runningTimeInHours = AppUtil.ConvertToIntValue(XmlTextSerializer.GetAppSettings("NutritionDataRunningTimeHour"), 0);
                runningTimeInDay = XmlTextSerializer.GetAppSettings("NutritionDataRunningTimeDay");


                ITrigger trigger;
                if (runHourlyBasis)
                {
                    trigger=TriggerBuilder.Create().ForJob(jobDetail).WithIdentity("NutritionDataTrigger").WithSchedule(CronScheduleBuilder.CronSchedule("0 0/" + runningTimeInMinImm + " * ? * *")).Build();
                    LogTraceFactory.WriteLogWithCategory("Assigned trigger on the basis of minutes", LogTraceCategoryNames.Tracing);
                }
                else
                {
                    string a = "0 " + runningTimeInMin + " " + runningTimeInHours + " ? * " + runningTimeInDay;
                    trigger = TriggerBuilder.Create().ForJob(jobDetail).WithIdentity("NutritionDataTrigger").WithSchedule(CronScheduleBuilder.CronSchedule("0 " + runningTimeInMin + " " + runningTimeInHours + " ? * " + runningTimeInDay)).Build();
                    LogTraceFactory.WriteLogWithCategory("Assigned trigger on the basis of Specific Time", LogTraceCategoryNames.Tracing);
                }
                //0 15 10 ? * MON-FRI
                //Start scheduler
                scheduler.Start();
                //Schedule Claim Process Service Job
                scheduler.ScheduleJob(jobDetail, trigger);
                LogTraceFactory.WriteLogWithCategory("Scheduler started and scheduled", LogTraceCategoryNames.Tracing);
            }, ExceptionPolicyNames.LoggingAndReplacingException.ToString());

        }

        /// <summary>
        /// Initialize Logging service and Exception Handling Settings
        /// </summary>
        private void InitializeLoggingAndExceptionHandlingSettings()
        {
            LogTraceFactory.InitializeLoggingService();
            ExceptionFactory.InitializeExceptionAopFramework();
        }

        public void PushNutritionDataFoodBook()
        {
           // var data = ServiceClient.GetFoodBookNutritionDataList(GetRequestData());
          //  var output = JsonConvert.SerializeObject(data);
            string URL = "https://nutrition-info-smartqdemo-pnbahkgobq-uc.a.run.app/nutrition_info";
           // string DATA = @"" + output + "";
            string DATA = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = DATA.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(DATA);
            requestWriter.Close();

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();
                }
            }
            catch (Exception e)
            {

            }
        }
        #endregion
    }
}
