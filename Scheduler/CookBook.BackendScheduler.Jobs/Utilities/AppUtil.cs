﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace CookBook.BackendScheduler.Jobs.Utilities
{
    public static class AppUtil
    {
        #region Static Methods

        /// <summary>
        /// Method to get default value of an instance
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <returns>returns default value</returns>
        public static T GetDefault<T>()
        {
            return default(T);
        }

        /// <summary>
        /// Method to parse string value into Enum type
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="value">string input value</param>
        /// <returns>returns enum</returns>
        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value);
        }

        /// <summary>
        /// Method to get enum from number value
        /// </summary>
        /// <typeparam name="T">Generic Enum type</typeparam>
        /// <param name="number">number</param>
        /// <returns>returns Enum</returns>
        public static T NumToEnum<T>(int number)
        {
            return (T)Enum.ToObject(typeof(T), number);
        }

        /// <summary>
        /// Property to get time stamp value
        /// </summary>
        /// <returns>returns time stamp value</returns>
        public static string GetTimestamp
        {
            get
            {
                return DateTime.Now.ToString("yyyyMMddHHmmssffff");
            }
        }

        /// <summary>
        /// Method to get integer value from object value
        /// Note:- Do not use it for Enum 
        /// </summary>
        /// <param name="valueToConvert">Value to converted into int.</param>
        /// <param name="defaultValue">Default value if conversion fail.</param>
        /// <returns>returns integer value</returns>
        public static int ConvertToIntValue(object valueToConvert, int defaultValue)
        {
            if (valueToConvert == null)
                return defaultValue;

            bool result = int.TryParse(valueToConvert.ToString(), out defaultValue);
            if (!result)
            {
                defaultValue = 0;
            }
            return defaultValue;
        }

        /// <summary>
        /// Method to get long value from object value
        /// </summary>
        /// <param name="valueToConvert">Value to converted into long.</param>
        /// <param name="defaultValue">Default value if conversion fail.</param>
        /// <returns>returns long value</returns>
        public static long ConvertToLongValue(object valueToConvert, long defaultValue)
        {
            if (valueToConvert == null)
                return defaultValue;

            bool result = long.TryParse(valueToConvert.ToString(), out defaultValue);
            if (!result)
            {
                defaultValue = 0;
            }
            return defaultValue;
        }

        /// <summary>
        /// Method to get decimal value from object value
        /// </summary>
        /// <param name="valueToConvert">Value to converted into long.</param>
        /// <param name="defaultValue">Default value if conversion fail.</param>
        /// <returns>returns decimal value</returns>
        public static decimal ConvertToDecimalValue(object valueToConvert, decimal defaultValue)
        {
            if (valueToConvert == null)
                return defaultValue;

            bool result = decimal.TryParse(valueToConvert.ToString(), out defaultValue);
            if (!result)
            {
                defaultValue = 0;
            }
            return defaultValue;
        }

        /// <summary>
        /// Method to get string value from object value
        /// </summary>
        /// <param name="valueToConvert">Value to converted into string.</param>
        /// <param name="defaultValue">Default value if conversion fail.</param>
        /// <returns>returns string value</returns>
        public static string ConvertToString(object valueToConvert, string defaultValue)
        {
            if (valueToConvert == null)
                return defaultValue;

            return valueToConvert.ToString();
        }

        /// <summary>
        /// Method to get Boolean value from object value
        /// </summary>
        /// <param name="valueToConvert">Value to converted into Boolean.</param>
        /// <param name="defaultValue">Default value if conversion fail.</param>
        /// <returns>returns Boolean value</returns>
        public static bool ConvertToBoolean(object valueToConvert, bool defaultValue)
        {
            if (valueToConvert == null)
                return defaultValue;

            bool result = bool.TryParse(valueToConvert.ToString(), out defaultValue);
            if (!result)
            {
                defaultValue = false;
            }
            return defaultValue;
        }

        /// <summary>
        ///  Method to get datetime value from object value
        /// </summary>
        /// <param name="valueToConvert">Value to converted into DateTime.</param>
        /// <param name="defaultValue">Default value if conversion fail.</param>
        /// <returns>returns DateTime value</returns>
        public static DateTime ConvertToDateTime(object valueToConvert, DateTime defaultValue)
        {
            if (valueToConvert == null)
                return defaultValue;

            bool result = DateTime.TryParse(valueToConvert.ToString(), out defaultValue);
            if (!result)
            {
                defaultValue = DateTime.Now;
            }
            return defaultValue;
        }

        /// <summary>
        /// Method to convert enum values integer.
        /// </summary>
        /// <param name="valueToConvert"> VAlue to converted into integer</param>
        /// <returns>returns Integer value.</returns>
        public static int ConvertEnumToIntValue(Enum valueToConvert)
        {
            return Convert.ToInt32(valueToConvert);
        }

        /// <summary>
        /// Method to convert enum values String.
        /// </summary>
        /// <param name="valueToConvert"> Value to converted into integer</param>
        /// <returns>returns Integer value.</returns>
        public static string ConvertEnumToString(Enum valueToConvert)
        {
            return Convert.ToString(Convert.ToInt32(valueToConvert));
        }

        /// <summary>
        /// Method to convert enum to string value
        /// </summary>
        /// <param name="valueToConvert">Enum to converted into string</param>
        /// <returns>returns enum string value</returns>
        public static string ConvertEnumToStringValue(Enum valueToConvert)
        {
            return Convert.ToString(valueToConvert);
        }

        /// <summary>
        /// Convert int to null value or actual value
        /// </summary>
        /// <param name="valuToConvert">Value to converted into integer</param>
        /// <returns>returns Integer value</returns>
        public static int? ConvertToInt(object valueToConvert)
        {
            if (valueToConvert == null)
                return null;

            int defaultValue = 0;
            if (!int.TryParse(valueToConvert.ToString(), out defaultValue))
            {
                // Default value is zero
            }

            return defaultValue;
        }

        /// <summary>
        /// Method to get guid value from object value
        /// </summary>
        /// <param name="valueToConvert">Value to be converted into guid.</param>
        /// <param name="defaultValue">Default value if conversion fail.</param> 
        /// <returns>returns guid value</returns>
        public static Guid ConvertToGuid(object valueToConvert, Guid defaultValue)
        {
            if (valueToConvert != null && Guid.TryParse(valueToConvert.ToString(), out defaultValue))
            {
                return defaultValue;
            }
            return defaultValue;
        }

        /// <summary>
        /// Convert long to null value or actual value
        /// </summary>
        /// <param name="valuToConvert">Value to converted into long</param>
        /// <returns>returns Long value</returns>
        public static long? ConvertToLong(object valueToConvert)
        {
            if (valueToConvert == null)
                return null;

            long defaultValue = 0;
            long.TryParse(valueToConvert.ToString(), out defaultValue);

            return defaultValue;
        }

        /// <summary>
        /// Method to compare two strings 
        /// </summary>
        /// <param name="input1">First String</param>
        /// <param name="input2">Second String</param>
        /// <param name="ignoreCase">Ignore Case</param>
        /// <returns>returns comparison status</returns>
        public static int CompareString(string input1, string input2, bool ignoreCase)
        {
            return string.Compare(input1, input2, ignoreCase, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Method to convert DataTable to List
        /// </summary>
        /// <typeparam name="T">Type of Object</typeparam>
        /// <param name="inputTable">Input table</param>
        /// <returns>returns generic list</returns>
        public static List<T> ConvertDataTableToList<T>(DataTable dataTable) where T : new()
        {
            List<T> genericList = new List<T>();
            Type classType = typeof(T);
            PropertyInfo[] propertyInfo = classType.GetProperties();
            List<DataColumn> dataColumnList = dataTable.Columns.Cast<DataColumn>().ToList();

            T classInstance;
            foreach (DataRow item in dataTable.Rows)
            {
                classInstance = (T)Activator.CreateInstance(classType);
                foreach (PropertyInfo propInfo in propertyInfo)
                {
                    try
                    {
                        DataColumn dataColumn = dataColumnList.Find(c => c.ColumnName == propInfo.Name);
                        if (dataColumn != null)
                        {
                            propInfo.SetValue(classInstance, item[propInfo.Name], null);
                        }
                    }
                    catch
                    {
                        // Exception
                    }
                }

                genericList.Add(classInstance);
            }

            return genericList;
        }

        /// <summary>
        /// Method to determine if specified date falls on week-end  
        /// </summary>
        /// <param name="specifiedDate">Specified Date</param>
        /// <returns>true,if weekend</returns>
        public static bool IsSpecifiedDateFallsOnWeekend(DateTime specifiedDate)
        {
            DayOfWeek dayOfWeek = specifiedDate.DayOfWeek;
            if (dayOfWeek == DayOfWeek.Sunday || dayOfWeek == DayOfWeek.Saturday)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Method to determine if specified date/day of week falls on Nth day of month
        /// </summary>
        /// <param name="specifiedDate">Specified Date</param>
        /// <param name="dayOfWeek">Day of Week</param>
        /// <param name="nthDay">Nth Day</param>
        /// <returns>Nth Day of Month</returns>
        public static bool GetNthDayOfMonth(DateTime specifiedDate, DayOfWeek dayOfWeek, int nthDay)
        {
            int day = specifiedDate.Day;
            return specifiedDate.DayOfWeek == dayOfWeek && (day - 1) / 7 == (nthDay - 1);
        }

        /// <summary>
        /// Method to Pad the Leading Zeros
        /// </summary>
        /// <param name="actualValue">Actual String</param>
        /// <param name="length">Length of String including Zeros</param>
        /// <returns>Resultant String</returns>
        public static string PadLeadingZero(string actualValue, int length)
        {
            //actualValue = "abc" Length = 10, resultantString = "0000000abc"
            string resultantString = actualValue.PadLeft(length, '0');
            return resultantString;
        }

        /// <summary>
        /// Method to Pad the Trailing Spaces
        /// </summary>
        /// <param name="actualValue">Actual String</param>
        /// <param name="length">Length of String including Space</param>
        /// <returns>Resultant String</returns>
        public static string PadTrailingSpace(string actualValue, int length)
        {
            string resultantString = actualValue.PadRight(length, ' ');
            return resultantString;
        }


        /// <summary>
        /// Method to get the sum of individual digit of number
        /// </summary>
        /// <param name="number">Number</param>
        /// <returns>Sum of Digit</returns>
        public static long GetDigitSum(long number)
        {
            long sum = 0;
            long remainder = 0;
            while (number != 0)
            {
                remainder = number % 10;
                number = number / 10;
                sum = sum + remainder;
            }
            return sum;
        }
        #endregion
    }
}
