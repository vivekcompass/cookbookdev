﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace CookBook.BackendScheduler.Jobs
{
    [RunInstaller(true)]
    public partial class CompassEmailServiceInstaller : System.Configuration.Install.Installer
    {
        #region Variables

        /// <summary>
        /// Define the object of ServiceProcessInstaller.
        /// </summary>
        private ServiceProcessInstaller process;
        /// <summary>
        /// Define the object of ServiceInstaller.
        /// </summary>
        private ServiceInstaller service;
        /// <summary>
        /// Variable to define the Service Name.
        /// </summary>
        private string serviceName;

        #endregion

        #region Constant Variables

        /// <summary>
        /// Constant property to define the Service Description.
        /// </summary>
        private const string SERVICE_DESCRIPTION = "Provides core functionality to complete background tasks.";

        #endregion

        #region .ctor

        /// <summary>
        /// Constructor
        /// </summary>
        public CompassEmailServiceInstaller()
        {
            try
            {
                process = new ServiceProcessInstaller();
                process.Account = ServiceAccount.LocalSystem;
                service = new ServiceInstaller();
                service.ServiceName = GetConfigurationValue("CompassSchedulerJobName");
                service.StartType = ServiceStartMode.Automatic;
                Installers.Add(process);
                Installers.Add(service);
            }
            catch (Exception)
            {
                //InvenigateLogger.InvenigateWindowServiceLogger.Error(ex.Message, ex);
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Method to get the Service Name from the Config File.
        /// </summary>
        /// <param name="key">string specifies the key value</param>
        /// <returns>Return the string specifies the App.Config Service Name</returns>
        private string GetConfigurationValue(string key)
        {
            Assembly service = Assembly.GetAssembly(typeof(CompassEmailServiceInstaller));
            if (service != null)
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(service.Location);
                if (config != null && config.AppSettings != null && config.AppSettings.Settings[key] != null)
                {
                    return config.AppSettings.Settings[key].Value;
                }
                else
                {
                    return serviceName;
                }
            }
            else
            {
                return serviceName;
            }
        }

        #endregion
    }
}
