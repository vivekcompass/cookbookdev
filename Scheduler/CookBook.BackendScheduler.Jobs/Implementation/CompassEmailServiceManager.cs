﻿using System;
using CookBook.BackendScheduler.Jobs.Contracts;
using System.Configuration;
using Microsoft.Practices.Unity;
using System.ComponentModel.Composition;
using System.ComponentModel;
using CookBook.Aspects.Factory;
using CookBook.Aspects.Constants;
using CookBook.BackendScheduler.Jobs.Utilities;

namespace CookBook.BackendScheduler.Jobs.Implementation
{
    [Export(typeof(ICompassEmailService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CompassEmailServiceManager : ICompassEmailService
    {

        #region Public Methods
        /// <summary>
        /// Send Exception Email
        /// </summary>
        public void SendExceptionEmail()
        {
            try
            {                
                LogTraceFactory.WriteLogWithCategory("Calling method to Send Pending Emails", LogTraceCategoryNames.Tracing);
                ServiceHelper.GetCookBookService().SendExceptionEmail();
                LogTraceFactory.WriteLogWithCategory("Calling method to Send Pending Emails Completed",LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }

        }

        #endregion
    }
}
