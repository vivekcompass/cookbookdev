﻿using CookBook.TestProject.Utilities;
using CookBook.Aspects.Factory;
using System;
using System.Configuration;
using System.Windows.Forms;
using CookBook.Aspects.Utils;
using Newtonsoft.Json;
using System.Net;
using System.Linq;
using System.IO;
using CookBook.Aspects.Constants;

namespace CookBook.TestProject
{
    public partial class Form1 : Form
    {        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                InitializeLoggingAndExceptionHandlingSettings();
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Initialize Logging service and Exception Handling Settings
        /// </summary>
        private void InitializeLoggingAndExceptionHandlingSettings()
        {
            LogTraceFactory.InitializeLoggingService();
            ExceptionFactory.InitializeExceptionAopFramework();
        }

        private void btnNutritionDataProcess_Click(object sender, EventArgs e)
        {
            try
            {
                var token = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("APIToken"), string.Empty);
                string URL = AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("APIUrl"), string.Empty);
                var data = ServiceHelper.GetCookBookService().GetFoodBookNutritionDataList(AppUtil.ConvertToString(XmlTextSerializer.GetAppSettings("EducationSectorCode"), string.Empty));
                var total = data.Count();
                var pageSize = AppUtil.ConvertToIntValue(XmlTextSerializer.GetAppSettings("RecordPageSize"), 0);
                var loopCount = 1;
                var pageLeft = total - pageSize * loopCount;
                while (pageLeft > 0)
                {
                    var skip = pageSize * (loopCount - 1);
                    if (loopCount > 1)
                    {
                        pageLeft = total - pageSize * loopCount;
                    }
                    var pdata = data.Skip(skip).Take(pageSize).ToArray();
                    var output = JsonConvert.SerializeObject(pdata);
                    string DATA = @"" + output + "";
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                    request.Method = "POST";
                    request.ContentType = "application/json";
                    request.Headers.Add("token", token);
                    request.ContentLength = DATA.Length;
                    StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                    requestWriter.Write(DATA);
                    requestWriter.Close();
                    try
                    {
                        WebResponse webResponse = request.GetResponse();
                        using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string response = responseReader.ReadToEnd();
                            Console.Out.WriteLine(response);
                            loopCount++;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
                    }
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, CookBook.Aspects.Constants.LogTraceCategoryNames.Important);
            }
        }

        #region Private Methods

        #endregion
    }
}
