﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.TestProject.Utilities
{
    public class EmployeeMasterData
    {
        public string EmployeeCode { get; set; }

        public string EmploymentStatus { get; set; }

        public string EmploymentType { get; set; }
        public string Salutation { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string EmployeeName { get; set; }

        public string FatherName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string Gender { get; set; }

        public string Company { get; set; }

        public string Department { get; set; }
        public string FunctionName { get; set; }

        public string Function { get; set; }

        public string Entity { get; set; }

        public string LTOGroup { get; set; }

        public string Role { get; set; }

        public string CompanyGroup { get; set; }
        public string HRSectorCode { get; set; }

        public string State { get; set; }

        public string Branch { get; set; }

        public string Division { get; set; }

        public string Location { get; set; }

        public string MaritalStatus { get; set; }

        public string PersonalEmailAddress { get; set; }

        public string OfficeEmailAddress { get; set; }

        public DateTime? DateofJoining { get; set; }

        public string Grade { get; set; }

        public string Designation { get; set; }

        public string ConfirmationStatus { get; set; }

        public string L1ManagerCode { get; set; }

        public string L1Manager { get; set; }

        public string L2ManagerCode { get; set; }

        public string L2Manager { get; set; }

        public string HRManagerCode { get; set; }

        public string HRManager { get; set; }

        public string Residence_AddressLine1 { get; set; }

        public string Residence_AddressLine2 { get; set; }

        public string Residence_State { get; set; }

        public string Residence_City { get; set; }

        public string Residence_Country { get; set; }

        public string ResidencePostalCode { get; set; }

        public string Residence_Landline { get; set; }

        public string Residence_Mobile { get; set; }

        public string Permanent_AddressLine1 { get; set; }

        public string Permanent_AddressLine2 { get; set; }

        public string Permanent_State { get; set; }

        public string Permanent_City { get; set; }

        public string Permanent_Country { get; set; }

        public string PermanentPostalCode { get; set; }

        public string Permanent_Landline { get; set; }

        public string Permanent_Mobile { get; set; }

        public string BloodGroup { get; set; }

        public DateTime? ConfirmationDate { get; set; }

        public string EmergencyContact_Name { get; set; }

        public string EmergencyContact_Relationship { get; set; }

        public string EmergencyContact_MobilePhone { get; set; }

        public string PFAccountNo { get; set; }

        public string PANNo { get; set; }

        public string BankName { get; set; }

        public string BankAccountNo { get; set; }

        public string IFSCCode { get; set; }

        public string UANNumber { get; set; }

        public int? NoticePeriod { get; set; }

        public string MobileNumber { get; set; }

        public DateTime? DateOfResignation { get; set; }

        public DateTime? FinalApprovedLWD { get; set; }

        public string NPSAccountNo { get; set; }

        public string BirthPlace { get; set; }

        public string Religion { get; set; }

        public string ESINo { get; set; }

        public string BusinessDesignation { get; set; }

        public string IsDisability { get; set; }

        public string TypeofDisability { get; set; }

        public int CurrentCompanyExperience { get; set; }

        public int TotalExperience { get; set; }

        public int Age { get; set; }

        public string Adharno { get; set; }
    }
}
