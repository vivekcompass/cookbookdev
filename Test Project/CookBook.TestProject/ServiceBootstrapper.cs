﻿using CookBook.Aspects.Factory;
using CookBook.Ioc;

namespace CookBook.TestProject
{
    public class ServiceBootstrapper
    {
        public static IIocManager IocEngine { get; private set; }
        public static void Initialize()
        {
            IocEngine = new IocManager(DiscoveryStrategy.SearchBaseDirectory);
            LogTraceFactory.InitializeLoggingService();
            ExceptionFactory.InitializeExceptionAopFramework();
        }
    }
}
