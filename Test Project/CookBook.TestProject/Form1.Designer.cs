﻿namespace CookBook.TestProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNutritionDataProcess = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnNutritionDataProcess
            // 
            this.btnNutritionDataProcess.Location = new System.Drawing.Point(104, 73);
            this.btnNutritionDataProcess.Name = "btnNutritionDataProcess";
            this.btnNutritionDataProcess.Size = new System.Drawing.Size(229, 23);
            this.btnNutritionDataProcess.TabIndex = 0;
            this.btnNutritionDataProcess.Text = "Execute NutritionData Process";
            this.btnNutritionDataProcess.UseVisualStyleBackColor = true;
            this.btnNutritionDataProcess.Click += new System.EventHandler(this.btnNutritionDataProcess_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1171, 536);
            this.Controls.Add(this.btnNutritionDataProcess);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Master Common Service";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNutritionDataProcess;
    }
}

