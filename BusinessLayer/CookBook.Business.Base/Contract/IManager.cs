﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookBook.Mapper;
using CookBook.DataAdapter.Base.Contracts;

namespace CookBook.Business.Base.Contract
{
    public interface IManager
    {
        IRepositoryTransactionManager TransactionManager { get; set; }

        IEntityMapper MapEngine { get; set; }
    }
}
