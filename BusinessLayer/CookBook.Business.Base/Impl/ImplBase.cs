﻿using CookBook.Business.Base.Contract;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.Mapper;
using Microsoft.Practices.Unity;

namespace CookBook.Business.Base.Impl
{
    public class ImplBase : IManager
    {
        [Dependency]
        public IRepositoryTransactionManager TransactionManager { get; set; }

        [Dependency]
        public IEntityMapper MapEngine { get; set; }
    }
}
