﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Business.Contract
{
    public interface IFoodCostSimulatorManager
    {
        List<DayPartData> GetDayPartBySiteList(RequestData requestData, int siteid);
        //List<SimulationList> GetSimulationDataListWithPaging(RequestData requestData, int pageindex, int totalRecordsPerPage);
        List<FoodCostSimulationData> GetSimulationDataListWithPaging(RequestData requestData, UserMasterResponseData userData);
        List<SimulationItemsData> GetItemsForSimulation(RequestData requestData, int siteId, int regionId, List<int> daypartId, bool checksiteItem);
        List<SimulationRegionItemDishCategoryData> GetSimulationRegionItemDishCategories(RequestData requestData, int siteId, int regionId, int itemId);
        SimulationBoardData GetSimulationBoard(int simId, RequestData requestData);
        string SaveSimulationBoard(SimulationBoardData model, RequestData requestData, FoodCostSimulationData simStatusUpdate);
        IList<SiteRecipeData> UpdateAndFetchSiteDishRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode);
        IList<SiteRecipeData> UpdateAndFetchSiteRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode);
        string SaveFoodCostSimSiteRecipeData(RecipeData modelr, SiteRecipeData model, IList<SiteRecipeMOGMappingData> baseRecipes, IList<SiteRecipeMOGMappingData> mogs, string simCode, RequestData requestData);
        IList<RecipeMOGMappingData> GetSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode, string simCode, RequestData requestData);
        string SaveFCSAPLHistory(FoodCostSimAPLHistoryData entity, RequestData requestData);
        IList<APLMasterData> GetFCSAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, string simuCode, RequestData requestData);
        IList<DishData> GetDishDataList(RequestData requestData, string siteCode, int itemId);

    }
}
