﻿using CookBook.Data.Common;
using System.Collections.Generic;
using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.Business.Contract
{
    public interface IMasterDataManager
    {
        IList<AllergenData> GetAllergenDataList( RequestData requestData);
        IList<SiteTypeData> GetSiteTypeDataList(RequestData requestData);
        List<APLMasterData> GetAPLMasterDataList(string articleNumber, string mogCode, RequestData requestData);
        List<APLMasterData> GetAPLMasterDataListForSector(string articleNumber, string mogCode, RequestData requestData);

        List<APLMasterData> NationalGetAPLMasterDataList(RequestData requestData);

        List<APLMasterData> GetAPLMasterDataListPageing(RequestData requestData, int pageindex);

        List<APLMasterData> GetNationalAPLExcelList(RequestData requestData);

        IList<APLMOGDATA> GetNOTMappedMOGDataListAPL(RequestData requestData);

        string UpdateMOGLISTAPL(RequestData request, string xml, string username);

        List<APLMAPPEDHISTORYDLIST> GetMOGAPLHISTORYDetAPL(RequestData requestData, string username);

        //List<CuisineMaster> GetCuisineMaster(RequestData requestData);

        List<APLMAPPEDHISTORYDLIST> GetMOGAPLHISTORYAPL(RequestData requestData, string username);

        List<APLMAPPEDHISTORYDLIST> GetshowGetALLHISTORYBATCHWISEAPL(RequestData requestData, string username,string batchno);

        List<APLMAPPEDHISTORYDLIST> GetALLHISTORYDetailsAPL(RequestData requestData, string username);

        IList<APLMasterData> GetAPLMasterRegionDataList(string articleNumber, string mogCode,int Region_ID, RequestData requestData);
        IList<APLMasterData> GetAPLMasterSiteDataList(string articleNumber, string mogCode,string SiteCode, RequestData requestData);
        IList<ConceptType1Data> GetConceptType1DataList( RequestData requestData);
        IList<ConceptType2Data> GetConceptType2DataList( RequestData requestData);
        IList<ConceptType3Data> GetConceptType3DataList( RequestData requestData);
        IList<ConceptType4Data> GetConceptType4DataList( RequestData requestData);
        IList<ConceptType5Data> GetConceptType5DataList( RequestData requestData);
        IList<DayPartData> GetDayPartDataList( RequestData requestData);
        IList<CuisineMasterData> GetCuisineDataList( RequestData requestData);

        string SaveCuisine(RequestData requestData, CuisineMasterData model, int username);
        string UpdateCuisine(RequestData requestData, CuisineMasterData model, int username);

        IList<HealthTagMasterData> GetHealthTagDataList(RequestData requestData);

        string SaveHealthTag(RequestData requestData, HealthTagMasterData model, int username);
        string UpdateHealthTag(RequestData requestData, HealthTagMasterData model, int username);

        IList<LifeStyleTagMasterData> GetLifeStyleTagDataList(RequestData requestData);

        string SaveLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username);
        string UpdateLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username);
        IList<RevenueTypeData> GetRevenueTypeDataList(RequestData requestData);

        IList<DietCategoryData> GetDietCategoryDataList( RequestData requestData);
        IList<ReasonData> GetReasonDataList( RequestData requestData);
        IList<SubSectorData> GetSubSectorDataList(RequestData requestData);
        IList<DishCategoryData> GetDishCategoryDataList( RequestData requestData);
        IList<DishCategorySiblingData> GetDishCategorySiblingDataList( RequestData requestData);

        IList<NationalDishCategoryData> NationalGetDishCategoryDataList(RequestData requestData);
        IList<NationalDishData> NationalGetDishDataList(RequestData requestData);


        IList<DishData> GetDishDataList( RequestData requestData);
        IList<DishRecipeMappingData> GetDishRecipeMappingDataList( RequestData requestData);
        IList<DishSubCategoryData> GetDishSubCategoryDataList( RequestData requestData);
        IList<DishTypeData> GetDishTypeDataList( RequestData requestData);
        IList<FoodProgramData> GetFoodProgramDataList( RequestData requestData);
        IList<ContainerData> GetContainerDataList( RequestData requestData);
        IList<ItemData> GetItemDataList( RequestData requestData);
        IList<GetUOMModuleMappingGridData> procGetUOMModuleMappingGridData(RequestData requestData);

        IList<ItemDishMappingData> GetItemDishMappingDataList( RequestData requestData);
        IList<ItemType1Data> GetItemType1DataList( RequestData requestData);
        IList<ItemType2Data> GetItemType2DataList( RequestData requestData);
        IList<MOGData> GetMOGDataList( RequestData requestData);
        IList<RecipeData> GetRecipeDataList( RequestData requestData);
        IList<UOMModuleMappingData> GetUOMModuleMappingDataList(RequestData requestData);
        IList<UOMModuleData> GetUOMModuleDataList(RequestData requestData);

        IList<RecipeMOGMappingData> GetRecipeMOGMappingDataList( RequestData requestData);
        IList<ServewareData> GetServewareDataList( RequestData requestData);
        IList<ServingTemperatureData> GetServingTemperatureDataList( RequestData requestData);
        IList<SiteMasterData> GetSiteMasterDataList( RequestData requestData);
        //Krish
        IList<SiteMasterData> GetSiteByRegionDataList(int regionId, RequestData requestData);
        IList<SiteMasterData> NationalGetSiteMasterDataList(RequestData requestData);
        IList<RegionMasterData> GetRegionMasterDataList( RequestData requestData);
        IList<SiteProfile1Data> GetSiteProfile1DataList( RequestData requestData);
        IList<SiteProfile2Data> GetSiteProfile2DataList( RequestData requestData);
        IList<SiteProfile3Data> GetSiteProfile3DataList( RequestData requestData);
        IList<UOMData> GetUOMDataList( RequestData requestData);
        IList<ReasonTypeData> GetReasonTypeDataList(RequestData requestData);
        IList<VisualCategoryData> GetVisualCategoryDataList( RequestData requestData);
        IList<CPUData> GetCPUDataList( RequestData requestData);
        IList<DKData> GetDKDataList( RequestData requestData);
        IList<CafeData> GetCafeDataList( RequestData requestData);
        IList<CapringMasterData> GetCapringMasterDataList(RequestData requestData);
        IList<ContainerTypeData> GetContainerTypeDataList( RequestData requestData);
        IList<ApplicationSettingData> GetApplicationSettingDataList( RequestData requestData);
        string SaveFoodProgramData(FoodProgramData model, RequestData requestData);
        string SaveContainerData(ContainerData model, RequestData requestData);

        string SaveVisualCategoryData(VisualCategoryData model, RequestData requestData);
        
        string SaveDishCategoryData(DishCategoryData model, RequestData requestData);
        
        string SaveDishSubCategoryData(DishSubCategoryData model, RequestData requestData);
        
        string SaveConceptType1Data(ConceptType1Data model, RequestData requestData);
        
        string SaveConceptType2Data(ConceptType2Data model, RequestData requestData);

        string SaveConceptType3Data(ConceptType3Data model, RequestData requestData);
        
        string SaveConceptType4Data(ConceptType4Data model, RequestData requestData);
        
        string SaveConceptType5Data(ConceptType5Data model, RequestData requestData);

        string SaveSiteProfile1Data(SiteProfile1Data model, RequestData requestData);

        string SaveSiteProfile2Data(SiteProfile2Data model, RequestData requestData);
        string SaveSiteProfile3Data(SiteProfile3Data model, RequestData requestData);

        string SaveItemType1Data(ItemType1Data model, RequestData requestData);

        string SaveItemType2Data(ItemType2Data model, RequestData requestData);
        
        string SaveDietCategoryData(DietCategoryData model, RequestData requestData);

        string SaveDayPartData(DayPartData model, RequestData requestData);
        string SaveReasonData(ReasonData model, RequestData requestData);

        string SaveRevenueTypeData(RevenueTypeData model, RequestData requestData);

        string SaveServewareData(ServewareData model, RequestData requestData);
        string SaveSiteTypeData(SiteTypeData model, RequestData requestData);

        string SaveAllergenData(AllergenData model, RequestData requestData);

        string SaveAPLMasterData(APLMasterData model, RequestData requestData);
        string SaveAPLMasterDataList(IList<APLMasterData> model, RequestData requestData);

        string SaveUOMData(UOMData model, RequestData requestData);
        string SaveUOMModuleData(UOMModuleData model, RequestData requestData);
        string SaveUOMModuleMappingData(UOMModuleMappingData model, RequestData requestData);
        string SaveReasonTypeData(ReasonTypeData model, RequestData requestData);
        string SaveServingTemperatureData(ServingTemperatureData model, RequestData requestData);

        string SaveSiteData(SiteMasterData model, RequestData requestData);
        
        string SaveSiteDataList(IList<SiteMasterData> model, RequestData requestData);
        
        string SaveCafeData(CafeData model, RequestData requestData);
        string SaveContainerTypeData(ContainerTypeData model, RequestData requestData);
        string SaveApplicationSettingData(ApplicationSettingData model, RequestData requestData);
        string SaveCafeDataList(IList<CafeData> model, RequestData requestData);
        string SaveContainerTypeDataList(IList<ContainerTypeData> model, RequestData requestData);
        string SaveColorData(ColorData model, RequestData requestData);
        IList<ColorData> GetColorDataList( RequestData requestData);
        IList<SiteDishContainerMappingData> GetSiteDishContainerMappingDataList(string siteCode, RequestData requestData);
        IList<SiteDishCategoryContainerMappingData> GetSiteDishCategoryContainerMappingDataList(string siteCode, RequestData requestData);

        string SaveSiteDishCategoryContainerMappingDataList(IList<SiteDishCategoryContainerMappingData> model, RequestData requestData);
        string SaveSiteDishContainerMappingDataList(IList<SiteDishContainerMappingData> model, RequestData requestData);
        string SaveSiteDishContainerMappingData(SiteDishContainerMappingData model, RequestData requestData);


        IList<RegionsDetailsByUserIdData> GetRegionsDetailsByUserIdDataList(RequestData requestData);
        IList<SiteDetailsByUserIdData> GetSiteDetailsByUserIdDataList(RequestData requestData);
        IList<UserRBFAMatrixData> GetUserRBFAMatrixDataByUserId(RequestData requestData);

        IList<ExpiryCategoryMasterData> GetExpiryCategoryDataList(RequestData requestData);
        IList<ShelfLifeUOMMasterData> GetShelfLifeUOMDataList(RequestData requestData);
        string SaveExpiryCategory(RequestData requestData, ExpiryCategoryMasterData model);
        string SaveNutritionMaster(RequestData requestData, NutritionElementMasterData model);
        IList<NutritionElementMasterData> GetNutritionMasterDataList(RequestData requestData);
        IList<NutritionElementTypeData> GetNutritionElementTypeDataList(RequestData requestData);
        IList<DietTypeData> GetDietTypeDataList(RequestData requestData);
        IList<TextureData> GetTextureDataList(RequestData requestData);
        IList<CommonMasterData> GetCommonMasterDataList(RequestData requestData, CommonMasterData commonMasterData);
        string SaveCommonMaster(RequestData requestData, CommonMasterData model);
        IList<MOGUOMData> GetMOGUOMDataList(RequestData requestData);

        //Krish
        IList<RangeTypeMasterData> GetRangeTypeMasterDataList(RequestData requestData);
        string SaveRangeTypeMaster(RequestData requestData, RangeTypeMasterData model);
        //Krish 11-10-2022
        string SaveRangeTypeAPLData(string articleNumber, string rangeTypeCode, RequestData requestData);
        //Krish 13-10-2022
        IList<ProcessTypeMasterData> GetProcessTypeMasterDataList(RequestData requestData);
        string SaveProcessTypeMaster(RequestData requestData, ProcessTypeMasterData model);
        string SaveProcessTypeMogData(RequestData requestData, List<string> processTypes, string mogCode, string rangeTypeCode, int inputStdDuration);
        IList<MogProcessTypeMappingData> GetProcessTypeMogData(RequestData requestData, string mogCode);
        IList<MogProcessTypeMappingData> GetProcessMappingDataList(RequestData requestData);
        string SaveAllergenMogData(RequestData requestData, List<string> contains, List<string> maycontains, string mogCode);
    }
}
