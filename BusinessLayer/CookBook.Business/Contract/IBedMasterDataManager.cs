﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Business.Contract
{
    public interface IBedMasterDataManager
    {
        List<BedMasterData> GetBedMaster(RequestData requestData);
        List<BedMasterHISTORY> GetBedMasterHISTORY(RequestData requestData, string username);
        List<BedMasterHISTORY> GetBedMasterHISTORYDet(RequestData requestData, string username);
        List<BedMasterHistoryBatchWise> GetshowGetALLBedMasterHISTORYBATCHWISE(RequestData requestData, string batchno);
        string AddUpdateBedMasterList(RequestData request, string xml, string username);
        string SaveBedMasterStatus(RequestData request, int id, int username);
        List<BedMasterHistoryAllData> GetALLHISTORYDetailsBedMaster(RequestData request);
    }
}
