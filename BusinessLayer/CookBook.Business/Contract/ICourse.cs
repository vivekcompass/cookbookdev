﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HSEQ.Business.Base.Contract;
using HSEQ.Data.Course;
using HSEQ.DataAdapter.Factory.Contract;

namespace HSEQ.Business.Contract
{
    public interface ICourse : IManager
    {
        ICourseRepository CourseRepository { get; set; }

        IList<CourseData> GetAllCourses();

        bool SaveCourse(CourseData course);

        CourseData GetCourse(int id);

        CourseData GetCourseByCode(string code);

        bool DeleteCourse(int courseId);

        IList<CourseCategoryMasterData> GetCourseCategories();

        IList<CourseKeywordMasterData> GetCourseKeywords();

        Tuple<IList<CourseData>, int> GetPagedCourseData(int pageNum, int pageSize = 0, string orderby = null, string orderExpression = null);
        
    }
}
