﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HSEQ.Business.Base.Contract;
using HSEQ.Data.Course;

namespace HSEQ.Business.Contract
{
    public interface ICourseCategory : IManager
    {
        IList<CourseCategoryMasterData> GetCourseCategories();

        bool SaveCourse(CourseCategoryMasterData courseCategory);
    }
}
