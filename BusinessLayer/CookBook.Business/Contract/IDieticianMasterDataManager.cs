﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Business.Contract
{
    public interface IDieticianMasterDataManager
    {
        List<DieticianMasterData> GetDieticianMaster(RequestData requestData);
        List<DieticianMasterHISTORY> GetDieticianMasterHISTORY(RequestData requestData, string username);
        List<DieticianMasterHISTORY> GetDieticianMasterHISTORYDet(RequestData requestData, string username);
        List<DieticianMasterHistoryBatchWise> GetshowGetALLDieticianMasterHISTORYBATCHWISE(RequestData requestData,  string batchno);
        string AddUpdateDieticianMasterList(RequestData request, string xml, string username);
        string SaveDieticianMasterStatus(RequestData request, int id, int username);
        List<DieticianMasterHistoryAllData> GetALLHISTORYDetailsDieticianMaster(RequestData request);
        string SaveHISPatientProfileMaster(RequestData request, List<HISPatientProfileData> hisPatientProfileData);
        string SaveHISUserData(RequestData request, List<HISUserData> hisUserData);
        string SaveHISBedMasterData(RequestData request, List<HISBedMasterData> hisBedMasterData);
    }
}
