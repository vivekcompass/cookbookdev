﻿using CookBook.Data.Common;
using System.Collections.Generic;
using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.Business.Contract
{
    public interface IMOGDataManager
    {
        IList<MOGData> GetMOGDataList(RequestData requestData, string SiteCode,int regionID);
        IList<MOGInheritedData> GetInheritedMOGDataList(RequestData requestData);
        IList<GetExpiryProductData> GetExpiryProductDataList(RequestData requestData);
        IList<NationalMOGData> GetNationalMOGDataList(RequestData requestData);
        IList<MOGNotMapped> GetNOTMappedMOGDataList(RequestData requestData);

        

        List<MOGAPLMAPPEDHISTORY> GetMOGAPLHISTORY(RequestData requestData, string username);

        List<MOGAPLMAPPEDHISTORY> GetALLHISTORYDetails(RequestData requestData, string username);

        List<MOGAPLMAPPEDHISTORY> GetMOGAPLHISTORYDet(RequestData requestData, string username);


        List<MOGAPLMAPPEDHISTORY> GetshowGetALLHISTORYBATCHWISE(RequestData requestData, string username,string batchno);

        string UpdateMOGLIST(RequestData requestData, string xml, string username);

        string SaveMOGData(MOGData model, RequestData requestData);
        
        string SaveMOGDataList(IList<MOGData> model, RequestData requestData);
        MOGImpactedData GetMOGImpactedDataList(int mogID, int RecipeID, bool isBaseRecipe, RequestData requestData);
        MOGAPLImpactedData GetMOGAPLImpactedDataList(int mogID, string aplList, int sectorID, RequestData requestData);
        IList<APLMasterData> GetAPLMasterDataListCommon(string articleNumber, string mogCode, RequestData requestData);
        string SaveExpiryProductData(GetExpiryProductData model, RequestData requestData);

        //Krish
        IList<GetExpiryProductData> GetExpiryProductDataOnlyExpiryNameList(RequestData requestData);
    }
}
