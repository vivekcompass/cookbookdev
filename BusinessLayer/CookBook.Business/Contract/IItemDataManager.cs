﻿using CookBook.Data.Common;
using System.Collections.Generic;
using CookBook.Data.MasterData;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using CookBook.Data.Request;

namespace CookBook.Business.Contract
{

    public interface IItemDataManager
    {
        IList<ItemData> GetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, string sectorName, RequestData requestData);
        IList<ItemExportData> GetItemExportDataList( RequestData requestData);

        IList<NationalItemData> NationalGetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, string sectorName, RequestData requestData);

        IList<ItemData> GetDishImpactedItemList(string dishCode, RequestData requestData);
        IList<PriceHistoryData> GetSiteItemPriceHistory(string siteCode, string itemCode, RequestData requestData);

        IList<UnitPriceData> GetSiteUnitPrice(string siteCode, RequestData requestData);
        IList<SiteDayPartMappingData> GetSiteDayPartMapping(string siteCode, RequestData requestData);
        IList<ItemDishMappingData> GetItemDishMappingDataList(string itemCode, RequestData requestData);
        IList<ItemDishCategoryMappingData> GetItemDishCategoryMappingDataList(string itemCode, RequestData requestData);

        IList<ItemDishCategoryMappingData> GetNationalItemDishCategoryMappingDataList(string itemCode, string sectorcode, RequestData requestData);
        IList<SiteMasterData> GetSiteMasterList(RequestData requestData);
        IList<RegionItemDishMappingData> GetRegionItemDishMappingDataList(string itemCode, string regionID, RequestData requestData);
        IList<RegionItemDishMappingData> GetMasterRegionItemDishMappingDataList(string regionID, RequestData requestData);
        IList<RegionItemInheritanceMappingData> GetRegionItemInheritanceMappingDataList(string regionID, RequestData requestData);
        IList<SiteItemDishMappingData> GetMasterSiteItemDishMappingDataList(string siteCode, RequestData requestData);
        IList<SiteItemDishMappingData> GetSiteItemDishMappingDataList(string itemCode, string siteCode, RequestData requestData);
        IList<FoodBookNutritionData> GetFoodBookNutritionDataList(string sectorCode);
        IList<SiteItemDayPartMappingData> GetSiteItemDayPartMappingDataList(string itemCode, string siteCode, RequestData requestData);
        IList<SiteDayPartMappingData> GetSiteDayPartMappingDataList(string siteCode, RequestData requestData);
        IList<FoodProgramDayPartMappingData> GetFoodProgramDayPartMappingDataList(string foodCode, RequestData requestData);
        IList<SiteItemInheritanceMappingData> GetSiteItemInheritanceMappingDataList(string SiteCode, RequestData requestData);

        IList<RegionDishCategoryMappingData> GetRegionDishCategoryMappingDataList(string regionID, string itemCode, RequestData requestData);
        IList<SiteDishCategoryMappingData> GetSiteDishCategoryMappingDataList(string SiteCode, string itemCode, RequestData requestData);
        string SaveRegionItemDishMappingDataList(IList<RegionItemDishMappingData> model, RequestData requestData);

        string mydishcateinactiveupdate(IList<RegionDishCategoryMappingData> model, RequestData requestData);
        string SaveSiteItemDishMappingDataList(IList<SiteItemDishMappingData> model, RequestData requestData);
        string SaveSiteItemDayPartMappingDataList(IList<SiteItemDayPartMappingData> model, RequestData requestData);

        string SaveFoodProgramDayPartMappingDataList(IList<FoodProgramDayPartMappingData> model, RequestData requestData);

        string SaveSiteDayPartMappingDataList(IList<SiteDayPartMappingData> model,bool isClosingTime, RequestData requestData);

        string SaveItemData(ItemData model, RequestData requestData);

        string SaveItemDataList(IList<ItemData> model, RequestData requestData);
        string SaveItemDishMappingDataList(IList<ItemDishMappingData> model, RequestData requestData);
        string SaveItemDishCategoryMappingDataList(IList<ItemDishCategoryMappingData> model, RequestData requestData);
        string SaveRegionDishCategoryMappingDataList(IList<RegionDishCategoryMappingData> model, RequestData requestData);
        string SaveSiteDishCategoryMappingDataList(IList<SiteDishCategoryMappingData> model, RequestData requestData);
        string SaveSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData requestData);
        string SaveMOGNationalToSectorInheritance(SaveMOGNationalToSectorInheritanceData model, RequestData requestData);
        string UpdateCostSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData requestData);

        IList<string> GetUniqueMappedItemCodesFromItem(RequestData requestData);
        IList<string> GetUniqueMappedItemCodesFromRegion(string RegionID, RequestData requestData);
        IList<RegionInheritedData> GetRegionInheritedData(string RegionID, string SectorID, RequestData requestData);
        string SaveRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData);
        string SaveToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData);
        string SaveToSiteInheritanceDataList(IList<SiteItemInheritanceMappingData> model, RequestData requestData);

        string UpdateCostRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData);

        IList<InheritanceChangesData> GetInheritChangesSectorToRegion(ItemInheritChangeRequestData itemInheritChangeRequestData, RequestData requestData);
        IList<InheritanceChangesData> GetInheritChangesRegionToSite(ItemInheritChangeRequestData itemInheritChangeRequestData, RequestData requestData);
        string SaveChangesSectorToRegion(List<InheritanceChangesData> model, RequestData requestData);
        string SaveChangesRegionToSite(List<InheritanceChangesData> model, RequestData requestData);

        SiteItemDropDownData GetSiteItemDropDownDataData(RequestData requestData);
        //Krish 28-07-2022
        SectorItemData GetSectorItemData(RequestData requestData, bool isAddEdit, bool onLoad);
        //Krish 30-07-2022
        RegionItemData GetRegionItemData(RequestData requestData, bool isSectorUser, int regionId = 0, bool gridOnly = false);
    }
}
