﻿using CookBook.Data.Common;
using System.Collections.Generic;
using CookBook.Data.MasterData;
using CookBook.Data.Request;

namespace CookBook.Business.Contract
{
    public interface ICostSimulatorManager
    {
        IList<DishRecipeMapData> GetRecipeMappingDataList(string DishCode, RequestData requestData);
        IList<SimulatedMappedRecipeMOGListSiteData> GetSimulatedMappedRecipeMOGListSiteDataList(int RecipeID, int SiteId, RequestData requestData);
        IList<SimulatedMappedRecipeMOGListSectorData> GetSimulatedMappedRecipeMOGListSectorDataList(int RecipeID, int RegionId, RequestData requestData);
        IList<SimulatedMappedRecipeMOGListNationalData> GetSimulatedMappedRecipeMOGListNationalDataList(int RecipeID, RequestData requestData);

    }
}
