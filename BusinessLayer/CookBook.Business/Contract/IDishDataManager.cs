﻿using CookBook.Data.Common;
using System.Collections.Generic;
using CookBook.Data.MasterData;
using CookBook.Data.Request;

namespace CookBook.Business.Contract
{
    public interface IDishDataManager
    {
        IList<DishData> GetDishDataList(int? foodProgramID, int? dietCategoryID, RequestData requestData);
        IList<DishData> GetSiteDishDataList(string siteCode, int? foodProgramID, int? dietCategoryID, RequestData requestData);
        IList<DishData> GetRegionDishMasterData(int RegionID, int? foodProgramID, int? dietCategoryID, RequestData requestData);
        IList<DishData> GetRegionDishDataList(int RegionID,int? foodProgramID, int? dietCategoryID, string subSectorCode, RequestData requestData);
        string SaveDishData(DishData model, RequestData requestData);
        
        string SaveDishDataList(IList<DishData> model, RequestData requestData);
        IList<DishCategoryData> GetDishCategory( RequestData requestData);
        string SaveDishRecipeMappingList(IList<DishRecipeMappingData> model, RequestData requestData);
        string SaveRegionDishRecipeMappingList(IList<RegionDishRecipeMappingData> model, RequestData requestData);
        string SaveSiteDishRecipeMappingList(IList<SiteDishRecipeMappingData> model, RequestData requestData);
        IList<DishRecipeMappingData> GetDishRecipeMappingDataList(string DishCode, RequestData requestData);
        IList<RegionDishRecipeMappingData> GetRegionDishRecipeMappingDataList(int RegionID,string DishCode, string SubSectorCode, RequestData requestData);
        IList<SiteDishRecipeMappingData> GetSiteDishRecipeMappingDataList(string SiteCode, string DishCode, RequestData requestData);
        IList<PlanningTagData> GetPlanningTag(RequestData requestData);
        string SavePlanningTagData(PlanningTagData model, RequestData requestData);
        //Krish 02-08-2022
        SectorDishData GetSectorDishData(RequestData requestData, string condition, bool isAddEdit, bool onLoad);
        //Krish 08-10-2022
        string SaveRangeTypeDishData(string dishCode, string rangeTypeCode, RequestData requestData);
    }
}
