﻿using CookBook.Business.Base.Contract;
using CookBook.Data.Common;
using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using System.Collections.Generic;

namespace CookBook.Business.Contract
{
    public interface IUser: IManager
    {
        UserMasterResponseData GetUserData(string userName, RequestData requestData);

        string AddUpdateUserDetails(UserMasterData userData, RequestData requestData);

        string DeleteUserData(string userName, RequestData requestData);
        IList<SectorData> GetSectorDataByUserID(int userID, RequestData requestData);
        string ChangeSectorData(int UserID, string sectorNumber, RequestData requestData);
        string ChangeUserRoleData(int UserID, int userRoleID, RequestData requestData);
        IList<ModuleData> GetModulesByUserID(int userID, RequestData requestData);
        IList<UserPermissionData> GetUserPermissionsByModuleCode(int userID, string moduleCode, RequestData requestData);
        IList<UserMasterData> GetUserMasterDataList(RequestData requestData);

        List<ManageSectorData> GetSectorDataList(RequestData requestData);

        List<ManageSectorData> GetSectorListInfo(RequestData requestData);

        List<LevelMasterData> GetLevelMasterData(RequestData requestData);

        List<FunctionMasterData> GetFunctionMasterData(RequestData requestData);

        List<PermissionMasterData> GetPermissionMasterData(RequestData requestData);


        string SaveManageSectorInfo(RequestData request, ManageSectorData model, int usercode);


        string SaveManageLevelInfo(RequestData request, LevelMasterData model, int usercode);


        string SaveFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode);

        string SavePermissionInfo(RequestData request, PermissionMasterData model, int usercode);


        string SaveCapringInfo(RequestData request, CapringMastre model, int usercode);

        string UpdateCapringInfo(RequestData request, CapringMastre model, int usercode);

        string UpdateCapringDescriptionInfo(RequestData request, CapringMastre model, int usercode);

        string UpdatePermissionInfo(RequestData request, PermissionMasterData model, int usercode);

        string UpdateFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode);

        string UpdatefunctionInactiveActive(RequestData request, FunctionMasterData model, int usercode);

        string UpdatePermissionInactiveActive(RequestData request, PermissionMasterData model, int usercode);

        string UpdateManageLevelInfo(RequestData request, LevelMasterData model, int usercode);


        string UpdateManageSectorInfo(RequestData request, ManageSectorData model, int usercode);

        string UpdateInactiveActive(RequestData request, ManageSectorData model, int usercode);

        string UpdateLevelInactiveActive(RequestData request, LevelMasterData model, int usercode);

        string UpdateCapringInactiveActive(RequestData request, CapringMastre model, int usercode);

        List<LevelMasterData> GetLevelDataList(RequestData requestData);

        List<CapringMastre> GetCapringMasterList(RequestData requestData);

        List<FunctionMasterData> GetFunctionDataList(RequestData requestData);

        List<PermissionMasterData> GetPermissionDataList(RequestData requestData);
        IList<UserRoleData> GetUserRoleDataByUserID(int userID, RequestData requestData);

    }
}
