﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HSEQ.Business.Base.Contract;
using HSEQ.DataAdapter.Factory.Contract;

namespace HSEQ.Business.Contract
{
    public interface IUserProduct : IManager
    {
        IUserProductRepository UserProductRepository { get; set; }
    }
}
