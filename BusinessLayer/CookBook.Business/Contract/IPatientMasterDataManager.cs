﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Business.Contract
{
    public interface IPatientMasterDataManager    
    {
        List<PatientMasterData> GetPatientMaster(RequestData requestData);
        List<PatientMasterHISTORY> GetPatientMasterHISTORY(RequestData requestData, string username);
        List<PatientMasterHISTORY> GetPatientMasterHISTORYDet(RequestData requestData, string username);
        List<PatientMasterHistoryBatchWise> GetshowGetALLPatientMasterHISTORYBATCHWISE(RequestData requestData, string batchno);
        string AddUpdatePatientMasterList(RequestData request, string xml, string username);
        string SavePatientMasterStatus(RequestData request, int id, int username);
        List<PatientMasterHistoryAllData> GetALLHISTORYDetailsPatientMaster(RequestData request);
    }
}
