﻿using CookBook.Data.Common;
using System.Collections.Generic;
using CookBook.Data.MasterData;
using CookBook.Data.Request;

namespace CookBook.Business.Contract
{
    public interface IRecipeDataManager
    {
        IList<RecipeData> GetRecipeDataList(string condition,  string DishCode, RequestData requestData);
        IList<ItemDishRecipeNutritionData> GetItemDishRecipeNutritionData(string SiteCode, RequestData requestData);

        IList<NationalRecipeData> GetNationalRecipeDataList(string condition, string DishCode, RequestData requestData);
        IList<RegionRecipeData> GetRegionRecipeDataList(int RegionID,string condition, RequestData requestData);
        IList<SiteRecipeData> GetSiteRecipeDataList(string SiteCode, string condition, RequestData requestData);
        //Krish
        IList<SiteRecipeData> GetSiteDishRecipeDataList(string SiteCode, string dishCode, string condition, RequestData requestData);
        IList<RecipeMOGMappingData> GetBaseRecipesByRecipeID(int recipeID, RequestData requestData);
        IList<RecipeMOGMappingData> GetRegionBaseRecipesByRecipeID(int regionID, int recipeID, RequestData requestData);
        IList<RecipeMOGMappingData> GetSiteBaseRecipesByRecipeID(string SiteCode, int recipeID,string recipeCode, RequestData requestData);
        IList<RecipeMOGMappingData> GetMOGsByRecipeID(int recipeID, RequestData requestData);
        IList<RecipeMOGMappingData> GetRegionMOGsByRecipeID(int regionID,int recipeID, RequestData requestData);
        IList<RecipeMOGMappingData> GetSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode, RequestData requestData);

        string SaveRecipeData(RecipeData model, IList<RecipeMOGMappingData> baseRecipes, IList<RecipeMOGMappingData> mogs, RequestData requestData);
        string SaveRegionRecipeData(RegionRecipeData model, IList<RegionRecipeMOGMappingData> baseRecipes, IList<RegionRecipeMOGMappingData> mogs, RequestData requestData);
       string SaveSiteRecipeData(SiteRecipeData model, IList<SiteRecipeMOGMappingData> baseRecipes, IList<SiteRecipeMOGMappingData> mogs, RequestData requestData);
        string SaveRecipeNutrientMappingData(RecipeData entity, RequestData requestData);
        string SaveRecipeDataList(IList<RecipeData> model, RequestData requestData);
        string SaveRegionRecipeDataList(IList<RegionRecipeData> model, RequestData requestData);
        string SaveSiteRecipeDataList(IList<SiteRecipeData> model, RequestData requestData);
        string SaveSiteRecipeAllergenData(string recipeCode, string siteCode, RequestData requestData);
        RecipePrintingData GetRecipePrintingData(string recipeCode, string siteCode, int regId, RequestData requestData);
        //Krish
        string SaveRecipeCopy(List<RecipeCopyData> model, List<string> recipes, string sourceSite, RequestData requestData);
        //Krish
        List<NutritionDishRecipeMogMapping> GetRecipeDishDataByDish(string dishCode, RequestData requestData);
        SectorRecipeData GetSectorRecipeData(RequestData requestData, bool isAddEdit, bool onLoad);
    }
}
