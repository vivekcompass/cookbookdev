﻿using CookBook.Data.Common;
using System.Collections.Generic;
using CookBook.Data.MasterData;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Data.Request;

namespace CookBook.Business.Contract
{
    public interface IFoodDataManager
    {

        string SaveMOGData(MOGData model, RequestData requestData);
        
        string SaveMOGDataList(IList<MOGData> model, RequestData requestData);
    }
}
