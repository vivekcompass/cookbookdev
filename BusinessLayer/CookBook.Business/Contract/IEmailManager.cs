﻿using CookBook.Business.Base.Contract;
using CookBook.Data.MasterData;
using CookBook.Data.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Business.Contract
{
    public interface IEmailManager : IManager
    {
        void SendExceptionEmail(RequestData requestData);
        void SendMOGEmail(MOGData model, RequestData requestData);
        void SendNewMOGEmail(MOGData model, RequestData requestData);
        void SendRecipeEmail(RecipeData model, RequestData requestData);

        bool SaveReminderEmail(string webSiteUrl, SendEmailFilter request, RequestData requestData);

        void SendGOVOOffSiteItemAutoInheritEmail(ItemData model, RequestData requestData);
    }
}
