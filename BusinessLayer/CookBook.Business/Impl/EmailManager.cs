﻿using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.Request;
using CookBook.DataAdapter.Factory;
using CookBook.DataAdapter.Factory.Contract;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.Composition;
using System;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.Data.MasterData;

namespace CookBook.Business.Impl
{
    [Export(typeof(IEmailManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class EmailManager : ImplBase, IEmailManager
    {
        [Dependency]
        public IEmailRepository EmailRepository { get; set; }

        [Dependency]
        public IUserMasterRepository UserMasterRepository { get; set; }

        public void SendExceptionEmail(RequestData requestData)
        {
            EmailRepository.SendPendingEmail(requestData);
        }

        public void SendMOGEmail(MOGData model, RequestData requestData)
        {
            EmailRepository.SendMOGEmail(model, requestData);
        }

        public void SendNewMOGEmail(MOGData model, RequestData requestData)
        {
            EmailRepository.SendNewMOGEmail(model, requestData);
        }
        public void SendRecipeEmail(RecipeData model, RequestData requestData)
        {
            EmailRepository.SendRecipeEmail(model, requestData);
        }
        public bool SaveReminderEmail(string webSiteUrl, SendEmailFilter request, RequestData requestData)
        {
            return true;
        }

        public void SendGOVOOffSiteItemAutoInheritEmail(ItemData model, RequestData requestData)
        {
            EmailRepository.SendGOVOOffSiteItemAutoInheritEmail(model, requestData);
        }
    }
}
