﻿using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.Common;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using CookBook.Mapper;
using CookBook.DataAdapter.Factory.Contract;
using CookBook.DataAdapter.Factory.Repository;
using CookBook.Data.MasterData;
using System.Linq;
using CookBook.DataAdapter.Factory;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using System;

namespace CookBook.Business.Impl
{
    [Export(typeof(IMasterDataManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MasterDataManager : ImplBase, IMasterDataManager
    {
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IAllergenRepository AllergenRepository { get; set; }
        [Dependency]
        public ICafeRepository CafeRepository { get; set; }
        [Dependency]
        public IConceptType1Repository ConceptType1Repository { get; set; }
        [Dependency]
        public IConceptType2Repository ConceptType2Repository { get; set; }
        [Dependency]
        public IConceptType3Repository ConceptType3Repository { get; set; }
        [Dependency]
        public IConceptType4Repository ConceptType4Repository { get; set; }
        [Dependency]
        public IConceptType5Repository ConceptType5Repository { get; set; }
        [Dependency]
        public IDayPartRepository DayPartRepository { get; set; }
        [Dependency]
        public IDietCategoryRepository DietCategoryRepository { get; set; }
        [Dependency]
        public IDishCategoryRepository DishCategoryRepository { get; set; }
        [Dependency]
        public IDishDayPartMappingRepository DishDayPartMappingRepository { get; set; }
        [Dependency]
        public IDishRecipeMappingRepository DishRecipeMappingRepository { get; set; }
        [Dependency]
        public IDishRepository DishRepository { get; set; }
        [Dependency]
        public IDishTypeRepository DishTypeRepository { get; set; }
        [Dependency]
        public IDishSubCategoryRepository DishSubCategoryRepository { get; set; }
        [Dependency]
        public IFoodProgramRepository FoodProgramRepository { get; set; }
        [Dependency]
        public IItemDishMappingRepository ItemDishMappingRepository { get; set; }
        [Dependency]
        public IItemRepository ItemRepository { get; set; }
        [Dependency]
        public IItemType1Repository ItemType1Repository { get; set; }
        [Dependency]
        public IItemType2Repository ItemType2Repository { get; set; }
        [Dependency]
        public IMOGRepository MOGRepository { get; set; }
        [Dependency]
        public IRecipeMOGMappingRepository RecipeMOGMappingRepository { get; set; }
        [Dependency]
        public IRecipeRepository RecipeRepository { get; set; }
        [Dependency]
        public IServewareRepository ServewareRepository { get; set; }
        [Dependency]
        public IServingTemperatureRepository ServingTemperatureRepository { get; set; }
        [Dependency]
        public ISiteMasterRepository SiteMasterRepository { get; set; }
        [Dependency]
        public ISiteMOGMappingRepository SiteMOGMappingRepository { get; set; }
        [Dependency]
        public ISiteItemMappingRepository SiteItemMappingRepository { get; set; }
        [Dependency]
        public ISiteProfile1Repository SiteProfile1Repository { get; set; }
        [Dependency]
        public ISiteProfile2Repository SiteProfile2Repository { get; set; }
        [Dependency]
        public ISiteProfile3Repository SiteProfile3Repository { get; set; }
        [Dependency]
        public IUOMRepository UOMRepository { get; set; }
        [Dependency]
        public IUserMasterRepository UserMasterRepository { get; set; }
        [Dependency]
        public IUserRoleMasterRepository UserRoleMasterRepository { get; set; }
        [Dependency]
        public IVisualCategoryRepository VisualCategoryRepository { get; set; }

        public IList<DropDownDataModel> GetAllSiteMasterData()
        {
            List<DropDownDataModel> result = new List<DropDownDataModel>();
           
            return result;
        }

        public IList<AllergenData> GetAllergenDataList() {
            List<AllergenData> result = new List<AllergenData>();
            var entityList = AllergenRepository.GetAll().ToList();
            EntityMapper.Map<IList<Allergen>, IList<AllergenData>>(entityList, result);
            return result; 
        }
        public IList<ConceptType1Data> GetConceptType1DataList(){
            List<ConceptType1Data> result = new List<ConceptType1Data>();
            var entityList = ConceptType1Repository.GetAll().ToList();
            EntityMapper.Map<IList<ConceptType1>, IList<ConceptType1Data>>(entityList, result);
            return result;
        }
        public IList<ConceptType2Data> GetConceptType2DataList(){
            List<ConceptType2Data> result = new List<ConceptType2Data>();
            var entityList = ConceptType2Repository.GetAll().ToList();
            EntityMapper.Map<IList<ConceptType2>, IList<ConceptType2Data>>(entityList, result);
            return result;
        }
        public IList<ConceptType3Data> GetConceptType3DataList(){
            List<ConceptType3Data> result = new List<ConceptType3Data>();
            var entityList = ConceptType3Repository.GetAll().ToList();
            EntityMapper.Map<IList<ConceptType3>, IList<ConceptType3Data>>(entityList, result);
            return result;
        }
        public IList<ConceptType4Data> GetConceptType4DataList(){
            List<ConceptType4Data> result = new List<ConceptType4Data>();
            var entityList = ConceptType4Repository.GetAll().ToList();
            EntityMapper.Map<IList<ConceptType4>, IList<ConceptType4Data>>(entityList, result);
            return result;
        }
        public IList<ConceptType5Data> GetConceptType5DataList(){
            List<ConceptType5Data> result = new List<ConceptType5Data>();
            var entityList = ConceptType5Repository.GetAll().ToList();
            EntityMapper.Map<IList<ConceptType5>, IList<ConceptType5Data>>(entityList, result);
            return result;
        }
        public IList<DayPartData> GetDayPartDataList(){
            List<DayPartData> result = new List<DayPartData>();
            var entityList = DayPartRepository.GetAll().ToList();
            EntityMapper.Map<IList<DayPart>, IList<DayPartData>>(entityList, result);
            return result;
        }
        public IList<DietCategoryData> GetDietCategoryDataList(){
            List<DietCategoryData> result = new List<DietCategoryData>();
            var entityList = DietCategoryRepository.GetAll().ToList();
            EntityMapper.Map<IList<DietCategory>, IList<DietCategoryData>>(entityList, result);
            return result;
        }
        public IList<DishCategoryData> GetDishCategoryDataList(){
            List<DishCategoryData> result = new List<DishCategoryData>();
            var entityList = DishCategoryRepository.GetAll().ToList();
            EntityMapper.Map<IList<DishCategory>, IList<DishCategoryData>>(entityList, result);
            return result;
        }
        public IList<DishData> GetDishDataList(){
            List<DishData> result = new List<DishData>();
            var entityList = DishRepository.GetAll().ToList();
            EntityMapper.Map<IList<Dish>, IList<DishData>>(entityList, result);
            return result;
        }
        public IList<DishDayPartMappingData> GetDishDayPartMappingDataList(){
            List<DishDayPartMappingData> result = new List<DishDayPartMappingData>();
            var entityList = DishDayPartMappingRepository.GetAll().ToList();
            EntityMapper.Map<IList<DishDayPartMapping>, IList<DishDayPartMappingData>>(entityList, result);
            return result;
        }
        public IList<DishRecipeMappingData> GetDishRecipeMappingDataList(){
            List<DishRecipeMappingData> result = new List<DishRecipeMappingData>();
            var entityList = DishRecipeMappingRepository.GetAll().ToList();
            EntityMapper.Map<IList<DishRecipeMapping>, IList<DishRecipeMappingData>>(entityList, result);
            return result;
        }
        public IList<DishSubCategoryData> GetDishSubCategoryDataList(){
            List<DishSubCategoryData> result = new List<DishSubCategoryData>();
            var entityList = DishSubCategoryRepository.GetAll().ToList();
            EntityMapper.Map<IList<DishSubCategory>, IList<DishSubCategoryData>>(entityList, result);
            return result;
        }
        public IList<DishTypeData> GetDishTypeDataList()
        {
            List<DishTypeData> result = new List<DishTypeData>();
            var entityList = DishTypeRepository.GetAll().ToList();
            EntityMapper.Map<IList<DishType>, IList<DishTypeData>>(entityList, result);
            return result;
        }
        public IList<FoodProgramData> GetFoodProgramDataList(){
            List<FoodProgramData> result = new List<FoodProgramData>();
            var entityList = FoodProgramRepository.GetAll().ToList();
            EntityMapper.Map<IList<FoodProgram>, IList<FoodProgramData>>(entityList, result);
            return result;
        }
        public IList<ItemData> GetItemDataList(){
            List<ItemData> result = new List<ItemData>();
            var entityList = ItemRepository.GetAll().ToList();
            EntityMapper.Map<IList<Item>, IList<ItemData>>(entityList, result);
            return result;
        }
        public IList<ItemDishMappingData> GetItemDishMappingDataList(){
            List<ItemDishMappingData> result = new List<ItemDishMappingData>();
            var entityList = ItemDishMappingRepository.GetAll().ToList();
            EntityMapper.Map<IList<ItemDishMapping>, IList<ItemDishMappingData>>(entityList, result);
            return result;
        }
        public IList<ItemType1Data> GetItemType1DataList(){
            List<ItemType1Data> result = new List<ItemType1Data>();
            var entityList = ItemType1Repository.GetAll().ToList();
            EntityMapper.Map<IList<ItemType1>, IList<ItemType1Data>>(entityList, result);
            return result;
        }
        public IList<ItemType2Data> GetItemType2DataList(){
            List<ItemType2Data> result = new List<ItemType2Data>();
            var entityList = ItemType2Repository.GetAll().ToList();
            EntityMapper.Map<IList<ItemType2>, IList<ItemType2Data>>(entityList, result);
            return result;
        }
        public IList<MOGData> GetMOGDataList(){
            List<MOGData> result = new List<MOGData>();
            var entityList = MOGRepository.GetAll().ToList();
            EntityMapper.Map<IList<MOG>, IList<MOGData>>(entityList, result);
            return result;
        }
        public IList<RecipeData> GetRecipeDataList(){
            List<RecipeData> result = new List<RecipeData>();
            var entityList = RecipeRepository.GetAll().ToList();
            EntityMapper.Map<IList<Recipe>, IList<RecipeData>>(entityList, result);
            return result;
        }
        public IList<RecipeMOGMappingData> GetRecipeMOGMappingDataList(){
            List<RecipeMOGMappingData> result = new List<RecipeMOGMappingData>();
            var entityList = RecipeMOGMappingRepository.GetAll().ToList();
            EntityMapper.Map<IList<RecipeMOGMapping>, IList<RecipeMOGMappingData>>(entityList, result);
            return result;
        }
        public IList<ServewareData> GetServewareDataList(){
            List<ServewareData> result = new List<ServewareData>();
            var entityList = ServewareRepository.GetAll().ToList();
            EntityMapper.Map<IList<Serveware>, IList<ServewareData>>(entityList, result);
            return result;
        }
        public IList<ServingTemperatureData> GetServingTemperatureDataList(){
            List<ServingTemperatureData> result = new List<ServingTemperatureData>();
            var entityList = ServingTemperatureRepository.GetAll().ToList();
            EntityMapper.Map<IList<ServingTemperature>, IList<ServingTemperatureData>>(entityList, result);
            return result;
        }
        public IList<SiteItemMappingData> GetSiteItemMappingDataList(){
            List<SiteItemMappingData> result = new List<SiteItemMappingData>();
            var entityList = SiteItemMappingRepository.GetAll().ToList();
            EntityMapper.Map<IList<SiteItemMapping>, IList<SiteItemMappingData>>(entityList, result);
            return result;
        }
        public IList<SiteMasterData> GetSiteMasterDataList(){
            List<SiteMasterData> result = new List<SiteMasterData>();
            var entityList = SiteMasterRepository.GetSiteDataList().Where(a=>a.SiteOrg == "IN10").ToList();
            EntityMapper.Map<IList<procGetSiteList_Result>, IList<SiteMasterData>>(entityList, result);
            return result;
        }
        public IList<SiteMOGMappingData> GetSiteMOGMappingDataList(){
            List<SiteMOGMappingData> result = new List<SiteMOGMappingData>();
            var entityList = SiteMOGMappingRepository.GetAll().ToList();
            EntityMapper.Map<IList<SiteMOGMapping>, IList<SiteMOGMappingData>>(entityList, result);
            return result;
        }
        public IList<SiteProfile1Data> GetSiteProfile1DataList(){
            List<SiteProfile1Data> result = new List<SiteProfile1Data>();
            var entityList = SiteProfile1Repository.GetAll().ToList();
            EntityMapper.Map<IList<SiteProfile1>, IList<SiteProfile1Data>>(entityList, result);
            return result;
        }
        public IList<SiteProfile2Data> GetSiteProfile2DataList(){
            List<SiteProfile2Data> result = new List<SiteProfile2Data>();
            var entityList = SiteProfile2Repository.GetAll().ToList();
            EntityMapper.Map<IList<SiteProfile2>, IList<SiteProfile2Data>>(entityList, result);
            return result;
        }
        public IList<SiteProfile3Data> GetSiteProfile3DataList(){
            List<SiteProfile3Data> result = new List<SiteProfile3Data>();
            var entityList = SiteProfile3Repository.GetAll().ToList();
            EntityMapper.Map<IList<SiteProfile3>, IList<SiteProfile3Data>>(entityList, result);
            return result;
        }
        public IList<UOMData> GetUOMDataList(){
            List<UOMData> result = new List<UOMData>();
            var entityList = UOMRepository.GetAll().ToList();
            EntityMapper.Map<IList<UOM>, IList<UOMData>>(entityList, result);
            return result;
        }
        public IList<VisualCategoryData> GetVisualCategoryDataList(){
            List<VisualCategoryData> result = new List<VisualCategoryData>();
            var entityList = VisualCategoryRepository.GetAll().ToList();
            EntityMapper.Map<IList<VisualCategory>, IList<VisualCategoryData>>(entityList, result);
            return result;
        }

        public IList<CPUData> GetCPUDataList()
        {
            List<CPUData> result = new List<CPUData>();
            var entityList = SiteMasterRepository.GetCPUDataList().ToList();
            EntityMapper.Map<IList<procGetCPUList_Result>, IList<CPUData>>(entityList, result);
            return result;
        }

        public IList<DKData> GetDKDataList()
        {
            List<DKData> result = new List<DKData>();
            var entityList = SiteMasterRepository.GetDKDataList().ToList();
            EntityMapper.Map<IList<procGetDKList_Result>, IList<DKData>>(entityList, result);
            return result;
        }

        public IList<CafeData> GetCafeDataList()
        {
            List<CafeData> result = new List<CafeData>();
            var entityList = CafeRepository.GetCafeDataList().ToList();
            EntityMapper.Map<IList<procGetCafeList_Result>, IList<CafeData>>(entityList, result);
            return result;
        }
        public string SaveFoodProgramData(FoodProgramData model)
        {
            FoodProgram entity = new FoodProgram();
            EntityMapper.Map<FoodProgramData, FoodProgram>(model, entity);
            var dup = FoodProgramRepository.Find(y => y.Name.Trim().ToLower() == model.Name.Trim().ToLower()).FirstOrDefault();
            if (dup != null && model.ID == 0)
                return "Duplicate";
            
             

            bool x = FoodProgramRepository.Save(entity);
            return x.ToString();
        }

        public string SaveVisualCategoryData(VisualCategoryData model)
        {
            VisualCategory entity = new VisualCategory();
            EntityMapper.Map<VisualCategoryData, VisualCategory>(model, entity);
            bool x = VisualCategoryRepository.Save(entity);
            return x.ToString();
        }

        public string SaveDishCategoryData(DishCategoryData model)
        {
            DishCategory entity = new DishCategory();
            EntityMapper.Map<DishCategoryData, DishCategory>(model, entity);
            if (entity.DishSubCategory_ID <= 0)
                entity.DishSubCategory_ID = null;
            bool x = DishCategoryRepository.Save(entity);
            return x.ToString();
        }

        public string SaveDishSubCategoryData(DishSubCategoryData model)
        {
            DishSubCategory entity = new DishSubCategory();
            EntityMapper.Map<DishSubCategoryData, DishSubCategory>(model, entity);
            bool x = DishSubCategoryRepository.Save(entity);
            return x.ToString();
        }

        public string SaveConceptType1Data(ConceptType1Data model)
        {
            ConceptType1 entity = new ConceptType1();
            EntityMapper.Map<ConceptType1Data, ConceptType1>(model, entity);
            bool x = ConceptType1Repository.Save(entity);
            return x.ToString();
        }

        public string SaveConceptType2Data(ConceptType2Data model)
        {
            ConceptType2 entity = new ConceptType2();
            EntityMapper.Map<ConceptType2Data, ConceptType2>(model, entity);
            bool x = ConceptType2Repository.Save(entity);
            return x.ToString();
        }

        public string SaveConceptType3Data(ConceptType3Data model)
        {
            ConceptType3 entity = new ConceptType3();
            EntityMapper.Map<ConceptType3Data, ConceptType3>(model, entity);
            bool x = ConceptType3Repository.Save(entity);
            return x.ToString();
        }

        public string SaveConceptType4Data(ConceptType4Data model)
        {
            ConceptType4 entity = new ConceptType4();
            EntityMapper.Map<ConceptType4Data, ConceptType4>(model, entity);
            bool x = ConceptType4Repository.Save(entity);
            return x.ToString();
        }

        public string SaveConceptType5Data(ConceptType5Data model)
        {
            ConceptType5 entity = new ConceptType5();
            EntityMapper.Map<ConceptType5Data, ConceptType5>(model, entity);
            bool x = ConceptType5Repository.Save(entity);
            return x.ToString();
        }

        public string SaveSiteProfile1Data(SiteProfile1Data model)
        {
            SiteProfile1 entity = new SiteProfile1();
            EntityMapper.Map<SiteProfile1Data, SiteProfile1>(model, entity);
            bool x = SiteProfile1Repository.Save(entity);
            return x.ToString();
        }

        public string SaveSiteProfile2Data(SiteProfile2Data model)
        {
            SiteProfile2 entity = new SiteProfile2();
            EntityMapper.Map<SiteProfile2Data, SiteProfile2>(model, entity);
            bool x = SiteProfile2Repository.Save(entity);
            return x.ToString();
        }

        public string SaveSiteProfile3Data(SiteProfile3Data model)
        {
            SiteProfile3 entity = new SiteProfile3();
            EntityMapper.Map<SiteProfile3Data, SiteProfile3>(model, entity);
            bool x = SiteProfile3Repository.Save(entity);
            return x.ToString();
        }

        public string SaveItemType1Data(ItemType1Data model)
        {
            ItemType1 entity = new ItemType1();
            EntityMapper.Map<ItemType1Data, ItemType1>(model, entity);
            bool x = ItemType1Repository.Save(entity);
            return x.ToString();
        }

        public string SaveItemType2Data(ItemType2Data model)
        {
            ItemType2 entity = new ItemType2();
            EntityMapper.Map<ItemType2Data, ItemType2>(model, entity);
            bool x = ItemType2Repository.Save(entity);
            return x.ToString();
        }

        public string SaveDietCategoryData(DietCategoryData model)
        {
            DietCategory entity = new DietCategory();
            EntityMapper.Map<DietCategoryData, DietCategory>(model, entity);
            bool x = DietCategoryRepository.Save(entity);
            return x.ToString();
        }

        public string SaveDayPartData(DayPartData model)
        {
            DayPart entity = new DayPart();
            EntityMapper.Map<DayPartData, DayPart>(model, entity);
            bool x = DayPartRepository.Save(entity);
            return x.ToString();
        }

        public string SaveServewareData(ServewareData model)
        {
            Serveware entity = new Serveware();
            EntityMapper.Map<ServewareData, Serveware>(model, entity);
            bool x = ServewareRepository.Save(entity);
            return x.ToString();
        }

        public string SaveAllergenData(AllergenData model)
        {
            Allergen entity = new Allergen();
            EntityMapper.Map<AllergenData, Allergen>(model, entity);
            bool x = AllergenRepository.Save(entity);
            return x.ToString();
        }

        public string SaveUOMData(UOMData model)
        {
            UOM entity = new UOM();
            EntityMapper.Map<UOMData, UOM>(model, entity);
            bool x = UOMRepository.Save(entity);
            return x.ToString();
        }

        public string SaveServingTemperatureData(ServingTemperatureData model)
        {
            ServingTemperature entity = new ServingTemperature();
            EntityMapper.Map<ServingTemperatureData, ServingTemperature>(model, entity);
            bool x = ServingTemperatureRepository.Save(entity);
            return x.ToString();
        }

        public string SaveSiteData(SiteMasterData model)
        {
            SiteMaster entity = new SiteMaster();
            EntityMapper.Map<SiteMasterData, SiteMaster>(model, entity);
            bool x = SiteMasterRepository.Save(entity);
            return x.ToString();
        }

        public string SaveSiteDataList(IList<SiteMasterData> model)
        {
            List<SiteMaster> entity = new List<SiteMaster>();
            EntityMapper.Map<IList<SiteMasterData>, IList<SiteMaster>>(model, entity);
            string x = SiteMasterRepository.SaveList(entity);
            return x;
        }

        public string SaveCafeData(CafeData model)
        {
            Cafe entity = new Cafe();
            if (model.ID == 0)
            {
                var code = CafeRepository.GetAll().OrderByDescending(z => z.ID).FirstOrDefault()?.CafeCode;
                if (code is null)
                    code = "CFC-00001";
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.CafeCode = code;
            }
            EntityMapper.Map<CafeData, Cafe>(model, entity);
            bool x = CafeRepository.Save(entity);
            return x.ToString();
        }

        public string SaveCafeDataList(IList<CafeData> model)
        {
            List<Cafe> entity = new List<Cafe>();
            EntityMapper.Map<IList<CafeData>, IList<Cafe>>(model, entity);
            string x = CafeRepository.SaveList(entity);
            return x;
        }
    }
}
