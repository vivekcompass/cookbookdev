﻿using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.Common;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using CookBook.Mapper;
using CookBook.DataAdapter.Factory.Contract;
using CookBook.DataAdapter.Factory.Repository;
using CookBook.Data.MasterData;
using System.Linq;
using CookBook.DataAdapter.Factory;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using CookBook.Data.Request;

namespace CookBook.Business.Impl
{
    [Export(typeof(ICostSimulatorManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CostSimulatorManager : ImplBase, ICostSimulatorManager
    {
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
       
        [Dependency]
        public ICostSimulatorRepository CostSimulatorRepository { get; set; }


        public IList<DishRecipeMapData> GetRecipeMappingDataList(string DishCode, RequestData requestData)
        {
            List<DishRecipeMapData> result = new List<DishRecipeMapData>();
            var entityList = CostSimulatorRepository.GetRecipeMappingDataList(DishCode,requestData).ToList();
            EntityMapper.Map<IList<procGetMappedDishRecipeList_Result>, IList<DishRecipeMapData>>(entityList, result);
            return result;
        }
        public IList<SimulatedMappedRecipeMOGListSiteData> GetSimulatedMappedRecipeMOGListSiteDataList(int RecipeID, int SiteId, RequestData requestData)
        {
            List<SimulatedMappedRecipeMOGListSiteData> result = new List<SimulatedMappedRecipeMOGListSiteData>();
            var entityList = CostSimulatorRepository.GetSimulatedMappedRecipeMOGListSiteDataList(RecipeID, SiteId,requestData).ToList();
            EntityMapper.Map<IList<procGetSimulatedMappedRecipeMOGListSite_Result>, IList<SimulatedMappedRecipeMOGListSiteData>>(entityList, result);
            return result;
        }
        public IList<SimulatedMappedRecipeMOGListSectorData> GetSimulatedMappedRecipeMOGListSectorDataList(int RecipeID, int SiteId, RequestData requestData)
        {
            List<SimulatedMappedRecipeMOGListSectorData> result = new List<SimulatedMappedRecipeMOGListSectorData>();
            var entityList = CostSimulatorRepository.GetSimulatedMappedRecipeMOGListSectorDataList(RecipeID, SiteId, requestData).ToList();
            EntityMapper.Map<IList<procGetSimulatedMappedRecipeMOGListSector_Result>, IList<SimulatedMappedRecipeMOGListSectorData>>(entityList, result);
            return result;
        }
        public IList<SimulatedMappedRecipeMOGListNationalData> GetSimulatedMappedRecipeMOGListNationalDataList(int RecipeID, RequestData requestData)
        {
            List<SimulatedMappedRecipeMOGListNationalData> result = new List<SimulatedMappedRecipeMOGListNationalData>();
            var entityList = CostSimulatorRepository.GetSimulatedMappedRecipeMOGListNationalDataList(RecipeID, requestData).ToList();
            EntityMapper.Map<IList<procGetSimulatedMappedRecipeMOGListNational_Result>, IList<SimulatedMappedRecipeMOGListNationalData>>(entityList, result);
            return result;
        }
    }
}
