﻿using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.Common;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using CookBook.Mapper;
using CookBook.DataAdapter.Factory.Contract;
using CookBook.DataAdapter.Factory.Repository;
using CookBook.Data.MasterData;
using System.Linq;
using CookBook.DataAdapter.Factory;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using CookBook.DataAdapter.Factory.Common;
using CookBook.Data.Request;

namespace CookBook.Business.Impl
{
    [Export(typeof(IMOGDataManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MOGDataManager : ImplBase, IMOGDataManager
    {
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IMOGRepository MOGRepository { get; set; }
        [Dependency]
        public INationalMOGRepository NationalMOGRepository { get; set; }

        public IList<MOGData> GetMOGDataList(RequestData requestData, string siteCode,int regionID)
        {
            List<MOGData> result = new List<MOGData>();
            if (!string.IsNullOrWhiteSpace(siteCode))
            {
                var entityList = MOGRepository.GetSiteMOGDataList(requestData, siteCode).ToList();
                EntityMapper.Map<IList<procGetSiteMOGList_Result>, IList<MOGData>>(entityList, result);
            }
            else if(regionID > 0)
            {
                var entityList = MOGRepository.GetRegionMOGDataList(requestData, regionID).ToList();
                EntityMapper.Map<IList<procGetRegionMOGList_Result>, IList<MOGData>>(entityList, result);
            }
            else
            {
                var entityList = MOGRepository.GetMOGDataList(requestData).ToList();
                EntityMapper.Map<IList<procGetMOGList_Result>, IList<MOGData>>(entityList, result);
            }
            return result;
        }

        public IList<MOGInheritedData> GetInheritedMOGDataList(RequestData requestData)
        {
            List<MOGInheritedData> result = new List<MOGInheritedData>();
            var entityList = MOGRepository.GetInheritedMOGDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetInheritMOGList_Result>, IList<MOGInheritedData>>(entityList, result);
            return result;
        }

        public MOGImpactedData GetMOGImpactedDataList(int mogID, int recipeID,bool isBaseRecipe, RequestData requestData)
        {
            MOGImpactedData result = new MOGImpactedData();

            List<MOGImpactedBaseRecipesData> mogImpactedBaseRecipesData = new List<MOGImpactedBaseRecipesData>();
            var entityList = MOGRepository.GetMOGImpactedBaseRecipiesDataList(mogID, recipeID,requestData).ToList();
            EntityMapper.Map<IList<procGetMOGImpactedBaseRecipes_Result>, IList<MOGImpactedBaseRecipesData>>(entityList, mogImpactedBaseRecipesData);
            result.MOGImpactedBaseRecipesData = mogImpactedBaseRecipesData;

            List<MOGImpactedFinalRecipesData> mogImpactedFinalRecipesData = new List<MOGImpactedFinalRecipesData>();
            var entityList1 = MOGRepository.GetMOGImpactedFinalRecipiesDataList(mogID, recipeID, requestData).ToList();
            EntityMapper.Map<IList<procGetMOGImpactedFinalRecipes_Result>, IList<MOGImpactedFinalRecipesData>>(entityList1, mogImpactedFinalRecipesData);
            result.MOGImpactedFinalRecipesData = mogImpactedFinalRecipesData;

            List<MOGImpactedDishesData> mogImpactedDishesData = new List<MOGImpactedDishesData>();
            var entityList2 = MOGRepository.GetMOGImpactedDishesDataList(mogID, recipeID, requestData).ToList();
            EntityMapper.Map<IList<procGetMOGImpactedDishes_Result>, IList<MOGImpactedDishesData>>(entityList2, mogImpactedDishesData);
            result.MOGImpactedDishesData = mogImpactedDishesData;

            List<MOGImpactedItemsData> mogImpactedItemsData = new List<MOGImpactedItemsData>();
            var entityList3 = MOGRepository.GetMOGImpactedItemsDataList(mogID, recipeID, requestData).ToList();
            EntityMapper.Map<IList<procGetMOGImpactedItems_Result>, IList<MOGImpactedItemsData>>(entityList3, mogImpactedItemsData);
            result.MOGImpactedItemsData = mogImpactedItemsData;

            List<MOGImpactedSiteData> mogImpactedSitesData = new List<MOGImpactedSiteData>();
            var entityList4 = MOGRepository.GetMOGImpactedSitesDataList(mogID, recipeID, requestData).ToList();
            EntityMapper.Map<IList<procGetMOGImpactedSites_Result>, IList<MOGImpactedSiteData>>(entityList4, mogImpactedSitesData);
            result.MOGImpactedSitesData = mogImpactedSitesData;

            List<MOGImpactedRegionData> mogImpactedRegionData = new List<MOGImpactedRegionData>();
            var entityList5 = MOGRepository.GetMOGImpactedRegionsDataList(mogID, recipeID, requestData).ToList();
            EntityMapper.Map<IList<procGetMOGImpactedRegion_Result>, IList<MOGImpactedRegionData>>(entityList5, mogImpactedRegionData);
            result.MOGImpactedRegionsData = mogImpactedRegionData;

            result.TotalImapactedBaseRecipes = mogImpactedBaseRecipesData.Count;
            result.TotalImapactedFinalRecipes = mogImpactedFinalRecipesData.Count;
            result.TotalImapactedDishes = mogImpactedDishesData.Count;
            result.TotalImapactedItems = mogImpactedItemsData.Count;
            result.TotalImapactedRegions = mogImpactedRegionData.Count;
            result.TotalImapactedSites = mogImpactedSitesData.Count;

          
            result.IsMOGImpacted = true;
            if (recipeID > 0)
            {
                if (isBaseRecipe)
                {
                    if (result.TotalImapactedBaseRecipes == 0 && result.TotalImapactedFinalRecipes == 0 &&  result.TotalImapactedDishes == 0 
                        && result.TotalImapactedItems == 0 && result.TotalImapactedSites == 0)
                    {
                        result.IsMOGImpacted = false;
                    }
                }
                else
                {
                    if (result.TotalImapactedDishes == 0 && result.TotalImapactedItems == 0 && result.TotalImapactedSites == 0)
                    {
                        result.IsMOGImpacted = false;
                    }
                }
               
            }
            else
            {
                if (result.TotalImapactedBaseRecipes == 0 && result.TotalImapactedDishes == 0 && result.TotalImapactedFinalRecipes == 0
                    && result.TotalImapactedItems == 0 && result.TotalImapactedSites == 0)
                {
                    result.IsMOGImpacted = false;
                }
            }

            return result;
        }

        public MOGAPLImpactedData GetMOGAPLImpactedDataList(int mogID,string aplList, int sectorID, RequestData requestData)
        {
            MOGAPLImpactedData result = new MOGAPLImpactedData();
           
            result.PreviousMappedAPL = "Salt, Catch, Sachet, 5000X1g";
            result.CurrentMappedAPL = " Sachet, 5000X1g";

            result.MOGAPLImpactedDishesData = new List<MOGAPLImpactedDishesData>();
            result.MOGAPLImpactedDishesRegionData = new List<MOGAPLImpactedDishesRegionData>();
            result.MOGAPLImpactedDishesSiteData = new List<MOGAPLImpactedDishesSiteData>();


            List<MOGAPLImpactedDishesData> mogImpactedDishesData = new List<MOGAPLImpactedDishesData>();
            var entityList1 = MOGRepository.procGetMOGAPLImpactedDishSector(mogID, aplList, sectorID, requestData).ToList();
            EntityMapper.Map<IList<procGetMOGAPLImpactedDisheSector_Result>, IList<MOGAPLImpactedDishesData>>(entityList1, mogImpactedDishesData);
            result.MOGAPLImpactedDishesData = mogImpactedDishesData;

            List<MOGAPLImpactedDishesRegionData> mogImpactedDishesRegionData = new List<MOGAPLImpactedDishesRegionData>();
            var entityList2 = MOGRepository.procGetMOGAPLImpactedDishRegion(mogID, aplList, sectorID, requestData).ToList();
            EntityMapper.Map<IList<procGetMOGAPLImpactedDishRegion_Result>, IList<MOGAPLImpactedDishesRegionData>>(entityList2, mogImpactedDishesRegionData);
            result.MOGAPLImpactedDishesRegionData = mogImpactedDishesRegionData;

            List<MOGAPLImpactedDishesSiteData> mogImpactedDishesSiteData = new List<MOGAPLImpactedDishesSiteData>();
            var entityList3 = MOGRepository.procGetMOGAPLImpactedDishSite(mogID, aplList, sectorID, requestData).ToList();
            EntityMapper.Map<IList<procGetMOGAPLImpactedDishSites_Result>, IList<MOGAPLImpactedDishesSiteData>>(entityList3, mogImpactedDishesSiteData);
            result.MOGAPLImpactedDishesSiteData = mogImpactedDishesSiteData;

            result.TotalImapactedDishes = mogImpactedDishesData.Count;
            result.TotalImapactedRegions = mogImpactedDishesRegionData.Count;
            result.TotalImapactedSites = mogImpactedDishesSiteData.Count;



            return result;
        }

        public IList<NationalMOGData> GetNationalMOGDataList( RequestData requestData)
        {
            List<NationalMOGData> result = new List<NationalMOGData>();
            var entityList = NationalMOGRepository.GetNationalMOGDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetNationalMOGList_Result>, IList<NationalMOGData>>(entityList, result);
            return result;
        }
        public IList<MOGNotMapped> GetNOTMappedMOGDataList(RequestData requestData)
        {
            List<MOGNotMapped> result = new List<MOGNotMapped>();
            var entityList = NationalMOGRepository.GetNOTMappedMOGDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetMogNotMappedList_Result>, IList<MOGNotMapped>>(entityList, result);
            return result;
        }

        public List<MOGAPLMAPPEDHISTORY> GetMOGAPLHISTORY(RequestData requestData, string username)
        {
            List<MOGAPLMAPPEDHISTORY> result = new List<MOGAPLMAPPEDHISTORY>();
            var entityList = NationalMOGRepository.GetMOGAPLHISTORY(requestData,username).ToList();
            EntityMapper.Map<List<ProcGetMOGBATCHDETAILS_Result>, List<MOGAPLMAPPEDHISTORY>>(entityList, result);
            return result;
        }
        public List<MOGAPLMAPPEDHISTORY> GetMOGAPLHISTORYDet(RequestData requestData, string username)
        {
            List<MOGAPLMAPPEDHISTORY> result = new List<MOGAPLMAPPEDHISTORY>();
            var entityList = NationalMOGRepository.GetMOGAPLHISTORYDet(requestData, username).ToList();
            EntityMapper.Map<List<procshowMOGAPLUPLOADDETAILS_Result>, List<MOGAPLMAPPEDHISTORY>>(entityList, result);
            return result;
        }


        public List<MOGAPLMAPPEDHISTORY> GetshowGetALLHISTORYBATCHWISE(RequestData requestData, string username,string batchno)
        {
            List<MOGAPLMAPPEDHISTORY> result = new List<MOGAPLMAPPEDHISTORY>();
            var entityList = NationalMOGRepository.GetshowGetALLHISTORYBATCHWISE(requestData, username,batchno).ToList();
            EntityMapper.Map<List<procshowGetALLHISTORYBATCHWISE_Result>, List<MOGAPLMAPPEDHISTORY>>(entityList, result);
            return result;
        }

        public List<MOGAPLMAPPEDHISTORY> GetALLHISTORYDetails(RequestData requestData, string username)
        {
            List<MOGAPLMAPPEDHISTORY> result = new List<MOGAPLMAPPEDHISTORY>();
            var entityList = NationalMOGRepository.GetALLHISTORYDetails(requestData, username).ToList();
            EntityMapper.Map<List<procshowGetALLHISTORY_Result>, List<MOGAPLMAPPEDHISTORY>>(entityList, result);
            return result;
        }
        //add by gaurav saini
        public string UpdateMOGLIST(RequestData requestData, string xml,string username)
        {
            List<MOGNotMapped> result = new List<MOGNotMapped>();
            string entityList = NationalMOGRepository.UpdateMOGLIST(requestData,xml,username).ToString();
            //EntityMapper.Map<IList<procGetMogNotMappedList_Result>, IList<MOGNotMapped>>(entityList, result);
            //return result;
            return entityList;
        }


        public IList<APLMasterData> GetAPLMasterDataListCommon(string articleNumber, string mogCode, RequestData requestData)
        {
            List<APLMasterData> result = new List<APLMasterData>();
            var entityList = NationalMOGRepository.GetAPLMasterDataListCommon(articleNumber,mogCode, requestData).ToList();
            EntityMapper.Map<IList<procGetAPLMasterDataCommon_Result>, IList<APLMasterData>>(entityList, result);
            return result;
        }

        public string SaveMOGData(MOGData model, RequestData requestData)
        {
            string sectors = "10,30,80,100";
            if (model.IsMOGNutrientMappingData)
            {
                var isExist = NationalMOGRepository.GetAll(requestData.SessionSectorName).Where(m => m.MOGCode == model.MOGCode).FirstOrDefault();
                if (isExist != null && isExist.ID > 0)
                {
                    isExist.NutritionData = model.NutritionData;
                    isExist.NutrientMOGQuantity = model.NutrientMOGQuantity;
                    NationalMOGRepository.Save(isExist, requestData.SessionSectorName);
                }
                return "true";
            }
            else if (model.IsNationalMOGData)
            {
                NationalMOG entity = new NationalMOG();
                if (model.ID == 0)
                {
                    var code = NationalMOGRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.MOGCode).FirstOrDefault()?.MOGCode;
                    if (code is null)
                        code = "MOG-00001";
                    else
                    {
                        var lcode = code.Substring(0, 3);
                        var rcode = code.Substring(4, 5);
                        code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                    }
                    model.MOGCode = code;
                }
                EntityMapper.Map<MOGData, NationalMOG>(model, entity);
                bool x = NationalMOGRepository.Save(entity, requestData.SessionSectorName);
                if (x)
                {
                    NationalMOGRepository.SaveNationalMOGToSectors(entity, sectors, requestData);
                }
                return x.ToString();
            }
            else
            {
                var isExist = MOGRepository.GetAll(requestData.SessionSectorName).Where(m => m.MOGCode == model.MOGCode).FirstOrDefault();
                if(isExist != null && isExist.ID > 0)
                {
                    isExist.UOMCode = model.UOMCode;
                    isExist.Alias = model.Alias;
                    MOGRepository.Save(isExist, requestData.SessionSectorName);
                }
                return "true";
            }
        }

        public string SaveMOGDataList(IList<MOGData> model, RequestData requestData)
        {
            List<NationalMOG> entity = new List<NationalMOG>();
            EntityMapper.Map<IList<MOGData>, IList<NationalMOG>>(model, entity);
            string x = NationalMOGRepository.SaveList(entity,requestData);
            return x;
        }

        public IList<GetExpiryProductData> GetExpiryProductDataList(RequestData requestData)
        {
            List<GetExpiryProductData> result = new List<GetExpiryProductData>();
            var entityList = MOGRepository.GetExpiryProductDataList(requestData).ToList();
            EntityMapper.Map<IList<ProcGetExpiryProductData_Result>, IList<GetExpiryProductData>>(entityList, result);
            return result;
        }

        public string SaveExpiryProductData(GetExpiryProductData model, RequestData requestData)
        {
            string x = MOGRepository.SaveExpiryProductData(model, requestData);
            return x;
        }
        //Krish
        public IList<GetExpiryProductData> GetExpiryProductDataOnlyExpiryNameList(RequestData requestData)
        {
            List<GetExpiryProductData> result = new List<GetExpiryProductData>();
            var entityList = MOGRepository.GetExpiryProductDataOnlyExpiryNameList(requestData).ToList();
            EntityMapper.Map<IList<ProcGetExpiryProductData_Result>, IList<GetExpiryProductData>>(entityList, result);
            return result;
        }
    }
}
