﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using HSEQ.Business.Base.Impl;
using HSEQ.Business.Contract;
using HSEQ.Data.Course;
using HSEQ.DataAdapter.Factory;
using HSEQ.DataAdapter.Factory.Contract;

namespace HSEQ.Business.Impl
{
    [Export(typeof(ICourse))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CourseManager : ImplBase, ICourse
    {
        [Dependency]
        public ICourseRepository CourseRepository { get; set; }

        [Dependency]
        public ICourseCategoryRepository CourseCategoryRepository { get; set; }

        [Dependency]
        public ICourseMetadataRepository CourseMetadataRepository { get; set; }

        public IList<CourseData> GetAllCourses()
        {
            IList<CourseData> courses = new List<CourseData>();
            courses = MapEngine.Map<IList<CourseData>>(CourseRepository.GetAll());
            return courses;
        }

        public bool SaveCourse(CourseData course)
        {
            bool isSuccess = false;
            CourseMaster courseEntity = new CourseMaster();
            courseEntity = MapEngine.Map<CourseMaster>(course);
            isSuccess = CourseRepository.Save(courseEntity);
            return isSuccess;
        }

        public CourseData GetCourse(int id)
        {
            CourseData course = new CourseData();
            var entity = CourseRepository.Find(id);
            course = MapEngine.Map<CourseData>(entity);
            return course;
        }

        public CourseData GetCourseByCode(string code)
        {
            CourseData course = new CourseData();
            var entity = CourseRepository.Find(k => k.CourseCode == code).FirstOrDefault();
            course = MapEngine.Map<CourseData>(entity);
            return course;
        }

        public bool DeleteCourse(int courseId)
        {
            bool isSuccess = false;
            CourseMaster courseEntity = new CourseMaster() { CourseID = courseId};
            isSuccess = CourseRepository.Delete(courseEntity);
            return isSuccess;
        }

        public IList<CourseCategoryMasterData> GetCourseCategories()
        {
            IList<CourseCategoryMasterData> categories = new List<CourseCategoryMasterData>();
            categories = MapEngine.Map<IList<CourseCategoryMasterData>>(CourseCategoryRepository.GetAll());
            return categories;
        }

        public IList<CourseKeywordMasterData> GetCourseKeywords()
        {
            IList<CourseKeywordMasterData> keywords = new List<CourseKeywordMasterData>();
            keywords = MapEngine.Map<IList<CourseKeywordMasterData>>(CourseMetadataRepository.GetCourseKeywords());
            return keywords;
        }

        public Tuple<IList<CourseData>, int> GetPagedCourseData(int pageNum, int pageSize = 0, string orderby = null, string orderExpression = null)
        {
            int recordCount = 0;
            var data = new List<CourseData>();
            TransactionManager.PerformWithNoLock(() =>
            {
                var result = CourseRepository.GetPagedCourseData(pageNum, pageSize, orderby, orderExpression);
                data = MapEngine.Map(result.Item1, data);
                recordCount = result.Item2;
            });
            return new Tuple<IList<CourseData>, int>(data, recordCount);
        }
    }
}
