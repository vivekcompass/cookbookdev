﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using HSEQ.Business.Base.Impl;
using HSEQ.Business.Contract;
using HSEQ.DataAdapter.Factory.Contract;

namespace HSEQ.Business.Impl
{
    [Export(typeof(IUserProduct))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserProductManager : ImplBase, IUserProduct
    {
        [Dependency]
        public IUserProductRepository UserProductRepository { get; set; }
    
    }
}
