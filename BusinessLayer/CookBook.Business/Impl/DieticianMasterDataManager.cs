﻿using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Encryption.Interfaces;
using CookBook.Mapper;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace CookBook.Business.Impl
{
    [Export(typeof(IDieticianMasterDataManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DieticianMasterDataManager : ImplBase, IDieticianMasterDataManager
    {
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IDieticianMasterRepository DieticianMasterRepository { get; set; }

        [Dependency]
        public IAesCryptoEngine AesCryptoEngine { get; set; }

        public List<DieticianMasterData> GetDieticianMaster(RequestData requestData)
        {
            List<DieticianMasterData> result = new List<DieticianMasterData>();
            var entityList = DieticianMasterRepository.GetDieticianMaster(requestData).ToList();
            EntityMapper.Map<List<procGetDieticianMaster_Result>, List<DieticianMasterData>>(entityList, result);

            return result;
        }

        public List<DieticianMasterHISTORY> GetDieticianMasterHISTORY(RequestData requestData, string username)
        {
            List<DieticianMasterHISTORY> result = new List<DieticianMasterHISTORY>();
            var entityList = DieticianMasterRepository.GetDieticianMasterHISTORY(requestData, username).ToList();
            EntityMapper.Map<List<procGetDieticianMasterBATCHHEADER_Result>, List<DieticianMasterHISTORY>>(entityList, result);
            return result;
        }
        public List<DieticianMasterHISTORY> GetDieticianMasterHISTORYDet(RequestData requestData, string username)
        {
            List<DieticianMasterHISTORY> result = new List<DieticianMasterHISTORY>();
            var entityList = DieticianMasterRepository.GetDieticianMasterHISTORYDet(requestData, username).ToList();
            EntityMapper.Map<List<procGetDieticianMasterBATCHDETAILS_Result>, List<DieticianMasterHISTORY>>(entityList, result);
            return result;
        }
        public List<DieticianMasterHistoryBatchWise> GetshowGetALLDieticianMasterHISTORYBATCHWISE(RequestData requestData,  string batchno)
        {
            List<DieticianMasterHistoryBatchWise> result = new List<DieticianMasterHistoryBatchWise>();
            var entityList = DieticianMasterRepository.GetshowGetALLDieticianMasterHISTORYBATCHWISE(requestData, batchno).ToList();
            EntityMapper.Map<List<procshowGetALLDieticianMasterHISTORYBATCHWISE_Result>, List<DieticianMasterHistoryBatchWise>>(entityList, result);
            return result;
        }
        public string AddUpdateDieticianMasterList(RequestData request, string xml, string username)
        {

            string entityList = DieticianMasterRepository.AddUpdateDieticianMasterList(request, xml, username).ToString();
            return entityList;
        }
        public string SaveDieticianMasterStatus(RequestData request, int id, int username)
        {
            string entityList = DieticianMasterRepository.SaveDieticianMasterStatus(request, id, username).ToString();
            return entityList;
        }
        public List<DieticianMasterHistoryAllData> GetALLHISTORYDetailsDieticianMaster(RequestData request)
        {
            List<DieticianMasterHistoryAllData> result = new List<DieticianMasterHistoryAllData>();
            var entityList = DieticianMasterRepository.GetALLHISTORYDetailsDieticianMaster(request).ToList();
            EntityMapper.Map<List<procshowGetALLHISTORYDieticianMaster_Result>, List<DieticianMasterHistoryAllData>>(entityList, result);
            return result;

        }

        #region Hospital Scheduler

        public string SaveHISPatientProfileMaster(RequestData request, List<HISPatientProfileData> hisPatientProfileData)
        {
            if (request.SiteCode == "144O")
            {
                if (hisPatientProfileData != null && hisPatientProfileData.Count > 0)
                {
                    hisPatientProfileData = hisPatientProfileData
                   .GroupBy(x => x.PatientID)
                   .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisPatientProfileData.ForEach(m =>
                    {
                        if (!string.IsNullOrEmpty(m.PatientID) && !string.IsNullOrEmpty(m.PatientName) && !string.IsNullOrEmpty(m.DateOfBirth)
                        && !string.IsNullOrEmpty(m.Bed))
                        {
                            m.PatientID = AesCryptoEngine.EncryptData(m.PatientID);
                            m.PatientName = AesCryptoEngine.EncryptData(m.PatientName);
                            m.HISBedCode = m.BedCode;
                            m.Bed = AesCryptoEngine.EncryptData(m.Bed);
                            m.DateOfBirth = AesCryptoEngine.EncryptData(m.DateOfBirth);
                        }
                    });
                }
                return DieticianMasterRepository.SaveHISPatientProfileMaster(request, hisPatientProfileData).ToString();
            }
            else if (request.SiteCode == "150F")
            {
                if (hisPatientProfileData != null && hisPatientProfileData.Count > 0)
                {
                    hisPatientProfileData = hisPatientProfileData
                   .GroupBy(x => x.PatientID)
                   .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisPatientProfileData.ForEach(m =>
                    {
                        if (!string.IsNullOrEmpty(m.PatientID) && !string.IsNullOrEmpty(m.PatientName) && !string.IsNullOrEmpty(m.DateOfBirth)
                        && !string.IsNullOrEmpty(m.Bed))
                        {
                            m.PatientID = AesCryptoEngine.EncryptData(m.PatientID);
                            m.PatientName = AesCryptoEngine.EncryptData(m.PatientName);
                            m.HISBedCode = m.BedCode;
                            m.Bed = AesCryptoEngine.EncryptData(m.Bed);
                            m.DateOfBirth = AesCryptoEngine.EncryptData(m.DateOfBirth);
                        }
                    });
                }
                return DieticianMasterRepository.SaveHISPatientProfileMaster_HN(request, hisPatientProfileData).ToString();
            }
            else if (request.SiteCode == "144S")
            {
                if (hisPatientProfileData != null && hisPatientProfileData.Count > 0)
                {
                    hisPatientProfileData = hisPatientProfileData
                   .GroupBy(x => x.PatientID)
                   .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisPatientProfileData.ForEach(m =>
                    {
                        if (!string.IsNullOrEmpty(m.PatientID) && !string.IsNullOrEmpty(m.PatientName) && !string.IsNullOrEmpty(m.DateOfBirth)
                        && !string.IsNullOrEmpty(m.Bed))
                        {
                            m.PatientID = AesCryptoEngine.EncryptData(m.PatientID);
                            m.PatientName = AesCryptoEngine.EncryptData(m.PatientName);
                            m.HISBedCode = m.BedCode;
                            m.Bed = AesCryptoEngine.EncryptData(m.Bed);
                            m.DateOfBirth = AesCryptoEngine.EncryptData(m.DateOfBirth);
                        }
                    });
                }
                return DieticianMasterRepository.SaveHISPatientProfileMaster_R10(request, hisPatientProfileData).ToString();
            }
            else if (request.SiteCode == "144N")
            {
                if (hisPatientProfileData != null && hisPatientProfileData.Count > 0)
                {
                    hisPatientProfileData = hisPatientProfileData
                   .GroupBy(x => x.PatientID)
                   .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisPatientProfileData.ForEach(m =>
                    {
                        if (!string.IsNullOrEmpty(m.PatientID) && !string.IsNullOrEmpty(m.PatientName) && !string.IsNullOrEmpty(m.DateOfBirth)
                        && !string.IsNullOrEmpty(m.Bed))
                        {
                            m.PatientID = AesCryptoEngine.EncryptData(m.PatientID);
                            m.PatientName = AesCryptoEngine.EncryptData(m.PatientName);
                            m.HISBedCode = m.BedCode;
                            m.Bed = AesCryptoEngine.EncryptData(m.Bed);
                            m.DateOfBirth = AesCryptoEngine.EncryptData(m.DateOfBirth);
                        }
                    });
                }
                return DieticianMasterRepository.SaveHISPatientProfileMaster_40HY(request, hisPatientProfileData).ToString();
            }
            else if (request.SiteCode == "144T")
            {
                if (hisPatientProfileData != null && hisPatientProfileData.Count > 0)
                {
                    hisPatientProfileData = hisPatientProfileData
                   .GroupBy(x => x.PatientID)
                   .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisPatientProfileData.ForEach(m =>
                    {
                        if (!string.IsNullOrEmpty(m.PatientID) && !string.IsNullOrEmpty(m.PatientName) && !string.IsNullOrEmpty(m.DateOfBirth)
                        && !string.IsNullOrEmpty(m.Bed))
                        {
                            m.PatientID = AesCryptoEngine.EncryptData(m.PatientID);
                            m.PatientName = AesCryptoEngine.EncryptData(m.PatientName);
                            m.HISBedCode = m.BedCode;
                            m.Bed = AesCryptoEngine.EncryptData(m.Bed);
                            m.DateOfBirth = AesCryptoEngine.EncryptData(m.DateOfBirth);
                        }
                    });
                }
                return DieticianMasterRepository.SaveHISPatientProfileMaster_40VI(request, hisPatientProfileData).ToString();
            }
            else if (request.SiteCode == "144P")
            {
                if (hisPatientProfileData != null && hisPatientProfileData.Count > 0)
                {
                    hisPatientProfileData = hisPatientProfileData
                   .GroupBy(x => x.PatientID)
                   .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisPatientProfileData.ForEach(m =>
                    {
                        if (!string.IsNullOrEmpty(m.PatientID) && !string.IsNullOrEmpty(m.PatientName) && !string.IsNullOrEmpty(m.DateOfBirth)
                        && !string.IsNullOrEmpty(m.Bed))
                        {
                            m.PatientID = AesCryptoEngine.EncryptData(m.PatientID);
                            m.PatientName = AesCryptoEngine.EncryptData(m.PatientName);
                            m.HISBedCode = m.BedCode;
                            m.Bed = AesCryptoEngine.EncryptData(m.Bed);
                            m.DateOfBirth = AesCryptoEngine.EncryptData(m.DateOfBirth);
                        }
                    });
                }
                return DieticianMasterRepository.SaveHISPatientProfileMaster_40DN(request, hisPatientProfileData).ToString();
            }
            return "True";
        }

        public string SaveHISUserData(RequestData request, List<HISUserData> hisUserData)
        {
            if (request.SiteCode == "144O")
            {
                if (hisUserData != null && hisUserData.Count > 0)
                {
                    hisUserData = hisUserData
                      .GroupBy(x => x.EmpCode)
                      .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisUserData.ForEach(m =>
                    {
                        m.EmpName = AesCryptoEngine.EncryptData(m.EmpName);
                        m.MobileNo = AesCryptoEngine.EncryptData(m.MobileNo);
                    });
                }
                return DieticianMasterRepository.SaveHISUserData(request, hisUserData).ToString();
            }
            else if (request.SiteCode == "150F")
            {
                if (hisUserData != null && hisUserData.Count > 0)
                {
                    hisUserData = hisUserData
                      .GroupBy(x => x.EmpCode)
                      .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisUserData.ForEach(m =>
                    {
                        m.EmpName = AesCryptoEngine.EncryptData(m.EmpName);
                        m.MobileNo = AesCryptoEngine.EncryptData(m.MobileNo);
                    });
                }
                return DieticianMasterRepository.SaveHISUserData_HN(request, hisUserData).ToString();
            }
            else if (request.SiteCode == "144S")
            {
                if (hisUserData != null && hisUserData.Count > 0)
                {
                    hisUserData = hisUserData
                      .GroupBy(x => x.EmpCode)
                      .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisUserData.ForEach(m =>
                    {
                        m.EmpName = AesCryptoEngine.EncryptData(m.EmpName);
                        m.MobileNo = AesCryptoEngine.EncryptData(m.MobileNo);
                    });
                }
                return DieticianMasterRepository.SaveHISUserData_R10(request, hisUserData).ToString();
            }
            else if (request.SiteCode == "144N")
            {
                if (hisUserData != null && hisUserData.Count > 0)
                {
                    hisUserData = hisUserData
                      .GroupBy(x => x.EmpCode)
                      .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisUserData.ForEach(m =>
                    {
                        m.EmpName = AesCryptoEngine.EncryptData(m.EmpName);
                        m.MobileNo = AesCryptoEngine.EncryptData(m.MobileNo);
                    });
                }
                return DieticianMasterRepository.SaveHISUserData_40HY(request, hisUserData).ToString();
            }

            else if (request.SiteCode == "144T")
            {
                if (hisUserData != null && hisUserData.Count > 0)
                {
                    hisUserData = hisUserData
                      .GroupBy(x => x.EmpCode)
                      .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisUserData.ForEach(m =>
                    {
                        m.EmpName = AesCryptoEngine.EncryptData(m.EmpName);
                        m.MobileNo = AesCryptoEngine.EncryptData(m.MobileNo);
                    });
                }
                return DieticianMasterRepository.SaveHISUserData_40VI(request, hisUserData).ToString();
            }
            else if (request.SiteCode == "144P")
            {
                if (hisUserData != null && hisUserData.Count > 0)
                {
                    hisUserData = hisUserData
                      .GroupBy(x => x.EmpCode)
                      .Select(grp => grp.OrderByDescending(x => x.ModifiedDate).First()).ToList();

                    hisUserData.ForEach(m =>
                    {
                        m.EmpName = AesCryptoEngine.EncryptData(m.EmpName);
                        m.MobileNo = AesCryptoEngine.EncryptData(m.MobileNo);
                    });
                }
                return DieticianMasterRepository.SaveHISUserData_40DN(request, hisUserData).ToString();
            }
            return "True";
        }
        public string SaveHISBedMasterData(RequestData request, List<HISBedMasterData> hisBedMasterData)
        {
            if (request.SiteCode == "144O")
            {
                if (hisBedMasterData != null && hisBedMasterData.Count > 0)
                {
                    hisBedMasterData = hisBedMasterData.GroupBy(x => x.BedCode)
                        .Select(grp => grp.OrderByDescending(x => x.BedCode).First()).ToList();

                }
                return DieticianMasterRepository.SaveHISBedMasterData(request, hisBedMasterData).ToString();
            }
            else if (request.SiteCode == "150F")
            {
                if (hisBedMasterData != null && hisBedMasterData.Count > 0)
                {
                    hisBedMasterData = hisBedMasterData.GroupBy(x => x.BedCode)
                        .Select(grp => grp.OrderByDescending(x => x.BedCode).First()).ToList();

                }
                return DieticianMasterRepository.SaveHISBedMasterData_HN(request, hisBedMasterData).ToString();
            }
            else if (request.SiteCode == "144S")
            {
                if (hisBedMasterData != null && hisBedMasterData.Count > 0)
                {
                    hisBedMasterData = hisBedMasterData.GroupBy(x => x.BedCode)
                        .Select(grp => grp.OrderByDescending(x => x.BedCode).First()).ToList();

                }
                return DieticianMasterRepository.SaveHISBedMasterData_R10(request, hisBedMasterData).ToString();
            }
            else if (request.SiteCode == "144N")
            {
                if (hisBedMasterData != null && hisBedMasterData.Count > 0)
                {
                    hisBedMasterData = hisBedMasterData.GroupBy(x => x.BedCode)
                        .Select(grp => grp.OrderByDescending(x => x.BedCode).First()).ToList();

                }
                return DieticianMasterRepository.SaveHISBedMasterData_40HY(request, hisBedMasterData).ToString();
            }
            else if (request.SiteCode == "144T")
            {
                if (hisBedMasterData != null && hisBedMasterData.Count > 0)
                {
                    hisBedMasterData = hisBedMasterData.GroupBy(x => x.BedCode)
                        .Select(grp => grp.OrderByDescending(x => x.BedCode).First()).ToList();

                }
                return DieticianMasterRepository.SaveHISBedMasterData_40VI(request, hisBedMasterData).ToString();
            }
            else if (request.SiteCode == "144P")
            {
                if (hisBedMasterData != null && hisBedMasterData.Count > 0)
                {
                    hisBedMasterData = hisBedMasterData.GroupBy(x => x.BedCode)
                        .Select(grp => grp.OrderByDescending(x => x.BedCode).First()).ToList();

                }
                return DieticianMasterRepository.SaveHISBedMasterData_40DN(request, hisBedMasterData).ToString();
            }
            return "True";

        }

        #endregion
    }
}