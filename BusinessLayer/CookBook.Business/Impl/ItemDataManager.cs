﻿using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.Common;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using CookBook.Mapper;
using CookBook.DataAdapter.Factory.Contract;
using CookBook.DataAdapter.Factory.Repository;
using CookBook.Data.MasterData;
using System.Linq;
using CookBook.DataAdapter.Factory;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Repository.Transaction;
using CookBook.DataAdapter.Factory.Repository.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.UserData;
using CookBook.DataAdapter.Base.DataEnums;
using CookBook.Aspects.Utils;
using CookBook.Data.Request;

namespace CookBook.Business.Impl
{
    [Export(typeof(IItemDataManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ItemDataManager : ImplBase, IItemDataManager
    {
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IItemRepository ItemRepository { get; set; }
        public IItemDishMappingRepository ItemDishMappingRepository { get; set; }
        public IRegionItemInheritanceMappingRepository RegionItemInheritanceMappingRepository { get; set; }
        [Dependency]
        public ISiteItemInheritanceMappingRepository SiteItemInheritanceMappingRepository { get; set; }

        public IItemDishCategoryMappingRepository ItemDishCategoryMappingRepository { get; set; }
        public IRegionDishCategoryMappingRepository RegionDishCategoryMappingRepository { get; set; }
        public ISiteDishCategoryMappingRepository SiteDishCategoryMappingRepository { get; set; }

        public IRegionItemDishMappingRepository RegionItemDishMappingRepository { get; set; }
        public ISiteItemDishMappingRepository SiteItemDishMappingRepository { get; set; }
        [Dependency]
        public ISiteItemDayPartMappingRepository SiteItemDayPartMappingRepository { get; set; }
        public ISiteDayPartMappingRepository SiteDayPartMappingRepository { get; set; }
        public IFoodProgramDayPartMappingRepository FoodProgramDayPartMappingRepository { get; set; }


        public IList<ItemData> GetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, string sectorName, RequestData requestData)
        {
            if (!string.IsNullOrWhiteSpace(sectorName))
                GenericData.DatabaseName = sectorName;
            List<ItemData> result = new List<ItemData>();
            var entityList = ItemRepository.GetItemDataList(itemCode, foodProgramID, dietCategoryID, requestData).ToList();
            EntityMapper.Map<IList<procGetItemList_Result>, IList<ItemData>>(entityList, result);
            return result;
        }

        public IList<NationalItemData> NationalGetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, string sectorName, RequestData requestData)
        {
            if (!string.IsNullOrWhiteSpace(sectorName))
                GenericData.DatabaseName = sectorName;
            List<NationalItemData> result = new List<NationalItemData>();
            var entityList = ItemRepository.NationalGetItemDataList(itemCode, foodProgramID, dietCategoryID, requestData).ToList();
            EntityMapper.Map<IList<procNationalGetItemList_Result>, IList<NationalItemData>>(entityList, result);
            return result;
        }

        public IList<PriceHistoryData> GetSiteItemPriceHistory(string siteCode, string itemCode, RequestData requestData)
        {

            List<PriceHistoryData> result = new List<PriceHistoryData>();
            var entityList = ItemRepository.GetSiteItemPriceHistory(siteCode, itemCode, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteItemPriceHistory_Result>, IList<PriceHistoryData>>(entityList, result);
            return result;
        }


        public IList<UnitPriceData> GetSiteUnitPrice(string siteCode, RequestData requestData)
        {

            List<UnitPriceData> result = new List<UnitPriceData>();
            var entityList = ItemRepository.GetSiteUnitPrice(siteCode, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteUnitPrice_Result>, IList<UnitPriceData>>(entityList, result);
            return result;
        }

        public IList<SiteDayPartMappingData> GetSiteDayPartMapping(string siteCode, RequestData requestData)
        {

            List<SiteDayPartMappingData> result = new List<SiteDayPartMappingData>();
            var entityList = ItemRepository.GetSiteDayPartMapping(siteCode, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteDayPartMapping_Result>, IList<SiteDayPartMappingData>>(entityList, result);
            return result;
        }

        public IList<ItemData> GetDishImpactedItemList(string dishCode, RequestData requestData)
        {
            List<ItemData> result = new List<ItemData>();
            var entityList = ItemRepository.GetDishImpactedItemList(dishCode, requestData).ToList();
            EntityMapper.Map<IList<procGetDishImpactedItemList_Result>, IList<ItemData>>(entityList, result);
            return result;
        }

        public IList<ItemDishMappingData> GetItemDishMappingDataList(string itemCode, RequestData requestData)
        {
            List<ItemDishMappingData> result = new List<ItemDishMappingData>();
            ItemDishMappingRepository = new ItemDishMappingRepository();
            var entityList = ItemDishMappingRepository.GetItemDishMappingDataList(itemCode, requestData).ToList();
            EntityMapper.Map<IList<ItemDishMapping>, IList<ItemDishMappingData>>(entityList, result);
            return result;
        }
        public IList<string> GetUniqueMappedItemCodesFromItem(RequestData requestData)
        {
            ItemDishCategoryMappingRepository = new ItemDishCategoryMappingRepository();
            var entityList = ItemDishCategoryMappingRepository.GetUniqueMappedItemCodesFromItem(requestData);
            return entityList.ToList();
        }
        public IList<string> GetUniqueMappedItemCodesFromRegion(string RegionID, RequestData requestData)
        {

            RegionDishCategoryMappingRepository = new RegionDishCategoryMappingRepository();
            var entityList = RegionDishCategoryMappingRepository.GetUniqueMappedItemCodesFromRegion(RegionID, requestData);
            return entityList.ToList();
        }
        public IList<RegionInheritedData> GetRegionInheritedData(string RegionID, string SectorID, RequestData requestData)
        {
            List<RegionInheritedData> result = new List<RegionInheritedData>();
            var entityList = ItemRepository.GetRegionInheritedData(RegionID, SectorID, requestData).ToList();
            EntityMapper.Map<IList<procGetRegionInheritedData_Result>, IList<RegionInheritedData>>(entityList, result);
            return result;
        }

        public IList<RegionItemDishMappingData> GetRegionItemDishMappingDataList(string itemCode, string regionID, RequestData requestData)
        {
            List<RegionItemDishMappingData> result = new List<RegionItemDishMappingData>();
            RegionItemDishMappingRepository = new RegionItemDishMappingRepository();
            var entityList = RegionItemDishMappingRepository.GetRegionItemDishMappingDataList(itemCode, regionID, requestData).ToList();
            EntityMapper.Map<IList<RegionItemDishMapping>, IList<RegionItemDishMappingData>>(entityList, result);
            return result;
        }
        public IList<RegionItemDishMappingData> GetMasterRegionItemDishMappingDataList(string regionID, RequestData requestData)
        {
            List<RegionItemDishMappingData> result = new List<RegionItemDishMappingData>();
            RegionItemDishMappingRepository = new RegionItemDishMappingRepository();
            var entityList = RegionItemDishMappingRepository.GetMasterRegionItemDishMappingDataList(regionID, requestData).ToList();
            EntityMapper.Map<IList<RegionItemDishMapping>, IList<RegionItemDishMappingData>>(entityList, result);
            return result;
        }


        public IList<SiteMasterData> GetSiteMasterList(RequestData requestData)
        {
            List<SiteMasterData> result = new List<SiteMasterData>();
            var siteMasterRepository = new SiteMasterRepository();
            var entityList = siteMasterRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<SiteMaster>, IList<SiteMasterData>>(entityList, result);
            return result;
        }


        public string SaveItemData(ItemData model, RequestData requestData)
        {
            Item entity = new Item();
            if (requestData.SessionSectorNumber != "30" && requestData.SessionSectorNumber != "80")
            {
                if (model.ID == 0)
                {
                    var code = ItemRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ItemCode).FirstOrDefault()?.ItemCode;
                    if (code is null)
                        code = "ITM-00001";
                    else
                    {
                        var lcode = code.Substring(0, 3);
                        var rcode = code.Substring(4, 5);
                        code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                    }
                    model.ItemCode = code;
                }
            }
            else if (requestData.SessionSectorNumber == "80")
            {
                model.MenuType1Code = "MT1-002";
                if (!string.IsNullOrEmpty(model.MenuType2Code))
                {
                    model.MenuType1Code = "MT1-001";
                    if (model.ID == 0)
                    {
                        var code = ItemRepository.GetAll(requestData.SessionSectorName).Where(m => m.ItemCode.StartsWith("78")).OrderByDescending(z => z.ItemCode).FirstOrDefault()?.ItemCode;
                        if (code is null)
                            code = "70000";
                        else
                        {
                            code = (Convert.ToInt32(code) + 1).ToString();
                        }
                        model.ItemCode = code;
                    }
                }
            }
            EntityMapper.Map<ItemData, Item>(model, entity);

            var dup = ItemRepository.Find(y => y.Name.Trim().ToLower() == model.ItemName.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
            if (dup != null && model.ID == 0)
                return "Duplicate";

            bool x = ItemRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveItemDataList(IList<ItemData> model, RequestData requestData)
        {
            List<Item> entity = new List<Item>();
            EntityMapper.Map<IList<ItemData>, IList<Item>>(model, entity);
            string x = ItemRepository.SaveList(entity, requestData);
            return x;
        }
        public string SaveItemDishMappingDataList(IList<ItemDishMappingData> model, RequestData requestData)
        {
            List<ItemDishMapping> entity = new List<ItemDishMapping>();
            ItemDishMappingRepository = new ItemDishMappingRepository();
            EntityMapper.Map<IList<ItemDishMappingData>, IList<ItemDishMapping>>(model, entity);
            string x = ItemDishMappingRepository.SaveList(entity, model[0].Status, requestData);
            return x;
        }


        public string SaveRegionItemDishMappingDataList(IList<RegionItemDishMappingData> model, RequestData requestData)
        {
            List<RegionItemDishMapping> entity = new List<RegionItemDishMapping>();
            RegionItemDishMappingRepository = new RegionItemDishMappingRepository();
            EntityMapper.Map<IList<RegionItemDishMappingData>, IList<RegionItemDishMapping>>(model, entity);
            string x = RegionItemDishMappingRepository.SaveRegionItemDishMappingList(entity, model[0].Status, requestData);
            return x;

        }


        public string mydishcateinactiveupdate(IList<RegionDishCategoryMappingData> model, RequestData requestData)
        {
            List<RegionDishCategoryMapping> entity = new List<RegionDishCategoryMapping>();
            RegionItemDishMappingRepository = new RegionItemDishMappingRepository();
            EntityMapper.Map<IList<RegionDishCategoryMappingData>, IList<RegionDishCategoryMapping>>(model, entity);
            string x = RegionItemDishMappingRepository.mydishcateinactiveupdate(entity, requestData);
            return x;

        }

        public IList<SiteItemDishMappingData> GetMasterSiteItemDishMappingDataList(string siteCode, RequestData requestData)
        {
            List<SiteItemDishMappingData> result = new List<SiteItemDishMappingData>();
            SiteItemDishMappingRepository = new SiteItemDishMappingRepository();
            var entityList = SiteItemDishMappingRepository.GetMasterSiteItemDishMappingDataList(siteCode, requestData).ToList();
            EntityMapper.Map<IList<SiteItemDishMapping>, IList<SiteItemDishMappingData>>(entityList, result);
            return result;
        }
        public IList<SiteItemDayPartMappingData> GetSiteItemDayPartMappingDataList(string itemCode, string siteCode, RequestData requestData)
        {
            List<SiteItemDayPartMappingData> result = new List<SiteItemDayPartMappingData>();
            SiteItemDayPartMappingRepository = new SiteItemDayPartMappingRepository();
            var entityList = SiteItemDayPartMappingRepository.GetSiteItemDayPartMappingDataList(itemCode, siteCode, requestData).ToList();
            EntityMapper.Map<IList<SiteItemDayPartMapping>, IList<SiteItemDayPartMappingData>>(entityList, result);
            return result;
        }
        public IList<SiteDayPartMappingData> GetSiteDayPartMappingDataList(string siteCode, RequestData requestData)
        {
            List<SiteDayPartMappingData> result = new List<SiteDayPartMappingData>();
            SiteDayPartMappingRepository = new SiteDayPartMappingRepository();
            var entityList = SiteDayPartMappingRepository.GetSiteDayPartMappingDataList(siteCode, requestData).ToList();
            EntityMapper.Map<IList<SiteDayPartMapping>, IList<SiteDayPartMappingData>>(entityList, result);
            return result;
        }

        public IList<FoodProgramDayPartMappingData> GetFoodProgramDayPartMappingDataList(string foodCode, RequestData requestData)
        {
            List<FoodProgramDayPartMappingData> result = new List<FoodProgramDayPartMappingData>();
            FoodProgramDayPartMappingRepository = new FoodProgramDayPartMappingRepository();
            var entityList = FoodProgramDayPartMappingRepository.GetFoodProgramDayPartMappingDataList(foodCode, requestData).ToList();
            EntityMapper.Map<IList<FoodProgramDayPartMapping>, IList<FoodProgramDayPartMappingData>>(entityList, result);
            return result;
        }
        public IList<SiteItemDishMappingData> GetSiteItemDishMappingDataList(string itemCode, string siteCode, RequestData requestData)
        {
            List<SiteItemDishMappingData> result = new List<SiteItemDishMappingData>();
            SiteItemDishMappingRepository = new SiteItemDishMappingRepository();
            var entityList = SiteItemDishMappingRepository.GetSiteItemDishMappingDataList(itemCode, siteCode, requestData).ToList();
            EntityMapper.Map<IList<SiteItemDishMapping>, IList<SiteItemDishMappingData>>(entityList, result);
            return result;
        }

        public IList<FoodBookNutritionData> GetFoodBookNutritionDataList(string sectorCode)
        {
            List<FoodBookNutritionData> result = new List<FoodBookNutritionData>();
            SiteItemDishMappingRepository = new SiteItemDishMappingRepository();
            result = SiteItemDishMappingRepository.GetFoodBookNutritionDataList(sectorCode).ToList();
            return result;
        }
        public string SaveSiteItemDayPartMappingDataList(IList<SiteItemDayPartMappingData> model, RequestData requestData)
        {
            List<SiteItemDayPartMapping> entity = new List<SiteItemDayPartMapping>();
            SiteItemDayPartMappingRepository = new SiteItemDayPartMappingRepository();
            EntityMapper.Map<IList<SiteItemDayPartMappingData>, IList<SiteItemDayPartMapping>>(model, entity);
            string x = SiteItemDayPartMappingRepository.SaveSiteItemDayPartMappingList(entity, requestData);
            return x;
        }
        public string SaveFoodProgramDayPartMappingDataList(IList<FoodProgramDayPartMappingData> model, RequestData requestData)
        {
            List<FoodProgramDayPartMapping> entity = new List<FoodProgramDayPartMapping>();
            FoodProgramDayPartMappingRepository = new FoodProgramDayPartMappingRepository();
            EntityMapper.Map<IList<FoodProgramDayPartMappingData>, IList<FoodProgramDayPartMapping>>(model, entity);
            string x = FoodProgramDayPartMappingRepository.SaveFoodProgramDayPartMappingList(entity, requestData);
            return x;
        }

        public string SaveSiteDayPartMappingDataList(IList<SiteDayPartMappingData> model, bool isClosingTime, RequestData requestData)
        {
            List<SiteDayPartMapping> entity = new List<SiteDayPartMapping>();
            SiteDayPartMappingRepository = new SiteDayPartMappingRepository();
            EntityMapper.Map<IList<SiteDayPartMappingData>, IList<SiteDayPartMapping>>(model, entity);
            string x = SiteDayPartMappingRepository.SaveSiteDayPartMappingList(entity, isClosingTime, requestData);
            return x;
        }
        public string SaveSiteItemDishMappingDataList(IList<SiteItemDishMappingData> model, RequestData requestData)
        {
            List<SiteItemDishMapping> entity = new List<SiteItemDishMapping>();
            SiteItemDishMappingRepository = new SiteItemDishMappingRepository();
            EntityMapper.Map<IList<SiteItemDishMappingData>, IList<SiteItemDishMapping>>(model, entity);
            string x = SiteItemDishMappingRepository.SaveSiteItemDishMappingList(entity, model[0].Status, requestData);
            return x;
        }
        public string SaveItemDishCategoryMappingDataList(IList<ItemDishCategoryMappingData> model, RequestData requestData)
        {
            List<ItemDishCategoryMapping> entity = new List<ItemDishCategoryMapping>();
            ItemDishCategoryMappingRepository = new ItemDishCategoryMappingRepository();
            EntityMapper.Map<IList<ItemDishCategoryMappingData>, IList<ItemDishCategoryMapping>>(model, entity);
            string x = ItemDishCategoryMappingRepository.SaveItemDishCategoryMappingList(entity, requestData);
            return x;
        }

        public IList<ItemDishCategoryMappingData> GetItemDishCategoryMappingDataList(string itemCode, RequestData requestData)
        {
            List<ItemDishCategoryMappingData> result = new List<ItemDishCategoryMappingData>();
            ItemDishCategoryMappingRepository = new ItemDishCategoryMappingRepository();
            var entityList = ItemDishCategoryMappingRepository.GetItemDishCategoryMappingDataList(itemCode, requestData).ToList();
            EntityMapper.Map<IList<ItemDishCategoryMapping>, IList<ItemDishCategoryMappingData>>(entityList, result);
            return result;
        }

        public IList<ItemDishCategoryMappingData> GetNationalItemDishCategoryMappingDataList(string itemCode, string sectorcode, RequestData requestData)
        {
            List<ItemDishCategoryMappingData> result = new List<ItemDishCategoryMappingData>();
            ItemDishCategoryMappingRepository = new ItemDishCategoryMappingRepository();
            var entityList = ItemDishCategoryMappingRepository.GetNationalItemDishCategoryMappingDataList(itemCode, sectorcode, requestData).ToList();
            EntityMapper.Map<IList<ItemDishCategoryMapping>, IList<ItemDishCategoryMappingData>>(entityList, result);
            return result;
        }
        public IList<RegionDishCategoryMappingData> GetRegionDishCategoryMappingDataList(string regionID, string itemCode, RequestData requestData)
        {
            List<RegionDishCategoryMappingData> result = new List<RegionDishCategoryMappingData>();
            RegionDishCategoryMappingRepository = new RegionDishCategoryMappingRepository();
            var entityList = RegionDishCategoryMappingRepository.GetRegionDishCategoryMappingDataList(regionID, itemCode, requestData).ToList();
            EntityMapper.Map<IList<RegionDishCategoryMapping>, IList<RegionDishCategoryMappingData>>(entityList, result);
            return result;
        }

        public IList<SiteDishCategoryMappingData> GetSiteDishCategoryMappingDataList(string SiteCode, string itemCode, RequestData requestData)
        {
            List<SiteDishCategoryMappingData> result = new List<SiteDishCategoryMappingData>();
            SiteDishCategoryMappingRepository = new SiteDishCategoryMappingRepository();
            var entityList = SiteDishCategoryMappingRepository.GetSiteDishCategoryMappingDataList(SiteCode, itemCode, requestData).ToList();
            EntityMapper.Map<IList<SiteDishCategoryMapping>, IList<SiteDishCategoryMappingData>>(entityList, result);
            return result;
        }

        public string SaveRegionDishCategoryMappingDataList(IList<RegionDishCategoryMappingData> model, RequestData requestData)
        {
            List<RegionDishCategoryMapping> entity = new List<RegionDishCategoryMapping>();
            RegionDishCategoryMappingRepository = new RegionDishCategoryMappingRepository();
            EntityMapper.Map<IList<RegionDishCategoryMappingData>, IList<RegionDishCategoryMapping>>(model, entity);
            string x = RegionDishCategoryMappingRepository.SaveRegionDishCategoryMappingList(entity, requestData);
            return x;
        }

        public string SaveSiteDishCategoryMappingDataList(IList<SiteDishCategoryMappingData> model, RequestData requestData)
        {
            List<SiteDishCategoryMapping> entity = new List<SiteDishCategoryMapping>();
            SiteDishCategoryMappingRepository = new SiteDishCategoryMappingRepository();
            EntityMapper.Map<IList<SiteDishCategoryMappingData>, IList<SiteDishCategoryMapping>>(model, entity);
            string x = SiteDishCategoryMappingRepository.SaveSiteDishCategoryMappingList(entity, requestData);
            return x;
        }

        public IList<RegionItemInheritanceMappingData> GetRegionItemInheritanceMappingDataList(string regionID, RequestData requestData)
        {
            List<RegionItemInheritanceMappingData> result = new List<RegionItemInheritanceMappingData>();
            RegionItemInheritanceMappingRepository = new RegionItemInheritanceMappingRepository();
            var entityList = RegionItemInheritanceMappingRepository.GetRegionItemInheritanceMappingDataList(regionID, requestData).ToList();
            EntityMapper.Map<IList<procGetRegionItemList_Result>, IList<RegionItemInheritanceMappingData>>(entityList, result);
            return result;
        }

        public string SaveSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData requestData)
        {
            RegionItemInheritanceMappingRepository = new RegionItemInheritanceMappingRepository();
            string x = RegionItemInheritanceMappingRepository.SaveSectorToRegionInheritance(model, requestData);
            return x;
        }

        public string SaveMOGNationalToSectorInheritance(SaveMOGNationalToSectorInheritanceData model, RequestData requestData)
        {
            RegionItemInheritanceMappingRepository = new RegionItemInheritanceMappingRepository();
            string x = RegionItemInheritanceMappingRepository.SaveMOGNationalToSectorInheritance(model, requestData);
            return x;
        }

        public string UpdateCostSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData requestData)
        {
            RegionItemInheritanceMappingRepository = new RegionItemInheritanceMappingRepository();
            string x = RegionItemInheritanceMappingRepository.UpdateCostSectorToRegionInheritance(model, requestData);
            return x;
        }
        public IList<SiteItemInheritanceMappingData> GetSiteItemInheritanceMappingDataList(string SiteCode, RequestData requestData)
        {
            List<SiteItemInheritanceMappingData> result = new List<SiteItemInheritanceMappingData>();
            SiteItemInheritanceMappingRepository = new SiteItemInheritanceMappingRepository();
            var entityList = SiteItemInheritanceMappingRepository.GetSiteItemInheritanceMappingDataList(SiteCode, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteItemInheritanceMappingData_Result>, IList<SiteItemInheritanceMappingData>>(entityList, result);
            return result;
        }

        public string SaveRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData)
        {
            SiteItemInheritanceMappingRepository = new SiteItemInheritanceMappingRepository();
            string x = SiteItemInheritanceMappingRepository.SaveRegionToSiteInheritance(model, requestData);
            return x;
        }

        public string SaveToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData)
        {
            SiteItemInheritanceMappingRepository = new SiteItemInheritanceMappingRepository();
            SiteItemInheritanceMapping entity = new SiteItemInheritanceMapping();
            EntityMapper.Map(model, entity);

            string x = SiteItemInheritanceMappingRepository.SaveToSiteInheritance(entity, requestData);
            return x;
        }

        public string SaveToSiteInheritanceDataList(IList<SiteItemInheritanceMappingData> model, RequestData requestData)
        {

            //List<RegionDishCategoryMapping> entity = new List<RegionDishCategoryMapping>();
            //RegionDishCategoryMappingRepository = new RegionDishCategoryMappingRepository();
            //EntityMapper.Map<IList<RegionDishCategoryMappingData>, IList<RegionDishCategoryMapping>>(model, entity);

            SiteItemInheritanceMappingRepository = new SiteItemInheritanceMappingRepository();
            List<SiteItemInheritanceMapping> entity = new List<SiteItemInheritanceMapping>();
            EntityMapper.Map<IList<SiteItemInheritanceMappingData>, List<SiteItemInheritanceMapping>>(model, entity);

            string x = SiteItemInheritanceMappingRepository.SaveToSiteInheritanceDataList(entity, requestData);
            return x;
        }

        public string UpdateCostRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData)
        {
            SiteItemInheritanceMappingRepository = new SiteItemInheritanceMappingRepository();
            string x = SiteItemInheritanceMappingRepository.UpdateCostRegionToSiteInheritance(model, requestData);
            return x;
        }

        public IList<ItemExportData> GetItemExportDataList(RequestData requestData)
        {
            List<ItemExportData> result = new List<ItemExportData>();
            var entityList = ItemRepository.GetItemExportDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetItemListExport_Result>, IList<ItemExportData>>(entityList, result);
            return result;
        }

        public IList<InheritanceChangesData> GetInheritChangesSectorToRegion(ItemInheritChangeRequestData itemInheritChangeRequestData, RequestData requestData)
        {
            List<InheritanceChangesData> result = new List<InheritanceChangesData>();
            var entityList = SiteItemInheritanceMappingRepository.GetInheritChangesSectorToRegion(itemInheritChangeRequestData, requestData).ToList();
            EntityMapper.Map<IList<ProcGetInheritChangesSectorToRegion_Result>, IList<InheritanceChangesData>>(entityList, result);
            return result;
        }

        public IList<InheritanceChangesData> GetInheritChangesRegionToSite(ItemInheritChangeRequestData itemInheritChangeRequestData, RequestData requestData)
        {
            List<InheritanceChangesData> result = new List<InheritanceChangesData>();
            var entityList = SiteItemInheritanceMappingRepository.GetInheritChangesRegionToSite(itemInheritChangeRequestData, requestData).ToList();
            EntityMapper.Map<IList<ProcGetInheritChangesRegionToSite_Result>, IList<InheritanceChangesData>>(entityList, result);
            return result;
        }

        public string SaveChangesSectorToRegion(List<InheritanceChangesData> model, RequestData requestData)
        {
            string x = SiteItemInheritanceMappingRepository.SaveChangesSectorToRegion(model, requestData);
            return x;
        }

        public string SaveChangesRegionToSite(List<InheritanceChangesData> model, RequestData requestData)
        {
            string x = SiteItemInheritanceMappingRepository.SaveChangesRegionToSite(model, requestData);
            return x;
        }

        public SiteItemDropDownData GetSiteItemDropDownDataData(RequestData requestData)
        {
            return ItemRepository.GetSiteItemDropDownDataData(requestData);
        }
        //Krish 28-07-2022
        public SectorItemData GetSectorItemData(RequestData requestData, bool isAddEdit, bool onLoad)
        {
            return ItemRepository.GetSectorItemData(requestData, isAddEdit,onLoad);
        }
        //Krish 30-07-2022
        public RegionItemData GetRegionItemData(RequestData requestData, bool isSectorUser, int regionId = 0, bool gridOnly = false)
        {
            return ItemRepository.GetRegionItemData(requestData, isSectorUser,regionId,gridOnly);
        }
    }
}
