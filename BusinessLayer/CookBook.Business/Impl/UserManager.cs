﻿using CookBook.Aspects.Constants;
using CookBook.Encryption.Interfaces;
using CookBook.Aspects.Factory;
using CookBook.Aspects.Utils;
using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.Common;
using CookBook.Data.UserData;
using CookBook.DataAdapter.Factory;
using CookBook.DataAdapter.Factory.Contract;
using CookBook.Mapper;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Microsoft.SharePoint.Client;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Base.DataEnums;
using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Factory.Repository;

namespace CookBook.Business.Impl
{
    [Export(typeof(IUser))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserManager : ImplBase, IUser
    {
        [Dependency]
        public IUserMasterRepository UserMasterRepository { get; set; }      
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IAesCryptoEngine AesCryptoEngine { get; set; }

        [Dependency]
        public IControlDeskRepository ControlDeskRepository { get; set; }

        #region NewCode

        public UserMasterResponseData GetUserData(string userName, RequestData requestData)
       {
            //LogTraceFactory.WriteLogWithCategory("Looged in at + ", LogTraceCategoryNames.General);
            UserMasterResponseData result = null;

            string encryptedUserName = AesCryptoEngine.EncryptData(userName.ToLower());

            procGetUserDataByUsername_Result procGetUserDataByUsername_Result = UserMasterRepository.GetUserDetailsByUsername(encryptedUserName,requestData);
            if(procGetUserDataByUsername_Result != null)
            {
                result = new UserMasterResponseData();
                result.UserId = procGetUserDataByUsername_Result.UserId;
                result.EmailId = procGetUserDataByUsername_Result.EmailId;
                result.UserName = procGetUserDataByUsername_Result.UserName;
                result.UserRoleId = procGetUserDataByUsername_Result.UserRoleId;
                result.UserRoleName = procGetUserDataByUsername_Result.UserRoleName;
                result.DisplayName = procGetUserDataByUsername_Result.DisplayName;
                result.SectorName = procGetUserDataByUsername_Result.SectorName;
                result.SectorNumber = procGetUserDataByUsername_Result.SectorNumber;
                result.IsSectorUser = procGetUserDataByUsername_Result.IsSectorUser == null ? false : procGetUserDataByUsername_Result.IsSectorUser.Value;
                //Krish
                result.StateRegionId = procGetUserDataByUsername_Result.StateRegionId;
            }
            return result;
        }

        public IList<SectorData> GetSectorDataByUserID (int userID, RequestData requestData)
        {
            List<SectorData> result = new List<SectorData>();
            var entityList = UserMasterRepository.GetSectorDataByUserID(userID,requestData).ToList();
            EntityMapper.Map<IList<procGetSectorDataByUserID_Result>, IList<SectorData>>(entityList, result);
            return result;

        }

        public IList<ModuleData> GetModulesByUserID(int userID, RequestData requestData)
        {
            List<ModuleData> result = new List<ModuleData>();
            var entityList = UserMasterRepository.GetModulesByUserID(userID,requestData).ToList();
            EntityMapper.Map<IList<procGetUserModules_Result>, IList<ModuleData>>(entityList, result);
            return result;
        }

        public IList<UserPermissionData> GetUserPermissionsByModuleCode(int userID, string moduleCode, RequestData requestData)
        {
            List<UserPermissionData> result = new List<UserPermissionData>();
            var entityList = UserMasterRepository.GetUserPermissionsByModuleCode(userID,moduleCode,requestData).ToList();
            EntityMapper.Map<IList<procGetUserModulePermission_Result>, IList<UserPermissionData>>(entityList, result);
            return result;
        }

        public string ChangeSectorData(int UserID, string sectorNumber, RequestData requestData)
        {
            string x = UserMasterRepository.ChangeSectorData(UserID, sectorNumber,requestData);
            return x.ToString();
        }

        public string ChangeUserRoleData(int UserID, int userRoleID, RequestData requestData)
        {
            string x = UserMasterRepository.ChangeUserRoleData(UserID, userRoleID, requestData);
            return x.ToString();
        }

        public string AddUpdateUserDetails(UserMasterData userData, RequestData requestData)
        {
            string result = string.Empty;
            string national = EnumUtils<GeographicLocations>.GetDescription(GeographicLocations.National).ToString();
            try
            {
                if (userData != null)
                {
                    UserMaster data = new UserMaster();
                    var pre = UserMasterRepository.Find(e => e.UserName == userData.UserName,requestData.SessionSectorName).FirstOrDefault();

                    if (pre != null)
                    {
                        data = pre;
                    }

                    data.UserName = userData.UserName;
                    data.IsActive = userData.IsActive;
                    data.EmailId = userData.EmailId;
                    if (pre == null)
                    {
                        data.CreatedDate = DateTime.Now;
                        data.CreatedBy = "Super Admin";
                    }
                    else
                    {
                        data.ModifiedDate = DateTime.Now;
                    }

                    UserMasterRepository.Save(data,requestData.SessionSectorName);
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public string DeleteUserData(string userName, RequestData requestData)
        {
            string result = string.Empty;
            try
            {
                UserMaster data = new UserMaster();
                data = UserMasterRepository.Find(e => e.UserName == userName,requestData.SessionSectorName).FirstOrDefault();
                if (data != null)
                {
                    UserMasterRepository.Delete(data, requestData.SessionSectorName);
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public IList<UserMasterData> GetUserMasterDataList(RequestData requestData)
        {
            List<UserMasterData> result = new List<UserMasterData>();
            var entityList = UserMasterRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<UserMaster>, IList<UserMasterData>>(entityList, result);
            return result;
        }

        public List<ManageSectorData> GetSectorDataList(RequestData requestData)
        {
            List<ManageSectorData> result = new List<ManageSectorData>();
            var entityList = ControlDeskRepository.GetManageSectorList(requestData).ToList();
            EntityMapper.Map<IList<procGetSectorMasterData_Result>, IList<ManageSectorData>>(entityList, result);
            return result;
        }

        public List<ManageSectorData> GetSectorListInfo(RequestData requestData)
        {
            List<ManageSectorData> result = new List<ManageSectorData>();
            var entityList = ControlDeskRepository.GetSectorListInfo(requestData).ToList();
            EntityMapper.Map<IList<procGetCapringSectorMaster_Result>, IList<ManageSectorData>>(entityList, result);
            return result;
        }


        public List<LevelMasterData> GetLevelMasterData(RequestData requestData)
        {
            List<LevelMasterData> result = new List<LevelMasterData>();
            var entityList = ControlDeskRepository.GetLevelMasterData(requestData).ToList();
            EntityMapper.Map<IList<procGetCapringLevelMaster_Result>, IList<LevelMasterData>>(entityList, result);
            return result;
        }

        public List<FunctionMasterData> GetFunctionMasterData(RequestData requestData)
        {
            List<FunctionMasterData> result = new List<FunctionMasterData>();
            var entityList = ControlDeskRepository.GetFunctionMasterData(requestData).ToList();
            EntityMapper.Map<IList<procGetCapringFunctionMaster_Result>, IList<FunctionMasterData>>(entityList, result);
            return result;
        }

        public List<PermissionMasterData> GetPermissionMasterData(RequestData requestData)
        {
            List<PermissionMasterData> result = new List<PermissionMasterData>();
            var entityList = ControlDeskRepository.GetPermissionMasterData(requestData).ToList();
            EntityMapper.Map<IList<procGetCapringPermissionMaster_Result>, IList<PermissionMasterData>>(entityList, result);
            return result;
        }


        public string SaveManageSectorInfo(RequestData request, ManageSectorData model, int usercode)
        {
            string entityList = ControlDeskRepository.SaveManageSectorInfo(request,model,usercode).ToString();            
            return entityList;
        }

        public string SaveManageLevelInfo(RequestData request, LevelMasterData model, int usercode)
        {
            string entityList = ControlDeskRepository.SaveManageLevelInfo(request, model, usercode).ToString();
            return entityList;
        }

        public string SaveFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode)
        {
            string entityList = ControlDeskRepository.SaveFunctionLevelInfo(request, model, usercode).ToString();
            return entityList;
        }


        public string SavePermissionInfo(RequestData request, PermissionMasterData model, int usercode)
        {
            string entityList = ControlDeskRepository.SavePermissionInfo(request, model, usercode).ToString();
            return entityList;
        }

        public string SaveCapringInfo(RequestData request, CapringMastre model, int usercode)
        {
            string entityList = ControlDeskRepository.SaveCapringInfo(request, model, usercode).ToString();
            return entityList;
        }

        public string UpdateCapringInfo(RequestData request, CapringMastre model, int usercode)
        {
            string entityList = ControlDeskRepository.UpdateCapringInfo(request, model, usercode).ToString();
            return entityList;
        }

        public string UpdateCapringDescriptionInfo(RequestData request, CapringMastre model, int usercode)
        {
            string entityList = ControlDeskRepository.UpdateCapringDescriptionInfo(request, model, usercode).ToString();
            return entityList;
        }

        public string UpdatePermissionInfo(RequestData request, PermissionMasterData model, int usercode)
        {
            string entityList = ControlDeskRepository.UpdatePermissionInfo(request, model, usercode).ToString();
            return entityList;
        }

        public string UpdateFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode)
        {
            string entityList = ControlDeskRepository.UpdateFunctionLevelInfo(request, model, usercode).ToString();
            return entityList;
        }

        public string UpdatefunctionInactiveActive(RequestData request, FunctionMasterData model, int usercode)
        {
            string entityList = ControlDeskRepository.UpdatefunctionInactiveActive(request, model, usercode).ToString();
            return entityList;
        }

        public string UpdatePermissionInactiveActive(RequestData request, PermissionMasterData model, int usercode)
        {
            string entityList = ControlDeskRepository.UpdatePermissionInactiveActive(request, model, usercode).ToString();
            return entityList;
        }

        public string UpdateManageLevelInfo(RequestData request, LevelMasterData model, int usercode)
        {
            string entityList = ControlDeskRepository.UpdateManageLevelInfo(request, model, usercode).ToString();
            return entityList;
        }

        public string UpdateManageSectorInfo(RequestData request, ManageSectorData model, int usercode)
        {
            string entityList = ControlDeskRepository.UpdateManageSectorInfo(request, model, usercode).ToString();
            return entityList;
        }

        public string UpdateInactiveActive(RequestData request, ManageSectorData model, int usercode)
        {
            string entityList = ControlDeskRepository.UpdateInactiveActive(request, model, usercode).ToString();
            return entityList;
        }

        public string UpdateLevelInactiveActive(RequestData request, LevelMasterData model, int usercode)
        {
            string entityList = ControlDeskRepository.UpdateLevelInactiveActive(request, model, usercode).ToString();
            return entityList;
        }

        public string UpdateCapringInactiveActive(RequestData request, CapringMastre model, int usercode)
        {
            string entityList = ControlDeskRepository.UpdateCapringInactiveActive(request, model, usercode).ToString();
            return entityList;
        }

        public List<LevelMasterData> GetLevelDataList(RequestData requestData)
        {
            List<LevelMasterData> result = new List<LevelMasterData>();
            var entityList = ControlDeskRepository.GetLevelDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetLevelMasterData_Result>, IList<LevelMasterData>>(entityList, result);
            return result;
        }

        public List<CapringMastre> GetCapringMasterList(RequestData requestData)
        {
            List<CapringMastre> result = new List<CapringMastre>();
            var entityList = ControlDeskRepository.GetCapringMasterList(requestData).ToList();
            EntityMapper.Map<IList<procGetCapringMasterData_Result>, IList<CapringMastre>>(entityList, result);
            return result;
        }

        public List<FunctionMasterData> GetFunctionDataList(RequestData requestData)
        {
            List<FunctionMasterData> result = new List<FunctionMasterData>();
            var entityList = ControlDeskRepository.GetFunctionDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetFunctionalityData_Result>, IList<FunctionMasterData>>(entityList, result);
            return result;
        }

        public List<PermissionMasterData> GetPermissionDataList(RequestData requestData)
        {
            List<PermissionMasterData> result = new List<PermissionMasterData>();
            var entityList = ControlDeskRepository.GetPermissionDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetPermissionData_Result>, IList<PermissionMasterData>>(entityList, result);
            return result;
        }

        #endregion

        //Krish
        public IList<UserRoleData> GetUserRoleDataByUserID(int userID, RequestData requestData)
        {
            List<UserRoleData> result = new List<UserRoleData>();
            var entityList = UserMasterRepository.GetUserRoleDataByUserID(userID, requestData).ToList();
            EntityMapper.Map<IList<procGetRoleDataByUserID_Result>, IList<UserRoleData>>(entityList, result);
            return result;

        }
    }
}
