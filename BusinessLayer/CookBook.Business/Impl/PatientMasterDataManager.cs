﻿using CookBook.Aspects.Constants;
using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.DataAdapter.Factory.Contract;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Encryption.EncryptImpl;
using CookBook.Mapper;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Business.Impl
{
    [Export(typeof(IPatientMasterDataManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class PatientMasterDataManager : ImplBase, IPatientMasterDataManager
    {
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IPatientMasterRepository PatientMasterRepository { get; set; }

        public List<PatientMasterData> GetPatientMaster(RequestData requestData)
        {
            List<PatientMasterData> result = new List<PatientMasterData>();
            var entityList = PatientMasterRepository.GetPatientMaster(requestData).ToList();
            EntityMapper.Map<List<procGetPatientMaster_Result>, List<PatientMasterData>>(entityList, result);
           
            return result;
        }

        public List<PatientMasterHISTORY> GetPatientMasterHISTORY(RequestData requestData, string username)
        {
            List<PatientMasterHISTORY> result = new List<PatientMasterHISTORY>();
            var entityList = PatientMasterRepository.GetPatientMasterHISTORY(requestData, username).ToList();
            EntityMapper.Map<List<procGetPatientMasterBATCHHEADER_Result>, List<PatientMasterHISTORY>>(entityList, result);
            return result;
        }
        public List<PatientMasterHISTORY> GetPatientMasterHISTORYDet(RequestData requestData, string username)
        {
            List<PatientMasterHISTORY> result = new List<PatientMasterHISTORY>();
            var entityList = PatientMasterRepository.GetPatientMasterHISTORYDet(requestData, username).ToList();
            EntityMapper.Map<List<procGetPatientMasterBATCHDETAILS_Result>, List<PatientMasterHISTORY>>(entityList, result);
            return result;
        }
        public List<PatientMasterHistoryBatchWise> GetshowGetALLPatientMasterHISTORYBATCHWISE(RequestData requestData, string batchno)
        {
            List<PatientMasterHistoryBatchWise> result = new List<PatientMasterHistoryBatchWise>();
            var entityList = PatientMasterRepository.GetshowGetALLPatientMasterHISTORYBATCHWISE(requestData, batchno).ToList();
            EntityMapper.Map<List<procshowGetALLPatientMasterHISTORYBATCHWISE_Result>, List<PatientMasterHistoryBatchWise>>(entityList, result);
            return result;
        }
        public string AddUpdatePatientMasterList(RequestData request, string xml, string username)
        {

            string entityList = PatientMasterRepository.AddUpdatePatientMasterList(request, xml, username).ToString();
            return entityList;
        }
        public string SavePatientMasterStatus(RequestData request, int id, int username)
        {
            string entityList = PatientMasterRepository.SavePatientMasterStatus(request, id, username).ToString();
            return entityList;
        }
        public List<PatientMasterHistoryAllData> GetALLHISTORYDetailsPatientMaster(RequestData request)
        {
            List<PatientMasterHistoryAllData> result = new List<PatientMasterHistoryAllData>();
            var entityList = PatientMasterRepository.GetALLHISTORYDetailsPatientMaster(request).ToList();
            EntityMapper.Map<List<procshowGetALLHISTORYPatientMaster_Result>, List<PatientMasterHistoryAllData>>(entityList, result);
            return result;

        }
    }
}
