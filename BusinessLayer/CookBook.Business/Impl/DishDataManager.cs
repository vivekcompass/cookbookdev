﻿using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.Common;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using CookBook.Mapper;
using CookBook.DataAdapter.Factory.Contract;
using CookBook.DataAdapter.Factory.Repository;
using CookBook.Data.MasterData;
using System.Linq;
using CookBook.DataAdapter.Factory;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using CookBook.Data.Request;

namespace CookBook.Business.Impl
{
    [Export(typeof(IDishDataManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DishDataManager : ImplBase, IDishDataManager
    {
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IDishRepository DishRepository { get; set; }
        [Dependency]
        public IDishRecipeMappingRepository DishRecipeMappingRepository { get; set; }
  
        public IList<DishData> GetDishDataList(int? foodProgramID, int? dietCategoryID, RequestData requestData)
        {
            List<DishData> result = new List<DishData>();
            var entityList = DishRepository.GetDishDataList(foodProgramID, dietCategoryID, requestData).ToList();
            EntityMapper.Map<IList<procGetDishList_Result>, IList<DishData>>(entityList, result);
            return result;
        }
        public IList<DishData> GetSiteDishDataList(string siteCode, int? foodProgramID, int? dietCategoryID, RequestData requestData)
        {
            List<DishData> result = new List<DishData>();
            var entityList = DishRepository.GetSiteDishDataList(siteCode,foodProgramID, dietCategoryID, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteDishList_Result>, IList<DishData>>(entityList, result);
            return result;
        }
        public IList<DishData> GetRegionDishMasterData(int regionID, int? foodProgramID, int? dietCategoryID, RequestData requestData)
        {
            List<DishData> result = new List<DishData>();
            var entityList = DishRepository.GetRegionDishMasterData(regionID, foodProgramID, dietCategoryID, requestData).ToList();
            EntityMapper.Map<IList<procGetRegionDishMasterData_Result>, IList<DishData>>(entityList, result);
            return result;
        }
        public IList<DishData> GetRegionDishDataList(int regionID, int? foodProgramID, int? dietCategoryID, string subSectorCode, RequestData requestData)
        {
            List<DishData> result = new List<DishData>();
            var entityList = DishRepository.GetRegionDishDataList(regionID,foodProgramID, dietCategoryID, subSectorCode, requestData).ToList();
            EntityMapper.Map<IList<procGetRegionDishList_Result>, IList<DishData>>(entityList, result);
            return result;
        }
        public IList<DishCategoryData> GetDishCategory(RequestData requestData)
        {
            List<DishCategoryData> result = new List<DishCategoryData>();
            var entityList = DishRepository.GetDishCategory(requestData).ToList();
            EntityMapper.Map<IList<DishCategory>, IList<DishCategoryData>>(entityList, result);
            return result;
        }

        public IList<PlanningTagData> GetPlanningTag(RequestData requestData)
        {
            List<PlanningTagData> result = new List<PlanningTagData>();
            var entityList = DishRepository.GetPlanningTag(requestData).ToList();
            EntityMapper.Map<IList<PlanningTag>, IList<PlanningTagData>>(entityList, result);
            return result;
        }

        public string SavePlanningTagData(PlanningTagData model, RequestData requestData)
        {
            string x = DishRepository.SavePlanningTagData(model, requestData);
            return x;
        }

        public string SaveDishData(DishData model, RequestData requestData)
        {
            Dish entity = new Dish();
            if (model.ID ==0 || model.ID == null) {
                var code = DishRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.DishCode).FirstOrDefault()?.DishCode;
                if (code is null)
                    if (requestData.SessionSectorNumber == "10")
                    {
                        if (model.SubSectorCode == "SSR-001")
                        {
                            code = "OSH-00001";
                        }
                        else if (model.SubSectorCode == "SSR-002")
                        {
                            code = "GSH-00001";
                        }
                        else if (model.SubSectorCode == "SSR-003")
                        {
                            code = "VSH-00001";
                        }
                        else
                        {
                            code = "DSH-00001";
                        }
                    }
                    else
                    {
                        code = "DSH-00001";
                    }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.DishCode = code;
               // model.DietTypeCode = "DIT-00001";
                if (string.IsNullOrWhiteSpace(model.DishCode))
                {
                    model.CreatedBy = requestData.SessionUserId;
                    model.CreatedOn = DateTime.Now;
                }
                else
                {
                    model.ModifiedBy = requestData.SessionUserId;
                    model.ModifiedOn = DateTime.Now;
                }
            }
            EntityMapper.Map<DishData, Dish>(model, entity);
            var dup = DishRepository.Find(y => y.Name.Trim().ToLower() == model.Name.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
            if (dup != null && model.ID == 0)
                return "Duplicate";

            bool x = DishRepository.Save(entity, requestData.SessionSectorName);
            DishRepository.SaveDishData(model, requestData);
            return x.ToString();
        }

        public string SaveDishDataList(IList<DishData> model, RequestData requestData)
        {
            List<Dish> entity = new List<Dish>();
            EntityMapper.Map<IList<DishData>, IList<Dish>>(model, entity);
            string x = DishRepository.SaveList(entity,requestData);
            return x;
        }

        public string SaveDishRecipeMappingList(IList<DishRecipeMappingData> model, RequestData requestData)
        {
            List<DishRecipeMapping> entity = new List<DishRecipeMapping>();
            EntityMapper.Map<IList<DishRecipeMappingData>, IList<DishRecipeMapping>>(model, entity);
            string x = DishRecipeMappingRepository.SaveDishRecipeMappingList(entity, model[0].Status,requestData);
            return x;
        }

        public string SaveRegionDishRecipeMappingList(IList<RegionDishRecipeMappingData> model, RequestData requestData)
        {
            List<RegionDishRecipeMapping> entity = new List<RegionDishRecipeMapping>();
            EntityMapper.Map<IList<RegionDishRecipeMappingData>, IList<RegionDishRecipeMapping>>(model, entity);
            string x = DishRecipeMappingRepository.SaveRegionDishRecipeMappingList(entity, requestData);
            return x;
        }
        public string SaveSiteDishRecipeMappingList(IList<SiteDishRecipeMappingData> model, RequestData requestData)
        {
            List<SiteDishRecipeMapping> entity = new List<SiteDishRecipeMapping>();
            EntityMapper.Map<IList<SiteDishRecipeMappingData>, IList<SiteDishRecipeMapping>>(model, entity);
            string x = DishRecipeMappingRepository.SaveSiteDishRecipeMappingList(entity, requestData);
            return x;
        }
        public IList<DishRecipeMappingData> GetDishRecipeMappingDataList(string DishCode, RequestData requestData)
        {
            List<DishRecipeMappingData> result = new List<DishRecipeMappingData>();
            var entityList = DishRecipeMappingRepository.GetDishRecipeMappingDataList(DishCode, requestData).ToList();
            EntityMapper.Map<IList<DishRecipeMapping>, IList<DishRecipeMappingData>>(entityList, result);
            return result;
        }
        public IList<RegionDishRecipeMappingData> GetRegionDishRecipeMappingDataList(int RegionID,string DishCode, string SubSectorCode, RequestData requestData)
        {
            List<RegionDishRecipeMappingData> result = new List<RegionDishRecipeMappingData>();
            var entityList = DishRecipeMappingRepository.GetRegionDishRecipeMappingDataList(RegionID, DishCode, SubSectorCode, requestData).ToList();
            EntityMapper.Map<IList<RegionDishRecipeMapping>, IList<RegionDishRecipeMappingData>>(entityList, result);
            return result;
        }
        public IList<SiteDishRecipeMappingData> GetSiteDishRecipeMappingDataList(string SiteCode, string DishCode, RequestData requestData)
        {
            List<SiteDishRecipeMappingData> result = new List<SiteDishRecipeMappingData>();
            var entityList = DishRecipeMappingRepository.GetSiteDishRecipeMappingDataList(SiteCode, DishCode, requestData).ToList();
            EntityMapper.Map<IList<SiteDishRecipeMapping>, IList<SiteDishRecipeMappingData>>(entityList, result);
            return result;
        }
        //Krish 02-08-2022
        public SectorDishData GetSectorDishData(RequestData requestData, string condition, bool isAddEdit, bool onLoad)
        {
            return DishRepository.GetSectorDishData(requestData, condition, isAddEdit, onLoad);
        }
        //Krish 08-10-2022
        public string SaveRangeTypeDishData(string dishCode, string rangeTypeCode, RequestData requestData)
        {
            return DishRepository.SaveRangeTypeDishData(dishCode, rangeTypeCode, requestData);
        }
    }
}
