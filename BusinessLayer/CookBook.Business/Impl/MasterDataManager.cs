﻿using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.Common;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using CookBook.Mapper;
using CookBook.DataAdapter.Factory.Contract;
using CookBook.DataAdapter.Factory.Repository;
using CookBook.Data.MasterData;
using System.Linq;
using CookBook.DataAdapter.Factory;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using System;
using System.Text.RegularExpressions;
using CookBook.DataAdapter.Factory.Repository.Transaction;
using CookBook.Data.Request;
using CookBook.DataAdapter.Factory.Repository.Common;

namespace CookBook.Business.Impl
{
    [Export(typeof(IMasterDataManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MasterDataManager : ImplBase, IMasterDataManager
    {
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public ISiteTypeRepository SiteTypeRepository { get; set; }

        [Dependency]
        public IAllergenRepository AllergenRepository { get; set; }
        [Dependency]
        public IAPLMasterRepository APLMasterRepository { get; set; }
        [Dependency]
        public ICafeRepository CafeRepository { get; set; }
        [Dependency]
        public ICapringMasterRepository CapringMasterRepository { get; set; }
        [Dependency]
        public IContainerTypeRepository ContainerTypeRepository { get; set; }
        [Dependency]
        public IApplicationSettingRepository ApplicationSettingRepository { get; set; }
        [Dependency]
        public IConceptType1Repository ConceptType1Repository { get; set; }
        [Dependency]
        public IConceptType2Repository ConceptType2Repository { get; set; }
        [Dependency]
        public IConceptType3Repository ConceptType3Repository { get; set; }
        [Dependency]
        public IConceptType4Repository ConceptType4Repository { get; set; }
        [Dependency]
        public IConceptType5Repository ConceptType5Repository { get; set; }
        [Dependency]
        public IDayPartRepository DayPartRepository { get; set; }

        [Dependency]
        public IRevenueTypeRepository RevenueTypeRepository { get; set; }

        [Dependency]
        public IDietCategoryRepository DietCategoryRepository { get; set; }
        [Dependency]
        public IReasonRepository ReasonRepository { get; set; }

        [Dependency]
        public ISubSectorRepository SubSectorRepository { get; set; }

        [Dependency]
        public IDishCategoryRepository DishCategoryRepository { get; set; }
        [Dependency]
        public IDishRecipeMappingRepository DishRecipeMappingRepository { get; set; }
        [Dependency]
        public IDishRepository DishRepository { get; set; }
        [Dependency]
        public IDishTypeRepository DishTypeRepository { get; set; }
        [Dependency]
        public IDishSubCategoryRepository DishSubCategoryRepository { get; set; }
        [Dependency]
        public IFoodProgramRepository FoodProgramRepository { get; set; }
        [Dependency]
        public IContainerRepository ContainerRepository { get; set; }
        [Dependency]
        public ISiteDishContainerMappingRepository SiteDishContainerMappingRepository { get; set; }
        [Dependency]
        public IItemDishMappingRepository ItemDishMappingRepository { get; set; }
        [Dependency]
        public IItemRepository ItemRepository { get; set; }
        [Dependency]
        public IItemType1Repository ItemType1Repository { get; set; }
        [Dependency]
        public IItemType2Repository ItemType2Repository { get; set; }
        [Dependency]
        public IMOGRepository MOGRepository { get; set; }
        [Dependency]
        public IRecipeMOGMappingRepository RecipeMOGMappingRepository { get; set; }
        [Dependency]
        public IRecipeRepository RecipeRepository { get; set; }
        [Dependency]
        public IServewareRepository ServewareRepository { get; set; }
        [Dependency]
        public IServingTemperatureRepository ServingTemperatureRepository { get; set; }
        [Dependency]
        public ISiteMasterRepository SiteMasterRepository { get; set; }

        [Dependency]
        public ISiteProfile1Repository SiteProfile1Repository { get; set; }
        [Dependency]
        public ISiteProfile2Repository SiteProfile2Repository { get; set; }
        [Dependency]
        public ISiteProfile3Repository SiteProfile3Repository { get; set; }
        [Dependency]
        public IUOMRepository UOMRepository { get; set; }
        [Dependency]
        public IUOMModuleMasterRepository UOMModuleMasterRepository { get; set; }
        [Dependency]
        public IUOMModuleMappingRepository UOMModuleMappingRepository { get; set; }
        [Dependency]
        public IReasonTypeRepository ReasonTypeRepository { get; set; }
        [Dependency]
        public IUserMasterRepository UserMasterRepository { get; set; }
        [Dependency]
        public IUserRoleMasterRepository UserRoleMasterRepository { get; set; }
        [Dependency]
        public IVisualCategoryRepository VisualCategoryRepository { get; set; }
        [Dependency]
        public IColorRepository ColorRepository { get; set; }

        [Dependency]
        public INationalMOGRepository NationalMOGRepository { get; set; }
        //Krish
        [Dependency]
        public IRangeTypeMasterRepository RangeTypeMasterRepository { get; set; }
        [Dependency]
        public IProcessTypeMasterRepository ProcessTypeMasterRepository { get; set; }
        public IList<DropDownDataModel> GetAllSiteMasterData(RequestData requestData)
        {
            List<DropDownDataModel> result = new List<DropDownDataModel>();

            return result;
        }
        public IList<SiteTypeData> GetSiteTypeDataList(RequestData requestData)
        {
            List<SiteTypeData> result = new List<SiteTypeData>();
            var entityList = SiteTypeRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<SiteType>, IList<SiteTypeData>>(entityList, result);
            return result;
        }
        public IList<AllergenData> GetAllergenDataList(RequestData requestData)
        {
            List<AllergenData> result = new List<AllergenData>();
            var entityList = AllergenRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<Allergen>, IList<AllergenData>>(entityList, result);
            return result;
        }
        public List<APLMasterData> GetAPLMasterDataList(string articleNumber, string mogCode, RequestData requestData)
        {
            List<APLMasterData> result = new List<APLMasterData>();
            var entityList = APLMasterRepository.GetAPLMasterDataList(articleNumber, mogCode, requestData).ToList();
            EntityMapper.Map<IList<procGetAPLMasterData_Result>, IList<APLMasterData>>(entityList, result);
            return result;
        }

        public List<APLMasterData> GetAPLMasterDataListForSector(string articleNumber, string mogCode, RequestData requestData)
        {
            List<APLMasterData> result = new List<APLMasterData>();
            var entityList = APLMasterRepository.GetAPLMasterDataListForSector(articleNumber, mogCode, requestData).ToList();
            EntityMapper.Map<IList<procGetAPLMasterDataforSector_Result>, IList<APLMasterData>>(entityList, result);
            return result;
        }


        public List<APLMasterData> NationalGetAPLMasterDataList(RequestData requestData)
        {
            List<APLMasterData> result = new List<APLMasterData>();
            var entityList = APLMasterRepository.NationalGetAPLMasterDataList(requestData).ToList();
            EntityMapper.Map<IList<procNationalGetAPLMasterData_Result>, IList<APLMasterData>>(entityList, result);
            return result;
        }

        public List<APLMasterData> GetAPLMasterDataListPageing(RequestData requestData, int pageindex)
        {
            List<APLMasterData> result = new List<APLMasterData>();
            var entityList = APLMasterRepository.GetAPLMasterDataListPageing(requestData, pageindex).ToList();
            EntityMapper.Map<IList<procGetAPLsPageWiseDataList_Result>, IList<APLMasterData>>(entityList, result);
            return result;
        }

        public List<APLMasterData> GetNationalAPLExcelList(RequestData requestData)
        {
            List<APLMasterData> result = new List<APLMasterData>();
            var entityList = APLMasterRepository.GetNationalAPLExcelList(requestData).ToList();
            EntityMapper.Map<IList<procGetNationalAPLMaster_Result>, IList<APLMasterData>>(entityList, result);
            return result;
        }


        public IList<APLMOGDATA> GetNOTMappedMOGDataListAPL(RequestData requestData)
        {
            List<APLMOGDATA> result = new List<APLMOGDATA>();
            var entityList = APLMasterRepository.GetNOTMappedMOGDataListAPL(requestData).ToList();
            EntityMapper.Map<IList<procGetAPLMasterMOGCode_Result>, IList<APLMOGDATA>>(entityList, result);
            return result;
        }

        public string UpdateMOGLISTAPL(RequestData request, string xml, string username)
        {
            List<APLMOGDATA> result = new List<APLMOGDATA>();
            string entityList = APLMasterRepository.UpdateMOGLISTAPL(request, xml, username).ToString();
            return entityList;
        }

        public List<APLMAPPEDHISTORYDLIST> GetMOGAPLHISTORYDetAPL(RequestData request, string username)
        {
            List<APLMAPPEDHISTORYDLIST> result = new List<APLMAPPEDHISTORYDLIST>();
            var entityList = APLMasterRepository.GetMOGAPLHISTORYDetAPL(request, username).ToList();
            EntityMapper.Map<List<procshowMOGAPLUPLOADDETAILSAPL_Result>, IList<APLMAPPEDHISTORYDLIST>>(entityList, result);

            return result;
        }

        //public List<CuisineMaster> GetCuisineMaster(RequestData request)
        //{
        //    List<CuisineMaster> result = new List<CuisineMaster>();
        //    var entityList = APLMasterRepository.GetCuisineMaster(request).ToList();
        //    EntityMapper.Map<List<procCuisineMasterList_Result>, IList<CuisineMaster>>(entityList, result);
        //    return result;
        //}

        public List<APLMAPPEDHISTORYDLIST> GetMOGAPLHISTORYAPL(RequestData request, string username)
        {
            List<APLMAPPEDHISTORYDLIST> result = new List<APLMAPPEDHISTORYDLIST>();
            var entityList = APLMasterRepository.GetMOGAPLHISTORYAPL(request, username).ToList();
            EntityMapper.Map<List<ProcGetMOGBATCHDETAILSAPL_Result>, IList<APLMAPPEDHISTORYDLIST>>(entityList, result);

            return result;
        }

        public List<APLMAPPEDHISTORYDLIST> GetshowGetALLHISTORYBATCHWISEAPL(RequestData request, string username, string batchno)
        {
            List<APLMAPPEDHISTORYDLIST> result = new List<APLMAPPEDHISTORYDLIST>();
            var entityList = APLMasterRepository.GetshowGetALLHISTORYBATCHWISEAPL(request, username, batchno).ToList();
            EntityMapper.Map<List<procshowGetALLHISTORYBATCHWISEAPL_Result>, IList<APLMAPPEDHISTORYDLIST>>(entityList, result);

            return result;
        }

        public List<APLMAPPEDHISTORYDLIST> GetALLHISTORYDetailsAPL(RequestData request, string username)
        {
            List<APLMAPPEDHISTORYDLIST> result = new List<APLMAPPEDHISTORYDLIST>();
            var entityList = APLMasterRepository.GetALLHISTORYDetailsAPL(request, username).ToList();
            EntityMapper.Map<List<procshowGetALLHISTORYAPL_Result>, IList<APLMAPPEDHISTORYDLIST>>(entityList, result);

            return result;
        }



        public IList<APLMasterData> GetAPLMasterRegionDataList(string articleNumber, string mogCode, int Region_ID, RequestData requestData)
        {
            List<APLMasterData> result = new List<APLMasterData>();
            var entityList = RecipeRepository.GetAPLMasterRegionDataList(articleNumber, mogCode, Region_ID, requestData).ToList();
            EntityMapper.Map<IList<procGetAPLMasterDataForRegion_Result>, IList<APLMasterData>>(entityList, result);
            return result;
        }
        public IList<APLMasterData> GetAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, RequestData requestData)
        {
            List<APLMasterData> result = new List<APLMasterData>();
            var entityList = RecipeRepository.GetAPLMasterSiteDataList(articleNumber, mogCode, SiteCode, requestData).ToList();
            EntityMapper.Map<IList<procGetAPLMasterDataForSite_Result>, IList<APLMasterData>>(entityList, result);
            return result;
        }
        public IList<ConceptType1Data> GetConceptType1DataList(RequestData requestData)
        {
            List<ConceptType1Data> result = new List<ConceptType1Data>();
            var entityList = ConceptType1Repository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<ConceptType1>, IList<ConceptType1Data>>(entityList, result);
            return result;
        }
        public IList<ConceptType2Data> GetConceptType2DataList(RequestData requestData)
        {
            List<ConceptType2Data> result = new List<ConceptType2Data>();
            var entityList = ConceptType2Repository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<ConceptType2>, IList<ConceptType2Data>>(entityList, result);
            return result;
        }
        public IList<ConceptType3Data> GetConceptType3DataList(RequestData requestData)
        {
            List<ConceptType3Data> result = new List<ConceptType3Data>();
            var entityList = ConceptType3Repository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<ConceptType3>, IList<ConceptType3Data>>(entityList, result);
            return result;
        }
        public IList<ConceptType4Data> GetConceptType4DataList(RequestData requestData)
        {
            List<ConceptType4Data> result = new List<ConceptType4Data>();
            var entityList = ConceptType4Repository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<ConceptType4>, IList<ConceptType4Data>>(entityList, result);
            return result;
        }
        public IList<ConceptType5Data> GetConceptType5DataList(RequestData requestData)
        {
            List<ConceptType5Data> result = new List<ConceptType5Data>();
            var entityList = ConceptType5Repository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<ConceptType5>, IList<ConceptType5Data>>(entityList, result);
            return result;
        }
        public IList<DayPartData> GetDayPartDataList(RequestData requestData)
        {
            List<DayPartData> result = new List<DayPartData>();
            var entityList = DayPartRepository.GetAll(requestData.SessionSectorName).OrderBy(m => m.SequenceNumber).ToList();
            EntityMapper.Map<IList<DayPart>, IList<DayPartData>>(entityList, result);
            return result;
        }

        public IList<RevenueTypeData> GetRevenueTypeDataList(RequestData requestData)
        {
            List<RevenueTypeData> result = new List<RevenueTypeData>();
            var entityList = RevenueTypeRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<RevenueType>, IList<RevenueTypeData>>(entityList, result);
            return result;
        }
        public IList<DietCategoryData> GetDietCategoryDataList(RequestData requestData)
        {
            List<DietCategoryData> result = new List<DietCategoryData>();
            var entityList = DietCategoryRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<DietCategory>, IList<DietCategoryData>>(entityList, result);
            return result;
        }
        public IList<ReasonData> GetReasonDataList(RequestData requestData)
        {
            List<ReasonData> result = new List<ReasonData>();
            var entityList = ReasonRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<Reason>, IList<ReasonData>>(entityList, result);
            return result;
        }
        public IList<SubSectorData> GetSubSectorDataList(RequestData requestData)
        {
            List<SubSectorData> result = new List<SubSectorData>();
            var entityList = SubSectorRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<SubSectorMaster>, IList<SubSectorData>>(entityList, result);
            return result;
        }
        public IList<DishCategoryData> GetDishCategoryDataList(RequestData requestData)
        {
            //List<DishCategoryData> result = new List<DishCategoryData>();
            //var entityList = DishCategoryRepository.GetAll(requestData.SessionSectorName).ToList();
            //EntityMapper.Map<IList<DishCategory>, IList<DishCategoryData>>(entityList, result);

            List<DishCategoryData> result = new List<DishCategoryData>();
            var entityList = DishCategoryRepository.GetDishCategoryDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetDishCategoryList_Result>, IList<DishCategoryData>>(entityList, result);
            return result;
        }


        public IList<NationalDishCategoryData> NationalGetDishCategoryDataList(RequestData requestData)
        {
            List<NationalDishCategoryData> result = new List<NationalDishCategoryData>();
            var entityList = APLMasterRepository.NationalGetDishCategoryDataList(requestData).ToList();
            EntityMapper.Map<IList<ProcShowNationalDishCategory_Result>, IList<NationalDishCategoryData>>(entityList, result);
            return result;


        }

        public IList<NationalDishData> NationalGetDishDataList(RequestData requestData)
        {
            List<NationalDishData> result = new List<NationalDishData>();
            var entityList = APLMasterRepository.NationalGetDishesDataList(requestData).ToList();
            EntityMapper.Map<IList<ProcShowNationalDishes_Result>, IList<NationalDishData>>(entityList, result);
            return result;


        }

        public IList<DishData> GetDishDataList(RequestData requestData)
        {
            List<DishData> result = new List<DishData>();
            
            var entityList = DishRepository.GetAll(requestData.SessionSectorName).ToList();
            
            EntityMapper.Map<IList<Dish>, IList<DishData>>(entityList, result);
            return result;
        }
        public IList<DishRecipeMappingData> GetDishRecipeMappingDataList(RequestData requestData)
        {
            List<DishRecipeMappingData> result = new List<DishRecipeMappingData>();
            var entityList = DishRecipeMappingRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<DishRecipeMapping>, IList<DishRecipeMappingData>>(entityList, result);
            return result;
        }
        public IList<DishSubCategoryData> GetDishSubCategoryDataList(RequestData requestData)
        {
            List<DishSubCategoryData> result = new List<DishSubCategoryData>();
            var entityList = DishSubCategoryRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<DishSubCategory>, IList<DishSubCategoryData>>(entityList, result);
            return result;
        }
        public IList<DishTypeData> GetDishTypeDataList(RequestData requestData)
        {
            List<DishTypeData> result = new List<DishTypeData>();
            var entityList = DishTypeRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<DishType>, IList<DishTypeData>>(entityList, result);
            return result;
        }
        public IList<FoodProgramData> GetFoodProgramDataList(RequestData requestData)
        {
            List<FoodProgramData> result = new List<FoodProgramData>();
            var entityList = FoodProgramRepository.GetFoodProgramDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetFoodProgramList_Result>, IList<FoodProgramData>>(entityList, result);
            return result;
        }

        public IList<ContainerData> GetContainerDataList(RequestData requestData)
        {
            List<ContainerData> result = new List<ContainerData>();
            var entityList = ContainerRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<Container>, IList<ContainerData>>(entityList, result);
            return result;
        }
        public IList<ItemData> GetItemDataList(RequestData requestData)
        {
            List<ItemData> result = new List<ItemData>();
            var entityList = ItemRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<Item>, IList<ItemData>>(entityList, result);
            return result;
        }
        public IList<ItemDishMappingData> GetItemDishMappingDataList(RequestData requestData)
        {
            List<ItemDishMappingData> result = new List<ItemDishMappingData>();
            var entityList = ItemDishMappingRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<ItemDishMapping>, IList<ItemDishMappingData>>(entityList, result);
            return result;
        }
        public IList<ItemType1Data> GetItemType1DataList(RequestData requestData)
        {
            List<ItemType1Data> result = new List<ItemType1Data>();
            var entityList = ItemType1Repository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<ItemType1>, IList<ItemType1Data>>(entityList, result);
            return result;
        }
        public IList<ItemType2Data> GetItemType2DataList(RequestData requestData)
        {
            List<ItemType2Data> result = new List<ItemType2Data>();
            var entityList = ItemType2Repository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<ItemType2>, IList<ItemType2Data>>(entityList, result);
            return result;
        }
        public IList<MOGData> GetMOGDataList(RequestData requestData)
        {
            List<MOGData> result = new List<MOGData>();
            var entityList = MOGRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<MOG>, IList<MOGData>>(entityList, result);
            return result;
        }

        public IList<RecipeData> GetRecipeDataList(RequestData requestData)
        {
            List<RecipeData> result = new List<RecipeData>();
            var entityList = RecipeRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<Recipe>, IList<RecipeData>>(entityList, result);
            return result;
        }


        public IList<RecipeMOGMappingData> GetRecipeMOGMappingDataList(RequestData requestData)
        {
            List<RecipeMOGMappingData> result = new List<RecipeMOGMappingData>();
            var entityList = RecipeMOGMappingRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<RecipeMOGMapping>, IList<RecipeMOGMappingData>>(entityList, result);
            return result;
        }
        public IList<ServewareData> GetServewareDataList(RequestData requestData)
        {
            List<ServewareData> result = new List<ServewareData>();
            var entityList = ServewareRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<Serveware>, IList<ServewareData>>(entityList, result);
            return result;
        }
        public IList<ServingTemperatureData> GetServingTemperatureDataList(RequestData requestData)
        {
            List<ServingTemperatureData> result = new List<ServingTemperatureData>();
            var entityList = ServingTemperatureRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<ServingTemperature>, IList<ServingTemperatureData>>(entityList, result);
            return result;
        }

        public IList<SiteMasterData> GetSiteMasterDataList(RequestData requestData)
        {
            List<SiteMasterData> result = new List<SiteMasterData>();
            var entityList = SiteMasterRepository.GetSiteDataList(requestData).Where(a => a.SiteOrg == "IN10").OrderBy(a => a.SiteName).ToList();
            EntityMapper.Map<IList<procGetSiteList_Result>, IList<SiteMasterData>>(entityList, result);
            return result;
        }
        //Krish
        public IList<SiteMasterData> GetSiteByRegionDataList(int regionId, RequestData requestData)
        {
            List<SiteMasterData> result = new List<SiteMasterData>();
            var entityList = SiteMasterRepository.GetSiteByRegionDataList(regionId, requestData).Where(a => a.SiteOrg == "IN10").OrderBy(a => a.SiteName).ToList();
            EntityMapper.Map<IList<procGetSiteByRegionList_Result>, IList<SiteMasterData>>(entityList, result);
            return result;
        }
        public IList<SiteMasterData> NationalGetSiteMasterDataList(RequestData requestData)
        {
            List<SiteMasterData> result = new List<SiteMasterData>();
            var entityList = SiteMasterRepository.NationalGetSiteMasterDataList(requestData).Where(a => a.SiteOrg == "IN10").OrderBy(a => a.SiteName).ToList();
            EntityMapper.Map<IList<NationalprocGetSiteList_Result>, IList<SiteMasterData>>(entityList, result);
            return result;
        }
        public IList<RegionMasterData> GetRegionMasterDataList(RequestData requestData)
        {
            List<RegionMasterData> result = new List<RegionMasterData>();
            var entityList = SiteMasterRepository.GetRegionMasterDataList(requestData).ToList();
            EntityMapper.Map<IList<procshowregionmaster_Result>, IList<RegionMasterData>>(entityList, result);
            return result;
        }

        public IList<SiteProfile1Data> GetSiteProfile1DataList(RequestData requestData)
        {
            List<SiteProfile1Data> result = new List<SiteProfile1Data>();
            var entityList = SiteProfile1Repository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<SiteProfile1>, IList<SiteProfile1Data>>(entityList, result);
            return result;
        }
        public IList<SiteProfile2Data> GetSiteProfile2DataList(RequestData requestData)
        {
            List<SiteProfile2Data> result = new List<SiteProfile2Data>();
            var entityList = SiteProfile2Repository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<SiteProfile2>, IList<SiteProfile2Data>>(entityList, result);
            return result;
        }
        public IList<SiteProfile3Data> GetSiteProfile3DataList(RequestData requestData)
        {
            List<SiteProfile3Data> result = new List<SiteProfile3Data>();
            var entityList = SiteProfile3Repository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<SiteProfile3>, IList<SiteProfile3Data>>(entityList, result);
            return result;
        }
        public IList<UOMData> GetUOMDataList(RequestData requestData)
        {
            List<UOMData> result = new List<UOMData>();
            var entityList = UOMRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<UOM>, IList<UOMData>>(entityList, result);
            return result;
        }

        public IList<UOMModuleMappingData> GetUOMModuleMappingDataList(RequestData requestData)
        {
            List<UOMModuleMappingData> result = new List<UOMModuleMappingData>();
            var entityList = UOMRepository.GetUOMModuleMappingDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetUOMModuleMappingList_Result>, IList<UOMModuleMappingData>>(entityList, result);
            return result;
        }


        public IList<GetUOMModuleMappingGridData> procGetUOMModuleMappingGridData(RequestData requestData)
        {
            List<GetUOMModuleMappingGridData> result = new List<GetUOMModuleMappingGridData>();
            var entityList = UOMRepository.procGetUOMModuleMappingGridData(requestData).ToList();
            EntityMapper.Map<IList<procGetUOMModuleMappingGridData_Result>, IList<GetUOMModuleMappingGridData>>(entityList, result);
            return result;
        }

        public IList<UOMModuleData> GetUOMModuleDataList(RequestData requestData)
        {
            List<UOMModuleData> result = new List<UOMModuleData>();
            var entityList = UOMModuleMasterRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<UOMModuleMaster>, IList<UOMModuleData>>(entityList, result);
            return result;
        }

        public IList<ReasonTypeData> GetReasonTypeDataList(RequestData requestData)
        {
            List<ReasonTypeData> result = new List<ReasonTypeData>();
            var entityList = ReasonTypeRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<ReasonType>, IList<ReasonTypeData>>(entityList, result);
            return result;
        }

        public IList<VisualCategoryData> GetVisualCategoryDataList(RequestData requestData)
        {
            List<VisualCategoryData> result = new List<VisualCategoryData>();
            var entityList = VisualCategoryRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<VisualCategory>, IList<VisualCategoryData>>(entityList, result);
            return result;
        }

        public IList<CPUData> GetCPUDataList(RequestData requestData)
        {
            List<CPUData> result = new List<CPUData>();
            var entityList = SiteMasterRepository.GetCPUDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetCPUList_Result>, IList<CPUData>>(entityList, result);
            return result;
        }

        public IList<DKData> GetDKDataList(RequestData requestData)
        {
            List<DKData> result = new List<DKData>();
            var entityList = SiteMasterRepository.GetDKDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetDKList_Result>, IList<DKData>>(entityList, result);
            return result;
        }

        public IList<CuisineMasterData> GetCuisineDataList(RequestData requestData)
        {
            List<CuisineMasterData> result = new List<CuisineMasterData>();
            var entityList = SiteMasterRepository.GetCuisineDataList(requestData).ToList();
            EntityMapper.Map<IList<CuisineMaster>, IList<CuisineMasterData>>(entityList, result);
            return result;
        }


        public string SaveCuisine(RequestData requestData, CuisineMasterData model, int username)
        {

            string result = SiteMasterRepository.SaveCuisine(requestData, model, username).ToString();

            return result;
        }

        public string UpdateCuisine(RequestData requestData, CuisineMasterData model, int username)
        {

            string result = SiteMasterRepository.UpdateCuisine(requestData, model, username).ToString();

            return result;
        }

        public IList<LifeStyleTagMasterData> GetLifeStyleTagDataList(RequestData requestData)
        {
            List<LifeStyleTagMasterData> result = new List<LifeStyleTagMasterData>();
            var entityList = SiteMasterRepository.GetLifeStyleTagDataList(requestData).ToList();
            EntityMapper.Map<IList<LifeStyleTagMaster>, IList<LifeStyleTagMasterData>>(entityList, result);
            return result;
        }


        public string SaveLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username)
        {

            string result = SiteMasterRepository.SaveLifeStyleTag(requestData, model, username).ToString();

            return result;
        }

        public string UpdateLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username)
        {

            string result = SiteMasterRepository.UpdateLifeStyleTag(requestData, model, username).ToString();

            return result;
        }

        public IList<HealthTagMasterData> GetHealthTagDataList(RequestData requestData)
        {
            List<HealthTagMasterData> result = new List<HealthTagMasterData>();
            var entityList = SiteMasterRepository.GetHealthTagDataList(requestData).ToList();
            EntityMapper.Map<IList<HealthTagMaster>, IList<HealthTagMasterData>>(entityList, result);
            return result;
        }


        public string SaveHealthTag(RequestData requestData, HealthTagMasterData model, int username)
        {

            string result = SiteMasterRepository.SaveHealthTag(requestData, model, username).ToString();

            return result;
        }

        public string UpdateHealthTag(RequestData requestData, HealthTagMasterData model, int username)
        {

            string result = SiteMasterRepository.UpdateHealthTag(requestData, model, username).ToString();

            return result;
        }


        public IList<CafeData> GetCafeDataList(RequestData requestData)
        {
            List<CafeData> result = new List<CafeData>();
            var entityList = CafeRepository.GetCafeDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetCafeList_Result>, IList<CafeData>>(entityList, result);
            return result;
        }


        public IList<CapringMasterData> GetCapringMasterDataList(RequestData requestData)
        {
            List<CapringMasterData> result = new List<CapringMasterData>();
            var entityList = CapringMasterRepository.GetCapringMasterDataList(requestData).ToList();
            EntityMapper.Map<IList<CapringMaster>, IList<CapringMasterData>>(entityList, result);
            return result;
        }

        public IList<ContainerTypeData> GetContainerTypeDataList(RequestData requestData)
        {
            List<ContainerTypeData> result = new List<ContainerTypeData>();
            var entityList = ContainerTypeRepository.GetContainerTypeDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetContainerType_Result>, IList<ContainerTypeData>>(entityList, result);
            return result;
        }
        public IList<ApplicationSettingData> GetApplicationSettingDataList(RequestData requestData)
        {
            List<ApplicationSettingData> result = new List<ApplicationSettingData>();
            var entityList = ApplicationSettingRepository.GetAll(requestData.SessionSectorName).ToList();
            EntityMapper.Map<IList<ApplicationSetting>, IList<ApplicationSettingData>>(entityList, result);
            return result;
        }

        public IList<ColorData> GetColorDataList(RequestData requestData)
        {
            List<ColorData> result = new List<ColorData>();
            var entityList = ColorRepository.GetAll(requestData.SessionSectorName).OrderBy(x => x.RGBCode).ToList();
            EntityMapper.Map<IList<Color>, IList<ColorData>>(entityList, result);
            return result;
        }
        public string SaveFoodProgramData(FoodProgramData model, RequestData requestData)
        {
            FoodProgram entity = new FoodProgram();

            if (model.ID == 0)
            {
                var code = FoodProgramRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.FoodProgramCode).FirstOrDefault()?.FoodProgramCode;
                if (code is null)
                {
                    code = "FPC-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.FoodProgramCode = code;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
            }
            EntityMapper.Map<FoodProgramData, FoodProgram>(model, entity);
            var dup = FoodProgramRepository.Find(y => y.Name.Trim().ToLower() == model.Name.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
            if (dup != null && model.ID == 0)
                return "Duplicate";

            bool x = FoodProgramRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveContainerData(ContainerData model, RequestData requestData)
        {
            Container entity = new Container();

            if (model.ID == 0)
            {
                var code = ContainerRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ContainerCode).FirstOrDefault()?.ContainerCode;
                if (code is null)
                {
                    code = "CON-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ContainerCode = code;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
            }
            EntityMapper.Map<ContainerData, Container>(model, entity);
            var dup = ContainerRepository.Find(y => y.Name.Trim().ToLower() == model.Name.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
            if (dup != null && model.ID == 0)
                return "Duplicate";

            bool x = ContainerRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveVisualCategoryData(VisualCategoryData model, RequestData requestData)
        {
            VisualCategory entity = new VisualCategory();
            if (model.ID == 0)
            {
                var code = VisualCategoryRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.VisualCategoryCode).FirstOrDefault()?.VisualCategoryCode;
                if (code is null)
                {
                    code = "VCC-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.VisualCategoryCode = code;
            }
            EntityMapper.Map<VisualCategoryData, VisualCategory>(model, entity);
            bool x = VisualCategoryRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveDishCategoryData(DishCategoryData model, RequestData requestData)
        {
            DishCategory entity = new DishCategory();


            if (model.ID == 0)
            {
                var code = DishCategoryRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.DishCategoryCode).FirstOrDefault()?.DishCategoryCode;
                if (code is null)
                {
                    if (requestData.SessionSectorNumber == "10")
                    {
                        if (model.SubSectorCode == "SSR-001")
                        {
                            code = "OCC-00001";
                        }
                        else if (model.SubSectorCode == "SSR-002")
                        {
                            code = "GCC-00001";
                        }
                        else if (model.SubSectorCode == "SSR-003")
                        {
                            code = "VCC-00001";
                        }
                        else
                        {
                            code = "DCC-00001";
                        }
                    }
                    else
                    {
                        code = "DCC-00001";
                    }

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.DishCategoryCode = code;
            }
            if (string.IsNullOrWhiteSpace(model.DishCategoryCode))
            {
                model.CreatedBy = requestData.SessionUserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedBy = requestData.SessionUserId;
                model.ModifiedOn = DateTime.Now;
            }
            EntityMapper.Map<DishCategoryData, DishCategory>(model, entity);
            if (entity.DishSubCategory_ID <= 0)
                entity.DishSubCategory_ID = null;

            bool x = DishCategoryRepository.Save(entity, requestData.SessionSectorName);
            DishCategoryRepository.SaveDishCategorySiblingMapping(model, requestData);
            return x.ToString();
        }

        public string SaveDishSubCategoryData(DishSubCategoryData model, RequestData requestData)
        {
            DishSubCategory entity = new DishSubCategory();
            if (model.ID == 0)
            {
                var code = DishSubCategoryRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.DishSubCategoryCode).FirstOrDefault()?.DishSubCategoryCode;
                if (code is null)
                {
                    code = "DSC-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.DishSubCategoryCode = code;
            }
            EntityMapper.Map<DishSubCategoryData, DishSubCategory>(model, entity);
            bool x = DishSubCategoryRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveConceptType1Data(ConceptType1Data model, RequestData requestData)
        {
            ConceptType1 entity = new ConceptType1();
            if (model.ID == 0)
            {
                var code = ConceptType1Repository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ConceptType1Code).FirstOrDefault()?.ConceptType1Code;
                if (code is null)
                {
                    code = "CT1-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ConceptType1Code = code;
            }
            EntityMapper.Map<ConceptType1Data, ConceptType1>(model, entity);
            bool x = ConceptType1Repository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveConceptType2Data(ConceptType2Data model, RequestData requestData)
        {
            ConceptType2 entity = new ConceptType2();
            if (model.ID == 0)
            {
                var code = ConceptType2Repository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ConceptType2Code).FirstOrDefault()?.ConceptType2Code;
                if (code is null)
                {
                    code = "CT2-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ConceptType2Code = code;
            }

            EntityMapper.Map<ConceptType2Data, ConceptType2>(model, entity);
            bool x = ConceptType2Repository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveConceptType3Data(ConceptType3Data model, RequestData requestData)
        {
            ConceptType3 entity = new ConceptType3();
            if (model.ID == 0)
            {
                var code = ConceptType3Repository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ConceptType3Code).FirstOrDefault()?.ConceptType3Code;
                if (code is null)
                {
                    code = "CT3-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ConceptType3Code = code;
            }

            EntityMapper.Map<ConceptType3Data, ConceptType3>(model, entity);
            bool x = ConceptType3Repository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveConceptType4Data(ConceptType4Data model, RequestData requestData)
        {
            ConceptType4 entity = new ConceptType4();
            if (model.ID == 0)
            {
                var code = ConceptType4Repository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ConceptType4Code).FirstOrDefault()?.ConceptType4Code;
                if (code is null)
                {
                    code = "CT4-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ConceptType4Code = code;
            }

            EntityMapper.Map<ConceptType4Data, ConceptType4>(model, entity);
            bool x = ConceptType4Repository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveConceptType5Data(ConceptType5Data model, RequestData requestData)
        {
            ConceptType5 entity = new ConceptType5();
            if (model.ID == 0)
            {
                var code = ConceptType5Repository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ConceptType5Code).FirstOrDefault()?.ConceptType5Code;
                if (code is null)
                {
                    code = "CT5-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ConceptType5Code = code;
            }

            EntityMapper.Map<ConceptType5Data, ConceptType5>(model, entity);
            bool x = ConceptType5Repository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveSiteProfile1Data(SiteProfile1Data model, RequestData requestData)
        {
            SiteProfile1 entity = new SiteProfile1();
            if (model.ID == 0)
            {
                var code = SiteProfile1Repository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.SiteProfile1Code).FirstOrDefault()?.SiteProfile1Code;
                if (code is null)
                {
                    code = "SP1-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.SiteProfile1Code = code;
            }
            EntityMapper.Map<SiteProfile1Data, SiteProfile1>(model, entity);
            bool x = SiteProfile1Repository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveSiteProfile2Data(SiteProfile2Data model, RequestData requestData)
        {
            SiteProfile2 entity = new SiteProfile2();
            if (model.ID == 0)
            {
                var code = SiteProfile2Repository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.SiteProfile2Code).FirstOrDefault()?.SiteProfile2Code;
                if (code is null)
                {
                    code = "SP2-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.SiteProfile2Code = code;
            }
            EntityMapper.Map<SiteProfile2Data, SiteProfile2>(model, entity);
            bool x = SiteProfile2Repository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveSiteProfile3Data(SiteProfile3Data model, RequestData requestData)
        {
            SiteProfile3 entity = new SiteProfile3();
            if (model.ID == 0)
            {
                var code = SiteProfile3Repository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.SiteProfile3Code).FirstOrDefault()?.SiteProfile3Code;
                if (code is null)
                {
                    code = "SP3-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.SiteProfile3Code = code;
            }
            EntityMapper.Map<SiteProfile3Data, SiteProfile3>(model, entity);
            bool x = SiteProfile3Repository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveItemType1Data(ItemType1Data model, RequestData requestData)
        {
            ItemType1 entity = new ItemType1();
            if (model.ID == 0)
            {
                var code = ItemType1Repository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ItemType1Code).FirstOrDefault()?.ItemType1Code;
                if (code is null)
                {
                    code = "IT1-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ItemType1Code = code;
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = requestData.SessionUserId;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = requestData.SessionUserId;
            }
            EntityMapper.Map<ItemType1Data, ItemType1>(model, entity);
            bool x = ItemType1Repository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveItemType2Data(ItemType2Data model, RequestData requestData)
        {
            ItemType2 entity = new ItemType2();
            if (model.ID == 0)
            {
                var code = ItemType2Repository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ItemType2Code).FirstOrDefault()?.ItemType2Code;
                if (code is null)
                {
                    code = "IT2-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ItemType2Code = code;
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = requestData.SessionUserId;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = requestData.SessionUserId;
            }
            EntityMapper.Map<ItemType2Data, ItemType2>(model, entity);
            bool x = ItemType2Repository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveDietCategoryData(DietCategoryData model, RequestData requestData)
        {
            DietCategory entity = new DietCategory();
            if (model.ID == 0)
            {
                var code = DietCategoryRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.DietCategoryCode).FirstOrDefault()?.DietCategoryCode;
                if (code is null)
                {
                    code = "DTC-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.DietCategoryCode = code;
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = requestData.SessionUserId;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = requestData.SessionUserId;
            }
            EntityMapper.Map<DietCategoryData, DietCategory>(model, entity);
            bool x = DietCategoryRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveDayPartData(DayPartData model, RequestData requestData)
        {
            DayPart entity = new DayPart();
            if (model.ID == 0)
            {
                var code = DayPartRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.DayPartCode).FirstOrDefault()?.DayPartCode;
                if (code is null)
                {
                    code = "DPC-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.DayPartCode = code;
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = requestData.SessionUserId;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = requestData.SessionUserId;
            }
            EntityMapper.Map<DayPartData, DayPart>(model, entity);
            bool x = DayPartRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }
        public string SaveReasonData(ReasonData model, RequestData requestData)
        {
            Reason entity = new Reason();
            if (model.ID == 0)
            {
                var code = ReasonRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ReasonCode).FirstOrDefault()?.ReasonCode;
                if (code is null)
                {
                    code = "RSN-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ReasonCode = code;
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = requestData.SessionUserId;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = requestData.SessionUserId;
            }
            EntityMapper.Map<ReasonData, Reason>(model, entity);
            bool x = ReasonRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }
        public string SaveRevenueTypeData(RevenueTypeData model, RequestData requestData)
        {
            RevenueType entity = new RevenueType();
            if (model.ID == 0)
            {
                var code = RevenueTypeRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.RevenueTypeCode).FirstOrDefault()?.RevenueTypeCode;
                if (code is null)
                {
                    code = "RVT-00001";
                }
                else
                {

                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');

                }


                model.RevenueTypeCode = code;
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = requestData.SessionUserId;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = requestData.SessionUserId;
            }
            EntityMapper.Map<RevenueTypeData, RevenueType>(model, entity);
            bool x = RevenueTypeRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveServewareData(ServewareData model, RequestData requestData)
        {
            Serveware entity = new Serveware();
            if (model.ID == 0)
            {
                var code = ServewareRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ServewareCode).FirstOrDefault()?.ServewareCode;
                if (code is null)
                {
                    code = "SWC-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ServewareCode = code;
            }
            EntityMapper.Map<ServewareData, Serveware>(model, entity);
            bool x = ServewareRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }
        public string SaveSiteTypeData(SiteTypeData model, RequestData requestData)
        {
            SiteType entity = new SiteType();
            if (model.ID == 0)
            {

                var code = SiteTypeRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.SiteTypeCode).FirstOrDefault()?.SiteTypeCode;
                if (code is null)
                {
                    code = "STP-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.SiteTypeCode = code;
            }
            EntityMapper.Map<SiteTypeData, SiteType>(model, entity);
            bool x = SiteTypeRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveAllergenData(AllergenData model, RequestData requestData)
        {
            Allergen entity = new Allergen();
            if (model.ID == 0)
            {

                var code = AllergenRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.AllergenCode).FirstOrDefault()?.AllergenCode;
                if (code is null)
                {
                    code = "ALG-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.AllergenCode = code;
            }
            EntityMapper.Map<AllergenData, Allergen>(model, entity);
            bool x = AllergenRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveAPLMasterData(APLMasterData model, RequestData requestData)
        {
            UniqueAPLMaster entity = new UniqueAPLMaster();
            EntityMapper.Map(model, entity);
            string x = APLMasterRepository.SaveAPLMasterData(entity, model.MOGStatus, model.AllergensCode, requestData);
            return x.ToString();
        }

        public string SaveAPLMasterDataList(IList<APLMasterData> model, RequestData requestData)
        {
            List<UniqueAPLMaster> entity = new List<UniqueAPLMaster>();
            EntityMapper.Map<IList<APLMasterData>, IList<UniqueAPLMaster>>(model, entity);
            string x = APLMasterRepository.SaveAPLMasterDataList(entity, model[0].MOGStatus, requestData);
            return x;
        }

        public string SaveUOMData(UOMData model, RequestData requestData)
        {
            UOM entity = new UOM();
            if (model.ID == 0)
            {
                var code = UOMRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.UOMCode).FirstOrDefault()?.UOMCode;
                if (code is null)
                {
                    code = "UOM-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.UOMCode = code;
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = requestData.SessionUserId;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = requestData.SessionUserId;
            }
            EntityMapper.Map<UOMData, UOM>(model, entity);
            var dup = UOMRepository.Find(y => y.Name.Trim().ToLower() == model.Name.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
            if (dup != null && model.ID == 0)
                return "Duplicate";
            bool x = UOMRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveUOMModuleData(UOMModuleData model, RequestData requestData)
        {
            UOMModuleMaster entity = new UOMModuleMaster();
            if (model.ID == 0)
            {
                var code = UOMModuleMasterRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.UOMModuleCode).FirstOrDefault()?.UOMModuleCode;
                if (code is null)
                {
                    code = "UMM-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.UOMModuleCode = code;
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = requestData.SessionUserId;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = requestData.SessionUserId;
            }
            EntityMapper.Map<UOMModuleData, UOMModuleMaster>(model, entity);
            var dup = UOMModuleMasterRepository.Find(y => y.Name.Trim().ToLower() == model.Name.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
            if (dup != null && model.ID == 0)
                return "Duplicate";
            bool x = UOMModuleMasterRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveUOMModuleMappingData(UOMModuleMappingData model, RequestData requestData)
        {
            var existinghList = UOMModuleMappingRepository.Find(item => item.UOMModuleCode == model.UOMModuleCode, requestData.SessionSectorName);
            foreach (UOMModuleMapping item in existinghList)
            {
                item.ModifiedOn = DateTime.Now;
                item.ModifiedBy = model.CreatedBy;
                item.IsActive = false;
                UOMModuleMappingRepository.Save(item, requestData.SessionSectorName);
            }
            if (!string.IsNullOrWhiteSpace(model.UOMCode))
            {
                var dataArray = model.UOMCode.Split(',');

                foreach (var uom in dataArray)
                {
                    var UOMModuleMapping = new UOMModuleMapping();
                    UOMModuleMapping.UOMCode = uom;
                    UOMModuleMapping.UOMModuleCode = model.UOMModuleCode;
                    UOMModuleMapping.IsActive = true;
                    var isItemExists = UOMModuleMappingRepository.Find(item => item.UOMCode == uom && item.UOMModuleCode == model.UOMModuleCode, requestData.SessionSectorName).FirstOrDefault();
                    if (isItemExists != null && isItemExists.ID > 0)
                    {
                        UOMModuleMapping.ModifiedOn = DateTime.Now;
                        UOMModuleMapping.ModifiedBy = model.CreatedBy;
                        UOMModuleMapping.ID = isItemExists.ID;
                    }
                    else
                    {
                        UOMModuleMapping.CreatedOn = DateTime.Now;
                        UOMModuleMapping.CreatedBy = model.CreatedBy;
                        UOMModuleMapping.ID = 0;
                    }
                    UOMModuleMappingRepository.Save(UOMModuleMapping, requestData.SessionSectorName);
                }

            }
            return "true";
        }


        public string SaveReasonTypeData(ReasonTypeData model, RequestData requestData)
        {
            ReasonType entity = new ReasonType();
            if (model.ID == 0)
            {
                var code = ReasonTypeRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ReasonTypeCode).FirstOrDefault()?.ReasonTypeCode;
                if (code is null)
                {
                    code = "RNT-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ReasonTypeCode = code;
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = requestData.SessionUserId;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = requestData.SessionUserId;
            }
            EntityMapper.Map<ReasonTypeData, ReasonType>(model, entity);
            var dup = ReasonTypeRepository.Find(y => y.Name.Trim().ToLower() == model.Name.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
            if (dup != null && model.ID == 0)
                return "Duplicate";
            bool x = ReasonTypeRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }


        public string SaveServingTemperatureData(ServingTemperatureData model, RequestData requestData)
        {
            ServingTemperature entity = new ServingTemperature();
            if (model.ID == 0)
            {
                var code = ServingTemperatureRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ServingTemperatureCode).FirstOrDefault()?.ServingTemperatureCode;
                if (code is null)
                {
                    code = "STC-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ServingTemperatureCode = code;
            }
            EntityMapper.Map<ServingTemperatureData, ServingTemperature>(model, entity);
            bool x = ServingTemperatureRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveSiteData(SiteMasterData model, RequestData requestData)
        {
            SiteMaster entity = new SiteMaster();
            EntityMapper.Map<SiteMasterData, SiteMaster>(model, entity);
            //  var dup = SiteMasterRepository.Find(y => y.SiteAlias.Trim().ToLower() == model.SiteAlias.Trim().ToLower()).FirstOrDefault();
            //if (dup != null )
            //  return "Duplicate";

            
            var siteData = SiteMasterRepository.Find(item => item.SiteCode == model.SiteCode, requestData.SessionSectorName).FirstOrDefault();
            if (siteData != null)
            {
                entity.SapSectorCode = siteData.SapSectorCode;
                entity.SapSectorName = siteData.SapSectorName;
                entity.CMPSectorCode = siteData.CMPSectorCode;

            }
            bool x = SiteMasterRepository.Save(entity, requestData.SessionSectorName);
            if (x)
            {
                SiteMasterRepository.UpdateHRISLocationData(requestData, model);
            }
            return x.ToString();
        }

        public string SaveSiteDataList(IList<SiteMasterData> model, RequestData requestData)
        {
            List<SiteMaster> entity = new List<SiteMaster>();
            EntityMapper.Map<IList<SiteMasterData>, IList<SiteMaster>>(model, entity);
            string x = SiteMasterRepository.SaveList(entity, requestData);
            return x;
        }

        public string SaveCafeData(CafeData model, RequestData requestData)
        {
            Cafe entity = new Cafe();
            if (model.ID == 0)
            {
                var code = CafeRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.CafeCode).FirstOrDefault()?.CafeCode;
                if (code is null)
                    code = "CFC-00001";
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.CafeCode = code;
            }

            EntityMapper.Map<CafeData, Cafe>(model, entity);
            var dup = CafeRepository.Find(y => y.CafeName.Trim().ToLower() == model.CafeName.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
            if (dup != null && model.ID == 0)
                return "Duplicate";

            bool x = CafeRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }
        public string SaveContainerTypeData(ContainerTypeData model, RequestData requestData)
        {
            ContainerType entity = new ContainerType();
            if (model.ID == 0)
            {
                var code = ContainerTypeRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ContainerTypeCode).FirstOrDefault()?.ContainerTypeCode;
                if (code is null)
                    code = "CNT-00001";
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ContainerTypeCode = code;
            }
            var dup = ContainerTypeRepository.Find(y => y.Name.Trim().ToLower() == model.Name.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
            if (dup != null && model.ID == 0)
                return "Duplicate";
            EntityMapper.Map<ContainerTypeData, ContainerType>(model, entity);
            bool x = ContainerTypeRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }
        public string SaveApplicationSettingData(ApplicationSettingData model, RequestData requestData)
        {
            ApplicationSetting entity = new ApplicationSetting();
            if (model.ID == 0)
            {
                var code = ApplicationSettingRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.Code).FirstOrDefault()?.Code;
                if (code is null)
                    code = "FLX-00001";
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.Code = code;
            }
            EntityMapper.Map<ApplicationSettingData, ApplicationSetting>(model, entity);
            bool x = ApplicationSettingRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }


        public string SaveCafeDataList(IList<CafeData> model, RequestData requestData)
        {
            List<Cafe> entity = new List<Cafe>();
            EntityMapper.Map<IList<CafeData>, IList<Cafe>>(model, entity);
            string x = CafeRepository.SaveList(entity, requestData);
            return x;
        }
        public string SaveContainerTypeDataList(IList<ContainerTypeData> model, RequestData requestData)
        {
            List<ContainerType> entity = new List<ContainerType>();
            EntityMapper.Map<IList<ContainerTypeData>, IList<ContainerType>>(model, entity);
            string x = ContainerTypeRepository.SaveList(entity, requestData);
            return x;
        }
        public string SaveColorData(ColorData model, RequestData requestData)
        {
            Color entity = new Color();
            if (model.ID == 0)
            {
                var code = ColorRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.ColorCode).FirstOrDefault()?.ColorCode;
                if (code is null)
                {
                    code = "CLR-00001";

                }
                else
                {

                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.ColorCode = code;
            }
            EntityMapper.Map<ColorData, Color>(model, entity);
            bool x = ColorRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }


        public IList<SiteDishContainerMappingData> GetSiteDishContainerDataList(string siteCode, RequestData requestData)
        {
            List<SiteDishContainerMappingData> result = new List<SiteDishContainerMappingData>();
            var entityList = SiteDishContainerMappingRepository.GetSiteDishContainerMappingDataList(siteCode, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteDishContainerMappingData_Result>, IList<SiteDishContainerMappingData>>(entityList, result);
            return result;
        }

        public IList<SiteDishCategoryContainerMappingData> GetSiteDishCategoryContainerDataList(string siteCode, RequestData requestData)
        {
            List<SiteDishCategoryContainerMappingData> result = new List<SiteDishCategoryContainerMappingData>();
            var entityList = SiteDishContainerMappingRepository.GetSiteDishCategoryContainerMappingDataList(siteCode, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteDishCategoryContainerMappingData_Result>, IList<SiteDishCategoryContainerMappingData>>(entityList, result);
            return result;
        }

        public string SaveSiteDishContainerMappingData(SiteDishContainerMappingData model, RequestData requestData)
        {
            SiteDishContainerMapping entity = new SiteDishContainerMapping();
            EntityMapper.Map(model, entity);
            bool x = SiteDishContainerMappingRepository.Save(entity, requestData.SessionSectorName);
            return x.ToString();
        }

        public string SaveSiteDishContainerMappingDataList(IList<SiteDishContainerMappingData> model, RequestData requestData)
        {
            List<SiteDishContainerMapping> entity = new List<SiteDishContainerMapping>();
            EntityMapper.Map<IList<SiteDishContainerMappingData>, IList<SiteDishContainerMapping>>(model, entity);
            string x = SiteDishContainerMappingRepository.SaveSiteDishContainerMappingDataList(entity, requestData);
            return x;
        }
        public string SaveSiteDishCategoryContainerMappingDataList(IList<SiteDishCategoryContainerMappingData> model, RequestData requestData)
        {
            List<SiteDishCategoryContainerMapping> entity = new List<SiteDishCategoryContainerMapping>();
            EntityMapper.Map<IList<SiteDishCategoryContainerMappingData>, IList<SiteDishCategoryContainerMapping>>(model, entity);
            string x = SiteDishContainerMappingRepository.SaveSiteDishCategoryContainerMappingDataList(entity, requestData);
            return x;
        }


        public IList<SiteDishContainerMappingData> GetSiteDishContainerMappingDataList(string siteCode, RequestData requestData)
        {
            List<SiteDishContainerMappingData> result = new List<SiteDishContainerMappingData>();
            var entityList = SiteDishContainerMappingRepository.GetSiteDishContainerMappingDataList(siteCode, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteDishContainerMappingData_Result>, IList<SiteDishContainerMappingData>>(entityList, result);
            return result;
        }

        public IList<SiteDishCategoryContainerMappingData> GetSiteDishCategoryContainerMappingDataList(string siteCode, RequestData requestData)
        {
            List<SiteDishCategoryContainerMappingData> result = new List<SiteDishCategoryContainerMappingData>();
            var entityList = SiteDishContainerMappingRepository.GetSiteDishCategoryContainerMappingDataList(siteCode, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteDishCategoryContainerMappingData_Result>, IList<SiteDishCategoryContainerMappingData>>(entityList, result);
            return result;
        }

        public IList<RegionsDetailsByUserIdData> GetRegionsDetailsByUserIdDataList(RequestData requestData)
        {
            List<RegionsDetailsByUserIdData> result = new List<RegionsDetailsByUserIdData>();
            var entityList = NationalMOGRepository.GetRegionsDetailsByUserIdDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetRegionsDetailsByUserId_Result>, IList<RegionsDetailsByUserIdData>>(entityList, result);
            return result;
        }
        public IList<SiteDetailsByUserIdData> GetSiteDetailsByUserIdDataList(RequestData requestData)
        {
            List<SiteDetailsByUserIdData> result = new List<SiteDetailsByUserIdData>();
            var entityList = NationalMOGRepository.GetSiteDetailsByUserIdDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetSiteDetailsByUserId_Result>, IList<SiteDetailsByUserIdData>>(entityList, result);
            return result;
        }
        public IList<UserRBFAMatrixData> GetUserRBFAMatrixDataByUserId(RequestData requestData)
        {
            List<UserRBFAMatrixData> result = new List<UserRBFAMatrixData>();
            var entityList = NationalMOGRepository.GetUserRBFAMatrixDataByUserId(requestData).ToList();
            EntityMapper.Map<IList<procGetUserRBFAMatrixByUserID_Result>, IList<UserRBFAMatrixData>>(entityList, result);
            return result;
        }

        public List<APLMasterData> NationalGetAPLMasterDataList(string articleNumber, string mogCode, RequestData requestData)
        {
            throw new NotImplementedException();
        }

        public IList<ExpiryCategoryMasterData> GetExpiryCategoryDataList(RequestData requestData)
        {
            List<ExpiryCategoryMasterData> result = new List<ExpiryCategoryMasterData>();
            var entityList = SiteMasterRepository.GetExpiryCategoryDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetExpiryCategoryMasterData_Result>, IList<ExpiryCategoryMasterData>>(entityList, result);
            return result;
        }

        public IList<ShelfLifeUOMMasterData> GetShelfLifeUOMDataList(RequestData requestData)
        {
            List<ShelfLifeUOMMasterData> result = new List<ShelfLifeUOMMasterData>();
            var entityList = SiteMasterRepository.GetShelfLifeUOMDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetShelfLifeUOMMasterData_Result>, IList<ShelfLifeUOMMasterData>>(entityList, result);
            return result;
        }

        public string SaveExpiryCategory(RequestData requestData, ExpiryCategoryMasterData model)
        {
            ExpiryCategoryMaster entity = new ExpiryCategoryMaster();
            EntityMapper.Map<ExpiryCategoryMasterData, ExpiryCategoryMaster>(model, entity);
            string x = SiteMasterRepository.SaveExpiryCategory(requestData, entity);
            return x;
        }

        public string SaveNutritionMaster(RequestData requestData, NutritionElementMasterData model)
        {
            NutritionElementMaster entity = new NutritionElementMaster();
            EntityMapper.Map<NutritionElementMasterData, NutritionElementMaster>(model, entity);
            string x = SiteMasterRepository.SaveNutritionMaster(requestData, entity);
            return x;
        }

        public IList<NutritionElementMasterData> GetNutritionMasterDataList(RequestData requestData)
        {
            List<NutritionElementMasterData> result = new List<NutritionElementMasterData>();
            var entityList = SiteMasterRepository.GetNutritionMasterDataList(requestData).ToList();
            EntityMapper.Map<IList<procGetNutritionMasterData_Result>, IList<NutritionElementMasterData>>(entityList, result);
            return result;
        }



        public IList<DietTypeData> GetDietTypeDataList(RequestData requestData)
        {
            List<DietTypeData> result = new List<DietTypeData>();
            var entityList = SiteMasterRepository.GetDietTypeDataList(requestData).ToList();
            EntityMapper.Map<IList<DietType>, IList<DietTypeData>>(entityList, result);
            return result;
        }



        public IList<TextureData> GetTextureDataList(RequestData requestData)
        {
            List<TextureData> result = new List<TextureData>();
            var entityList = SiteMasterRepository.GetTextureDataList(requestData).ToList();
            EntityMapper.Map<IList<Texture>, IList<TextureData>>(entityList, result);
            return result;
        }

        public string SaveCommonMaster(RequestData requestData, CommonMasterData model)
        {
            return SiteMasterRepository.SaveCommonMaster(requestData, model).ToString();
        }

        public IList<CommonMasterData> GetCommonMasterDataList(RequestData requestData, CommonMasterData model)
        {
            List<CommonMasterData> result = new List<CommonMasterData>();
            if (model.ModuleName.ToUpper() == "NUTRIENTTYPE")
            {
                var entityList = SiteMasterRepository.GetNutrientTypeDataList(requestData).ToList();
                EntityMapper.Map<IList<NutritionElementType>, IList<CommonMasterData>>(entityList, result);
            }
            else if (model.ModuleName.ToUpper() == "NUTRIENTUOM")
            {
                var entityList = SiteMasterRepository.GetNutrientUOMDataList(requestData).ToList();
                EntityMapper.Map<IList<NutrientElementUOM>, IList<CommonMasterData>>(entityList, result);
            }
            else if (model.ModuleName.ToUpper() == "ITEMSECTOR")
            {
                var entityList = SiteMasterRepository.GetItemSectorMasterDataList(requestData).ToList();
                EntityMapper.Map<IList<ItemSectorMaster>, IList<CommonMasterData>>(entityList, result);
            }
            else if (model.ModuleName.ToUpper() == "MENUTYPE1")
            {
                var entityList = SiteMasterRepository.GetMenuType1MasterDataList(requestData).ToList();
                EntityMapper.Map<IList<MenuType1>, IList<CommonMasterData>>(entityList, result);
            }
            else if (model.ModuleName.ToUpper() == "MENUTYPE2")
            {
                var entityList = SiteMasterRepository.GetMenuType2MasterDataList(requestData).ToList();
                EntityMapper.Map<IList<MenuType2>, IList<CommonMasterData>>(entityList, result);
            }
            else if (model.ModuleName.ToUpper() == "AREA")
            {
                var entityList = SiteMasterRepository.GetAreaMasterDataList(requestData).ToList();
                EntityMapper.Map<IList<Area>, IList<CommonMasterData>>(entityList, result);
            }

            return result;
        }

        public IList<MOGUOMData> GetMOGUOMDataList(RequestData requestData)
        {
            List<MOGUOMData> result = new List<MOGUOMData>();
            var entityList = MOGRepository.GetMOGUOMDataList(requestData);
            EntityMapper.Map<IList<procGetMOGUOMDetails_Result>, IList<MOGUOMData>>(entityList, result);
            return result;
        }

        public IList<NutritionElementTypeData> GetNutritionElementTypeDataList(RequestData requestData)
        {
            List<NutritionElementTypeData> result = new List<NutritionElementTypeData>();
            var entityList = SiteMasterRepository.GetNutritionElementTypeDataList(requestData).ToList();
            EntityMapper.Map<IList<NutritionElementType>, IList<NutritionElementTypeData>>(entityList, result);
            return result;
        }

        public IList<DishCategorySiblingData> GetDishCategorySiblingDataList(RequestData requestData)
        {
            List<DishCategorySiblingData> result = new List<DishCategorySiblingData>();
            var entityList = DishCategoryRepository.GetDishCategorySiblingList(requestData).ToList();
            EntityMapper.Map<IList<procGetDishCategorySiblingList_Result>, IList<DishCategorySiblingData>>(entityList, result);
            return result;
        }

        //Krish
        public IList<RangeTypeMasterData> GetRangeTypeMasterDataList(RequestData requestData)
        {
            List<RangeTypeMasterData> result = new List<RangeTypeMasterData>();
            var entityList = RangeTypeMasterRepository.GetRangeTypeMasterDataList(requestData);
            EntityMapper.Map<IList<procGetRangeTypeMaster_Result>, IList<RangeTypeMasterData>>(entityList, result);
            return result;
        }
        public string SaveRangeTypeMaster(RequestData requestData, RangeTypeMasterData model)
        {
            RangeType entity = new RangeType();
            if (model.ID == 0)
            {
                var code = RangeTypeMasterRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.Code).FirstOrDefault()?.Code;
                if (code is null)
                {
                    code = "RNG-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.Code = code;
            }
            EntityMapper.Map<RangeTypeMasterData, RangeType>(model, entity);
            string x = RangeTypeMasterRepository.SaveRangeTypeMasterDataList(entity, requestData);
            return x;
        }
        //Krish 08-10-2022
        public string SaveRangeTypeAPLData(string articleNumber, string rangeTypeCode, RequestData requestData)
        {
            return APLMasterRepository.SaveRangeTypeAPLData(articleNumber, rangeTypeCode, requestData);
        }
        //Krish 13-10-2022
        public IList<ProcessTypeMasterData> GetProcessTypeMasterDataList(RequestData requestData)
        {
            List<ProcessTypeMasterData> result = new List<ProcessTypeMasterData>();
            var entityList = ProcessTypeMasterRepository.GetProcessTypeMasterDataList(requestData);
            EntityMapper.Map<IList<procGetProcessTypeMaster_Result>, IList<ProcessTypeMasterData>>(entityList, result);
            return result;
        }
        public string SaveProcessTypeMaster(RequestData requestData, ProcessTypeMasterData model)
        {
            ProcessType entity = new ProcessType();
            if (model.ID == 0)
            {
                var code = ProcessTypeMasterRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.Code).FirstOrDefault()?.Code;
                if (code is null)
                {
                    code = "PRT-00001";

                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.Code = code;
            }
            EntityMapper.Map<ProcessTypeMasterData, ProcessType>(model, entity);
            string x = ProcessTypeMasterRepository.SaveProcessTypeMasterDataList(entity, requestData);
            return x;
        }
        //Krish 18-10-2022
        public string SaveProcessTypeMogData(RequestData requestData, List<string> processTypes, string mogCode, string rangeTypeCode, int inputStdDuration)
        {
            return NationalMOGRepository.SaveProcessTypeMogData(requestData, processTypes, mogCode, rangeTypeCode, inputStdDuration);
        }

        public string SaveAllergenMogData(RequestData requestData, List<string> contains, List<string> maycontains, string mogCode)
        {
            return NationalMOGRepository.SaveAllergenMogData(requestData, contains,maycontains, mogCode);
        }
        public IList<MogProcessTypeMappingData> GetProcessTypeMogData(RequestData requestData, string mogCode)
        {
            List<MogProcessTypeMappingData> result = new List<MogProcessTypeMappingData>();
            var entityList = NationalMOGRepository.GetProcessTypeMogData(requestData, mogCode);
            EntityMapper.Map<IList<MogProcessTypeMapping>, IList<MogProcessTypeMappingData>>(entityList, result);
            return result;
        }
        public IList<MogProcessTypeMappingData> GetProcessMappingDataList(RequestData requestData)
        {
            List<MogProcessTypeMappingData> result = new List<MogProcessTypeMappingData>();
            var entityList = ProcessTypeMasterRepository.GetProcessMappingDataList(requestData);
            EntityMapper.Map<IList<MogProcessTypeMapping>, IList<MogProcessTypeMappingData>>(entityList, result);
            return result;
        }
    }
}
