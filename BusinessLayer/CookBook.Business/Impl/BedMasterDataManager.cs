﻿using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Mapper;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace CookBook.Business.Impl
{    
    [Export(typeof(IBedMasterDataManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class BedMasterDataManager : ImplBase, IBedMasterDataManager
    {
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IBedMasterRepository BedMasterRepository { get; set; }

        public List<BedMasterData> GetBedMaster(RequestData requestData)
        {
            List<BedMasterData> result = new List<BedMasterData>();
            var entityList = BedMasterRepository.GetBedMaster(requestData).ToList();
            EntityMapper.Map<List<procGetBedMaster_Result>, List<BedMasterData>>(entityList, result);

            return result;
        }

        public List<BedMasterHISTORY> GetBedMasterHISTORY(RequestData requestData, string username)
        {
            List<BedMasterHISTORY> result = new List<BedMasterHISTORY>();
            var entityList = BedMasterRepository.GetBedMasterHISTORY(requestData, username).ToList();
            EntityMapper.Map<List<procGetBedMasterBATCHHEADER_Result>, List<BedMasterHISTORY>>(entityList, result);
            return result;
        }
        public List<BedMasterHISTORY> GetBedMasterHISTORYDet(RequestData requestData, string username)
        {
            List<BedMasterHISTORY> result = new List<BedMasterHISTORY>();
            var entityList = BedMasterRepository.GetBedMasterHISTORYDet(requestData, username).ToList();
            EntityMapper.Map<List<procGetBedMasterBATCHDETAILS_Result>, List<BedMasterHISTORY>>(entityList, result);
            return result;
        }
        public List<BedMasterHistoryBatchWise> GetshowGetALLBedMasterHISTORYBATCHWISE(RequestData requestData, string batchno)
        {
            List<BedMasterHistoryBatchWise> result = new List<BedMasterHistoryBatchWise>();
            var entityList = BedMasterRepository.GetshowGetALLBedMasterHISTORYBATCHWISE(requestData,  batchno).ToList();
            EntityMapper.Map<List<procshowGetALLBedMasterHISTORYBATCHWISE_Result>, List<BedMasterHistoryBatchWise>>(entityList, result);
            return result;
        }
        public string AddUpdateBedMasterList(RequestData request, string xml, string username)
        {

            string entityList = BedMasterRepository.AddUpdateBedMasterList(request, xml, username).ToString();
            return entityList;
        }
        public string SaveBedMasterStatus(RequestData request, int id, int username)
        {
            string entityList = BedMasterRepository.SaveBedMasterStatus(request, id, username).ToString();
            return entityList;
        }
        public List<BedMasterHistoryAllData> GetALLHISTORYDetailsBedMaster(RequestData request)
        {
            List<BedMasterHistoryAllData> result = new List<BedMasterHistoryAllData>();
            var entityList = BedMasterRepository.GetALLHISTORYDetailsBedMaster(request).ToList();
            EntityMapper.Map<List<procshowGetALLHISTORYBedMaster_Result>, List<BedMasterHistoryAllData>>(entityList, result);
            return result;

        }
    }
}
