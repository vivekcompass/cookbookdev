﻿using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.Common;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using CookBook.Mapper;
using CookBook.DataAdapter.Factory.Contract;
using CookBook.DataAdapter.Factory.Repository;
using CookBook.Data.MasterData;
using System.Linq;
using CookBook.DataAdapter.Factory;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using CookBook.Data.Request;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.Business.Impl
{
    [Export(typeof(IRecipeDataManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RecipeDataManager : ImplBase, IRecipeDataManager
    {
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IRecipeRepository RecipeRepository { get; set; }

        public IList<RecipeData> GetRecipeDataList(string condition, string DishCode, RequestData requestData)
        {
            List<RecipeData> result = new List<RecipeData>();
            var entityList = RecipeRepository.GetRecipeDataList(condition, DishCode, requestData).ToList();
            EntityMapper.Map<IList<procGetRecipeList_Result>, IList<RecipeData>>(entityList, result);
            return result;
        }
        public IList<NationalRecipeData> GetNationalRecipeDataList(string condition, string DishCode, RequestData requestData)
        {
            List<NationalRecipeData> result = new List<NationalRecipeData>();
            var entityList = RecipeRepository.GetNationalRecipeDataList(condition, DishCode, requestData).ToList();
            EntityMapper.Map<IList<procGetnationalRecipeList_Result>, IList<NationalRecipeData>>(entityList, result);
            return result;
        }

        public IList<RegionRecipeData> GetRegionRecipeDataList(int RegionID, string condition, RequestData requestData)
        {
            List<RegionRecipeData> result = new List<RegionRecipeData>();
            var entityList = RecipeRepository.GetRegionRecipeDataList(RegionID, condition, requestData).ToList();
            EntityMapper.Map<IList<procGetRegionRecipeList_Result>, IList<RegionRecipeData>>(entityList, result);
            return result;
        }
        public IList<SiteRecipeData> GetSiteRecipeDataList(string SiteCode, string condition, RequestData requestData)
        {
            List<SiteRecipeData> result = new List<SiteRecipeData>();
            var entityList = RecipeRepository.GetSiteRecipeDataList(SiteCode, condition, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteRecipeList_Result>, IList<SiteRecipeData>>(entityList, result);
            return result;
        }
        //Krish
        public IList<SiteRecipeData> GetSiteDishRecipeDataList(string SiteCode, string dishCode, string condition, RequestData requestData)
        {
            List<SiteRecipeData> result = new List<SiteRecipeData>();
            var entityList = RecipeRepository.GetSiteDishRecipeDataList(SiteCode, dishCode, condition, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteDishRecipeList_Result>, IList<SiteRecipeData>>(entityList, result);
            return result;
        }
        public IList<RecipeMOGMappingData> GetBaseRecipesByRecipeID(int recipeID, RequestData requestData)
        {
            List<RecipeMOGMappingData> result = new List<RecipeMOGMappingData>();
            var entityList = RecipeRepository.GetBaseRecipesByRecipeID(recipeID, requestData).ToList();
            EntityMapper.Map<IList<procGetBaseRecipesByRecipeID_Result>, IList<RecipeMOGMappingData>>(entityList, result);
            return result;
        }

        public IList<RecipeMOGMappingData> GetRegionBaseRecipesByRecipeID(int regionID, int recipeID, RequestData requestData)
        {
            List<RecipeMOGMappingData> result = new List<RecipeMOGMappingData>();
            var entityList = RecipeRepository.GetRegionBaseRecipesByRecipeID(regionID, recipeID, requestData).ToList();
            EntityMapper.Map<IList<procGetRegionBaseRecipesByRecipeID_Result>, IList<RecipeMOGMappingData>>(entityList, result);
            return result;
        }

        public IList<RecipeMOGMappingData> GetSiteBaseRecipesByRecipeID(string SiteCode, int recipeID, string recipeCode, RequestData requestData)
        {
            List<RecipeMOGMappingData> result = new List<RecipeMOGMappingData>();
            var entityList = RecipeRepository.GetSiteBaseRecipesByRecipeID(SiteCode, recipeID, recipeCode, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteBaseRecipesByRecipeID_Result>, IList<RecipeMOGMappingData>>(entityList, result);
            return result;
        }


        public IList<RecipeMOGMappingData> GetMOGsByRecipeID(int recipeID, RequestData requestData)
        {
            List<RecipeMOGMappingData> result = new List<RecipeMOGMappingData>();
            var entityList = RecipeRepository.GetMOGsByRecipeID(recipeID, requestData).ToList();
            EntityMapper.Map<IList<procGetMOGsByRecipeID_Result>, IList<RecipeMOGMappingData>>(entityList, result);
            return result;
        }

        public IList<RecipeMOGMappingData> GetRegionMOGsByRecipeID(int regionID, int recipeID, RequestData requestData)
        {
            List<RecipeMOGMappingData> result = new List<RecipeMOGMappingData>();
            var entityList = RecipeRepository.GetRegionMOGsByRecipeID(regionID, recipeID, requestData).ToList();
            EntityMapper.Map<IList<procGetRegionMOGsByRecipeID_Result>, IList<RecipeMOGMappingData>>(entityList, result);
            return result;
        }

        public IList<RecipeMOGMappingData> GetSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode, RequestData requestData)
        {
            List<RecipeMOGMappingData> result = new List<RecipeMOGMappingData>();
            var entityList = RecipeRepository.GetSiteMOGsByRecipeID(SiteCode, recipeID, recipeCode, requestData).ToList();
            EntityMapper.Map<IList<procGetSiteMOGsByRecipeID_Result>, IList<RecipeMOGMappingData>>(entityList, result);
            return result;
        }

        public string SaveRecipeData(RecipeData model, IList<RecipeMOGMappingData> baseRecipes, IList<RecipeMOGMappingData> mogs, RequestData requestData)
        {
            Recipe entity = new Recipe();
            if (model.ID is null || model.ID == null)
            {
                var code = RecipeRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.RecipeCode).FirstOrDefault()?.RecipeCode;
                if (code is null)
                {
                    if (requestData.SessionSectorNumber == "10")
                    {
                        if (model.SubSectorCode == "SSR-001")
                        {
                            code = "OCP-00001";
                        }
                        else if (model.SubSectorCode == "SSR-002")
                        {
                            code = "GCP-00001";
                        }
                        else if (model.SubSectorCode == "SSR-003")
                        {
                            code = "VCP-00001";
                        }
                        else
                        {
                            code = "RCP-00001";
                        }
                    }
                    else
                    {
                        code = "RCP-00001";
                    }
                }
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.RecipeCode = code;

            }
            var dup = RecipeRepository.Find(y => y.Name.Trim().ToLower() == model.Name.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
            if (dup != null && (model.ID == 0 || model.ID == null))
                return "Duplicate";
            List<RecipeMOGMapping> recipeMOGMappingEntity1 = new List<RecipeMOGMapping>();
            List<RecipeMOGMapping> recipeMOGMappingEntity2 = new List<RecipeMOGMapping>();
            EntityMapper.Map<RecipeData, Recipe>(model, entity);

            EntityMapper.Map<IList<RecipeMOGMappingData>, IList<RecipeMOGMapping>>(baseRecipes, recipeMOGMappingEntity1);
            EntityMapper.Map<IList<RecipeMOGMappingData>, IList<RecipeMOGMapping>>(mogs, recipeMOGMappingEntity2);
            if (recipeMOGMappingEntity1.Count == 0 && recipeMOGMappingEntity2.Count == 0 && !model.IsRecipeStatusChange)
            {
                // 1 for set pending
                entity.Status = 1;
            }
            string x = RecipeRepository.SaveRecipeData(entity, recipeMOGMappingEntity1, recipeMOGMappingEntity2, requestData);
            return x;
        }

        public string SaveRegionRecipeData(RegionRecipeData model, IList<RegionRecipeMOGMappingData> baseRecipes, IList<RegionRecipeMOGMappingData> mogs, RequestData requestData)
        {
            RegionRecipe entity = new RegionRecipe();
            if (model.ID == 0)
            {
                var code = RecipeRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.RecipeCode).FirstOrDefault()?.RecipeCode;
                if (code is null)
                    code = "RCP-00001";
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.RecipeCode = code;

            }
            List<RegionRecipeMOGMapping> recipeMOGMappingEntity1 = new List<RegionRecipeMOGMapping>();
            List<RegionRecipeMOGMapping> recipeMOGMappingEntity2 = new List<RegionRecipeMOGMapping>();
            EntityMapper.Map<RegionRecipeData, RegionRecipe>(model, entity);
            EntityMapper.Map<IList<RegionRecipeMOGMappingData>, IList<RegionRecipeMOGMapping>>(baseRecipes, recipeMOGMappingEntity1);
            EntityMapper.Map<IList<RegionRecipeMOGMappingData>, IList<RegionRecipeMOGMapping>>(mogs, recipeMOGMappingEntity2);

            string x = RecipeRepository.SaveRegionRecipeData(entity, recipeMOGMappingEntity1, recipeMOGMappingEntity2, requestData);
            return x;
        }

        public string SaveSiteRecipeData(SiteRecipeData model, IList<SiteRecipeMOGMappingData> baseRecipes, IList<SiteRecipeMOGMappingData> mogs, RequestData requestData)
        {
            SiteRecipe entity = new SiteRecipe();
            if (model.ID == 0)
            {
                var code = RecipeRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.RecipeCode).FirstOrDefault()?.RecipeCode;
                if (code is null)
                    code = "RCP-00001";
                else
                {
                    var lcode = code.Substring(0, 3);
                    var rcode = code.Substring(4, 5);
                    code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                }
                model.RecipeCode = code;
            }
            List<SiteRecipeMOGMapping> recipeMOGMappingEntity1 = new List<SiteRecipeMOGMapping>();
            List<SiteRecipeMOGMapping> recipeMOGMappingEntity2 = new List<SiteRecipeMOGMapping>();
            EntityMapper.Map<SiteRecipeData, SiteRecipe>(model, entity);
            EntityMapper.Map<IList<SiteRecipeMOGMappingData>, IList<SiteRecipeMOGMapping>>(baseRecipes, recipeMOGMappingEntity1);
            EntityMapper.Map<IList<SiteRecipeMOGMappingData>, IList<SiteRecipeMOGMapping>>(mogs, recipeMOGMappingEntity2);

            string x = RecipeRepository.SaveSiteRecipeData(entity, recipeMOGMappingEntity1, recipeMOGMappingEntity2, requestData);
            return x;
        }

        public string SaveRecipeDataList(IList<RecipeData> model, RequestData requestData)
        {
            List<Recipe> entity = new List<Recipe>();
            EntityMapper.Map<IList<RecipeData>, IList<Recipe>>(model, entity);
            string x = RecipeRepository.SaveList(entity, requestData);
            return x;
        }
        public string SaveRegionRecipeDataList(IList<RegionRecipeData> model, RequestData requestData)
        {
            List<RegionRecipe> entity = new List<RegionRecipe>();
            EntityMapper.Map<IList<RegionRecipeData>, IList<RegionRecipe>>(model, entity);
            string x = RecipeRepository.SaveRegionList(entity, requestData);
            return x;
        }

        public string SaveSiteRecipeDataList(IList<SiteRecipeData> model, RequestData requestData)
        {
            List<SiteRecipe> entity = new List<SiteRecipe>();
            EntityMapper.Map<IList<SiteRecipeData>, IList<SiteRecipe>>(model, entity);
            string x = RecipeRepository.SaveSiteList(entity, requestData);
            return x;
        }

        public string SaveSiteRecipeAllergenData(string recipeCode, string siteCode, RequestData requestData)
        {
            string x = RecipeRepository.SaveSiteRecipeAllergenData(recipeCode, siteCode, requestData);
            return x;
        }

        public string SaveRecipeNutrientMappingData(RecipeData entity, RequestData requestData)
        {
            return RecipeRepository.SaveRecipeNutrientMappingData(entity, requestData);
        }

        public IList<ItemDishRecipeNutritionData> GetItemDishRecipeNutritionData(string SiteCode, RequestData requestData)
        {
            List<ItemDishRecipeNutritionData> result = new List<ItemDishRecipeNutritionData>();
            var entityList = RecipeRepository.GetItemDishRecipeNutritionData(SiteCode, requestData).ToList();
            EntityMapper.Map<IList<procGetItemDishRecipeNutritionData_Result>, IList<ItemDishRecipeNutritionData>>(entityList, result);
            return result;
        }
        //Krish
        public RecipePrintingData GetRecipePrintingData(string recipeCode, string siteCode, int regId, RequestData requestData)
        {
            List<RecipePrintingMOGListData> result = new List<RecipePrintingMOGListData>();
            var entityList = RecipeRepository.GetRecipePrintingData(recipeCode, siteCode, regId, requestData).ToList();
            EntityMapper.Map<IList<procGetRecipeMOGNLevelFOrPrinting_Result>, IList<RecipePrintingMOGListData>>(entityList, result);
            var data = new RecipePrintingData();
            data.Mogs = result;
            var recDet = RecipeRepository.GetRecipeDishDataForPrinting(recipeCode, requestData);
            data.DishName = recDet;
            return data;
        }
        //Krish
        public string SaveRecipeCopy(List<RecipeCopyData> model, List<string> recipes, string sourceSite, RequestData requestData)
        {
            var recDet = RecipeRepository.SaveRecipeCopy(model,recipes,sourceSite, requestData);
            return recDet;
        }
        //Krish
        public List<NutritionDishRecipeMogMapping> GetRecipeDishDataByDish(string dishCode, RequestData requestData)
        {
            List<NutritionDishRecipeMogMapping> result = new List<NutritionDishRecipeMogMapping>();
            result = RecipeRepository.GetRecipeDishDataByDish(dishCode, requestData);
            foreach (var item in result)
            {
                requestData.RecipeCode = item.RecipeCode;
                item.RecipeMOG = GetMOGNLevelForSplitScreen(requestData).ToList();
            }
            return result;
        }
        public SectorRecipeData GetSectorRecipeData(RequestData requestData, bool isAddEdit, bool onLoad)
        {
            return RecipeRepository.GetSectorRecipeData(requestData, isAddEdit,onLoad);
        }
        //Krish 06-08-2022
        public IList<RecipeMOGMappingData> GetMOGNLevelForSplitScreen( RequestData requestData)
        {
            List<RecipeMOGMappingData> result = new List<RecipeMOGMappingData>();
            var entityList = RecipeRepository.GetMOGNLevelForSplitScreen(requestData.RecipeCode, requestData).ToList();
            EntityMapper.Map<IList<procGetMOGNLevelForSplitScreen_Result>, IList<RecipeMOGMappingData>>(entityList, result);
            return result;
        }
    }
}
