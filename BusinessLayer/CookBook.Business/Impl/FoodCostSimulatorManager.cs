﻿using CookBook.Aspects.Constants;
using CookBook.Business.Base.Impl;
using CookBook.Business.Contract;
using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.DataAdapter.Factory.Contract;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Mapper;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Business.Impl
{
    [Export(typeof(IFoodCostSimulatorManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class FoodCostSimulatorManager : ImplBase, IFoodCostSimulatorManager
    {
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IFoodCostSimulatorRepository FoodCostSimulatorRepository { get; set; }
        [Dependency]
        public IItemDishCategoryMappingRepository ItemDishCategoryMappingRepository { get; set; }
        [Dependency]
        public IRecipeRepository RecipeRepository { get; set; }
        [Dependency]
        public IItemRepository ItemRepository { get; set; }

        public List<DayPartData> GetDayPartBySiteList(RequestData requestData, int siteid)
        {
            List<DayPartData> result = new List<DayPartData>();
            result = FoodCostSimulatorRepository.GetDayPartBySiteList(siteid, requestData).ToList();
            //EntityMapper.Map<IList<SimulationList>, IList<SimulationList>>(entityList, result);
            return result;
        }
        public List<FoodCostSimulationData> GetSimulationDataListWithPaging(RequestData requestData, UserMasterResponseData userData)
        {
            //List<SimulationList> result = new List<SimulationList>();
            //result = FoodCostSimulatorRepository.GetSimulationDataListWithPaging(requestData, pageindex, totalRecordsPerPage).ToList();
            ////EntityMapper.Map<IList<SimulationList>, IList<SimulationList>>(entityList, result);
            //return result;
            List<FoodCostSimulationData> result = new List<FoodCostSimulationData>();
            //var entityList = FoodCostSimulatorRepository.GetAll(requestData.SessionSectorName).OrderByDescending(a => a.CreateOn).ToList();
            var entityList = FoodCostSimulatorRepository.GetSimulationDataListWithPaging(requestData, userData, 0, 0).ToList();
            EntityMapper.Map<IList<FoodCostSimulation>, IList<FoodCostSimulationData>>(entityList, result);
            return result;
        }
        public List<SimulationItemsData> GetItemsForSimulation(RequestData requestData, int siteId, int regionId, List<int> daypartId, bool checksiteItem)
        {
            List<SimulationItemsData> result = new List<SimulationItemsData>();
            result = FoodCostSimulatorRepository.GetItemsForSimulation(requestData, siteId, regionId, daypartId,checksiteItem).ToList();
            //EntityMapper.Map<IList<SimulationList>, IList<SimulationList>>(entityList, result);
            return result;
        }
        public List<SimulationRegionItemDishCategoryData> GetSimulationRegionItemDishCategories(RequestData requestData, int siteId, int regionId, int itemId)
        {
            List<SimulationRegionItemDishCategoryData> result = new List<SimulationRegionItemDishCategoryData>();
            result = FoodCostSimulatorRepository.GetSimulationRegionItemDishCategories(requestData, siteId, regionId, itemId).ToList();
            //EntityMapper.Map<IList<SimulationList>, IList<SimulationList>>(entityList, result);
            return result;
        }
        public string SaveSimulationBoard(SimulationBoardData model, RequestData requestData, FoodCostSimulationData simStatusUpdate)
        {
            if (simStatusUpdate != null)
            {
                //Status modify
                FoodCostSimulation entity = new FoodCostSimulation();
                var get = FoodCostSimulatorRepository.GetAll(requestData.SessionSectorName).FirstOrDefault(a => a.ID == simStatusUpdate.ID);
                simStatusUpdate.CreateOn = get.CreateOn;
                EntityMapper.Map<FoodCostSimulationData, FoodCostSimulation>(simStatusUpdate, entity);
                bool x = FoodCostSimulatorRepository.Save(entity, requestData.SessionSectorName);
                return x.ToString();
            }
            else
            {
                switch (model.Status)
                {
                    // coming from js
                    case "Draft":
                        model.Status = FoodCostSimulationStatus.Draft.ToString();
                        break;
                    case "Versioned":
                        model.Status = FoodCostSimulationStatus.Versioned.ToString();
                        break;
                    case "Final":
                        model.Status = FoodCostSimulationStatus.Final.ToString();
                        break;
                    case "Incomplete":
                        model.Status = FoodCostSimulationStatus.Incomplete.ToString();
                        break;
                    default:
                        break;
                }
                if (model.Status == FoodCostSimulationStatus.Versioned.ToString() || model.Status == FoodCostSimulationStatus.Final.ToString())
                    model.Version = "V1";

                if (model.ID == 0)
                {
                    var dup = FoodCostSimulatorRepository.Find(y => y.Name.Trim().ToLower() == model.SimulationName.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
                    if (dup != null && (model.ID == 0))
                        return "Duplicate";
                    var code = FoodCostSimulatorRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.SimulationCode).FirstOrDefault()?.SimulationCode;
                    if (code is null)
                        code = "SIM-00001";
                    else
                    {
                        var lcode = code.Substring(0, 3);
                        var rcode = code.Substring(4, 5);
                        code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                    }
                    model.SimCode = code;
                }
                else
                {
                    var code = FoodCostSimulatorRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.SimulationCode).FirstOrDefault()?.SimulationCode;
                    model.SimCode = code;
                }

                var result = FoodCostSimulatorRepository.SaveSimulationBoard(model, requestData);
                //EntityMapper.Map<IList<SimulationList>, IList<SimulationList>>(entityList, result);
                return result;
            }
        }
        public SimulationBoardData GetSimulationBoard(int simId, RequestData requestData)
        {
            return FoodCostSimulatorRepository.GetSimulationBoard(simId, requestData);
        }
        public IList<SiteRecipeData> UpdateAndFetchSiteDishRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode)
        {
            List<SiteRecipeData> result = new List<SiteRecipeData>();
            var entityList = FoodCostSimulatorRepository.UpdateAndFetchSiteDishRecipeData(requestData, simCode, SiteCode, DishCode).ToList();
            EntityMapper.Map<IList<procUpdateGetFCSSiteDishRecipeList_Result>, IList<SiteRecipeData>>(entityList, result);
            return result;
        }
        public IList<SiteRecipeData> UpdateAndFetchSiteRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode)
        {
            List<SiteRecipeData> result = new List<SiteRecipeData>();
            var entityList = FoodCostSimulatorRepository.UpdateAndFetchSiteRecipeData(requestData, simCode, SiteCode, DishCode).ToList();
            EntityMapper.Map<IList<procGetFCSSiteRecipeData_Result>, IList<SiteRecipeData>>(entityList, result);
            return result;
        }

        public string SaveFoodCostSimSiteRecipeData(RecipeData modelr, SiteRecipeData model, IList<SiteRecipeMOGMappingData> baseRecipes, IList<SiteRecipeMOGMappingData> mogs, string simCode, RequestData requestData)
        {
            FoodCostSimSiteRecipe entity = new FoodCostSimSiteRecipe();
            FoodCostSimRecipe recipeEntity = new FoodCostSimRecipe();
            if (modelr != null)
            {
                var dup = FoodCostSimulatorRepository.GetAllRecipes(requestData).Where(y => y.Name.Trim().ToLower() == modelr.Name.Trim().ToLower()).FirstOrDefault();
                if (dup != null && (modelr != null && modelr.ID == 0))
                    return "Duplicate";
            }

            //string r = FoodCostSimulatorRepository.SaveSiteRecipeData(entity,requestData);
            //var chk = FoodCostSimulatorRepository.GetAllRecipes(requestData).FirstOrDefault(a => a.ID == model.ID);
            var chk = FoodCostSimulatorRepository.GetAllRecipes(requestData).FirstOrDefault(a => a.RecipeCode == model.RecipeCode);
            var chkSiteRecipe = FoodCostSimulatorRepository.GetAllSiteRecipe(requestData, model.SiteCode, simCode).FirstOrDefault(a => a.RecipeCode == model.RecipeCode);
            var chkMapping = FoodCostSimulatorRepository.GetSiteDishRecipeMapping(requestData, model.SiteCode, model.DishCode, simCode).FirstOrDefault(a => a.RecipeCode == model.RecipeCode);

            if (chkSiteRecipe == null)
            {
                model.ID = 0;
                model.CreatedBy = requestData.SessionUserId;
                model.CreatedOn = DateTime.Now;
            }
            if (chk == null && modelr == null)
            {
                modelr = modelr ?? new RecipeData();
                //EntityMapper.Map<RecipeData, SiteRecipeData>(modelr, model);
                modelr.RecipeAlias = model.RecipeAlias;
                modelr.RecipeCode = model.RecipeCode;
                modelr.Name = model.Name;
                modelr.OldRecipeName = "";
                modelr.RecipeUOMCategory = model.RecipeUOMCategory;
                modelr.WeightPerUnit = model.WeightPerUnit;
                modelr.WeightPerUnitUOM = model.WeightPerUnitUOM;
                modelr.WeightPerUnitGravy = model.WeightPerUnitGravy;
                modelr.WeightPerUnitUOMGravy = model.WeightPerUnitUOMGravy;
                modelr.TotalIngredientWeight = model.TotalIngredientWeight;
                modelr.UOM_ID = model.UOM_ID;
                modelr.UOMCode = model.UOMCode;
                modelr.Quantity = model.Quantity;
                modelr.IsBase = model.IsBase;
                modelr.CostPerKG = model.CostPerKG;
                modelr.CostPerUOM = model.CostPerUOM;
                modelr.TotalCost = model.TotalCost;
                modelr.Instructions = model.Instructions;
                modelr.IsActive = model.IsActive;
                //modelr.IsRecipeNameChange = model.IsRecipeNameChange;
                //modelr.IsRecipeStatusChange = model.IsRecipeStatusChange;
                modelr.Status = model.Status;
                modelr.CreatedOn = model.CreatedOn;
                modelr.CreatedBy = model.CreatedBy;
            }
            if (model.ID == 0)
            {
                //var code = RecipeRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.RecipeCode).FirstOrDefault()?.RecipeCode;
                
                if (chk == null)    //-> When adding recipe from dropdown when recipe icon is clicked
                {
                    string code = FoodCostSimulatorRepository.GetAllRecipes(requestData).OrderByDescending(z => z.RecipeCode).FirstOrDefault()?.RecipeCode;
                    if (code is null)
                        code = "RCP-00001";
                    else
                    {
                        var lcode = code.Substring(0, 3);
                        var rcode = code.Substring(4, 5);
                        code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                    }
                    model.RecipeCode = code;

                    if (modelr != null && (modelr.ID == 0 || modelr.ID==null))
                        modelr.RecipeCode = code;
                }                
            }

            List<FoodCostSimSiteRecipeMOGMapping> recipeMOGMappingEntity1 = new List<FoodCostSimSiteRecipeMOGMapping>();
            List<FoodCostSimSiteRecipeMOGMapping> recipeMOGMappingEntity2 = new List<FoodCostSimSiteRecipeMOGMapping>();
            if (modelr != null)
            {
                EntityMapper.Map<RecipeData, FoodCostSimRecipe>(modelr, recipeEntity);
                recipeEntity.SimulationCode = simCode;
            }
            //else if (chk == null)
            //{
            //    EntityMapper.Map<SiteRecipeData, FoodCostSimRecipe>(model, recipeEntity);
            //    recipeEntity.SimulationCode = simCode;
            //}
            else
                recipeEntity = null;
            EntityMapper.Map<SiteRecipeData, FoodCostSimSiteRecipe>(model, entity);
            entity.SimulationCode = simCode;
            List<FoodCostSimAPLHistory> entityHis = new List<FoodCostSimAPLHistory>();
            if (model.History != null && model.History.Count > 0)
            {
                foreach (var item in model.History)
                {
                    item.ItemCode = ItemRepository.GetAll(requestData.SessionSectorName).FirstOrDefault(a => a.ID == item.ItemID).ItemCode;
                }
            }
            EntityMapper.Map<List<FoodCostSimAPLHistoryData>, List<FoodCostSimAPLHistory>>(model.History, entityHis);
            //if (model.ID > 0)
            //{
            //    var chkReci = RecipeRepository.GetAll(requestData.SessionSectorName).FirstOrDefault(a => a.RecipeCode == model.RecipeCode);
            //    if (chkReci != null)
            //    {
            //        entity.OID = chkReci.ID;
            //        recipeEntity.OID = chkReci.ID;
            //    }
            //}
            EntityMapper.Map<IList<SiteRecipeMOGMappingData>, IList<FoodCostSimSiteRecipeMOGMapping>>(baseRecipes, recipeMOGMappingEntity1);
            EntityMapper.Map<IList<SiteRecipeMOGMappingData>, IList<FoodCostSimSiteRecipeMOGMapping>>(mogs, recipeMOGMappingEntity2);

            //TOD if chkMapping is null insert into FoodCostSimSiteDishRecipeMapping & check and modify FoodCostSimSiteRecipeMOGMapping
            if (chkMapping == null)
            {
                return FoodCostSimulatorRepository.SaveFoodCostSimSiteDishRecipeMappingAndMOG(requestData, recipeEntity, entity, recipeMOGMappingEntity2, model.DishID, model.DishCode, entityHis, recipeMOGMappingEntity1);
            }

            string x = FoodCostSimulatorRepository.SaveFoodCostSimSiteRecipeData(recipeEntity, entity, recipeMOGMappingEntity1, recipeMOGMappingEntity2, entityHis, model.DishID, model.DishCode, requestData);
            return x;
        }
        //public string SaveRecipeData(RecipeData model, IList<RecipeMOGMappingData> baseRecipes, IList<RecipeMOGMappingData> mogs, RequestData requestData)
        //{
        //    Recipe entity = new Recipe();
        //    if (model.ID is null || model.ID == null)
        //    {
        //        var code = RecipeRepository.GetAll(requestData.SessionSectorName).OrderByDescending(z => z.RecipeCode).FirstOrDefault()?.RecipeCode;
        //        if (code is null)
        //            code = "RCP-00001";
        //        else
        //        {
        //            var lcode = code.Substring(0, 3);
        //            var rcode = code.Substring(4, 5);
        //            code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
        //        }
        //        model.RecipeCode = code;

        //    }
        //    var dup = RecipeRepository.Find(y => y.Name.Trim().ToLower() == model.Name.Trim().ToLower(), requestData.SessionSectorName).FirstOrDefault();
        //    if (dup != null && (model.ID == 0 || model.ID == null))
        //        return "Duplicate";
        //    List<RecipeMOGMapping> recipeMOGMappingEntity1 = new List<RecipeMOGMapping>();
        //    List<RecipeMOGMapping> recipeMOGMappingEntity2 = new List<RecipeMOGMapping>();
        //    EntityMapper.Map<RecipeData, Recipe>(model, entity);

        //    EntityMapper.Map<IList<RecipeMOGMappingData>, IList<RecipeMOGMapping>>(baseRecipes, recipeMOGMappingEntity1);
        //    EntityMapper.Map<IList<RecipeMOGMappingData>, IList<RecipeMOGMapping>>(mogs, recipeMOGMappingEntity2);
        //    if (recipeMOGMappingEntity1.Count == 0 && recipeMOGMappingEntity2.Count == 0 && !model.IsRecipeStatusChange)
        //    {
        //        // 1 for set pending
        //        entity.Status = 1;
        //    }
        //    string x = RecipeRepository.SaveRecipeData(entity, recipeMOGMappingEntity1, recipeMOGMappingEntity2, requestData);
        //    return x;
        //}
        public IList<RecipeMOGMappingData> GetSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode, string simCode, RequestData requestData)
        {
            List<RecipeMOGMappingData> result = new List<RecipeMOGMappingData>();
            var entityList = FoodCostSimulatorRepository.GetSiteMOGsByRecipeID(SiteCode, recipeID, recipeCode, simCode, requestData).ToList();
            EntityMapper.Map<IList<procGetFCSSiteMOGsByRecipeID_Result>, IList<RecipeMOGMappingData>>(entityList, result);
            return result;
        }
        public string SaveFCSAPLHistory(FoodCostSimAPLHistoryData entity, RequestData requestData)
        {
            var entity1 = new FoodCostSimAPLHistory();
            //var entityList = FoodCostSimulatorRepository.GetSiteMOGsByRecipeID(SiteCode, recipeID, recipeCode, simCode, requestData).ToList();
            EntityMapper.Map<FoodCostSimAPLHistoryData, FoodCostSimAPLHistory>(entity, entity1);
            var result = FoodCostSimulatorRepository.SaveFCSAPLHistory(entity1, requestData);
            return result;
        }
        public IList<APLMasterData> GetFCSAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, string simuCode, RequestData requestData)
        {
            List<APLMasterData> result = new List<APLMasterData>();
            var chkVersion = FoodCostSimulatorRepository.IsFCSAPLGet(simuCode, requestData);

            var entityList = FoodCostSimulatorRepository.GetFCSAPLMasterSiteDataList(articleNumber, mogCode, SiteCode, simuCode, requestData, chkVersion).ToList();
            EntityMapper.Map<IList<procGetFCSAPLMasterDataForSite_Result>, IList<APLMasterData>>(entityList, result);
            return result;
        }
        public IList<DishData> GetDishDataList(RequestData requestData, string siteCode,int itemId)
        {
            //List<DishData> result = new List<DishData>();
            var entityList = FoodCostSimulatorRepository.GetFCSDish(requestData, siteCode,itemId).ToList();
            //EntityMapper.Map<IList<FoodCostSimSiteDish>, IList<DishData>>(entityList, result);
            return entityList;
        }
    }
}
