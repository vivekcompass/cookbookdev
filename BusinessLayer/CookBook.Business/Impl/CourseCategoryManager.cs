﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using HSEQ.Business.Base.Impl;
using HSEQ.Business.Contract;
using HSEQ.Data.Course;
using HSEQ.DataAdapter.Factory;
using HSEQ.DataAdapter.Factory.Contract;

namespace HSEQ.Business.Impl
{
    [Export(typeof(ICourseCategory))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CourseCategoryManager : ImplBase, ICourseCategory
    {
        [Dependency]
        public ICourseCategoryRepository CourseCategoryRepository { get; set; }

        public IList<CourseCategoryMasterData> GetCourseCategories()
        {
            IList<CourseCategoryMasterData> categories = new List<CourseCategoryMasterData>();
            categories = MapEngine.Map<IList<CourseCategoryMasterData>>(CourseCategoryRepository.GetAll());
            return categories;
        }

        public bool SaveCourse(CourseCategoryMasterData courseCategory)
        {
            coursecategorymaster entity = new coursecategorymaster();
            entity = MapEngine.Map<coursecategorymaster>(courseCategory);
            return CourseCategoryRepository.Save(entity);
        }
    }
}
