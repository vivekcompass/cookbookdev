﻿using CookBook.DataAdapter.Base.Contracts;

namespace CookBook.DataAdapter.Factory.Common
{
    public partial class EmailBody : IKeyedModel<int>
    {
        public int Key
        {
            get { return this.ID; }
        }
    }
}
