﻿using CookBook.DataAdapter.Base.Contracts;

namespace CookBook.DataAdapter.Factory.Transaction
{
    public partial class Reason : IKeyedModel<int>
    {
        public int Key
        {
            get { return ID; }
        }
    }
}
