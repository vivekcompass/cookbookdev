﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookBook.DataAdapter.Base.Contracts;

namespace CookBook.DataAdapter.Factory.Transaction
{
    public partial class SiteDishContainerMapping : IKeyedModel<int>
    {
        public int Key
        {
            get { return ID; }
        }
    }
}
