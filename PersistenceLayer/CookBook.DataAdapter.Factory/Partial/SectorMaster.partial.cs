﻿using CookBook.DataAdapter.Base.Contracts;

namespace CookBook.DataAdapter.Factory.Common
{
    public partial class SectorMaster : IKeyedModel<int>
    {
        public int Key
        {
            get { return Id; }
        }
    }
}
