﻿using CookBook.DataAdapter.Base.Contracts;

namespace CookBook.DataAdapter.Factory.Common
{
    public partial class SmtpServerSetting : IKeyedModel<int>
    {
        public int Key
        {
            get { return this.SmtpServerSettingId; }
        }
    }
}
