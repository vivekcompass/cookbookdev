﻿using CookBook.DataAdapter.Base.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Common
{
    public partial class EmailNotification : IKeyedModel<long>
    {
        public long Key
        {
            get { return this.EmailNotificationID; }
        }
    }
}
