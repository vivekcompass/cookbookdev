﻿using CookBook.DataAdapter.Base.Contracts;

namespace CookBook.DataAdapter.Factory.Common
{
    public partial class UserRoleMaster : IKeyedModel<int>
    {
        public int Key
        {
            get { return this.UserRoleId; }
        }
    }
}
