﻿using CookBook.DataAdapter.Base.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Transaction
{    
    public partial class RangeType : IKeyedModel<int>
    {
        public int Key
        {
            get { return ID; }
        }
    }
}
