//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CookBook.DataAdapter.Factory.Transaction
{
    using System;
    
    public partial class procGetRecipeMOGNLevelFOrPrinting_Result
    {
        public string MOGCode { get; set; }
        public string MOGName { get; set; }
        public string UOMName { get; set; }
        public Nullable<decimal> MOGQty { get; set; }
    }
}
