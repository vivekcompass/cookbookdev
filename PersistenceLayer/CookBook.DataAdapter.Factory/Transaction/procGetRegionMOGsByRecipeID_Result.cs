//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CookBook.DataAdapter.Factory.Transaction
{
    using System;
    
    public partial class procGetRegionMOGsByRecipeID_Result
    {
        public int ID { get; set; }
        public int Recipe_ID { get; set; }
        public string RecipeName { get; set; }
        public Nullable<int> MOG_ID { get; set; }
        public string MOGCode { get; set; }
        public string MOGName { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<decimal> Yield { get; set; }
        public int UOM_ID { get; set; }
        public string UOMName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<decimal> CostPerUOM { get; set; }
        public Nullable<decimal> CostPerKG { get; set; }
        public Nullable<decimal> TotalCost { get; set; }
        public string UOMCode { get; set; }
        public Nullable<bool> IsMajor { get; set; }
        public Nullable<decimal> IngredientPerc { get; set; }
        public Nullable<decimal> SectorIngredientPerc { get; set; }
        public Nullable<decimal> SectorTotalCost { get; set; }
        public Nullable<decimal> SectorQuantity { get; set; }
        public string SubSectorCode { get; set; }
        public string AllergensName { get; set; }
        public Nullable<decimal> DKGTotal { get; set; }
        public decimal DKGValue { get; set; }
        public string NGAllergen { get; set; }
        public string NGRemark { get; set; }
    }
}
