//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CookBook.DataAdapter.Factory.Transaction
{
    using System;
    
    public partial class procGetAPLMasterDataforSector_Result
    {
        public int ArticleID { get; set; }
        public string ArticleNumber { get; set; }
        public string ArticleDescription { get; set; }
        public string UOM { get; set; }
        public string MerchandizeCategory { get; set; }
        public string MerchandizeCategoryDesc { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ArticleType { get; set; }
        public string Hierlevel3 { get; set; }
        public string MOGCode { get; set; }
        public string MOGName { get; set; }
        public Nullable<decimal> StandardCostPerKg { get; set; }
        public Nullable<decimal> ArticleCost { get; set; }
        public string SiteName { get; set; }
        public string SiteAlias { get; set; }
        public string SiteCode { get; set; }
    }
}
