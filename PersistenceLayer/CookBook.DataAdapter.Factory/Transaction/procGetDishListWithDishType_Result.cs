//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CookBook.DataAdapter.Factory.Transaction
{
    using System;
    
    public partial class procGetDishListWithDishType_Result
    {
        public int DishID { get; set; }
        public string DishName { get; set; }
        public Nullable<bool> Status { get; set; }
        public int DishType_ID { get; set; }
        public string DishType { get; set; }
    }
}
