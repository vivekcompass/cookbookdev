//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CookBook.DataAdapter.Factory.Transaction
{
    using System;
    using System.Collections.Generic;
    
    public partial class SiteItemDishMapping
    {
        public int ID { get; set; }
        public int Site_ID { get; set; }
        public int Item_ID { get; set; }
        public int Dish_ID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string SiteCode { get; set; }
        public string ItemCode { get; set; }
        public string DishCode { get; set; }
        public Nullable<decimal> ServedPortion { get; set; }
        public Nullable<decimal> StandardPortion { get; set; }
        public Nullable<decimal> ContractPortion { get; set; }
        public Nullable<int> Reason_ID { get; set; }
        public Nullable<decimal> StandardWeightPerUOM { get; set; }
        public string StandardUOMCode { get; set; }
        public Nullable<decimal> ServedWeightPerUOM { get; set; }
        public string ServedUOMCode { get; set; }
        public Nullable<decimal> ContractWeightPerUOM { get; set; }
        public string ContractUOMCode { get; set; }
        public string ReasonCode { get; set; }
        public string Colorcode { get; set; }
        public string NutritionData { get; set; }
    }
}
