﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{   
    [Export(typeof(IBedMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class BedMasterRepository : EFSynchronousRepository<int, BedMaster, CookBookCoreEntities>,
        IBedMasterRepository
    {
        protected override DbSet<BedMaster> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.BedMasters;
        }

        protected override BedMaster FindImpl(int key, DbSet<BedMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override BedMaster FindImplWithExpand(int key, DbSet<BedMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public List<procGetBedMaster_Result> GetBedMaster(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetBedMaster().ToList();
                }
            }
        }
        public List<procGetBedMasterBATCHHEADER_Result> GetBedMasterHISTORY(RequestData requestData, string username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetBedMasterBATCHHEADER(username).ToList();
                }
            }
        }

        public List<procGetBedMasterBATCHDETAILS_Result> GetBedMasterHISTORYDet(RequestData requestData, string username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetBedMasterBATCHDETAILS(username).ToList();
                }
            }
        }
        public List<procshowGetALLBedMasterHISTORYBATCHWISE_Result> GetshowGetALLBedMasterHISTORYBATCHWISE(RequestData requestData,  string batchno)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procshowGetALLBedMasterHISTORYBATCHWISE(null, batchno).ToList();
                }
            }
        }
        public string AddUpdateBedMasterList(RequestData request, string xml, string username)
        {
            string outres = string.Empty;

            //CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    //return coreEntities.ProgBulkUpdateXMLMOGAPL(xml, username).ToList();
                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    context.procBulkInsertXMLBedMaster(xml, username, myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;
                }
            }
        }
        public string SaveBedMasterStatus(RequestData request, int id, int username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    var getPm = context.BedMasters.FirstOrDefault(a => a.ID == id);
                    getPm.IsActive = getPm.IsActive ? false : true;
                    getPm.ModifiedBy = username;
                    getPm.ModifiedOn = DateTime.Now;
                    context.SaveChanges();
                }
            }
            return "Success";
        }
        public List<procshowGetALLHISTORYBedMaster_Result> GetALLHISTORYDetailsBedMaster(RequestData request)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    return context.procshowGetALLHISTORYBedMaster(null).ToList();
                }
            }
        }
    }
}
