﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IConceptType2Repository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ConceptType2Repository : EFSynchronousRepository<int, ConceptType2, CookBookCommonEntities>,
        IConceptType2Repository
    {
        protected override DbSet<ConceptType2> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.ConceptType2;
        }

        protected override ConceptType2 FindImpl(int key, DbSet<ConceptType2> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ConceptType2 FindImplWithExpand(int key, DbSet<ConceptType2> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
