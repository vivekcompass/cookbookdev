﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{    
    [Export(typeof(IProcessTypeMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ProcessTypeMasterRepository : EFSynchronousRepository<int, ProcessType, CookBookCoreEntities>,
        IProcessTypeMasterRepository
    {
        protected override DbSet<ProcessType> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.ProcessTypes;
        }

        protected override ProcessType FindImpl(int key, DbSet<ProcessType> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ProcessType FindImplWithExpand(int key, DbSet<ProcessType> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public List<procGetProcessTypeMaster_Result> GetProcessTypeMasterDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetProcessTypeMaster(null).ToList();
                }
            }
        }

        public string SaveProcessTypeMasterDataList(ProcessType model, RequestData requestData)
        {          
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.procSaveProcessTypeMaster(model.ID, model.Code, model.Name, model.IsActive,  requestData.SessionUserId,requestData.SessionUserId, DateTime.Now, DateTime.Now);
                    context.SaveChanges();

                    return "true";
                }
            }

        }
        public List<MogProcessTypeMapping> GetProcessMappingDataList(RequestData requestData)
        {
            CookBookCommonEntities commonEntities = new CookBookCommonEntities();
            var getList = commonEntities.MogProcessTypeMappings.Where(a => a.IsActive==true).ToList();
            return getList;
        }
        
    }
}
