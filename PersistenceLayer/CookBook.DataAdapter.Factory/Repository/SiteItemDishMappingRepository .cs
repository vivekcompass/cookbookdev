﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.Request;
using System.Xml.Linq;
using CookBook.DataAdapter.Base.EF;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(ISiteItemDishMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteItemDishMappingRepository : EFSynchronousRepository<int, SiteItemDishMapping, CookBookCoreEntities>,
        ISiteItemDishMappingRepository
    {
        protected override DbSet<SiteItemDishMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.SiteItemDishMappings;
        }

        protected override SiteItemDishMapping FindImpl(int key, DbSet<SiteItemDishMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SiteItemDishMapping FindImplWithExpand(int key, DbSet<SiteItemDishMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<SiteItemDishMapping> GetSiteItemDishMappingDataList(string itemcode, string siteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                //int ItemID = int.Parse(itemID);
            
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = from x in context.SiteItemDishMappings
                               where x.ItemCode == itemcode && x.SiteCode == siteCode && x.IsActive.Value
                               select x;
                    return list.ToList();
                }
            }
        }

        public IList<SiteItemDishMapping> GetMasterSiteItemDishMappingDataList(string SiteCode, RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {

               
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = from x in context.SiteItemDishMappings
                               where x.SiteCode == SiteCode
                               select x;
                    return list.ToList();
                }
            }
        }


        public string SaveSiteItemDishMappingList(IList<SiteItemDishMapping> model, int regionStatus, RequestData requestData)
        {
            string ItemCode = model[0].ItemCode;
            string siteCode = model[0].SiteCode;


            var xEle = new XElement("SiteItemDishMappings",
                from steItemDishMapping in model
                select new XElement("SiteItemDishMapping",
                             new XAttribute("ID", steItemDishMapping.ID),
                               new XElement("Site_ID", steItemDishMapping.Site_ID),
                               new XElement("Item_ID", steItemDishMapping.Item_ID),
                               new XElement("Dish_ID", steItemDishMapping.Dish_ID),
                               new XElement("IsActive", steItemDishMapping.IsActive),
                               new XElement("CreatedBy", steItemDishMapping.CreatedBy),
                               new XElement("CreatedOn", steItemDishMapping.CreatedOn),
                               new XElement("ModifiedBy", steItemDishMapping.ModifiedBy),
                               new XElement("ModifiedOn", steItemDishMapping.ModifiedOn),
                               new XElement("SiteCode", steItemDishMapping.SiteCode),
                               new XElement("ItemCode", steItemDishMapping.ItemCode),
                               new XElement("DishCode", steItemDishMapping.DishCode),
                               new XElement("ServedPortion", steItemDishMapping.ServedPortion),
                               new XElement("StandardPortion", steItemDishMapping.StandardPortion),
                               new XElement("ContractPortion", steItemDishMapping.ContractPortion),
                               new XElement("Reason_ID", steItemDishMapping.Reason_ID),
                               new XElement("StandardWeightPerUOM", steItemDishMapping.StandardWeightPerUOM),
                               new XElement("StandardUOMCode", steItemDishMapping.StandardUOMCode),
                               new XElement("ServedWeightPerUOM", steItemDishMapping.ServedWeightPerUOM),
                               new XElement("ServedUOMCode", steItemDishMapping.ServedUOMCode),
                               new XElement("ContractWeightPerUOM", steItemDishMapping.ContractWeightPerUOM),
                               new XElement("ContractUOMCode", steItemDishMapping.ContractUOMCode),
                               new XElement("ReasonCode", steItemDishMapping.ReasonCode),
                               new XElement("Colorcode", steItemDishMapping.Colorcode)
                           ));


            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    context.procSaveSiteItemDishMapping(ItemCode, requestData.SessionUserId, regionStatus, siteCode, xEle.ToString());
                    context.SaveChanges();
                }
            }
            return "true";
        }

        public IList<FoodBookNutritionData> GetFoodBookNutritionDataList(string sectorCode)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, GetSectorName(sectorCode)))
                {
                    var Results = MultipleResultSets
                      .MultipleResults(context, $"EXEC procGetChartwellsNutritionData '{sectorCode}'")
                      .AddResult<FoodBookNutritionData>()
                      .Execute();


                    var Output = Results[0] as FoodBookNutritionData[];
                    return Output.ToList();
                }
            }
        }

        private string GetSectorName(string sectorCode)
        {
            string sectorName = string.Empty;
            if (sectorCode == "SEC-0002")
            {
                sectorName = "FS - MANUFACTURING";
            }
            else if (sectorCode == "SEC-0003")
            {
                sectorName = "FS - CORE";
            }
            else if (sectorCode == "SEC-0001")
            {
                sectorName = "FS - GOOGLE";
            }
            else if (sectorCode == "SEC-0008")
            {
                sectorName = "HEALTHCARE";
            }
            else if (sectorCode == "SEC-0009")
            {
                sectorName = "HORECA";
            }
            else if (sectorCode == "SEC-0010")
            {
                sectorName = "EDUCATION";
            }
            return sectorName;
        }
    }
}
