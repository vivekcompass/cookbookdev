﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IFoodProgramDayPartMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class FoodProgramDayPartMappingRepository : EFSynchronousRepository<int, FoodProgramDayPartMapping, CookBookCoreEntities>,
        IFoodProgramDayPartMappingRepository
    {
        protected override DbSet<FoodProgramDayPartMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.FoodProgramDayPartMappings;
        }

        protected override FoodProgramDayPartMapping FindImpl(int key, DbSet<FoodProgramDayPartMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override FoodProgramDayPartMapping FindImplWithExpand(int key, DbSet<FoodProgramDayPartMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<FoodProgramDayPartMapping> GetFoodProgramDayPartMappingDataList(string foodCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = from x in context.FoodProgramDayPartMappings
                               where x.FoodProgramCode == foodCode && x.IsActive.Value
                               select x;
                    return list.ToList();
                }
            }
        }

    
        public string SaveFoodProgramDayPartMappingList(IList<FoodProgramDayPartMapping> model,  RequestData requestData)
        {
            string foodCode = model[0].FoodProgramCode;
            string dayPartCode = model[0].DayPartCode;
            //check karna hai
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                   
                    var list = from x in context.FoodProgramDayPartMappings
                               where  x.FoodProgramCode == foodCode && x.IsActive.Value
                               select x;
                    IList<FoodProgramDayPartMapping> deactivateItemList = list.ToList();
                   // var deactivateItemList = llist.ToList();
                    if (deactivateItemList != null && deactivateItemList.Count() > 0)
                    {
                        foreach (FoodProgramDayPartMapping m in deactivateItemList)
                        {
                            if (!String.IsNullOrWhiteSpace(m.FoodProgramCode))
                            {

                                m.IsActive = false;
                                m.ModifiedBy = requestData.SessionUserId; 
                                m.ModifiedOn = DateTime.Now;
                                context.FoodProgramDayPartMappings.AddOrUpdate(m);
                            }
                        }
                        context.SaveChanges();
                    }
                 
                   
                }
            }
            if (string.IsNullOrWhiteSpace(dayPartCode))
            {
                return "true";
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (FoodProgramDayPartMapping m in model)
                    {

                        m.IsActive = true;
                        var isItemExists = context.FoodProgramDayPartMappings.Where(item => item.FoodProgramCode == m.FoodProgramCode && item.DayPartCode==m.DayPartCode).FirstOrDefault();
                     
                        
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            isItemExists.ModifiedBy = requestData.SessionUserId; 
                            isItemExists.ModifiedOn = DateTime.Now;
                            isItemExists.IsActive = true;
                            context.FoodProgramDayPartMappings.AddOrUpdate(isItemExists);
                        }
                        else
                        {
                            m.CreatedOn = DateTime.Now;
                            m.CreatedBy = requestData.SessionUserId; 
                            m.ModifiedBy = null;
                            m.ModifiedOn = null;
                            m.ID = 0;
                            context.FoodProgramDayPartMappings.AddOrUpdate(m);
                        }


                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }
    }
}
