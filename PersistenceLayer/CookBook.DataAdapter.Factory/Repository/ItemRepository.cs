﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Data.Request;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Base.EF;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IItemRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ItemRepository : EFSynchronousRepository<int, Item, CookBookCoreEntities>,
        IItemRepository
    {
        protected override DbSet<Item> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.Items;
        }

        protected override Item FindImpl(int key, DbSet<Item> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override Item FindImplWithExpand(int key, DbSet<Item> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetItemList_Result> GetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetItemList(itemCode, foodProgramID, dietCategoryID, requestData.SessionSectorNumber, requestData.SiteCode).ToList();
                }
            }
        }


        public IList<procNationalGetItemList_Result> NationalGetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, RequestData requestData)
        {
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return cookBookCommon.procNationalGetItemList().ToList();
                }
            }
        }

        public IList<procGetSiteItemPriceHistory_Result> GetSiteItemPriceHistory(string siteCode, string itemCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetSiteItemPriceHistory(siteCode, itemCode).ToList();
                }
            }
        }
        public IList<procGetSiteUnitPrice_Result> GetSiteUnitPrice(string siteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetSiteUnitPrice(siteCode).ToList();
                }
            }
        }

        public IList<procGetSiteDayPartMapping_Result> GetSiteDayPartMapping(string siteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetSiteDayPartMapping(siteCode).ToList();
                }
            }
        }

        public IList<procGetDishImpactedItemList_Result> GetDishImpactedItemList(string dishCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetDishImpactedItemList(dishCode).ToList();
                }
            }
        }


        public string SaveList(IList<Item> model, RequestData requestData)
        {


            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (Item m in model)
                    {

                        context.Items.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<procGetRegionInheritedData_Result> GetRegionInheritedData(string RegionID, string SectorID, RequestData requestData)
        {
            int regionId = int.Parse(RegionID);
            int sectorId = int.Parse(SectorID);
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetRegionInheritedData(regionId, sectorId).ToList();
                }
            }
        }

        public IList<procGetItemListExport_Result> GetItemExportDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetItemListExport().ToList();
                }
            }
        }

        public SiteItemDropDownData GetSiteItemDropDownDataData(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    if (string.IsNullOrWhiteSpace(requestData.SiteCode))
                    {
                        var Results = MultipleResultSets
                     .MultipleResults(context, $"EXEC procGetItemDropDownDataOnLoad '{requestData.SessionSectorNumber}'")

                     .AddResult<SiteMasterData>()
                     .Execute();
                        var Output = new SiteItemDropDownData
                        {
                            SiteMasterData = Results[0] as SiteMasterData[]
                        };
                        return Output;
                    }
                    else if (!string.IsNullOrWhiteSpace(requestData.ItemCode))
                    {
                        var Results = MultipleResultSets
                       .MultipleResults(context, $"EXEC procGetSiteItemDataOnMapDish '{requestData.SessionSectorNumber}','{requestData.SiteCode}','{requestData.ItemCode}'")
                       .AddResult<DayPartData>()
                       .AddResult<ReasonData>()
                       .AddResult<UOMModuleMappingData>()
                       .AddResult<DishCategoryData>()
                       .AddResult<DishData>()
                       .AddResult<SiteDishCategoryMappingData>()
                       .AddResult<SiteItemDishMappingData>()
                       .AddResult<SiteDayPartMappingData>()
                       .Execute();
                        var Output = new SiteItemDropDownData
                        {
                            DayPartData = Results[0] as DayPartData[],
                            ReasonData = Results[1] as ReasonData[],
                            UOMModuleMappingData = Results[2] as UOMModuleMappingData[],
                            DishCategoryData = Results[3] as DishCategoryData[],
                            SiteDishData = Results[4] as DishData[],
                            SiteDishCategoryMappingData = Results[5] as SiteDishCategoryMappingData[],
                            SiteItemDishMappingData = Results[6] as SiteItemDishMappingData[],
                            SiteDayPartMappingData = Results[7] as SiteDayPartMappingData[],
                        };
                        return Output;
                    }
                    else
                    {
                        var Results = MultipleResultSets
                       .MultipleResults(context, $"EXEC procGetSiteItemDataOnGo '{requestData.SessionSectorNumber}','{requestData.SiteCode}'")
                       .AddResult<SiteItemInheritanceData>()
                       .Execute();
                        var Output = new SiteItemDropDownData
                        {
                            SiteItemInheritanceData = Results[0] as SiteItemInheritanceData[],
                        };
                        return Output;
                    }
                }
            }

        }

        //Krish 25-07-2022
        public SectorItemData GetSectorItemData(RequestData requestData, bool isAddEdit, bool onLoad)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    if (!isAddEdit)
                    {
                        //var Results = new List<System.Collections.IEnumerable>();
                        if (onLoad)
                        {
                            var Results = MultipleResultSets
                              .MultipleResults(context, $"EXEC procGetSectorItemDataOnLoad '{requestData.SessionSectorNumber}'")
                              .AddResult<DietCategoryData>()
                              //.AddResult<SubSectorData>()
                              //.AddResult<FoodProgramData>()
                              //.AddResult<CommonMasterData>()
                              //.AddResult<CommonMasterData>()
                              //.AddResult<TextureData>()
                              //.AddResult<DayPartData>()
                              //.AddResult<UOMModuleMappingData>()
                              //.AddResult<DishCategoryData>()
                              //.AddResult<DishData>()
                              ////.AddResult<ItemData>()
                              .Execute();


                            var Output = new SectorItemData
                            {
                                DietCategoryData = Results[0] as DietCategoryData[],
                                //SubSectorData = Results[1] as SubSectorData[],
                                //FoodProgramData = Results[2] as FoodProgramData[],
                                //ItemSectorMasterData = Results[3] as CommonMasterData[],
                                //MenuType2MasterData = Results[4] as CommonMasterData[],
                                //TextureData = Results[5] as TextureData[],
                                //DayPartData = Results[6] as DayPartData[],

                                //UOMModuleMappingData = Results[7] as UOMModuleMappingData[],
                                //DishCategoryData = Results[8] as DishCategoryData[],
                                //DishData = Results[9] as DishData[],
                                ////ItemData = Results[10] as ItemData[]
                            };
                            return Output;
                        }
                        else
                        {
                            var Results = MultipleResultSets
                              .MultipleResults(context, $"EXEC procGetSectorItemDataOnLoad '{requestData.SessionSectorNumber}'")
                              .AddResult<DietCategoryData>()
                              .AddResult<SubSectorData>()
                              .AddResult<FoodProgramData>()
                              .AddResult<CommonMasterData>()
                              .AddResult<CommonMasterData>()
                              .AddResult<TextureData>()
                              .AddResult<DayPartData>()
                              .AddResult<UOMModuleMappingData>()
                              .AddResult<DishCategoryData>()
                              .AddResult<DishData>()
                              //.AddResult<ItemData>()
                              .Execute();


                            var Output = new SectorItemData
                            {
                                DietCategoryData = Results[0] as DietCategoryData[],
                                SubSectorData = Results[1] as SubSectorData[],
                                FoodProgramData = Results[2] as FoodProgramData[],
                                ItemSectorMasterData = Results[3] as CommonMasterData[],
                                MenuType2MasterData = Results[4] as CommonMasterData[],
                                TextureData = Results[5] as TextureData[],
                                DayPartData = Results[6] as DayPartData[],

                                UOMModuleMappingData = Results[7] as UOMModuleMappingData[],
                                DishCategoryData = Results[8] as DishCategoryData[],
                                DishData = Results[9] as DishData[],
                                //ItemData = Results[10] as ItemData[]
                            };
                            return Output;
                        }
                    }
                    else
                    {
                        var Results = MultipleResultSets
                         .MultipleResults(context, $"EXEC procGetSectorItemDataOnAddEdit")
                         .AddResult<VisualCategoryData>()
                         .AddResult<ServewareData>()
                         .AddResult<ItemType1Data>()
                         .AddResult<ItemType2Data>()
                         .AddResult<ConceptType1Data>()
                         .AddResult<ConceptType2Data>()
                         .AddResult<ConceptType3Data>()
                         .AddResult<ConceptType4Data>()
                         .AddResult<ConceptType5Data>()
                         .Execute();
                        var Output = new SectorItemData
                        {
                            VisualCategoryData = Results[0] as VisualCategoryData[],
                            ServewareData = Results[1] as ServewareData[],
                            ItemType1Data = Results[2] as ItemType1Data[],
                            ItemType2Data = Results[3] as ItemType2Data[],
                            ConceptType1Data = Results[4] as ConceptType1Data[],
                            ConceptType2Data = Results[5] as ConceptType2Data[],
                            ConceptType3Data = Results[6] as ConceptType3Data[],

                            ConceptType4Data = Results[7] as ConceptType4Data[],
                            ConceptType5Data = Results[8] as ConceptType5Data[]

                        };
                        return Output;
                    }
                }
            }

        }

        //Krish 30-07-2022
        public RegionItemData GetRegionItemData(RequestData requestData, bool isSectorUser, int regionId = 0, bool gridOnly = false)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var userId = requestData.SessionUserId;
                    var secNumber = requestData.SessionSectorNumber;
                    if (regionId == 0)
                    {
                        if (!isSectorUser)
                        {
                            //var Results = new List<System.Collections.IEnumerable>();

                            var Results = MultipleResultSets
                              .MultipleResults(context, $"EXEC procGetRegionItemDataOnLoad '{secNumber}', '{userId}'")
                              .AddResult<SubSectorData>()
                              .AddResult<RegionsDetailsByUserIdData>()
                               .AddResult<UOMModuleMappingData>()
                              .AddResult<DishCategoryData>()
                              .AddResult<FoodProgramData>()
                              //.AddResult<ItemData>()
                              .AddResult<DietCategoryData>()

                              .Execute();


                            var Output = new RegionItemData
                            {
                                SubSectorData = Results[0] as SubSectorData[],
                                RegionsDetailsByUserIdData = Results[1] as RegionsDetailsByUserIdData[],
                                UOMModuleMappingData = Results[2] as UOMModuleMappingData[],
                                DishCategoryData = Results[3] as DishCategoryData[],
                                FoodProgramData = Results[4] as FoodProgramData[],
                                //ItemData = Results[5] as ItemData[],
                                DietCategoryData = Results[5] as DietCategoryData[],

                            };
                            return Output;

                        }
                        else
                        {
                            var Results = MultipleResultSets
                              .MultipleResults(context, $"EXEC procGetRegionItemDataOnLoad '{secNumber}', 0")
                              .AddResult<SubSectorData>()
                              .AddResult<RegionMasterData>()
                               .AddResult<UOMModuleMappingData>()
                              .AddResult<DishCategoryData>()
                              .AddResult<FoodProgramData>()
                              .AddResult<ItemData>()
                              .AddResult<DietCategoryData>()

                              .Execute();


                            var Output = new RegionItemData
                            {
                                SubSectorData = Results[0] as SubSectorData[],
                                RegionMasterData = Results[1] as RegionMasterData[],
                                UOMModuleMappingData = Results[2] as UOMModuleMappingData[],
                                DishCategoryData = Results[3] as DishCategoryData[],
                                FoodProgramData = Results[4] as FoodProgramData[],
                                ItemData = Results[5] as ItemData[],
                                DietCategoryData = Results[6] as DietCategoryData[],

                            };
                            return Output;
                        }
                    }
                    else
                    {
                        if (gridOnly)
                        {
                            var Results = MultipleResultSets
                             .MultipleResults(context, $"EXEC procGetRegionItemDataGridOnly '{secNumber}', '{regionId}'")
                              .AddResult<RegionItemInheritanceMappingData>()
                                  .AddResult<string>()
                                  .AddResult<ItemData>()
                             .Execute();


                            var Output = new RegionItemData
                            {                                
                                RegionItemInheritanceMappingData = Results[0] as RegionItemInheritanceMappingData[],
                                UniqueMappedItemCodesFromRegion = Results[1] as string[],
                                ItemData = Results[2] as ItemData[],
                            };
                            return Output;
                        }
                        else
                        {
                            var Results = MultipleResultSets
                                  .MultipleResults(context, $"EXEC procGetRegionItemDataOnGo '{secNumber}', '{regionId}'")
                                  .AddResult<DishData>()
                                  .AddResult<DishData>()
                                  // .AddResult<RegionItemInheritanceMappingData>()
                                  //.AddResult<string>()
                                  //.AddResult<ItemData>()

                                  .Execute();


                            var Output = new RegionItemData
                            {
                                RegionDishMasterData = Results[0] as DishData[],
                                DishData = Results[1] as DishData[],
                                //RegionItemInheritanceMappingData = Results[2] as RegionItemInheritanceMappingData[],
                                //UniqueMappedItemCodesFromRegion = Results[3] as string[],
                                //ItemData = Results[4] as ItemData[],
                            };
                            return Output;
                        }
                    }
                }
            }

        }
    }
}

