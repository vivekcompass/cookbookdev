﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Base.DataEnums;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(ICapringMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CapringMasterRepository : EFSynchronousRepository<int, CapringMaster, CookBookCommonEntities>,
        ICapringMasterRepository
    {
        protected override DbSet<CapringMaster> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.CapringMasters;
        }

        protected override CapringMaster FindImpl(int key, DbSet<CapringMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override CapringMaster FindImplWithExpand(int key, DbSet<CapringMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

       

       public List<CapringMaster> GetCapringMasterDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.CapringMasters.ToList();
                }
            }
        }
    }
}
