﻿using CookBook.Aspects.Constants;
using CookBook.Aspects.Factory;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using CookBook.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using CookBook.Data.MasterData;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IEmailRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class EmailRepository : EFSynchronousRepository<long, EmailNotification, CookBookCommonEntities>,
       IEmailRepository
    {
        protected override DbSet<EmailNotification> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.EmailNotifications;
        }

        protected override EmailNotification FindImpl(long key, DbSet<EmailNotification> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.EmailNotificationID == key);
        }

        protected override EmailNotification FindImplWithExpand(long key, DbSet<EmailNotification> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.EmailNotificationID == key);
        }



        public void SendPendingEmail(RequestData requestData)
        {
            bool isMailSent = false;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    context.Configuration.LazyLoadingEnabled = false;

                    IEnumerable<EmailNotification> pendingEmails = context.EmailNotifications.Where(x => x.IsDelivered == false).ToList();


                    SmtpServerSetting smtpSetting = context.SmtpServerSettings.FirstOrDefault();

                    if (smtpSetting != null)
                    {
                        if (pendingEmails != null)
                        {
                            foreach (var email in pendingEmails)
                            {
                                isMailSent = false;
                                isMailSent = SendEmail(email, smtpSetting);

                                if (isMailSent) //Mail sent successfully
                                {
                                    var data = context.EmailNotifications.Where(m => m.EmailNotificationID == email.EmailNotificationID).FirstOrDefault();
                                    data.IsDelivered = true;
                                    context.SaveChanges();
                                }
                                else //Mail sent fail
                                {
                                }
                            }
                        }
                    }
                }


            }

        }

        public void SendMOGEmail(MOGData model,RequestData requestData)
        {
            bool isMailSent = false;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    context.Configuration.LazyLoadingEnabled = false;
                    SmtpServerSetting smtpSetting = context.SmtpServerSettings.FirstOrDefault();
                    if (smtpSetting != null)
                    {
                        isMailSent = false;
                        EmailNotification email = new EmailNotification();
                        EmailBody emailBody = new EmailBody();
                        emailBody = GetEmailDetails("MOGNameChange",requestData);
                        emailBody.EmailBodyData= emailBody.EmailBodyData.Replace("###OLDMOG", model.OldMOGName);
                        emailBody.EmailBodyData = emailBody.EmailBodyData.Replace("###NEWMOG", model.Name);
                        email.EmailBody = emailBody.EmailBodyData;
                        email.ToEmail = emailBody.ToEmail;
                        email.CcEmail = emailBody.CcEmail;
                        email.Subject = emailBody.EmailSubject;
                        isMailSent = SendEmail(email, smtpSetting);
                    }
                }
            }
        }

        public void SendNewMOGEmail(MOGData model, RequestData requestData)
        {
            bool isMailSent = false;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.Configuration.LazyLoadingEnabled = false;
                    SmtpServerSetting smtpSetting = context.SmtpServerSettings.FirstOrDefault();
                    if (smtpSetting != null)
                    {
                        isMailSent = false;
                        EmailNotification email = new EmailNotification();
                        EmailBody emailBody = new EmailBody();
                        emailBody = GetEmailDetails("NewMOG", requestData);
                        emailBody.EmailBodyData = emailBody.EmailBodyData.Replace("###MOGName", model.Name);
                        email.EmailBody = emailBody.EmailBodyData;
                        email.ToEmail = emailBody.ToEmail;
                        email.CcEmail = emailBody.CcEmail;
                        email.Subject = emailBody.EmailSubject;
                        isMailSent = SendEmail(email, smtpSetting);
                    }
                }
            }
        }

        public void SendRecipeEmail(RecipeData model, RequestData requestData)
        {
            bool isMailSent = false;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    context.Configuration.LazyLoadingEnabled = false;
                    SmtpServerSetting smtpSetting = context.SmtpServerSettings.FirstOrDefault();
                    if (smtpSetting != null)
                    {
                        isMailSent = false;
                        EmailNotification email = new EmailNotification();
                        EmailBody emailBody = new EmailBody();
                        if (model.IsRecipeStatusChange)
                        {
                            emailBody = GetEmailDetails("BaseRecipeDeactivate",requestData);
                        }
                        else
                        {
                            if (model.IsRecipeNameChange)
                            {
                                emailBody = GetEmailDetails("BaseRecipeNameChange",requestData);
                                emailBody.EmailBodyData = emailBody.EmailBodyData.Replace("###OLDBR", model.OldRecipeName);
                                emailBody.EmailBodyData = emailBody.EmailBodyData.Replace("###NEWBR", model.Name);
                            }
                            else
                            {
                                emailBody = GetEmailDetails("BaseRecipeConfigChange",requestData);
                            }
                        }
                        email.EmailBody = emailBody.EmailBodyData;
                        email.ToEmail = emailBody.ToEmail;
                        email.CcEmail = emailBody.CcEmail;
                        email.Subject = emailBody.EmailSubject;
                        isMailSent = SendEmail(email, smtpSetting);
                    }
                }
            }
        }

        private bool SendEmail(EmailNotification emailNotification, SmtpServerSetting smtpSetting)
        {
            bool isSuccess = false;
            string fileName = string.Empty;
            try
            {
                //Sets the from address for this e-mail message. 
                string fromEmail = string.Empty;
                string fromName = string.Empty;

                fromEmail = smtpSetting.FromEmail;
                fromName = smtpSetting.FromName;

                //Setup email header
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(fromEmail, fromName);

                mailMessage.To.Add(new MailAddress(emailNotification.ToEmail));

                // sets the message subject.
                mailMessage.Subject = emailNotification.Subject;

                if (emailNotification.CcEmail != "" && emailNotification.CcEmail != null)
                {
                    if (!string.IsNullOrEmpty(emailNotification.CcEmail))
                    {
                        string[] CCId = emailNotification.CcEmail.Split(',');

                        foreach (string CCEmail in CCId)
                        {
                            mailMessage.CC.Add(new MailAddress(CCEmail)); //Adding Multiple CC email Id
                        }
                    }
                    else
                    {
                        mailMessage.CC.Add(new MailAddress(emailNotification.CcEmail));
                    }
                }
                // sets the message body. 
                mailMessage.Body = emailNotification.EmailBody;

                    // sets a value indicating whether the mail message body is in Html. 
                    mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                    mailMessage.IsBodyHtml = true;


                    //Attach file
                    if (!string.IsNullOrEmpty(emailNotification.Attachment))
                    {
                        mailMessage.Attachments.Add(new Attachment(emailNotification.Attachment));
                    }

                    // SmtpClient Class Allows applications to send e-mail by using the Simple Mail Transfer Protocol (SMTP).
                    SmtpClient smtpClient = new SmtpClient();

                    //Specifies how email messages are delivered. Here Email is sent through the network to an SMTP server.
                    smtpClient.Credentials = new System.Net.NetworkCredential(smtpSetting.UserName, smtpSetting.Password);
                    smtpClient.Port = Convert.ToInt32(smtpSetting.PortNumber);
                    smtpClient.Host = smtpSetting.ServerName;
                    smtpClient.EnableSsl = smtpSetting.IsSsl.Value;

                    LogTraceFactory.WriteLogWithCategory("Smtp settings configured and mail is composed.", LogTraceCategoryNames.Tracing);
                    //Let's send it
                    smtpClient.Send(mailMessage);
                    isSuccess = true;
                    LogTraceFactory.WriteLogWithCategory("Mail is sent to the respective addresses.", LogTraceCategoryNames.Tracing);
                    // Do cleanup
                    mailMessage.Dispose();
                    smtpClient = null;
                    //if (!string.IsNullOrEmpty(fileName))
                    //    File.Delete(fileName);
                }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("Exception occured while sending mail:" + ex.Message, LogTraceCategoryNames.Tracing);
                isSuccess = false;
                //    failedMessage = ex.Message;
                //if (!string.IsNullOrEmpty(fileName))
                //    File.Delete(fileName);
            }
            return isSuccess;
        }

        public EmailBody GetEmailDetails(string type, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.EmailBodies.Where(m => m.ModuleType == type).FirstOrDefault();
                }
            }
        }

        public SmtpServerSetting GetSmtpServerSettings(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.SmtpServerSettings.FirstOrDefault();
                }
            }
        }

        public void SendGOVOOffSiteItemAutoInheritEmail(ItemData model, RequestData requestData)
        {
            bool isMailSent = false;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.Configuration.LazyLoadingEnabled = false;
                    SmtpServerSetting smtpSetting = context.SmtpServerSettings.FirstOrDefault();
                    if (smtpSetting != null)
                    {
                        isMailSent = false;
                        EmailNotification email = new EmailNotification();
                        EmailBody emailBody = new EmailBody();
                        emailBody = GetEmailDetails("GOVOOFFSiteAutoItemInherit", requestData);
                        emailBody.EmailBodyData = emailBody.EmailBodyData.Replace("###ITEMNAME", model.ItemName);
                        email.EmailBody = emailBody.EmailBodyData;
                        email.ToEmail = emailBody.ToEmail;
                        email.CcEmail = emailBody.CcEmail;
                        email.Subject = emailBody.EmailSubject;
                        isMailSent = SendEmail(email, smtpSetting);
                    }
                }
            }
        }
    }
}
