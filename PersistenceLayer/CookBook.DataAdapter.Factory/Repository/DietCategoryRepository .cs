﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IDietCategoryRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DietCategoryRepository : EFSynchronousRepository<int, DietCategory, CookBookCommonEntities>,
        IDietCategoryRepository
    {
        protected override DbSet<DietCategory> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.DietCategories;
        }

        protected override DietCategory FindImpl(int key, DbSet<DietCategory> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override DietCategory FindImplWithExpand(int key, DbSet<DietCategory> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
