﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IItemDishCategoryMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ItemDishCategoryMappingRepository : EFSynchronousRepository<int, ItemDishCategoryMapping, CookBookCoreEntities>,
        IItemDishCategoryMappingRepository
    {
        protected override DbSet<ItemDishCategoryMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.ItemDishCategoryMappings;
        }

        protected override ItemDishCategoryMapping FindImpl(int key, DbSet<ItemDishCategoryMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ItemDishCategoryMapping FindImplWithExpand(int key, DbSet<ItemDishCategoryMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<ItemDishCategoryMapping> GetItemDishCategoryMappingDataList(string itemCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = from x in context.ItemDishCategoryMappings
                               where x.ItemCode == itemCode && x.IsActive.Value
                              select x;
                             
                    return list.ToList();
                }
            }
        }

        public IList<ItemDishCategoryMapping> GetNationalItemDishCategoryMappingDataList(string itemCode, string sectorcode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                if (sectorcode == "FS - MANUFACTURING")
                {
                    sectorcode = "FS - MANUFACTURING";
                }
                else if (sectorcode == "FS - CORE")
                {
                    sectorcode = "FS - CORE";
                }
                else if (sectorcode == "FS - GOOGLE")
                {
                    sectorcode = "FS - GOOGLE";
                }
                using (var context = GetContext(transaction, sectorcode))
                {
                    var list = from x in context.ItemDishCategoryMappings
                               where x.ItemCode == itemCode && x.IsActive.Value
                               select x;

                    return list.ToList();
                }
            }
        }

        public string SaveItemDishCategoryMappingList(IList<ItemDishCategoryMapping> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                int ItemID = model[0].Item_ID;
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = from x in context.ItemDishCategoryMappings
                               where x.Item_ID == ItemID && x.IsActive.Value
                               select x;
                    IList<ItemDishCategoryMapping> llist = list.ToList();
                    bool flag = false;
                    foreach (ItemDishCategoryMapping m in llist)
                    {
                        flag = false;
                        foreach (ItemDishCategoryMapping mo in model)
                        {
                            if(mo.ID == m.ID)
                            {
                                flag = true;
                                break;
                            }
                        }
                        if (!flag)
                        {
                            m.IsActive = false;
                            m.ModifiedOn = DateTime.Now;
                            m.ModifiedBy = m.CreatedBy;
                            context.ItemDishCategoryMappings.AddOrUpdate(m);
                        }
                         
                    }

                    context.SaveChanges();
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (ItemDishCategoryMapping m in model)
                    {
                        m.IsActive = true;
                        var isItemExists = context.ItemDishCategoryMappings.Where(item => item.ItemCode == m.ItemCode && item.DishCategoryCode == m.DishCategoryCode).FirstOrDefault();
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            m.ModifiedOn = DateTime.Now;
                            m.ModifiedBy = m.CreatedBy;
                            m.ID = isItemExists.ID;
                        }
                        else
                        {
                            m.CreatedOn = DateTime.Now;
                            m.ID = 0;
                        }
                        context.ItemDishCategoryMappings.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<string> GetUniqueMappedItemCodesFromItem(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {


                    var list = (from idm in context.ItemDishCategoryMappings
                                where idm.IsActive.Value
                                select idm.ItemCode).Distinct().ToList();

                    return list.ToList();
                }
            }
        }
    }
}
