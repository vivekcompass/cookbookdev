﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System.Security.Cryptography.X509Certificates;
using System.Data.Entity.Migrations;
using System;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IUserMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserMasterRepository : EFSynchronousRepository<int, UserMaster, CookBookCommonEntities>,
        IUserMasterRepository
    {
        protected override DbSet<UserMaster> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.UserMasters;
        }

        protected override UserMaster FindImpl(int key, DbSet<UserMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.UserId == key);
        }

        protected override UserMaster FindImplWithExpand(int key, DbSet<UserMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.UserId == key);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public procGetUserDataByUsername_Result GetUserDetailsByUsername(string username, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,null))
                {
                    try
                    {
                        return context.procGetUserDataByUsername(username).FirstOrDefault();
                    }
                   catch(Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }

        public IList<procGetSectorDataByUserID_Result> GetSectorDataByUserID(int userID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetSectorDataByUserID(userID).ToList();
                }
            }
        }

        public IList<procGetUserModules_Result> GetModulesByUserID(int userID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetUserModules(userID).ToList();
                }
            }
        }

        public IList<procGetUserModulePermission_Result> GetUserPermissionsByModuleCode(int userID, string moduleCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetUserModulePermission(userID,moduleCode).ToList();
                }
            }
        }


        public string ChangeSectorData (int UserID, string sectorNumber, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var m = context.UserMasters.FirstOrDefault(x => x.UserId == UserID);
                    m.SectorNumber = sectorNumber;
                    m.ModifiedBy = UserID.ToString();
                    m.ModifiedDate = DateTime.Now;
                    context.UserMasters.AddOrUpdate(m);
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public string ChangeUserRoleData(int UserID, int userRoleID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var m = context.UserMasters.FirstOrDefault(x => x.UserId == UserID);
                    m.UserRoleId = userRoleID;
                    m.ModifiedBy = UserID.ToString();
                    m.ModifiedDate = DateTime.Now;
                    context.UserMasters.AddOrUpdate(m);
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<procGetRoleDataByUserID_Result> GetUserRoleDataByUserID(int userID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetRoleDataByUserID(userID).ToList();
                }
            }
        }
    }
}
