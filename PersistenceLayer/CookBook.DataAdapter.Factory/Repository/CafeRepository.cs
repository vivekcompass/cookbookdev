﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Base.DataEnums;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(ICafeRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CafeRepository : EFSynchronousRepository<int, Cafe, CookBookCommonEntities>,
        ICafeRepository
    {
        protected override DbSet<Cafe> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.Cafes;
        }

        protected override Cafe FindImpl(int key, DbSet<Cafe> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override Cafe FindImplWithExpand(int key, DbSet<Cafe> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public string SaveList(IList<Cafe> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (Cafe m in model)
                    {

                        context.Cafes.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<procGetCafeList_Result> GetCafeDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetCafeList("", requestData.SessionSectorNumber).ToList();
                }
            }
        }
    }
}
