﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Data.Request;
using CookBook.Data.MasterData;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Aspects.Constants;
using CookBook.Data.UserData;
using System.Data.SqlClient;

namespace CookBook.DataAdapter.Factory.Repository
{
    [Export(typeof(IFoodCostSimulatorRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]

    public class FoodCostSimulatorRepository : EFSynchronousRepository<int, FoodCostSimulation, CookBookCoreEntities>,
        IFoodCostSimulatorRepository
    {
        protected override DbSet<FoodCostSimulation> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.FoodCostSimulations;
        }
        protected override FoodCostSimulation FindImpl(int key, DbSet<FoodCostSimulation> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override FoodCostSimulation FindImplWithExpand(int key, DbSet<FoodCostSimulation> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<DayPartData> GetDayPartBySiteList(int siteId, RequestData requestData)
        {
            //string sector = requestData.SessionSectorNumber;
            CookBookCommonEntities commonContext = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    string sitecode = commonContext.SiteMasters.FirstOrDefault(a => a.SiteID == siteId).SiteCode;
                    var getList = (from a in context.DayParts
                                   join b in context.SiteDayPartMappings on a.DayPartCode equals b.DayPartCode
                                   where b.SiteCode == sitecode && b.IsActive == true
                                   select new DayPartData
                                   {
                                       ID = a.ID,
                                       Name = a.Name,
                                       DayPartCode = a.DayPartCode
                                   }).ToList();
                    return getList;
                }
            }
        }

        public IList<FoodCostSimulation> GetSimulationDataListWithPaging(RequestData requestData, UserMasterResponseData userData, int pageIndex, int totalRecordsPerPage)
        {
            CookBookCommonEntities commonContext = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var getList = new List<FoodCostSimulation>();
                    if (userData.UserRoleName.ToLower().Contains("admin") || userData.UserRoleName.ToLower().Contains("team"))
                        getList = (from a in context.FoodCostSimulations
                                   select a).OrderByDescending(a => a.CreateOn).ToList();
                    else
                    {
                        var siteList = commonContext.UserSiteMappings.Where(a => a.UserId == requestData.SessionUserId);
                        getList = (from a in context.FoodCostSimulations
                                   where siteList.Any(x => x.SiteCode == a.SiteCode)
                                   select a).OrderByDescending(a => a.CreateOn).ToList();
                    }
                    //select new SimulationList
                    //{
                    //    ID = a.ID,
                    //    CreateOn = a.CreateOn.ToString("dd MMM yyy hh:mm tt"),
                    //    Name = a.Name,
                    //    SimulationCode = a.SimulationCode,
                    //    Status = a.Status,
                    //    Version = a.Version,
                    //    IsActive = a.IsActive
                    //});
                    return getList;
                }
            }
        }
        public IList<SimulationItemsData> GetItemsForSimulation(RequestData requestData, int siteId, int regionId, List<int> daypartId,bool checksiteItem)
        {
            CookBookCommonEntities commonContext = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    string dpCode = string.Empty;
                    string regCode = string.Empty;
                    string siteCode = string.Empty;

                    var getItems = new List<SimulationItemsData>();
                    //var getItems = (from it in context.Items
                    //                    //join sidp in context.SiteItemDayPartMappings on it.ItemCode equals sidp.ItemCode
                    //                    //where sidp.IsActive == true
                    //                where it.IsActive == true
                    //                select new SimulationItemsData
                    //                {
                    //                    ItemID = it.ID,
                    //                    ItemCode = it.ItemCode,
                    //                    ItemName = it.Name,
                    //                    DayPartCode = "",
                    //                    //DishCategoryList = GetDishCategoriesFromItem(it.ID, context),
                    //                    RegionID = regionId,
                    //                    //SiteCode = sidp.SiteCode
                    //                });
                    //if (regionId > 0)
                    //    getItems = (from a in getItems
                    //                where a.RegionID == regionId
                    //                select a);
                    if (siteId > 0)
                    {
                        var sitecode = commonContext.SiteMasters.FirstOrDefault(a => a.SiteID == siteId);
                        //getItems = (from a in getItems
                        //            join b in context.SiteItemDayPartMappings on a.ItemCode equals b.ItemCode
                        //            where b.SiteCode == sitecode.SiteCode
                        //            select a);
                        //14th April
                        //check and insert updated site dishes
                        if (checksiteItem)
                        {
                            var result2 = context.procSaveFCSSiteDish(sitecode.SiteCode, false);
                        }

                        if (daypartId.Count > 0 && daypartId.FirstOrDefault() != 0)
                        {
                            List<SimulationItemsData> mappedItems = new List<SimulationItemsData>();
                            foreach (var item in daypartId)
                            {
                                if (item != 0)
                                {
                                    var dpcode = context.DayParts.FirstOrDefault(a => a.ID == item);

                                    //Check in SiteItemInheritanceMappings table
                                    var siteItemInheritace = (from b in context.SiteItemInheritanceMappings
                                                              join a in context.Items on b.Item_ID equals a.ID
                                                              where b.SiteCode == sitecode.SiteCode && b.IsActive == true
                                                              select a).ToList();

                                    var getMappedItems = (from a in siteItemInheritace
                                                          join b in context.SiteItemDayPartMappings on a.ItemCode equals b.ItemCode
                                                          join c in context.DayParts on b.DayPartCode equals c.DayPartCode
                                                          where a.IsActive == true && b.IsActive == true && b.SiteCode == sitecode.SiteCode && b.DayPartCode == dpcode.DayPartCode
                                                          select new SimulationItemsData
                                                          {
                                                              ItemID = a.ID,
                                                              ItemCode = a.ItemCode,
                                                              ItemName = a.Name,
                                                              DayPartCode = b.DayPartCode,
                                                              DayPartID = c.ID,
                                                              DayPartName = c.Name,
                                                              //DayPartCode = sidp.DayPartCode,
                                                              RegionID = regionId,
                                                              SiteCode = b.SiteCode
                                                              //DishCategoryList = GetDishCategoriesFromItem(a.ID, context)
                                                              //SiteCode = sidp.SiteCode
                                                          }).OrderBy(a => a.ItemName).ToList();

                                    //Duplicate Check for items
                                    //if (mappedItems.Count > 0)
                                    //{
                                    //    foreach (var mitems in getMappedItems)
                                    //    {
                                    //        if (!mappedItems.Any(a => a.ItemID == mitems.ItemID))
                                    //            mappedItems.Add(mitems);
                                    //    }
                                    //}
                                    //else
                                    mappedItems.AddRange(getMappedItems);
                                }
                            }
                            //if (mappedItems.Count > 0)
                            return FetchDishCategories(mappedItems, sitecode.SiteCode, context);
                            //return mappedItems;
                        }
                    }

                    return FetchDishCategories(getItems.ToList(), "", context);
                    //var getList = (from it in context.Items
                    //               join sidp in context.SiteItemDayPartMappings on it.ItemCode equals sidp.ItemCode
                    //               where sidp.IsActive==true
                    //               select new SimulationItemsData
                    //               {
                    //                   ItemID = it.ID,
                    //                   ItemCode = it.ItemCode,
                    //                   ItemName = it.Name,
                    //                   DayPartCode = sidp.DayPartCode,
                    //                   RegionID = regionId,
                    //                   SiteCode = sidp.SiteCode
                    //               });

                    //if (siteId > 0)
                    //{
                    //    siteCode = commonContext.SiteMasters.FirstOrDefault(a => a.SiteID == siteId).SiteCode;
                    //    getList = getList.Where(a => a.SiteCode == siteCode);
                    //    if (daypartId.Count > 0)
                    //    {
                    //        List<SimulationItemsData> dpItems = new List<SimulationItemsData>();
                    //        foreach (var item in daypartId)
                    //        {
                    //            if (item != 0)
                    //            {
                    //                dpCode = context.DayParts.FirstOrDefault(a => a.ID == item).DayPartCode;
                    //                getList = getList.Where(a => a.DayPartCode == dpCode);
                    //                dpItems.AddRange(getList);
                    //            }
                    //        }
                    //        return dpItems.Distinct().ToList();
                    //    }                        
                    //}
                    //else
                    //{
                    //    // Get sites from region
                    //    var getSites = commonContext.SiteMasters.Where(a => a.RegionID == regionId && a.IsActive == true).ToList();
                    //    List<SimulationItemsData> regionItems = new List<SimulationItemsData>();
                    //    foreach (var item in getSites)
                    //    {
                    //        var chkSite = getList.Where(a => a.SiteCode == item.SiteCode);                            
                    //        regionItems.AddRange(chkSite);
                    //    }
                    //    if (daypartId.Count > 0)
                    //    {
                    //        List<SimulationItemsData> dpItems = new List<SimulationItemsData>();
                    //        foreach (var item in daypartId)
                    //        {
                    //            if (item != 0)
                    //            {
                    //                dpCode = context.DayParts.FirstOrDefault(a => a.ID == item).DayPartCode;
                    //                regionItems = regionItems.Where(a => a.DayPartCode == dpCode).ToList();
                    //                dpItems.AddRange(getList);
                    //            }
                    //        }
                    //        return dpItems.Distinct().ToList();
                    //    }
                    //    return regionItems.Distinct().ToList();
                    //}


                    //return getList.Distinct().ToList();
                }
            }
        }
        private List<SimulationItemsData> FetchDishCategories(List<SimulationItemsData> data, string siteCode, CookBookCoreEntities context)
        {
            //List<SimulationItemsData> result =new List<SimulationItemsData>();

            foreach (var item in data)
            {
                item.DishCategoryList = GetDishCategoriesFromItem(item.ItemID, item.SiteCode, context, item.ItemCode);
                //result.Add(item);
            }
            //return result;
            return data;
        }
        private List<SimulationRegionItemDishCategoryData> GetDishCategoriesFromItem(int itemID, string siteCode, CookBookCoreEntities context, string itemCode)
        {
            //SiteItemDishCategory table
            //var getDishCategory = (from a in context.ItemDishCategoryMappings
            //                           //var getDishCategory = (from a in context.SiteItemDishMappings
            //                           //join d in context.Dishes on a.Dish_ID equals d.ID
            //                       join b in context.DishCategories on a.DishCategory_ID equals b.ID
            //                       where a.Item_ID == itemID && a.IsActive == true
            //                       //&& a.SiteCode== siteCode
            //                       select new SimulationRegionItemDishCategoryData
            //                       {
            //                           ItemName = context.Items.FirstOrDefault(a => a.ID == itemID).Name,
            //                           ItemID = itemID,
            //                           DishCategoryID = b.ID,
            //                           DishCategoryName = b.Name,
            //                           DishCategoryCode = b.DishCategoryCode,
            //                           Dishes = context.Dishes.Where(c => c.DishCategory_ID == b.ID && c.IsActive == true).Select(d => new SimulationItemDishData { DishID = d.ID, DishName = d.Name, CostPerKG = d.CostPerKG }).ToList()
            //                       }
            //                             ).ToList();
            var getDishCategory = (from a in context.SiteDishCategoryMappings
                                   join i in context.Items on a.Item_ID equals i.ID
                                   join d in context.DishCategories on a.DishCategory_ID equals d.ID
                                   //join k in context.SiteItemDishMappings on 
                                   where a.Item_ID == itemID && a.IsActive == true && a.SiteCode == siteCode
                                   select new SimulationRegionItemDishCategoryData
                                   {
                                       ItemID = itemID,
                                       ItemName = i.Name,
                                       DishCategoryID = d.ID,
                                       DishCategoryCode = d.DishCategoryCode,
                                       DishCategoryName = d.Name,
                                       //Dishes = context.Dishes.Where(c => c.DishCategory_ID == d.ID && c.IsActive == true).Select(d => new SimulationItemDishData { DishID = d.ID, DishName = d.Name, DishCode = d.DishCode, CostPerKG = d.CostPerKG }).ToList()
                                       //Dishes = context.SiteItemDishMappings.Where(c => c.DishCategory_ID == d.ID && c.IsActive == true).Select(d => new SimulationItemDishData { DishID = d.ID, DishName = d.Name, DishCode = d.DishCode, CostPerKG = d.CostPerKG }).ToList()
                                   }).ToList();


            var chkFCSSiteDish = context.FoodCostSimSiteDishes.Count(a => a.SiteCode == siteCode);
            foreach (var item in getDishCategory)
            {
                if (chkFCSSiteDish > 0)
                    item.Dishes = (from a in context.Dishes
                                   join ab in context.FoodCostSimSiteDishes on a.DishCode equals ab.DishCode
                                   join b in context.SiteItemDishMappings on ab.DishCode equals b.DishCode
                                   //join u in context.UOMs on b.ServedUOMCode equals u.UOMCode
                                   where b.ItemCode == itemCode && ab.SiteCode == siteCode && b.IsActive == true && a.DishCategoryCode == item.DishCategoryCode
                                   select new SimulationItemDishData { DishID = a.ID, DishName = a.Name, DishCode = a.DishCode, CostPerKG = ab.CostPerKG, ServedPortion = b.ServedPortion }).ToList();
                //select new SimulationItemDishData { DishID = a.ID, DishName = a.Name, DishCode = a.DishCode, CostPerKG = ab.CostPerKG, ServedPortion = b.ServedPortion,UOM=u.Name}).ToList();
                else
                    item.Dishes = (from a in context.Dishes
                                   join ab in context.SiteDishes on a.DishCode equals ab.DishCode
                                   join b in context.SiteItemDishMappings on ab.DishCode equals b.DishCode
                                   //join u in context.UOMs on b.ServedUOMCode equals u.UOMCode
                                   where b.ItemCode == itemCode && ab.SiteCode == siteCode && b.IsActive == true && a.DishCategoryCode == item.DishCategoryCode
                                   select new SimulationItemDishData { DishID = a.ID, DishName = a.Name, DishCode = a.DishCode, CostPerKG = ab.CostPerKG, ServedPortion = b.ServedPortion }).ToList();
                //select new SimulationItemDishData { DishID = a.ID, DishName = a.Name, DishCode = a.DishCode, CostPerKG = ab.CostPerKG, ServedPortion = b.ServedPortion, UOM = u.Name }).ToList();
            }
            //foreach (var item in getDishCategory)
            //{
            //    var getDish=context.SiteItemDishMappings.Where(a=>a.SiteCode==siteCode && a.Item_ID==itemID )
            //    item.Dishes=new SimulationItemDishData {
            //     DishID=
            //    }
            //}
            return getDishCategory;
        }
        public IList<SimulationRegionItemDishCategoryData> GetSimulationRegionItemDishCategories(RequestData requestData, int siteId, int regionId, int itemId)
        {
            CookBookCommonEntities commonContext = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var getsiteCode = commonContext.SiteMasters.FirstOrDefault(a => a.SiteID == siteId).SiteCode;
                    string itemCode = "";
                    var getDishCategory = GetDishCategoriesFromItem(itemId, getsiteCode, context, itemCode);
                    return getDishCategory;
                    //return getList.Distinct().ToList();
                }
            }
        }

        //public IList<SimulationRegionItemDishCategoryData> GetItemsDishCategories(RequestData requestData, List<int> itemId)
        //{
        //    using (var transaction = new EFTransaction())
        //    {
        //        using (var context = GetContext(transaction, requestData.SessionSectorName))
        //        {
        //            var getDishCategory = new List<SimulationRegionItemDishCategoryData>();
        //            foreach (var item in itemId)
        //            {
        //                getDishCategory.AddRange(GetDishCategoriesFromItem(item, context));
        //            }

        //            return getDishCategory;
        //            //return getList.Distinct().ToList();
        //        }
        //    }
        //}

        public SimulationBoardData GetSimulationBoard(int simId, RequestData requestData)
        {
            CookBookCommonEntities commonContext = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var getsim = (from a in context.FoodCostSimulations
                                  where a.ID == simId
                                  select new SimulationBoardData
                                  {
                                      ID = a.ID,
                                      RegionID = a.RegionID,
                                      SiteID = a.SiteID,
                                      SiteCode = a.SiteCode,
                                      SimulationName = a.Name,
                                      SimCode = a.SimulationCode,
                                      NoOfDays = a.NoOfDays,
                                      Status = a.Status,
                                      Version = a.Version
                                  }).FirstOrDefault();
                    //getsim.SiteCode = commonContext.SiteMasters.FirstOrDefault(x => x.SiteID == getsim.SiteID).SiteCode;

                    //if removed from fcssitedish table recheck and delete that record
                    if (getsim.Status.Contains(FoodCostSimulationStatus.Incomplete.ToString()) || getsim.Status.Contains(FoodCostSimulationStatus.Draft.ToString()))
                    {
                        var sco = new SqlParameter("@simCode", getsim.SimCode);
                        context.Database.ExecuteSqlCommand("exec procCheckAndRemoveDeletedSiteDishFCS @simCode", sco);
                    }

                    var getdata = (from a in context.FoodCostSimulationBoards
                                   where a.SimulationID == getsim.ID
                                   let it = context.Items.Where(p => p.ID == a.ItemID).FirstOrDefault()
                                   where it != null
                                   let ds = context.Dishes.Where(p => p.ID == a.DishID).FirstOrDefault()
                                   where ds != null
                                   let dc = context.DishCategories.Where(p => p.ID == a.DishCategoryID).FirstOrDefault()
                                   where dc != null
                                   let dp = context.DayParts.Where(p => p.ID == a.DayPartID).FirstOrDefault()
                                   where dp != null
                                   //join d in context.Dishes on a.DishID equals d.ID into dis
                                   //join dc in context.DishCategories on a.DishCategoryID equals dc.ID into dishc
                                   //join it in context.Items on a.ItemID equals it.ID into itm
                                   //join dp in context.DayParts on a.DayPartID equals dp.ID into dayp
                                   select new FoodCostSimulationBoardData
                                   {
                                       ID = a.ID,
                                       SimulationID = a.SimulationID.Value,
                                       DayPartID = a.DayPartID,
                                       DayNumber = a.DayNumber,
                                       DishCategoryID = a.DishCategoryID,
                                       DishID = a.DishID,
                                       LunchAvgCost = a.LunchAvgCost,
                                       ItemID = a.ItemID,
                                       Pax = a.Pax,
                                       Gm = a.Gm,
                                       ServedPortion = a.Gm,
                                       Cost = a.Cost,

                                       ItemName = it.Name,
                                       ItemCode = it.ItemCode,
                                       DayPartCode = dp.DayPartCode,
                                       DayPartName = dp.Name,
                                       DishName = ds.Name,
                                       DishCode = ds.DishCode,
                                       DishCategoryName = dc.Name,
                                       DishCategoryCode = dc.DishCategoryCode,
                                   }).ToList();
                    getsim.FoodCostSimulationBoard = getdata;
                    //var getdata = context.FoodCostSimulationBoards.Where(a => a.SimulationID == getsim.ID);
                    //int index = 1;

                    //var data = new List<SimulationBoardItemsData>();
                    //var dataO = new List<SimulationBoardValues>();
                    //var dishes = new List<SimulationBoardDS>();
                    //var dishc = new List<SimulationBoardDC>();
                    //foreach (var item in getdata)
                    //{
                    //    dataO.Add(new SimulationBoardValues
                    //    {
                    //        index = index,
                    //        paxTxt = item.Pax.HasValue ? item.Pax.ToString() : string.Empty,
                    //        grmTxt = item.Gm,
                    //        costTxt = item.Cost
                    //    });
                    //    index++;
                    //}
                    //foreach (var item in getdata)
                    //{
                    //    var dishDet = context.Dishes.FirstOrDefault(x => x.ID == item.DishID);
                    //    if (!dishes.Any(x => x.DishID == item.DishID))
                    //    {
                    //        dishes.Add(new SimulationBoardDS
                    //        {
                    //            DishID = dishDet.ID,
                    //            DishName = dishDet.Name,
                    //            CostPerKG = dishDet.CostPerKG,
                    //            dataO = dataO
                    //        });
                    //    }
                    //}
                    //foreach (var item in getdata)
                    //{
                    //    var dishCat = context.DishCategories.FirstOrDefault(x => x.ID == item.DishCategoryID);
                    //    var it = context.Items.FirstOrDefault(x => x.ID == item.ItemID);
                    //    if (!dishc.Any(x => x.DishCategoryID == item.DishCategoryID))
                    //    {
                    //        dishc.Add(new SimulationBoardDC
                    //        {
                    //            DishCategoryCode = dishCat.DishCategoryCode,
                    //            DishCategoryID = dishCat.ID,
                    //            DishCategoryName = dishCat.Name,
                    //            ItemID = it.ID,
                    //            ItemName = it.Name,
                    //            Dishes = dishes
                    //        });
                    //    }
                    //}
                    //foreach (var item in getdata)
                    //{
                    //    var daypart = context.DayParts.FirstOrDefault(x => x.ID == item.DayPartID);
                    //    var it = context.Items.FirstOrDefault(x => x.ID == item.ItemID);
                    //    if (!data.Any(x => x.value == item.ItemID && x.dpcode == daypart.DayPartCode))
                    //    {
                    //        data.Add(new SimulationBoardItemsData
                    //        {
                    //            code = it.ItemCode,
                    //            dpcode = daypart.DayPartCode,
                    //            text = it.Name + " - " + daypart.Name,
                    //            value = it.ID,
                    //            TotalAvgCost = item.LunchAvgCost.HasValue ? item.LunchAvgCost.ToString() : string.Empty,
                    //            DishCategories = dishc
                    //        });
                    //    }

                    //}
                    ////var getishcat=(from a in context.fo)
                    ////var getsimData = (from a in context.FoodCostSimulations
                    ////                  join b in context.FoodCostSimulationBoards on a.ID equals b.SimulationID
                    ////                  join i in context.Items on b.ItemID equals i.ID
                    ////                  join d in context.DayParts on b.DayPartID equals d.ID
                    ////                  where a.ID == simId
                    ////                  select new SimulationBoardItemsData
                    ////                  {
                    ////                      code = i.ItemCode,
                    ////                      value = b.ItemID,
                    ////                      dpcode = d.DayPartCode,
                    ////                      text = i.Name + " - " + d.Name,
                    ////    TotalAvgCost = b.LunchAvgCost.HasValue ? b.LunchAvgCost.ToString() : string.Empty
                    ////}).ToList();
                    //getsim.ItemsData = data;

                    //Insert recipe data
                    //18-03-2022 Already happening when saving the board
                    //var result1 = context.procUpdateGetFCSSiteRecipeList(getsim.SimCode, getsim.SiteCode, true);

                    ////Check for Versioned and store or fetch master APL
                    //var chkVersion = getsim.Version == FoodCostSimulationStatus.Versioned.ToString();
                    //if (chkVersion)
                    //{
                    //    var chkAPL = context.FoodCostSimAPLMasters.Where(a => a.SimulationCode == getsim.SimCode && a.Site == getsim.SiteCode);
                    //    if (chkAPL.Count() == 0)
                    //    {
                    //        //CookBookCommonEntities commonContext = new CookBookCommonEntities();
                    //        var getMOGS = context.FoodCostSimSiteRecipeMOGMappings.Where(a => a.SimulationCode == getsim.SimCode && a.SiteCode == getsim.SiteCode).ToList();
                    //        foreach (var item in getMOGS)
                    //        {
                    //            var aplDetails = commonContext.APLMasters.FirstOrDefault(a => a.ArticleNumber == item.ArticleNumber);
                    //            if (aplDetails != null)
                    //            {
                    //                context.procSaveFCSAPLMaster(item.ArticleNumber, getsim.SimCode, getsim.SiteCode);
                    //            }
                    //        }
                    //    }
                    //}
                    return getsim;
                }
            }
        }

        public string SaveSimulationBoard(SimulationBoardData model, RequestData requestData)
        {
            CookBookCommonEntities commonContext = new CookBookCommonEntities();
            string sitecode = !string.IsNullOrEmpty(model.SiteCode) ? model.SiteCode : commonContext.SiteMasters.FirstOrDefault(x => x.SiteID == model.SiteID).SiteCode;
            string simId = string.Empty;
            string simCode = string.Empty;
            if (model.Status == FoodCostSimulationStatus.Incomplete.ToString())
            {
                using (var transaction = new EFTransaction())
                {
                    using (var context = GetContext(transaction, requestData.SessionSectorName))
                    {
                        //Insert SiteDish data
                        var result2 = context.procSaveFCSSiteDish(sitecode, false);
                        context.SaveChanges();
                        //return simId + "_" + simCode;
                    }
                }
            }            
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var foodcostdata = new FoodCostSimulation();
                    if (model.ID > 0)
                    {
                        foodcostdata = context.FoodCostSimulations.FirstOrDefault(a => a.ID == model.ID);
                        foodcostdata.ModifiedBy = model.ModifiedBy;
                        foodcostdata.ModifiedOn = model.ModifiedOn;
                        //switch (model.Status)
                        //{
                        //    case "Draft":
                        //        break;
                        //    case "Versioned":

                        //        break;
                        //    case "Final":
                        //        break;
                        //    default:
                        //        break;
                        //}

                        ////14th April
                        ////if removed from fcssitedish table recheck and delete that record
                        //if (model.Status.Contains(FoodCostSimulationStatus.Incomplete.ToString()) || model.Status.Contains(FoodCostSimulationStatus.Draft.ToString()))
                        //{
                        //    var sco = new SqlParameter("@simCode", foodcostdata.SimulationCode);
                        //    context.Database.ExecuteSqlCommand("exec procCheckAndRemoveDeletedSiteDishFCS @simCode", sco);
                        //}
                    }
                    else
                    {
                        foodcostdata.CreatedBy = model.CreatedBy;
                        foodcostdata.CreateOn = model.CreatedOn;
                        foodcostdata.IsActive = true;
                    }
                    //foodcostdata.RegionID = model.RegionID;
                    //foodcostdata.SiteID = model.SiteID;
                    //foodcostdata.Name = model.SimulationName;
                    //foodcostdata.NoOfDays = model.NoOfDays;
                    ////foodcostdata.SimulationCode = model.SimCode;
                    //foodcostdata.Status = model.Status;

                    if (model.ID > 0)
                    {
                        //if (model.Status == FoodCostSimulationStatus.Draft.ToString() || model.Status == FoodCostSimulationStatus.Final.ToString() || model.Status == FoodCostSimulationStatus.Incomplete.ToString())
                        //{
                        //Status is draft or final and case is update
                        //remove exisitng board and add new
                        //update status and remove version
                        //TODO Testing
                        //if (model.Status != FoodCostSimulationStatus.Versioned.ToString())
                        if (foodcostdata.Status != FoodCostSimulationStatus.Versioned.ToString())
                        {
                            var res = context.procDeleteExisitngSimBoardData(foodcostdata.ID);
                        }
                        //context.FoodCostSimulationBoards.Where(a => a.SimulationID == foodcostdata.ID).ToList().ForEach(a => context.FoodCostSimulationBoards.Remove(a));

                        //foodcostdata.Version = string.Empty;
                        if (!model.saveOnly)
                        {
                            if (model.Status == FoodCostSimulationStatus.Versioned.ToString())
                            {
                                if (foodcostdata.Status == FoodCostSimulationStatus.Versioned.ToString())
                                {
                                    foodcostdata = new FoodCostSimulation();
                                    foodcostdata.CreatedBy = model.ModifiedBy;
                                    foodcostdata.CreateOn = DateTime.Now;
                                }

                                foodcostdata.SimulationCode = model.SimCode;
                                var getLastVr = context.FoodCostSimulations.Where(a => a.SimulationCode == model.SimCode).OrderByDescending(a => a.ID).FirstOrDefault();
                                if (getLastVr == null || (getLastVr != null && string.IsNullOrEmpty(getLastVr.Version)))
                                    foodcostdata.Version = "V1";
                                else
                                    foodcostdata.Version = "V" + (context.FoodCostSimulations.Where(a => a.Status.Contains(FoodCostSimulationStatus.Versioned.ToString()) && a.SimulationCode == foodcostdata.SimulationCode).Count() + 1);


                                foodcostdata.IsActive = true;
                                foodcostdata.Status = model.Status;

                            }
                        }
                        foodcostdata.RegionID = model.RegionID;
                        foodcostdata.SiteID = model.SiteID;
                        foodcostdata.SiteCode = !string.IsNullOrEmpty(model.SiteCode) ? model.SiteCode : commonContext.SiteMasters.FirstOrDefault(x => x.SiteID == model.SiteID).SiteCode;
                        foodcostdata.Name = model.SimulationName;
                        foodcostdata.NoOfDays = model.NoOfDays;
                        //}
                        //else if (model.Status == FoodCostSimulationStatus.Versioned.ToString())
                        if (!model.saveOnly)
                        {
                            if (model.Status == FoodCostSimulationStatus.Versioned.ToString())
                            {
                                //Status is versioned, create new case with existing sim code
                                //update version number
                                //if (foodcostdata.Status == FoodCostSimulationStatus.Versioned.ToString())
                                //{
                                //    foodcostdata = new FoodCostSimulation();
                                //    foodcostdata.CreatedBy = model.ModifiedBy;
                                //    foodcostdata.CreateOn = DateTime.Now;
                                //}
                                //foodcostdata.SimulationCode = context.FoodCostSimulations.FirstOrDefault(a => a.ID == model.ID).SimulationCode;

                                //foodcostdata.IsActive = true;
                                //foodcostdata.Status = model.Status;
                                context.FoodCostSimulations.AddOrUpdate(foodcostdata);

                            }

                            foodcostdata.Status = model.Status;
                        }
                    }
                    else
                    {
                        if (model.Status == FoodCostSimulationStatus.Versioned.ToString() || model.Status == FoodCostSimulationStatus.Final.ToString())
                            foodcostdata.Version = "V1";

                        foodcostdata.RegionID = model.RegionID;
                        foodcostdata.SiteID = model.SiteID;
                        foodcostdata.SiteCode = !string.IsNullOrEmpty(model.SiteCode) ? model.SiteCode : commonContext.SiteMasters.FirstOrDefault(x => x.SiteID == model.SiteID).SiteCode;
                        foodcostdata.Name = model.SimulationName;
                        foodcostdata.NoOfDays = model.NoOfDays;
                        foodcostdata.SimulationCode = model.SimCode;
                        foodcostdata.Status = model.Status;

                        context.FoodCostSimulations.AddOrUpdate(foodcostdata);
                    }
                    context.SaveChanges();
                    //if (model.Status == FoodCostSimulationStatus.Versioned.ToString() || model.Status == FoodCostSimulationStatus.Final.ToString())
                    //{
                    //    //APL List
                    //    var chkAPL = context.FoodCostSimAPLMasters.Where(a => a.SimulationCode == foodcostdata.SimulationCode);
                    //    if (chkAPL.Count() == 0)
                    //    { 

                    //    }
                    //}

                    if (model.ItemsData != null)
                    {
                        foreach (var item in model.ItemsData)
                        {
                            foreach (var dishCat in item.DishCategories)
                            {
                                if (dishCat.Dishes != null && dishCat.Dishes.Count > 0)
                                {
                                    foreach (var dishes in dishCat.Dishes)
                                    {
                                        ////using (var transaction1 = new EFTransaction())
                                        ////{
                                        ////    using (var context1 = GetContext(transaction, requestData.SessionSectorName))
                                        ////    {
                                        ////8th April Recipe Mapping fix
                                        var sc = new SqlParameter("@siteCode", sitecode);
                                        var dc = new SqlParameter("@dishCode", dishes.DishCode);
                                        var rc = new SqlParameter("@recipeCode","" );
                                        var sco = new SqlParameter("@simCode", foodcostdata.SimulationCode);
                                        context.Database.ExecuteSqlCommand("exec procSaveFCSSiteDishRecipeMpping @siteCode , @dishCode , @recipeCode , @simCode", sc, dc, rc, sco);
                                        //var chksdrm = context.SiteDishRecipeMappings.Where(a => a.DishCode == dishes.DishCode && a.SiteCode == sitecode).ToList();
                                        //if (chksdrm.Count > 0)
                                        //{
                                        //    foreach (var sdrm in chksdrm)
                                        //    {
                                        //        var chkrecp = context.FoodCostSimRecipes.FirstOrDefault(a => a.RecipeCode == sdrm.RecipeCode);
                                        //        if (chkrecp == null)
                                        //        {
                                        //            var getSiteRecipe = context.Recipes.FirstOrDefault(a => a.RecipeCode == sdrm.RecipeCode);
                                        //            if (getSiteRecipe != null)
                                        //            {
                                        //                context.FoodCostSimRecipes.Add(new FoodCostSimRecipe
                                        //                {
                                        //                    AllergensName = getSiteRecipe.AllergensName,
                                        //                    BaseRecipeCode = getSiteRecipe.BaseRecipeCode,
                                        //                    CostPerKG = getSiteRecipe.CostPerKG,
                                        //                    RecipeCode = getSiteRecipe.RecipeCode,
                                        //                    CostPerUOM = getSiteRecipe.CostPerUOM,
                                        //                    CreatedBy = getSiteRecipe.CreatedBy,
                                        //                    CreatedOn = getSiteRecipe.CreatedOn,
                                        //                    ExpiryCategoryCode = getSiteRecipe.ExpiryCategoryCode,
                                        //                    IngredientsName = getSiteRecipe.IngredientsName,
                                        //                    Instructions = getSiteRecipe.Instructions,
                                        //                    IsActive = getSiteRecipe.IsActive,
                                        //                    IsBase = getSiteRecipe.IsBase,
                                        //                    IsElementory = getSiteRecipe.IsElementory,
                                        //                    ModifiedBy = getSiteRecipe.ModifiedBy,
                                        //                    ModifiedOn = getSiteRecipe.ModifiedOn,
                                        //                    Name = getSiteRecipe.Name,
                                        //                    NutrientRecipeQuantity = getSiteRecipe.NutrientRecipeQuantity,
                                        //                    NutritionData = getSiteRecipe.NutritionData,
                                        //                    OID = getSiteRecipe.ID,
                                        //                    Quantity = getSiteRecipe.Quantity,
                                        //                    RecipeAlias = getSiteRecipe.RecipeAlias,
                                        //                    RecipeUOMCategory = getSiteRecipe.RecipeUOMCategory,
                                        //                    ServingTemperature_ID = getSiteRecipe.ServingTemperature_ID,
                                        //                    //SimulationCode=foodcostdata.SimulationCode,
                                        //                    Status = getSiteRecipe.Status,
                                        //                    TotalCost = getSiteRecipe.TotalCost,
                                        //                    TotalIngredientWeight = getSiteRecipe.TotalIngredientWeight,
                                        //                    Type = getSiteRecipe.Type,
                                        //                    UOMCode = getSiteRecipe.UOMCode,
                                        //                    UOM_ID = getSiteRecipe.UOM_ID,
                                        //                    WeightPerUnit = getSiteRecipe.WeightPerUnit,
                                        //                    WeightPerUnitGravy = getSiteRecipe.WeightPerUnitGravy,
                                        //                    WeightPerUnitUOM = getSiteRecipe.WeightPerUnitUOM,
                                        //                    WeightPerUnitUOMGravy = getSiteRecipe.WeightPerUnitUOMGravy,
                                        //                    Yield = getSiteRecipe.Yield
                                        //                });
                                        //                context.SaveChanges();
                                        //            }
                                        //        }
                                        //        var chkfcsrecp = context.FoodCostSimSiteRecipes.FirstOrDefault(a => a.RecipeCode == sdrm.RecipeCode && a.SiteCode == sitecode);
                                        //        if (chkfcsrecp == null)
                                        //        {
                                        //            var getSiteRecipe = context.SiteRecipes.FirstOrDefault(a => a.RecipeCode == sdrm.RecipeCode && a.SiteCode == sitecode);
                                        //            if (getSiteRecipe != null)
                                        //            {
                                        //                context.FoodCostSimSiteRecipes.Add(new FoodCostSimSiteRecipe
                                        //                {
                                        //                    Site_ID = getSiteRecipe.Site_ID,
                                        //                    SiteCode = getSiteRecipe.SiteCode,
                                        //                    Region_ID = getSiteRecipe.Region_ID,
                                        //                    Name = getSiteRecipe.Name,
                                        //                    UOM_ID = getSiteRecipe.UOM_ID,
                                        //                    Quantity = getSiteRecipe.Quantity,
                                        //                    Instructions = getSiteRecipe.Instructions,
                                        //                    IsBase = getSiteRecipe.IsBase,
                                        //                    IsActive = getSiteRecipe.IsActive,
                                        //                    CreatedBy = getSiteRecipe.CreatedBy,
                                        //                    CreatedOn = getSiteRecipe.CreatedOn,
                                        //                    ModifiedBy = getSiteRecipe.ModifiedBy,
                                        //                    ModifiedOn = getSiteRecipe.ModifiedOn,
                                        //                    RecipeCode = getSiteRecipe.RecipeCode,
                                        //                    UOMCode = getSiteRecipe.UOMCode,
                                        //                    CostPerUOM = getSiteRecipe.CostPerUOM,
                                        //                    CostPerKG = getSiteRecipe.CostPerKG,
                                        //                    TotalCost = getSiteRecipe.TotalCost,
                                        //                    RegionCostPerUOM = getSiteRecipe.RegionCostPerUOM,
                                        //                    RegionCostPerKG = getSiteRecipe.RegionCostPerKG,
                                        //                    RegionTotalCost = getSiteRecipe.RegionTotalCost,
                                        //                    Status = getSiteRecipe.Status,
                                        //                    AllergensName = getSiteRecipe.AllergensName,
                                        //                    RecipeUOMCategory = getSiteRecipe.RecipeUOMCategory,
                                        //                    WeightPerUnit = getSiteRecipe.WeightPerUnit,
                                        //                    WeightPerUnitUOM = getSiteRecipe.WeightPerUnitUOM,
                                        //                    WeightPerUnitGravy = getSiteRecipe.WeightPerUnitGravy,
                                        //                    WeightPerUnitUOMGravy = getSiteRecipe.WeightPerUnitUOMGravy,
                                        //                    TotalIngredientWeight = getSiteRecipe.TotalIngredientWeight,
                                        //                    NutritionData = getSiteRecipe.NutritionData,
                                        //                    IngredientsName = getSiteRecipe.IngredientsName,
                                        //                    SimulationCode = foodcostdata.SimulationCode,
                                        //                    OID = getSiteRecipe.ID
                                        //                });
                                        //                context.SaveChanges();
                                        //            }
                                        //        }
                                        //        var chkfcsdrm = context.FoodCostSimSiteDishRecipeMappings.FirstOrDefault(a => a.SiteCode == sitecode && a.SimulationCode == foodcostdata.SimulationCode && a.DishCode == sdrm.DishCode);
                                        //        if (chkfcsdrm == null)
                                        //        {
                                        //            context.FoodCostSimSiteDishRecipeMappings.Add(new FoodCostSimSiteDishRecipeMapping
                                        //            {
                                        //                Site_ID = sdrm.Site_ID,
                                        //                SiteCode = sdrm.SiteCode,
                                        //                Dish_ID = sdrm.Dish_ID,
                                        //                Recipe_ID = sdrm.Recipe_ID,
                                        //                IsActive = sdrm.IsActive,
                                        //                CreatedBy = sdrm.CreatedBy,
                                        //                CreatedOn = sdrm.CreatedOn,
                                        //                ModifiedBy = sdrm.ModifiedBy,
                                        //                ModifiedOn = sdrm.ModifiedOn,
                                        //                DishCode = sdrm.DishCode,
                                        //                RecipeCode = sdrm.RecipeCode,
                                        //                IsDefault = sdrm.IsDefault,
                                        //                SimulationCode = foodcostdata.SimulationCode,
                                        //                OID = sdrm.ID
                                        //            });
                                        //            context.SaveChanges();
                                        //        }
                                        //    }

                                        //}
                                        ////    }
                                        ////}
                                        foreach (var daywise in dishes.dataO)
                                        {
                                            var itemsData = new FoodCostSimulationBoard();
                                            //if (daywise.boradId > 0)
                                            //    itemsData = context.FoodCostSimulationBoards.FirstOrDefault(a => a.ID == daywise.boradId);
                                            itemsData.SimulationID = foodcostdata.ID;
                                            itemsData.SimulationCode = foodcostdata.SimulationCode;
                                            itemsData.ItemID = Convert.ToInt32(item.value.Split('_')[0]);
                                            itemsData.DishCategoryID = dishCat.DishCategoryID;
                                            itemsData.DishID = dishes.DishID;
                                            itemsData.DayPartID = context.DayParts.FirstOrDefault(a => a.DayPartCode == item.dpcode).ID;
                                            itemsData.DayNumber = daywise.index;
                                            if (!string.IsNullOrEmpty(daywise.paxTxt))
                                                itemsData.Pax = Convert.ToInt32(daywise.paxTxt);
                                            if (!string.IsNullOrEmpty(daywise.grmTxt) && daywise.grmTxt != "null" && daywise.grmTxt != "-")
                                                itemsData.Gm = daywise.grmTxt;
                                            else
                                            {
                                                var getsp = context.SiteItemDishMappings.FirstOrDefault(a => a.DishCode == dishes.DishCode && a.SiteCode == sitecode && a.ItemCode == item.code);
                                                if (getsp != null)
                                                    itemsData.Gm = getsp.ServedPortion.HasValue ? getsp.ServedPortion.ToString() : "";
                                            }
                                            if (!string.IsNullOrEmpty(daywise.grmTxt) && daywise.grmTxt != "null" && daywise.grmTxt != "-")
                                                itemsData.Cost = daywise.costTxt;
                                            itemsData.LunchAvgCost = Convert.ToDecimal(item.TotalAvgCost);

                                            context.FoodCostSimulationBoards.AddOrUpdate(itemsData);
                                            //context.SaveChanges();
                                            //if(itemsData.ID>0)
                                            //context.FoodCostSimulationBoards.Add(itemsData);
                                        }
                                    }
                                }
                            }

                        }
                        context.SaveChanges();
                    }
                    //var SiteCode = commonContext.SiteMasters.FirstOrDefault(x => x.SiteID == model.SiteID).SiteCode;
                    var SiteCode = sitecode;
                    if (model.Status == FoodCostSimulationStatus.Incomplete.ToString())
                    {

                        //Insert recipe data
                        var result1 = context.procUpdateGetFCSSiteRecipeList(foodcostdata.SimulationCode, SiteCode, true);
                        context.SaveChanges();
                    }
                    simId = foodcostdata.ID.ToString();
                    simCode = foodcostdata.SimulationCode;
                    //sitecode = SiteCode;

                    //if (model.Status == FoodCostSimulationStatus.Versioned.ToString() || model.Status == FoodCostSimulationStatus.Final.ToString())
                    //{
                    //    //APL List
                    //    //var chkAPL = context.FoodCostSimAPLMasters.Where(a => a.SimulationCode == foodcostdata.SimulationCode);
                    //    //if (chkAPL.Count() == 0)
                    //    //{
                    //    //}
                    //    var getMogs = context.FoodCostSimSiteRecipeMOGMappings.Where(a => a.SimulationCode == foodcostdata.SimulationCode && !string.IsNullOrEmpty(a.ArticleNumber)).ToList();
                    //    if (getMogs.Count > 0)
                    //    {
                    //        foreach (var item in getMogs)
                    //        {
                    //            var result = context.procSaveFCSAPLMaster(SiteCode, foodcostdata.SimulationCode, item.ArticleNumber, false);
                    //            //var chk = context.FoodCostSimAPLMasters.Where(a => a.Site == SiteCode && a.SimulationCode == foodcostdata.SimulationCode && a.ArticleNumber == item.ArticleNumber);
                    //            //if (chk.Count() == 0)
                    //            //{ 

                    //            //}
                    //        }
                    //        context.SaveChanges();
                    //    }
                    //}
                }
            }

            if (!model.saveOnly && (model.Status == FoodCostSimulationStatus.Versioned.ToString() || model.Status == FoodCostSimulationStatus.Final.ToString()))
            {
                using (var transaction = new EFTransaction())
                {
                    using (var context = GetContext(transaction, requestData.SessionSectorName))
                    {
                        var getMogs = context.FoodCostSimSiteRecipeMOGMappings.Where(a => a.SimulationCode == simCode && !string.IsNullOrEmpty(a.ArticleNumber)).ToList();
                        if (getMogs.Count > 0)
                        {
                            foreach (var item in getMogs)
                            {
                                if (!string.IsNullOrEmpty(item.ArticleNumber))
                                {
                                    var result = context.procSaveFCSAPLMaster(sitecode, simCode, Convert.ToInt64(item.ArticleNumber), false);
                                }
                            }
                            context.SaveChanges();
                        }
                    }
                }
            }
            if (model.Status == FoodCostSimulationStatus.Incomplete.ToString())
            {
                //using (var transaction = new EFTransaction())
                //{
                //    using (var context = GetContext(transaction, requestData.SessionSectorName))
                //    {
                //        //Insert SiteDish data
                //        var result2 = context.procSaveFCSSiteDish(sitecode, false);
                //        context.SaveChanges();
                return simId + "_" + simCode;
                //    }
                //}
            }
            //return "true";
            return simId + "_" + simCode;
        }

        public IList<procUpdateGetFCSSiteDishRecipeList_Result> UpdateAndFetchSiteDishRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    //var getsitedishrecipe = context.FoodCostSimSiteDishRecipeMappings.Where(a => a.SimulationCode == simCode && a.SiteCode == SiteCode && a.DishCode == DishCode).ToList();
                    //if (getsitedishrecipe.Count == 0)
                    //{                         
                    //}
                    var result = new List<procUpdateGetFCSSiteDishRecipeList_Result>();
                    var getSimulation = context.FoodCostSimulations.FirstOrDefault(a => a.SimulationCode == simCode);
                    //if (getSimulation != null && (getSimulation.Status.Contains(FoodCostSimulationStatus.Versioned.ToString()) ))
                    //{
                    //    var result1 = context.procUpdateGetFCSSiteRecipeList(simCode, SiteCode, false);
                    //    result = context.procUpdateGetFCSSiteDishRecipeList(simCode, SiteCode, DishCode, false).ToList();
                    //}
                    //else
                    //{
                    //var result1 = context.procUpdateGetFCSSiteRecipeList(simCode, SiteCode, true);
                    result = context.procUpdateGetFCSSiteDishRecipeList(simCode, SiteCode, DishCode, true).ToList();
                    //}
                    return result;
                }
            }
        }
        public IList<procGetFCSSiteRecipeData_Result> UpdateAndFetchSiteRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    //var getsitedishrecipe = context.FoodCostSimSiteDishRecipeMappings.Where(a => a.SimulationCode == simCode && a.SiteCode == SiteCode && a.DishCode == DishCode).ToList();
                    //if (getsitedishrecipe.Count == 0)
                    //{                         
                    //}
                    var result1 = new List<procGetFCSSiteRecipeData_Result>();
                    var getSimulation = context.FoodCostSimulations.FirstOrDefault(a => a.SimulationCode == simCode);
                    //if (getSimulation != null && (getSimulation.Status.Contains(FoodCostSimulationStatus.Versioned.ToString())))
                    //{
                    //     result1 = context.procUpdateGetFCSSiteRecipeList(simCode, SiteCode, false).ToList();
                    //    //var result = context.procUpdateGetFCSSiteDishRecipeList(simCode, SiteCode, DishCode, false);
                    //}
                    //else
                    //{
                    //result1 = context.procUpdateGetFCSSiteRecipeList(simCode, SiteCode, true).ToList();
                    result1 = context.procGetFCSSiteRecipeData(SiteCode, simCode).ToList();
                    var result2 = new List<procGetFCSSiteRecipeData_Result>();
                    foreach (var item in result1)
                    {
                        //FoodCostSimSiteDishRecipeMapping chkMap = null;
                        //if (!string.IsNullOrEmpty(DishCode))
                        //    chkMap = context.FoodCostSimSiteDishRecipeMappings.FirstOrDefault(a => a.SiteCode == item.SiteCode && a.RecipeCode == item.RecipeCode && a.DishCode!=DishCode);
                        //else
                        var chkMap = context.FoodCostSimSiteDishRecipeMappings.FirstOrDefault(a => a.SiteCode == item.SiteCode && a.RecipeCode == item.RecipeCode && a.SimulationCode == simCode);

                        if (chkMap == null)
                            result2.Add(item);
                        else if (!string.IsNullOrEmpty(DishCode) && chkMap.DishCode == DishCode)
                            result2.Add(item);
                    }

                    //var result = context.procUpdateGetFCSSiteDishRecipeList(simCode, SiteCode, DishCode, true);
                    //}
                    return result2;
                }
            }
        }
        //public IList<procGetFCSSiteRecipeData_Result> UpdateAndFetchSiteRecipeData_New(RequestData requestData, string simCode, string SiteCode,string dishCode)
        //{
        //    using (var transaction = new EFTransaction())
        //    {
        //        using (var context = GetContext(transaction, requestData.SessionSectorName))
        //        {
        //            //var getsitedishrecipe = context.FoodCostSimSiteDishRecipeMappings.Where(a => a.SimulationCode == simCode && a.SiteCode == SiteCode && a.DishCode == DishCode).ToList();
        //            //if (getsitedishrecipe.Count == 0)
        //            //{                         
        //            //}
        //            var result1 = new List<procGetFCSSiteRecipeData_Result>();
        //            //var getSimulation = context.FoodCostSimulations.FirstOrDefault(a => a.SimulationCode == simCode);
        //            //if (getSimulation != null && (getSimulation.Status.Contains(FoodCostSimulationStatus.Versioned.ToString())))
        //            //{
        //            //     result1 = context.procUpdateGetFCSSiteRecipeList(simCode, SiteCode, false).ToList();
        //            //    //var result = context.procUpdateGetFCSSiteDishRecipeList(simCode, SiteCode, DishCode, false);
        //            //}
        //            //else
        //            //{
        //            //result1 = context.procUpdateGetFCSSiteRecipeList(simCode, SiteCode, true).ToList();
        //            var result =  context.procUpdateGetFCSSiteDishRecipeList(simCode, SiteCode, dishCode, true).ToList();
        //            result1 = context.procGetFCSSiteRecipeData(SiteCode, simCode).ToList();

        //            //var result = context.procUpdateGetFCSSiteDishRecipeList(simCode, SiteCode, DishCode, true);
        //            //}
        //            return result1;
        //        }
        //    }
        //}
        public IList<FoodCostSimRecipe> GetAllRecipes(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.FoodCostSimRecipes.ToList();
                }
            }
        }
        public IList<FoodCostSimSiteRecipe> GetAllSiteRecipe(RequestData requestData, string sitecode, string simCode)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    //return context.FoodCostSimSiteRecipes.Where(a => a.SiteCode == sitecode && a.SimulationCode == simCode).ToList();
                    return context.FoodCostSimSiteRecipes.Where(a => a.SiteCode == sitecode).ToList();
                }
            }
        }
        public IList<FoodCostSimSiteDishRecipeMapping> GetSiteDishRecipeMapping(RequestData requestData, string sitecode, string dishcode, string simCode)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.FoodCostSimSiteDishRecipeMappings.Where(a => a.SiteCode == sitecode && a.DishCode == dishcode && a.SimulationCode == simCode).ToList();
                }
            }
        }
        public IList<procGetFCSSiteMOGsByRecipeID_Result> GetSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode, string simCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var chkFcstbl = context.FoodCostSimSiteRecipeMOGMappings.Count(a => a.SiteCode == SiteCode && a.RecipeCode == recipeCode && a.SimulationCode == simCode);
                    if (chkFcstbl == 0)
                    {
                        var getDishCode = context.SiteDishRecipeMappings.FirstOrDefault(a => a.SiteCode == SiteCode && a.RecipeCode == recipeCode);
                        if (getDishCode != null)
                        {
                            var chkFcsDishtbl = context.FoodCostSimSiteDishRecipeMappings.FirstOrDefault(a => a.SiteCode == SiteCode && a.RecipeCode == recipeCode && a.DishCode == getDishCode.DishCode && a.SimulationCode == simCode);
                            if (chkFcsDishtbl == null)
                            {
                                context.FoodCostSimSiteDishRecipeMappings.Add(new FoodCostSimSiteDishRecipeMapping
                                {
                                    SimulationCode = simCode,
                                    CreatedBy = getDishCode.CreatedBy,
                                    CreatedOn = DateTime.Now,
                                    DishCode = getDishCode.DishCode,
                                    Dish_ID = getDishCode.Dish_ID,
                                    IsActive = getDishCode.IsActive,
                                    OID = getDishCode.ID,
                                    IsDefault = getDishCode.IsDefault,
                                    RecipeCode = getDishCode.RecipeCode,
                                    Recipe_ID = getDishCode.Recipe_ID,
                                    SiteCode = getDishCode.SiteCode,
                                    Site_ID = getDishCode.Site_ID
                                });
                                context.SaveChanges();
                            }
                            var chkMogMapping = context.SiteRecipeMOGMappings.Where(a => a.SiteCode == SiteCode && a.RecipeCode == recipeCode).ToList();
                            foreach (var item in chkMogMapping)
                            {
                                context.FoodCostSimSiteRecipeMOGMappings.Add(new FoodCostSimSiteRecipeMOGMapping
                                {
                                    SimulationCode = simCode,
                                    CreatedBy = item.CreatedBy,
                                    CreatedOn = DateTime.Now,
                                    IsActive = item.IsActive,
                                    OID = item.ID,
                                    RecipeCode = item.RecipeCode,
                                    Recipe_ID = item.Recipe_ID,
                                    SiteCode = item.SiteCode,
                                    Site_ID = item.Site_ID,
                                    AllergensName = item.AllergensName,
                                    ArticleNumber = item.ArticleNumber,
                                    BaseRecipeCode = item.BaseRecipeCode,
                                    BaseRecipe_ID = item.BaseRecipe_ID,
                                    CostAsOfDate = item.CostAsOfDate,
                                    CostPerKG = item.CostPerKG,
                                    CostPerUOM = item.CostPerUOM,
                                    IngredientPerc = item.IngredientPerc,
                                    IsMajor = item.IsMajor,
                                    MOGCode = item.MOGCode,
                                    MOG_ID = item.MOG_ID,
                                    NutritionData = item.NutritionData,
                                    Quantity = item.Quantity,
                                    RegionIngredientPerc = item.RegionIngredientPerc,
                                    RegionQuantity = item.RegionQuantity,
                                    RegionTotalCost = item.RegionTotalCost,
                                    Region_ID = item.Region_ID,
                                    TotalCost = item.TotalCost,
                                    UOMCode = item.UOMCode,
                                    UOM_ID = item.UOM_ID,
                                    Yield = item.Yield
                                });
                            }
                            context.SaveChanges();
                        }
                    }
                    return context.procGetFCSSiteMOGsByRecipeID(recipeID, recipeCode, SiteCode, simCode, requestData.SessionSectorNumber).ToList();
                }
            }
        }
        public string SaveFoodCostSimRecipeData(FoodCostSimRecipe entityRec, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.FoodCostSimRecipes.AddOrUpdate(entityRec);
                    context.SaveChanges();
                }
            }
            return "true";
        }
        public string SaveFoodCostSimSiteDishRecipeMappingAndMOG(RequestData requestData, FoodCostSimRecipe entityRec, FoodCostSimSiteRecipe entity, IList<FoodCostSimSiteRecipeMOGMapping> entity2, int dishId, string dishCode, List<FoodCostSimAPLHistory> entityHis, IList<FoodCostSimSiteRecipeMOGMapping> entity1)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    //FCS Recipe
                    if (entityRec != null)
                    {
                        context.FoodCostSimRecipes.AddOrUpdate(entityRec);
                        context.SaveChanges();
                    }

                    //SiteRecipe
                    context.FoodCostSimSiteRecipes.AddOrUpdate(entity);
                    context.SaveChanges();

                    var setDefault = context.FoodCostSimSiteDishRecipeMappings.Where(a => a.DishCode == dishCode && a.SiteCode == entity.SiteCode && a.SimulationCode == entity.SimulationCode).ToList();
                    if (setDefault.Count > 0)
                    {
                        setDefault.ForEach(a => a.IsDefault = false);
                        context.SaveChanges();
                    }
                    context.FoodCostSimSiteDishRecipeMappings.Add(new FoodCostSimSiteDishRecipeMapping
                    {
                        SiteCode = entity.SiteCode,
                        Site_ID = (int)entity.Site_ID,
                        CreatedBy = entity.ModifiedBy,
                        CreatedOn = DateTime.Now,
                        IsActive = true,
                        RecipeCode = entity.RecipeCode,
                        Recipe_ID = entity.ID,
                        SimulationCode = entity.SimulationCode,
                        Dish_ID = dishId,
                        DishCode = dishCode,
                        IsDefault = true
                    });
                    context.SaveChanges();

                    var setSiteDishNewValue = context.FoodCostSimSiteDishes.FirstOrDefault(a => a.DishCode == dishCode && a.SiteCode == entity.SiteCode);
                    if (setSiteDishNewValue != null)
                    {
                        setSiteDishNewValue.CostPerKG = entity.CostPerKG;
                        context.SaveChanges();
                    }

                    var list = from x in context.FoodCostSimSiteRecipeMOGMappings
                               where x.RecipeCode == entity.RecipeCode && x.SiteCode == entity.SiteCode && x.SimulationCode == entity.SimulationCode
                               select x;
                    IList<FoodCostSimSiteRecipeMOGMapping> llist = list.ToList();

                    //foreach (FoodCostSimSiteRecipeMOGMapping m in llist)
                    //{
                    //m.IsActive = false;
                    //m.ModifiedBy = entity.ModifiedBy;
                    //m.ModifiedOn = DateTime.Now;
                    //context.FoodCostSimSiteRecipeMOGMappings.AddOrUpdate(m);                        
                    //}
                    context.FoodCostSimSiteRecipeMOGMappings.RemoveRange(llist);
                    context.SaveChanges();
                    foreach (FoodCostSimSiteRecipeMOGMapping m in entity2)
                    {
                        m.IsActive = true;
                        var isItemExists = context.FoodCostSimSiteRecipeMOGMappings.Where(item => item.RecipeCode == m.RecipeCode && item.MOG_ID == m.MOG_ID
                                                                            && item.SiteCode == m.SiteCode && item.SimulationCode == entity.SimulationCode).FirstOrDefault();
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            isItemExists.IsActive = true;
                            isItemExists.ModifiedBy = entity.ModifiedBy;
                            isItemExists.ModifiedOn = DateTime.Now;
                            isItemExists.Quantity = m.Quantity;
                            isItemExists.IngredientPerc = m.IngredientPerc;
                            isItemExists.TotalCost = m.TotalCost;
                            isItemExists.ArticleNumber = m.ArticleNumber;
                            context.FoodCostSimSiteRecipeMOGMappings.AddOrUpdate(isItemExists);
                        }
                        else
                        {

                            m.CreatedOn = DateTime.Now;
                            m.CreatedBy = entity.CreatedBy;
                            m.ModifiedBy = null;
                            m.ModifiedOn = null;
                            m.ID = 0;
                            m.Recipe_ID = entity.ID;//Id missing
                            m.RecipeCode = entity.RecipeCode;
                            m.SimulationCode = entity.SimulationCode;
                            m.Site_ID = entity.Site_ID;
                            m.SiteCode = entity.SiteCode;
                            m.Region_ID = (int)entity.Region_ID;
                            context.FoodCostSimSiteRecipeMOGMappings.AddOrUpdate(m);
                        }
                        context.SaveChanges();
                    }

                }

            }

            if (entityHis != null && entityHis.Count > 0)
            {
                using (var transaction = new EFTransaction())
                {
                    using (var context = GetContext(transaction, requestData.SessionSectorName))
                    {
                        //foreach (var item in entityHis)
                        //{
                        //    item.ItemCode = context.Items.FirstOrDefault(a => a.ID == item.ID).ItemCode;
                        //}
                        context.FoodCostSimAPLHistories.AddRange(entityHis);
                        context.SaveChanges();
                    }
                }
            }
            //Base Recipe issue
            //17-05-2022
            try
            {
                using (var transaction = new EFTransaction())
                {
                    using (var context = GetContext(transaction, requestData.SessionSectorName))
                    {
                        foreach (FoodCostSimSiteRecipeMOGMapping m in entity1)
                        {
                            m.IsActive = true;
                            var isItemExists = context.FoodCostSimSiteRecipeMOGMappings.Where(item => item.RecipeCode == m.RecipeCode && item.BaseRecipe_ID == m.BaseRecipe_ID
                                                                                && item.SiteCode == m.SiteCode && item.SimulationCode == entity.SimulationCode).FirstOrDefault();
                            m.Recipe_ID = entity.ID;
                            m.RecipeCode = entity.RecipeCode;
                            if (isItemExists != null && isItemExists.ID > 0)
                            {
                                isItemExists.ModifiedBy = entity.ModifiedBy;
                                isItemExists.ModifiedOn = DateTime.Now;
                                isItemExists.Quantity = m.Quantity;
                                isItemExists.IngredientPerc = m.IngredientPerc;
                                isItemExists.TotalCost = m.TotalCost;

                                context.FoodCostSimSiteRecipeMOGMappings.AddOrUpdate(isItemExists);
                            }
                            else
                            {
                                m.CreatedOn = DateTime.Now;
                                m.CreatedBy = entity.CreatedBy;
                                m.ModifiedBy = null;
                                m.ModifiedOn = null;
                                m.ID = 0;
                                m.SimulationCode = entity.SimulationCode;
                                context.FoodCostSimSiteRecipeMOGMappings.AddOrUpdate(m);
                            }

                        }
                        context.SaveChanges();
                    }
                }
            }
            catch { }
            return "true";
        }
        public string SaveFoodCostSimSiteRecipeData(FoodCostSimRecipe entityRec, FoodCostSimSiteRecipe entity, IList<FoodCostSimSiteRecipeMOGMapping> entity1, IList<FoodCostSimSiteRecipeMOGMapping> entity2, List<FoodCostSimAPLHistory> entityHis, int dishId, string dishCode, RequestData requestData)
        {
            int RecipeID = entity.ID;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    if (entityRec != null)
                        if (string.IsNullOrEmpty(entityRec.RecipeCode))
                            entityRec.RecipeCode = entity.RecipeCode;

                    if (entityRec != null)
                    {
                        context.FoodCostSimRecipes.AddOrUpdate(entityRec);
                        context.SaveChanges();
                    }

                    context.FoodCostSimSiteRecipes.AddOrUpdate(entity);
                    if (entityRec != null)
                    {
                        //var isAdd = entity.ID;
                        if (entity.ID == 0)
                        {
                            //var setDefault = context.FoodCostSimSiteDishRecipeMappings.Where(a => a.DishCode == dishCode && a.SiteCode == entity.SiteCode).ToList();
                            var setDefault = context.FoodCostSimSiteDishRecipeMappings.Where(a => a.DishCode == dishCode && a.SiteCode == entity.SiteCode && a.SimulationCode == entity.SimulationCode).ToList();
                            if (setDefault.Count > 0)
                            {
                                setDefault.ForEach(a => a.IsDefault = false);
                                context.SaveChanges();
                            }
                            context.FoodCostSimSiteDishRecipeMappings.Add(new FoodCostSimSiteDishRecipeMapping
                            {
                                SiteCode = entity.SiteCode,
                                Site_ID = (int)entity.Site_ID,
                                CreatedBy = entity.CreatedBy,
                                CreatedOn = entity.CreatedOn,
                                IsActive = true,
                                RecipeCode = entity.RecipeCode,
                                Recipe_ID = entityRec.ID,
                                SimulationCode = entity.SimulationCode,
                                Dish_ID = dishId,
                                DishCode = dishCode,
                                IsDefault = true
                            });
                        }
                    }
                    else
                    {
                        //var setDefault = context.FoodCostSimSiteDishRecipeMappings.Where(a => a.DishCode == dishCode && a.SiteCode == entity.SiteCode).ToList();
                        var setDefault = context.FoodCostSimSiteDishRecipeMappings.Where(a => a.DishCode == dishCode && a.SiteCode == entity.SiteCode && a.SimulationCode == entity.SimulationCode).ToList();
                        if (setDefault.Count > 0)
                        {
                            setDefault.ForEach(a => a.IsDefault = false);
                            context.SaveChanges();
                        }

                        //var chkSiteDish = context.FoodCostSimSiteDishRecipeMappings.FirstOrDefault(a => a.RecipeCode == entity.RecipeCode && a.DishCode == dishCode && a.SiteCode == entity.SiteCode);
                        var chkSiteDish = context.FoodCostSimSiteDishRecipeMappings.FirstOrDefault(a => a.RecipeCode == entity.RecipeCode && a.DishCode == dishCode && a.SiteCode == entity.SiteCode && a.SimulationCode == entity.SimulationCode);
                        if (chkSiteDish != null)
                        {
                            chkSiteDish.ModifiedBy = entity.ModifiedBy;
                            chkSiteDish.ModifiedOn = entity.ModifiedOn;
                            chkSiteDish.IsDefault = true;
                            var fcsSiteDish = context.FoodCostSimSiteDishes.FirstOrDefault(a => a.SiteCode == entity.SiteCode && a.DishCode == dishCode);
                            if (fcsSiteDish != null)
                            {
                                var fcsSiteReci = context.FoodCostSimSiteRecipes.FirstOrDefault(a => a.SiteCode == entity.SiteCode && a.RecipeCode == entity.RecipeCode);
                                if (fcsSiteReci != null)
                                {
                                    fcsSiteDish.CostPerKG = fcsSiteReci.CostPerKG;
                                }
                            }
                            context.SaveChanges();
                        }
                        else
                        {
                            context.FoodCostSimSiteDishRecipeMappings.Add(new FoodCostSimSiteDishRecipeMapping
                            {
                                SiteCode = entity.SiteCode,
                                Site_ID = (int)entity.Site_ID,
                                CreatedBy = entity.ModifiedBy,
                                CreatedOn = DateTime.Now,
                                IsActive = true,
                                RecipeCode = entity.RecipeCode,
                                Recipe_ID = entity.ID,
                                SimulationCode = entity.SimulationCode,
                                Dish_ID = dishId,
                                DishCode = dishCode,
                                IsDefault = true
                            });
                            context.SaveChanges();
                        }

                    }

                    var setSiteDishNewValue = context.FoodCostSimSiteDishes.FirstOrDefault(a => a.DishCode == dishCode && a.SiteCode == entity.SiteCode);
                    if (setSiteDishNewValue != null)
                    {
                        setSiteDishNewValue.CostPerKG = entity.CostPerKG;
                        context.SaveChanges();
                    }

                    //---- Delete functionality not availabe for site ---//

                    var list = from x in context.FoodCostSimSiteRecipeMOGMappings
                               where x.RecipeCode == entity.RecipeCode && x.SiteCode == entity.SiteCode && x.SimulationCode == entity.SimulationCode
                               select x;
                    IList<FoodCostSimSiteRecipeMOGMapping> llist = list.ToList();

                    //foreach (FoodCostSimSiteRecipeMOGMapping m in llist)
                    //{
                    //m.IsActive = false;
                    //m.ModifiedBy = entity.ModifiedBy;
                    //m.ModifiedOn = DateTime.Now;
                    //context.FoodCostSimSiteRecipeMOGMappings.AddOrUpdate(m);                        
                    //}
                    context.FoodCostSimSiteRecipeMOGMappings.RemoveRange(llist);
                    context.SaveChanges();
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (FoodCostSimSiteRecipeMOGMapping m in entity1)
                    {
                        m.IsActive = true;
                        var isItemExists = context.FoodCostSimSiteRecipeMOGMappings.Where(item => item.RecipeCode == m.RecipeCode && item.BaseRecipe_ID == m.BaseRecipe_ID
                                                                            && item.SiteCode == m.SiteCode && item.SimulationCode == entity.SimulationCode).FirstOrDefault();
                        m.Recipe_ID = entity.ID;
                        m.RecipeCode = entity.RecipeCode;
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            isItemExists.ModifiedBy = entity.ModifiedBy;
                            isItemExists.ModifiedOn = DateTime.Now;
                            isItemExists.Quantity = m.Quantity;
                            isItemExists.IngredientPerc = m.IngredientPerc;
                            isItemExists.TotalCost = m.TotalCost;

                            context.FoodCostSimSiteRecipeMOGMappings.AddOrUpdate(isItemExists);
                        }
                        else
                        {
                            m.CreatedOn = DateTime.Now;
                            m.CreatedBy = entity.CreatedBy;
                            m.ModifiedBy = null;
                            m.ModifiedOn = null;
                            m.ID = 0;
                            m.SimulationCode = entity.SimulationCode;
                            context.FoodCostSimSiteRecipeMOGMappings.AddOrUpdate(m);
                        }

                    }
                    context.SaveChanges();
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (FoodCostSimSiteRecipeMOGMapping m in entity2)
                    {
                        m.IsActive = true;
                        var isItemExists = context.FoodCostSimSiteRecipeMOGMappings.Where(item => item.RecipeCode == m.RecipeCode && item.MOG_ID == m.MOG_ID
                                                                            && item.SiteCode == m.SiteCode && item.SimulationCode == entity.SimulationCode).FirstOrDefault();
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            isItemExists.IsActive = true;
                            isItemExists.ModifiedBy = entity.ModifiedBy;
                            isItemExists.ModifiedOn = DateTime.Now;
                            isItemExists.Quantity = m.Quantity;
                            isItemExists.IngredientPerc = m.IngredientPerc;
                            isItemExists.TotalCost = m.TotalCost;
                            isItemExists.ArticleNumber = m.ArticleNumber;
                            context.FoodCostSimSiteRecipeMOGMappings.AddOrUpdate(isItemExists);
                        }
                        else
                        {

                            m.CreatedOn = DateTime.Now;
                            m.CreatedBy = entity.CreatedBy;
                            m.ModifiedBy = null;
                            m.ModifiedOn = null;
                            m.ID = 0;
                            m.Recipe_ID = entity.ID;//Id missing
                            m.RecipeCode = entity.RecipeCode;
                            m.SimulationCode = entity.SimulationCode;
                            m.Site_ID = entity.Site_ID;
                            m.SiteCode = entity.SiteCode;
                            m.Region_ID = (int)entity.Region_ID;
                            context.FoodCostSimSiteRecipeMOGMappings.AddOrUpdate(m);
                        }
                        context.SaveChanges();
                    }
                }
            }
            if (entityHis != null && entityHis.Count > 0)
            {
                using (var transaction = new EFTransaction())
                {
                    using (var context = GetContext(transaction, requestData.SessionSectorName))
                    {
                        //foreach (var item in entityHis)
                        //{
                        //    item.ItemCode = context.Items.FirstOrDefault(a => a.ID == item.ID).ItemCode;
                        //}
                        context.FoodCostSimAPLHistories.AddRange(entityHis);
                        context.SaveChanges();
                    }
                }
            }
            //using (var transaction = new EFTransaction())
            //{
            //    using (var context = GetContext(transaction, requestData.SessionSectorName))
            //    {
            //        context.procUpdateAllergensAtSiteByRecipeCode(entity.RecipeCode, entity.SiteCode);

            //        context.SaveChanges();
            //    }
            //}
            return "true";
        }
        public string SaveFCSAPLHistory(FoodCostSimAPLHistory entity, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.FoodCostSimAPLHistories.AddOrUpdate(entity);
                    context.SaveChanges();
                }
            }
            return "true";
        }
        public bool IsFCSAPLGet(string simCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var chkIfVersioned = context.FoodCostSimulations.FirstOrDefault(a => a.SimulationCode == simCode);
                    var retChkVers = chkIfVersioned.Status == FoodCostSimulationStatus.Versioned.ToString();
                    if (retChkVers)
                    {
                        var getFCSAPLMaster = context.FoodCostSimAPLMasters.Where(a => a.SimulationCode == simCode);
                        return getFCSAPLMaster.Count() > 0;
                    }
                    return retChkVers;
                }
            }
        }
        public IList<procGetFCSAPLMasterDataForSite_Result> GetFCSAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, string simuCode, RequestData requestData, bool isVersioned)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetFCSAPLMasterDataForSite(simuCode, articleNumber, mogCode, SiteCode, requestData.SessionSectorNumber, isVersioned).ToList();
                }
            }
        }
        //public IList<procGetFCSAPLMasterDataForSite2_Result> GetFCSAPLMasterSiteDataList2(string articleNumber, string mogCode, string SiteCode, string simuCode, RequestData requestData)
        //{
        //    using (var transaction = new EFTransaction())
        //    {
        //        using (var context = GetContext(transaction, requestData.SessionSectorName))
        //        {
        //            return context.procGetFCSAPLMasterDataForSite2(simuCode, articleNumber, mogCode, SiteCode, requestData.SessionSectorNumber).ToList();
        //        }
        //    }
        //}
        public IList<DishData> GetFCSDish(RequestData requestData, string siteCode, int itemId)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    if (string.IsNullOrEmpty(siteCode))
                        return (from a in context.FoodCostSimSiteDishes
                                join b in context.Dishes on a.DishCode equals b.DishCode
                                where a.IsActive == true
                                select new DishData
                                {
                                    ID = b.ID,
                                    Name = b.Name,
                                    DishCode = b.DishCode,
                                    DishCategory_ID = b.DishCategory_ID,
                                    DishCategoryName = context.DishCategories.FirstOrDefault(x => x.DishCategoryCode == b.DishCategoryCode).Name,
                                    DishCategoryCode = b.DishCategoryCode,
                                    CostPerKG = a.CostPerKG,
                                    UOMCode = b.UOMCode,
                                    DietCategoryCode = b.DietCategoryCode,
                                    CuisineName = b.CuisineCode,
                                    RecipeMapStatus = b.Recipe_ID.HasValue ? "Yes" : "No",
                                    ServedPortion = context.SiteItemDishMappings.FirstOrDefault(x => x.DishCode == a.DishCode).ServedPortion
                                }).ToList();
                    //        return context.FoodCostSimSiteDishes.Where(a => a.IsActive==true).Select(a=>new DishData {
                    //            ID=
                    //        "DishID": dataSource[i].ID, "DishName": dataSource[i].Name, "DishCode": dataSource[i].DishCode,
                    //"DishCategoryID": dataSource[i].DishCategory_ID, "DishCategoryName": dataSource[i].DishCategoryName,
                    //"DishCategoryCode": dataSource[i].DishCategoryCode,
                    //"CostPerKG": dataSource[i].CostPerKG, "tooltip": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
                    //"DietCategoryCode": dataSource[i].DietCategoryCode,
                    //"CuisineName": dataSource[i].CuisineName, "RecipeMapStatus": dataSource[i].RecipeMapStatus
                    //        }).ToList();
                    else
                    {
                        //check and insert updated site dishes
                        var result2 = context.procSaveFCSSiteDish(siteCode, false);
                        //var getItem = context.Items.FirstOrDefault(a => a.ID == itemId).ItemCode;
                        ////var chkSiteItemMap = context.SiteItemDishMappings.FirstOrDefault(a => a.ItemCode == getItem && a.SiteCode == siteCode);
                        //var chkSiteItemMap = context.SiteItemDishMappings.Where(a => a.SiteCode == siteCode);
                        //if (chkSiteItemMap != null)
                        //{
                        //    if (chkSiteItemMap.IsActive != true)
                        //    {
                        //        var willDel = context.FoodCostSimSiteDishes.FirstOrDefault(a => a.SiteCode == siteCode && a.DishCode == chkSiteItemMap.DishCode);
                        //        context.FoodCostSimSiteDishes.Remove(willDel);
                        //        context.SaveChanges();
                        //    }
                        //}
                        return (from a in context.FoodCostSimSiteDishes
                                join b in context.Dishes on a.DishCode equals b.DishCode
                                //join c in context.SiteItemDishMappings on a.DishCode equals c.DishCode
                                where a.SiteCode == siteCode && a.IsActive == true
                                select new DishData
                                {
                                    ID = b.ID,
                                    Name = b.Name,
                                    DishCode = b.DishCode,
                                    DishCategory_ID = b.DishCategory_ID,
                                    DishCategoryName = context.DishCategories.FirstOrDefault(x => x.DishCategoryCode == b.DishCategoryCode).Name,
                                    DishCategoryCode = b.DishCategoryCode,
                                    CostPerKG = a.CostPerKG,
                                    UOMCode = b.UOMCode,
                                    DietCategoryCode = b.DietCategoryCode,
                                    CuisineName = b.CuisineCode,
                                    RecipeMapStatus = b.Recipe_ID.HasValue ? "Yes" : "No",
                                    ServedPortion = context.SiteItemDishMappings.FirstOrDefault(x => x.DishCode == a.DishCode && x.SiteCode == siteCode).ServedPortion
                                }).ToList();
                    }
                    //return context.FoodCostSimSiteDishes.Where(a => a.SiteCode == siteCode && a.IsActive == true).ToList();
                }
            }
        }


        public IList<procGetFCSAPLMasterDataForSite_Result> GetFCSAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, string simuCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetFCSAPLMasterDataForSite(simuCode, articleNumber, mogCode, SiteCode, requestData.SessionSectorNumber, true).ToList();
                }
            }
        }
    }
}
