﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(ICostSimulatorRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CostSimulatorRepository : EFSynchronousRepository<int, CostSimulation, CookBookCoreEntities>,
        ICostSimulatorRepository
    {
        protected override DbSet<CostSimulation> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.CostSimulations;
        }

        protected override CostSimulation FindImpl(int key, DbSet<CostSimulation> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override CostSimulation FindImplWithExpand(int key, DbSet<CostSimulation> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

       

        public IList<procGetMappedDishRecipeList_Result> GetRecipeMappingDataList(string DishCode, RequestData requestData)
        {
            
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetMappedDishRecipeList(DishCode).ToList();
                }
            }
        }

        public IList<procGetSimulatedMappedRecipeMOGListSite_Result> GetSimulatedMappedRecipeMOGListSiteDataList(int RecipeID, int SiteId, RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetSimulatedMappedRecipeMOGListSite(RecipeID, SiteId,requestData.SessionSectorNumber).ToList();
                }
            }
        }
        public IList<procGetSimulatedMappedRecipeMOGListSector_Result> GetSimulatedMappedRecipeMOGListSectorDataList(int RecipeID, int RegionId, RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetSimulatedMappedRecipeMOGListSector(RecipeID, RegionId, requestData.SessionSectorNumber).ToList();
                }
            }
        }
        public IList<procGetSimulatedMappedRecipeMOGListNational_Result> GetSimulatedMappedRecipeMOGListNationalDataList(int RecipeID, RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetSimulatedMappedRecipeMOGListNational(RecipeID, requestData.SessionSectorNumber).ToList();
                }
            }
        }
    }
}

