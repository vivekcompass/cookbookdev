﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IUOMModuleMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UOMModuleMasterRepository : EFSynchronousRepository<int, UOMModuleMaster, CookBookCoreEntities>,
        IUOMModuleMasterRepository
    {
        protected override DbSet<UOMModuleMaster> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.UOMModuleMasters;
        }

        protected override UOMModuleMaster FindImpl(int key, DbSet<UOMModuleMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override UOMModuleMaster FindImplWithExpand(int key, DbSet<UOMModuleMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetUOMModuleMappingList_Result> GetUOMModuleMasterModuleMappingDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetUOMModuleMappingList().ToList();
                }
            }
        }
    }
}
