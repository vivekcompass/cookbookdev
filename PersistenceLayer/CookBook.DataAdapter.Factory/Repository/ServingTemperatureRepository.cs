﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IServingTemperatureRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ServingTemperatureRepository : EFSynchronousRepository<int, ServingTemperature, CookBookCommonEntities>,
        IServingTemperatureRepository
    {
        protected override DbSet<ServingTemperature> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.ServingTemperatures;
        }

        protected override ServingTemperature FindImpl(int key, DbSet<ServingTemperature> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ServingTemperature FindImplWithExpand(int key, DbSet<ServingTemperature> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
