﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(ISiteDayPartMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteDayPartMappingRepository : EFSynchronousRepository<int, SiteDayPartMapping, CookBookCoreEntities>,
        ISiteDayPartMappingRepository
    {
        protected override DbSet<SiteDayPartMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.SiteDayPartMappings;
        }

        protected override SiteDayPartMapping FindImpl(int key, DbSet<SiteDayPartMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SiteDayPartMapping FindImplWithExpand(int key, DbSet<SiteDayPartMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<SiteDayPartMapping> GetSiteDayPartMappingDataList(string siteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                //int ID = int.Parse(ID);

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var list = from x in context.SiteDayPartMappings
                               where x.SiteCode == siteCode && x.IsActive.Value
                               select x;
                    return list.ToList();
                }
            }
        }


        public string SaveSiteDayPartMappingList(IList<SiteDayPartMapping> model, bool isClosingTime, RequestData requestData)
        {
            string siteCode = model[0].SiteCode;
            if (isClosingTime)
            {
                return SaveSiteDayPartClosingTimeMappingList(model, requestData);
            }
            else

            {
                string dayPartCode = model[0].DayPartCode;
                //check karna hai
                using (var transaction = new EFTransaction())
                {
                    using (var context = GetContext(transaction, requestData.SessionSectorName))
                    {

                        var list = from x in context.SiteDayPartMappings
                                   where x.SiteCode == siteCode && x.IsActive.Value
                                   select x;
                        IList<SiteDayPartMapping> deactivateItemList = list.ToList();
                        // var deactivateItemList = llist.ToList();
                        if (deactivateItemList != null && deactivateItemList.Count() > 0)
                        {
                            foreach (SiteDayPartMapping m in deactivateItemList)
                            {
                                if (!String.IsNullOrWhiteSpace(m.SiteCode))
                                {

                                    m.IsActive = false;
                                    m.ModifiedBy = requestData.SessionUserId;
                                    m.ModifiedOn = DateTime.Now;
                                    context.SiteDayPartMappings.AddOrUpdate(m);
                                }
                            }
                            context.SaveChanges();
                        }


                    }
                }
                if (string.IsNullOrWhiteSpace(dayPartCode))
                {
                    return "true";
                }

                using (var transaction = new EFTransaction())
                {
                    using (var context = GetContext(transaction, requestData.SessionSectorName))
                    {
                        foreach (SiteDayPartMapping m in model)
                        {

                            m.IsActive = true;
                            var isItemExists = context.SiteDayPartMappings.Where(item => item.SiteCode == m.SiteCode && item.DayPartCode == m.DayPartCode).FirstOrDefault();


                            if (isItemExists != null && isItemExists.ID > 0)
                            {
                                isItemExists.ModifiedBy = requestData.SessionUserId;
                                isItemExists.ModifiedOn = DateTime.Now;
                                isItemExists.IsActive = true;
                                context.SiteDayPartMappings.AddOrUpdate(isItemExists);
                            }
                            else
                            {
                                m.CreatedOn = DateTime.Now;
                                m.CreatedBy = requestData.SessionUserId;
                                m.ModifiedBy = null;
                                m.ModifiedOn = null;
                                m.ID = 0;
                                context.SiteDayPartMappings.AddOrUpdate(m);
                            }


                        }
                        context.SaveChanges();
                        return "true";
                    }
                }
            }
        }

        public string SaveSiteDayPartClosingTimeMappingList(IList<SiteDayPartMapping> model, RequestData requestData)
        {

            string siteCode = model[0].SiteCode;
            string dayPartCode = model[0].DayPartCode;

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (SiteDayPartMapping m in model)
                    {

                        m.IsActive = true;
                        var isItemExists = context.SiteDayPartMappings.Where(item => item.SiteCode == m.SiteCode && item.DayPartCode == m.DayPartCode).FirstOrDefault();


                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            isItemExists.ModifiedBy = requestData.SessionUserId;
                            isItemExists.ModifiedOn = DateTime.Now;
                            isItemExists.IsActive = true;
                            isItemExists.ClosingStatus = m.ClosingStatus;
                            isItemExists.ClosingTime = m.ClosingTime;
                            context.SiteDayPartMappings.AddOrUpdate(isItemExists);
                        }

                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }
    }
    
}
