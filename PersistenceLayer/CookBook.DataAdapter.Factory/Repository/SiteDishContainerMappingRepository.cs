﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(ISiteDishContainerMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteDishContainerMappingRepository : EFSynchronousRepository<int, SiteDishContainerMapping, CookBookCoreEntities>,
        ISiteDishContainerMappingRepository
    {
     
        public IList<procGetSiteDishContainerMappingData_Result> GetSiteDishContainerMappingDataList(string siteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetSiteDishContainerMappingData(siteCode).ToList();
                }
            }
        }
        
        public IList<procGetSiteDishCategoryContainerMappingData_Result> GetSiteDishCategoryContainerMappingDataList(string siteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetSiteDishCategoryContainerMappingData(siteCode).ToList();
                }
            }
        }


        public string SaveSiteDishContainerMappingDataList(IList<SiteDishContainerMapping> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (SiteDishContainerMapping m in model)
                    {
                        if (!String.IsNullOrWhiteSpace(m.DishCode))
                        {
                            var isItemExists = context.SiteDishContainerMappings.Where(item => item.SiteCode == m.SiteCode && item.DishCode == m.DishCode).FirstOrDefault();

                            if (isItemExists != null && isItemExists.ID > 0)
                            {
                                m.ModifiedOn = DateTime.Now;
                                m.ModifiedBy = m.CreatedBy;
                                m.ID = isItemExists.ID;
                            }
                            else
                            {
                                m.CreatedOn = DateTime.Now;
                                m.CreatedBy = requestData.SessionUserId;
                                m.ID = 0;
                            }
                            context.SiteDishContainerMappings.AddOrUpdate(m);
                        }
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }
        public string SaveSiteDishCategoryContainerMappingDataList(IList<SiteDishCategoryContainerMapping> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (SiteDishCategoryContainerMapping m in model)
                    {
                        if (!String.IsNullOrWhiteSpace(m.DishCategoryCode))
                        {
                            var isItemExists = context.SiteDishCategoryContainerMappings.Where(item => item.SiteCode == m.SiteCode && item.DishCategoryCode == m.DishCategoryCode).FirstOrDefault();

                            if (isItemExists != null && isItemExists.ID > 0)
                            {
                                m.ModifiedOn = DateTime.Now;
                                m.ModifiedBy = m.CreatedBy;
                                m.ID = isItemExists.ID;
                            }
                            else
                            {
                                m.CreatedOn = DateTime.Now;
                                m.CreatedBy = requestData.SessionUserId;
                                m.ID = 0;
                            }
                            context.SiteDishCategoryContainerMappings.AddOrUpdate(m);
                        }
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }
        protected override SiteDishContainerMapping FindImpl(int key, DbSet<SiteDishContainerMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SiteDishContainerMapping FindImplWithExpand(int key, DbSet<SiteDishContainerMapping> dbSet, string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        protected override DbSet<SiteDishContainerMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.SiteDishContainerMappings;
        }
    }

}
