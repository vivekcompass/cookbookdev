﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IVisualCategoryRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class VisualCategoryRepository : EFSynchronousRepository<int, VisualCategory, CookBookCommonEntities>,
        IVisualCategoryRepository
    {
        protected override DbSet<VisualCategory> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.VisualCategories;
        }

        protected override VisualCategory FindImpl(int key, DbSet<VisualCategory> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override VisualCategory FindImplWithExpand(int key, DbSet<VisualCategory> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
