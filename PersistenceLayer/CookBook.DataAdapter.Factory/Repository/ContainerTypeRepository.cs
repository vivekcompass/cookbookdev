﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System.Data.Entity.Migrations;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IContainerTypeRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ContainerTypeRepository : EFSynchronousRepository<int, ContainerType, CookBookCoreEntities>,
        IContainerTypeRepository
    {
        protected override DbSet<ContainerType> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.ContainerTypes;
        }

        protected override ContainerType FindImpl(int key, DbSet<ContainerType> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ContainerType FindImplWithExpand(int key, DbSet<ContainerType> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public string SaveList(IList<ContainerType> model,RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (ContainerType m in model)
                    {

                        context.ContainerTypes.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<procGetContainerType_Result> GetContainerTypeDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetContainerType().ToList();
                }
            }
        }
    }
}
