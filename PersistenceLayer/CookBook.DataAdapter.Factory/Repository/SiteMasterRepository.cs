﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System.Web.ModelBinding;
using System;
using CookBook.Data.Request;
using CookBook.Data.MasterData;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(ISiteMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteMasterRepository : EFSynchronousRepository<int, SiteMaster, CookBookCommonEntities>,
        ISiteMasterRepository
    {
        protected override DbSet<SiteMaster> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.SiteMasters;
        }

        protected override SiteMaster FindImpl(int key, DbSet<SiteMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.SiteID == key);
        }

        protected override SiteMaster FindImplWithExpand(int key, DbSet<SiteMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.SiteID == key);
        }

        public string SaveList(IList<SiteMaster> model,RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (SiteMaster m in model)
                    {
                        context.SiteMasters.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
            
        }

        public IList<procGetSiteList_Result> GetSiteDataList(RequestData requestData)
        {
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetSiteList(requestData.SiteCode,sector).ToList();
                }
            }
        }

        //Krish
        public IList<procGetSiteByRegionList_Result> GetSiteByRegionDataList(int regionId,RequestData requestData)
        {
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetSiteByRegionList("", sector, regionId).ToList();
                }
            }
        }
        
        public IList<NationalprocGetSiteList_Result> NationalGetSiteMasterDataList(RequestData requestData)
        {
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.NationalprocGetSiteList("").ToList();
                }
            }
        }



        public IList<CuisineMaster> GetCuisineDataList(RequestData requestData)
        {
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.CuisineMasters.ToList();
                }
            }
        }


        public string SaveCuisine(RequestData requestData, CuisineMasterData model, int username)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    string code = (from record in context.CuisineMasters orderby record.ID descending select record.CuisineCode).First();
                    if (string.IsNullOrEmpty(code))
                    {
                        code = "CUS-00001";

                    }
                    else
                    {
                        var lcode = code.Substring(0, 3);
                        var rcode = code.Substring(4, 5);
                        code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0'); ;
                        model.CuisineCode = code;
                        res= context.procSavecuisinemasterDetails(model.CuisineCode, model.Name, username).ToString();
                    }
                }
            }
            return "Success";

        }


        public string UpdateCuisine(RequestData requestData, CuisineMasterData model, int username)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    res = context.procUpdatecuisinemasterDetails(model.CuisineCode, model.Name, username).ToString();
                    
                }
            }
            return "Success";

        }


        public IList<LifeStyleTagMaster> GetLifeStyleTagDataList(RequestData requestData)
        {
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.LifeStyleTagMasters.ToList();
                }
            }
        }


        public string SaveLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    string code = (from record in context.LifeStyleTagMasters orderby record.ID descending select record.LifeStyleTagCode).FirstOrDefault();
                    if (string.IsNullOrEmpty(code))
                    {
                        code = "LST-00001";

                    }
                    else
                    {
                        var lcode = code.Substring(0, 3);
                        var rcode = code.Substring(4, 5);
                        code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0'); ;
                        
                        
                    }
                    model.LifeStyleTagCode = code;
                    var lifeStyleTagMaster = new LifeStyleTagMaster();
                    lifeStyleTagMaster.Name = model.Name;
                    lifeStyleTagMaster.LifeStyleTagCode = model.LifeStyleTagCode;
                    lifeStyleTagMaster.CreatedBy = requestData.SessionUserId;
                    lifeStyleTagMaster.CreatedOn = DateTime.Now;
                    lifeStyleTagMaster.IsActive = true;
                    context.LifeStyleTagMasters.Add(lifeStyleTagMaster);
                    context.SaveChanges();
                }
            }
            return "Success";

        }


        public string UpdateLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var lifestyletagdata = context.LifeStyleTagMasters.FirstOrDefault(m => m.LifeStyleTagCode == model.LifeStyleTagCode);
                    if (lifestyletagdata != null && lifestyletagdata.ID > 0)
                    {
                        lifestyletagdata.Name = model.Name;
                        lifestyletagdata.ModifiedBy = requestData.SessionUserId;
                        lifestyletagdata.ModifiedOn = DateTime.Now;
                        lifestyletagdata.IsActive = model.IsActive;
                        context.LifeStyleTagMasters.AddOrUpdate(lifestyletagdata);
                        context.SaveChanges();
                    }

                }
            }
            return "Success";

        }

        public IList<HealthTagMaster> GetHealthTagDataList(RequestData requestData)
        {
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.HealthTagMasters.Where(m => m.SectorCode == requestData.SessionSectorNumber).ToList();
                }
            }
        }




        public string SaveHealthTag(RequestData requestData, HealthTagMasterData model, int username)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    string code = (from record in context.HealthTagMasters orderby record.ID descending select record.HealthTagCode).FirstOrDefault();
                    if (string.IsNullOrEmpty(code))
                    {
                        code = "HLT-00001";

                    }
                    else
                    {
                        var lcode = code.Substring(0, 3);
                        var rcode = code.Substring(4, 5);
                        code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0'); ;
                    }
                    model.HealthTagCode = code;
                    var healthTagMaster = new HealthTagMaster();
                    healthTagMaster.Name = model.Name;
                    healthTagMaster.HealthTagCode = model.HealthTagCode;
                    healthTagMaster.CreatedBy = requestData.SessionUserId;
                    healthTagMaster.CreatedOn = DateTime.Now;
                    healthTagMaster.IsActive = true;
                    healthTagMaster.SectorCode = requestData.SessionSectorNumber;
                    context.HealthTagMasters.Add(healthTagMaster);
                    context.SaveChanges();
                }
            }
            return "Success";

        }


        public string UpdateHealthTag(RequestData requestData, HealthTagMasterData model, int username)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var healthtagdata = context.HealthTagMasters.FirstOrDefault(m => m.HealthTagCode == model.HealthTagCode);
                    if(healthtagdata != null && healthtagdata.ID > 0)
                    {
                        healthtagdata.Name = model.Name;
                        healthtagdata.ModifiedBy = requestData.SessionUserId;
                        healthtagdata.ModifiedOn = DateTime.Now;
                        healthtagdata.IsActive = model.IsActive;
                        healthtagdata.SectorCode = requestData.SessionSectorNumber;
                        context.HealthTagMasters.AddOrUpdate(healthtagdata);
                        context.SaveChanges();
                    }
                }
            }
            return "Success";

        }



        public IList<procshowregionmaster_Result> GetRegionMasterDataList(RequestData requestData)
        {
            string sector = requestData.SessionSectorNumber;
            
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {


                    // return context.RegionMasters.ToList();
                    return  context.procshowregionmaster(sector).ToList();
                }
            }
        }

       

        public IList<procGetCPUList_Result> GetCPUDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetCPUList().ToList();
                }
            }
        }

        public IList<procGetDKList_Result> GetDKDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetDKList().ToList();
                }
            }
        }

        public string UpdateHRISLocationData(RequestData requestData, SiteMasterData siteMasterData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procUpdateHRISLocationData(siteMasterData.PeopleStrongSiteAlias, siteMasterData.SiteCode, requestData.SessionUserId).ToString();
                }
            }
        }

        public IList<procGetExpiryCategoryMasterData_Result> GetExpiryCategoryDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetExpiryCategoryMasterData().ToList();
                }
            }
        }

        public IList<procGetShelfLifeUOMMasterData_Result> GetShelfLifeUOMDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetShelfLifeUOMMasterData().ToList();
                }
            }
        }

        public string SaveExpiryCategory(RequestData requestData, ExpiryCategoryMaster model)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                   
                    if (model.ID > 0)
                    {
                        var getdata = context.ExpiryCategoryMasters.Where(m => m.ID == model.ID).FirstOrDefault();
                        if(getdata !=null && getdata.ID > 0)
                        {
                            getdata.Name = model.Name;
                            getdata.ShelfLife = model.ShelfLife;
                            getdata.ShelfLifeUOMCode = model.ShelfLifeUOMCode;
                            getdata.ExpiryCategoryCode = model.ExpiryCategoryCode;
                            getdata.ModifiedBy = requestData.SessionUserId;
                            getdata.ModifiedOn = DateTime.Now;
                            context.ExpiryCategoryMasters.AddOrUpdate(getdata);
                        }
                    }
                    else
                    {
                        string code = (from record in context.ExpiryCategoryMasters orderby record.ID descending select record.ExpiryCategoryCode).FirstOrDefault();
                        if (string.IsNullOrEmpty(code))
                        {
                            code = "ECT-00001";

                        }
                        else
                        {
                            var lcode = code.Substring(0, 3);
                            var rcode = code.Substring(4, 5);
                            code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                        }
                        model.ExpiryCategoryCode = code;
                        model.CreatedBy = requestData.SessionUserId;
                        model.CreatedOn = DateTime.Now;
                        context.ExpiryCategoryMasters.AddOrUpdate(model);
                    }
                    context.SaveChanges();
                }
            }
            return "Success";
        }

        public string SaveNutritionMaster(RequestData requestData, NutritionElementMaster model)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    if (model.ID > 0)
                    {
                        var getdata = context.NutritionElementMasters.Where(m => m.ID == model.ID).FirstOrDefault();
                        if (getdata != null && getdata.ID > 0)
                        {
                            getdata.Name = model.Name;
                            getdata.NutritionElementTypeCode = model.NutritionElementTypeCode;
                            getdata.NutritionElementUOMCode = model.NutritionElementUOMCode;
                            getdata.NutritionElementCode = model.NutritionElementCode;
                            getdata.DisplayOrder = model.DisplayOrder;
                            getdata.IsDisplay = model.IsDisplay;
                            getdata.ModifiedBy = requestData.SessionUserId;
                            getdata.ModifiedOn = DateTime.Now;
                            getdata.IsActive = model.IsActive;
                            context.NutritionElementMasters.AddOrUpdate(getdata);
                        }
                    }
                    else
                    {
                        string code = (from record in context.NutritionElementMasters orderby record.ID descending select record.NutritionElementCode).FirstOrDefault();
                        if (string.IsNullOrEmpty(code))
                        {
                            code = "NTR-00001";

                        }
                        else
                        {
                            var lcode = code.Substring(0, 3);
                            var rcode = code.Substring(4, 5);
                            code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0'); ;
                        }
                        model.NutritionElementCode = code;
                        model.CreatedBy = requestData.SessionUserId;
                        model.CreatedOn = DateTime.Now;
                        context.NutritionElementMasters.AddOrUpdate(model);
                    }
                    context.SaveChanges();
                }
            }
            return "Success";
        }

        public IList<procGetNutritionMasterData_Result> GetNutritionMasterDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetNutritionMasterData().ToList();
                }
            }
        }

        public IList<NutritionElementType> GetNutritionElementTypeDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.NutritionElementTypes.ToList();
                }
            }
        }

        public IList<DietType> GetDietTypeDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.DietTypes.ToList();
                }
            }
        }

        public IList<Texture> GetTextureDataList(RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.Textures.ToList();
                }
            }
        }

        public string SaveCommonMaster(RequestData requestData, CommonMasterData model)
        {
            var retval = string.Empty;
            if (model.ModuleName.ToUpper() == "TEXTURE")
            {
               retval =  SaveTextureMaster(requestData, model);
            }
            else if(model.ModuleName.ToUpper() == "DIETTYPE")
            {
               retval = SaveDietTypeMaster(requestData, model);
            }
            else if (model.ModuleName.ToUpper() == "NUTRIENTTYPE")
            {
                retval = SaveNutrientTypeMaster(requestData, model);
            }
            else if (model.ModuleName.ToUpper() == "NUTRIENTUOM")
            {
                retval = SaveNutrientUOMMaster(requestData, model);
            }
            return retval;
        }

        public string SaveTextureMaster(RequestData requestData, CommonMasterData model)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    if (!string.IsNullOrEmpty(model.Code))
                    {
                        var getdata = context.Textures.Where(m => m.TextureCode == model.Code).FirstOrDefault();
                        if (getdata != null && getdata.ID > 0)
                        {
                            getdata.Name = model.Name;
                            getdata.TextureCode = model.Code;
                            getdata.ModifiedBy = requestData.SessionUserId;
                            getdata.ModifiedOn = DateTime.Now;
                            getdata.IsActive = model.IsActive;
                            context.Textures.AddOrUpdate(getdata);
                        }
                    }
                    else
                    {
                        string code = (from record in context.Textures orderby record.ID descending select record.TextureCode).FirstOrDefault();
                        if (string.IsNullOrEmpty(code))
                        {
                            code = "TXT-00001";

                        }
                        else
                        {
                            var lcode = code.Substring(0, 3);
                            var rcode = code.Substring(4, 5);
                            code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0'); ;
                        }
                        var texture = new Texture();
                        texture.TextureCode = code;
                        texture.Name = model.Name;
                        texture.IsActive = true;
                        texture.CreatedBy = requestData.SessionUserId;
                        texture.CreatedOn = DateTime.Now;
                        context.Textures.AddOrUpdate(texture);
                    }
                    context.SaveChanges();
                }
            }
            return "Success";
        }

        public string SaveDietTypeMaster(RequestData requestData, CommonMasterData model)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    if (!string.IsNullOrEmpty(model.Code))
                    {
                        var getdata = context.DietTypes.Where(m => m.DietTypeCode == model.Code).FirstOrDefault();
                        if (getdata != null && getdata.ID > 0)
                        {
                            getdata.Name = model.Name;
                            getdata.DietTypeCode = model.Code;
                            getdata.ModifiedBy = requestData.SessionUserId;
                            getdata.ModifiedOn = DateTime.Now;
                            getdata.IsActive = model.IsActive;
                            context.DietTypes.AddOrUpdate(getdata);
                        }
                    }
                    else
                    {
                        string code = (from record in context.DietTypes orderby record.ID descending select record.DietTypeCode).FirstOrDefault();
                        if (string.IsNullOrEmpty(code))
                        {
                            code = "DIT-00001";

                        }
                        else
                        {
                            var lcode = code.Substring(0, 3);
                            var rcode = code.Substring(4, 5);
                            code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0'); ;
                        }
                        var DietType = new DietType();
                        DietType.DietTypeCode = code;
                        DietType.Name = model.Name;
                        DietType.IsActive = true;
                        DietType.CreatedBy = requestData.SessionUserId;
                        DietType.CreatedOn = DateTime.Now;
                        context.DietTypes.AddOrUpdate(DietType);
                    }
                    context.SaveChanges();
                }
            }
            return "Success";
        }

        public string SaveNutrientUOMMaster(RequestData requestData, CommonMasterData model)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    if (!string.IsNullOrEmpty(model.Code))
                    {
                        var getdata = context.NutrientElementUOMs.Where(m => m.Code == model.Code).FirstOrDefault();
                        if (getdata != null && getdata.ID > 0)
                        {
                            getdata.Name = model.Name;
                            getdata.Code = model.Code;
                            getdata.ModifiedBy = requestData.SessionUserId;
                            getdata.ModifiedOn = DateTime.Now;
                            getdata.IsActive = model.IsActive;
                            context.NutrientElementUOMs.AddOrUpdate(getdata);
                        }
                    }
                    else
                    {
                        string code = (from record in context.NutrientElementUOMs orderby record.ID descending select record.Code).FirstOrDefault();
                        if (string.IsNullOrEmpty(code))
                        {
                            code = "NUM-00001";

                        }
                        else
                        {
                            var lcode = code.Substring(0, 3);
                            var rcode = code.Substring(4, 5);
                            code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0'); ;
                        }
                        var texture = new NutrientElementUOM();
                        texture.Code = code;
                        texture.Name = model.Name;
                        texture.IsActive = true;
                        texture.CreatedBy = requestData.SessionUserId;
                        texture.CreatedOn = DateTime.Now;
                        context.NutrientElementUOMs.AddOrUpdate(texture);
                    }
                    context.SaveChanges();
                }
            }
            return "Success";
        }

        public string SaveNutrientTypeMaster(RequestData requestData, CommonMasterData model)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    if (!string.IsNullOrEmpty(model.Code))
                    {
                        var getdata = context.NutritionElementTypes.Where(m => m.Code == model.Code).FirstOrDefault();
                        if (getdata != null && getdata.ID > 0)
                        {
                            getdata.Name = model.Name;
                            getdata.Code = model.Code;
                            getdata.ModifiedBy = requestData.SessionUserId;
                            getdata.ModifiedOn = DateTime.Now;
                            getdata.IsActive = model.IsActive;
                            context.NutritionElementTypes.AddOrUpdate(getdata);
                        }
                    }
                    else
                    {
                        string code = (from record in context.Textures orderby record.ID descending select record.TextureCode).FirstOrDefault();
                        if (string.IsNullOrEmpty(code))
                        {
                            code = "NTR-00001";

                        }
                        else
                        {
                            var lcode = code.Substring(0, 3);
                            var rcode = code.Substring(4, 5);
                            code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0'); ;
                        }
                        var texture = new NutritionElementType();
                        texture.Code = code;
                        texture.Name = model.Name;
                        texture.IsActive = true;
                        texture.CreatedBy = requestData.SessionUserId;
                        texture.CreatedOn = DateTime.Now;
                        context.NutritionElementTypes.AddOrUpdate(texture);
                    }
                    context.SaveChanges();
                }
            }
            return "Success";
        }



        public IList<NutrientElementUOM> GetNutrientUOMDataList(RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.NutrientElementUOMs.ToList();
                }
            }
        }

        public IList<ItemSectorMaster> GetItemSectorMasterDataList(RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.ItemSectorMasters.ToList();
                }
            }
        }

        public IList<MenuType2> GetMenuType2MasterDataList(RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.MenuType2.ToList();
                }
            }
        }

        public IList<Area> GetAreaMasterDataList(RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.Areas.ToList();
                }
            }
        }

        public IList<MenuType1> GetMenuType1MasterDataList(RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.MenuType1.ToList();
                }
            }
        }

        public IList<NutritionElementType> GetNutrientTypeDataList(RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.NutritionElementTypes.ToList();
                }
            }
        }
    }
}
