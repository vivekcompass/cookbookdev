﻿using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IReasonRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ReasonRepository : EFSynchronousRepository<int, Reason, CookBookCoreEntities>,
        IReasonRepository
    {
        protected override DbSet<Reason> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.Reasons;
        }

        protected override Reason FindImpl(int key, DbSet<Reason> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override Reason FindImplWithExpand(int key, DbSet<Reason> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
