﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.Data.MasterData;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IAllergenRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AllergenRepositoy : EFSynchronousRepository<int, Allergen, CookBookCommonEntities>,
        IAllergenRepository
    {
        protected override DbSet<Allergen> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.Allergens;
        }

        protected override Allergen FindImpl(int key, DbSet<Allergen> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override Allergen FindImplWithExpand(int key, DbSet<Allergen> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
