﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IContainerRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ContainerRepository : EFSynchronousRepository<int, Container, CookBookCoreEntities>,
        IContainerRepository
    {
        protected override DbSet<Container> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.Containers;
        }

        protected override Container FindImpl(int key, DbSet<Container> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override Container FindImplWithExpand(int key, DbSet<Container> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
