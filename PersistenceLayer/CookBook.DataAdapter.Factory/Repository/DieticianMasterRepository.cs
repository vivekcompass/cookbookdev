﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CookBook.Encryption.Interfaces;
using System.Data.Entity.Migrations;
using CookBook.Aspects.Factory;
using CookBook.Aspects.Constants;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IDieticianMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DieticianMasterRepository : EFSynchronousRepository<int, DieticianMaster, CookBookCoreEntities>,
        IDieticianMasterRepository
    {

        protected override DbSet<DieticianMaster> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.DieticianMasters;
        }

        protected override DieticianMaster FindImpl(int key, DbSet<DieticianMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override DieticianMaster FindImplWithExpand(int key, DbSet<DieticianMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public List<procGetDieticianMaster_Result> GetDieticianMaster(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetDieticianMaster().ToList();
                }
            }
        }
        public List<procGetDieticianMasterBATCHHEADER_Result> GetDieticianMasterHISTORY(RequestData requestData, string username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetDieticianMasterBATCHHEADER(username).ToList();
                }
            }
        }

        public List<procGetDieticianMasterBATCHDETAILS_Result> GetDieticianMasterHISTORYDet(RequestData requestData, string username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetDieticianMasterBATCHDETAILS(username).ToList();
                }
            }
        }
        public List<procshowGetALLDieticianMasterHISTORYBATCHWISE_Result> GetshowGetALLDieticianMasterHISTORYBATCHWISE(RequestData requestData,  string batchno)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procshowGetALLDieticianMasterHISTORYBATCHWISE(null, batchno).ToList();
                }
            }
        }
        public string AddUpdateDieticianMasterList(RequestData request, string xml, string username)
        {
            string outres = string.Empty;

            //CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    //return coreEntities.ProgBulkUpdateXMLMOGAPL(xml, username).ToList();
                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    context.procBulkInsertXMLDieticianMaster(xml, username, myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;
                }
            }
        }
        public string SaveDieticianMasterStatus(RequestData request, int id, int username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    var getPm = context.DieticianMasters.FirstOrDefault(a => a.ID == id);
                    getPm.IsActive = getPm.IsActive ? false : true;
                    getPm.ModifiedBy = username;
                    getPm.ModifiedOn = DateTime.Now;
                    context.SaveChanges();
                }
            }
            return "Success";
        }
        public List<procshowGetALLHISTORYDieticianMaster_Result> GetALLHISTORYDetailsDieticianMaster(RequestData request)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    return context.procshowGetALLHISTORYDieticianMaster(null).ToList();
                }
            }
        }

        #region Kondapur Hosptial
        public string SaveHISPatientProfileMaster(RequestData requestData, List<HISPatientProfileData> model)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var bedList = context.BedMasters.ToList();
                    foreach (HISPatientProfileData hobj in model)
                    {
                        if (!string.IsNullOrEmpty(hobj.PatientID) && !string.IsNullOrEmpty(hobj.PatientName) &&
                            !string.IsNullOrEmpty(hobj.DateOfBirth) && !string.IsNullOrEmpty(hobj.Bed))
                        {

                            var obj = new PatientProfile();
                            obj = context.PatientProfiles.Where(m => m.PatientID == hobj.PatientID).FirstOrDefault();
                            var bedOjb = bedList.Where(m => m.HISBedCode == hobj.HISBedCode).FirstOrDefault();

                            if (obj != null && !string.IsNullOrWhiteSpace(obj.PatientID))
                            {
                                if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                {
                                    obj.BedCode = bedOjb.BedCode;
                                    obj.FloorNumber = bedOjb.FloorNumber;
                                }
                                // obj.SiteCode = requestData.SiteCode;
                                obj.PatientName = hobj.PatientName;
                                obj.HISBedCode = hobj.BedCode;
                                obj.BedNumber = hobj.Bed;
                                obj.ModifiedBy = "1";
                                obj.ModifiedDate = DateTime.Now;
                                obj.DateofBirth = hobj.DateOfBirth;
                                obj.VisitStatus = hobj.VisitStatus;
                                obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj.IsActive = true;
                                }
                                else
                                {
                                    obj.IsActive = false;
                                    if(obj.VisitStatus != hobj.VisitStatus)
                                    {
                                        obj.FinancialDischargeDate = System.DateTime.Now;
                                    }

                                }
                                if (hobj.NationalityCode == "IND")
                                {
                                    obj.MenuType = "PBP-00001";
                                }
                                else if (hobj.NationalityCode == "INTL")
                                {
                                    obj.MenuType = "PBP-00003";
                                }
                                else
                                {
                                    obj.MenuType = "PBP-00002";
                                }
                                context.PatientProfiles.AddOrUpdate(obj);
                            }
                            else
                            {
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj = new PatientProfile();
                                    if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                    {
                                        obj.BedCode = bedOjb.BedCode;
                                        obj.FloorNumber = bedOjb.FloorNumber;
                                    }
                                    obj.SiteCode = requestData.SiteCode;
                                    obj.PatientID = hobj.PatientID;
                                    obj.PatientName = hobj.PatientName;
                                    obj.HISBedCode = hobj.BedCode;
                                    obj.BedNumber = hobj.Bed;
                                    obj.CreatedBy = "1";
                                    obj.CreatedDate = DateTime.Now;
                                    obj.DateofBirth = hobj.DateOfBirth;
                                    obj.VisitStatus = hobj.VisitStatus;
                                    obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                    if (hobj.VisitStatus != "Financial Discharge")
                                    {
                                        obj.IsActive = true;
                                    }
                                    else
                                    {
                                        obj.IsActive = false;
                                        if (obj.VisitStatus != hobj.VisitStatus)
                                        {
                                            obj.FinancialDischargeDate = System.DateTime.Now;
                                        }
                                    }
                                    if (hobj.NationalityCode == "IND")
                                    {
                                        obj.MenuType = "PBP-00001";
                                    }
                                    else if (hobj.NationalityCode == "INTL")
                                    {
                                        obj.MenuType = "PBP-00003";
                                    }
                                    else
                                    {
                                        obj.MenuType = "PBP-00002";
                                    }
                                    obj.DietCategory = "DTC-00003";
                                    obj.DietType = "[{\"DietTypeCode\":\"DIT-00001\",\"DietTypeName\":\"Normal\"}]";
                                    obj.Texture = "TXT-00001";
                                    obj.AllergicTo = "[]";
                                    obj.OtherAllergens = "[]";

                                    obj.IsSpecificFreq = false;
                                    obj.IsHomeFood = false;
                                    context.PatientProfiles.AddOrUpdate(obj);
                                }
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }

            return "ture";
        }

        public string SaveHISUserData(RequestData requestData, List<HISUserData> model)
        {
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (HISUserData hobj in model)
                    {
                        var obj = new UserMaster();
                        obj = context.UserMasters.Where(m => m.EmpCode == hobj.EmpCode).FirstOrDefault();

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.EmpCode))
                        {
                            //obj.SiteCode = requestData.SiteCode;
                            // obj.EmpCode = hobj.EmpCode;
                            //obj.UserName = hobj.EmpName;
                            //obj.Mobile = hobj.MobileNo;
                            //if (obj.IsActive)
                            //{
                            //    continue;
                            //}
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.ModifiedBy = "1";
                            obj.ModifiedDate = DateTime.Now;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new UserMaster();
                            obj.SiteCode = requestData.SiteCode;
                            obj.EmpCode = hobj.EmpCode;
                            obj.UserName = hobj.EmpName;
                            obj.Mobile = hobj.MobileNo;
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.CreatedBy = "1";
                            obj.CreatedDate = DateTime.Now;
                            obj.SiteCode = requestData.SiteCode;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                    }
                    context.SaveChanges();
                }
            }
            return "ture";
        }

        public string SaveHISBedMasterData(RequestData requestData, List<HISBedMasterData> model)
        {
            using (var transaction = new EFTransaction())
            {
                LogTraceFactory.WriteLogWithCategory("Service SaveHISBedMasterData- Start "+requestData.SessionSectorName+"", LogTraceCategoryNames.Tracing);
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    LogTraceFactory.WriteLogWithCategory("Service SaveHISBedMasterData- Start " + context.Database.Connection.ConnectionString.ToString()
                        + "", LogTraceCategoryNames.Tracing);
                    var codeCount = 0;
                    string code = "BED-00001";
                    var roombillingTypeCategories = context.RoomBillingCategories.ToList();
                    foreach (HISBedMasterData hobj in model)
                    {
                        var obj = new BedMaster();
                        obj = context.BedMasters.Where(m => m.HISBedCode == hobj.BedCode).FirstOrDefault();
                        var roomBillingType = new RoomBillingCategory();
                        if (obj.FloorNumber == "LABOUR WARD")
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name.ToLower() == obj.FloorNumber.ToLower()
                                                                                   && obj.FloorNumber.ToUpper() == "LABOUR WARD").FirstOrDefault();
                        }
                        else
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name.ToLower() == hobj.Bedcategory.ToLower()).FirstOrDefault();
                        }

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.BedCode))
                        {
                            // obj.SiteCode = requestData.SiteCode;
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;
                            obj.FloorNumber = !string.IsNullOrWhiteSpace(hobj.WardName) ? hobj.WardName.Substring(3) : hobj.WardName;
                            obj.RoomNumber = hobj.WardCode;
                            obj.ModifiedBy = 1;
                            obj.ModifiedOn = DateTime.Now;
                            if (hobj.Bedcategory == "BASINET" || hobj.Bedcategory == "DAY CARE" || hobj.Bedcategory == "NICU"
                                || hobj.WardName == "WF LABOUR WARD")
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new BedMaster();

                            if (codeCount == 0)
                            {
                                code = context.BedMasters.OrderByDescending(z => z.BedCode).FirstOrDefault()?.BedCode;
                                if (code is null)
                                {
                                    code = "BED-00001";
                                }
                            }
                            if (code != null && codeCount > 0)
                            {
                                var lcode = code.Substring(0, 3);
                                var rcode = code.Substring(4, 5);
                                code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                            }

                            obj.BedCode = code;
                            obj.SiteCode = requestData.SiteCode;
                            obj.HISBedCode = hobj.BedCode;
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;
                            obj.FloorNumber = !string.IsNullOrWhiteSpace(hobj.WardName) ? hobj.WardName.Substring(3) : hobj.WardName;
                            obj.RoomNumber = hobj.WardCode;
                            obj.IsActive = true;
                            obj.CreatedBy = 1;
                            obj.CreatedOn = DateTime.Now;
                            if (hobj.Bedcategory == "BASINET" || hobj.Bedcategory == "DAY CARE" || hobj.Bedcategory == "NICU"
                               || hobj.WardName == "WF LABOUR WARD")
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.Add(obj);
                        }
                        codeCount++;
                    }
                    context.SaveChanges();
                }
            }
            LogTraceFactory.WriteLogWithCategory("Service SaveHISBedMasterData- end", LogTraceCategoryNames.Tracing);
            return "ture";
        }
        #endregion

        #region HN Hosptial
        public string SaveHISPatientProfileMaster_HN(RequestData requestData, List<HISPatientProfileData> model)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var bedList = context.BedMasters.ToList();
                    foreach (HISPatientProfileData hobj in model)
                    {
                        if (!string.IsNullOrEmpty(hobj.PatientID) && !string.IsNullOrEmpty(hobj.PatientName) &&
                            !string.IsNullOrEmpty(hobj.DateOfBirth) && !string.IsNullOrEmpty(hobj.Bed))
                        {

                            var obj = new PatientProfile();
                            obj = context.PatientProfiles.Where(m => m.PatientID == hobj.PatientID).FirstOrDefault();
                            var bedOjb = bedList.Where(m => m.HISBedCode == hobj.HISBedCode).FirstOrDefault();

                            if (obj != null && !string.IsNullOrWhiteSpace(obj.PatientID))
                            {
                                if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                {
                                    obj.BedCode = bedOjb.BedCode;
                                    obj.FloorNumber = bedOjb.FloorNumber;
                                }
                                // obj.SiteCode = requestData.SiteCode;
                                obj.PatientName = hobj.PatientName;
                                obj.HISBedCode = hobj.BedCode;
                                obj.BedNumber = hobj.Bed;
                                obj.ModifiedBy = "1";
                                obj.ModifiedDate = DateTime.Now;
                                obj.DateofBirth = hobj.DateOfBirth;
                                obj.VisitStatus = hobj.VisitStatus;
                                obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj.IsActive = true;
                                }
                                else
                                {
                                    obj.IsActive = false;
                                    if (obj.VisitStatus != hobj.VisitStatus)
                                    {
                                        obj.FinancialDischargeDate = System.DateTime.Now;
                                    }
                                }
                                if (hobj.NationalityCode == "IND")
                                {
                                    obj.MenuType = "PBP-00001";
                                }
                                else if (hobj.NationalityCode == "INTL")
                                {
                                    obj.MenuType = "PBP-00003";
                                }
                                else
                                {
                                    obj.MenuType = "PBP-00002";
                                }
                                context.PatientProfiles.AddOrUpdate(obj);
                            }
                            else
                            {
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj = new PatientProfile();
                                    if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                    {
                                        obj.BedCode = bedOjb.BedCode;
                                        obj.FloorNumber = bedOjb.FloorNumber;
                                    }
                                    obj.SiteCode = requestData.SiteCode;
                                    obj.PatientID = hobj.PatientID;
                                    obj.PatientName = hobj.PatientName;
                                    obj.HISBedCode = hobj.BedCode;
                                    obj.BedNumber = hobj.Bed;
                                    obj.CreatedBy = "1";
                                    obj.CreatedDate = DateTime.Now;
                                    obj.DateofBirth = hobj.DateOfBirth;
                                    obj.VisitStatus = hobj.VisitStatus;
                                    obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                    if (hobj.VisitStatus != "Financial Discharge")
                                    {
                                        obj.IsActive = true;
                                    }
                                    else
                                    {
                                        obj.IsActive = false;
                                        if (obj.VisitStatus != hobj.VisitStatus)
                                        {
                                            obj.FinancialDischargeDate = System.DateTime.Now;
                                        }
                                    }
                                    if (hobj.NationalityCode == "IND")
                                    {
                                        obj.MenuType = "PBP-00001";
                                    }
                                    else if (hobj.NationalityCode == "INTL")
                                    {
                                        obj.MenuType = "PBP-00003";
                                    }
                                    else
                                    {
                                        obj.MenuType = "PBP-00002";
                                    }
                                    obj.DietCategory = "DTC-00003";
                                    obj.DietType = "[{\"DietTypeCode\":\"DIT-00001\",\"DietTypeName\":\"Normal\"}]";
                                    obj.Texture = "TXT-00001";
                                    obj.AllergicTo = "[]";
                                    obj.OtherAllergens = "[]";

                                    obj.IsSpecificFreq = false;
                                    obj.IsHomeFood = false;
                                    context.PatientProfiles.AddOrUpdate(obj);
                                }
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }

            return "ture";
        }

        public string SaveHISUserData_HN(RequestData requestData, List<HISUserData> model)
        {
            LogTraceFactory.WriteLogWithCategory("Service SaveHISUserData_HN- Start " + requestData.SessionSectorName + model.Count() +"", LogTraceCategoryNames.Tracing);
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (HISUserData hobj in model)
                    {
                        var obj = new UserMaster();
                        obj = context.UserMasters.Where(m => m.EmpCode == hobj.EmpCode).FirstOrDefault();

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.EmpCode))
                        {
                            //obj.SiteCode = requestData.SiteCode;
                            // obj.EmpCode = hobj.EmpCode;
                            //obj.UserName = hobj.EmpName;
                            //obj.Mobile = hobj.MobileNo;
                            //if (obj.IsActive)
                            //{
                            //    continue;
                            //}
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.ModifiedBy = "1";
                            obj.ModifiedDate = DateTime.Now;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new UserMaster();
                            obj.SiteCode = requestData.SiteCode;
                            obj.EmpCode = hobj.EmpCode;
                            obj.UserName = hobj.EmpName;
                            obj.Mobile = hobj.MobileNo;
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.CreatedBy = "1";
                            obj.CreatedDate = DateTime.Now;
                            obj.SiteCode = requestData.SiteCode;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                    }
                    context.SaveChanges();
                }
            }
            LogTraceFactory.WriteLogWithCategory("Service SaveHISUserData_HN- End " + requestData.SessionSectorName + "", LogTraceCategoryNames.Tracing);
            return "ture";
           
        }

        public string SaveHISBedMasterData_HN(RequestData requestData, List<HISBedMasterData> model)
        {
            using (var transaction = new EFTransaction())
            {
                LogTraceFactory.WriteLogWithCategory("Service HN SaveHISBedMasterData- Start " + requestData.SessionSectorName + "", LogTraceCategoryNames.Tracing);
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    LogTraceFactory.WriteLogWithCategory("Service HN SaveHISBedMasterData- Start " + context.Database.Connection.ConnectionString.ToString()
                        + "", LogTraceCategoryNames.Tracing);

                    var codeCount = 0;
                    string code = "BED-00001";
                    var roombillingTypeCategories = context.RoomBillingCategories.ToList();
                    foreach (HISBedMasterData hobj in model)
                    {
                        var obj = new BedMaster();
                        obj = context.BedMasters.Where(m => m.HISBedCode == hobj.BedCode).FirstOrDefault();
                        var roomBillingType = new RoomBillingCategory();
                        roomBillingType = roombillingTypeCategories.Where(m => m.Name.ToLower() == hobj.Bedcategory.ToLower()).FirstOrDefault();

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.BedCode))
                        {
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;

                            var floorNo = !string.IsNullOrWhiteSpace(hobj.WardName) ? hobj.WardName.Substring(0,2) : hobj.WardName;
                            if(floorNo == "GF")
                            {
                                obj.FloorNumber = "GROUND FLOOR";
                            }
                            else if (floorNo == "1F")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (floorNo == "2F")
                            {
                                obj.FloorNumber = "2ND FLOOR";
                            }
                            else if (floorNo == "3F")
                            {
                                obj.FloorNumber = "3RD FLOOR";
                            }
                            else if (floorNo == "4F")
                            {
                                obj.FloorNumber = "4TH FLOOR";
                            }
                            obj.RoomNumber = hobj.WardCode;
                            obj.ModifiedBy = 1;
                            obj.ModifiedOn = DateTime.Now;
                            if (hobj.Bedcategory == "BASINET" || hobj.Bedcategory == "DAY CARE" || hobj.Bedcategory == "NICU" ||
                                hobj.WardName == "4F -IVF" || hobj.WardName == "4F -OT")
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new BedMaster();

                            if (codeCount == 0)
                            {
                                code = context.BedMasters.OrderByDescending(z => z.BedCode).FirstOrDefault()?.BedCode;
                                if (code is null)
                                {
                                    code = "BED-00001";
                                }
                            }
                            if (code != null && codeCount > 0)
                            {
                                var lcode = code.Substring(0, 3);
                                var rcode = code.Substring(4, 5);
                                code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                            }

                            obj.BedCode = code;
                            obj.SiteCode = requestData.SiteCode;
                            obj.HISBedCode = hobj.BedCode;
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;
                            obj.RoomNumber = hobj.WardCode;
                            obj.IsActive = true;
                            obj.CreatedBy = 1;
                            obj.CreatedOn = DateTime.Now;
                            var floorNo = !string.IsNullOrWhiteSpace(hobj.WardName) ? hobj.WardName.Substring(0, 2) : hobj.WardName;
                            if (floorNo == "GF")
                            {
                                obj.FloorNumber = "GROUND FLOOR";
                            }
                            else if (floorNo == "1F")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (floorNo == "2F")
                            {
                                obj.FloorNumber = "2ND FLOOR";
                            }
                            else if (floorNo == "3F")
                            {
                                obj.FloorNumber = "3RD FLOOR";
                            }
                            else if (floorNo == "4F")
                            {
                                obj.FloorNumber = "4TH FLOOR";
                            }
                            if (hobj.Bedcategory == "BASINET" || hobj.Bedcategory == "DAY CARE" || hobj.Bedcategory == "NICU"
                              ||  hobj.WardName == "4F -IVF" || hobj.WardName == "4F -OT")
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.Add(obj);
                        }
                        codeCount++;
                    }
                    context.SaveChanges();
                }
            }
            LogTraceFactory.WriteLogWithCategory("Service HN SaveHISBedMasterData- end", LogTraceCategoryNames.Tracing);
            return "ture";
        }

        #endregion


        #region R10 Hosptial
        public string SaveHISPatientProfileMaster_R10(RequestData requestData, List<HISPatientProfileData> model)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var bedList = context.BedMasters.ToList();
                    foreach (HISPatientProfileData hobj in model)
                    {
                        if (!string.IsNullOrEmpty(hobj.PatientID) && !string.IsNullOrEmpty(hobj.PatientName) &&
                            !string.IsNullOrEmpty(hobj.DateOfBirth) && !string.IsNullOrEmpty(hobj.Bed))
                        {

                            var obj = new PatientProfile();
                            obj = context.PatientProfiles.Where(m => m.PatientID == hobj.PatientID).FirstOrDefault();
                            var bedOjb = bedList.Where(m => m.HISBedCode == hobj.HISBedCode).FirstOrDefault();

                            if (obj != null && !string.IsNullOrWhiteSpace(obj.PatientID))
                            {
                                if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                {
                                    obj.BedCode = bedOjb.BedCode;
                                    obj.FloorNumber = bedOjb.FloorNumber;
                                }
                                // obj.SiteCode = requestData.SiteCode;
                                obj.PatientName = hobj.PatientName;
                                obj.HISBedCode = hobj.BedCode;
                                obj.BedNumber = hobj.Bed;
                                obj.ModifiedBy = "1";
                                obj.ModifiedDate = DateTime.Now;
                                obj.DateofBirth = hobj.DateOfBirth;
                                obj.VisitStatus = hobj.VisitStatus;
                                obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj.IsActive = true;
                                }
                                else
                                {
                                    obj.IsActive = false;
                                    if (obj.VisitStatus != hobj.VisitStatus)
                                    {
                                        obj.FinancialDischargeDate = System.DateTime.Now;
                                    }
                                }
                                if (hobj.NationalityCode == "IND")
                                {
                                    obj.MenuType = "PBP-00001";
                                }
                                //else if (hobj.NationalityCode == "INTL")
                                //{
                                //    obj.MenuType = "PBP-00003";
                                //}
                                else
                                {
                                    obj.MenuType = "PBP-00003";
                                }
                                context.PatientProfiles.AddOrUpdate(obj);
                            }
                            else
                            {
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj = new PatientProfile();
                                    if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                    {
                                        obj.BedCode = bedOjb.BedCode;
                                        obj.FloorNumber = bedOjb.FloorNumber;
                                    }
                                    obj.SiteCode = requestData.SiteCode;
                                    obj.PatientID = hobj.PatientID;
                                    obj.PatientName = hobj.PatientName;
                                    obj.HISBedCode = hobj.BedCode;
                                    obj.BedNumber = hobj.Bed;
                                    obj.CreatedBy = "1";
                                    obj.CreatedDate = DateTime.Now;
                                    obj.DateofBirth = hobj.DateOfBirth;
                                    obj.VisitStatus = hobj.VisitStatus;
                                    obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                    if (hobj.VisitStatus != "Financial Discharge")
                                    {
                                        obj.IsActive = true;
                                    }
                                    else
                                    {
                                        obj.IsActive = false;
                                        if (obj.VisitStatus != hobj.VisitStatus)
                                        {
                                            obj.FinancialDischargeDate = System.DateTime.Now;
                                        }
                                    }
                                    if (hobj.NationalityCode == "IND")
                                    {
                                        obj.MenuType = "PBP-00001";
                                    }
                                    //else if (hobj.NationalityCode == "INTL")
                                    //{
                                    //    obj.MenuType = "PBP-00003";
                                    //}
                                    else
                                    {
                                        obj.MenuType = "PBP-00002";
                                    }
                                    obj.DietCategory = "DTC-00003";
                                    obj.DietType = "[{\"DietTypeCode\":\"DIT-00004\",\"DietTypeName\":\"Cardiac\"}]";
                                    obj.Texture = "TXT-00001";
                                    obj.AllergicTo = "[]";
                                    obj.OtherAllergens = "[]";

                                    obj.IsSpecificFreq = false;
                                    obj.IsHomeFood = false;
                                    context.PatientProfiles.AddOrUpdate(obj);
                                }
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }

            return "ture";
        }

        public string SaveHISUserData_R10(RequestData requestData, List<HISUserData> model)
        {
            LogTraceFactory.WriteLogWithCategory("Service SaveHISUserData_R10- Start " + requestData.SessionSectorName + model.Count() + "", LogTraceCategoryNames.Tracing);
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (HISUserData hobj in model)
                    {
                        var obj = new UserMaster();
                        obj = context.UserMasters.Where(m => m.EmpCode == hobj.EmpCode).FirstOrDefault();

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.EmpCode))
                        {
                            //obj.SiteCode = requestData.SiteCode;
                            // obj.EmpCode = hobj.EmpCode;
                            //obj.UserName = hobj.EmpName;
                            //obj.Mobile = hobj.MobileNo;
                            //if (obj.IsActive)
                            //{
                            //    continue;
                            //}
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.ModifiedBy = "1";
                            obj.ModifiedDate = DateTime.Now;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new UserMaster();
                            obj.SiteCode = requestData.SiteCode;
                            obj.EmpCode = hobj.EmpCode;
                            obj.UserName = hobj.EmpName;
                            obj.Mobile = hobj.MobileNo;
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.CreatedBy = "1";
                            obj.CreatedDate = DateTime.Now;
                            obj.SiteCode = requestData.SiteCode;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                    }
                    context.SaveChanges();
                }
            }
            LogTraceFactory.WriteLogWithCategory("Service SaveHISUserData_R10- End " + requestData.SessionSectorName + "", LogTraceCategoryNames.Tracing);
            return "ture";
        }

        public string SaveHISBedMasterData_R10(RequestData requestData, List<HISBedMasterData> model)
        {
            using (var transaction = new EFTransaction())
            {
                LogTraceFactory.WriteLogWithCategory("Service HN SaveHISBedMasterData- Start " + requestData.SessionSectorName + "", LogTraceCategoryNames.Tracing);
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    LogTraceFactory.WriteLogWithCategory("Service HN SaveHISBedMasterData- Start " + context.Database.Connection.ConnectionString.ToString()
                        + "", LogTraceCategoryNames.Tracing);

                    var codeCount = 0;
                    string code = "BED-00001";
                    var roombillingTypeCategories = context.RoomBillingCategories.ToList();
                    foreach (HISBedMasterData hobj in model)
                    {
                        var obj = new BedMaster();
                        obj = context.BedMasters.Where(m => m.HISBedCode == hobj.BedCode).FirstOrDefault();
                        var roomBillingType = new RoomBillingCategory();
                        roomBillingType = roombillingTypeCategories.Where(m => m.Name.ToLower() == hobj.Bedcategory.ToLower()).FirstOrDefault();

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.BedCode))
                        {
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;

                            var floorNo = !string.IsNullOrWhiteSpace(hobj.WardName) ? hobj.WardName.Substring(0, 2) : hobj.WardName;
                            if (floorNo == "GF")
                            {
                                obj.FloorNumber = "GROUND FLOOR";
                            }
                            else if (floorNo == "1F")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (floorNo == "2F")
                            {
                                obj.FloorNumber = "2ND FLOOR";
                            }
                            else if (floorNo == "3F")
                            {
                                obj.FloorNumber = "3RD FLOOR";
                            }
                            else if (floorNo == "4F")
                            {
                                obj.FloorNumber = "4TH FLOOR";
                            }
                            obj.RoomNumber = hobj.WardCode;
                            obj.ModifiedBy = 1;
                            obj.ModifiedOn = DateTime.Now;
                            if (hobj.Bedcategory == "BASINET" || hobj.Bedcategory == "DAY CARE" || hobj.Bedcategory == "NICU"
                                || hobj.Bedcategory == "PICU" || hobj.WardName == "4F -IVF" || hobj.WardName == "4F -OT")
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new BedMaster();

                            if (codeCount == 0)
                            {
                                code = context.BedMasters.OrderByDescending(z => z.BedCode).FirstOrDefault()?.BedCode;
                                if (code is null)
                                {
                                    code = "BED-00001";
                                }
                            }
                            if (code != null && codeCount > 0)
                            {
                                var lcode = code.Substring(0, 3);
                                var rcode = code.Substring(4, 5);
                                code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                            }

                            obj.BedCode = code;
                            obj.SiteCode = requestData.SiteCode;
                            obj.HISBedCode = hobj.BedCode;
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;
                            obj.RoomNumber = hobj.WardCode;
                            obj.IsActive = true;
                            obj.CreatedBy = 1;
                            obj.CreatedOn = DateTime.Now;
                            var floorNo = !string.IsNullOrWhiteSpace(hobj.WardName) ? hobj.WardName.Substring(0, 2) : hobj.WardName;
                            if (floorNo == "GF")
                            {
                                obj.FloorNumber = "GROUND FLOOR";
                            }
                            else if (floorNo == "1F")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (floorNo == "2F")
                            {
                                obj.FloorNumber = "2ND FLOOR";
                            }
                            else if (floorNo == "3F")
                            {
                                obj.FloorNumber = "3RD FLOOR";
                            }
                            else if (floorNo == "4F")
                            {
                                obj.FloorNumber = "4TH FLOOR";
                            }
                            if (hobj.Bedcategory == "BASINET" || hobj.Bedcategory == "DAY CARE" || hobj.Bedcategory == "NICU"
                              || hobj.Bedcategory == "PICU" || hobj.WardName == "4F -IVF" || hobj.WardName == "4F -OT")
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.Add(obj);
                        }
                        codeCount++;
                    }
                    context.SaveChanges();
                }
            }
            LogTraceFactory.WriteLogWithCategory("Service HN SaveHISBedMasterData- end", LogTraceCategoryNames.Tracing);
            return "ture";
        }

        #endregion

        #region 40HY Hosptial
        public string SaveHISPatientProfileMaster_40HY(RequestData requestData, List<HISPatientProfileData> model)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var bedList = context.BedMasters.ToList();
                    foreach (HISPatientProfileData hobj in model)
                    {
                        if (!string.IsNullOrEmpty(hobj.PatientID) && !string.IsNullOrEmpty(hobj.PatientName) &&
                            !string.IsNullOrEmpty(hobj.DateOfBirth) && !string.IsNullOrEmpty(hobj.Bed))
                        {

                            var obj = new PatientProfile();
                            obj = context.PatientProfiles.Where(m => m.PatientID == hobj.PatientID).FirstOrDefault();
                            var bedOjb = bedList.Where(m => m.HISBedCode == hobj.HISBedCode).FirstOrDefault();

                            if (obj != null && !string.IsNullOrWhiteSpace(obj.PatientID))
                            {
                                if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                {
                                    obj.BedCode = bedOjb.BedCode;
                                    obj.FloorNumber = bedOjb.FloorNumber;
                                }
                                // obj.SiteCode = requestData.SiteCode;
                                obj.PatientName = hobj.PatientName;
                                obj.HISBedCode = hobj.BedCode;
                                obj.BedNumber = hobj.Bed;
                                obj.ModifiedBy = "1";
                                obj.ModifiedDate = DateTime.Now;
                                obj.DateofBirth = hobj.DateOfBirth;
                                obj.VisitStatus = hobj.VisitStatus;
                                obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj.IsActive = true;
                                }
                                else
                                {
                                    obj.IsActive = false;
                                    if (obj.VisitStatus != hobj.VisitStatus)
                                    {
                                        obj.FinancialDischargeDate = System.DateTime.Now;
                                    }
                                }
                                if (hobj.NationalityCode == "IND")
                                {
                                    obj.MenuType = "PBP-00001";
                                }
                                //else if (hobj.NationalityCode == "INTL")
                                //{
                                //    obj.MenuType = "PBP-00003";
                                //}
                                else
                                {
                                    obj.MenuType = "PBP-00003";
                                }
                                context.PatientProfiles.AddOrUpdate(obj);
                            }
                            else
                            {
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj = new PatientProfile();
                                    if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                    {
                                        obj.BedCode = bedOjb.BedCode;
                                        obj.FloorNumber = bedOjb.FloorNumber;
                                    }
                                    obj.SiteCode = requestData.SiteCode;
                                    obj.PatientID = hobj.PatientID;
                                    obj.PatientName = hobj.PatientName;
                                    obj.HISBedCode = hobj.BedCode;
                                    obj.BedNumber = hobj.Bed;
                                    obj.CreatedBy = "1";
                                    obj.CreatedDate = DateTime.Now;
                                    obj.DateofBirth = hobj.DateOfBirth;
                                    obj.VisitStatus = hobj.VisitStatus;
                                    obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                    if (hobj.VisitStatus != "Financial Discharge")
                                    {
                                        obj.IsActive = true;
                                    }
                                    else
                                    {
                                        obj.IsActive = false;
                                        if (obj.VisitStatus != hobj.VisitStatus)
                                        {
                                            obj.FinancialDischargeDate = System.DateTime.Now;
                                        }
                                    }
                                    if (hobj.NationalityCode == "IND")
                                    {
                                        obj.MenuType = "PBP-00001";
                                    }
                                    //else if (hobj.NationalityCode == "INTL")
                                    //{
                                    //    obj.MenuType = "PBP-00003";
                                    //}
                                    else
                                    {
                                        obj.MenuType = "PBP-00002";
                                    }
                                    obj.DietCategory = "DTC-00003";
                                    obj.DietType = "[{\"DietTypeCode\":\"DIT-00004\",\"DietTypeName\":\"Cardiac\"}]";
                                    obj.Texture = "TXT-00001";
                                    obj.AllergicTo = "[]";
                                    obj.OtherAllergens = "[]";

                                    obj.IsSpecificFreq = false;
                                    obj.IsHomeFood = false;
                                    context.PatientProfiles.AddOrUpdate(obj);
                                }
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }

            return "ture";
        }

        public string SaveHISUserData_40HY(RequestData requestData, List<HISUserData> model)
        {
            LogTraceFactory.WriteLogWithCategory("Service SaveHISUserData_40HY- Start " + requestData.SessionSectorName + model.Count() + "", LogTraceCategoryNames.Tracing);
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (HISUserData hobj in model)
                    {
                        var obj = new UserMaster();
                        obj = context.UserMasters.Where(m => m.EmpCode == hobj.EmpCode).FirstOrDefault();

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.EmpCode))
                        {
                            //obj.SiteCode = requestData.SiteCode;
                            // obj.EmpCode = hobj.EmpCode;
                            //obj.UserName = hobj.EmpName;
                            //obj.Mobile = hobj.MobileNo;
                            //if (obj.IsActive)
                            //{
                            //    continue;
                            //}
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.ModifiedBy = "1";
                            obj.ModifiedDate = DateTime.Now;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new UserMaster();
                            obj.SiteCode = requestData.SiteCode;
                            obj.EmpCode = hobj.EmpCode;
                            obj.UserName = hobj.EmpName;
                            obj.Mobile = hobj.MobileNo;
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.CreatedBy = "1";
                            obj.CreatedDate = DateTime.Now;
                            obj.SiteCode = requestData.SiteCode;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                    }
                    context.SaveChanges();
                }
            }
            LogTraceFactory.WriteLogWithCategory("Service SaveHISUserData_40HY- End " + requestData.SessionSectorName + "", LogTraceCategoryNames.Tracing);
            return "ture";
        }

        public string SaveHISBedMasterData_40HY(RequestData requestData, List<HISBedMasterData> model)
        {
            using (var transaction = new EFTransaction())
            {
                LogTraceFactory.WriteLogWithCategory("Service 40HY SaveHISBedMasterData- Start " + requestData.SessionSectorName + "", LogTraceCategoryNames.Tracing);
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    LogTraceFactory.WriteLogWithCategory("Service 40HY SaveHISBedMasterData- Start " + context.Database.Connection.ConnectionString.ToString()
                        + "", LogTraceCategoryNames.Tracing);

                    var codeCount = 0;
                    string code = "BED-00001";
                    var roombillingTypeCategories = context.RoomBillingCategories.ToList();
                    foreach (HISBedMasterData hobj in model)
                    {
                        var obj = new BedMaster();
                        obj = context.BedMasters.Where(m => m.HISBedCode == hobj.BedCode).FirstOrDefault();
                        var roomBillingType = new RoomBillingCategory();
                        roomBillingType = roombillingTypeCategories.Where(m => m.Name.ToLower() == hobj.Bedcategory.ToLower()).FirstOrDefault();

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.BedCode))
                        {
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;

                            var floorNo = !string.IsNullOrWhiteSpace(hobj.WardName) ? hobj.WardName.Substring(0, 2) : hobj.WardName;
                            if (floorNo == "GF")
                            {
                                obj.FloorNumber = "GROUND FLOOR";
                            }
                            else if (floorNo == "1F")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (floorNo == "2F")
                            {
                                obj.FloorNumber = "2ND FLOOR";
                            }
                            else if (floorNo == "3F")
                            {
                                obj.FloorNumber = "3RD FLOOR";
                            }
                            else if (floorNo == "4F")
                            {
                                obj.FloorNumber = "4TH FLOOR";
                            }
                            obj.RoomNumber = hobj.WardCode;
                            obj.ModifiedBy = 1;
                            obj.ModifiedOn = DateTime.Now;
                            if (hobj.Bedcategory == "BASINET" || hobj.Bedcategory == "DAY CARE" || hobj.Bedcategory == "NICU"
                                || hobj.Bedcategory == "PICU" || hobj.WardName == "4F -IVF" || hobj.WardName == "4F -OT")
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new BedMaster();

                            if (codeCount == 0)
                            {
                                code = context.BedMasters.OrderByDescending(z => z.BedCode).FirstOrDefault()?.BedCode;
                                if (code is null)
                                {
                                    code = "BED-00001";
                                }
                            }
                            if (code != null && codeCount > 0)
                            {
                                var lcode = code.Substring(0, 3);
                                var rcode = code.Substring(4, 5);
                                code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                            }

                            obj.BedCode = code;
                            obj.SiteCode = requestData.SiteCode;
                            obj.HISBedCode = hobj.BedCode;
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;
                            obj.RoomNumber = hobj.WardCode;
                            obj.IsActive = true;
                            obj.CreatedBy = 1;
                            obj.CreatedOn = DateTime.Now;
                            var floorNo = !string.IsNullOrWhiteSpace(hobj.WardName) ? hobj.WardName.Substring(0, 2) : hobj.WardName;
                            if (floorNo == "GF")
                            {
                                obj.FloorNumber = "GROUND FLOOR";
                            }
                            else if (floorNo == "1F")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (floorNo == "2F")
                            {
                                obj.FloorNumber = "2ND FLOOR";
                            }
                            else if (floorNo == "3F")
                            {
                                obj.FloorNumber = "3RD FLOOR";
                            }
                            else if (floorNo == "4F")
                            {
                                obj.FloorNumber = "4TH FLOOR";
                            }
                            if (hobj.Bedcategory == "BASINET" || hobj.Bedcategory == "DAY CARE" || hobj.Bedcategory == "NICU"
                              || hobj.Bedcategory == "PICU" || hobj.WardName == "4F -IVF" || hobj.WardName == "4F -OT")
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.Add(obj);
                        }
                        codeCount++;
                    }
                    context.SaveChanges();
                }
            }
            LogTraceFactory.WriteLogWithCategory("Service 40HY SaveHISBedMasterData- end", LogTraceCategoryNames.Tracing);
            return "ture";
        }

        #endregion

        #region 40VI Hosptial
        public string SaveHISPatientProfileMaster_40VI(RequestData requestData, List<HISPatientProfileData> model)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var bedList = context.BedMasters.ToList();
                    foreach (HISPatientProfileData hobj in model)
                    {
                        if (!string.IsNullOrEmpty(hobj.PatientID) && !string.IsNullOrEmpty(hobj.PatientName) &&
                            !string.IsNullOrEmpty(hobj.DateOfBirth) && !string.IsNullOrEmpty(hobj.Bed))
                        {

                            var obj = new PatientProfile();
                            obj = context.PatientProfiles.Where(m => m.PatientID == hobj.PatientID).FirstOrDefault();
                            var bedOjb = bedList.Where(m => m.HISBedCode == hobj.HISBedCode).FirstOrDefault();

                            if (obj != null && !string.IsNullOrWhiteSpace(obj.PatientID))
                            {
                                if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                {
                                    obj.BedCode = bedOjb.BedCode;
                                    obj.FloorNumber = bedOjb.FloorNumber;
                                }
                                // obj.SiteCode = requestData.SiteCode;
                                obj.PatientName = hobj.PatientName;
                                obj.HISBedCode = hobj.BedCode;
                                obj.BedNumber = hobj.Bed;
                                obj.ModifiedBy = "1";
                                obj.ModifiedDate = DateTime.Now;
                                obj.DateofBirth = hobj.DateOfBirth;
                                obj.VisitStatus = hobj.VisitStatus;
                                obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj.IsActive = true;
                                }
                                else
                                {
                                    obj.IsActive = false;
                                    if (obj.VisitStatus != hobj.VisitStatus)
                                    {
                                        obj.FinancialDischargeDate = System.DateTime.Now;
                                    }
                                }
                                if (hobj.NationalityCode == "IND")
                                {
                                    obj.MenuType = "PBP-00001";
                                }
                                //else if (hobj.NationalityCode == "INTL")
                                //{
                                //    obj.MenuType = "PBP-00003";
                                //}
                                else
                                {
                                    obj.MenuType = "PBP-00003";
                                }
                                context.PatientProfiles.AddOrUpdate(obj);
                            }
                            else
                            {
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj = new PatientProfile();
                                    if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                    {
                                        obj.BedCode = bedOjb.BedCode;
                                        obj.FloorNumber = bedOjb.FloorNumber;
                                    }
                                    obj.SiteCode = requestData.SiteCode;
                                    obj.PatientID = hobj.PatientID;
                                    obj.PatientName = hobj.PatientName;
                                    obj.HISBedCode = hobj.BedCode;
                                    obj.BedNumber = hobj.Bed;
                                    obj.CreatedBy = "1";
                                    obj.CreatedDate = DateTime.Now;
                                    obj.DateofBirth = hobj.DateOfBirth;
                                    obj.VisitStatus = hobj.VisitStatus;
                                    obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                    if (hobj.VisitStatus != "Financial Discharge")
                                    {
                                        obj.IsActive = true;
                                    }
                                    else
                                    {
                                        obj.IsActive = false;
                                        if (obj.VisitStatus != hobj.VisitStatus)
                                        {
                                            obj.FinancialDischargeDate = System.DateTime.Now;
                                        }
                                    }
                                    if (hobj.NationalityCode == "IND")
                                    {
                                        obj.MenuType = "PBP-00001";
                                    }
                                    //else if (hobj.NationalityCode == "INTL")
                                    //{
                                    //    obj.MenuType = "PBP-00003";
                                    //}
                                    else
                                    {
                                        obj.MenuType = "PBP-00002";
                                    }
                                    obj.DietCategory = "DTC-00003";
                                    obj.DietType = "[{\"DietTypeCode\":\"DIT-00001\",\"DietTypeName\":\"Normal\"}]";
                                    obj.Texture = "TXT-00001";
                                    obj.AllergicTo = "[]";
                                    obj.OtherAllergens = "[]";

                                    obj.IsSpecificFreq = false;
                                    obj.IsHomeFood = false;
                                    context.PatientProfiles.AddOrUpdate(obj);
                                }
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }

            return "ture";
        }

        public string SaveHISUserData_40VI(RequestData requestData, List<HISUserData> model)
        {
            LogTraceFactory.WriteLogWithCategory("Service SaveHISUserData_40VI- Start " + requestData.SessionSectorName + model.Count() + "", LogTraceCategoryNames.Tracing);
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (HISUserData hobj in model)
                    {
                        var obj = new UserMaster();
                        obj = context.UserMasters.Where(m => m.EmpCode == hobj.EmpCode).FirstOrDefault();

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.EmpCode))
                        {
                            //obj.SiteCode = requestData.SiteCode;
                            // obj.EmpCode = hobj.EmpCode;
                            //obj.UserName = hobj.EmpName;
                            //obj.Mobile = hobj.MobileNo;
                            //if (obj.IsActive)
                            //{
                            //    continue;
                            //}
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.ModifiedBy = "1";
                            obj.ModifiedDate = DateTime.Now;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new UserMaster();
                            obj.SiteCode = requestData.SiteCode;
                            obj.EmpCode = hobj.EmpCode;
                            obj.UserName = hobj.EmpName;
                            obj.Mobile = hobj.MobileNo;
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.CreatedBy = "1";
                            obj.CreatedDate = DateTime.Now;
                            obj.SiteCode = requestData.SiteCode;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                    }
                    context.SaveChanges();
                }
            }
            LogTraceFactory.WriteLogWithCategory("Service SaveHISUserData_40VI- End " + requestData.SessionSectorName + "", LogTraceCategoryNames.Tracing);
            return "ture";
        }

        public string SaveHISBedMasterData_40VI(RequestData requestData, List<HISBedMasterData> model)
        {
            using (var transaction = new EFTransaction())
            {
                LogTraceFactory.WriteLogWithCategory("Service 40VI SaveHISBedMasterData- Start " + requestData.SessionSectorName + "", LogTraceCategoryNames.Tracing);
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    LogTraceFactory.WriteLogWithCategory("Service 40VI SaveHISBedMasterData- Start " + context.Database.Connection.ConnectionString.ToString()
                        + "", LogTraceCategoryNames.Tracing);

                    var codeCount = 0;
                    string code = "BED-00001";
                    var roombillingTypeCategories = context.RoomBillingCategories.ToList();
                    foreach (HISBedMasterData hobj in model)
                    {
                        var obj = new BedMaster();
                        obj = context.BedMasters.Where(m => m.HISBedCode == hobj.BedCode).FirstOrDefault();
                        var roomBillingType = new RoomBillingCategory();
                        if (hobj.Bedcategory == "SHARED WARD")
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name == "Emergency").FirstOrDefault();
                        }
                        else if (hobj.Bedcategory == "FOUR SHARING")
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name == "HDU").FirstOrDefault();
                        }
                        else if (hobj.WardCode == "NB LABOUR WARD")
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name == "Labour Ward").FirstOrDefault();
                        }
                        else if (hobj.Bedcategory == "MULTI SHARING WARD")
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name == "Multi-Sharing Ward").FirstOrDefault();
                        }
                        else
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name.ToLower() == hobj.Bedcategory.ToLower()).FirstOrDefault();
                        }

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.BedCode))
                        {
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;

                            var floorNo = !string.IsNullOrWhiteSpace(hobj.WardName) ? hobj.WardName.Substring(0, 5) : hobj.WardName;
                            if (floorNo == "N 0 G")
                            {
                                obj.FloorNumber = "GROUND FLOOR";
                            }
                            else if (floorNo == "N 1F-")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (floorNo == "N 2F-")
                            {
                                obj.FloorNumber = "2ND FLOOR";
                            }
                            else if (floorNo == "N 3F-")
                            {
                                obj.FloorNumber = "3RD FLOOR";
                            }
                            obj.RoomNumber = hobj.WardCode;
                            obj.ModifiedBy = 1;
                            obj.ModifiedOn = DateTime.Now;
                            if (hobj.Bedcategory == "BASINET"  || hobj.Bedcategory == "NICU" )
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new BedMaster();

                            if (codeCount == 0)
                            {
                                code = context.BedMasters.OrderByDescending(z => z.BedCode).FirstOrDefault()?.BedCode;
                                if (code is null)
                                {
                                    code = "BED-00001";
                                }
                            }
                            if (code != null && codeCount > 0)
                            {
                                var lcode = code.Substring(0, 3);
                                var rcode = code.Substring(4, 5);
                                code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                            }

                            obj.BedCode = code;
                            obj.SiteCode = requestData.SiteCode;
                            obj.HISBedCode = hobj.BedCode;
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;
                            obj.RoomNumber = hobj.WardCode;
                            obj.IsActive = true;
                            obj.CreatedBy = 1;
                            obj.CreatedOn = DateTime.Now;
                            var floorNo = !string.IsNullOrWhiteSpace(hobj.WardName) ? hobj.WardName.Substring(0, 5) : hobj.WardName;
                            if (floorNo == "N 0 G")
                            {
                                obj.FloorNumber = "GROUND FLOOR";
                            }
                            else if (floorNo == "N 1F-")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (floorNo == "N 2F-")
                            {
                                obj.FloorNumber = "2ND FLOOR";
                            }
                            else if (floorNo == "N 3F-")
                            {
                                obj.FloorNumber = "3RD FLOOR";
                            }

                            if (hobj.Bedcategory == "BASINET" || hobj.Bedcategory == "NICU")
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.Add(obj);
                        }
                        codeCount++;
                    }
                    context.SaveChanges();
                }
            }
            LogTraceFactory.WriteLogWithCategory("Service 40VI SaveHISBedMasterData- end", LogTraceCategoryNames.Tracing);
            return "ture";
        }

        #endregion

        #region 40DN Hosptial
        public string SaveHISPatientProfileMaster_40DN(RequestData requestData, List<HISPatientProfileData> model)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var bedList = context.BedMasters.ToList();
                    foreach (HISPatientProfileData hobj in model)
                    {
                        if (!string.IsNullOrEmpty(hobj.PatientID) && !string.IsNullOrEmpty(hobj.PatientName) &&
                            !string.IsNullOrEmpty(hobj.DateOfBirth) && !string.IsNullOrEmpty(hobj.Bed))
                        {

                            var obj = new PatientProfile();
                            obj = context.PatientProfiles.Where(m => m.PatientID == hobj.PatientID).FirstOrDefault();
                            var bedOjb = bedList.Where(m => m.HISBedCode == hobj.HISBedCode).FirstOrDefault();

                            if (obj != null && !string.IsNullOrWhiteSpace(obj.PatientID))
                            {
                                if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                {
                                    obj.BedCode = bedOjb.BedCode;
                                    obj.FloorNumber = bedOjb.FloorNumber;
                                }
                                // obj.SiteCode = requestData.SiteCode;
                                obj.PatientName = hobj.PatientName;
                                obj.HISBedCode = hobj.BedCode;
                                obj.BedNumber = hobj.Bed;
                                obj.ModifiedBy = "1";
                                obj.ModifiedDate = DateTime.Now;
                                obj.DateofBirth = hobj.DateOfBirth;
                                obj.VisitStatus = hobj.VisitStatus;
                                obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj.IsActive = true;
                                }
                                else
                                {
                                    obj.IsActive = false;
                                    if (obj.VisitStatus != hobj.VisitStatus)
                                    {
                                        obj.FinancialDischargeDate = System.DateTime.Now;
                                    }
                                }
                                if (hobj.NationalityCode == "IND")
                                {
                                    obj.MenuType = "PBP-00001";
                                }
                                //else if (hobj.NationalityCode == "INTL")
                                //{
                                //    obj.MenuType = "PBP-00003";
                                //}
                                else
                                {
                                    obj.MenuType = "PBP-00003";
                                }
                                context.PatientProfiles.AddOrUpdate(obj);
                            }
                            else
                            {
                                if (hobj.VisitStatus != "Financial Discharge")
                                {
                                    obj = new PatientProfile();
                                    if (bedList != null && bedList.Count() > 0 && bedOjb != null && bedOjb.BedCode != null)
                                    {
                                        obj.BedCode = bedOjb.BedCode;
                                        obj.FloorNumber = bedOjb.FloorNumber;
                                    }
                                    obj.SiteCode = requestData.SiteCode;
                                    obj.PatientID = hobj.PatientID;
                                    obj.PatientName = hobj.PatientName;
                                    obj.HISBedCode = hobj.BedCode;
                                    obj.BedNumber = hobj.Bed;
                                    obj.CreatedBy = "1";
                                    obj.CreatedDate = DateTime.Now;
                                    obj.DateofBirth = hobj.DateOfBirth;
                                    obj.VisitStatus = hobj.VisitStatus;
                                    obj.MedicalDischargeDate = hobj.MedicalDischargeDate;
                                    if (hobj.VisitStatus != "Financial Discharge")
                                    {
                                        obj.IsActive = true;
                                    }
                                    else
                                    {
                                        obj.IsActive = false;
                                        if (obj.VisitStatus != hobj.VisitStatus)
                                        {
                                            obj.FinancialDischargeDate = System.DateTime.Now;
                                        }
                                    }
                                    if (hobj.NationalityCode == "IND")
                                    {
                                        obj.MenuType = "PBP-00001";
                                    }
                                    //else if (hobj.NationalityCode == "INTL")
                                    //{
                                    //    obj.MenuType = "PBP-00003";
                                    //}
                                    else
                                    {
                                        obj.MenuType = "PBP-00002";
                                    }
                                    obj.DietCategory = "DTC-00003";
                                    obj.DietType = "[{\"DietTypeCode\":\"DIT-00001\",\"DietTypeName\":\"Normal\"}]";
                                    obj.Texture = "TXT-00001";
                                    obj.AllergicTo = "[]";
                                    obj.OtherAllergens = "[]";

                                    obj.IsSpecificFreq = false;
                                    obj.IsHomeFood = false;
                                    context.PatientProfiles.AddOrUpdate(obj);
                                }
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }

            return "ture";
        }

        public string SaveHISUserData_40DN(RequestData requestData, List<HISUserData> model)
        {
            LogTraceFactory.WriteLogWithCategory("Service SaveHISUserData_40DN- Start " + requestData.SessionSectorName + model.Count() + "", LogTraceCategoryNames.Tracing);
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (HISUserData hobj in model)
                    {
                        var obj = new UserMaster();
                        obj = context.UserMasters.Where(m => m.EmpCode == hobj.EmpCode).FirstOrDefault();

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.EmpCode))
                        {
                            //obj.SiteCode = requestData.SiteCode;
                            // obj.EmpCode = hobj.EmpCode;
                            //obj.UserName = hobj.EmpName;
                            //obj.Mobile = hobj.MobileNo;
                            //if (obj.IsActive)
                            //{
                            //    continue;
                            //}
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.ModifiedBy = "1";
                            obj.ModifiedDate = DateTime.Now;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new UserMaster();
                            obj.SiteCode = requestData.SiteCode;
                            obj.EmpCode = hobj.EmpCode;
                            obj.UserName = hobj.EmpName;
                            obj.Mobile = hobj.MobileNo;
                            obj.IsActive = string.IsNullOrWhiteSpace(hobj.ActiveTo) ? true : false;
                            obj.CreatedBy = "1";
                            obj.CreatedDate = DateTime.Now;
                            obj.SiteCode = requestData.SiteCode;
                            context.UserMasters.AddOrUpdate(obj);
                        }
                    }
                    context.SaveChanges();
                }
            }
            LogTraceFactory.WriteLogWithCategory("Service SaveHISUserData_40DN- End " + requestData.SessionSectorName + "", LogTraceCategoryNames.Tracing);
            return "ture";
        }

        public string SaveHISBedMasterData_40DN(RequestData requestData, List<HISBedMasterData> model)
        {
            using (var transaction = new EFTransaction())
            {
                LogTraceFactory.WriteLogWithCategory("Service 40DN SaveHISBedMasterData- Start " + requestData.SessionSectorName + "", LogTraceCategoryNames.Tracing);
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    LogTraceFactory.WriteLogWithCategory("Service 40DN SaveHISBedMasterData- Start " + context.Database.Connection.ConnectionString.ToString()
                        + "", LogTraceCategoryNames.Tracing);

                    var codeCount = 0;
                    string code = "BED-00001";
                    var roombillingTypeCategories = context.RoomBillingCategories.ToList();
                    foreach (HISBedMasterData hobj in model)
                    {
                        var roomtype = "RTP-00001";
                        var obj = new BedMaster();
                        obj = context.BedMasters.Where(m => m.HISBedCode == hobj.BedCode).FirstOrDefault();
                        var roomBillingType = new RoomBillingCategory();
                        if (hobj.Bedcategory == "FOUR SHARING" && hobj.WardCode == "2ND FLOOR WARD")
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name == "Four Sharing").FirstOrDefault();
                        }
                        else if (hobj.WardCode == "BIRTHING ROOM")
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name == "Birthing Room").FirstOrDefault();
                        }
                        else if (hobj.WardCode == "EMR")
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name == "Emergency").FirstOrDefault();
                            roomtype = "RTP-00002";
                        }
                        else if (hobj.WardCode == "IVF")
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name == "IVF").FirstOrDefault();
                            roomtype = "RTP-00002";
                        }
                        else if (hobj.WardCode == "MICU")
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name == "MICU").FirstOrDefault();
                        }
                        else if (hobj.WardCode == "PRE/POST OP")
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name == "PRE/POST OP").FirstOrDefault();
                        }
                        else
                        {
                            roomBillingType = roombillingTypeCategories.Where(m => m.Name.ToLower() == hobj.Bedcategory.ToLower()).FirstOrDefault();
                        }

                        if (obj != null && !string.IsNullOrWhiteSpace(obj.BedCode))
                        {
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;

                            var floorNo = !string.IsNullOrWhiteSpace(hobj.WardName) ? hobj.WardName.Substring(0, 5) : hobj.WardName;
                            if (floorNo == "1ST FLOOR")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (floorNo == "2ND FLOOR" || hobj.WardName == "NICU" || hobj.WardName == "PICU")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (hobj.WardName == "PRIVATE" || hobj.WardName == "TWIN SHARING" || hobj.WardName == "FOUR SHARING")
                            {
                                obj.FloorNumber = "3ND FLOOR";
                            }
                            else if (hobj.WardName == "PRE/POST OP" || hobj.WardName == "BIRTHING ROOM" || hobj.WardName == "4TH FLOOR PRIVATE" || hobj.WardName == "MICU")
                            {
                                obj.FloorNumber = "4TH FLOOR";
                            }
                            else if (hobj.WardName == "EMERGENCY")
                            {
                                obj.FloorNumber = "EMERGENCY";
                            }
                            else if (hobj.WardCode == "IVF")
                            {
                                obj.FloorNumber = "IVF";
                            }
                            obj.RoomNumber = hobj.WardCode;
                            obj.ModifiedBy = 1;
                            obj.ModifiedOn = DateTime.Now;
                            if (hobj.Bedcategory == "BASINET" || hobj.Bedcategory == "NICU" || roomtype == "RTP-00002")
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.AddOrUpdate(obj);
                        }
                        else
                        {
                            obj = new BedMaster();

                            if (codeCount == 0)
                            {
                                code = context.BedMasters.OrderByDescending(z => z.BedCode).FirstOrDefault()?.BedCode;
                                if (code is null)
                                {
                                    code = "BED-00001";
                                }
                            }
                            if (code != null && codeCount > 0)
                            {
                                var lcode = code.Substring(0, 3);
                                var rcode = code.Substring(4, 5);
                                code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0');
                            }

                            obj.BedCode = code;
                            obj.SiteCode = requestData.SiteCode;
                            obj.HISBedCode = hobj.BedCode;
                            obj.BedNumber = hobj.BedName;
                            obj.RoomBillingCategoryCode = roomBillingType == null ? "" : roomBillingType.RoomBillingCategoryCode;
                            obj.RoomNumber = hobj.WardCode;
                            obj.IsActive = true;
                            obj.CreatedBy = 1;
                            obj.CreatedOn = DateTime.Now;
                            var floorNo = (!string.IsNullOrWhiteSpace(hobj.WardName) && hobj.WardName.Length > 9) ? hobj.WardName.Substring(0, 9) : hobj.WardName;
                            if (floorNo == "1ST FLOOR")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (floorNo == "2ND FLOOR" || hobj.WardName == "NICU" || hobj.WardName == "PICU")
                            {
                                obj.FloorNumber = "1ST FLOOR";
                            }
                            else if (hobj.WardName == "PRIVATE" || hobj.WardName == "TWIN SHARING" || hobj.WardName == "FOUR SHARING")
                            {
                                obj.FloorNumber = "3ND FLOOR";
                            }
                            else if (hobj.WardName == "PRE/POST OP" || hobj.WardName == "BIRTHING ROOM" || hobj.WardName == "4TH FLOOR PRIVATE" || hobj.WardName == "MICU")
                            {
                                obj.FloorNumber = "4TH FLOOR";
                            }
                            else if (hobj.WardName == "EMERGENCY")
                            {
                                obj.FloorNumber = "EMERGENCY";
                            }
                            else if (hobj.WardCode == "IVF")
                            {
                                obj.FloorNumber = "IVF";
                            }

                            if (hobj.Bedcategory == "BASINET" || hobj.Bedcategory == "NICU" || roomtype == "RTP-00002")
                            {
                                obj.RoomTypeCode = "RTP-00002";
                            }
                            else
                            {
                                obj.RoomTypeCode = "RTP-00001";
                            }
                            context.BedMasters.Add(obj);
                        }
                        codeCount++;
                    }
                    context.SaveChanges();
                }
            }
            LogTraceFactory.WriteLogWithCategory("Service 40DN SaveHISBedMasterData- end", LogTraceCategoryNames.Tracing);
            return "ture";
        }

        #endregion


    }
}
