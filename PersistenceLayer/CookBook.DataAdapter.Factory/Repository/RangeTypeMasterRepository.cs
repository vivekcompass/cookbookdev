﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{    
    [Export(typeof(IRangeTypeMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RangeTypeMasterRepository : EFSynchronousRepository<int, RangeType, CookBookCoreEntities>,
        IRangeTypeMasterRepository
    {
        protected override DbSet<RangeType> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.RangeTypes;
        }

        protected override RangeType FindImpl(int key, DbSet<RangeType> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override RangeType FindImplWithExpand(int key, DbSet<RangeType> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public List<procGetRangeTypeMaster_Result> GetRangeTypeMasterDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetRangeTypeMaster(null).ToList();
                }
            }
        }

        public string SaveRangeTypeMasterDataList(RangeType model, RequestData requestData)
        {          
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.procSaveRangeTypeMaster(model.ID, model.Code, model.Name, model.IsActive, model.StdTempFrom, model.StdTempTo, requestData.SessionUserId,requestData.SessionUserId, DateTime.Now, DateTime.Now);
                    context.SaveChanges();

                    return "true";
                }
            }

        }
    }
}
