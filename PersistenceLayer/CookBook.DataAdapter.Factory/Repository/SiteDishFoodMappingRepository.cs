﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(ISiteDishFoodPanMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteDishFoodPanMappingRepository : EFSynchronousRepository<int, SiteDishFoodPanMapping, CookBookCoreEntities>,
        ISiteDishFoodPanMappingRepository
    {
     
        public IList<procGetSiteDishFoodMappingData_Result> GetSiteDishFoodMappingDataList(string siteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetSiteDishFoodMappingData(siteCode).ToList();
                }
            }
        }
        
        public IList<procGetSiteDishCategoryFoodMappingData_Result> GetSiteDishCategoryFoodMappingDataList(string siteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context =GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetSiteDishCategoryFoodMappingData(siteCode).ToList();
                }
            }
        }

        protected override SiteDishFoodPanMapping FindImpl(int key, DbSet<SiteDishFoodPanMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SiteDishFoodPanMapping FindImplWithExpand(int key, DbSet<SiteDishFoodPanMapping> dbSet, string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        protected override DbSet<SiteDishFoodPanMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.SiteDishFoodPanMappings;
        }
    }

}
