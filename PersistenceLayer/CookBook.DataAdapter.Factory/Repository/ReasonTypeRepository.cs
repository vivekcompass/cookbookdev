﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IReasonTypeRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ReasonTypeRepository : EFSynchronousRepository<int, ReasonType, CookBookCoreEntities>,
        IReasonTypeRepository
    {
        protected override DbSet<ReasonType> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.ReasonTypes;
        }

        protected override ReasonType FindImpl(int key, DbSet<ReasonType> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ReasonType FindImplWithExpand(int key, DbSet<ReasonType> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
