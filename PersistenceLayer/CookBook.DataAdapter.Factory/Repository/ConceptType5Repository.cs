﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IConceptType5Repository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ConceptType5Repository : EFSynchronousRepository<int, ConceptType5, CookBookCommonEntities>,
        IConceptType5Repository
    {
        protected override DbSet<ConceptType5> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.ConceptType5;
        }

        protected override ConceptType5 FindImpl(int key, DbSet<ConceptType5> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ConceptType5 FindImplWithExpand(int key, DbSet<ConceptType5> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
