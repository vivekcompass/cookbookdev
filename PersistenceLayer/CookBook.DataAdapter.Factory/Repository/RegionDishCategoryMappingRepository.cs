﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IRegionDishCategoryMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RegionDishCategoryMappingRepository : EFSynchronousRepository<int, RegionDishCategoryMapping, CookBookCoreEntities>,
        IRegionDishCategoryMappingRepository
    {
        protected override DbSet<RegionDishCategoryMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.RegionDishCategoryMappings;
        }

        protected override RegionDishCategoryMapping FindImpl(int key, DbSet<RegionDishCategoryMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override RegionDishCategoryMapping FindImplWithExpand(int key, DbSet<RegionDishCategoryMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<RegionDishCategoryMapping> GetRegionDishCategoryMappingDataList(string RegionCode,string ItemCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                int regionCode = int.Parse(RegionCode);
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var ret = context.procUpdateCostForAllItemAtRegions(ItemCode, regionCode, requestData.SessionSectorNumber);
                    context.SaveChanges();

                    var list = from x in context.RegionDishCategoryMappings
                                   //where x.Region_ID == regionCode && x.ItemCode == ItemCode && x.IsActive.Value
                               where x.Region_ID == regionCode && x.ItemCode == ItemCode 
                               select x;
                             
                    return list.ToList();
                }
            }
        }
     
        public string SaveRegionDishCategoryMappingList(IList<RegionDishCategoryMapping> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                int RegionID = model[0].Region_ID;
                string ItemCode = model[0].ItemCode;
                var subSectorCode = model[0].SubSectorCode;
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    if (requestData.SessionSectorNumber == "10" && context.SubSectorMasters.Count() > 0)
                    {
                        var list = from x in context.RegionDishCategoryMappings
                                   where x.Region_ID == RegionID && x.ItemCode == ItemCode && x.SubSectorCode == subSectorCode && x.IsActive.Value
                                   select x;

                        IList<RegionDishCategoryMapping> llist  = list.ToList();
                        bool flag = false;
                        foreach (RegionDishCategoryMapping m in llist)
                        {
                            flag = false;
                            foreach (RegionDishCategoryMapping mo in model)
                            {
                                if (mo.ID == m.ID)
                                {
                                    flag = true;
                                    break;
                                }
                            }
                            if (!flag)
                            {
                                m.IsActive = false;
                                m.ModifiedOn = DateTime.Now;
                                m.ModifiedBy = m.CreatedBy;
                                context.RegionDishCategoryMappings.AddOrUpdate(m);
                            }

                        }
                    }
                    else
                    {
                       var list = from x in context.RegionDishCategoryMappings
                               where x.Region_ID == RegionID && x.ItemCode == ItemCode && x.IsActive.Value
                               select x;
                            IList<RegionDishCategoryMapping> llist = list.ToList();
                            bool flag = false;
                            foreach (RegionDishCategoryMapping m in llist)
                            {
                                flag = false;
                                foreach (RegionDishCategoryMapping mo in model)
                                {
                                    if (mo.ID == m.ID)
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (!flag)
                                {
                                    m.IsActive = false;
                                    m.ModifiedOn = DateTime.Now;
                                    m.ModifiedBy = m.CreatedBy;
                                    context.RegionDishCategoryMappings.AddOrUpdate(m);
                                }

                            }
                    }
                    
                   

                    context.SaveChanges();
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (RegionDishCategoryMapping m in model)
                    {
                        var isItemExists = new RegionDishCategoryMapping();
                        m.IsActive = true;
                        if (m.ID > 0)
                        {
                            m.ModifiedOn = DateTime.Now;
                            m.ModifiedBy = m.CreatedBy;
                        }
                        else
                        {
                            m.CreatedOn = DateTime.Now;
                        }
                        if (requestData.SessionSectorNumber == "10" && context.SubSectorMasters.Count() > 0)
                        {
                            isItemExists = context.RegionDishCategoryMappings.Where(item => item.ItemCode == m.ItemCode && item.DishCategoryCode == m.DishCategoryCode
                                           && item.Region_ID == m.Region_ID && item.SubSectorCode == m.SubSectorCode).FirstOrDefault();
                        }
                        else
                        {
                            isItemExists = context.RegionDishCategoryMappings.Where(item => item.ItemCode == m.ItemCode && item.DishCategoryCode == m.DishCategoryCode
                                          && item.Region_ID == m.Region_ID).FirstOrDefault();
                        }
                           
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            m.ID = isItemExists.ID;
                        }
                        else
                        {
                            m.ID = 0;
                        }
                        context.RegionDishCategoryMappings.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<string> GetUniqueMappedItemCodesFromRegion(string RegionID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                int regionID = int.Parse(RegionID);
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                   
                    //var list = (from idm in context.RegionDishCategoryMappings
                    //            where idm.Region_ID == regionID && idm.IsActive.Value
                    //            select idm.ItemCode).Distinct().ToList();


                    var list = (from idm in context.RegionDishCategoryMappings
                                 join rim in context.RegionItemInheritanceMappings on new { idm.ItemCode, idm.Region_ID } equals new { rim.ItemCode,rim.Region_ID }
                                 where idm.Region_ID == regionID && idm.IsActive.Value && rim.Status == 3
                                 select idm.ItemCode).Distinct().ToList();

                    return list.ToList();
                }
            }
        }
    }
}
