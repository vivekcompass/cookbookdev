﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IUserRoleMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserRoleMasterRepository : EFSynchronousRepository<int, UserRoleMaster, CookBookCommonEntities>,
        IUserRoleMasterRepository
    {
        protected override DbSet<UserRoleMaster> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.UserRoleMasters;
        }

        protected override UserRoleMaster FindImpl(int key, DbSet<UserRoleMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.UserRoleId == key);
        }

        protected override UserRoleMaster FindImplWithExpand(int key, DbSet<UserRoleMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.UserRoleId == key);
        }
    }
}
