﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(ISiteItemDayPartMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteItemDayPartMappingRepository : EFSynchronousRepository<int, SiteItemDayPartMapping, CookBookCoreEntities>,
        ISiteItemDayPartMappingRepository
    {
        protected override DbSet<SiteItemDayPartMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.SiteItemDayPartMappings;
        }

        protected override SiteItemDayPartMapping FindImpl(int key, DbSet<SiteItemDayPartMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SiteItemDayPartMapping FindImplWithExpand(int key, DbSet<SiteItemDayPartMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<SiteItemDayPartMapping> GetSiteItemDayPartMappingDataList(string itemcode, string siteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                //int ItemID = int.Parse(itemID);
            
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = from x in context.SiteItemDayPartMappings
                               where x.ItemCode == itemcode && x.SiteCode == siteCode && x.IsActive.Value
                               select x;
                    return list.ToList();
                }
            }
        }

        public IList<SiteItemDayPartMapping> GetMasterSiteItemDayPartMappingDataList(string SiteCode, RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {

               
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = from x in context.SiteItemDayPartMappings
                               where x.SiteCode == SiteCode
                               select x;
                    return list.ToList();
                }
            }
        }


        public string SaveSiteItemDayPartMappingList(IList<SiteItemDayPartMapping> model,  RequestData requestData)
        {
            string ItemCode = model[0].ItemCode;
            string siteCode = model[0].SiteCode;
           
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                   
                    var list = from x in context.SiteItemDayPartMappings
                               where x.ItemCode == ItemCode && x.SiteCode ==siteCode && x.IsActive.Value
                               select x;
                    IList<SiteItemDayPartMapping> deactivateItemList = list.ToList();
                   // var deactivateItemList = llist.ToList();
                    if (deactivateItemList != null && deactivateItemList.Count() > 0)
                    {
                        foreach (SiteItemDayPartMapping m in deactivateItemList)
                        {
                            if (!String.IsNullOrWhiteSpace(m.ItemCode))
                            {

                                m.IsActive = false;
                                m.ModifiedBy = requestData.SessionUserId; 
                                m.ModifiedOn = DateTime.Now;
                                context.SiteItemDayPartMappings.AddOrUpdate(m);
                            }
                        }
                        context.SaveChanges();
                    }
                 
                   
                }
            }

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (SiteItemDayPartMapping m in model)
                    {

                        m.IsActive = true;
                        var isItemExists = context.SiteItemDayPartMappings.Where(item => item.ItemCode == m.ItemCode && item.DayPartCode==m.DayPartCode &&
                                                                            item.SiteCode == m.SiteCode).FirstOrDefault();
                     
                        
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            isItemExists.ModifiedBy = requestData.SessionUserId; 
                            isItemExists.ModifiedOn = DateTime.Now;
                            isItemExists.IsActive = true;
                            context.SiteItemDayPartMappings.AddOrUpdate(isItemExists);
                        }
                        else
                        {
                            m.CreatedOn = DateTime.Now;
                            m.CreatedBy = requestData.SessionUserId; 
                            m.ModifiedBy = null;
                            m.ModifiedOn = null;
                            
                            m.ID = 0;
                            context.SiteItemDayPartMappings.AddOrUpdate(m);
                        }



                        //code to save
                        //context.SiteItemDayPartMappings.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                  
                }
            }

            // Insert SiteDayPartMapping Data
            using (var transaction = new EFTransaction())
            {
               
               // First Deactivate all the day part mapping for a site
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    var sitelist = from x in context.SiteDayPartMappings
                                   where x.SiteCode == siteCode && x.IsActive.Value
                                   select x;

                    IList<SiteDayPartMapping> sitedeactivateItemList = sitelist.ToList();
                    if (sitedeactivateItemList != null && sitedeactivateItemList.Count() > 0)
                    {
                        foreach (SiteDayPartMapping m in sitedeactivateItemList)
                        {
                            if (!String.IsNullOrWhiteSpace(m.SiteCode))
                            {
                                m.IsActive = false;
                                m.ModifiedBy = requestData.SessionUserId;
                                m.ModifiedOn = DateTime.Now;
                                context.SiteDayPartMappings.AddOrUpdate(m);
                            }
                        }
                        context.SaveChanges();
                    }

                    // Insert and update site day part mapping data.

                    var list = from x in context.SiteItemDayPartMappings
                               where x.SiteCode == siteCode && x.IsActive.Value
                               select x;

                    IList<SiteItemDayPartMapping> siteItemDayParrtList = list.Distinct().ToList();
                    foreach (SiteItemDayPartMapping m in siteItemDayParrtList)
                    {
                        m.IsActive = true;
                        var isItemExists = context.SiteDayPartMappings.Where(item => item.SiteCode == m.SiteCode && item.DayPartCode == m.DayPartCode).FirstOrDefault();

                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            isItemExists.ModifiedBy = requestData.SessionUserId;
                            isItemExists.ModifiedOn = DateTime.Now;
                            isItemExists.IsActive = true;
                            context.SiteDayPartMappings.AddOrUpdate(isItemExists);
                        }
                        else
                        {
                            SiteDayPartMapping siteDayPartMapping = new SiteDayPartMapping();
                            siteDayPartMapping.CreatedOn = DateTime.Now;
                            siteDayPartMapping.CreatedOn = DateTime.Now;
                            siteDayPartMapping.CreatedBy = requestData.SessionUserId;
                            siteDayPartMapping.ModifiedBy = null;
                            siteDayPartMapping.ModifiedOn = null;
                            siteDayPartMapping.ID = 0;
                            siteDayPartMapping.SiteCode = m.SiteCode;
                            siteDayPartMapping.DayPartCode = m.DayPartCode;
                            siteDayPartMapping.IsActive = true;
                            siteDayPartMapping.ClosingStatus = 1;
                            context.SiteDayPartMappings.AddOrUpdate(siteDayPartMapping);
                        }
                    }
                    context.SaveChanges();

                }
            }
            return "true";

        }
    }
}
