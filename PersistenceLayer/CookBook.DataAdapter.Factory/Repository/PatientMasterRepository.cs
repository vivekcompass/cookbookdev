﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IPatientMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class PatientMasterRepository : EFSynchronousRepository<int, PatientMaster, CookBookCoreEntities>,
        IPatientMasterRepository
    {
        protected override DbSet<PatientMaster> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.PatientMasters;
        }

        protected override PatientMaster FindImpl(int key, DbSet<PatientMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override PatientMaster FindImplWithExpand(int key, DbSet<PatientMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public List<procGetPatientMaster_Result> GetPatientMaster(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetPatientMaster().ToList();
                }
            }
        }
        public List<procGetPatientMasterBATCHHEADER_Result> GetPatientMasterHISTORY(RequestData requestData, string username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetPatientMasterBATCHHEADER(username).ToList();
                }
            }
        }

        public List<procGetPatientMasterBATCHDETAILS_Result> GetPatientMasterHISTORYDet(RequestData requestData, string username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetPatientMasterBATCHDETAILS(username).ToList();
                }
            }
        }
        public List<procshowGetALLPatientMasterHISTORYBATCHWISE_Result> GetshowGetALLPatientMasterHISTORYBATCHWISE(RequestData requestData, string batchno)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procshowGetALLPatientMasterHISTORYBATCHWISE(null, batchno).ToList();
                }
            }
        }
        public string AddUpdatePatientMasterList(RequestData request, string xml, string username)
        {
            string outres = string.Empty;

            //CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    //return coreEntities.ProgBulkUpdateXMLMOGAPL(xml, username).ToList();
                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    context.procBulkInsertXMLPatientMaster(xml, username, myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;
                }
            }
        }
        public string SavePatientMasterStatus(RequestData request, int id,int username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    var getPm = context.PatientMasters.FirstOrDefault(a => a.ID == id);
                    getPm.IsActive = getPm.IsActive ? false : true;
                    getPm.ModifiedBy = username;
                    getPm.ModifiedOn = DateTime.Now;
                    context.SaveChanges();
                }
            }
            return "Success";
        }
        public List<procshowGetALLHISTORYPatientMaster_Result> GetALLHISTORYDetailsPatientMaster(RequestData request)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    return context.procshowGetALLHISTORYPatientMaster(null).ToList();
                }
            }
        }
    }
}
