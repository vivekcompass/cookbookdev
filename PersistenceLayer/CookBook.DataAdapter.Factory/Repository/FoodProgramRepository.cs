﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IFoodProgramRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class FoodProgramRepository : EFSynchronousRepository<int, FoodProgram, CookBookCoreEntities>,
        IFoodProgramRepository
    {
        protected override DbSet<FoodProgram> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.FoodPrograms;
        }

        protected override FoodProgram FindImpl(int key, DbSet<FoodProgram> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override FoodProgram FindImplWithExpand(int key, DbSet<FoodProgram> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetFoodProgramList_Result> GetFoodProgramDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetFoodProgramList().ToList();
                }
            }
        }
    }
}
