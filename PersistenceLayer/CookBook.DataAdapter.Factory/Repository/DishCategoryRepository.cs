﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IDishCategoryRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DishCategoryRepository : EFSynchronousRepository<int, DishCategory, CookBookCoreEntities>,
        IDishCategoryRepository
    {
        protected override DbSet<DishCategory> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.DishCategories;
        }

        protected override DishCategory FindImpl(int key, DbSet<DishCategory> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override DishCategory FindImplWithExpand(int key, DbSet<DishCategory> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public string SaveDishCategorySiblingMapping(DishCategoryData model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.procSaveDishCategorySiblingData(model.DishCategoryCode,model.DayPartCode,model.DishCategorySiblings,requestData.SessionUserId);
                }
            }
            return "True";
        }

        List<procGetDishCategorySiblingList_Result> IDishCategoryRepository.GetDishCategorySiblingList(RequestData request)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    return context.procGetDishCategorySiblingList().ToList();
                }
            }
        }

        List<procGetDishCategoryList_Result> IDishCategoryRepository.GetDishCategoryDataList(RequestData request)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    return context.procGetDishCategoryList().ToList();
                }
            }
        }
    }
}
