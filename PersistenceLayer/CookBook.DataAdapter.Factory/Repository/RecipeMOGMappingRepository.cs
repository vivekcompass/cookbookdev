﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IRecipeMOGMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RecipeMOGMappingRepository : EFSynchronousRepository<int, RecipeMOGMapping, CookBookCoreEntities>,
        IRecipeMOGMappingRepository
    {
        protected override DbSet<RecipeMOGMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.RecipeMOGMappings;
        }

        protected override RecipeMOGMapping FindImpl(int key, DbSet<RecipeMOGMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override RecipeMOGMapping FindImplWithExpand(int key, DbSet<RecipeMOGMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
