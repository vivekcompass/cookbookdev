﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IDayPartRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DayPartRepository : EFSynchronousRepository<int, DayPart, CookBookCoreEntities>,
        IDayPartRepository
    {
        protected override DbSet<DayPart> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.DayParts;
        }

        protected override DayPart FindImpl(int key, DbSet<DayPart> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override DayPart FindImplWithExpand(int key, DbSet<DayPart> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
