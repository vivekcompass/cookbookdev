﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System.Data.Entity.Migrations;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IApplicationSettingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ApplicationSettingRepository : EFSynchronousRepository<int, ApplicationSetting, CookBookCoreEntities>,
        IApplicationSettingRepository 
    {
        protected override DbSet<ApplicationSetting> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.ApplicationSettings;
        }

        protected override ApplicationSetting FindImpl(int key, DbSet<ApplicationSetting> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ApplicationSetting FindImplWithExpand(int key, DbSet<ApplicationSetting> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

       }
}
