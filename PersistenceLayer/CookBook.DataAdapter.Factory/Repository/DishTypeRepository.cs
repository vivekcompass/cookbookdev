﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IDishTypeRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DishTypeRepository : EFSynchronousRepository<int, DishType, CookBookCoreEntities>,
        IDishTypeRepository
    {
        protected override DbSet<DishType> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.DishTypes;
        }

        protected override DishType FindImpl(int key, DbSet<DishType> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override DishType FindImplWithExpand(int key, DbSet<DishType> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
