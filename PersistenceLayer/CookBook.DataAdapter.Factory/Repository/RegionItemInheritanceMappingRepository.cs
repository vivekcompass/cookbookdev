﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System.Linq.Dynamic;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IRegionItemInheritanceMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RegionItemInheritanceMappingRepository : EFSynchronousRepository<int, RegionItemInheritanceMapping, CookBookCoreEntities>,
        IRegionItemInheritanceMappingRepository
    {
        protected override DbSet<RegionItemInheritanceMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.RegionItemInheritanceMappings;
        }

        protected override RegionItemInheritanceMapping FindImpl(int key, DbSet<RegionItemInheritanceMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override RegionItemInheritanceMapping FindImplWithExpand(int key, DbSet<RegionItemInheritanceMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetRegionItemList_Result> GetRegionItemInheritanceMappingDataList(string regionID, RequestData requestData)
        {
            //using (var transaction = new EFTransaction())
            //{
            //    int RegionID = int.Parse(regionID);
            //    using (var context = GetContext(transaction,requestData.SessionSectorName))
            //    {
            //        var list = from x in context.RegionItemInheritanceMappings
            //                   where x.Region_ID == RegionID
            //                   select x;

            //        return list.ToList();
            //    }
            //}
            using (var transaction = new EFTransaction())
            {
                int RegionID = int.Parse(regionID);
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var list = context.procGetRegionItemList(RegionID,"", null,null, requestData.SessionSectorNumber);
                    return list.ToList();
                }
            }
        }

        public string SaveSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                int regionID = model.Region_ID;
                int userID = (int)model.CreatedBy;
                string itemCodes = model.ItemCode;
                bool isActive = (bool)model.IsActive;
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var ret = context.SaveSectorToRegionInheritance(regionID, itemCodes, userID, isActive, model.SubSectorCode,requestData.SessionSectorNumber);
                    context.SaveChanges();
                    return "True";
                }
            }
        }

        public string SaveMOGNationalToSectorInheritance(SaveMOGNationalToSectorInheritanceData model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                int userID = (int)model.CreatedBy;
                string mogCodes = model.MOGCode;
                bool isActive = (bool)model.IsActive;
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var ret = context.SaveMOGNationalToSectorInheritance( mogCodes, userID, isActive, requestData.SessionSectorNumber);
                    context.SaveChanges();
                    return "True";
                }
            }
        }

        public string UpdateCostSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                int regionID = model.Region_ID;
                int userID = (int)model.CreatedBy;
                string itemCodes = model.ItemCode;
                bool isActive = (bool)model.IsActive;
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var ret = context.procUpdateCostForAllItemAtRegions(itemCodes, regionID,requestData.SessionSectorNumber);
                    context.SaveChanges();
                    return "True";
                }
            }
        }

    }
}
