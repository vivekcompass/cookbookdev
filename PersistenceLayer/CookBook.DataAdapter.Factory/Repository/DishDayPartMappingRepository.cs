﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IDishDayPartMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DishDayPartMappingRepository : EFSynchronousRepository<int, DishDayPartMapping, CookBookCoreEntities>,
        IDishDayPartMappingRepository
    {
        protected override DbSet<DishDayPartMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.DishDayPartMappings;
        }

        protected override DishDayPartMapping FindImpl(int key, DbSet<DishDayPartMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override DishDayPartMapping FindImplWithExpand(int key, DbSet<DishDayPartMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
