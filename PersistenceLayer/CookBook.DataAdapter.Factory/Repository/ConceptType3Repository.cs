﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IConceptType3Repository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ConceptType3Repository : EFSynchronousRepository<int, ConceptType3, CookBookCommonEntities>,
        IConceptType3Repository
    {
        protected override DbSet<ConceptType3> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.ConceptType3;
        }

        protected override ConceptType3 FindImpl(int key, DbSet<ConceptType3> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ConceptType3 FindImplWithExpand(int key, DbSet<ConceptType3> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
