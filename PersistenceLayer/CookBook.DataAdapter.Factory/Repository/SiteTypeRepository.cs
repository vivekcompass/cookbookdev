﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.Data.MasterData;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(ISiteTypeRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteTypeRepositoy : EFSynchronousRepository<int, SiteType, CookBookCommonEntities>,
        ISiteTypeRepository
    {
        protected override DbSet<SiteType> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.SiteTypes;
        }

        protected override SiteType FindImpl(int key, DbSet<SiteType> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SiteType FindImplWithExpand(int key, DbSet<SiteType> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
