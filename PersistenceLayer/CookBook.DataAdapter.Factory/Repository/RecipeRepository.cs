﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using CookBook.Data.Request;
using CookBook.DataAdapter.Factory.Common;
using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base.EF;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IRecipeRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RecipeRepository : EFSynchronousRepository<int, Recipe, CookBookCoreEntities>,
        IRecipeRepository
    {
        protected override DbSet<Recipe> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.Recipes;
        }

        protected override Recipe FindImpl(int key, DbSet<Recipe> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override Recipe FindImplWithExpand(int key, DbSet<Recipe> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetRecipeList_Result> GetRecipeDataList(string condition, string DishCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetRecipeList(condition, DishCode, requestData.SessionSectorNumber).ToList();
                }
            }
        }

        public IList<procGetnationalRecipeList_Result> GetNationalRecipeDataList(string condition, string DishCode, RequestData requestData)
        {
            CookBookCommonEntities commonEntities = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return commonEntities.procGetnationalRecipeList(condition, DishCode).ToList();
                }
            }
        }
        public IList<procGetRegionRecipeList_Result> GetRegionRecipeDataList(int RegionID, string condition, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetRegionRecipeList(RegionID, condition, requestData.SubSectorCode, requestData.SessionSectorNumber).ToList();
                    //return null;
                }
            }
        }

        public IList<procGetSiteRecipeList_Result> GetSiteRecipeDataList(string SiteCode, string condition, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetSiteRecipeList(SiteCode, condition).ToList();
                }
            }
        }
        //Krish
        public IList<procGetSiteDishRecipeList_Result> GetSiteDishRecipeDataList(string SiteCode, string dishCode, string condition, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetSiteDishRecipeList(SiteCode, dishCode, condition).ToList();
                }
            }
        }


        public IList<procGetBaseRecipesByRecipeID_Result> GetBaseRecipesByRecipeID(int recipeID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetBaseRecipesByRecipeID(requestData.RecipeCode).ToList();
                }
            }
        }

        public IList<procGetRegionBaseRecipesByRecipeID_Result> GetRegionBaseRecipesByRecipeID(int regionID, int recipeID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetRegionBaseRecipesByRecipeID(requestData.RecipeCode, regionID, requestData.SubSectorCode).ToList();
                }
            }
        }
        public IList<procGetSiteBaseRecipesByRecipeID_Result> GetSiteBaseRecipesByRecipeID(string SiteCode, int recipeID, string recipeCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetSiteBaseRecipesByRecipeID(recipeCode, SiteCode, requestData.RegionID).ToList();
                }
            }
        }

        public IList<procGetMOGsByRecipeID_Result> GetMOGsByRecipeID(int recipeID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetMOGsByRecipeID(requestData.RecipeCode).ToList();
                }
            }
        }

        public IList<procGetRegionMOGsByRecipeID_Result> GetRegionMOGsByRecipeID(int regionID, int recipeID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetRegionMOGsByRecipeID(requestData.RecipeCode, regionID, requestData.SubSectorCode, requestData.SessionSectorNumber).ToList();
                }
            }
        }
        public IList<procGetSiteMOGsByRecipeID_Result> GetSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetSiteMOGsByRecipeID(recipeCode, SiteCode, requestData.SessionSectorNumber, requestData.RegionID).ToList();
                }
            }
        }

        public string SaveList(IList<Recipe> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (Recipe m in model)
                    {

                        context.Recipes.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }
        public string SaveRegionList(IList<RegionRecipe> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (RegionRecipe m in model)
                    {

                        context.RegionRecipes.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }
        public string SaveSiteList(IList<SiteRecipe> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (SiteRecipe m in model)
                    {

                        context.SiteRecipes.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }
        public string SaveRecipeData(Recipe entity, IList<RecipeMOGMapping> entity1, IList<RecipeMOGMapping> entity2, RequestData requestData)
        {
            int RecipeID = entity.ID;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    if (requestData.SessionSectorNumber == "20")
                    {
                        entity.DKgPerUOM = entity.TotalIngredientWeight > 0 ? entity.TotalIngredientWeight / entity.Quantity : 0;
                        entity.CostPerUOM = entity.CostPerKG;
                    }
                    context.Recipes.AddOrUpdate(entity);
                    context.SaveChanges();

                    var list = from x in context.RecipeMOGMappings
                               where x.Recipe_ID == RecipeID
                               select x;
                    IList<RecipeMOGMapping> llist = list.ToList();

                    foreach (RecipeMOGMapping m in llist)
                    {
                        m.IsActive = false;
                        m.ModifiedBy = entity.ModifiedBy;
                        m.ModifiedOn = DateTime.Now;
                        context.RecipeMOGMappings.AddOrUpdate(m);
                    }

                    context.SaveChanges();
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    entity1 = entity1.Reverse().ToList();
                    foreach (RecipeMOGMapping m in entity1)
                    {
                        m.IsActive = true;
                        var isItemExists = context.RecipeMOGMappings.Where(item => item.RecipeCode == m.RecipeCode && item.BaseRecipeCode == m.BaseRecipeCode).FirstOrDefault();
                        m.Recipe_ID = entity.ID;
                        m.RecipeCode = entity.RecipeCode;
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            m.ModifiedOn = DateTime.Now;
                            m.ModifiedBy = entity.ModifiedBy;
                            m.CreatedOn = isItemExists.CreatedOn;
                            m.CreatedBy = isItemExists.CreatedBy;
                            m.ID = isItemExists.ID;
                        }
                        else
                        {
                            m.CreatedOn = DateTime.Now;
                            m.CreatedBy = entity.ModifiedBy;
                            m.ModifiedBy = null;
                            m.ModifiedOn = null;
                            m.ID = 0;
                        }
                        context.RecipeMOGMappings.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    entity2 = entity2.Reverse().ToList();
                    foreach (RecipeMOGMapping m in entity2)
                    {
                        m.IsActive = true;
                        var isItemExists = context.RecipeMOGMappings.Where(item => item.RecipeCode == m.RecipeCode && item.MOGCode == m.MOGCode).FirstOrDefault();
                        m.Recipe_ID = entity.ID;
                        m.RecipeCode = entity.RecipeCode;
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            m.ModifiedOn = DateTime.Now;
                            m.ModifiedBy = entity.ModifiedBy;
                            m.CreatedOn = isItemExists.CreatedOn;
                            m.CreatedBy = isItemExists.CreatedBy;
                            m.ID = isItemExists.ID;
                        }
                        else
                        {
                            m.CreatedOn = DateTime.Now;
                            m.CreatedBy = entity.ModifiedBy;
                            m.ModifiedBy = null;
                            m.ModifiedOn = null;
                            m.ID = 0;
                        }
                        context.RecipeMOGMappings.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                }
            }
            return "true";
        }

        public string SaveRegionRecipeData(RegionRecipe entity, IList<RegionRecipeMOGMapping> entity1, IList<RegionRecipeMOGMapping> entity2, RequestData requestData)
        {
            int RecipeID = entity.ID;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var lst = from x in context.RegionRecipes
                              where x.ID == entity.ID
                              select x;
                    RegionRecipe rr = lst.First();
                    rr.Instructions = entity.Instructions;
                    rr.TotalCost = entity.TotalCost;
                    rr.CostPerKG = entity.CostPerKG;
                    rr.CostPerUOM = entity.CostPerKG;
                    rr.ModifiedOn = DateTime.Now;
                    rr.ModifiedBy = entity.ModifiedBy;
                    rr.Quantity = entity.Quantity;
                    rr.UOMCode = entity.UOMCode;
                    rr.UOM_ID = entity.UOM_ID;
                    rr.WeightPerUnit = entity.WeightPerUnit;
                    rr.WeightPerUnitGravy = entity.WeightPerUnitGravy;
                    rr.WeightPerUnitUOM = entity.WeightPerUnitUOM;
                    rr.WeightPerUnitUOMGravy = entity.WeightPerUnitUOMGravy;
                    rr.TotalIngredientWeight = entity.TotalIngredientWeight;
                    rr.RecipeUOMCategory = entity.RecipeUOMCategory;
                    if (requestData.SessionSectorNumber == "20")
                    {
                        entity.DKgPerUOM = entity.TotalIngredientWeight > 0 ? entity.TotalIngredientWeight / entity.Quantity : 0;
                        rr.DKgPerUOM = entity.DKgPerUOM;
                    }

                    context.RegionRecipes.AddOrUpdate(rr);

                    context.SaveChanges();

                    //---- Delete functionality not availabe for site ---//

                    var list = from x in context.RegionRecipeMOGMappings
                               where x.RecipeCode == entity.RecipeCode && x.Region_ID == entity.Region_ID
                               select x;
                    IList<RegionRecipeMOGMapping> llist = list.ToList();

                    foreach (RegionRecipeMOGMapping m in llist)
                    {
                        m.IsActive = false;
                        m.ModifiedBy = entity.ModifiedBy;
                        m.ModifiedOn = DateTime.Now;
                        context.RegionRecipeMOGMappings.AddOrUpdate(m);
                    }

                    context.SaveChanges();
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (RegionRecipeMOGMapping m in entity1)
                    {
                        m.IsActive = true;
                        var isItemExists = context.RegionRecipeMOGMappings.Where(item => item.RecipeCode == m.RecipeCode && item.BaseRecipe_ID == m.BaseRecipe_ID
                                                                            && item.Region_ID == m.Region_ID).FirstOrDefault();
                        m.Recipe_ID = entity.ID;
                        m.RecipeCode = entity.RecipeCode;
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            isItemExists.IsActive = true;
                            isItemExists.ModifiedBy = entity.ModifiedBy;
                            isItemExists.ModifiedOn = DateTime.Now;
                            isItemExists.Quantity = m.Quantity;
                            isItemExists.IngredientPerc = m.IngredientPerc;
                            isItemExists.TotalCost = m.TotalCost;

                            context.RegionRecipeMOGMappings.AddOrUpdate(isItemExists);
                        }
                        else
                        {
                            m.CreatedOn = DateTime.Now;
                            m.CreatedBy = entity.ModifiedBy;
                            m.ModifiedBy = null;
                            m.ModifiedOn = null;
                            m.ID = 0;
                            context.RegionRecipeMOGMappings.AddOrUpdate(m);
                        }

                    }
                    context.SaveChanges();
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (RegionRecipeMOGMapping m in entity2)
                    {
                        m.IsActive = true;
                        var isItemExists = context.RegionRecipeMOGMappings.Where(item => item.RecipeCode == m.RecipeCode && item.MOGCode == m.MOGCode
                                                                            && item.Region_ID == m.Region_ID).FirstOrDefault();
                        m.Recipe_ID = entity.ID;
                        m.RecipeCode = entity.RecipeCode;
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            isItemExists.IsActive = true;
                            isItemExists.ModifiedBy = entity.ModifiedBy;
                            isItemExists.ModifiedOn = DateTime.Now;
                            isItemExists.Quantity = m.Quantity;
                            isItemExists.IngredientPerc = m.IngredientPerc;
                            isItemExists.TotalCost = m.TotalCost;
                            isItemExists.DKGValue = m.DKGValue;
                            isItemExists.UOMCode = m.UOMCode;
                            isItemExists.UOM_ID = m.UOM_ID;
                            context.RegionRecipeMOGMappings.AddOrUpdate(isItemExists);
                        }
                        else
                        {
                            m.CreatedOn = DateTime.Now;
                            m.CreatedBy = entity.ModifiedBy;
                            m.ModifiedBy = null;
                            m.ModifiedOn = null;
                            m.ID = 0;
                            context.RegionRecipeMOGMappings.AddOrUpdate(m);
                        }

                    }
                    context.SaveChanges();
                }
            }
            return "true";
        }


        public string SaveSiteRecipeData(SiteRecipe entity, IList<SiteRecipeMOGMapping> entity1, IList<SiteRecipeMOGMapping> entity2, RequestData requestData)
        {
            int RecipeID = entity.ID;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    if (requestData.SessionSectorNumber == "20")
                    {
                        entity.DKgPerUOM = entity.TotalIngredientWeight > 0 ? entity.TotalIngredientWeight / entity.Quantity : 0;
                        entity.CostPerUOM = entity.CostPerKG;
                    }
                    context.SiteRecipes.AddOrUpdate(entity);

                    //---- Delete functionality not availabe for site ---//

                    var list = from x in context.SiteRecipeMOGMappings
                               where x.RecipeCode == entity.RecipeCode && x.SiteCode == entity.SiteCode
                               select x;
                    IList<SiteRecipeMOGMapping> llist = list.ToList();

                    foreach (SiteRecipeMOGMapping m in llist)
                    {
                        m.IsActive = false;
                        m.ModifiedBy = entity.ModifiedBy;
                        m.ModifiedOn = DateTime.Now;
                        context.SiteRecipeMOGMappings.AddOrUpdate(m);
                    }

                    context.SaveChanges();
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (SiteRecipeMOGMapping m in entity1)
                    {
                        m.IsActive = true;
                        var isItemExists = context.SiteRecipeMOGMappings.Where(item => item.RecipeCode == m.RecipeCode && item.BaseRecipe_ID == m.BaseRecipe_ID
                                                                            && item.SiteCode == m.SiteCode).FirstOrDefault();
                        m.Recipe_ID = entity.ID;
                        m.RecipeCode = entity.RecipeCode;
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            isItemExists.ModifiedBy = entity.ModifiedBy;
                            isItemExists.ModifiedOn = DateTime.Now;
                            isItemExists.Quantity = m.Quantity;
                            isItemExists.IngredientPerc = m.IngredientPerc;
                            isItemExists.TotalCost = m.TotalCost;

                            context.SiteRecipeMOGMappings.AddOrUpdate(isItemExists);
                        }
                        else
                        {
                            m.CreatedOn = DateTime.Now;
                            m.CreatedBy = entity.ModifiedBy;
                            m.ModifiedBy = null;
                            m.ModifiedOn = null;
                            m.ID = 0;
                            context.SiteRecipeMOGMappings.AddOrUpdate(m);
                        }

                    }
                    context.SaveChanges();
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (SiteRecipeMOGMapping m in entity2)
                    {
                        m.IsActive = true;
                        var isItemExists = context.SiteRecipeMOGMappings.Where(item => item.RecipeCode == m.RecipeCode && item.MOG_ID == m.MOG_ID
                                                                            && item.SiteCode == m.SiteCode).FirstOrDefault();
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            isItemExists.IsActive = true;
                            isItemExists.ModifiedBy = entity.ModifiedBy;
                            isItemExists.ModifiedOn = DateTime.Now;
                            isItemExists.Quantity = m.Quantity;
                            isItemExists.IngredientPerc = m.IngredientPerc;
                            isItemExists.TotalCost = m.TotalCost;
                            isItemExists.ArticleNumber = m.ArticleNumber;
                            isItemExists.DKGValue = m.DKGValue;
                            isItemExists.UOMCode = m.UOMCode;
                            isItemExists.UOM_ID = m.UOM_ID;
                            context.SiteRecipeMOGMappings.AddOrUpdate(isItemExists);
                        }
                        else
                        {
                            m.CreatedOn = DateTime.Now;
                            m.CreatedBy = entity.ModifiedBy;
                            m.ModifiedBy = null;
                            m.ModifiedOn = null;
                            m.ID = 0;
                            m.Recipe_ID = entity.ID;//Id missing
                            m.RecipeCode = entity.RecipeCode;
                            context.SiteRecipeMOGMappings.AddOrUpdate(m);
                        }
                        context.SaveChanges();
                    }
                }
            }

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.procUpdateAllergensAtSiteByRecipeCode(entity.RecipeCode, entity.SiteCode);

                    context.SaveChanges();
                }
            }
            return "true";
        }

        public string SaveSiteRecipeAllergenData(string recipeCode, string siteCode, RequestData requestData)
        {
            string allergnString = "";
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.procUpdateAllergensAtSiteByRecipeCode(recipeCode, siteCode);

                    context.SaveChanges();

                    var recipeData = context.SiteRecipes.Where(m => m.SiteCode == siteCode && m.RecipeCode == recipeCode).FirstOrDefault();
                    if (recipeData != null && recipeData.ID > 0)
                    {
                        allergnString = recipeData.AllergensName;
                    }
                }
            }
            return allergnString;
        }

        public IList<procGetAPLMasterDataForRegion_Result> GetAPLMasterRegionDataList(string articleNumber, string mogCode, int Region_ID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetAPLMasterDataForRegion(articleNumber, mogCode, Region_ID, requestData.SessionSectorNumber).ToList();
                }
            }
        }
        public IList<procGetAPLMasterDataForSite_Result> GetAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetAPLMasterDataForSite(articleNumber, mogCode, SiteCode, requestData.SessionSectorNumber).ToList();
                }
            }
        }

        public string SaveRecipeNutrientMappingData(RecipeData model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var isItemExists = context.Recipes.Where(item => item.RecipeCode == model.RecipeCode).FirstOrDefault();
                    if (isItemExists != null && isItemExists.ID > 0)
                    {
                        isItemExists.ModifiedBy = requestData.SessionUserId;
                        isItemExists.ModifiedOn = DateTime.Now;
                        isItemExists.NutrientRecipeQuantity = model.NutrientRecipeQuantity;
                        isItemExists.NutritionData = model.NutritionData;
                        context.Recipes.AddOrUpdate(isItemExists);
                        context.SaveChanges();
                    }
                }
            }
            return "true";
        }

        public IList<procGetItemDishRecipeNutritionData_Result> GetItemDishRecipeNutritionData(string SiteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetItemDishRecipeNutritionData(SiteCode).ToList();
                }
            }
        }

        //Krish
        public IList<procGetRecipeMOGNLevelFOrPrinting_Result> GetRecipePrintingData(string recipeCode, string siteCode, int regId, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetRecipeMOGNLevelFOrPrinting(recipeCode, regId, siteCode, requestData.SessionSectorNumber).OrderByDescending(a => a.MOGQty).ToList();
                }
            }
        }
        public string GetRecipeDishDataForPrinting(string recipeCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var isMappingExists = context.DishRecipeMappings.Where(m => m.RecipeCode == recipeCode);
                    if (isMappingExists != null && isMappingExists.Count() > 0)
                    {
                        var getDish = (from r in context.Recipes
                                       join rd in context.DishRecipeMappings on r.RecipeCode equals rd.RecipeCode
                                       join d in context.Dishes on rd.DishCode equals d.DishCode
                                       where r.RecipeCode == recipeCode
                                       select d.DishCode + " " + d.Name).ToList().Aggregate((a, b) => a + "," + b);

                        return getDish;
                    }
                }
            }
            return "";
        }
        public List<NutritionDishRecipeMogMapping> GetRecipeDishDataByDish(string dishCode, RequestData requestData)
        {
            var data = new List<NutritionDishRecipeMogMapping>();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var isMappingExists = context.DishRecipeMappings.Where(m => m.DishCode == dishCode);
                    if (isMappingExists != null && isMappingExists.Count() > 0)
                    {
                        var getDish = (from d in context.Dishes
                                       join rd in context.DishRecipeMappings on d.DishCode equals rd.DishCode
                                       join r in context.Recipes on rd.RecipeCode equals r.RecipeCode
                                       where d.DishCode == dishCode
                                       select new NutritionDishRecipeMogMapping
                                       {
                                           DishCode = d.DishCode,
                                           DishName = d.Name,
                                           RecipeCode = r.RecipeCode,
                                           RecipeName = r.Name,
                                           RecipeID = r.ID,
                                           Quantity = r.Quantity,
                                           Allergens = r.AllergensName,
                                           NGAllergens = r.NGAllergens,
                                           Instructions = r.Instructions,
                                           RecipeType = r.IsBase == true ? "Base" : "Final",
                                           IsDefault = rd.IsDefault
                                           //RecipeMOG=
                                           //Krish 06-08-2022
                                           // RecipeMOG=context.procGetMOGNLevelForSplitScreen(r.RecipeCode,requestData.SessionSectorNumber)
                                       }).ToList();
                        //var getrecipeMog=context.procGetRecipeMOGNLevelFOrPrinting()
                        return getDish;
                    }
                }
            }
            return data;
        }
        //Krish 06-08-2022
        public IList<procGetMOGNLevelForSplitScreen_Result> GetMOGNLevelForSplitScreen(string recipeCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetMOGNLevelForSplitScreen(recipeCode, requestData.SessionSectorNumber).OrderByDescending(a => a.MOGQty).ToList();
                }
            }
        }

        //Krish 31-05-2022
        public string SaveRecipeCopy(List<RecipeCopyData> model, List<string> recipes, string sourceSite, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                var commonDb = new CookBookCommonEntities();
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    try
                    {
                        foreach (var item in model)
                        {
                            foreach (var rec in recipes)
                            {

                                context.SiteRecipes.Where(a => a.SiteCode == item.SiteCode && a.RecipeCode == rec).ToList().ForEach(a => context.SiteRecipes.Remove(a));
                                context.SiteRecipeMOGMappings.Where(a => a.SiteCode == item.SiteCode && a.RecipeCode == rec).ToList().ForEach(a => context.SiteRecipeMOGMappings.Remove(a));
                                context.SaveChanges();

                                var site = commonDb.SiteMasters.FirstOrDefault(a => a.SiteCode == item.SiteCode).SiteID;
                                context.procNLevelRecipeCopyForSite(requestData.SessionSectorNumber, sourceSite, item.SiteCode, rec, requestData.SessionUserId, site);


                            }
                        }
                    }
                    catch (Exception ex) { return "false"; }
                    //var isItemExists = context.Recipes.Where(item => item.RecipeCode == model.RecipeCode).FirstOrDefault();
                    //if (isItemExists != null && isItemExists.ID > 0)
                    //{
                    //    isItemExists.ModifiedBy = requestData.SessionUserId;
                    //    isItemExists.ModifiedOn = DateTime.Now;
                    //    isItemExists.NutrientRecipeQuantity = model.NutrientRecipeQuantity;
                    //    isItemExists.NutritionData = model.NutritionData;
                    //    context.Recipes.AddOrUpdate(isItemExists);
                    //    context.SaveChanges();
                    //}
                }
            }
            return "true";
        }

        //Krish 03-08-2022
        public SectorRecipeData GetSectorRecipeData(RequestData requestData, bool isAddEdit, bool onLoad)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    if (isAddEdit)
                    {


                        var Results = MultipleResultSets
                         .MultipleResults(context, $"EXEC procGetSectorRecipeDataAddEdit {requestData.SessionSectorNumber}")

                         .AddResult<MOGData>()
                         .AddResult<DishData>()
                         .AddResult<UOMModuleMappingData>()
                         .AddResult<NutritionElementMasterData>()
                         .AddResult<SubSectorData>()
                         .AddResult<RecipeData>()
                         .AddResult<UOMData>()
                         .Execute();
                        var Output = new SectorRecipeData
                        {

                            MOGData = Results[0] as MOGData[],
                            DishData = Results[1] as DishData[],
                            UOMModuleMappingData = Results[2] as UOMModuleMappingData[],
                            NutritionElementMasterData = Results[3] as NutritionElementMasterData[],
                            SubSectorData = Results[4] as SubSectorData[],
                            RecipeData = Results[5] as RecipeData[],
                            UOMData = Results[6] as UOMData[]
                        };

                        return Output;
                    }

                    if (onLoad)
                    {


                        var Results = MultipleResultSets
                         .MultipleResults(context, $"EXEC procGetSectorRecipeDataOnLoad {requestData.SessionSectorNumber}")

                         .AddResult<ApplicationSettingData>()

                         .AddResult<RecipeData>()
                         .Execute();
                        var Output = new SectorRecipeData
                        {

                            ApplicationSettingData = Results[0] as ApplicationSettingData[],

                            RecipeData = Results[1] as RecipeData[]
                        };

                        return Output;
                    }
                    return new SectorRecipeData();
                }
            }
        }

    }
}
