﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System.Data.Entity.Migrations;
using System;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IDishRecipeMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DishRecipeMappingRepository : EFSynchronousRepository<int, DishRecipeMapping, CookBookCoreEntities>,
        IDishRecipeMappingRepository
    {
        protected override DbSet<DishRecipeMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.DishRecipeMappings;
        }

        protected override DishRecipeMapping FindImpl(int key, DbSet<DishRecipeMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override DishRecipeMapping FindImplWithExpand(int key, DbSet<DishRecipeMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public string SaveDishRecipeMappingList(IList<DishRecipeMapping> model, int dishStatus, RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
             
                string DishCode = model[0].DishCode;
              

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    var dishObj = context.Dishes.Where(m => m.DishCode == DishCode).FirstOrDefault();
                    dishObj.Status = dishStatus;
                    dishObj.ModifiedBy = model[0].CreatedBy;
                    dishObj.ModifiedOn = DateTime.Now;
                    context.Dishes.AddOrUpdate(dishObj);
                    var list = from x in context.DishRecipeMappings
                               where x.DishCode == DishCode
                               select x;
                    IList<DishRecipeMapping> llist = list.ToList();
                    foreach (DishRecipeMapping m in llist)
                    {
                        m.IsActive = false;
                        m.ModifiedBy = model[0].CreatedBy;
                        m.ModifiedOn = DateTime.Now;
                        context.DishRecipeMappings.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                   
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (DishRecipeMapping m in model)
                    {
                        m.IsActive = true;
                        var isItemExists = context.DishRecipeMappings.Where(item => item.DishCode == m.DishCode && item.RecipeCode == m.RecipeCode).FirstOrDefault();
                        if (isItemExists != null && isItemExists.ID > 0)
                            if (isItemExists != null && isItemExists.ID > 0)
                            {
                                m.ModifiedOn = DateTime.Now;
                                m.ModifiedBy = m.CreatedBy;
                                m.ID = isItemExists.ID;
                            }
                            else
                            {
                                m.CreatedOn = DateTime.Now;
                                m.ID = 0;
                            }
                        context.DishRecipeMappings.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }

       public  IList<DishRecipeMapping> GetDishRecipeMappingDataList(string DishCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
               
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = from x in context.DishRecipeMappings
                               where x.DishCode == DishCode && x.IsActive.Value
                               select x;

                    return list.ToList();
                }
            }
        }

        public IList<RegionDishRecipeMapping> GetRegionDishRecipeMappingDataList(int RegionID,string DishCode, string SubSectorCode, RequestData requestData)
        {
            var dishRecipeList = new List<RegionDishRecipeMapping>();
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {

                    if (!string.IsNullOrWhiteSpace(SubSectorCode))
                    {
                        var list = from x in context.RegionDishRecipeMappings
                                   where x.DishCode == DishCode && x.Region_ID == RegionID && x.SubSectorCode == SubSectorCode //&& x.IsActive.Value
                                   select x;
                        dishRecipeList = list.ToList();
                    }
                    else
                    {
                        var list = from x in context.RegionDishRecipeMappings
                                   where x.DishCode == DishCode && x.Region_ID == RegionID //&& x.IsActive.Value
                                   select x;
                        dishRecipeList = list.ToList();
                    }
                        

                    return dishRecipeList;
                }
            }
        }

        public IList<SiteDishRecipeMapping> GetSiteDishRecipeMappingDataList(string SiteCode, string DishCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = from x in context.SiteDishRecipeMappings
                               where x.DishCode == DishCode && x.SiteCode == SiteCode //&& x.IsActive.Value
                               select x;

                    return list.ToList();
                }
            }
        }


        public string SaveRegionDishRecipeMappingList(IList<RegionDishRecipeMapping> model, RequestData requestData)
        {
            var dishCode = model[0].DishCode;
            int Region_ID = model[0].Region_ID;
            //linq
            //modified date
            

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (RegionDishRecipeMapping m in model)
                    {

                        context.RegionDishRecipeMappings.AddOrUpdate(m);
                    }
            
                    context.SaveChanges();
                    var list = from x in context.RegionDishes
                               where x.DishCode == dishCode && x.Region_ID == Region_ID
                               select x;
                    var regionDishData = list.FirstOrDefault();
                    regionDishData.ModifiedOn = DateTime.Now;
                    context.RegionDishes.AddOrUpdate(regionDishData);
                    
                    context.SaveChanges();
                    return "true";
                }
            }
        }
        public string SaveSiteDishRecipeMappingList(IList<SiteDishRecipeMapping> model, RequestData requestData)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (SiteDishRecipeMapping m in model)
                    {

                        context.SiteDishRecipeMappings.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }
    }
}
