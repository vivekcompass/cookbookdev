﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IConceptType4Repository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ConceptType4Repository : EFSynchronousRepository<int, ConceptType4, CookBookCommonEntities>,
        IConceptType4Repository
    {
        protected override DbSet<ConceptType4> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.ConceptType4;
        }

        protected override ConceptType4 FindImpl(int key, DbSet<ConceptType4> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ConceptType4 FindImplWithExpand(int key, DbSet<ConceptType4> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
