﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IRevenueTypeRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RevenueTypeRepository : EFSynchronousRepository<int, RevenueType, CookBookCoreEntities>,
        IRevenueTypeRepository
    {
        protected override DbSet<RevenueType> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.RevenueTypes;
        }

        protected override RevenueType FindImpl(int key, DbSet<RevenueType> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override RevenueType FindImplWithExpand(int key, DbSet<RevenueType> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
