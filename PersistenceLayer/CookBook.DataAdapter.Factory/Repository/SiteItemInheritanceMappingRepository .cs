﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using CookBook.Data.Request;
using System;
using CookBook.Data.UserData;
using System.Xml.Linq;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(ISiteItemInheritanceMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteItemInheritanceMappingRepository : EFSynchronousRepository<int, SiteItemInheritanceMapping, CookBookCoreEntities>,
        ISiteItemInheritanceMappingRepository
    {
        protected override DbSet<SiteItemInheritanceMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.SiteItemInheritanceMappings;
        }

        protected override SiteItemInheritanceMapping FindImpl(int key, DbSet<SiteItemInheritanceMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SiteItemInheritanceMapping FindImplWithExpand(int key, DbSet<SiteItemInheritanceMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetSiteItemInheritanceMappingData_Result> GetSiteItemInheritanceMappingDataList(string SiteCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = context.procGetSiteItemInheritanceMappingData(SiteCode, null);
                    return list.ToList();
                }
            }
        }
        public string SaveToSiteInheritance(SiteItemInheritanceMapping model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
               
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.SiteItemInheritanceMappings.AddOrUpdate(model);
                    context.SaveChanges();
                    return "True";
                }
            }
        }

        public string SaveRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                string SiteCode = model.SiteCode;
                int userID = (int)model.CreatedBy;
                string itemCodes = model.ItemCode;
                bool isActive = (bool)model.IsActive;
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var ret = context.SaveRegionToSiteInheritance(SiteCode, itemCodes, userID, isActive,requestData.SubSectorCode,requestData.SessionSectorNumber);
                    context.SaveChanges();
                    return "True";
                }
            }
        }

        public string UpdateCostRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                string SiteCode = model.SiteCode;
                int userID = (int)model.CreatedBy;
                string itemCodes = model.ItemCode;
                bool isActive = (bool)model.IsActive;
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var ret = context.procUpdateCostForAllItemAtSite(itemCodes,SiteCode);
                    context.SaveChanges();
                    return "True";
                }
            }
        }

        public string SaveToSiteInheritanceDataList(IList<SiteItemInheritanceMapping> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {


                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (var m in model)
                    {
                        bool isSaveToPriceHistory = false;
                        string SiteCode = m?.SiteCode;
                        string itemCode = m?.ItemCode;
                        decimal? price = m?.ItemPrice;
                        decimal? maxmealcount = m?.MaxMealCount;
                        int? priceStatus = m?.PriceStatus;
                        var list = from x in context.SiteItemInheritanceMappings
                                   where x.SiteCode == SiteCode && x.ItemCode == itemCode
                                   select x;
                        SiteItemInheritanceMapping listf = list.FirstOrDefault();
                        if (priceStatus == 3 && listf.ItemPrice != price)
                        {
                            isSaveToPriceHistory = true;
                        }
                        listf.ModifiedBy = m.ModifiedBy;
                        listf.ModifiedOn = m.ModifiedOn;
                        listf.PriceStatus = priceStatus;
                        listf.MaxMealCount = maxmealcount;
                        listf.ItemPrice = price;

                        context.SiteItemInheritanceMappings.AddOrUpdate(listf);
                        if (isSaveToPriceHistory)
                        {
                            ItemPriceHistory iph = new ItemPriceHistory();
                            iph.ItemCode = itemCode;
                            iph.SiteCode = SiteCode;
                            iph.ItemPrice = price;
                            iph.ModifiedBy = m.ModifiedBy;
                            iph.DateOn = m.ModifiedOn;
                            context.ItemPriceHistories.AddOrUpdate(iph);
                        }
                    }
                    context.SaveChanges();
                    return "true";

                }

            }

        }

        public IList<ProcGetInheritChangesSectorToRegion_Result> GetInheritChangesSectorToRegion(ItemInheritChangeRequestData model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var list = context.ProcGetInheritChangesSectorToRegion(model.RegionID, model.ItemCode, model.SubSectorCode);
                    return list.ToList();
                }
            }
        }

        public IList<ProcGetInheritChangesRegionToSite_Result> GetInheritChangesRegionToSite(ItemInheritChangeRequestData model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var list = context.ProcGetInheritChangesRegionToSite(model.RegionID, model.ItemCode, model.SubSectorCode);
                    return list.ToList();
                }
            }
        }

        public string SaveChangesSectorToRegion(List<InheritanceChangesData> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                var xEle = new XElement("InheritanceChangesDatas",
               from obj in model
               select new XElement("InheritanceChangesData",
                      new XElement("Code", obj.Code),
                      new XElement("ChangeType", obj.ChangeType),
                      new XElement("ChangeMadeBy", obj.ChangeMadeBy),
                      new XElement("ChangeFrom", obj.ChangeFrom), 
                      new XElement("ActionType", obj.ActionType),
                      new XElement("ChangeTo", obj.ChangeTo)
                    ));
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.procSaveComparedSectorToRegionItemData(model[0].ItemCode, requestData.SessionUserId, model[0].RegionID, model[0].SubSectorCode, xEle.ToString());
                }
            }
            return "true";
        }
            
        public string SaveChangesRegionToSite(List<InheritanceChangesData> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.SaveRegionToSiteInheritance("", "", requestData.SessionUserId, true, "", requestData.SessionSectorNumber);
                }
            }
            return "true";
        }
    }
}
