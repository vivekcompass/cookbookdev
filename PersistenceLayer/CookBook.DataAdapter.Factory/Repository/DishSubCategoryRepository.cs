﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IDishSubCategoryRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DishSubCategoryRepository : EFSynchronousRepository<int, DishSubCategory, CookBookCoreEntities>,
        IDishSubCategoryRepository
    {
        protected override DbSet<DishSubCategory> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.DishSubCategories;
        }

        protected override DishSubCategory FindImpl(int key, DbSet<DishSubCategory> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override DishSubCategory FindImplWithExpand(int key, DbSet<DishSubCategory> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
