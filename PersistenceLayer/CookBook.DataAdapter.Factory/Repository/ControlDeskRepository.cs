﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Contract;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Repository
{
    [Export(typeof(IControlDeskRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ControlDeskRepository : EFSynchronousRepository<int, SectorMaster, CookBookCommonEntities>,IControlDeskRepository
    {
       
        public List<procGetSectorMasterData_Result> GetManageSectorList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetSectorMasterData().ToList();
                }
            }
        }

        public List<procGetCapringSectorMaster_Result> GetSectorListInfo(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetCapringSectorMaster().ToList();
                }
            }
        }


        public List<procGetCapringLevelMaster_Result> GetLevelMasterData(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetCapringLevelMaster().ToList();
                }
            }
        }

        public List<procGetCapringFunctionMaster_Result> GetFunctionMasterData(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetCapringFunctionMaster().ToList();
                }
            }
        }

        public List<procGetCapringPermissionMaster_Result> GetPermissionMasterData(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetCapringPermissionMaster().ToList();
                }
            }
        }



        public string SaveManageSectorInfo(RequestData request, ManageSectorData model, int usercode)
        {
            string outres = string.Empty;
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    cookBookCommon.procSaveSactorData(model.SectorNumber,model.SectorName,model.SectorCode,usercode,myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;
                }
            }
        }

        public string SaveManageLevelInfo(RequestData request, LevelMasterData model, int usercode)
        {
            string outres = string.Empty;
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    string code = (from record in cookBookCommon.LevelMasters orderby record.ID descending select record.Code).First();
                    if (string.IsNullOrEmpty(code))
                    {
                        code = "1";
                        System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                        cookBookCommon.procSavelevelData(code, model.Name, model.Description, usercode, myOutputParamString).ToString();
                        outres = Convert.ToString(myOutputParamString.Value);
                        return outres;
                    }
                    else
                    {
                        code = (Convert.ToInt32(code) + 1).ToString();
                        model.Code = code;
                        System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                        cookBookCommon.procSavelevelData(model.Code, model.Name, model.Description, usercode, myOutputParamString).ToString();
                        outres = Convert.ToString(myOutputParamString.Value);
                        return outres;
                    }
                    
                }
            }
        }

        public string SaveFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode)
        {
            string outres = string.Empty;
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    string code = (from record in cookBookCommon.FunctionalityMasters orderby record.ID descending select record.Code).First();
                    if (string.IsNullOrEmpty(code))
                    {
                        code = "01";
                        System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                        cookBookCommon.procSavefunctionData(code, model.Name, model.Description,model.Remarks, usercode, myOutputParamString).ToString();
                        outres = Convert.ToString(myOutputParamString.Value);
                        return outres;
                    }
                    else
                    {
                        code = (Convert.ToInt32(code) + 1).ToString().PadLeft(2, '0');
                        model.Code = code;
                        System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                        cookBookCommon.procSavefunctionData(code, model.Name, model.Description, model.Remarks, usercode, myOutputParamString).ToString();
                        outres = Convert.ToString(myOutputParamString.Value);
                        return outres;
                    }

                }
            }
        }

        public string SavePermissionInfo(RequestData request, PermissionMasterData model, int usercode)
        {
            string outres = string.Empty;
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    string code = (from record in cookBookCommon.PermissionMasters orderby record.ID descending select record.Code).First();
                    if (string.IsNullOrEmpty(code))
                    {
                        code = "1";
                        System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                        cookBookCommon.procSavePermissionData(code, model.typ, model.Description, usercode, myOutputParamString).ToString();
                        outres = Convert.ToString(myOutputParamString.Value);
                        return outres;
                    }
                    else
                    {
                        code = (Convert.ToInt32(code) + 1).ToString();
                        model.Code = code;
                        System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                        cookBookCommon.procSavePermissionData(code, model.typ, model.Description, usercode, myOutputParamString).ToString();
                        outres = Convert.ToString(myOutputParamString.Value);
                        return outres;
                    }

                }
            }
        }


        public string SaveCapringInfo(RequestData request, CapringMastre model, int usercode)
        {
            string outres = string.Empty;
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    
                        System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                        cookBookCommon.ProcSaveCapringInfo(model.CAPRINGCODE, model.Description,  usercode,model.SectorCode,Convert.ToInt32(model.LevelCode),model.FunctionCode, Convert.ToInt32(model.PermissionCode), myOutputParamString).ToString();
                        outres = Convert.ToString(myOutputParamString.Value);
                        return outres;
                    
                }
            }
        }

        public string UpdateCapringInfo(RequestData request, CapringMastre model, int usercode)
        {
            string outres = string.Empty;
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                   
                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    cookBookCommon.ProcUpdateCapringInfo(model.ID,model.CAPRINGCODE, model.Description,  usercode, model.SectorCode,Convert.ToInt32(model.LevelCode), model.FunctionCode, Convert.ToInt32(model.PermissionCode), myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;

                }
            }
        }

        public string UpdateCapringDescriptionInfo(RequestData request, CapringMastre model, int usercode)
        {
            string outres = string.Empty;
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {

                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    cookBookCommon.ProcUpdateCapringDescriptionInfo(model.ID, model.CAPRINGCODE, model.Description, usercode, model.SectorCode, Convert.ToInt32(model.LevelCode), model.FunctionCode,Convert.ToInt32(model.PermissionCode), myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;

                }
            }
        }


        public string UpdatePermissionInfo(RequestData request, PermissionMasterData model, int usercode)
        {
            string outres = string.Empty;
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                   
                        System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                        cookBookCommon.procUpdatePermissionData(model.ID, model.typ, model.Description, usercode, myOutputParamString).ToString();
                        outres = Convert.ToString(myOutputParamString.Value);
                        return outres;
                    

                }
            }
        }

        public string UpdateFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode)
        {
            string outres = string.Empty;
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                       
                        System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                        cookBookCommon.procUpdatefunctionData(model.ID, model.Name, model.Description, model.Remarks, usercode, myOutputParamString).ToString();
                        outres = Convert.ToString(myOutputParamString.Value);
                        return outres;
                    

                }
            }
        }

        public string UpdatefunctionInactiveActive(RequestData request, FunctionMasterData model, int usercode)
        {
            string outres = string.Empty;
            bool actinac;
            if (model.IsActive == false)
            {
                actinac = Convert.ToBoolean(0);
            }
            else
            {
                actinac = Convert.ToBoolean(1);
            }
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {

                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    cookBookCommon.procUpdatefunctionActiveInactiveData(model.ID, actinac, usercode, myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;


                }
            }
        }


        public string UpdatePermissionInactiveActive(RequestData request, PermissionMasterData model, int usercode)
        {
            string outres = string.Empty;
            bool actinac;
            if (model.IsActive == false)
            {
                actinac = Convert.ToBoolean(0);
            }
            else
            {
                actinac = Convert.ToBoolean(1);
            }
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {

                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    cookBookCommon.procUpdatePermissionActiveInactive(model.ID, actinac, usercode, myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;


                }
            }
        }


        public string UpdateManageLevelInfo(RequestData request, LevelMasterData model, int usercode)
        {
            string outres = string.Empty;
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                        System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                        cookBookCommon.procUpdateLevelData(model.id, model.Name, model.Description, usercode, myOutputParamString).ToString();
                        outres = Convert.ToString(myOutputParamString.Value);
                        return outres;
                    

                }
            }
        }



        public string UpdateManageSectorInfo(RequestData request, ManageSectorData model, int usercode)
        {
            string outres = string.Empty;
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    cookBookCommon.procUpdateSactorData(model.id, model.SectorNumber, model.SectorName, model.SectorCode, usercode, myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;
                }
            }
        }


        public string UpdateInactiveActive(RequestData request, ManageSectorData model, int usercode)
        {
            string outres = string.Empty;
            bool actinac;
            if(model.IsActive==false)
            {
                actinac = Convert.ToBoolean(0);
            }
            else
            {
                actinac = Convert.ToBoolean(1);
            }
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    cookBookCommon.procUpdateSactorActiveInactiveData(model.id,actinac, usercode, myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;
                }
            }
        }

        public string UpdateLevelInactiveActive(RequestData request, LevelMasterData model, int usercode)
        {
            string outres = string.Empty;
            bool actinac;
            if (model.IsActive ==false)
            {
                actinac = Convert.ToBoolean(0);
            }
            else
            {
                actinac = Convert.ToBoolean(1);
            }
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    cookBookCommon.procUpdateLevelActiveInactiveData(model.id, actinac, usercode, myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;
                }
            }
        }


        public string UpdateCapringInactiveActive(RequestData request, CapringMastre model, int usercode)
        {
            string outres = string.Empty;
            bool actinac;
            if (model.IsActive == false)
            {
                actinac = Convert.ToBoolean(0);
            }
            else
            {
                actinac = Convert.ToBoolean(1);
            }
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    cookBookCommon.procUpdateCapringActiveInactiveData(model.ID, actinac, usercode, myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;
                }
            }
        }


        public List<procGetLevelMasterData_Result> GetLevelDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetLevelMasterData().ToList();
                }
            }
        }


        public List<procGetCapringMasterData_Result> GetCapringMasterList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetCapringMasterData().ToList();
                }
            }
        }


        public List<procGetFunctionalityData_Result> GetFunctionDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetFunctionalityData().ToList();
                }
            }
        }


        public List<procGetPermissionData_Result> GetPermissionDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetPermissionData().ToList();
                }
            }
        }


        protected override SectorMaster FindImpl(int key, DbSet<SectorMaster> dbSet)
        {
            throw new NotImplementedException();
        }

        protected override SectorMaster FindImplWithExpand(int key, DbSet<SectorMaster> dbSet, string includeQueryPath)
        {
            throw new NotImplementedException();
        }

        protected override DbSet<SectorMaster> GetDbSet(CookBookCommonEntities entityContext)
        {
            throw new NotImplementedException();
        }
    }
}
