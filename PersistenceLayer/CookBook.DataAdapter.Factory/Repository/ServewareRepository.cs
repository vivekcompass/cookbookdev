﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IServewareRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ServewareRepository : EFSynchronousRepository<int, Serveware, CookBookCommonEntities>,
        IServewareRepository
    {
        protected override DbSet<Serveware> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.Servewares;
        }

        protected override Serveware FindImpl(int key, DbSet<Serveware> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override Serveware FindImplWithExpand(int key, DbSet<Serveware> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
