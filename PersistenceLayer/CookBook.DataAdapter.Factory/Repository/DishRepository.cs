﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using CookBook.Data.UserData;
using CookBook.DataAdapter.Base.DataEnums;
using CookBook.Data.Request;
using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base.EF;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IDishRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DishRepository : EFSynchronousRepository<int, Dish, CookBookCoreEntities>,
        IDishRepository
    {
        protected override DbSet<Dish> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.Dishes;
        }

        protected override Dish FindImpl(int key, DbSet<Dish> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override Dish FindImplWithExpand(int key, DbSet<Dish> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetDishList_Result> GetDishDataList(int? foodProgramID, int? dietCategoryID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetDishList(foodProgramID, dietCategoryID, requestData.SessionSectorNumber, requestData.SiteCode).ToList();
                }
            }
        }
        //Krish 25-10-2022
        //public IList<procGetSectorDishWithRangeType_Result> GetDishDataRangeTypeList(RequestData requestData)
        //{
        //    using (var transaction = new EFTransaction())
        //    {
        //        using (var context = GetContext(transaction, requestData.SessionSectorName))
        //        {
        //            return context.procGetSectorDishWithRangeType().ToList();
        //        }
        //    }
        //}

        public IList<procGetSiteDishList_Result> GetSiteDishDataList(string siteCode, int? foodProgramID, int? dietCategoryID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetSiteDishList(siteCode, foodProgramID, dietCategoryID).ToList();
                }
            }
        }
        public IList<procGetRegionDishMasterData_Result> GetRegionDishMasterData(int RegionID, int? foodProgramID, int? dietCategoryID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetRegionDishMasterData(RegionID, foodProgramID, dietCategoryID).ToList();
                }
            }
        }

        public IList<procGetRegionDishList_Result> GetRegionDishDataList(int RegionID, int? foodProgramID, int? dietCategoryID, string subSectorCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetRegionDishList(RegionID, foodProgramID, dietCategoryID, subSectorCode, requestData.SessionSectorNumber).ToList();
                }
            }
        }
        public string SaveList(IList<Dish> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    foreach (Dish m in model)
                    {
                        context.Dishes.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<DishCategory> GetDishCategory(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.DishCategories.ToList();
                }
            }
        }

        public string SaveDishData(DishData model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var existinghList = context.DishHealthTagMappings.Where(item => item.DishCode == model.DishCode);
                    foreach (DishHealthTagMapping item in existinghList)
                    {
                        item.ModifiedOn = DateTime.Now;
                        item.ModifiedBy = model.CreatedBy;
                        item.IsActive = false;
                        context.DishHealthTagMappings.AddOrUpdate(item);
                    }
                    if (!string.IsNullOrWhiteSpace(model.HealthTagCode))
                    {
                        var dataArray = model.HealthTagCode.Split(',');

                        foreach (var health in dataArray)
                        {
                            var dishHealthTagMapping = new DishHealthTagMapping();
                            dishHealthTagMapping.HealthTagCode = health;
                            dishHealthTagMapping.DishCode = model.DishCode;
                            dishHealthTagMapping.IsActive = true;
                            var isItemExists = context.DishHealthTagMappings.Where(item => item.HealthTagCode == health && item.DishCode == model.DishCode).FirstOrDefault();
                            if (isItemExists != null && isItemExists.ID > 0)
                            {
                                dishHealthTagMapping.ModifiedOn = DateTime.Now;
                                dishHealthTagMapping.ModifiedBy = model.CreatedBy;
                                dishHealthTagMapping.ID = isItemExists.ID;
                            }
                            else
                            {
                                dishHealthTagMapping.CreatedOn = DateTime.Now;
                                dishHealthTagMapping.CreatedBy = model.CreatedBy;
                                dishHealthTagMapping.ID = 0;
                            }
                            context.DishHealthTagMappings.AddOrUpdate(dishHealthTagMapping);
                        }

                    }
                    else
                    {
                        //var existinghList = context.DishHealthTagMappings.Where(item => item.DishCode == model.DishCode);
                        //foreach (DishHealthTagMapping item in existinghList)
                        //{
                        //    item.ModifiedOn = DateTime.Now;
                        //    item.ModifiedBy = model.CreatedBy;
                        //    item.IsActive = false;
                        //    context.DishHealthTagMappings.AddOrUpdate(item);
                        //}
                    }

                    if (!string.IsNullOrWhiteSpace(model.LifeStyleTagCode))
                    {
                        var dataArray = model.LifeStyleTagCode.Split(',');

                        var existinglList = context.DishLifeStyleTagMappings.Where(item => item.DishCode == model.DishCode);
                        foreach (DishLifeStyleTagMapping item in existinglList)
                        {
                            item.ModifiedOn = DateTime.Now;
                            item.ModifiedBy = model.CreatedBy;
                            item.IsActive = false;
                            context.DishLifeStyleTagMappings.AddOrUpdate(item);
                        }
                        foreach (var health in dataArray)
                        {
                            var dishLifeStyleTagMapping = new DishLifeStyleTagMapping();
                            dishLifeStyleTagMapping.LifeStyleTagCode = health;
                            dishLifeStyleTagMapping.DishCode = model.DishCode;
                            dishLifeStyleTagMapping.IsActive = true;
                            var isItemExists = context.DishLifeStyleTagMappings.Where(item => item.LifeStyleTagCode == health && item.DishCode == model.DishCode).FirstOrDefault();
                            if (isItemExists != null && isItemExists.ID > 0)
                            {
                                dishLifeStyleTagMapping.ModifiedOn = DateTime.Now;
                                dishLifeStyleTagMapping.ModifiedBy = model.CreatedBy;
                                dishLifeStyleTagMapping.ID = isItemExists.ID;
                            }
                            else
                            {
                                dishLifeStyleTagMapping.CreatedOn = DateTime.Now;
                                dishLifeStyleTagMapping.CreatedBy = model.CreatedBy;
                                dishLifeStyleTagMapping.ID = 0;
                            }
                            context.DishLifeStyleTagMappings.AddOrUpdate(dishLifeStyleTagMapping);
                        }
                        context.SaveChanges();
                    }
                    else
                    {

                        //var existinglList = context.DishLifeStyleTagMappings.Where(item => item.DishCode == model.DishCode);
                        //foreach (DishLifeStyleTagMapping item in existinglList)
                        //{
                        //    item.ModifiedOn = DateTime.Now;
                        //    item.ModifiedBy = model.CreatedBy;
                        //    item.IsActive = false;
                        //    context.DishLifeStyleTagMappings.AddOrUpdate(item);

                        //}
                    }


                    if (!string.IsNullOrWhiteSpace(model.DietTypeCode))
                    {
                        var dataArray = model.DietTypeCode.Split(',');

                        var existinglList = context.DishDietTypeMappings.Where(item => item.DishCode == model.DishCode);
                        foreach (DishDietTypeMapping item in existinglList)
                        {
                            item.ModifiedOn = DateTime.Now;
                            item.ModifiedBy = model.CreatedBy;
                            item.IsActive = false;
                            context.DishDietTypeMappings.AddOrUpdate(item);
                        }
                        foreach (var health in dataArray)
                        {
                            var dishDietTypeMapping = new DishDietTypeMapping();
                            dishDietTypeMapping.DietTypeCode = health;
                            dishDietTypeMapping.DishCode = model.DishCode;
                            dishDietTypeMapping.IsActive = true;
                            var isItemExists = context.DishDietTypeMappings.Where(item => item.DietTypeCode == health && item.DishCode == model.DishCode).FirstOrDefault();
                            if (isItemExists != null && isItemExists.ID > 0)
                            {
                                dishDietTypeMapping.ModifiedOn = DateTime.Now;
                                dishDietTypeMapping.ModifiedBy = model.CreatedBy;
                                dishDietTypeMapping.ID = isItemExists.ID;
                            }
                            else
                            {
                                dishDietTypeMapping.CreatedOn = DateTime.Now;
                                dishDietTypeMapping.CreatedBy = model.CreatedBy;
                                dishDietTypeMapping.ID = 0;
                            }
                            context.DishDietTypeMappings.AddOrUpdate(dishDietTypeMapping);
                        }
                        context.SaveChanges();
                    }
                    else
                    {

                        //var existinglList = context.DishDietTypeMappings.Where(item => item.DishCode == model.DishCode);
                        //foreach (DishDietTypeMapping item in existinglList)
                        //{
                        //    item.ModifiedOn = DateTime.Now;
                        //    item.ModifiedBy = model.CreatedBy;
                        //    item.IsActive = false;
                        //    context.DishDietTypeMappings.AddOrUpdate(item);

                        //}
                    }

                    if (!string.IsNullOrWhiteSpace(model.PatientBillingProfileCode))
                    {
                        var dataArray = model.PatientBillingProfileCode.Split(',');

                        var existinglList = context.DishPatientBillingProfileMappings.Where(item => item.DishCode == model.DishCode);
                        foreach (DishPatientBillingProfileMapping item in existinglList)
                        {
                            item.ModifiedOn = DateTime.Now;
                            item.ModifiedBy = model.CreatedBy;
                            item.IsActive = false;
                            context.DishPatientBillingProfileMappings.AddOrUpdate(item);
                        }
                        foreach (var health in dataArray)
                        {
                            var dishMappingObj = new DishPatientBillingProfileMapping();
                            dishMappingObj.PatientBillingProfileCode = health;
                            dishMappingObj.DishCode = model.DishCode;
                            dishMappingObj.IsActive = true;
                            var isItemExists = context.DishPatientBillingProfileMappings.Where(item => item.PatientBillingProfileCode
                            == health && item.DishCode == model.DishCode).FirstOrDefault();
                            if (isItemExists != null && isItemExists.ID > 0)
                            {
                                dishMappingObj.ModifiedOn = DateTime.Now;
                                dishMappingObj.ModifiedBy = model.CreatedBy;
                                dishMappingObj.ID = isItemExists.ID;
                            }
                            else
                            {
                                dishMappingObj.CreatedOn = DateTime.Now;
                                dishMappingObj.CreatedBy = model.CreatedBy;
                                dishMappingObj.ID = 0;
                            }
                            context.DishPatientBillingProfileMappings.AddOrUpdate(dishMappingObj);
                        }
                        context.SaveChanges();
                    }
                    context.SaveChanges();

                  
                }
            }

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.procPushSectortoALLRegionWithMapping(model.DishCode, model.CreatedBy, "");
                    context.SaveChanges();
                }
            }
            return "true";
        }

        public IList<PlanningTag> GetPlanningTag(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.PlanningTags.ToList();
                }
            }
        }

        public string SavePlanningTagData(PlanningTagData model, RequestData requestData)
        {
            string res = string.Empty;
            string sector = requestData.SessionSectorNumber;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    if (!string.IsNullOrEmpty(model.TagCode))
                    {
                        var getdata = context.PlanningTags.Where(m => m.TagCode == model.TagCode).FirstOrDefault();
                        if (getdata != null && getdata.ID > 0)
                        {
                            getdata.TagName = model.TagName;
                            getdata.TagCode = model.TagCode;
                            getdata.TagDescription = model.TagDescription;
                            getdata.TagDisplayCode = model.TagDisplayCode;
                            getdata.ModifiedBy = requestData.SessionUserId;
                            getdata.ModifiedOn = DateTime.Now;
                            getdata.IsActive = model.IsActive;
                            context.PlanningTags.AddOrUpdate(getdata);
                        }
                    }
                    else
                    {
                        string code = (from record in context.PlanningTags orderby record.ID descending select record.TagCode).FirstOrDefault();
                        if (string.IsNullOrEmpty(code))
                        {
                            code = "PLT-00001";

                        }
                        else
                        {
                            var lcode = code.Substring(0, 3);
                            var rcode = code.Substring(4, 5);
                            code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(5, '0'); ;
                        }
                        var dataModel = new PlanningTag();
                        dataModel.TagCode = code;
                        dataModel.TagName = model.TagName;
                        dataModel.TagDisplayCode = model.TagDisplayCode;
                        dataModel.TagDescription = model.TagDescription;
                        dataModel.IsActive = true;
                        dataModel.CreatedBy = requestData.SessionUserId;
                        dataModel.CreatedOn = DateTime.Now;
                        context.PlanningTags.AddOrUpdate(dataModel);
                    }
                    context.SaveChanges();
                }
            }
            return "Success";
        }

        //Krish 02-08-2022
        public SectorDishData GetSectorDishData(RequestData requestData, string condition, bool isAddEdit, bool onLoad)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    if (onLoad)
                    {
                        var Results = MultipleResultSets
                          .MultipleResults(context, $"EXEC procGetSectorDishDataOnLoad '{requestData.SessionSectorNumber}'")
                          .AddResult<ApplicationSettingData>()
                          .AddResult<DietCategoryData>()
                          .AddResult<DishData>()
                          .Execute();


                        var Output = new SectorDishData
                        {
                            ApplicationSettingData = Results[0] as ApplicationSettingData[],
                            DietCategoryData = Results[1] as DietCategoryData[],
                            DishData = Results[2] as DishData[]

                        };
                        return Output;
                    }

                    if (isAddEdit)
                    {
                        var Output = new SectorDishData();
                        if (requestData.SessionSectorNumber == "10")
                        {
                            if (requestData.SessionUserRoleId == 13)
                            {
                                var Results = MultipleResultSets
                                 .MultipleResults(context, $"EXEC procGetSectorDishDataAddEdit '{requestData.SessionSectorNumber}',{requestData.SessionUserRoleId}")
                                 .AddResult<CuisineMasterData>()
                                 .AddResult<SubSectorData>()
                                 .AddResult<HealthTagMasterData>()
                                 .AddResult<LifeStyleTagMasterData>()
                                 .AddResult<DietTypeData>()
                                 .AddResult<ColorData>()
                                 //.AddResult<DietCategoryData>()
                                 .AddResult<DishCategoryData>()
                                 .AddResult<UOMModuleMappingData>()
                                 .AddResult<DishTypeData>()
                                 .AddResult<TextureData>()
                                 .AddResult<ServingTemperatureData>()
                                 .Execute();
                                Output = new SectorDishData
                                {
                                    CuisineMasterData = Results[0] as CuisineMasterData[],
                                    SubSectorData = Results[1] as SubSectorData[],
                                    HealthTagMasterData = Results[2] as HealthTagMasterData[],
                                    LifeStyleTagMasterData = Results[3] as LifeStyleTagMasterData[],
                                    DietTypeData = Results[4] as DietTypeData[],
                                    ColorData = Results[5] as ColorData[],
                                    //DietCategoryData = Results[6] as DietCategoryData[],
                                    DishCategoryData = Results[6] as DishCategoryData[],
                                    UOMModuleMappingData = Results[7] as UOMModuleMappingData[],
                                    DishTypeData = Results[8] as DishTypeData[],
                                    TextureData = Results[9] as TextureData[],
                                    ServingTemperatureData = Results[10] as ServingTemperatureData[]
                                };
                            }
                            else
                            {
                                var Results = MultipleResultSets
                                 .MultipleResults(context, $"EXEC procGetSectorDishDataAddEdit '{requestData.SessionSectorNumber}',{requestData.SessionUserRoleId}")
                                 .AddResult<CuisineMasterData>()
                                 .AddResult<SubSectorData>()

                                 .AddResult<ColorData>()
                                 //.AddResult<DietCategoryData>()
                                 .AddResult<DishCategoryData>()
                                 .AddResult<UOMModuleMappingData>()
                                 .AddResult<DishTypeData>()
                                 .AddResult<TextureData>()
                                 .AddResult<ServingTemperatureData>()
                                 .Execute();
                                Output = new SectorDishData
                                {
                                    CuisineMasterData = Results[0] as CuisineMasterData[],
                                    SubSectorData = Results[1] as SubSectorData[],

                                    ColorData = Results[2] as ColorData[],
                                    //DietCategoryData = Results[3] as DietCategoryData[],
                                    DishCategoryData = Results[3] as DishCategoryData[],
                                    UOMModuleMappingData = Results[4] as UOMModuleMappingData[],
                                    DishTypeData = Results[5] as DishTypeData[],
                                    TextureData = Results[6] as TextureData[],
                                    ServingTemperatureData = Results[7] as ServingTemperatureData[]
                                };
                            }
                        }
                        else
                        {
                            if (requestData.SessionUserRoleId == 13)
                            {
                                var Results = MultipleResultSets
                                 .MultipleResults(context, $"EXEC procGetSectorDishDataAddEdit '{requestData.SessionSectorNumber}',{requestData.SessionUserRoleId}")

                                 .AddResult<HealthTagMasterData>()
                                 .AddResult<LifeStyleTagMasterData>()
                                 .AddResult<DietTypeData>()
                                 .AddResult<ColorData>()
                                 //.AddResult<DietCategoryData>()
                                 .AddResult<DishCategoryData>()
                                 .AddResult<UOMModuleMappingData>()
                                 .AddResult<DishTypeData>()
                                 .AddResult<TextureData>()
                                 .AddResult<ServingTemperatureData>()
                                 .Execute();
                                Output = new SectorDishData
                                {

                                    HealthTagMasterData = Results[0] as HealthTagMasterData[],
                                    LifeStyleTagMasterData = Results[1] as LifeStyleTagMasterData[],
                                    DietTypeData = Results[2] as DietTypeData[],
                                    ColorData = Results[3] as ColorData[],
                                    //DietCategoryData = Results[4] as DietCategoryData[],
                                    DishCategoryData = Results[4] as DishCategoryData[],
                                    UOMModuleMappingData = Results[5] as UOMModuleMappingData[],
                                    DishTypeData = Results[6] as DishTypeData[],
                                    TextureData = Results[7] as TextureData[],
                                    ServingTemperatureData = Results[8] as ServingTemperatureData[]
                                };
                            }
                            else
                            {
                                var Results = MultipleResultSets
                                 .MultipleResults(context, $"EXEC procGetSectorDishDataAddEdit '{requestData.SessionSectorNumber}',{requestData.SessionUserRoleId}")

                                 .AddResult<ColorData>()
                                 //.AddResult<DietCategoryData>()
                                 .AddResult<DishCategoryData>()
                                 .AddResult<UOMModuleMappingData>()
                                 .AddResult<DishTypeData>()
                                 .AddResult<TextureData>()
                                 .AddResult<ServingTemperatureData>()
                                 .Execute();
                                Output = new SectorDishData
                                {

                                    ColorData = Results[0] as ColorData[],
                                    // DietCategoryData = Results[1] as DietCategoryData[],
                                    DishCategoryData = Results[1] as DishCategoryData[],
                                    UOMModuleMappingData = Results[2] as UOMModuleMappingData[],
                                    DishTypeData = Results[3] as DishTypeData[],
                                    TextureData = Results[4] as TextureData[],
                                    ServingTemperatureData = Results[5] as ServingTemperatureData[]
                                };
                            }
                        }
                        return Output;
                    }

                    if (!string.IsNullOrEmpty(requestData.DishCode))
                    {
                        var Results = MultipleResultSets
                              .MultipleResults(context, $"EXEC procGetSectorDishDataOnMapRecipe '{condition}','{requestData.DishCode}','{requestData.SessionSectorNumber}'")
                              .AddResult<RecipeData>()
                              .AddResult<DishRecipeMappingData>()

                              .Execute();


                        var Output = new SectorDishData
                        {
                            RecipeData = Results[0] as RecipeData[],
                            DishRecipeMappingData = Results[1] as DishRecipeMappingData[],

                        };
                        return Output;
                    }

                    return new SectorDishData();
                }
            }
        }

        //Krish 08-10-2022
        public string SaveRangeTypeDishData(string dishCode, string rangeTypeCode,RequestData requestData)
        {
            string res = "true";
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    try
                    {
                        var getDish = context.Dishes.FirstOrDefault(a => a.DishCode == dishCode);
                        var rangeType = rangeTypeCode.Split(':');
                        getDish.CookingRangeTypeCode = rangeType[0];
                        getDish.ServingRangeTypeCode = rangeType[1];
                        context.SaveChanges();
                    }
                    catch(Exception ex) { res= "false"; }
                }
            }
            return res;
        }
    }
}
