﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(ISiteProfile3Repository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteProfile3Repository : EFSynchronousRepository<int, SiteProfile3, CookBookCommonEntities>,
        ISiteProfile3Repository
    {
        protected override DbSet<SiteProfile3> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.SiteProfile3;
        }

        protected override SiteProfile3 FindImpl(int key, DbSet<SiteProfile3> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SiteProfile3 FindImplWithExpand(int key, DbSet<SiteProfile3> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
