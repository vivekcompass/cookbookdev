﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IConceptType1Repository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ConceptType1Repository : EFSynchronousRepository<int, ConceptType1, CookBookCommonEntities>,
        IConceptType1Repository
    {
        protected override DbSet<ConceptType1> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.ConceptType1;
        }

        protected override ConceptType1 FindImpl(int key, DbSet<ConceptType1> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ConceptType1 FindImplWithExpand(int key, DbSet<ConceptType1> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
