﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Data.Request;
using CookBook.Data.MasterData;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IMOGRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MOGRepository : EFSynchronousRepository<int, MOG, CookBookCoreEntities>,
        IMOGRepository
    {
        protected override DbSet<MOG> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.MOGs;
        }

        protected override MOG FindImpl(int key, DbSet<MOG> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override MOG FindImplWithExpand(int key, DbSet<MOG> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetMOGList_Result> GetMOGDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetMOGList().ToList();
                }
            }
        }

        public IList<procGetInheritMOGList_Result> GetInheritedMOGDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetInheritMOGList().ToList();
                }
            }
        }

        public IList<procGetSiteMOGList_Result> GetSiteMOGDataList(RequestData requestData, string siteCode)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetSiteMOGList(siteCode).ToList();
                }
            }
        }

        public IList<procGetRegionMOGList_Result> GetRegionMOGDataList(RequestData requestData, int regionID)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetRegionMOGList(regionID, requestData.SessionSectorNumber).ToList();
                }
            }
        }

        public IList<procGetMOGImpactedBaseRecipes_Result> GetMOGImpactedBaseRecipiesDataList(int mogID, int recipeID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetMOGImpactedBaseRecipes(mogID, recipeID).ToList();
                }
            }
        }

        public IList<procGetMOGImpactedFinalRecipes_Result> GetMOGImpactedFinalRecipiesDataList(int mogID, int recipeID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetMOGImpactedFinalRecipes(mogID,recipeID).ToList();
                }
            }
        }

        public IList<procGetMOGImpactedDishes_Result> GetMOGImpactedDishesDataList(int mogID, int recipeID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetMOGImpactedDishes(mogID, recipeID).ToList();
                }
            }
        }

        public IList<procGetMOGImpactedItems_Result> GetMOGImpactedItemsDataList(int mogID, int recipeID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetMOGImpactedItems(mogID, recipeID).ToList();
                }
            }
        }

        public IList<procGetMOGImpactedSites_Result> GetMOGImpactedSitesDataList(int mogID, int recipeID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetMOGImpactedSites(mogID, recipeID).ToList();
                }
            }
        }

        public IList<procGetMOGImpactedRegion_Result> GetMOGImpactedRegionsDataList(int mogID, int recipeID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetMOGImpactedRegion(mogID, recipeID).ToList();
                }
            }
        }

        public IList<procGetMOGAPLImpactedDisheSector_Result> procGetMOGAPLImpactedDishSector(int mogID,string aplList ,int sectorID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetMOGAPLImpactedDisheSector(mogID, aplList,sectorID).ToList();
                }
            }
        }

        public IList<procGetMOGAPLImpactedDishRegion_Result> procGetMOGAPLImpactedDishRegion(int mogID, string aplList, int sectorID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetMOGAPLImpactedDishRegion(mogID, aplList, sectorID).ToList();
                }
            }
        }

        public IList<procGetMOGAPLImpactedDishSites_Result> procGetMOGAPLImpactedDishSite(int mogID, string aplList, int sectorID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetMOGAPLImpactedDishSites(mogID, aplList, sectorID).ToList();
                }
            }
        }

        public string SaveList(IList<MOG> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (MOG m in model)
                    {

                        context.MOGs.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<ProcGetExpiryProductData_Result> GetExpiryProductDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.ProcGetExpiryProductData().ToList();
                }
            }
        }

        public string SaveExpiryProductData(GetExpiryProductData model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.procSaveProductExpiryCategoryData(model.ProductCode,model.ProductType,model.ProductExpiryCode,requestData.SessionUserId);
                    return "true";
                }
            }
        }

        public IList<procGetMOGUOMDetails_Result> GetMOGUOMDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetMOGUOMDetails().ToList();
                }
            }
        }
        //Krish
        public IList<ProcGetExpiryProductData_Result> GetExpiryProductDataOnlyExpiryNameList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.ProcGetExpiryProductData().Where(d=>!string.IsNullOrEmpty(d.ProductExpiryName)).ToList();
                }
            }
        }
    }
}
