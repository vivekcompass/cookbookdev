﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.Data.MasterData;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Data.Entity.Migrations;
using CookBook.DataAdapter.Factory.Common;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IAPLMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class APLMasterRepositoy : EFSynchronousRepository<int, UniqueAPLMaster, CookBookCommonEntities>,
        IAPLMasterRepository
    {
        
        protected override DbSet<UniqueAPLMaster> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.UniqueAPLMasters;
        }

        protected override UniqueAPLMaster FindImpl(int key, DbSet<UniqueAPLMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ArticleID == key);
        }

        protected override UniqueAPLMaster FindImplWithExpand(int key, DbSet<UniqueAPLMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ArticleID == key);
        }

        public List<procGetAPLMasterData_Result> GetAPLMasterDataList(string articleNumber, string mogCode,RequestData requestData)
        {
            
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return cookBookCommon.procGetAPLMasterData(articleNumber, mogCode).ToList();
                }
            }
        }

        public List<procGetAPLMasterDataforSector_Result> GetAPLMasterDataListForSector(string articleNumber, string mogCode, RequestData requestData)
        {

            CookBookCoreEntities cookBookCoreEntities = new CookBookCoreEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return cookBookCoreEntities.procGetAPLMasterDataforSector(articleNumber, mogCode,requestData.SessionSectorNumber).ToList();
                }
            }
        }

        public List<procNationalGetAPLMasterData_Result> NationalGetAPLMasterDataList(RequestData requestData)
        {

            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return cookBookCommon.procNationalGetAPLMasterData().ToList();
                }
            }
        }

        public List<procGetAPLsPageWiseDataList_Result> GetAPLMasterDataListPageing(RequestData requestData, int pageindex)
        {

            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return cookBookCommon.procGetAPLsPageWiseDataList(pageindex).ToList();
                }
            }
        }


        public List<procGetNationalAPLMaster_Result> GetNationalAPLExcelList(RequestData requestData)
        {

            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return cookBookCommon.procGetNationalAPLMaster().ToList();
                }
            }
        }

        public List<ProcShowNationalDishCategory_Result> NationalGetDishCategoryDataList(RequestData requestData)
        {

            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return cookBookCommon.ProcShowNationalDishCategory().ToList();
                }
            }
        }

        public List<ProcShowNationalDishes_Result> NationalGetDishesDataList(RequestData requestData)
        {

            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return cookBookCommon.ProcShowNationalDishes().ToList();
                }
            }
        }

        public string UpdateMOGLISTAPL(RequestData request, string xml, string username)
        {
            string outres = string.Empty;
            
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    //return coreEntities.ProgBulkUpdateXMLMOGAPL(xml, username).ToList();
                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    cookBookCommon.ProgBulkUpdateXMLMOGAPL(xml, username, myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;
                }
            }
        }


        public List<procshowMOGAPLUPLOADDETAILSAPL_Result> GetMOGAPLHISTORYDetAPL(RequestData request, string username)
        {
           
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    return context.procshowMOGAPLUPLOADDETAILSAPL(username).ToList();
                }
            }
        }
        //public List<procCuisineMasterList_Result> GetCuisineMaster(RequestData request)
        //{

        //    using (var transaction = new EFTransaction())
        //    {
        //        using (var context = GetContext(transaction, request.SessionSectorName))
        //        {
        //            return context.procCuisineMasterList().ToList();
        //        }
        //    }
        //}
        public List<ProcGetMOGBATCHDETAILSAPL_Result> GetMOGAPLHISTORYAPL(RequestData request, string username)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    return context.ProcGetMOGBATCHDETAILSAPL(username).ToList();
                }
            }
        }
        public List<procshowGetALLHISTORYBATCHWISEAPL_Result> GetshowGetALLHISTORYBATCHWISEAPL(RequestData request, string username,string batchno)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    return context.procshowGetALLHISTORYBATCHWISEAPL(username,batchno).ToList();
                }
            }
        }

        public List<procshowGetALLHISTORYAPL_Result> GetALLHISTORYDetailsAPL(RequestData request, string username)
        {

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, request.SessionSectorName))
                {
                    return context.procshowGetALLHISTORYAPL(username).ToList();
                }
            }
        }

        public IList<procGetAPLMasterMOGCode_Result> GetNOTMappedMOGDataListAPL(RequestData requestData)
        {
           // CookBookCoreEntities coreEntities = new CookBookCoreEntities();
            CookBookCommonEntities cookBookCommon = new CookBookCommonEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetAPLMasterMOGCode().ToList();
                }
            }
        }


       

        public string SaveAPLMasterDataList(IList<UniqueAPLMaster> model, int mogStatus, RequestData requestData)
        {
            //// CookBookCoreEntities coreEntities = new CookBookCoreEntities();
            // if (mogStatus > 0)
            // {
            //     using (var transaction = new EFTransaction())
            //     {
            //         using (var context = GetContext(transaction,requestData.SessionSectorName))
            //         {
            //             var mogCode = model[0].MOGCode;
            //             var mogObj = context.NationalMOGs.Where(m => m.MOGCode == mogCode).FirstOrDefault();
            //             mogObj.ModifiedBy = model[0].ModifiedBy;
            //             mogObj.ModifiedOn = model[0].ModifiedDate;
            //             mogObj.Status = mogStatus;
            //             context.NationalMOGs.AddOrUpdate(mogObj);
            //             context.SaveChanges();
            //         }
            //     }
            // }

           

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (UniqueAPLMaster m in model)
                    {
                        m.ModifiedDate = DateTime.Now;
                        context.UniqueAPLMasters.AddOrUpdate(m);
                    }

                    context.SaveChanges();

                    context.ProcUpdateNationalMOGStatus();
                    context.SaveChanges();

                    return "true";
                }
            }

        }

        public string SaveAPLMasterData(UniqueAPLMaster model, int mogStatus,string allergenCodes ,RequestData requestData)
        {
            // CookBookCoreEntities coreEntities = new CookBookCoreEntities();
            if (mogStatus > 0)
            {
                using (var transaction = new EFTransaction())
                {
                    using (var context = GetContext(transaction, requestData.SessionSectorName))
                    {
                        var mogCode = model.MOGCode;
                        var mogObj = context.NationalMOGs.Where(m => m.MOGCode == mogCode).FirstOrDefault();
                        mogObj.ModifiedBy = model.ModifiedBy;
                        mogObj.ModifiedOn = model.ModifiedDate;
                        mogObj.Status = mogStatus;
                        context.NationalMOGs.AddOrUpdate(mogObj);
                        context.SaveChanges();
                    }
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    model.ModifiedDate = DateTime.Now;
                    context.UniqueAPLMasters.AddOrUpdate(model);
                    context.SaveChanges();

                    if (!string.IsNullOrWhiteSpace(allergenCodes)){
                        var allergenArray = allergenCodes.Split(',');
                        var existingList = context.AllergenAPLMappings.Where(item => item.ArticleNumber == model.ArticleNumber);
                        foreach (AllergenAPLMapping allergen in existingList)
                        {
                            allergen.ModifiedOn = DateTime.Now;
                            allergen.ModifiedBy = model.CreatedBy;
                            allergen.IsActive = false;
                            context.AllergenAPLMappings.AddOrUpdate(allergen);
                        }

                        foreach (var allergen in allergenArray)
                        {
                            var allergenAPLMapping = new AllergenAPLMapping();
                            allergenAPLMapping.AllergenCode = allergen;
                            allergenAPLMapping.ArticleNumber = model.ArticleNumber;
                            allergenAPLMapping.IsActive = true;
                            var isItemExists = context.AllergenAPLMappings.Where(item => item.AllergenCode == allergen && item.ArticleNumber == model.ArticleNumber).FirstOrDefault();
                            if (isItemExists != null && isItemExists.ID > 0)
                            {
                                allergenAPLMapping.ModifiedOn = DateTime.Now;
                                allergenAPLMapping.ModifiedBy = model.CreatedBy;
                                allergenAPLMapping.ID = isItemExists.ID;
                            }
                            else
                            {
                                allergenAPLMapping.CreatedOn = DateTime.Now;
                                allergenAPLMapping.CreatedBy = model.CreatedBy;
                                allergenAPLMapping.ID = 0;
                            }
                            context.AllergenAPLMappings.AddOrUpdate(allergenAPLMapping);
                        }
                        context.SaveChanges();
                    }
                    return "true";
                }
            }
        }

        //Krish 08-10-2022
        public string SaveRangeTypeAPLData(string articleNumber, string rangeTypeCode, RequestData requestData)
        {
            string res = "true";
            CookBookCommonEntities commonEntities = new CookBookCommonEntities();
            //using (var transaction = new EFTransaction())
            //{
            //    using (var context = GetContext(transaction, requestData.SessionSectorName))
            //    {
                    try
                    {
                        var getAPL = commonEntities.UniqueAPLMasters.FirstOrDefault(a => a.ArticleNumber == articleNumber);
                        getAPL.RangeTypeCode = rangeTypeCode;
                        commonEntities.SaveChanges();
                    }
                    catch (Exception ex) { res = "false"; }
            //    }
            //}
            return res;
        }
    }

   
}