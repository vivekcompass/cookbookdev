﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.Request;
using System.Xml.Linq;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IRegionItemDishMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RegionItemDishMappingRepository : EFSynchronousRepository<int, RegionItemDishMapping, CookBookCoreEntities>,
        IRegionItemDishMappingRepository
    {
        protected override DbSet<RegionItemDishMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.RegionItemDishMappings;
        }

        protected override RegionItemDishMapping FindImpl(int key, DbSet<RegionItemDishMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override RegionItemDishMapping FindImplWithExpand(int key, DbSet<RegionItemDishMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<RegionItemDishMapping> GetRegionItemDishMappingDataList(string itemID,string regionID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
              //  int ItemID = int.Parse(itemID);
                int RegionID = int.Parse(regionID);
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    if (!string.IsNullOrWhiteSpace(requestData.SubSectorCode))
                    {
                        var list = from x in context.RegionItemDishMappings
                                   where x.ItemCode == itemID && x.Region_ID == RegionID && x.SubSectorCode == requestData.SubSectorCode && x.IsActive.Value
                                   select x;
                        return list.ToList();
                    }
                    else
                    {
                        var list = from x in context.RegionItemDishMappings
                                   where x.ItemCode == itemID && x.Region_ID == RegionID && x.IsActive.Value
                                   select x;
                        return list.ToList();
                    }
                    
                }
            }
        }

        public IList<RegionItemDishMapping> GetMasterRegionItemDishMappingDataList(string regionID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                
                int RegionID = int.Parse(regionID);
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = from x in context.RegionItemDishMappings
                               where x.Region_ID == RegionID && x.IsActive.Value
                               select x;
                    return list.ToList();
                }
            }
        }


        public string SaveRegionItemDishMappingList(IList<RegionItemDishMapping> model, int regionStatus, RequestData requestData)
        {
            string ItemCode = model[0].ItemCode;
            int RegionID = model[0].Region_ID == null ? 0 : model[0].Region_ID.Value;
            string subSectorCode = model[0].SubSectorCode;
            var xEle = new XElement("RegionItemDishMappings",
          from regionItemDishMapping in model
          select new XElement("RegionItemDishMapping",
                       new XAttribute("ID", regionItemDishMapping.ID),
                         new XElement("Item_ID", regionItemDishMapping.Item_ID),
                         new XElement("Dish_ID", regionItemDishMapping.Dish_ID),
                         new XElement("StandardPortion", regionItemDishMapping.StandardPortion),
                         new XElement("ContractPortion", regionItemDishMapping.ContractPortion),
                         new XElement("IsActive", regionItemDishMapping.IsActive),
                         new XElement("CreatedBy", regionItemDishMapping.CreatedBy),
                         new XElement("CreatedOn", regionItemDishMapping.CreatedOn),
                         new XElement("ModifiedBy", regionItemDishMapping.ModifiedBy),
                         new XElement("ModifiedOn", regionItemDishMapping.ModifiedOn),
                         new XElement("ItemCode", regionItemDishMapping.ItemCode),
                         new XElement("DishCode", regionItemDishMapping.DishCode),
                         new XElement("Region_ID", regionItemDishMapping.Region_ID),
                         new XElement("UOMCode", regionItemDishMapping.UOMCode),
                         new XElement("WeightPerUOM", regionItemDishMapping.WeightPerUOM),
                         new XElement("SubSectorCode", regionItemDishMapping.SubSectorCode)
                     ));


            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.procSaveRegionItemDishMapping(ItemCode, requestData.SessionUserId, regionStatus,RegionID, subSectorCode,xEle.ToString());
                    context.SaveChanges();
                }
            }
            return "true";
        }


        public string mydishcateinactiveupdate(IList<RegionDishCategoryMapping> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    foreach (RegionDishCategoryMapping m in model)
                    {

                        context.RegionDishCategoryMappings.AddOrUpdate(m);
                        
                    }

                    context.SaveChanges();
                }
            }

            
            return "true";
        }
    }
}
