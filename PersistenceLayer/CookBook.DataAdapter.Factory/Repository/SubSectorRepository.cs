﻿using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(ISubSectorRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SubSectorRepository : EFSynchronousRepository<int, SubSectorMaster, CookBookCoreEntities>,
        ISubSectorRepository
    {
        protected override DbSet<SubSectorMaster> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.SubSectorMasters;
        }

        protected override SubSectorMaster FindImpl(int key, DbSet<SubSectorMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SubSectorMaster FindImplWithExpand(int key, DbSet<SubSectorMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
