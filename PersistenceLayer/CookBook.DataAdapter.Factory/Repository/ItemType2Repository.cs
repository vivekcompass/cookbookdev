﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IItemType2Repository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ItemType2Repository : EFSynchronousRepository<int, ItemType2, CookBookCommonEntities>,
        IItemType2Repository
    {
        protected override DbSet<ItemType2> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.ItemType2;
        }

        protected override ItemType2 FindImpl(int key, DbSet<ItemType2> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ItemType2 FindImplWithExpand(int key, DbSet<ItemType2> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
