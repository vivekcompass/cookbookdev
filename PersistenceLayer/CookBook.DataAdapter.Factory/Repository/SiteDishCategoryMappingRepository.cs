﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(ISiteDishCategoryMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteDishCategoryMappingRepository : EFSynchronousRepository<int, SiteDishCategoryMapping, CookBookCoreEntities>,
        ISiteDishCategoryMappingRepository
    {
        protected override DbSet<SiteDishCategoryMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.SiteDishCategoryMappings;
        }

        protected override SiteDishCategoryMapping FindImpl(int key, DbSet<SiteDishCategoryMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SiteDishCategoryMapping FindImplWithExpand(int key, DbSet<SiteDishCategoryMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<SiteDishCategoryMapping> GetSiteDishCategoryMappingDataList(string SiteCode,string ItemCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                   // var ret = context.procUpdateCostForAllItemAtSite(ItemCode, SiteCode);
                    context.SaveChanges();

                    var list = from x in context.SiteDishCategoryMappings
                               where x.SiteCode == SiteCode && x.ItemCode == ItemCode //&& x.IsActive.Value
                               select x;
                             
                    return list.ToList();
                }
            }
        }
     
        public string SaveSiteDishCategoryMappingList(IList<SiteDishCategoryMapping> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                int SiteID = model[0].Site_ID;
                string ItemCode = model[0].ItemCode;
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    var list = from x in context.SiteDishCategoryMappings
                               where x.Site_ID == SiteID && x.IsActive.Value && x.ItemCode == ItemCode
                               select x;

                    IList<SiteDishCategoryMapping> llist = list.ToList();
                    bool flag = false;
                    foreach (SiteDishCategoryMapping m in llist)
                    {
                        flag = false;
                        foreach (SiteDishCategoryMapping mo in model)
                        {
                            if(mo.ID == m.ID)
                            {
                                flag = true;
                                break;
                            }
                        }
                        if (!flag)
                        {
                            m.IsActive = false;
                            m.ModifiedOn = DateTime.Now;
                            m.ModifiedBy = m.CreatedBy;
                            context.SiteDishCategoryMappings.AddOrUpdate(m);
                        }
                         
                    }

                    context.SaveChanges();
                }
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (SiteDishCategoryMapping m in model)
                    {
                       
                        var isItemExists = context.SiteDishCategoryMappings.Where(item => item.ItemCode == m.ItemCode && item.DishCategoryCode == m.DishCategoryCode
                                                                && item.Site_ID == m.Site_ID).FirstOrDefault();
                        if (isItemExists != null && isItemExists.ID > 0)
                        {
                            m.ID = isItemExists.ID;
                            m.ModifiedOn = DateTime.Now;
                            m.ModifiedBy = m.CreatedBy;
                        }
                        else
                        {
                            m.ID = 0;

                            m.CreatedOn = DateTime.Now;
                        }
                       // m.IsActive = true;
                        context.SiteDishCategoryMappings.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }
    
}
}
