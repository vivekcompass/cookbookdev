﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IUOMModuleMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UOMModuleMappingRepository : EFSynchronousRepository<int, UOMModuleMapping, CookBookCoreEntities>,
        IUOMModuleMappingRepository
    {
        protected override DbSet<UOMModuleMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.UOMModuleMappings;
        }

        protected override UOMModuleMapping FindImpl(int key, DbSet<UOMModuleMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override UOMModuleMapping FindImplWithExpand(int key, DbSet<UOMModuleMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetUOMModuleMappingList_Result> GetUOMModuleMappingModuleMappingDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetUOMModuleMappingList().ToList();
                }
            }
        }
    }
}
