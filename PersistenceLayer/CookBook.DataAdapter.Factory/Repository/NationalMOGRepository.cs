﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.Data.MasterData;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Data.Entity.Migrations;
using CookBook.DataAdapter.Factory.Common;
using CookBook.Data.Request;
using System.Data.Entity.Core.Objects;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(INationalMOGRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class NationalMOGRepositoy : EFSynchronousRepository<int, NationalMOG, CookBookCommonEntities>,
        INationalMOGRepository
    {
        
        protected override DbSet<NationalMOG> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.NationalMOGs;
        }

        protected override NationalMOG FindImpl(int key, DbSet<NationalMOG> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override NationalMOG FindImplWithExpand(int key, DbSet<NationalMOG> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetNationalMOGList_Result> GetNationalMOGDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetNationalMOGList().ToList();
                }
            }
        }

        public IList<procGetMogNotMappedList_Result> GetNOTMappedMOGDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetMogNotMappedList().ToList();
                }
            }
        }

        public List<ProcGetMOGBATCHDETAILS_Result> GetMOGAPLHISTORY(RequestData requestData, string username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.ProcGetMOGBATCHDETAILS(username).ToList();
                }
            }
        }

        public List<procshowMOGAPLUPLOADDETAILS_Result> GetMOGAPLHISTORYDet(RequestData requestData, string username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procshowMOGAPLUPLOADDETAILS(username).ToList();
                }
            }
        }

        public List<procshowGetALLHISTORYBATCHWISE_Result> GetshowGetALLHISTORYBATCHWISE(RequestData requestData, string username,string batchno)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procshowGetALLHISTORYBATCHWISE(username,batchno).ToList();
                }
            }
        }



        public List<procshowGetALLHISTORY_Result> GetALLHISTORYDetails(RequestData requestData, string username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procshowGetALLHISTORY(username).ToList();
                }
            }
        }

        public string UpdateMOGLIST(RequestData requestData, string xml, string username)
        {
            string res = string.Empty;
            string outres = string.Empty;

            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    System.Data.Entity.Core.Objects.ObjectParameter myOutputParamString = new System.Data.Entity.Core.Objects.ObjectParameter("result", typeof(string));
                    context.ProgBulkUpdateXMLMOG(xml, username, myOutputParamString).ToString();
                    outres = Convert.ToString(myOutputParamString.Value);
                    return outres;
                }
            }
        }




        public string SaveNationalMOGToSectors(NationalMOG model, string sectors, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    context.procSaveNationalMOGToSectors(model.CreatedBy, model.MOGCode, sectors);
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<procGetAPLMasterDataCommon_Result> GetAPLMasterDataListCommon(string articleNumber, string mogCode, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    return context.procGetAPLMasterDataCommon(articleNumber, mogCode).ToList();
                }
            }
        }

        public string SaveList(IList<NationalMOG> model, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction,requestData.SessionSectorName))
                {
                    foreach (NationalMOG m in model)
                    {

                        context.NationalMOGs.AddOrUpdate(m);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }
        public IList<procGetRegionsDetailsByUserId_Result> GetRegionsDetailsByUserIdDataList(RequestData requestData)
        {
            CookBookCoreEntities coreEntities = new CookBookCoreEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    return context.procGetRegionsDetailsByUserId(requestData.SessionUserId).ToList();
                }
            }
        }
        public IList<procGetSiteDetailsByUserId_Result> GetSiteDetailsByUserIdDataList(RequestData requestData)
        {
            CookBookCoreEntities coreEntities = new CookBookCoreEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    return context.procGetSiteDetailsByUserId(requestData.SessionUserId, Convert.ToInt32(requestData.SessionSectorNumber)).ToList();
                }
            }
        }

        public IList<procGetUserRBFAMatrixByUserID_Result> GetUserRBFAMatrixDataByUserId(RequestData requestData)
        {
            CookBookCoreEntities coreEntities = new CookBookCoreEntities();
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {

                    return context.procGetUserRBFAMatrixByUserID(requestData.SessionUserId).ToList();
                }
            }
        }
        public List<MogProcessTypeMapping> GetProcessTypeMogData(RequestData requestData, string mogCode)
        {
            CookBookCommonEntities commonEntities = new CookBookCommonEntities();
            var getList = commonEntities.MogProcessTypeMappings.Where(a => a.MOGCode == mogCode).ToList();
            return getList;
        }
        
        public string SaveProcessTypeMogData( RequestData requestData,List<string> processTypes,string mogCode,string rangeTypeCode,int inputStdDuration)
        {
            CookBookCommonEntities commonEntities = new CookBookCommonEntities();
            if (processTypes.Count > 0)
            {
                var chk = commonEntities.MogProcessTypeMappings.Where(a => a.MOGCode == mogCode).Count();
                var ismod = false;
                if (chk > 0)
                {
                    commonEntities.MogProcessTypeMappings.Where(a => a.MOGCode == mogCode).ToList().ForEach(a => commonEntities.MogProcessTypeMappings.Remove(a));
                    commonEntities.SaveChanges();
                    ismod = true;
                }
                foreach (var item in processTypes)
                {
                    if (ismod)
                    {
                        commonEntities.MogProcessTypeMappings.Add(new MogProcessTypeMapping
                        {
                            CreatedBy = requestData.SessionUserId,
                            CreatedDate = DateTime.Now,
                            ModifiedDate=DateTime.Now,
                            ModifiedBy= requestData.SessionUserId,
                            IsActive = true,
                            MOGCode = mogCode,
                            ProcessTypeCode = item,
                            RangeTypeCode = rangeTypeCode,
                            StandardDuration = inputStdDuration
                        });
                    }
                    else
                    {
                        commonEntities.MogProcessTypeMappings.Add(new MogProcessTypeMapping
                        {
                            CreatedBy = requestData.SessionUserId,
                            CreatedDate = DateTime.Now,
                            IsActive = true,
                            MOGCode = mogCode,
                            ProcessTypeCode = item,
                            RangeTypeCode = rangeTypeCode,
                            StandardDuration = inputStdDuration
                        });
                    }
                    commonEntities.SaveChanges();
                }
                //return "true";
            }
            return "true";
        }


        public string SaveAllergenMogData(RequestData requestData, List<string> contains, List<string> maycontains, string mogCode)
        {
            CookBookCommonEntities commonEntities = new CookBookCommonEntities();
           // commonEntities.procSaveNationalMOGToSectors(contains, maycontains, mogCode);
            return "true";
        }

    }
}
