﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(ISiteProfile2Repository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteProfile2Repository : EFSynchronousRepository<int, SiteProfile2, CookBookCommonEntities>,
        ISiteProfile2Repository
    {
        protected override DbSet<SiteProfile2> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.SiteProfile2;
        }

        protected override SiteProfile2 FindImpl(int key, DbSet<SiteProfile2> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SiteProfile2 FindImplWithExpand(int key, DbSet<SiteProfile2> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
