﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IItemType1Repository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ItemType1Repository : EFSynchronousRepository<int, ItemType1, CookBookCommonEntities>,
        IItemType1Repository
    {
        protected override DbSet<ItemType1> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.ItemType1;
        }

        protected override ItemType1 FindImpl(int key, DbSet<ItemType1> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ItemType1 FindImplWithExpand(int key, DbSet<ItemType1> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
