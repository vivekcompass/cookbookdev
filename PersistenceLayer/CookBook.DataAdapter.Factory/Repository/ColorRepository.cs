﻿using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Transaction;
namespace CookBook.DataAdapter.Factory.Repository.Transaction

{
    [Export(typeof(IColorRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ColorRepository : EFSynchronousRepository<int, Color, CookBookCoreEntities>,
        IColorRepository
    {
        protected override DbSet<Color> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.Colors;
        }

        protected override Color FindImpl(int key, DbSet<Color> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override Color FindImplWithExpand(int key, DbSet<Color> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
