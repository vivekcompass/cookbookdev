﻿using CookBook.Data.MasterData;
using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;
using System;
using CookBook.Data.Request;
using System.Xml.Linq;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IItemDishMappingRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ItemDishMappingRepository : EFSynchronousRepository<int, ItemDishMapping, CookBookCoreEntities>,
        IItemDishMappingRepository
    {
        protected override DbSet<ItemDishMapping> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.ItemDishMappings;
        }

        protected override ItemDishMapping FindImpl(int key, DbSet<ItemDishMapping> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override ItemDishMapping FindImplWithExpand(int key, DbSet<ItemDishMapping> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<ItemDishMapping> GetItemDishMappingDataList(string itemID, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                //int ItemID = int.Parse(itemID);
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    var list = from x in context.ItemDishMappings
                               where x.ItemCode == itemID && x.IsActive.Value
                               select x;

                    return list.ToList();
                }
            }
        }

        public string SaveList(IList<ItemDishMapping> model, int itemStatus, RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                string ItemCode = model[0].ItemCode;
                var xEle = new XElement("ItemDishMappings",
                 from regionItemDishMapping in model
                 select new XElement("ItemDishMapping",
                      new XAttribute("ID", regionItemDishMapping.ID),
                        new XElement("Item_ID", regionItemDishMapping.Item_ID),
                        new XElement("Dish_ID", regionItemDishMapping.Dish_ID),
                        new XElement("StandardPortion", regionItemDishMapping.StandardPortion),
                        new XElement("ContractPortion", regionItemDishMapping.ContractPortion),
                        new XElement("IsActive", regionItemDishMapping.IsActive),
                        new XElement("CreatedBy", regionItemDishMapping.CreatedBy),
                        new XElement("CreatedOn", regionItemDishMapping.CreatedOn),
                        new XElement("ModifiedBy", regionItemDishMapping.ModifiedBy),
                        new XElement("ModifiedOn", regionItemDishMapping.ModifiedOn),
                        new XElement("ItemCode", regionItemDishMapping.ItemCode),
                        new XElement("DishCode", regionItemDishMapping.DishCode),
                        new XElement("UOMCode", regionItemDishMapping.UOMCode),
                        new XElement("WeightPerUOM", regionItemDishMapping.WeightPerUOM))
                    );

                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    context.procSaveItemDishMapping(ItemCode, requestData.SessionUserId, itemStatus, xEle.ToString(), requestData.SessionSectorNumber);
                    context.SaveChanges();
                    return "true";
                }
            }
        }
    }
}
