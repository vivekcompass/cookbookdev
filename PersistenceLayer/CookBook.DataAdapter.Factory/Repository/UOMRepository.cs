﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Transaction;
using CookBook.DataAdapter.Factory.Transaction;
using CookBook.Data.Request;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IUOMRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UOMRepository : EFSynchronousRepository<int, UOM, CookBookCoreEntities>,
        IUOMRepository
    {
        protected override DbSet<UOM> GetDbSet(CookBookCoreEntities entityContext)
        {
            return entityContext.UOMs;
        }

        protected override UOM FindImpl(int key, DbSet<UOM> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override UOM FindImplWithExpand(int key, DbSet<UOM> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetUOMModuleMappingList_Result> GetUOMModuleMappingDataList(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetUOMModuleMappingList().ToList();
                }
            }
        }

        public IList<procGetUOMModuleMappingGridData_Result> procGetUOMModuleMappingGridData(RequestData requestData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction, requestData.SessionSectorName))
                {
                    return context.procGetUOMModuleMappingGridData().ToList();
                }
            }
        }
    }
}
