﻿using CookBook.DataAdapter.Base;
using CookBook.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using CookBook.DataAdapter.Factory.Contract.Common;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(ISiteProfile1Repository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SiteProfile1Repository : EFSynchronousRepository<int, SiteProfile1, CookBookCommonEntities>,
        ISiteProfile1Repository
    {
        protected override DbSet<SiteProfile1> GetDbSet(CookBookCommonEntities entityContext)
        {
            return entityContext.SiteProfile1;
        }

        protected override SiteProfile1 FindImpl(int key, DbSet<SiteProfile1> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override SiteProfile1 FindImplWithExpand(int key, DbSet<SiteProfile1> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }
    }
}
