﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IAPLMasterRepository : ICrudRepository<int, UniqueAPLMaster>
    {
        List<procGetAPLMasterData_Result> GetAPLMasterDataList(string articleNumber, string mogCode, RequestData requestData);
        List<procGetAPLMasterDataforSector_Result> GetAPLMasterDataListForSector(string articleNumber, string mogCode, RequestData requestData);

        List<ProcShowNationalDishCategory_Result> NationalGetDishCategoryDataList(RequestData request);
        List<ProcShowNationalDishes_Result> NationalGetDishesDataList(RequestData requestData);


        List<procNationalGetAPLMasterData_Result> NationalGetAPLMasterDataList(RequestData requestData);


        List<procGetAPLsPageWiseDataList_Result> GetAPLMasterDataListPageing(RequestData requestData, int pageindex);


        List<procGetNationalAPLMaster_Result> GetNationalAPLExcelList(RequestData requestData);
        string UpdateMOGLISTAPL(RequestData request, string xml, string username);
        List<procshowMOGAPLUPLOADDETAILSAPL_Result> GetMOGAPLHISTORYDetAPL(RequestData request,string username);

        //List<procCuisineMasterList_Result> GetCuisineMaster(RequestData request);

        List<ProcGetMOGBATCHDETAILSAPL_Result> GetMOGAPLHISTORYAPL(RequestData request, string username);

        List<procshowGetALLHISTORYBATCHWISEAPL_Result> GetshowGetALLHISTORYBATCHWISEAPL(RequestData request, string username,string batchno);

        List<procshowGetALLHISTORYAPL_Result> GetALLHISTORYDetailsAPL(RequestData request, string username);

        IList<procGetAPLMasterMOGCode_Result> GetNOTMappedMOGDataListAPL(RequestData requestData);
        
        string SaveAPLMasterDataList(IList<UniqueAPLMaster> model, int mogStatus, RequestData requestData);
        string SaveAPLMasterData(UniqueAPLMaster model, int mogStatus,string allergenCodes, RequestData requestData);
        //Krish 11-10-2022
        string SaveRangeTypeAPLData(string articleNumber, string rangeTypeCode, RequestData requestData);
    }
}
