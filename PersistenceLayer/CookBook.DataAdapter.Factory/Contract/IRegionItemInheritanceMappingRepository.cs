﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IRegionItemInheritanceMappingRepository : ICrudRepository<int, RegionItemInheritanceMapping>
    {
        IList<procGetRegionItemList_Result> GetRegionItemInheritanceMappingDataList(string regionID, RequestData requestData);
        string SaveSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData requestData);
        string SaveMOGNationalToSectorInheritance(SaveMOGNationalToSectorInheritanceData model, RequestData requestData);
        string UpdateCostSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData requestData);
    }
}
