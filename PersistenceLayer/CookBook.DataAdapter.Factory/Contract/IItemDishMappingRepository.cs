﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IItemDishMappingRepository : ICrudRepository<int, ItemDishMapping>
    {
        string SaveList(IList<ItemDishMapping> model, int itemStatus, RequestData requestData);
        
        IList<ItemDishMapping> GetItemDishMappingDataList(string itemCode, RequestData requestData);
        //IList<SiteMasterData> GetSiteMasterList();
    }
}
