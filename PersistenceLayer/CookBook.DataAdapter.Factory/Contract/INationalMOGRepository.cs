﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface INationalMOGRepository : ICrudRepository<int, NationalMOG>
    {
        IList<procGetNationalMOGList_Result> GetNationalMOGDataList(RequestData requestData);

        IList<procGetMogNotMappedList_Result> GetNOTMappedMOGDataList(RequestData requestData);

        List<ProcGetMOGBATCHDETAILS_Result> GetMOGAPLHISTORY(RequestData requestData, string username);

        List<procshowMOGAPLUPLOADDETAILS_Result> GetMOGAPLHISTORYDet(RequestData requestData, string username);


        List<procshowGetALLHISTORYBATCHWISE_Result> GetshowGetALLHISTORYBATCHWISE(RequestData requestData, string username,string batchno);


        List<procshowGetALLHISTORY_Result> GetALLHISTORYDetails(RequestData requestData, string username);

        string UpdateMOGLIST(RequestData requestData, string xml, string username);
        string SaveNationalMOGToSectors(NationalMOG model, string sectors, RequestData requestData);
        IList<procGetAPLMasterDataCommon_Result> GetAPLMasterDataListCommon(string articleNumber, string mogCode, RequestData requestData);
        string SaveList(IList<NationalMOG> model, RequestData requestData);
        IList<procGetRegionsDetailsByUserId_Result> GetRegionsDetailsByUserIdDataList(RequestData requestData);
        IList<procGetSiteDetailsByUserId_Result> GetSiteDetailsByUserIdDataList(RequestData requestData);
        IList<procGetUserRBFAMatrixByUserID_Result> GetUserRBFAMatrixDataByUserId(RequestData requestData);
        //Krish 18-10-2022
        string SaveProcessTypeMogData(RequestData requestData, List<string> processTypes, string mogCode, string rangeTypeCode, int inputStdDuration);
        List<MogProcessTypeMapping> GetProcessTypeMogData(RequestData requestData, string mogCode);
        string SaveAllergenMogData(RequestData requestData, List<string> contains, List<string> maycontains, string mogCode);

    }
}
