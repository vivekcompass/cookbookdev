﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface ISiteDishContainerMappingRepository : ICrudRepository<int, SiteDishContainerMapping>
    {
       
        IList<procGetSiteDishContainerMappingData_Result> GetSiteDishContainerMappingDataList(string SiteCode, RequestData requestData);
        string SaveSiteDishContainerMappingDataList(IList<SiteDishContainerMapping> model, RequestData requestData);

        IList<procGetSiteDishCategoryContainerMappingData_Result> GetSiteDishCategoryContainerMappingDataList(string SiteCode, RequestData requestData);
        string SaveSiteDishCategoryContainerMappingDataList(IList<SiteDishCategoryContainerMapping> model, RequestData requestData);
    }
}
