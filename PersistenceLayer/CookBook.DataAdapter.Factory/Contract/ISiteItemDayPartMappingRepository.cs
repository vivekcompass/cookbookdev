﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface ISiteItemDayPartMappingRepository : ICrudRepository<int, SiteItemDayPartMapping>
    {
        
        
        IList<SiteItemDayPartMapping> GetSiteItemDayPartMappingDataList(string itemCode,string sitecode, RequestData requestData);
        IList<SiteItemDayPartMapping> GetMasterSiteItemDayPartMappingDataList(string SiteCode, RequestData requestData);
        string SaveSiteItemDayPartMappingList(IList<SiteItemDayPartMapping> model,  RequestData requestData);
    }
}
