﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface ISiteDayPartMappingRepository : ICrudRepository<int, SiteDayPartMapping>
    {
        IList<SiteDayPartMapping> GetSiteDayPartMappingDataList(string sitecode, RequestData requestData);
        string SaveSiteDayPartMappingList(IList<SiteDayPartMapping> model, bool isClosingTime,  RequestData requestData);
    }
}
