﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IRegionItemDishMappingRepository : ICrudRepository<int, RegionItemDishMapping>
    {
        
        
        IList<RegionItemDishMapping> GetRegionItemDishMappingDataList(string itemCode,string regionID, RequestData requestData);
        IList<RegionItemDishMapping> GetMasterRegionItemDishMappingDataList(string regionID, RequestData requestData);
        string SaveRegionItemDishMappingList(IList<RegionItemDishMapping> model, int regionStatus, RequestData requestData);

        string mydishcateinactiveupdate(IList<RegionDishCategoryMapping> model,  RequestData requestData);
    }
}
