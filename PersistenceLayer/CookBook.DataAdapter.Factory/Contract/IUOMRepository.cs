﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IUOMRepository: ICrudRepository<int, UOM>
    {
        IList<procGetUOMModuleMappingList_Result> GetUOMModuleMappingDataList(RequestData requestData);
        IList<procGetUOMModuleMappingGridData_Result> procGetUOMModuleMappingGridData(RequestData requestData);
    }
}
