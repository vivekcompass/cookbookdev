﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IItemRepository : ICrudRepository<int, Item>
    {
        IList<procGetItemList_Result> GetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, RequestData requestData);
        IList<procGetItemListExport_Result> GetItemExportDataList( RequestData requestData);

        IList<procNationalGetItemList_Result> NationalGetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, RequestData requestData);

        IList<procGetSiteUnitPrice_Result> GetSiteUnitPrice(string siteCode, RequestData requestData);
        IList<procGetSiteDayPartMapping_Result> GetSiteDayPartMapping(string siteCode, RequestData requestData);
        IList<procGetSiteItemPriceHistory_Result> GetSiteItemPriceHistory(string siteCode, string itemCode, RequestData requestData);

        IList<procGetDishImpactedItemList_Result> GetDishImpactedItemList(string dishCode, RequestData requestData);

        IList<procGetRegionInheritedData_Result> GetRegionInheritedData(string RegionID,string SectorID, RequestData requestData);
        SiteItemDropDownData GetSiteItemDropDownDataData( RequestData requestData);

        string SaveList(IList<Item> model, RequestData requestData);
        //Krish 28-07-2022
        SectorItemData GetSectorItemData(RequestData requestData, bool isAddEdit, bool onLoad);
        //Krish 30-07-2022
        RegionItemData GetRegionItemData(RequestData requestData, bool isSectorUser, int regionId = 0, bool gridOnly = false);
    }
}
