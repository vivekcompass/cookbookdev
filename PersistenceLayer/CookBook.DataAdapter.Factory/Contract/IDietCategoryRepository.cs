﻿using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Common
{
    public interface IDietCategoryRepository : ICrudRepository<int, DietCategory>
    {
    }
}
