﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface ISiteItemDishMappingRepository : ICrudRepository<int, SiteItemDishMapping>
    {
        
        
        IList<SiteItemDishMapping> GetSiteItemDishMappingDataList(string itemCode,string sitecode, RequestData requestData);
        IList<SiteItemDishMapping> GetMasterSiteItemDishMappingDataList(string SiteCode, RequestData requestData);
        IList<FoodBookNutritionData> GetFoodBookNutritionDataList(string sectorCode);
        string SaveSiteItemDishMappingList(IList<SiteItemDishMapping> model, int siteStatus, RequestData requestData);
    }
}
