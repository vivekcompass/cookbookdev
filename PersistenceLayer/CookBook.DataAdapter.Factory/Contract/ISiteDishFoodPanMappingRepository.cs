﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface ISiteDishFoodPanMappingRepository : ICrudRepository<int, SiteDishFoodPanMapping>
    {
       
        IList<procGetSiteDishFoodMappingData_Result> GetSiteDishFoodMappingDataList(string SiteCode,RequestData requestData);
        string SaveSiteDishFoodPanMappingDataList(IList<SiteDishFoodPanMapping> model, RequestData requestData);
        IList<procGetSiteDishCategoryFoodMappingData_Result> GetSiteDishCategoryFoodMappingDataList(string SiteCode, RequestData requestData);
    }
}
