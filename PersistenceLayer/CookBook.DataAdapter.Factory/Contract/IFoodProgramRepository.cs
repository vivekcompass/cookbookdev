﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IFoodProgramRepository : ICrudRepository<int, FoodProgram>
    {
        IList<procGetFoodProgramList_Result> GetFoodProgramDataList(RequestData requestData);
    }
}
