﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IRecipeRepository: ICrudRepository<int, Recipe>
    {
        IList<procGetAPLMasterDataForRegion_Result> GetAPLMasterRegionDataList(string articleNumber, string mogCode, int Regin_ID, RequestData requestData);
        IList<procGetAPLMasterDataForSite_Result> GetAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, RequestData requestData);
        IList<procGetRecipeList_Result> GetRecipeDataList(string condition,string DishCode, RequestData requestData);
        IList<procGetItemDishRecipeNutritionData_Result> GetItemDishRecipeNutritionData(string SiteCode, RequestData requestData);

        IList<procGetnationalRecipeList_Result> GetNationalRecipeDataList(string condition, string DishCode, RequestData requestData);
        IList<procGetRegionRecipeList_Result> GetRegionRecipeDataList(int RegionID,string condition, RequestData requestData);
        IList<procGetSiteRecipeList_Result> GetSiteRecipeDataList(string SiteCode, string condition, RequestData requestData);
        //Krish
        IList<procGetSiteDishRecipeList_Result> GetSiteDishRecipeDataList(string SiteCode, string dishCode, string condition, RequestData requestData);

        IList<procGetBaseRecipesByRecipeID_Result> GetBaseRecipesByRecipeID(int recipeID, RequestData requestData);
        IList<procGetRegionBaseRecipesByRecipeID_Result> GetRegionBaseRecipesByRecipeID(int RegionID, int recipeID, RequestData requestData);
        IList<procGetSiteBaseRecipesByRecipeID_Result> GetSiteBaseRecipesByRecipeID(string siteCode, int recipeID, string recipeCode, RequestData requestData);

        IList<procGetMOGsByRecipeID_Result> GetMOGsByRecipeID(int recipeID, RequestData requestData);
        IList<procGetRegionMOGsByRecipeID_Result> GetRegionMOGsByRecipeID(int RegionID,int recipeID, RequestData requestData);
        IList<procGetSiteMOGsByRecipeID_Result> GetSiteMOGsByRecipeID(string siteCode, int recipeID, string recipeCode, RequestData requestData);
        string SaveList(IList<Recipe> model, RequestData requestData);
        string SaveRegionList(IList<RegionRecipe> model, RequestData requestData);
        string SaveSiteList(IList<SiteRecipe> model, RequestData requestData);
        string SaveRecipeData(Recipe entity, IList<RecipeMOGMapping> entity1, IList<RecipeMOGMapping> entity2, RequestData requestData);
        string SaveRecipeNutrientMappingData(RecipeData entity, RequestData requestData);
        string SaveRegionRecipeData(RegionRecipe entity, IList<RegionRecipeMOGMapping> entity1, IList<RegionRecipeMOGMapping> entity2, RequestData requestData);
        string SaveSiteRecipeData(SiteRecipe entity, IList<SiteRecipeMOGMapping> entity1, IList<SiteRecipeMOGMapping> entity2, RequestData requestData);
        string SaveSiteRecipeAllergenData(string recipeCode, string siteCode, RequestData requestData);
        //Krish
        IList<procGetRecipeMOGNLevelFOrPrinting_Result> GetRecipePrintingData(string recipeCode, string siteCode, int regId, RequestData requestData);
        string GetRecipeDishDataForPrinting(string recipeCode, RequestData requestData);
        //Krish
        string SaveRecipeCopy(List<RecipeCopyData> model, List<string> recipes, string sourceSite, RequestData requestData);
        //Krish
        List<NutritionDishRecipeMogMapping> GetRecipeDishDataByDish(string dishCode, RequestData requestData);
        SectorRecipeData GetSectorRecipeData(RequestData requestData, bool isAddEdit, bool onLoad);
        //Krish 06-08-2022
        IList<procGetMOGNLevelForSplitScreen_Result> GetMOGNLevelForSplitScreen(string recipeCode, RequestData requestData);
    }
}
