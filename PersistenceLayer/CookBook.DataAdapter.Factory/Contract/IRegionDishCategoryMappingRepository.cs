﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IRegionDishCategoryMappingRepository : ICrudRepository<int, RegionDishCategoryMapping>
    {
        string SaveRegionDishCategoryMappingList(IList<RegionDishCategoryMapping> model, RequestData requestData);

        IList<RegionDishCategoryMapping> GetRegionDishCategoryMappingDataList(string RegionID, string ItemCode, RequestData requestData);
        IList<string> GetUniqueMappedItemCodesFromRegion(string RegionID, RequestData requestData);
    }
}
