﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface ISiteDishCategoryMappingRepository : ICrudRepository<int, SiteDishCategoryMapping>
    {
        string SaveSiteDishCategoryMappingList(IList<SiteDishCategoryMapping> model, RequestData requestData);
        
        IList<SiteDishCategoryMapping> GetSiteDishCategoryMappingDataList(string SiteCode, string ItemCode, RequestData requestData);
     
    }
}
