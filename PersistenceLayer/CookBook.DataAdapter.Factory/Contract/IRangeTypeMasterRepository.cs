﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IRangeTypeMasterRepository : ICrudRepository<int, RangeType>
    {
        List<procGetRangeTypeMaster_Result> GetRangeTypeMasterDataList(RequestData requestData);
        string SaveRangeTypeMasterDataList(RangeType model, RequestData requestData);
    }
}
