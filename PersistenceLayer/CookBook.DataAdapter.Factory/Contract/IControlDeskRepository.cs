﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Contract
{
   public interface IControlDeskRepository : ICrudRepository<int,SectorMaster>
    {
        List<procGetSectorMasterData_Result> GetManageSectorList(RequestData requestData);

        List<procGetCapringSectorMaster_Result> GetSectorListInfo(RequestData requestData);

        List<procGetCapringLevelMaster_Result> GetLevelMasterData(RequestData requestData);
        List<procGetCapringFunctionMaster_Result> GetFunctionMasterData(RequestData requestData);
        List<procGetCapringPermissionMaster_Result> GetPermissionMasterData(RequestData requestData);


        string SaveManageSectorInfo(RequestData request, ManageSectorData model, int usercode);

        string SaveManageLevelInfo(RequestData request, LevelMasterData model, int usercode);

        string SaveFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode);

        string SavePermissionInfo(RequestData request, PermissionMasterData model, int usercode);

        string SaveCapringInfo(RequestData request, CapringMastre model, int usercode);

        string UpdateCapringInfo(RequestData request, CapringMastre model, int usercode);

        string UpdateCapringDescriptionInfo(RequestData request, CapringMastre model, int usercode);

        string UpdatePermissionInfo(RequestData request, PermissionMasterData model, int usercode);

        string UpdateFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode);

        string UpdatefunctionInactiveActive(RequestData request, FunctionMasterData model, int usercode);

        string UpdatePermissionInactiveActive(RequestData request, PermissionMasterData model, int usercode);

        string UpdateManageLevelInfo(RequestData request, LevelMasterData model, int usercode);

        string UpdateManageSectorInfo(RequestData request, ManageSectorData model, int usercode);

        string UpdateInactiveActive(RequestData request, ManageSectorData model, int usercode);

        string UpdateLevelInactiveActive(RequestData request, LevelMasterData model, int usercode);

        string UpdateCapringInactiveActive(RequestData request, CapringMastre model, int usercode);

        List<procGetLevelMasterData_Result> GetLevelDataList(RequestData requestData);

        List<procGetCapringMasterData_Result> GetCapringMasterList(RequestData requestData);

        List<procGetFunctionalityData_Result> GetFunctionDataList(RequestData requestData);

        List<procGetPermissionData_Result> GetPermissionDataList(RequestData requestData);
    }
}
