﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IMOGRepository: ICrudRepository<int, MOG>
    {
        IList<procGetMOGList_Result> GetMOGDataList(RequestData requestData);
        IList<procGetInheritMOGList_Result> GetInheritedMOGDataList(RequestData requestData);
        IList<procGetSiteMOGList_Result> GetSiteMOGDataList(RequestData requestData, string siteCode);
        IList<procGetRegionMOGList_Result> GetRegionMOGDataList(RequestData requestData, int regionID);
        IList<procGetMOGImpactedBaseRecipes_Result> GetMOGImpactedBaseRecipiesDataList(int mogID,int recipeID, RequestData requestData);
        IList<procGetMOGImpactedFinalRecipes_Result> GetMOGImpactedFinalRecipiesDataList(int mogID, int recipeID, RequestData requestData);
        IList<procGetMOGImpactedDishes_Result> GetMOGImpactedDishesDataList(int mogID, int recipeID, RequestData requestData);
        IList<procGetMOGImpactedItems_Result> GetMOGImpactedItemsDataList(int mogID, int recipeID, RequestData requestData);
        IList<procGetMOGImpactedSites_Result> GetMOGImpactedSitesDataList(int mogID, int recipeID, RequestData requestData);
        IList<procGetMOGImpactedRegion_Result> GetMOGImpactedRegionsDataList(int mogID, int recipeID, RequestData requestData);
        IList<procGetMOGAPLImpactedDisheSector_Result> procGetMOGAPLImpactedDishSector(int mogID, string aplList, int sectorID, RequestData requestData);
        IList<procGetMOGAPLImpactedDishRegion_Result> procGetMOGAPLImpactedDishRegion(int mogID, string aplList, int sectorID, RequestData requestData);
        IList<procGetMOGAPLImpactedDishSites_Result> procGetMOGAPLImpactedDishSite(int mogID, string aplList, int sectorID, RequestData requestData);
        string SaveList(IList<MOG> model, RequestData requestData);
        IList<ProcGetExpiryProductData_Result> GetExpiryProductDataList(RequestData requestData);
        string SaveExpiryProductData(GetExpiryProductData model, RequestData requestData);
        IList<procGetMOGUOMDetails_Result> GetMOGUOMDataList(RequestData requestData);

        //Krish
        IList<ProcGetExpiryProductData_Result> GetExpiryProductDataOnlyExpiryNameList(RequestData requestData);
    }
}
