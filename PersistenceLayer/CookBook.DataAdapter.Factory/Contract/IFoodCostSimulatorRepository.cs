﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Contract
{
    public interface IFoodCostSimulatorRepository : ICrudRepository<int, FoodCostSimulation>
    {
        IList<DayPartData> GetDayPartBySiteList(int siteId, RequestData requestData);
        IList<FoodCostSimulation> GetSimulationDataListWithPaging(RequestData requestData, UserMasterResponseData userData, int pageIndex, int totalRecordsPerPage);
        IList<SimulationItemsData> GetItemsForSimulation(RequestData requestData, int siteId, int regionId, List<int> daypartId, bool checksiteItem);
        IList<SimulationRegionItemDishCategoryData> GetSimulationRegionItemDishCategories(RequestData requestData, int siteId, int regionId, int itemId);
        SimulationBoardData GetSimulationBoard(int simId, RequestData requestData);
        string SaveSimulationBoard(SimulationBoardData model, RequestData requestData);
        IList<procUpdateGetFCSSiteDishRecipeList_Result> UpdateAndFetchSiteDishRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode);
        IList<procGetFCSSiteRecipeData_Result> UpdateAndFetchSiteRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode);
        IList<FoodCostSimRecipe> GetAllRecipes(RequestData requestData);
        IList<FoodCostSimSiteRecipe> GetAllSiteRecipe(RequestData requestData, string sitecode, string simCode);
        string SaveFoodCostSimSiteDishRecipeMappingAndMOG(RequestData requestData, FoodCostSimRecipe entityRec, FoodCostSimSiteRecipe entity, IList<FoodCostSimSiteRecipeMOGMapping> entity2, int dishId, string dishCode, List<FoodCostSimAPLHistory> entityHis, IList<FoodCostSimSiteRecipeMOGMapping> entity1);
        IList<FoodCostSimSiteDishRecipeMapping> GetSiteDishRecipeMapping(RequestData requestData, string sitecode, string dishcode, string simCode);
        string SaveFoodCostSimRecipeData(FoodCostSimRecipe entityRec, RequestData requestData);
        string SaveFoodCostSimSiteRecipeData(FoodCostSimRecipe entityRec, FoodCostSimSiteRecipe entity, IList<FoodCostSimSiteRecipeMOGMapping> entity1, IList<FoodCostSimSiteRecipeMOGMapping> entity2, List<FoodCostSimAPLHistory> entityHis,int dishId,string dishCode, RequestData requestData);
        IList<procGetFCSSiteMOGsByRecipeID_Result> GetSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode, string simCode, RequestData requestData);
        string SaveFCSAPLHistory(FoodCostSimAPLHistory entity, RequestData requestData);
        IList<procGetFCSAPLMasterDataForSite_Result> GetFCSAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, string simuCode, RequestData requestData, bool isVersioned);
        IList<DishData> GetFCSDish(RequestData requestData, string siteCode, int itemId);
        bool IsFCSAPLGet(string simCode, RequestData requestData);
        //IList<procGetFCSAPLMasterDataForSite2_Result> GetFCSAPLMasterSiteDataList2(string articleNumber, string mogCode, string SiteCode, string simuCode, RequestData requestData);
    }
}
