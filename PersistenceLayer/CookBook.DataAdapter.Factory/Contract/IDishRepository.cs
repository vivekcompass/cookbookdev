﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IDishRepository : ICrudRepository<int, Dish>
    {
        IList<procGetDishList_Result> GetDishDataList(int? foodProgramID, int? dietCategoryID, RequestData requestData);
        IList<procGetSiteDishList_Result> GetSiteDishDataList(string siteCode, int? foodProgramID, int? dietCategoryID, RequestData requestData);
        IList<procGetRegionDishList_Result> GetRegionDishDataList(int RegionID,int? foodProgramID, int? dietCategoryID, string subSectorCode, RequestData requestData);
        IList<procGetRegionDishMasterData_Result> GetRegionDishMasterData(int RegionID, int? foodProgramID, int? dietCategoryID,RequestData requestData);
        string SaveList(IList<Dish> model, RequestData requestData);
        IList<DishCategory> GetDishCategory(RequestData requestData);
        string SaveDishData(DishData model, RequestData requestData);
        IList<PlanningTag> GetPlanningTag(RequestData requestData);
        string SavePlanningTagData(PlanningTagData model, RequestData requestData);
        //Krish 02-08-2022
        SectorDishData GetSectorDishData(RequestData requestData, string condition, bool isAddEdit, bool onLoad);
        //Krish 08-10-2022

        string SaveRangeTypeDishData(string dishCode, string rangeTypeCode, RequestData requestData);
        //Krish 25-10-2022
        //IList<procGetSectorDishWithRangeType_Result> GetDishDataRangeTypeList(RequestData requestData);
    }
}
