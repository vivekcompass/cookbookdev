﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface ICostSimulatorRepository : ICrudRepository<int, CostSimulation>
    {
        //IList<procGetItemList_Result> GetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID);

        //IList<procGetRegionInheritedData_Result> GetRegionInheritedData(string RegionID,string SectorID);

        //string SaveList(IList<Item> model);

        IList<procGetMappedDishRecipeList_Result> GetRecipeMappingDataList(string DishCode, RequestData requestData);
        IList<procGetSimulatedMappedRecipeMOGListSite_Result> GetSimulatedMappedRecipeMOGListSiteDataList(int RecipeID, int SiteId, RequestData requestData);
        IList<procGetSimulatedMappedRecipeMOGListSector_Result> GetSimulatedMappedRecipeMOGListSectorDataList(int RecipeID, int RegionId, RequestData requestData);
        IList<procGetSimulatedMappedRecipeMOGListNational_Result> GetSimulatedMappedRecipeMOGListNationalDataList(int RecipeID, RequestData requestData);

    }
}
