﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IDishCategoryRepository : ICrudRepository<int, DishCategory>
    {
        string SaveDishCategorySiblingMapping(DishCategoryData model, RequestData requestData);
        List<procGetDishCategorySiblingList_Result> GetDishCategorySiblingList(RequestData request);
        List<procGetDishCategoryList_Result> GetDishCategoryDataList(RequestData request);
    }
}
