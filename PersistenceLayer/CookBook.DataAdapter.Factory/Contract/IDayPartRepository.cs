﻿using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IRevenueTypeRepository : ICrudRepository<int, RevenueType>
    {
    }
}
