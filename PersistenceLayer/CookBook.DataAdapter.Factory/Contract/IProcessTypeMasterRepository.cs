﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IProcessTypeMasterRepository : ICrudRepository<int,ProcessType>
    {
        List<procGetProcessTypeMaster_Result> GetProcessTypeMasterDataList(RequestData requestData);
        string SaveProcessTypeMasterDataList(ProcessType model, RequestData requestData);
        List<MogProcessTypeMapping> GetProcessMappingDataList(RequestData requestData);
    }
}
