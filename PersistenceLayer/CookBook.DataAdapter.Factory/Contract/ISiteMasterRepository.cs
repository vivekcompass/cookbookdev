﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Common
{
    public interface ISiteMasterRepository: ICrudRepository<int, SiteMaster>
    {
        string SaveList(IList<SiteMaster> model, RequestData requestData);
        IList<procGetSiteList_Result> GetSiteDataList(RequestData requestData);
        //Krish
        IList<procGetSiteByRegionList_Result> GetSiteByRegionDataList(int regionId, RequestData requestData);
        IList<NationalprocGetSiteList_Result> NationalGetSiteMasterDataList(RequestData requestData);


        IList<procshowregionmaster_Result> GetRegionMasterDataList(RequestData requestData);
        IList<procGetCPUList_Result> GetCPUDataList(RequestData requestData);
        IList<procGetDKList_Result> GetDKDataList(RequestData requestData);

        IList<CuisineMaster> GetCuisineDataList(RequestData requestData);
        string SaveCuisine(RequestData requestData, CuisineMasterData model, int username);

        string UpdateCuisine(RequestData requestData, CuisineMasterData model, int username);

        IList<HealthTagMaster> GetHealthTagDataList(RequestData requestData);
        string SaveHealthTag(RequestData requestData, HealthTagMasterData model, int username);

        string UpdateHealthTag(RequestData requestData, HealthTagMasterData model, int username);

        IList<LifeStyleTagMaster> GetLifeStyleTagDataList(RequestData requestData);
        string SaveLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username);

        string UpdateLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username);
        string UpdateHRISLocationData(RequestData requestData, SiteMasterData siteMasterData);
        IList<procGetExpiryCategoryMasterData_Result> GetExpiryCategoryDataList(RequestData requestData);
        IList<procGetShelfLifeUOMMasterData_Result> GetShelfLifeUOMDataList(RequestData requestData);
        string SaveExpiryCategory(RequestData requestData, ExpiryCategoryMaster model);
        string SaveNutritionMaster(RequestData requestData, NutritionElementMaster model);
        IList<procGetNutritionMasterData_Result> GetNutritionMasterDataList(RequestData requestData);
        IList<NutritionElementType> GetNutritionElementTypeDataList(RequestData requestData);

        IList<DietType> GetDietTypeDataList(RequestData requestData);
        IList<Texture> GetTextureDataList(RequestData requestData);
        IList<NutrientElementUOM> GetNutrientUOMDataList(RequestData requestData);
        IList<NutritionElementType> GetNutrientTypeDataList(RequestData requestData);
        IList<ItemSectorMaster> GetItemSectorMasterDataList(RequestData requestData);
        IList<MenuType1> GetMenuType1MasterDataList(RequestData requestData);
        IList<MenuType2> GetMenuType2MasterDataList(RequestData requestData);
        IList<Area> GetAreaMasterDataList(RequestData requestData);

        string SaveCommonMaster(RequestData requestData, CommonMasterData model);
    }
}
