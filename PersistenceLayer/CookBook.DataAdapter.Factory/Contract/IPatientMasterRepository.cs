﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IPatientMasterRepository : ICrudRepository<int, PatientMaster>
    {
        List<procGetPatientMaster_Result> GetPatientMaster(RequestData requestData);
        List<procGetPatientMasterBATCHHEADER_Result> GetPatientMasterHISTORY(RequestData requestData, string username);
        List<procGetPatientMasterBATCHDETAILS_Result> GetPatientMasterHISTORYDet(RequestData requestData, string username);
        List<procshowGetALLPatientMasterHISTORYBATCHWISE_Result> GetshowGetALLPatientMasterHISTORYBATCHWISE(RequestData requestData,  string batchno);
        string AddUpdatePatientMasterList(RequestData request, string xml, string username);
        string SavePatientMasterStatus(RequestData request, int id, int username);
        List<procshowGetALLHISTORYPatientMaster_Result> GetALLHISTORYDetailsPatientMaster(RequestData request);
    }
}
