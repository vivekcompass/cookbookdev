﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Common
{
    public interface ICafeRepository : ICrudRepository<int, Cafe>
    {
        IList<procGetCafeList_Result> GetCafeDataList(RequestData requestData);
        string SaveList(IList<Cafe> model, RequestData requestData);
    }
}
