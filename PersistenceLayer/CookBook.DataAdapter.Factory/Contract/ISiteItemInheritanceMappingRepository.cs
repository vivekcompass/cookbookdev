﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface ISiteItemInheritanceMappingRepository : ICrudRepository<int, SiteItemInheritanceMapping>
    {
       
        IList<procGetSiteItemInheritanceMappingData_Result> GetSiteItemInheritanceMappingDataList(string SiteCode, RequestData requestData);
      
        string SaveRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData);
        string SaveToSiteInheritance(SiteItemInheritanceMapping model, RequestData requestData);
        IList<ProcGetInheritChangesSectorToRegion_Result> GetInheritChangesSectorToRegion(ItemInheritChangeRequestData itemInheritChangeRequestData, RequestData requestData);
        IList<ProcGetInheritChangesRegionToSite_Result> GetInheritChangesRegionToSite(ItemInheritChangeRequestData itemInheritChangeRequestData, RequestData requestData);
        string SaveChangesSectorToRegion(List<InheritanceChangesData> model, RequestData requestData);
        string SaveChangesRegionToSite(List<InheritanceChangesData> model, RequestData requestData);
        string SaveToSiteInheritanceDataList(IList<SiteItemInheritanceMapping> model, RequestData requestData);

        string UpdateCostRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData);
    }
}
