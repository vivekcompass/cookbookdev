﻿using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;

namespace CookBook.DataAdapter.Factory.Repository.Transaction
{
    public interface IReasonRepository : ICrudRepository<int, Reason>
    {
    }
}
