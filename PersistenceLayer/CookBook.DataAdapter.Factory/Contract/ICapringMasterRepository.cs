﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Common
{
    public interface ICapringMasterRepository : ICrudRepository<int, CapringMaster>
    {
        List<CapringMaster> GetCapringMasterDataList(RequestData requestData);
       
    }
}
