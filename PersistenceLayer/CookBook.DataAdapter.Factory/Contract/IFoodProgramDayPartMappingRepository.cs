﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IFoodProgramDayPartMappingRepository : ICrudRepository<int, FoodProgramDayPartMapping>
    {
        IList<FoodProgramDayPartMapping> GetFoodProgramDayPartMappingDataList(string foodCode, RequestData requestData);
        string SaveFoodProgramDayPartMappingList(IList<FoodProgramDayPartMapping> model,  RequestData requestData);
    }
}
