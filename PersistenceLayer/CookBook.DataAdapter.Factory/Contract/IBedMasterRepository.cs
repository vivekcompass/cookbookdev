﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{  
    public interface IBedMasterRepository : ICrudRepository<int, BedMaster>
    {
        List<procGetBedMaster_Result> GetBedMaster(RequestData requestData);
        List<procGetBedMasterBATCHHEADER_Result> GetBedMasterHISTORY(RequestData requestData, string username);
        List<procGetBedMasterBATCHDETAILS_Result> GetBedMasterHISTORYDet(RequestData requestData, string username);
        List<procshowGetALLBedMasterHISTORYBATCHWISE_Result> GetshowGetALLBedMasterHISTORYBATCHWISE(RequestData requestData,  string batchno);
        string AddUpdateBedMasterList(RequestData request, string xml, string username);
        string SaveBedMasterStatus(RequestData request, int id, int username);
        List<procshowGetALLHISTORYBedMaster_Result> GetALLHISTORYDetailsBedMaster(RequestData request);
    }
}
