﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IContainerTypeRepository : ICrudRepository<int, ContainerType>
    {
        IList<procGetContainerType_Result> GetContainerTypeDataList(RequestData requestData);
        string SaveList(IList<ContainerType> model, RequestData requestData);
    }
}
