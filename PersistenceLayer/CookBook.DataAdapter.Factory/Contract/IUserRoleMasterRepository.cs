﻿using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;

namespace CookBook.DataAdapter.Factory.Contract.Common
{
    public interface IUserRoleMasterRepository : ICrudRepository<int, UserRoleMaster>
    {
    }
}
