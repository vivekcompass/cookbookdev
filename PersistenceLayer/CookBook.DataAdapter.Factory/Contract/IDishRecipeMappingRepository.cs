﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IDishRecipeMappingRepository : ICrudRepository<int, DishRecipeMapping>
    {
        string SaveDishRecipeMappingList(IList<DishRecipeMapping> model, int dishStatus, RequestData requestData);
        string SaveRegionDishRecipeMappingList(IList<RegionDishRecipeMapping> model, RequestData requestData);
        string SaveSiteDishRecipeMappingList(IList<SiteDishRecipeMapping> model, RequestData requestData);
        IList<DishRecipeMapping> GetDishRecipeMappingDataList(string DishCode, RequestData requestData);
        IList<RegionDishRecipeMapping> GetRegionDishRecipeMappingDataList(int RegionID,string DishCode, string SubSectorCode, RequestData requestData);
        IList<SiteDishRecipeMapping> GetSiteDishRecipeMappingDataList(string SiteCode, string DishCode, RequestData requestData);

    }
}
