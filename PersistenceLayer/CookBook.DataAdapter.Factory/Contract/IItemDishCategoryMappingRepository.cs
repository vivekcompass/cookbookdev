﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IItemDishCategoryMappingRepository : ICrudRepository<int, ItemDishCategoryMapping>
    {
        string SaveItemDishCategoryMappingList(IList<ItemDishCategoryMapping> model, RequestData requestData);
        
        IList<ItemDishCategoryMapping> GetItemDishCategoryMappingDataList(string itemCode, RequestData requestData);

        IList<ItemDishCategoryMapping> GetNationalItemDishCategoryMappingDataList(string itemCode, string sectorcode, RequestData requestData);
        IList<string> GetUniqueMappedItemCodesFromItem(RequestData requestData);

    }
}
