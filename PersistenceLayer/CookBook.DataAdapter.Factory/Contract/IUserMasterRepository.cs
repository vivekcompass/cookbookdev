﻿using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Factory.Contract.Common
{
    public interface IUserMasterRepository : ICrudRepository<int, UserMaster>
    {
        procGetUserDataByUsername_Result GetUserDetailsByUsername(string username,RequestData requestData);

        IList<procGetSectorDataByUserID_Result> GetSectorDataByUserID(int userID, RequestData requestData);
        string ChangeSectorData(int UserID, string sectorNumber, RequestData requestData);
        string ChangeUserRoleData(int UserID, int userRoleID, RequestData requestData);
        IList<procGetUserModules_Result> GetModulesByUserID(int userID, RequestData requestData);
        IList<procGetUserModulePermission_Result> GetUserPermissionsByModuleCode(int userID, string moduleCode, RequestData requestData);
        IList<procGetRoleDataByUserID_Result> GetUserRoleDataByUserID(int userID, RequestData requestData);
    }
}
