﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Contract
{
    public interface IEmailRepository : ICrudRepository<long, EmailNotification>
    {
        void SendPendingEmail(RequestData requestData);
        void SendMOGEmail(MOGData model, RequestData requestData);
        void SendNewMOGEmail(MOGData model, RequestData requestData);
        void SendRecipeEmail(RecipeData model, RequestData requestData);

        EmailBody GetEmailDetails(string type, RequestData requestData);

        SmtpServerSetting GetSmtpServerSettings(RequestData requestData);

        void SendGOVOOffSiteItemAutoInheritEmail(ItemData model, RequestData requestData);
    }
}
