﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Factory.Contract.Transaction
{
    public interface IDieticianMasterRepository : ICrudRepository<int, DieticianMaster>
    {
        List<procGetDieticianMaster_Result> GetDieticianMaster(RequestData requestData);
        List<procGetDieticianMasterBATCHHEADER_Result> GetDieticianMasterHISTORY(RequestData requestData, string username);
        List<procGetDieticianMasterBATCHDETAILS_Result> GetDieticianMasterHISTORYDet(RequestData requestData, string username);
        List<procshowGetALLDieticianMasterHISTORYBATCHWISE_Result> GetshowGetALLDieticianMasterHISTORYBATCHWISE(RequestData requestData, string batchno);
        string AddUpdateDieticianMasterList(RequestData request, string xml, string username);
        string SaveDieticianMasterStatus(RequestData request, int id, int username);
        List<procshowGetALLHISTORYDieticianMaster_Result> GetALLHISTORYDetailsDieticianMaster(RequestData request);

        string SaveHISPatientProfileMaster(RequestData request, List<HISPatientProfileData> hisPatientProfileData);
        string SaveHISUserData(RequestData request, List<HISUserData> hisUserData);
        string SaveHISBedMasterData(RequestData request, List<HISBedMasterData> hisBedMasterData);

        string SaveHISPatientProfileMaster_HN(RequestData request, List<HISPatientProfileData> hisPatientProfileData);
        string SaveHISUserData_HN(RequestData request, List<HISUserData> hisUserData);
        string SaveHISBedMasterData_HN(RequestData request, List<HISBedMasterData> hisBedMasterData);

        string SaveHISPatientProfileMaster_R10(RequestData request, List<HISPatientProfileData> hisPatientProfileData);
        string SaveHISUserData_R10(RequestData request, List<HISUserData> hisUserData);
        string SaveHISBedMasterData_R10(RequestData request, List<HISBedMasterData> hisBedMasterData);

        string SaveHISPatientProfileMaster_40HY(RequestData request, List<HISPatientProfileData> hisPatientProfileData);
        string SaveHISUserData_40HY(RequestData request, List<HISUserData> hisUserData);
        string SaveHISBedMasterData_40HY(RequestData request, List<HISBedMasterData> hisBedMasterData);

        string SaveHISPatientProfileMaster_40VI(RequestData request, List<HISPatientProfileData> hisPatientProfileData);
        string SaveHISUserData_40VI(RequestData request, List<HISUserData> hisUserData);
        string SaveHISBedMasterData_40VI(RequestData request, List<HISBedMasterData> hisBedMasterData);

        string SaveHISPatientProfileMaster_40DN(RequestData request, List<HISPatientProfileData> hisPatientProfileData);
        string SaveHISUserData_40DN(RequestData request, List<HISUserData> hisUserData);
        string SaveHISBedMasterData_40DN(RequestData request, List<HISBedMasterData> hisBedMasterData);

    }
}
