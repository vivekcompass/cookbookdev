//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CookBook.DataAdapter.Factory.Common
{
    using System;
    using System.Collections.Generic;
    
    public partial class SmtpServerSetting
    {
        public int SmtpServerSettingId { get; set; }
        public string ServerName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public Nullable<bool> IsSsl { get; set; }
        public string PortNumber { get; set; }
        public string SmtpType { get; set; }
    }
}
