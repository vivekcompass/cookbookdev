﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookBook.DataAdapter.Base.Model;

namespace CookBook.DataAdapter.Base.Extensions
{
    public static class BulkUpsertExtensions
    {
        public static void BulkUpsert<TEntity>(this DbContext ctx, IEnumerable<TEntity> records, int? batchSize = null) where TEntity : class
        {
            Type t = typeof(TEntity);
            string tableName = t.Name;
            string tabelTypeName = tableName + "Type";
            string colQuery = string.Format(@"SELECT  T1.Column_Name, Data_Type, 
			CASE when is_nullable='YES' then 'NULL'
				else 'NOT NULL'
				end as is_nullable,
            CASE 
				WHEN t1.CHARACTER_MAXIMUM_LENGTH is null 
						Then ''
				WHEN t1.CHARACTER_MAXIMUM_LENGTH <0
						Then '(MAX)'  
				else
					'('+ CONVERT(varchar(15), t1.CHARACTER_MAXIMUM_LENGTH) +')'
				end as Data_Length,
            ORDINAL_POSITION,
		    CASE 
                  WHEN t3.Column_Name is not null 
                     THEN t2.Constraint_Type
                  ELSE null 
            END as  Constraint_Type
            FROM INFORMATION_SCHEMA.COLUMNS T1 
            Inner JOIN 
            INFORMATION_SCHEMA.TABLE_CONSTRAINTS T2
            on T1.Table_Name = T2.Table_Name
            left Join 
            INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE T3
            on  t2.Table_Name = t3.Table_Name
            And t2.Constraint_Name = t3.Constraint_Name
            AND t2.Table_Name = t3.Table_Name
            and T1.Column_Name=t3.Column_Name
            where T1.Table_Name =  '{0}'", tableName);
            var colDefintion = ctx.Database.SqlQuery<FieldDefinition>(colQuery).ToList();
            string fieldDefintion = string.Join(",", colDefintion.Select(x => x.Column_Name + " " +
                                x.Data_Type + x.Data_Length + " " + x.is_nullable));
            string queryString = string.Format(@"
                            IF EXISTS (SELECT * FROM sys.types WHERE is_user_defined = 1 AND name = '{0}')
                            BEGIN 
                                DROP TYPE dbo.{0}
                            END 
                            CREATE TYPE dbo.{0} AS TABLE
                            ({1})", tabelTypeName, fieldDefintion);
            ctx.Database.ExecuteSqlCommand(queryString);
            var joinStatement = string.Join(" AND ", colDefintion.Where(x => x.Constraint_Type != null && x.Constraint_Type.ToUpper() == "PRIMARY KEY").Select(x => string.Format("target.{0}=source.{0}", x.Column_Name)));
            var updateStatement = string.Join(",", colDefintion.Where(x => x.Constraint_Type == null || x.Constraint_Type.ToUpper() != "PRIMARY KEY").Select(x => string.Format("target.{0}=source.{0}", x.Column_Name)));
            var insertStatement = string.Join(",", colDefintion.OrderBy(x => x.Ordinal_Position).Select(x => string.Format("source.{0}", x.Column_Name)));

            int skip = 0;
            int offset = 100000;
            batchSize = batchSize ?? offset;
            if (batchSize <= 50000)
            {
                offset = 50000;
            }
            else if (batchSize >= 100000)
            {
                offset = 100000;
            }
            else if (batchSize > 50000 & batchSize < 100000)
            {
                offset = batchSize.Value;
            }
            int take = offset;
            while (skip < records.Count())
            {
                var customerDataTable = GetDataTable<TEntity>(records.Skip(skip).Take(offset));
                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@tbl" + tableName;
                parameter.SqlDbType = System.Data.SqlDbType.Structured;
                parameter.TypeName = tabelTypeName;
                parameter.Value = customerDataTable;
                queryString = string.Format(@"
                          MERGE INTO {0} target
                          USING {1} source
                          ON {2}
                          WHEN MATCHED THEN
                          UPDATE SET {3}
                          WHEN NOT MATCHED THEN
                          INSERT VALUES({4});",
                          tableName, parameter.ParameterName, joinStatement, updateStatement, insertStatement);
                ctx.Database.ExecuteSqlCommand(queryString, parameter);
                skip += offset;
            }
        }

        private static DataTable GetDataTable<T>(IEnumerable<T> entities)
        {
            entities = entities.ToArray();

            Type t = typeof(T);

            var properties = t.GetProperties().Where(EventTypeFilter).ToList();
            var table = new DataTable();
            properties.RemoveAll(x => x.PropertyType.Name.Contains("ICollection"));


            foreach (var property in properties)
            {
                Type propertyType = property.PropertyType;
                if (propertyType.IsGenericType &&
                    propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    propertyType = Nullable.GetUnderlyingType(propertyType);
                }
                table.Columns.Add(new DataColumn(property.Name, propertyType));
            }

            foreach (var entity in entities)
            {
                table.Rows.Add(properties.Select(
                  property => GetPropertyValue(
                  property.GetValue(entity, null))).ToArray());
            }
            return table;
        }

        private static bool EventTypeFilter(System.Reflection.PropertyInfo p)
        {
            var attribute = Attribute.GetCustomAttribute(p,
                typeof(AssociationAttribute)) as AssociationAttribute;

            if (attribute == null) return true;
            if (attribute.IsForeignKey == false) return true;

            return false;
        }

        private static object GetPropertyValue(object o)
        {
            if (o == null)
                return DBNull.Value;
            return o;
        }
    }
}
