﻿namespace CookBook.DataAdapter.Base.Contracts
{
    public interface ITransactable
    {
        IRepositoryTransactionManager RepositoryTransactionManager { get; set; }
    }
}
