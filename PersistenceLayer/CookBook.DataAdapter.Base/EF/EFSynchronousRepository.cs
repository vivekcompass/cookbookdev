﻿using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Base.DataEnums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace CookBook.DataAdapter.Base
{
    /// <summary>
    /// Extends SerialCrudServiceBase, but makes a strongly typed DB Context available to sub-types, along with upsert and delete
    /// </summary>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    /// <typeparam name="TDbContext">The type of the database context.</typeparam>
    public abstract class EFSynchronousRepository<TKey, TModel, TDbContext> : SynchronousRepositoryBase<TKey, TModel>, IDisposable
        where TDbContext : DbContext, new()
        where TModel : class, IKeyedModel<TKey>
    {
        protected EFSynchronousRepository()
        {
        }

        protected TDbContext GetContext()
        {
            //if
            //var efTransaction = transaction as EFTransaction;
            //if (efTransaction == null)
            //{
            //    throw new ArgumentException(string.Format("EF Repository GetContext invoked with {0} transaction", transaction.GetType().Name));
            //}
            TDbContext entityContext = null;
            EFTransaction entityTransaction = null;
            try
            {
                //using (var efTransaction = new EFTransaction())
                //{
                string nameOrConnectionString = null;
                var repositoryTransactionManager = this.RepositoryTransactionManager as EFRepositoryTransactionManager;
                if (repositoryTransactionManager != null)
                {
                    entityTransaction = repositoryTransactionManager.BeginChanges() as EFTransaction;
                    if (entityTransaction == null)
                    {
                        throw new ArgumentException("EF Repository GetContext invoked with null transaction");
                    }
                    nameOrConnectionString = repositoryTransactionManager.GetNameOrConfigurationString(entityContext);
                }
                entityContext = (TDbContext)entityTransaction.GetContext(entityContext, nameOrConnectionString);
                entityContext.Database.CommandTimeout = 600;
            }
            catch (Exception ex)
            {
                if (entityTransaction != null)
                {
                    entityTransaction.Dispose();
                    entityTransaction = null;
                }
                throw ex;
            }
            //}
            return entityContext;
        }

        protected TDbContext GetContext(ITransaction transaction, string sessionSectorName)
        {
            var entityTransaction = transaction as EFTransaction;
            if (entityTransaction == null)
            {
                throw new ArgumentException(string.Format("EF Repository GetContext invoked with {0} transaction", transaction != null ? transaction.GetType().Name : "null"));
            }
            TDbContext entityContext = null;

            string nameOrConnectionString = null;
            var repositoryTransactionManager = this.RepositoryTransactionManager as EFRepositoryTransactionManager;
            if (repositoryTransactionManager != null)
            {
                nameOrConnectionString = repositoryTransactionManager.GetNameOrConfigurationString(entityContext);
            }
            entityContext = (TDbContext)entityTransaction.GetContext(entityContext, nameOrConnectionString);
            entityContext.Database.CommandTimeout = 600;

            try
            {
                string CommonDatabase = ConfigurationManager.AppSettings["CommonDatabase"];
                string CoreDatabase = ConfigurationManager.AppSettings["CoreDatabase"];
                string ManufacturingDatabase = ConfigurationManager.AppSettings["ManufacturingDatabase"];
                string GoogleDatabase = ConfigurationManager.AppSettings["GoogleDatabase"];
                string AggregationDatabase = ConfigurationManager.AppSettings["AggregationDatabase"];
                string HealthcareDatabase = ConfigurationManager.AppSettings["HealthcareDatabase"];
                string HorecaDatabase = ConfigurationManager.AppSettings["HorecaDatabase"];
                string EducationDatabase = ConfigurationManager.AppSettings["EducationDatabase"];
                string DTRWA_144O = ConfigurationManager.AppSettings["DTRWA_144O"];
                string DTRWA_150F = ConfigurationManager.AppSettings["DTRWA_150F"];
                string DTRWA_144S = ConfigurationManager.AppSettings["DTRWA_144S"];
                string DTRWA_144P = ConfigurationManager.AppSettings["DTRWA_144P"];
                string DTRWA_144T = ConfigurationManager.AppSettings["DTRWA_144T"];


                string connectionString = entityContext.Database.Connection.ConnectionString;
                SqlConnectionStringBuilder sqlConnectionString = new SqlConnectionStringBuilder(connectionString);
                if (sqlConnectionString.InitialCatalog != CommonDatabase)
                {
                    sessionSectorName = sessionSectorName.ToUpper();
                    if (sessionSectorName == "FS - MANUFACTURING")
                    {
                        sqlConnectionString.InitialCatalog = ManufacturingDatabase;
                    }
                    else if (sessionSectorName == "FS - CORE")
                    {
                        sqlConnectionString.InitialCatalog = CoreDatabase;
                    }
                    else if (sessionSectorName == "FS - GOOGLE")
                    {
                        sqlConnectionString.InitialCatalog = GoogleDatabase;
                    }
                    else if (sessionSectorName == "AGGREGATION")
                    {
                        sqlConnectionString.InitialCatalog = AggregationDatabase;
                    }
                    else if (sessionSectorName == "HEALTHCARE")
                    {
                        sqlConnectionString.InitialCatalog = HealthcareDatabase;
                    }
                    else if (sessionSectorName == "PRODUCT TEAM")
                    {
                        sqlConnectionString.InitialCatalog = AggregationDatabase;
                    }
                    else if (sessionSectorName == "HORECA")
                    {
                        sqlConnectionString.InitialCatalog = HorecaDatabase;
                    }
                    else if (sessionSectorName == "EDUCATION")
                    {
                        sqlConnectionString.InitialCatalog = EducationDatabase;
                    }
                    else if (sessionSectorName == "DTRWA_144O")
                    {
                        sqlConnectionString.InitialCatalog = DTRWA_144O;
                    }
                    else if (sessionSectorName == "DTRWA_150F")
                    {
                        sqlConnectionString.InitialCatalog = DTRWA_150F;
                    }
                    else if (sessionSectorName == "DTRWA_144S")
                    {
                        sqlConnectionString.InitialCatalog = DTRWA_144S;
                    }
                    else if (sessionSectorName == "DTRWA_144P")
                    {
                        sqlConnectionString.InitialCatalog = DTRWA_144P;
                    }
                    else if (sessionSectorName == "DTRWA_144T")
                    {
                        sqlConnectionString.InitialCatalog = DTRWA_144T;
                    }
                    else
                    {
                        sqlConnectionString.InitialCatalog = CoreDatabase;
                    }
                    entityContext.Database.Connection.ConnectionString = sqlConnectionString.ConnectionString;
                }
            }
            catch
            {

            }

            return entityContext;
        }


        private DbSet<TModel> GetDbSet(ITransaction transaction, string sessionSectorName)
        {
            return this.GetDbSet(this.GetContext(transaction, sessionSectorName));
        }

        protected override IQueryable<TModel> Expand(IQueryable<TModel> query, string path)
        {
            return query.Include(path);
        }

        protected override IQueryable<TModel> GetAllImpl(ITransaction externalTransaction, string sessionSectorName)
        {
            return this.GetDbSet(externalTransaction, sessionSectorName).AsQueryable();
        }

        protected override IQueryable<TModel> GetAllImpl(ITransaction externalTransaction, string sessionSectorName, string[] include)
        {
            var data = this.GetDbSet(externalTransaction, sessionSectorName).AsQueryable();
            if (include != null)
            {
                foreach (string entityName in include)
                {
                    data = data.Include(entityName);
                }
            }
            return data;
        }

        protected override TModel FindImpl(TKey key, ITransaction externalTransaction, string sessionSectorName)
        {
            return this.FindImpl(key, this.GetContext(externalTransaction, sessionSectorName));
        }

        protected override TModel FindImplWithExpand(TKey key, string includeQueryPath, ITransaction externalTransaction, string sessionSectorName)
        {
            return this.FindImplWithExpand(key, this.GetDbSet(externalTransaction, sessionSectorName), includeQueryPath);
        }

        protected TModel FindImpl(TKey key, TDbContext entityContext)
        {
            return this.FindImpl(key, this.GetDbSet(entityContext));
        }

        protected override RepositoryAction DetermineSaveAction(TModel model, ITransaction externalTransaction, string sessionSectorName)
        {
            RepositoryAction action = RepositoryAction.Create;

            var entityContext = this.GetContext(externalTransaction, sessionSectorName);
            TModel old = this.FindImpl(model.Key, entityContext);
            if (old != null)
            {
                action = RepositoryAction.Update;
            }

            return action;
        }

        protected override bool SaveCreateImpl(TModel model, ITransaction externalTransaction, string sessionSectorName)
        {
            return this.EFSaveCreateImpl(model, externalTransaction, sessionSectorName);
        }

        protected virtual bool EFSaveCreateImpl(TModel model, ITransaction externalTransaction, string sessionSectorName)
        {
            this.GetDbSet(externalTransaction, sessionSectorName).Add(model);

            return true;
        }

        protected virtual bool EFExecuteCommand(string command, ITransaction externalTransaction, string sessionSectorName, params object[] parameters)
        {
            return this.GetContext(externalTransaction, sessionSectorName).Database.ExecuteSqlCommand(command, parameters) > 0;
        }

        protected override bool SaveUpdateImpl(TModel model, ITransaction externalTransaction, string sessionSectorName)
        {
            return this.EFSaveUpdateImpl(model, externalTransaction, sessionSectorName);
        }

        protected override bool ExecuteCommand(string command, ITransaction externalTransaction, string sessionSectorName, params object[] parameters)
        {
            return this.EFExecuteCommand(command, externalTransaction, sessionSectorName, parameters);
        }

        protected virtual bool EFSaveUpdateImpl(TModel model, ITransaction externalTransaction, string sessionSectorName)
        {
            var entityContext = this.GetContext(externalTransaction, sessionSectorName);
            TModel old = this.FindImpl(model.Key, entityContext);
            entityContext.Entry(old).CurrentValues.SetValues(model);
            return true;
        }

        protected override bool DeleteImpl(TModel model, ITransaction externalTransaction, string sessionSectorName)
        {
            var dbSet = this.GetDbSet(externalTransaction, sessionSectorName);
            dbSet.Attach(model);
            dbSet.Remove(model);

            return true;
        }

        protected override IEnumerable<TData> ExecuteDbCommandImpl<TData>(string commandName, params object[] parameters)
        {
            IEnumerable<TData> result = null;

            using (var repoContext = GetContext())
            {
                if (parameters == null)
                {
                    result = repoContext.Database.SqlQuery<TData>(string.Format("Exec {0}", commandName)).ToList();
                }
                else
                {
                    result = repoContext.Database.SqlQuery<TData>(string.Format("Exec {0}", commandName), parameters).ToList();
                }
            }
            return result;
        }

        protected override bool ExecuteNonQueryCommandImpl(string commandName, bool ensureTransaction = false, params object[] parameters)
        {
            bool isSuccess = false;
            using (var repoContext = GetContext())
            {
                if (!ensureTransaction)
                {
                    isSuccess = repoContext.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, string.Format("Exec {0}", commandName), parameters) > 0;
                }
                else
                {
                    isSuccess = repoContext.Database.ExecuteSqlCommand(string.Format("Exec {0}", commandName), parameters) > 0;
                }
            }
            return isSuccess;
        }

        protected override int ExecuteNonQueryImpl(string commandName, bool ensureTransaction = false, params object[] parameters)
        {
            int affectedRecords;
            using (var repoContext = GetContext())
            {
                if (!ensureTransaction)
                {
                    try
                    {
                        affectedRecords = repoContext.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, string.Format("Exec {0}", commandName), parameters);
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                        throw;
                    }
                }
                else
                {
                    affectedRecords = repoContext.Database.ExecuteSqlCommand(string.Format("Exec {0}", commandName), parameters);
                }
            }
            return affectedRecords;
        }

        protected override T ExecuteScalarCommandImpl<T>(string commandName, string sessionSectorName, params object[] parameters)
        {
            T scalarRecord;
            using (var repoContext = GetContext())
            {
                if (parameters == null)
                {
                    DataTable table = ExecuteDataTableCommand(commandName, sessionSectorName, null);
                    if (table != null && table.Rows.Count > 0)
                    {
                        scalarRecord = (T)table.Rows[0][0];
                    }
                }
                scalarRecord = repoContext.Database.SqlQuery<T>(string.Format("Exec {0}", commandName), parameters).FirstOrDefault();
            }
            return scalarRecord;
        }

        protected override IEnumerable<T> ExecuteSqlInlineQueryImpl<T>(string query)
        {
            IEnumerable<T> result;
            using (var repoContext = GetContext())
            {
                result = repoContext.Database.SqlQuery<T>(query).ToList();
            }
            return result;
        }

        protected override int ExecuteSqlInlineNonQueryImpl(string query, params object[] parameters)
        {
            int affectedRecords;
            using (var repoContext = GetContext())
            {
                if (parameters == null)
                {
                    affectedRecords = repoContext.Database.ExecuteSqlCommand(query);
                }
                else
                {
                    affectedRecords = repoContext.Database.ExecuteSqlCommand(query, parameters);
                }
            }
            return affectedRecords;
        }


        //public EFRepositoryTransactionManager RepositoryTransactionManager { get; set; }
        /// <summary>
        /// Execute the sql command to get the data table 
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="externalTransaction">external transaction</param>
        /// <param name="tableToFill">table to fill</param>
        /// <param name="parameters">command parameters</param>
        /// <returns>returns data table</returns>
        protected override DataTable ExecuteDataTableCommandImpl(string commandName, string sessionSectorName, ITransaction externalTransaction, DataTable tableToFill, params object[] parameters)
        {
            if (tableToFill == null)
            {
                throw new ArgumentException("Provided Data Table tableToFill instance is null.");
            }
            // Satisfies rule: SetLocaleForDataTypes.
            tableToFill.Locale = CultureInfo.InvariantCulture;
            using (var repoContext = GetContext())
            {
                try
                {
                    string CommonDatabase = ConfigurationManager.AppSettings["CommonDatabase"];
                    string CoreDatabase = ConfigurationManager.AppSettings["CoreDatabase"];
                    string ManufacturingDatabase = ConfigurationManager.AppSettings["ManufacturingDatabase"];
                    string GoogleDatabase = ConfigurationManager.AppSettings["GoogleDatabase"];
                    string AggregationDatabase = ConfigurationManager.AppSettings["AggregationDatabase"];
                    string HealthcareDatabase = ConfigurationManager.AppSettings["HealthcareDatabase"];
                    string HorecaDatabase = ConfigurationManager.AppSettings["HorecaDatabase"];
                    string EducationDatabase = ConfigurationManager.AppSettings["EducationDatabase"];
                    string DTRWA_144O = ConfigurationManager.AppSettings["DTRWA_144O"];
                    string DTRWA_150F = ConfigurationManager.AppSettings["DTRWA_150F"];
                    string DTRWA_144S = ConfigurationManager.AppSettings["DTRWA_144S"];
                    string DTRWA_144P = ConfigurationManager.AppSettings["DTRWA_144P"];
                    string DTRWA_144T = ConfigurationManager.AppSettings["DTRWA_144T"];

                    string connectionString = repoContext.Database.Connection.ConnectionString;
                    SqlConnectionStringBuilder sqlConnectionString = new SqlConnectionStringBuilder(connectionString);
                    if (sqlConnectionString.InitialCatalog != CommonDatabase)
                    {
                        sessionSectorName = sessionSectorName.ToUpper();
                        if (sessionSectorName == "FS - MANUFACTURING")
                        {
                            sqlConnectionString.InitialCatalog = ManufacturingDatabase;
                        }
                        else if (sessionSectorName == "FS - CORE")
                        {
                            sqlConnectionString.InitialCatalog = CoreDatabase;
                        }
                        else if (sessionSectorName == "FS - GOOGLE")
                        {
                            sqlConnectionString.InitialCatalog = GoogleDatabase;
                        }
                        else if (sessionSectorName == "AGGREGATION")
                        {
                            sqlConnectionString.InitialCatalog = AggregationDatabase;
                        }
                        else if (sessionSectorName == "HEALTHCARE")
                        {
                            sqlConnectionString.InitialCatalog = HealthcareDatabase;
                        }
                        else if (sessionSectorName == "PRODUCT TEAM")
                        {
                            sqlConnectionString.InitialCatalog = AggregationDatabase;
                        }
                        else if (sessionSectorName == "HORECA")
                        {
                            sqlConnectionString.InitialCatalog = HorecaDatabase;
                        }
                        else if (sessionSectorName == "EDUCATION")
                        {
                            sqlConnectionString.InitialCatalog = EducationDatabase;
                        }
                        else if (sessionSectorName == "DTRWA_144O")
                        {
                            sqlConnectionString.InitialCatalog = DTRWA_144O;
                        }
                        else if (sessionSectorName == "DTRWA_150F")
                        {
                            sqlConnectionString.InitialCatalog = DTRWA_150F;
                        }
                        else if (sessionSectorName == "DTRWA_144S")
                        {
                            sqlConnectionString.InitialCatalog = DTRWA_144S;
                        }
                        else if (sessionSectorName == "DTRWA_144P")
                        {
                            sqlConnectionString.InitialCatalog = DTRWA_144P;
                        }
                        else if (sessionSectorName == "DTRWA_144T")
                        {
                            sqlConnectionString.InitialCatalog = DTRWA_144T;
                        }
                        else
                        {
                            sqlConnectionString.InitialCatalog = CoreDatabase;
                        }
                        repoContext.Database.Connection.ConnectionString = sqlConnectionString.ConnectionString;
                    }
                }
                catch
                {

                }


                //var context = this.GetContext(transaction);
                var conn = repoContext.Database.Connection;
                var connectionState = conn.State;
                try
                {
                    //using (context)
                    //{
                    if (connectionState != ConnectionState.Open)
                    {
                        conn.Open();
                    }
                    using (var cmd = conn.CreateCommand())
                    {

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        cmd.CommandText = commandName;
                       // cmd.CommandTimeout = 1000000;
                        using (var reader = cmd.ExecuteReader())
                        {
                            tableToFill.Load(reader);
                        }
                    }
                    //}
                }
                finally
                {
                    if (connectionState != ConnectionState.Open)
                    {
                        conn.Close();
                    }
                }
            }
            return tableToFill;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                RepositoryTransactionManager = null;
            }

        }

        //public EFRepositoryTransactionManager RepositoryTransactionManager { get; set; }

        #region Abstract Methods

        protected abstract DbSet<TModel> GetDbSet(TDbContext entityContext);
        protected abstract TModel FindImpl(TKey key, DbSet<TModel> dbSet);
        protected abstract TModel FindImplWithExpand(TKey key, DbSet<TModel> dbSet, string includeQueryPath);

        #endregion
    }
}
