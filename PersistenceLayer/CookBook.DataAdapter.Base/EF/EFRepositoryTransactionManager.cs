﻿using CookBook.DataAdapter.Base.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace CookBook.DataAdapter.Base
{
    /// <summary>
    /// This is mostly a factory class that simply instantiates the transactions, so it's sharable
    /// </summary>
    [Export(typeof(IRepositoryTransactionManager))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class EFRepositoryTransactionManager : RepositoryTransactionManagerBase
    {
        private Dictionary<Type, string> contextNamesOrConnectionStrings = new Dictionary<Type, string>();

        public void AddNameOrConnectionString<TDbContext>(TDbContext entityContext, string nameOrConnectionString)
        {
            contextNamesOrConnectionStrings.Add(entityContext.GetType(), nameOrConnectionString);
        }

        public string GetNameOrConfigurationString<TDbContext>(TDbContext entityContext)
        {
            string nameOrConnectionString = null;
            if (entityContext == null)
            {
                entityContext = Activator.CreateInstance<TDbContext>();
            }
            var dbContextType = entityContext.GetType();
            if (contextNamesOrConnectionStrings.ContainsKey(dbContextType))
            {
                nameOrConnectionString = contextNamesOrConnectionStrings[dbContextType];
            }
            return nameOrConnectionString;
        }

        public override ITransaction BeginChanges()
        {
            ExecutionStrategyDbConfiguration.SuspendExecutionStrategy(true);
            return new EFTransaction();
        }

    }
}
