﻿using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Base.DataEnums;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;

namespace CookBook.DataAdapter.Base
{
    public abstract class CompositeRepositoryBase<TModel> : ICompositeCrudRepository<TModel>
    {
        private object serviceLock = new object();

        [Dependency]
        public IRepositoryTransactionManager RepositoryTransactionManager { get; set; }

        public IEnumerable<TModel> Find(Expression<Func<TModel, bool>> filterExpression, ITransaction externalTransaction = null, IEnumerable<string> expandPaths = null)
        {
            IEnumerable<TModel> result = null;
            lock (serviceLock)
            {
                using (externalTransaction = GetTransaction(externalTransaction))
                {
                    result = GetQueryable(externalTransaction).Where(filterExpression);
                    if (expandPaths != null)
                    {
                        foreach (var expandPath in expandPaths)
                        {
                            result = Expand(result as IQueryable<TModel>, expandPath);
                        }
                    }
                    if (result != null)
                    {
                        result = result.ToList();
                    }
                }

            }
            return result;
        }

        protected abstract IQueryable<TModel> Expand(IQueryable<TModel> query, string path);

        protected abstract bool ExecuteCommand(string command, ITransaction externalTransaction, params object[] parameters);

        public TModel Find(Dictionary<string, dynamic> keys, ITransaction externalTransaction = null)
        {
            TModel result = default(TModel);
            lock (serviceLock)
            {
                using (externalTransaction = GetTransaction(externalTransaction))
                {
                    result = FindImpl(keys, externalTransaction);
                }
            }
            return result;
        }

        protected abstract TModel FindImpl(Dictionary<string, dynamic> keys, ITransaction externalTransaction);

        public IEnumerable<TModel> GetAll(ITransaction externalTransaction = null)
        {
            IEnumerable<TModel> result = null;
            lock (serviceLock)
            {
                using (externalTransaction = GetTransaction(externalTransaction))
                {
                    result = GetAllImpl(externalTransaction);
                }
            }
            return result;
        }

        public IQueryable<TModel> GetQueryable(ITransaction externalTransaction)
        {
            IQueryable<TModel> result = null;
            Debug.Assert(externalTransaction != null, "GetQueryable invoked with no transaction");
            lock (serviceLock)
            {
                result = GetAllImpl(externalTransaction);
            }
            return result;
        }

        protected abstract IQueryable<TModel> GetAllImpl(ITransaction externalTransaction);
        protected abstract IQueryable<TModel> GetAllImpl(ITransaction externalTransaction, string[] include);

        /// <summary>
        /// Validates the model. Throws an exception if 
        /// </summary>
        /// <param name="model">The model.</param>
        protected virtual void ValidateModel(TModel model)
        {
        }


        public bool Save(TModel model, ITransaction externalTransaction = null)
        {
            bool success = false;
            lock (serviceLock)
            {
                bool localTransaction = false;
                //AzureExecutionStrategyDbConfiguration.
                if (externalTransaction == null)
                {
                    localTransaction = true;
                }
                using (externalTransaction = GetTransaction(externalTransaction))
                {
                    ValidateModel(model);
                    var action = DetermineSaveAction(model, externalTransaction);

                    switch (action)
                    {
                        case RepositoryAction.Create:
                            success = SaveCreate(model, externalTransaction);
                            break;

                        case RepositoryAction.Update:
                            success = SaveUpdate(model, externalTransaction);
                            break;

                        default:
                            throw new InvalidOperationException(string.Format("Save yielded {0} action", action));
                    }

                    if (localTransaction && success)
                    {
                        externalTransaction.Commit();
                    }
                }
            }
            return success;
        }

        protected abstract RepositoryAction DetermineSaveAction(TModel model, ITransaction externalTransaction);

        private bool SaveCreate(TModel model, ITransaction externalTransaction)
        {
            return SaveCreateImpl(model, externalTransaction);
        }
        protected abstract bool SaveCreateImpl(TModel model, ITransaction externalTransaction);

        private bool SaveUpdate(TModel model, ITransaction externalTransaction)
        {
            return SaveUpdateImpl(model, externalTransaction);
        }
        protected abstract bool SaveUpdateImpl(TModel model, ITransaction externalTransaction);

        public bool Delete(TModel model, ITransaction externalTransaction = null)
        {
            bool success = false;
            lock (serviceLock)
            {
                bool localTransaction = false;
                if (externalTransaction == null)
                {
                    localTransaction = true;
                }
                using (externalTransaction = GetTransaction(externalTransaction))
                {
                    success = DeleteImpl(model, externalTransaction);

                    if (localTransaction && success)
                    {
                        externalTransaction.Commit();
                    }
                }
            }
            return success;
        }

        public bool ExecuteSqlCommand(string command, ITransaction externalTransaction = null, params object[] parameters)
        {
            bool success = false;
            lock (serviceLock)
            {
                bool localTransaction = false;
                if (externalTransaction == null)
                {
                    localTransaction = true;
                }
                using (externalTransaction = GetTransaction(externalTransaction))
                {
                    success = ExecuteCommand(command, externalTransaction, parameters);

                    if (localTransaction && success)
                    {
                        externalTransaction.Commit();
                    }
                }
            }
            return success;
        }

        public IEnumerable<TData> ExecuteDbCommand<TData>(string commandName, string sessionSectorName, params object[] parameters)
        {
            return ExecuteDbCommandImpl<TData>(commandName, sessionSectorName, parameters);
        }

        /// <summary>
        /// Execute the non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="ensureTransaction">ensure transaction</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns boolean status</returns>
        public bool ExecuteNonQueryCommand(string commandName, bool ensureTransaction = false, params object[] parameters)
        {
            return ExecuteNonQueryCommandImpl(commandName, ensureTransaction, parameters);
        }

        /// <summary>
        /// Execute the non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns boolean status</returns>
        public bool ExecuteNonQueryCommand(string commandName, params object[] parameters)
        {
            return ExecuteNonQueryCommandImpl(commandName, false, parameters);
        }

        /// <summary>
        /// Execute the non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="ensureTransaction">ensure transaction</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns boolean status</returns>
        public int ExecuteNonQuery(string commandName, bool ensureTransaction = false, params object[] parameters)
        {
            return ExecuteNonQueryImpl(commandName, ensureTransaction, parameters);
        }

        /// <summary>
        /// Execute the non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns boolean status</returns>
        public int ExecuteNonQuery(string commandName, params object[] parameters)
        {
            return ExecuteNonQueryImpl(commandName, false, parameters);
        }

        /// <summary>
        /// Method to execute the scalar command values
        /// </summary>
        /// <typeparam name="T">generic type parameter</typeparam>
        /// <param name="commandName">command name</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns scalar value</returns>
        public T ExecuteScalarCommand<T>(string commandName, string sessionSectorName, params object[] parameters)
        {
            return ExecuteScalarCommandImpl<T>(commandName, parameters);
        }

        /// <summary>
        /// Method to retuns collection using inline sql query
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="query">sql query</param>
        /// <returns>returns entity collection</returns>
        public IEnumerable<T> ExecuteSqlInlineQuery<T>(string query)
        {
            return ExecuteSqlInlineQueryImpl<T>(query);
        }

        /// <summary>
        /// Execute the sql command to get the data table 
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="tableToFill">table to fill</param>
        /// <param name="parameters">command parameters</param>
        /// <returns>returns data table</returns>
        public DataTable ExecuteDataTableCommand(string commandName, string sessionSectorName, DataTable tableToFill, params object[] parameters)
        {
            return ExecuteDataTableCommandImpl(commandName, sessionSectorName, null, tableToFill, parameters);
        }

        /// <summary>
        /// Method to execute the scalar command values
        /// </summary>
        /// <param name="query">query</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns scalar value</returns>
        public int ExecuteSqlInlineNonQuery(string query, params object[] parameters)
        {
            return ExecuteSqlInlineNonQueryImpl(query, parameters);
        }

        protected virtual ITransaction GetTransaction(ITransaction externalTransaction)
        {
            if (externalTransaction == null)
            {
                if (this.RepositoryTransactionManager == null)
                {
                    externalTransaction = new EFTransaction();
                }
                else
                {
                    externalTransaction = this.RepositoryTransactionManager.BeginChanges();
                }
            }
            return externalTransaction;
        }

        protected abstract IEnumerable<TData> ExecuteDbCommandImpl<TData>(string commandName, string sessionSectorName, params object[] parameters);
        protected abstract bool ExecuteNonQueryCommandImpl(string commandName, bool ensureTransaction = false, params object[] parameters);
        protected abstract int ExecuteNonQueryImpl(string commandName, bool ensureTransaction = false, params object[] parameters);
        protected abstract T ExecuteScalarCommandImpl<T>(string commandName, params object[] parameters);
        protected abstract IEnumerable<T> ExecuteSqlInlineQueryImpl<T>(string query);
        protected abstract DataTable ExecuteDataTableCommandImpl(string commandName, string sessionSectorName, ITransaction externalTransaction, DataTable tableToFill, params object[] parameters);
        protected abstract bool DeleteImpl(TModel model, ITransaction externalTransaction);
        protected abstract int ExecuteSqlInlineNonQueryImpl(string query, params object[] parameters);
    }
}
