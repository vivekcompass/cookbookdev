﻿using System;
using System.Data.Entity;

namespace CookBook.DataAdapter.Base
{
    public class EFContextBase
    {

        public EFContextBase()
        {

        }

        protected TDbContext GetContext<TDbContext>() where TDbContext : DbContext
        {
            TDbContext context = null;
            Type dbContextType = typeof(TDbContext);
            context = (TDbContext)Activator.CreateInstance(dbContextType);
            return context;
        }
    }
}
