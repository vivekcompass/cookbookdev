﻿using CookBook.DataAdapter.Base.Model;
using System;
using System.Collections.Generic;

namespace CookBook.DataAdapter.Base
{
    public sealed class ConnectionDataProvider
    {
        private static volatile ConnectionDataProvider current;
        private static object syncRoot = new Object();

        private Dictionary<string, DatabaseConnectionInfo> connectionDictionary = new Dictionary<string, DatabaseConnectionInfo>();

        private ConnectionDataProvider(IList<DatabaseConnectionInfo> databaseConnections)
        {
            foreach (var connection in databaseConnections)
            {
                connectionDictionary.Add(connection.ClientCode, connection);
                // _dicGuidByTpaID.Add(info.TpaID, info.OrganizationGuid);
            }
        }

        public DatabaseConnectionInfo this[string index]
        {
            get { return GetClientConnectionInfo(index); }
        }

        private DatabaseConnectionInfo GetClientConnectionInfo(string code)
        {
            if (!connectionDictionary.ContainsKey(code))
            {
                //log and reload!
                var dbInfo = ReloadClientConnectionInfo();
                SetCurrent(dbInfo);
            }
            return connectionDictionary[code];
        }

        private static IList<DatabaseConnectionInfo> ReloadClientConnectionInfo()
        {
            return new List<DatabaseConnectionInfo>();
        }

        private static void SetCurrent(IList<DatabaseConnectionInfo> dbInfo)
        {
            lock (syncRoot)
            {
                current = new ConnectionDataProvider(dbInfo);
            }
        }

        public static ConnectionDataProvider Current
        {
            get
            {
                if (current == null)
                {
                    lock (syncRoot)
                    {
                        if (current == null)
                        {
                            IList<DatabaseConnectionInfo> dbInfo = ReloadClientConnectionInfo();
                            current = new ConnectionDataProvider(dbInfo);
                        }
                    }
                }

                return current;
            }
        }
    }
}
