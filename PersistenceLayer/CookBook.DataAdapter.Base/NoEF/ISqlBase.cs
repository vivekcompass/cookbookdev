﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Base.NoEF
{
    public interface ISqlBase
    {
        bool IsDatabaseConnectionActive();
        void InitializeDatabase(string name);
    }
}
