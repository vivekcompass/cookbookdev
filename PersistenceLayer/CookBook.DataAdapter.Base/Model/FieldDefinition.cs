﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Base.Model
{
    internal class FieldDefinition
    {
        public string Column_Name { get; set; }
        public string Data_Type { get; set; }
        public string Data_Length { get; set; }
        public string is_nullable { get; set; }
        public string Constraint_Type { get; set; }
        public int Ordinal_Position { get; set; }
    }
}
