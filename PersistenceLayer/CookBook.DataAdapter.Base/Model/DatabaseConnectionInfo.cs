﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookBook.DataAdapter.Base.DataEnums;

namespace CookBook.DataAdapter.Base.Model
{
    [Serializable]
    public class DatabaseConnectionInfo
    {
        public string ModuleName { get; set; }
        public string ClientCode { get; set; }
        public string DataSourceName { get; set; }
        public string DatabaseName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int AuthenticationModeID { get; set; }
        public string AuthenticationType { get; set; }

        public string ConnectionString
        {
            get
            {
                SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();
                sqlBuilder.DataSource = DataSourceName;
                sqlBuilder.InitialCatalog = DatabaseName;

                sqlBuilder.MultipleActiveResultSets = true;
                sqlBuilder.MaxPoolSize = 100;
                sqlBuilder.MinPoolSize = 10;


                if (AuthenticationModeID == (int)(AuthenticationModes.Windows))
                {
                    sqlBuilder.IntegratedSecurity = true;
                }
                else if (AuthenticationModeID == (int)(AuthenticationModes.SQLServer))
                {
                    sqlBuilder.IntegratedSecurity = false;
                    sqlBuilder.UserID = Username;
                    sqlBuilder.Password = Password;
                }

                return sqlBuilder.ToString();
            }

        }

        public string ReadOnlyConnectionString
        {
            get
            {
                SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder(this.ConnectionString);
                //TODO: Implement this at a later date.
                //sqlBuilder.ApplicationIntent = ApplicationIntent.ReadOnly;

                return sqlBuilder.ToString();
            }
        }

        public string EntityFrameworkConnectionString
        {
            get
            {
                EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();
                entityBuilder.ProviderConnectionString = this.ConnectionString;
                entityBuilder.Provider = "System.Data.SqlClient";
                if (ModuleName == "01")
                {
                    entityBuilder.Metadata = @"res://*/ArchDb.csdl|res://*/ArchDb.ssdl|res://*/ArchDb.msl";
                }
                string connectionString = string.Empty;
                using (EntityConnection con = new EntityConnection(entityBuilder.ToString()))
                {
                    connectionString = con.ConnectionString;
                }
                return connectionString;
                //return entityBuilder.ToString();

            }
        }

        public string ReadOnlyEntityFrameworkConnectionString
        {
            get
            {
                EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();
                entityBuilder.ProviderConnectionString = this.ReadOnlyConnectionString;
                entityBuilder.Provider = "System.Data.SqlClient";
                if (ModuleName == "01")
                {
                    entityBuilder.Metadata = @"res://*/EDMX.ArchDb.csdl|
                                           res://*/EDMX.ArchDb.ssdl|
                                           res://*/EDMX.ArchDb.msl";
                }

                return entityBuilder.ToString();
            }
        }
    }
}
