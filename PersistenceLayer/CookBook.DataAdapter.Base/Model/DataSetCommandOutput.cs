﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Base.Model
{
    public class DataSetCommandOutput : CommandOutput
    {
        public DataSet Data { get; set; }
    }
}
