﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Base.Model
{
    public class SprocAccessorOutput<T> : CommandOutput
        where T : class, new()
    {
        public IEnumerable<T> Data { get; set; }
    }

}
