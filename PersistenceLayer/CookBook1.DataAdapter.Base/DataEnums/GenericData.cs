﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CookBook.DataAdapter.Base.DataEnums
{
    public static class GenericData
    {
        public static string DatabaseName { get; set; }
        public static string SectorNumber { get; set; }
        public static int UserID { get; set; }
    }
}
