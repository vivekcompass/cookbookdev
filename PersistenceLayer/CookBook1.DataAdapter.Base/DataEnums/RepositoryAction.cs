﻿namespace CookBook.DataAdapter.Base.DataEnums
{
    public enum RepositoryAction
    {
        Create,
        Read,
        Update,
        Delete
    }
}
