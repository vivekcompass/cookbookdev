﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CookBook.DataAdapter.Base.Attributes;

namespace CookBook.DataAdapter.Base.Extensions
{
    internal static class AdoDataExtensions
    {
        public static IEnumerable<T> AutoMap<T>(this IDataReader reader)
            where T : class, new()
        {
            LinkedList<T> dataList = new LinkedList<T>();
            PropertyInfo[] properties = typeof(T).GetProperties();
            T item = null;

            Type tAttrib = typeof(DataField);

            if (reader != null)
            {
                while (reader.Read())
                {
                    item = new T();
                    foreach (PropertyInfo property in properties)
                    {
                        var attribute = Attribute.GetCustomAttribute(property, tAttrib) as DataField;
                        if (attribute != null)
                        {
                            if (reader.HasColumn(attribute.TableField ?? property.Name))
                            {
                                var value = CastValue(reader[attribute.TableField ?? property.Name], attribute.SourceType ?? "System.String", property.PropertyType);
                                property.SetValue(item, value, null); // null means no indexes
                            }
                        }
                        else
                        {
                            if (reader.HasColumn(property.Name) && !object.Equals(reader[property.Name], DBNull.Value))
                            {
                                property.SetValue(item, reader[property.Name], null);

                            }
                        }
                    }

                    dataList.AddLast(item);
                }

                reader.Close();
            }
            return dataList;
        }

        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        private static object CastValue(object value, string sourceType, Type targetType)
        {
            //* This implementation requires further extension to convert types as per requirement arose.
            if (sourceType == "System.DateTime")
            {
                if (targetType.Name == "TimeSpan")
                {
                    var dtValue = (DateTime)value;
                    TimeSpan tsValue = dtValue.TimeOfDay;
                    value = tsValue;
                }
            }
            else if (sourceType == "System.String")
            {
                value = value.ToString();
            }
            return value;
        }
    }
}
