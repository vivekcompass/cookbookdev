﻿using System.Collections.Generic;

namespace CookBook.DataAdapter.Base.Contracts
{
    public interface ICompositeModel
    {
        Dictionary<string, dynamic> CompositeKeys { get; }
    }
}
