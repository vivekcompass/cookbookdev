﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CookBook.DataAdapter.Base.Contracts
{
    public interface ICrudRepository<TKey, TModel> : ITransactable, IExecutionObject
    {

        /// <summary>
        /// Finds the record off the key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="externalTransaction">The potential external transaction, otherwise the command will be executed in a local transaction</param>
        /// <returns></returns>
        TModel Find(TKey key, ITransaction externalTransaction = null);

        /// <summary>
        /// Finds the record off the key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="includeQueryPath">table name which needs to be included</param>
        /// <param name="externalTransaction">The potential external transaction, otherwise the command will be executed in a local transaction</param>
        /// <returns></returns>
        TModel FindWithExpand(TKey key, string includeQueryPath, ITransaction externalTransaction = null);

        /// <summary>
        /// Finds this instance based on a query expression.
        /// </summary>
        /// <returns></returns>
        IEnumerable<TModel> Find(Expression<Func<TModel, bool>> filterExpression, ITransaction externalTransaction = null, IEnumerable<string> expandPaths = null);

        /// <summary>
        /// Gets all off the records and potentially immediately executes the query
        /// </summary>
        /// <param name="externalTransaction">The potential external transaction, otherwise the command will be executed in a local transaction</param>
        /// <returns></returns>
        IEnumerable<TModel> GetAll(ITransaction externalTransaction = null);

        /// <summary>
        /// Gets the deferred entity to potentially add additional search clauses upon
        /// </summary>
        /// <param name="externalTransaction">The potential external transaction, otherwise the command will be executed in a local transaction</param>
        /// <returns></returns>
        IQueryable<TModel> GetQueryable(ITransaction externalTransaction);

        /// <summary>
        /// Saves the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="externalTransaction">The potential external transaction, otherwise the command will be executed in a local transaction</param>
        /// <returns></returns>
        bool Save(TModel model, ITransaction externalTransaction = null);

        /// <summary>
        /// Deletes the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="externalTransaction">The potential external transaction, otherwise the command will be executed in a local transaction</param>
        /// <returns></returns>
        bool Delete(TModel model, ITransaction externalTransaction = null);


    }
}
