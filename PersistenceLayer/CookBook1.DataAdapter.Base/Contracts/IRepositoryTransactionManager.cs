﻿using System;

namespace CookBook.DataAdapter.Base.Contracts
{
    /// <summary>
    /// Transaction Manager for repositories that helps with bulk updates.
    /// </summary>
    public interface IRepositoryTransactionManager
    {
        /// <summary>
        /// Start the transaction boundary for changes.
        /// </summary>
        /// <returns></returns>
        ITransaction BeginChanges();

        /// <summary>
        /// Performs the specified action within a transaction.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="localFinalAction">The action to execute if the transaction was local and prior to disposal.</param>
        /// <param name="externalTransaction">A potential transaction passed in.</param>
        /// <returns>returns the booean status</returns>
        bool Perform(Action<ITransaction> action, Action<ITransaction> localFinalAction = null, ITransaction externalTransaction = null);

        bool Perform(Action action, bool isTransactionScope);

        bool PerformWithNoLock(Action action);
    }
}
