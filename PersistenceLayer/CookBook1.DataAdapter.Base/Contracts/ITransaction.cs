﻿using System;

namespace CookBook.DataAdapter.Base.Contracts
{
    public interface ITransaction : IDisposable
    {
        object Commit();
        object Cancel();
    }
}
