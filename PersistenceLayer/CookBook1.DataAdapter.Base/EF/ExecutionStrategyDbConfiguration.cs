﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Base
{
    internal class ExecutionStrategyDbConfiguration : DbConfiguration
    {
        public ExecutionStrategyDbConfiguration()
        {
            this.SetExecutionStrategy("System.Data.SqlClient", () => (IDbExecutionStrategy)new DefaultExecutionStrategy());

        }

        //<summary>
        //Suspend the execution strategy for code that needs to use transactions.
        //</summary>
        //<value>The suspend execution strategy.</value>
        public static void SuspendExecutionStrategy(bool value)
        {
            CallContext.LogicalSetData("SuspendExecutionStrategy", value);
        }

    }
}
