﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using CookBook.DataAdapter.Base.Contracts;
using CookBook.DataAdapter.Base.DataEnums;

namespace CookBook.DataAdapter.Base
{
    /// <summary>
    /// A CRUD service that runs method in serial form and does not allow for concurrent operations.
    /// </summary>
    /// <typeparam name="TKey">The type of the T key.</typeparam>
    /// <typeparam name="TModel">The type of the T model.</typeparam>
    public abstract class SynchronousRepositoryBase<TKey, TModel> : ICrudRepository<TKey, TModel>
    {
        private object serviceLock = new object();

        [Dependency]
        public IRepositoryTransactionManager RepositoryTransactionManager { get; set; }

        public IEnumerable<TModel> Find(Expression<Func<TModel, bool>> filterExpression, ITransaction externalTransaction = null, IEnumerable<string> expandPaths = null)
        {
            IEnumerable<TModel> result = null;

            lock (serviceLock)
            {
                bool localTransaction = false;
                if (externalTransaction == null)
                {
                    localTransaction = true;
                    externalTransaction = this.RepositoryTransactionManager.BeginChanges();
                }

                result = GetQueryable(externalTransaction).Where(filterExpression);
                if (expandPaths != null)
                {
                    foreach (var expandPath in expandPaths)
                    {
                        result = Expand(result as IQueryable<TModel>, expandPath);
                    }
                }

                if (localTransaction)
                {
                    if (result != null)
                    {
                        result = result.ToList();
                    }
                    externalTransaction.Dispose();
                }
            }
            return result;
        }

        public TModel FindWithExpand(TKey key, string includeQueryPath, ITransaction externalTransaction = null)
        {
            TModel result = default(TModel);
            lock (serviceLock)
            {
                bool localTransaction = false;
                if (externalTransaction == null)
                {
                    localTransaction = true;
                    externalTransaction = this.RepositoryTransactionManager.BeginChanges();
                }

                result = FindImplWithExpand(key, includeQueryPath, externalTransaction);

                if (localTransaction)
                {
                    externalTransaction.Dispose();
                }
            }
            return result;
        }

        public TModel Find(TKey key, ITransaction externalTransaction = null)
        {
            TModel result = default(TModel);
            try
            {
                lock (serviceLock)
                {
                    bool localTransaction = false;
                    if (externalTransaction == null)
                    {
                        localTransaction = true;
                        if (this.RepositoryTransactionManager == null)
                        {
                            externalTransaction = GetTransaction(null);
                            if (externalTransaction == null)
                            {
                                throw new ArgumentException("EF Repository GetContext invoked with null transaction");
                            }
                        }
                        else
                        {
                            externalTransaction = this.RepositoryTransactionManager.BeginChanges();
                        }
                    }

                    result = FindImpl(key, externalTransaction);

                    if (localTransaction)
                    {
                        externalTransaction.Dispose();
                    }
                }
            }
            catch(Exception ex)
            {
                if(externalTransaction != null)
                {
                    externalTransaction.Dispose();
                }
                throw ex;
            }
            return result;
        }

        public IEnumerable<TModel> GetAll(ITransaction externalTransaction = null)
        {
            IEnumerable<TModel> result = null;
            try
            {
                lock (serviceLock)
                {
                    bool localTransaction = false;
                    if (externalTransaction == null)
                    {
                        localTransaction = true;
                        if (this.RepositoryTransactionManager == null)
                        {
                            externalTransaction = GetTransaction(null);
                            if (externalTransaction == null)
                            {
                                throw new ArgumentException("EF Repository GetContext invoked with null transaction");
                            }
                        }
                        else
                        {
                            externalTransaction = this.RepositoryTransactionManager.BeginChanges();
                        }
                    }

                    result = GetAllImpl(externalTransaction);

                    if (localTransaction)
                    {
                        // if it's a local transaction, the object will be immediately converted to a list and extracted
                        if (result != null)
                        {
                            result = result.ToList();
                        }
                        externalTransaction.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                if (externalTransaction != null)
                {
                    externalTransaction.Dispose();
                }
                Debug.Write(ex.Message);
                throw;
            }
            return result;
        }

        public IQueryable<TModel> GetQueryable(ITransaction externalTransaction)
        {
            IQueryable<TModel> result = null;
            Debug.Assert(externalTransaction != null, "GetQueryable invoked with no transaction");
            lock (serviceLock)
            {
                result = GetAllImpl(externalTransaction);
            }
            return result;
        }

        /// <summary>
        /// Validates the model. Throws an exception if 
        /// </summary>
        /// <param name="model">The model.</param>
        protected virtual void ValidateModel(TModel model)
        {
        }

        public bool Save(TModel model, ITransaction externalTransaction = null)
        {
            bool success = false;
            lock (serviceLock)
            {
                bool localTransaction = false;
                //AzureExecutionStrategyDbConfiguration.
                if (externalTransaction == null)
                {
                    localTransaction = true;
                    externalTransaction = this.RepositoryTransactionManager.BeginChanges();
                }

                ValidateModel(model);
                var action = DetermineSaveAction(model, externalTransaction);

                switch (action)
                {
                    case RepositoryAction.Create:
                        success = SaveCreate(model, externalTransaction);
                        break;

                    case RepositoryAction.Update:
                        success = SaveUpdate(model, externalTransaction);
                        break;

                    default:
                        throw new InvalidOperationException(string.Format("Save yielded {0} action", action));
                }

                if (localTransaction && success)
                {
                    externalTransaction.Commit();
                }
            }
            return success;
        }

        private bool SaveCreate(TModel model, ITransaction externalTransaction)
        {
            return SaveCreateImpl(model, externalTransaction);
        }


        private bool SaveUpdate(TModel model, ITransaction externalTransaction)
        {
            return SaveUpdateImpl(model, externalTransaction);
        }


        public bool Delete(TModel model, ITransaction externalTransaction = null)
        {
            bool success = false;
            lock (serviceLock)
            {
                bool localTransaction = false;
                if (externalTransaction == null)
                {
                    localTransaction = true;
                    externalTransaction = this.RepositoryTransactionManager.BeginChanges();
                }

                success = DeleteImpl(model, externalTransaction);

                if (localTransaction && success)
                {
                    externalTransaction.Commit();
                }
            }
            return success;
        }

        public bool ExecuteSqlCommand(string command, ITransaction externalTransaction = null, params object[] parameters)
        {
            bool success = false;
            lock (serviceLock)
            {
                bool localTransaction = false;
                if (externalTransaction == null)
                {
                    localTransaction = true;
                    externalTransaction = this.RepositoryTransactionManager.BeginChanges();
                }

                success = ExecuteCommand(command, externalTransaction, parameters);

                if (localTransaction && success)
                {
                    externalTransaction.Commit();
                }
            }
            return success;
        }

        public IEnumerable<TData> ExecuteDbCommand<TData>(string commandName, params object[] parameters)
        {
            return ExecuteDbCommandImpl<TData>(commandName, parameters);
        }

        /// <summary>
        /// Execute the non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="ensureTransaction">ensure transaction</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns boolean status</returns>
        public bool ExecuteNonQueryCommand(string commandName, bool ensureTransaction = false, params object[] parameters)
        {
            return ExecuteNonQueryCommandImpl(commandName, ensureTransaction, parameters);
        }

        /// <summary>
        /// Execute the non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns boolean status</returns>
        public bool ExecuteNonQueryCommand(string commandName, params object[] parameters)
        {
            return ExecuteNonQueryCommandImpl(commandName, false, parameters);
        }

        /// <summary>
        /// Execute the non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="ensureTransaction">ensure transaction</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns boolean status</returns>
        public int ExecuteNonQuery(string commandName, bool ensureTransaction = false, params object[] parameters)
        {
            return ExecuteNonQueryImpl(commandName, ensureTransaction, parameters);
        }

        /// <summary>
        /// Execute the non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns boolean status</returns>
        public int ExecuteNonQuery(string commandName, params object[] parameters)
        {
            return ExecuteNonQueryImpl(commandName, false, parameters);
        }

        /// <summary>
        /// Method to execute the scalar command values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="commandName">command name</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns scalar value</returns>
        public T ExecuteScalarCommand<T>(string commandName, params object[] parameters)
        {
            return ExecuteScalarCommandImpl<T>(commandName, parameters);
        }

        /// <summary>
        /// Method to execute the sql inline queries using entity framework context
        /// </summary>
        /// <param name="query">sql query to execute</param>
        /// <param name="parameters">sql parameters</param>
        /// <returns>returns affected database records count</returns>
        public int ExecuteSqlInlineNonQuery(string query, params object[] parameters)
        {
            return ExecuteSqlInlineNonQueryImpl(query, parameters);
        }

        /// <summary>
        /// Method to retuns collection using inline sql query
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="query">sql query</param>
        /// <returns>returns entity collection</returns>
        public IEnumerable<T> ExecuteSqlInlineQuery<T>(string query)
        {
            return ExecuteSqlInlineQueryImpl<T>(query);
        }

        /// <summary>
        /// Execute the sql command to get the data table 
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="tableToFill">table to fill</param>
        /// <param name="parameters">command parameters</param>
        /// <returns>returns data table</returns>
        public DataTable ExecuteDataTableCommand(string commandName, DataTable tableToFill, params object[] parameters)
        {
            return ExecuteDataTableCommandImpl(commandName, null, tableToFill, parameters);
        }

        protected virtual ITransaction GetTransaction(ITransaction externalTransaction)
        {
            if (externalTransaction == null)
            {
                if (this.RepositoryTransactionManager == null)
                {
                    externalTransaction = new EFTransaction();
                }
                else
                {
                    externalTransaction = this.RepositoryTransactionManager.BeginChanges();
                }
            }
            return externalTransaction;
        }

        #region abstract methods

        protected abstract IQueryable<TModel> Expand(IQueryable<TModel> query, string path);
        protected abstract bool ExecuteCommand(string command, ITransaction externalTransaction, params object[] parameters);
        protected abstract IEnumerable<TData> ExecuteDbCommandImpl<TData>(string commandName, params object[] parameters);
        protected abstract bool ExecuteNonQueryCommandImpl(string commandName, bool ensureTransaction = false, params object[] parameters);
        protected abstract int ExecuteNonQueryImpl(string commandName, bool ensureTransaction = false, params object[] parameters);
        protected abstract T ExecuteScalarCommandImpl<T>(string commandName, params object[] parameters);
        protected abstract IEnumerable<T> ExecuteSqlInlineQueryImpl<T>(string query);
        protected abstract bool DeleteImpl(TModel model, ITransaction externalTransaction);
        protected abstract DataTable ExecuteDataTableCommandImpl(string commandName, ITransaction externalTransaction, DataTable tableToFill, params object[] parameters);
        protected abstract int ExecuteSqlInlineNonQueryImpl(string query, params object[] parameters);
        protected abstract bool SaveCreateImpl(TModel model, ITransaction externalTransaction);
        protected abstract bool SaveUpdateImpl(TModel model, ITransaction externalTransaction);
        protected abstract RepositoryAction DetermineSaveAction(TModel model, ITransaction externalTransaction);
        protected abstract IQueryable<TModel> GetAllImpl(ITransaction externalTransaction);
        protected abstract IQueryable<TModel> GetAllImpl(ITransaction externalTransaction, string[] include);
        protected abstract TModel FindImpl(TKey key, ITransaction externalTransaction);
        protected abstract TModel FindImplWithExpand(TKey key, string includeQueryPath, ITransaction externalTransaction);

        #endregion
    }
}
