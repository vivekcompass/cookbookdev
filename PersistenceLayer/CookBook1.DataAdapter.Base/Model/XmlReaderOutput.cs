﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;

namespace CookBook.DataAdapter.Base.Model
{
    public class XmlReaderOutput : CommandOutput
    {
        public IXPathNavigable Document { get; set; }
    }
}
