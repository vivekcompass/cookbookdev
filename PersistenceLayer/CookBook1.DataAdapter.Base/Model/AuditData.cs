﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Base.Model
{
    public class AuditData
    {
        public long DataAuditID { get; set; }
        public DateTime RevisionStamp { get; set; }
        public string TableName { get; set; }
        public string Username { get; set; }
        public int? UserID { get; set; }
        public string Actions { get; set; }
        public string OldData { get; set; }
        public string NewData { get; set; }
        public string ColumnName { get; set; }
        public IList<AuditDetailData> DataAuditDetails { get; set; }
    }
}
