﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Base.Model
{
    public class AuditDetailData
    {
        public long DataAuditDetailID { get; set; }
        public long DataAuditID { get; set; }
        public string OldData { get; set; }
        public string NewData { get; set; }
        public string ChangedColumn { get; set; }
    }
}
