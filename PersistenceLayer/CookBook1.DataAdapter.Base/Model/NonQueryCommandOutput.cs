﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Base.Model
{
    public class NonQueryCommandOutput : CommandOutput
    {
        /// <summary>
        /// Effected Rows in database, in case if "SET NOCOUNT ON;" then it will have value -1 
        /// </summary>
        public int EffectedRows { get; set; }
        /// <summary>
        /// Wrap the output command parameter values in dictionary, paremeter name goes into key and value in value of dictionary object
        /// </summary>
        public Dictionary<string, object> OutputValues { get; set; }
       
    }
}
