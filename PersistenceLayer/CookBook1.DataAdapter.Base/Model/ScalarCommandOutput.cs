﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DataAdapter.Base.Model
{
    public class ScalarCommandOutput : CommandOutput
    {
        public object Value { get; set; }
    }
}
