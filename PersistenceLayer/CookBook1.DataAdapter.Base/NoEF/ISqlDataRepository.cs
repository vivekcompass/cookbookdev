﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using CookBook.DataAdapter.Base.DataEnums;
using CookBook.DataAdapter.Base.Model;

namespace CookBook.DataAdapter.Base.NoEF
{
    public interface ISqlDataRepository : ISqlBase
    {
        #region Execute Non Query Commands

        /// <summary>
        /// Method to execute the non query command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <returns>returns the command output</returns>
        NonQueryCommandOutput ExecuteNonQuery(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters);

        /// <summary>
        /// Method to execute the non query command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns the command output</returns>
        NonQueryCommandOutput ExecuteNonQuery(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired);

        /// <summary>
        /// Method to execute the non query command using stored procedure name and sql object parameters
        /// </summary>
        /// <param name="storedProcedureName">stored procedure name</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <param name="parameters">sql object parametr array</param>
        /// <returns>returns command output</returns>
        NonQueryCommandOutput ExecuteNonQuery(string storedProcedureName, bool isTransactionRequired, params object[] parameters);

        /// <summary>
        /// Method to execute the sql non query using command type and command text 
        /// </summary>
        /// <param name="commandText">command text to be executed</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns command output</returns>
        NonQueryCommandOutput ExecuteNonQuery(string commandText, SqlCommandTypes commandType, bool isTransactionRequired);

        #endregion

        #region Execute Scalar Commands

        /// <summary>
        /// Method to execute the scalar command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <returns>returns the command output</returns>
        ScalarCommandOutput ExecuteScalar(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters);

        /// <summary>
        /// Method to execute the scalar command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns the command output</returns>
        ScalarCommandOutput ExecuteScalar(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired);

        /// <summary>
        /// Method to execute the scalar command using stored procedure name and sql object parameters
        /// </summary>
        /// <param name="storedProcedureName">stored procedure name</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <param name="parameters">sql object parametr array</param>
        /// <returns>returns command output</returns>
        ScalarCommandOutput ExecuteScalar(string storedProcedureName, bool isTransactionRequired, params object[] parameters);

        /// <summary>
        /// Method to execute the scalar query using command type and command text 
        /// </summary>
        /// <param name="commandText">command text to be executed</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns command output</returns>
        ScalarCommandOutput ExecuteScalar(string commandText, SqlCommandTypes commandType, bool isTransactionRequired);

        #endregion

        #region Execute Reader Commands

        /// <summary>
        /// Method to execute the reader command using name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <returns>returns the command output</returns>
        DataReaderOutput<T> ExecuteReader<T>(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters)
            where T : class,new();

        /// <summary>
        /// Method to execute the reader command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns the command output</returns>
        DataReaderOutput<T> ExecuteReader<T>(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired)
            where T : class,new();

        /// <summary>
        /// Method to execute the reader command using stored procedure name and sql object parameters
        /// </summary>
        /// <param name="storedProcedureName">stored procedure name</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <param name="parameters">sql object parametr array</param>
        /// <returns>returns command output</returns>
        DataReaderOutput<T> ExecuteReader<T>(string storedProcedureName, bool isTransactionRequired, params object[] parameters)
            where T : class,new();

        /// <summary>
        /// Method to execute the data reader using command type and command text 
        /// </summary>
        /// <param name="commandText">command text to be executed</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns command output</returns>
        DataReaderOutput<T> ExecuteReader<T>(string commandText, SqlCommandTypes commandType, bool isTransactionRequired)
            where T : class,new();

        #endregion

        #region Execute Dataset Commands
        /// <summary>
        /// Method to load the data set with specified table name and command text
        /// </summary>
        /// <param name="commandText">command text</param>
        /// <param name="commandType">Sql Command Type</param>
        /// <param name="dataSet">dataset</param>
        /// <param name="tableName">table name</param>
        /// <param name="parameters">sql parameters</param>
        /// <param name="isTransactionRequired">transaction required</param>
        void LoadDataSet(string commandText, SqlCommandTypes commandType, DataSet dataSet, string tableName, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired);

        /// <summary>
        /// Method to load the data set with specified table name and command text
        /// </summary>
        /// <param name="commandText">command text</param>
        /// <param name="commandType">Sql Command Type</param>
        /// <param name="dataSet">dataset</param>
        /// <param name="tableNames">table name</param>
        /// <param name="parameters">sql parameters</param>
        /// <param name="isTransactionRequired">transaction required</param>
        void LoadDataSet(string commandText, SqlCommandTypes commandType, DataSet dataSet, string[] tableNames, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired);

        /// <summary>
        /// Method to execute the Dataset command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <returns>returns the command output</returns>
        DataSetCommandOutput ExecuteDataSet(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters);

        /// <summary>
        /// Method to execute the dataset command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns the command output</returns>
        DataSetCommandOutput ExecuteDataSet(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired);

        /// <summary>
        /// Method to execute the dataset command using stored procedure name and sql object parameters
        /// </summary>
        /// <param name="storedProcedureName">stored procedure name</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <param name="parameters">sql object parametr array</param>
        /// <returns>returns command output</returns>
        DataSetCommandOutput ExecuteDataSet(string storedProcedureName, bool isTransactionRequired, params object[] parameters);

        /// <summary>
        /// Method to execute the dataset query using command type and command text 
        /// </summary>
        /// <param name="commandText">command text to be executed</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns command output</returns>
        DataSetCommandOutput ExecuteDataSet(string commandText, SqlCommandTypes commandType, bool isTransactionRequired);

        #endregion

        #region Stored Procedure Accessor Methods

        /// <summary>
        /// Method to execute the stored procedure and returns the result as IEnumerable T. 
        /// The conversion from data record to T will be done for each property based on matching property name to column name
        /// </summary>
        /// <param name="procedureName">stored procedure name</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <returns>returns the sproc accessor output</returns>
        SprocAccessorOutput<T> ExecuteSprocAccessor<T>(string procedureName, params object[] parameters) where T : class,new();

        /// <summary>
        /// Method to execute the sql string and returns the result as IEnumerable T. 
        /// The conversion from data record to T will be done for each property based on matching property name to column name
        /// </summary>
        /// <param name="sqlString">sql string</param>
        /// <returns>returns the sproc accessor output</returns>
        SprocAccessorOutput<T> ExecuteSqlStringAccessor<T>(string sqlString) where T : class,new();

        /// <summary>
        /// Method to execute the stored procedures and returns the result as IEnumerable T. 
        /// The conversion from data record to T will be done for each property based on matching property name to column name
        /// If mappings is provided then for mapping columns rule will be applicable
        /// </summary>
        /// <param name="procedureName">procedure name</param>
        ///  <param name="mappings"> mappings</param>
        ///  <param name="parameters">sql object parameter list</param>
        /// <returns>returns the sproc accessor output</returns>
        SprocAccessorOutput<T> ExecuteSprocAccessorWithMapping<T>(string procedureName, IRowMapper<T> mappings, params object[] parameters) where T : class, new();

        /// <summary>
        /// Method to execute the sql string and returns the result as IEnumerable T. 
        /// The conversion from data record to T will be done for each property based on matching property name to column name
        /// If mappings is provided then for mapping columns rule will be applicable
        /// </summary>
        /// <param name="sqlString">sql string</param>
        /// <param name="mappings">mappings</param>
        /// <returns>returns the sproc accessor output</returns>
        SprocAccessorOutput<T> ExecuteSqlStringAccessorWithMapping<T>(string sqlString, IRowMapper<T> mappings) where T : class, new();

        #endregion

        #region Xml Reader Commands

        /// <summary>
        /// Method to execute xnl reader on basis of xml query
        /// </summary>
        /// <param name="commandText">xml sql query</param>
        /// <param name="commandType">command type</param>
        /// <param name="isTransactionRequired">is transaction required</param>
        /// <param name="parameters">sql parameters</param>
        /// <returns>returns xml reader output</returns>
        XmlReaderOutput ExecuteXmlReader(string commandText, SqlCommandTypes commandType, bool isTransactionRequired, IEnumerable<SqlCommandParameter> parameters);

        /// <summary>
        /// Method to execute xnl reader on basis of xml query
        /// </summary>
        /// <param name="commandText">xml sql query</param>
        /// <param name="commandType">command type</param>
        /// <param name="isTransactionRequired">is transaction required</param>
        /// <returns>returns xml reader output</returns>
        XmlReaderOutput ExecuteXmlReader(string commandText, SqlCommandTypes commandType, bool isTransactionRequired);

        #endregion
    }
}
