﻿using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            if (filters != null)
            {
                filters.Add(new HandleErrorAttribute());

                // Add a custom filter to set the X-Frame-Options header
                filters.Add(new XFrameOptionsFilter());
            }
        }
    }

    public class XFrameOptionsFilter : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
           // filterContext.HttpContext.Response.Headers.Add("X-Frame-Options", "SAMEORIGIN");
            filterContext.HttpContext.Response.Headers.Add("Content-Security-Policy", "frame-src 'self' https://localhost/CookBook.MvcApp/");
            base.OnResultExecuting(filterContext);
        }
    }
}
