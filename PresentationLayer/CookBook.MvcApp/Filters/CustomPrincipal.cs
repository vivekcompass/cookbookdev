﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace CookBook.MvcApp.Filters
{
    public class CustomPrincipal: GenericPrincipal
    {
        private string vehicleNo;

        public CustomPrincipal(IIdentity identity, string[] roles, string vehicleno)
            : base(identity, roles)
        {
            vehicleNo = vehicleno;
        }

        public string VehicleNo { get { return vehicleNo; } set { vehicleNo = value; } }
    }
}