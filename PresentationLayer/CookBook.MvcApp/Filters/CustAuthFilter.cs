﻿using CookBook.MvcApp.Core;
using CookBook.MvcApp.CookBookService;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using ClosedXML.Excel;
using CookBook.Data.UserData;
using CookBook.Aspects.Utils;
using System.Net;
using System;
using System.IO;
using System.Web.Util;
using Microsoft.Security.Application;
using System.Text.RegularExpressions;

namespace CookBook.MvcApp.Filters
{
    public class CustAuthFilter : AuthorizeAttribute
    {
        ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
        public override void OnAuthorization(AuthorizationContext filterContext)
     {
            if (filterContext.RequestContext.HttpContext.Session["UserLoginDetails"] == null || (filterContext.RequestContext.HttpContext.Session["UserLoginDetails"] != null && ((UserMasterResponseData)filterContext.RequestContext.HttpContext.Session["UserLoginDetails"]).UserId <= 0))
            {
                if (!filterContext.RequestContext.HttpContext.Request.IsAuthenticated)
                {
                    filterContext.RequestContext.HttpContext.GetOwinContext().Authentication.Challenge(
                        new AuthenticationProperties { RedirectUri = ConfigurationManager.AppSettings["redirectUri"].ToString() },
                        OpenIdConnectAuthenticationDefaults.AuthenticationType);
                }
                else
                {
                    //string UserName = User.Identity.Name;
                    var UserInstance = filterContext.RequestContext.HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
                    string UserName = UserInstance.FindFirst("preferred_username").Value;

                    if (string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.TEST_USER]))
                    {
                        UserName = ConfigurationManager.AppSettings[Constants.TEST_USER];
                    }
                    var res = ServiceClient.GetUserData(UserName,null);
                    if (res != null)
                    {
                        res.ProfileName = @System.Security.Claims.ClaimsPrincipal.Current.FindFirst("name").Value;
                        filterContext.RequestContext.HttpContext.Session["UserLoginDetails"] = res;
                        if (res == null && (res != null && res.UserId <= 0))
                        { 
                            filterContext.Result = new RedirectResult("~/Home/NotAuthenticated"); 
                        }
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult("~/Home/NotAuthenticated");
                    }

                    var request = filterContext.HttpContext.Request;
                    var controller = request.RequestContext.RouteData.Values["controller"].ToString();
                    var action = request.RequestContext.RouteData.Values["action"].ToString();
                   

                    var url = filterContext.HttpContext.Request.Url.AbsoluteUri;
                    var method = filterContext.HttpContext.Request.HttpMethod;

                    var isAjaxRequest = request.Headers["X-Requested-With"] == "XMLHttpRequest";
                    var isDirectRequest = !isAjaxRequest;

                }
            }
        }

       

    }

    public class LogRequestDetailsAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            var isAjaxRequest = request.Headers["X-Requested-With"] == "XMLHttpRequest";
            var isDirectRequest = !isAjaxRequest;

            // Log request details
            var logMessage = isAjaxRequest ? "AJAX request detected" : "Direct request detected";
            // Example logging (replace with actual logging code)
            System.Diagnostics.Debug.WriteLine(logMessage);
            // Check if the request is an AJAX request

            if (isDirectRequest)
            {

                //filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                filterContext.Result = new RedirectResult("~/Home/NotAuthenticated");
            }
            base.OnActionExecuted(filterContext);
        }
    }

    public class ValidateJsonXssAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var request = filterContext.HttpContext?.Request;
            if (request != null && "application/json".Equals(request.ContentType, StringComparison.OrdinalIgnoreCase))
            {
                if (request.ContentLength > 0 && request.Form.Count == 0) // 
                {
                    if (request.InputStream.Position > 0)
                        request.InputStream.Position = 0; // InputStream has already been read once from "ProcessRequest"
                    using (var reader = new StreamReader(request.InputStream))
                    {
                        var postedContent = reader.ReadToEnd(); // Get posted JSON content
                        var isValid = RequestValidator.Current.InvokeIsValidRequestString(HttpContext.Current, postedContent,
                            RequestValidationSource.Form, "postedJson", out var failureIndex); // Invoke XSS validation
                        if (!isValid) // Not valid, so throw request validation exception
                            throw new HttpRequestValidationException("Potentially unsafe input detected");
                    }
                }
            }
        }
    }

    public class ValidateXSSAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            foreach (var param in filterContext.ActionParameters)
            {
                if (param.Value != null)
                {
                    // Check if the parameter is a model
                    var properties = param.Value.GetType().GetProperties();

                    foreach (var property in properties)
                    {
                        if (property.PropertyType == typeof(string))
                        {
                            var value = property.GetValue(param.Value) as string;

                            if (!string.IsNullOrEmpty(value) && ContainsScriptTag(value))
                            {
                                //filterContext.Result = new HttpStatusCodeResult(400, "Invalid input detected.");
                                //return;

                                filterContext.Result = new JsonResult
                                {
                                    Data = new { xsssuccess = false, message = "Invalid input detected." },
                                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                                };
                                return;
                            }
                        }
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }

        private bool ContainsScriptTag(string input)
        {
            // Regex to detect <script> tags
            var scriptRegex = new Regex(@"<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>", RegexOptions.IgnoreCase);
            return scriptRegex.IsMatch(input);
        }
    }


    //public class XssFilterAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        var parameters = filterContext.ActionParameters;

    //        foreach (var key in parameters.Keys)
    //        {
    //            var model = parameters[key];
    //            var properties = parameters[key].GetType().GetProperties();

    //            foreach (var property in properties)
    //            {
    //                if (property.PropertyType == typeof(string))
    //                {
    //                    // Sanitize string properties
    //                    var value = property.GetValue(model) as string;
    //                    property.SetValue(model, HttpUtility.HtmlEncode(value));
    //                }
    //            }
    //        }
    //        base.OnActionExecuting(filterContext);
    //    }
    //}
}
    