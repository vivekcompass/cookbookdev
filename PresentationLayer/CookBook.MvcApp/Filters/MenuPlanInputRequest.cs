﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookBook.MvcApp.Filters
{
    public class MenuPlanInputRequest
    {
        public int ServiceTypeId { get; set; }

        public int DishId { get; set; }

        public string Date { get; set; }

        public DateTime SelectedDate { get; set; }
    }
}