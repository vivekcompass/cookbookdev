﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CookBook.MvcApp.Filters
{
    public class MenuRequestFilter
    {
        public int ServiceTypeId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }
    }
}