﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CookBook.Aspects.Factory;
using CookBook.DryIoc;
using CookBook.Ioc.Mvc;
using CookBook.MvcApp.Controllers;

namespace CookBook.MvcApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            LogTraceFactory.InitializeLoggingService();
            ExceptionFactory.InitializeExceptionAopFramework();

            MvcBootstrapper.Initialize(new MvcIocManager().Container);
            //IDryIocManager locator = new DryIocManager(DiscoveryStrategy.SearchBaseDirectory, HostType.Mvc);
        }

        protected void Application_Error()
        {
            Exception ex = Server.GetLastError();
            LogTraceFactory.WriteLogWithCategory(ex.ToString(), Aspects.Constants.LogTraceCategoryNames.Important);
            Response.Redirect("~/Home/Error");
        }
        protected void Session_Start(object sender, EventArgs e)
        {
            Session.Timeout = 100000;
        }

        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
        public class NoDirectAccessAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                if (filterContext.HttpContext.Request.UrlReferrer == null ||
                            filterContext.HttpContext.Request.Url.Host != filterContext.HttpContext.Request.UrlReferrer.Host)
                {
                    filterContext.Result = new RedirectToRouteResult(new
                                   RouteValueDictionary(new { controller = "Home", action = "Index", area = "" }));
                }
            }
        }

        //protected void Application_EndRequest(Object sender, EventArgs e)
        //{
        //    if (Context.Response.StatusCode == 404)
        //    {
        //        Server.ClearError();
        //        Response.Redirect("~/Home/ErrorNotFound");
        //    }
        //    else if (Context.Response.StatusCode == 500)
        //    {
        //        Server.ClearError();
        //        Response.Redirect("~/Home/ErrorInternalServerError");
        //    }
        //}
    }
}
