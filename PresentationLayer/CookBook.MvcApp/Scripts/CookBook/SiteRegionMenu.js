﻿//import { hidden } from "modernizr";

var foodprogramdata = [];
var menuitemdata = [];
var localdata = [];
var itemG = {};
var sitemasterdata = [];
var configmasterdata = [];
var regionmasterdata = [];
var user;
var status = "";
var varname = "";
var datamodel;
var ItemName = "";
var dishcategorydata = [];
var dietcategorydata = [];
var itemObject = {};
var ingredientObject = [];
var dishdata = [];
var masterGroupList = [];
var masterQty = [];
var currentMasterQtyList = [];
var currentMasterList = [];
var masterItems = [];
var finalModel = [];
var finalModelExits = [];
var eid = 0;
var allMenuData = [];
var confingMenuData = [];
var unconfigMasterData = [];
var itemsids = [];
var tr = 0;
var multiarray = [];
var flagAlert = 0;
var finalModelExitsDishCategory = [];
var finalModelDishCategory = [];
var ID = 0;
var flagCurated = 0;
var flagSec = 0;
var uomdata = [];
var uommodulemappingdata = [];
var reasondata = [];
var typedata = [{ value: 1, text: "Standard Quantity" }, { value: 0, text: "Contract Quantity" }]
var flagRegion = 0;
var flagSector = 0;
var flagSite = 0;
var flagSecIsActive = 0;
var dayPartArray = [];
var statuscheck = 0;
$("#ConfigMaster").hide();
var user;
var grid;
var InheritedItem = [];
var siteMasterDataSource = [];
var siteDataSource = [];
var flagToggle = 1;
var checkedIds = {};
var checkedItemCodes = [];
var UniqueItemCodes = [];
var regionID = 0;
var Pub_checkedItemCodes = [];
var siteNonInheritedMasterDataSource = [];
var SiteCode = "";
var selItemDietCategory = 0;
var isPublishBtnClick = false;
var globalChecked = false;
var modelprice = {};
var daypartdata = [];
var daypartdataInactive = [];
var siteDayPartResult = [];
var siteInheritanceData = [];
var SiteDishCategoryMappingData = [];
var SiteItemDishMappingData = [];
var SiteDayPartMappingData = [];
var SiteDishData = [];
var validNumber = new RegExp(/^\d*\.?\d*$/);
var lastValid = 0;
var isDropDownDataLoaded = false;

function validateNumber(elem) {

    if (validNumber.test(elem.value)) {
        lastValid = elem.value;
    } else {
        elem.value = lastValid;
    }
}

    HttpClient.MakeRequest(CookBookMasters.GetSiteItemDropDownDataData, function (data) {
        var result = data;
        daypartdata = [];

      

        sitemasterdata = result.SiteMasterData;
        sitemasterdata.forEach(function (item) {
            item.SiteName = item.SiteName + " - " + item.SiteCode;
            return item;
        })
        //sitemasterdata = sitemasterdata.filter(item => item.IsActive != false);
        if (sitemasterdata.length > 1) {
            sitemasterdata.unshift({ "SiteCode": 0, "SiteName": "Select" });
            $("#btnGo").css("display", "inline-block");
        }
        else {
            $("#btnGo").css("display", "inline-none");


        }
        populateSiteMasterDropdown();
        $("#ConfigMaster").hide();

    }, { regionID: 0 }, false);



kendo.data.DataSource.prototype.dataFiltered = function () {
    // Gets the filter from the dataSource
    var filters = this.filter();

    // Gets the full set of data from the data source
    var allData = this.data();

    // Applies the filter to the data
    var query = new kendo.data.Query(allData);

    // Returns the filtered data
    return query.filter(filters).data;
}

function populateDayPartMultiSelect() {
    $("#inputdaypart").kendoMultiSelect({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "code",
        dataSource: daypartdata,
        index: 0,
        autoClose: false
    });
}
function preSetSector() {
    var flag = 0;
    $("#dishCategory").empty();
    masterGroupList = [];
    CreateDishCategoryOutline(ID, tr.ItemCode);

}
function mainSourceSet(ID) {

    flagSite = 1;
    $("#dishCategory").empty();
    var multiarray1 = [];
    mainSource(ID);

}

function preSetRegion() {
    $("#dishCategory").empty();
    var dropdownlist = $("#inputsite").data("kendoDropDownList");
    var siteCode = dropdownlist.value();
    if (siteCode == 0) {
        Utility.Page_Alert_Warning("Please select the Region to proceed", "Validation", "Ok",
            function () {

            }
        );
        return;
    }
    var itemcode = tr.ItemCode;
    masterGroupList = [];
    CreateRegionDishCategoryOutline(siteCode, itemcode)

}
function removeDuplicateObjectFromArray(array, key) {
    let check = {};
    let res = [];
    for (let i = 0; i < array.length; i++) {
        if (!check[array[i][key]]) {
            check[array[i][key]] = true;
            res.push(array[i]);
        }
    }
    return res;
}

function getSiteDataOnGo() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteItemDropDownDataData, function (data) {
        siteInheritanceData = data.SiteItemInheritanceData;
    }, { siteCode: SiteCode, regionID: regionID }, true);
}

function refreshSiteInheritanceData() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteItemDropDownDataData, function (data) {
        siteInheritanceData = data.SiteItemInheritanceData;
    }, { siteCode: SiteCode, regionID: regionID }, true);
}

function populateSiteDishDropdown() {
    dishdata = [];
    var dataSource = SiteDishData;
    dishdata.push({ "value": 0, "text": "Select", "price": 0 });
    for (var i = 0; i < dataSource.length; i++) {
        dishdata.push({
            "value": dataSource[i].ID, "text": dataSource[i].Name, "DishCode": dataSource[i].DishCode,
            "DishCategoryID": dataSource[i].DishCategory_ID, "DishCategoryName": dataSource[i].DishCategoryName,
            "price": dataSource[i].CostPerKG, "tooltip": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
            "DietCategoryCode": dataSource[i].DietCategoryCode,
            "CuisineName": dataSource[i].CuisineName, "RecipeMapStatus": dataSource[i].RecipeMapStatus
        });
    }
}

function populateSiteMasterDropdown() {
    $("#inputsite").kendoDropDownList({
        filter: "contains",
        dataTextField: "SiteName",
        dataValueField: "SiteCode",
        dataSource: sitemasterdata,
        index: 0,
    });
    if (sitemasterdata.length == 1) {
        $("#btnGo").click();
        var dropdownlist = $("#inputsite").data("kendoDropDownList");
        dropdownlist.wrapper.hide();
    }

}


function populateFoodProgramDropdown() {
    $("#inputfoodprogram").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: foodprogramdata,
        index: 0,
    });
    var dropdownlist = $("#inputfoodprogram").data("kendoDropDownList");
    dropdownlist.bind("change", dropdownlist_selectMain);

}


function dropdownlist_selectMain(eid) {

    var ID = eid.sender.dataItem(eid.item).value;
    if (ID == 0) {
        localdata = menuitemdata;
    }
    else {
        localdata = menuitemdata.filter(function (item) {

            return item.FoodProgram_ID == ID;
        });
    }
    localdata.unshift({ "ID": 0, "ItemName": "Select" });
    //populateMenuItemDropdown();
}


function dropdownSelectSub(e) {
    eid = e;
}

// Added Populate type
function populateTypeDropdown(typeid, typevalue) {
    $(typeid).kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: typedata,
        index: 0,
    });
    var dropdownlist = $(typeid).data("kendoDropDownList");
    if (typevalue == -1)
        dropdownlist.select(0);//value("Select");
    else if (typevalue == -2)
        dropdownlist.select(1);
    else
        dropdownlist.value(typevalue);

    dropdownlist.bind("change", typedropdownlist_select);
}
function typedropdownlist_select(e) {

    
    var ID = e.sender.dataItem(e.item).value;
    if (ID == 0) {
        return;
    }
    $("#" + e.sender.element.context.id + "").val(ID);
}
// Added Populate UOM
function populateReasonDropdown(reasonid, reasonvalue) {
    $(reasonid).kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: reasondata,
        index: 0,
    });
    var dropdownlist = $(reasonid).data("kendoDropDownList");
    if (reasonvalue == -1 || reasonvalue == null || reasonvalue == 'undefined' || reasonvalue == "null")
        dropdownlist.select(0);//value("Select");
    else
        dropdownlist.value(reasonvalue);

    dropdownlist.bind("change", reasondropdownlist_select);
    dropdownlist.bind("open", reasondropdownlist_select);
    var winObject = $(reasonid).data("kendoDropDownList");
    winObject.wrapper.addClass("myCustomClass");

}
function reasondropdownlist_select(e) {

    var spanid = e.sender.element.context.id;
    var numeric = e.sender.element.context.id.substring(11);
    if ($("#inputsubqtysmall" + numeric).val() == $("#serinputsubqtysmall" + numeric).val()) {
        $("#" + spanid).data("kendoDropDownList").select(0);
        $("#" + spanid).data("kendoDropDownList").enable(false);

    } else {
        $("#" + spanid).data("kendoDropDownList").enable(true)
    }
    var ID = e.sender.dataItem(e.item).value;
    if (ID == 0) {
        return;
    }
    $("#" + e.sender.element.context.id + "").val(ID);
}
function populateUOMDropdown(uomid, uomvalue) {

    $(uomid).kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
    var dropdownlist = $(uomid).data("kendoDropDownList");
    if (dropdownlist == 'undefined') {
        return;
    }
    if (uomvalue == -1)
        dropdownlist.select(1);//value("Select");
    else
        dropdownlist.value(uomvalue);

    dropdownlist.bind("change", uomdropdownlist_select);
}

function uomdropdownlist_select(e) {
    //Jai Hanuman
    var ID = e.sender.dataItem(e.item).value;
    if (ID == 0) {
        return;
    }
    
    $("#" + e.sender.element.context.id + "").val(ID);
    var idnum = e.sender.element.context.id.substring(3);
    var dishFlag = e.sender.element.context.id.search("small");
    var outUOM = $('#' + e.sender.element.context.id).data("kendoDropDownList").text();
    $("#qseruom" + e.sender.element.context.id).text(outUOM);
    if (ID == "UOM-00003") {
        $("#wtspan" + e.sender.element.context.id).show();
        $("#outwtuom" + e.sender.element.context.id).css("visibility", "visible");

    }
    else {
        $("#wtspan" + e.sender.element.context.id).hide();
        $("#outwtuom" + e.sender.element.context.id).css("visibility", "hidden");

    }
    if (dishFlag > 0) {
        var sm = e.sender.element.context.id.substring(7);
        calculateCostPerServing(sm);
        return
    }
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    var outUOM = $('#' + e.sender.element.context.id).data("kendoDropDownList").text();
    for (var i = 0; i < len; i++) {

        var sm = subItem[i].id;

        if ($("#dishuom" + sm).data('kendoDropDownList') !== undefined) {
            $("#dishuom" + sm).data('kendoDropDownList').value(ID);
        }

        if ($("#dishuomtype" + sm).data('kendoDropDownList') !== undefined) {
            $("#dishuomtype" + sm).data('kendoDropDownList').value(ID);
        }

        $("#qseruomdishuom" + sm).text(outUOM);

        if (ID == "UOM-00003") {
            $("#wtspandishuom" + sm).show();
            $("#outwtuomdishuom" + sm).css("visibility", "visible");
            $("#wtspandishuomtype" + sm).show();
        } else {
            $("#wtspandishuom" + sm).hide();
            $("#outwtuomdishuom" + sm).css("visibility", "hidden");
            $("#wtspandishuomtype" + sm).hide();
        }
        calculateCostPerServing(sm);
    }


}

//Ended

function apply() {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
        function () {
            $("#dishCategory").empty();
            var dropdownlist = $("#inputsite").data("kendoDropDownList");
            var siteCode = dropdownlist.value();
            sameSource(ID, tr.ItemCode, siteCode);

            $("#configMaster").hide();
            $("#mapMaster").show();
            Utility.UnLoading();
        },
        function () {
            Utility.UnLoading();
        }
    );
}

function getSiteItemDishMpappingData(ItemCode, siteCode) {
    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteItemDropDownDataData, function (data) {
        if (!isDropDownDataLoaded) {
            isDropDownDataLoaded = true;
            SiteDishData = data.SiteDishData;
            populateSiteDishDropdown();
            var dataSource = data.DayPartData;
            for (var i = 0; i < dataSource.length; i++) {
                if (!dataSource[i].IsActive)
                    daypartdataInactive.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "code": dataSource[i].DayPartCode, "IsActive": dataSource[i].IsActive });

                daypartdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "code": dataSource[i].DayPartCode, "IsActive": dataSource[i].IsActive });
            }
            populateDayPartMultiSelect();

            var reasontempdata = data.ReasonData;
            reasondata = [];
            reasondata.push({ "value": "0", "text": "Select" });
            for (var i = 0; i < reasontempdata.length; i++) {
                if (reasontempdata[i].ApplicableTo == "RNT-001")
                    reasondata.push({ "value": reasontempdata[i].ReasonCode, "text": reasontempdata[i].ReasonDescription, "ID": reasontempdata[i].ID });
            }

            var uommtempdata = data.UOMModuleMappingData;
            uommodulemappingdata = [];
            uommodulemappingdata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < uommtempdata.length; i++) {
                if (uommtempdata[i].IsActive) {
                    uommodulemappingdata.push({ "value": uommtempdata[i].UOMCode, "text": uommtempdata[i].UOMName, "UOMModuleCode": uommtempdata[i].UOMModuleCode });
                }
            }
            uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");

            var dishcategorytempdata = data.DishCategoryData;
            dishcategorydata = [];

            for (var i = 0; i < dishcategorytempdata.length; i++) {
                dishcategorydata.push({ "value": dishcategorytempdata[i].ID, "text": dishcategorytempdata[i].Name, "code": dishcategorytempdata[i].DishCategoryCode });
            }
        }
        SiteDishCategoryMappingData = data.SiteDishCategoryMappingData;
        SiteItemDishMappingData = data.SiteItemDishMappingData;
        SiteDayPartMappingData = data.SiteDayPartMappingData;
    }, { siteCode: siteCode, regionID: regionID, itemCode: ItemCode}, true);
}

// Building from Same Site Source Start
function CreateSiteDishCategoryOutline(itemcode, siteCode) {
    finalModelExitsDishCategory = [];
    flagSec = 0;
    flagSecIsActive = 0;
    flagRegion = 0;
    finalModelExitsDishCategory = SiteDishCategoryMappingData;
    // - refresh dish price ---//
    //  populateSiteDishDropdown();
    if (finalModelExitsDishCategory.length < 1) {
        flagRegion = 1;
        return;
    }
    var dataItems = [];
    var multiarray = [];
    currentMasterQtyList = [];

    //finalModelExitsDishCategory.forEach(function (xi) {
    //    if (multiarray.indexOf(parseInt(xi.DishCategory_ID)) == -1) {
    //        multiarray.push(xi.DishCategory_ID);
    //        dataItems.push({
    //            "value": xi.DishCategory_ID, "text": xi.DishCategoryName, "stdqty": xi.StandardPortion, "ucode": xi.UOMCode, "wtuom": xi.WeightPerUOM
    //            , "IsActive": xi.IsActive, "dishcategorycode": xi.DishCategoryCode, "DishCategory_ID": xi.DishCategory_ID
    //        });
    //    }
    //});

    dataItems1 = finalModelExitsDishCategory.filter(function (x) {
        if (multiarray.indexOf(parseInt(x.DishCategory_ID)) == -1) {
            multiarray.push(x.DishCategory_ID);
            return x;
        }
    });

    for (xi of dataItems1) {
        dishcategorydata.forEach(function (yi) {
            if (yi.code == xi.DishCategoryCode) {
                dataItems.push({
                    "value": xi.DishCategory_ID, "text": yi.text, "stdqty": xi.StandardPortion, "ucode": xi.UOMCode, "wtuom": xi.WeightPerUOM
                    , "IsActive": xi.IsActive, "dishcategorycode": xi.DishCategoryCode, "DishCategory_ID": xi.DishCategory_ID
                })
            }
        });
    }

    for (item of dataItems) {
        var divid = "dish" + item.value;
        var dishesid = "inputdish" + item.value;
        var inputqty = "inputqty" + item.value;
        var subdishid = "subinputdish" + item.value;
        var uomid = "uom" + item.value;
        var wtqty = item.wtuom;//radha
        if (wtqty == null)
            wtqty = 0;
        var wtspanid = "wtspan" + uomid;
        var uomcode = item.ucode;
        var wtuom = "wtuom" + uomid;
        var dblock = item.IsActive ? "none" : "block";
        var dnone = item.IsActive ? "block" : "none";
        var isChecked = item.IsActive ? "checked" : "";
        var perServingHTML = "";

        if (user.SectorNumber !== "20") {
            perServingHTML = ' <input id="' + inputqty + '"  class="qty"  type="Number" oninput="validateNumber(this);" value="0" min="0" onchange="updateQuantity(' + inputqty + ')" oninput="validateNumber(this);" /> ' +
                '<div id = "' + uomid + '" class="colorG">' +
                '</div > ' +
                '<span class="serv">Per Serving</span>' +
                '          <span id="' + wtspanid + '">' +
                '                <input id="' + wtuom + '" class="qtyT" type="Number" oninput="validateNumber(this);" value="' + wtqty + '" min="0" onchange="updateWtPerUOMQuantity(' + wtuom + ')" oninput="validateNumber(this);" />' +
                '                <span class="wtlabel">wt. /pc (kg)</span>' +
                '          </span>';
        }
        else {
            perServingHTML = '  <input style="display:none" id="' + inputqty + '" class="qty" type="Number" oninput="validateNumber(this);" value="0" min="0" onchange="updateQuantity(' + inputqty + ')" oninput="validateNumber(this);" /> ' +
                '<div style="display:none" id = "" class="colorG">' +
                '</div > ' +
                '<span style="display:none" class="serv">Per Serving</span>' +
                '          <span style="display:none" id="' + wtspanid + '">' +
                '                <input id="' + wtuom + '" class="qtyT" type="Number" oninput="validateNumber(this);" value="' + wtqty + '" min="0" onchange="updateWtPerUOMQuantity(' + wtuom + ')" oninput="validateNumber(this);" />' +
                '                <span class="wtlabel">wt. /pc (kg)</span>' +
                '          </span>';
        }

        var sb = new StringBuilder();
        sb.append('<div class="dish" >' +
                '<div class="upper"  id=' + divid + '>' +
                '<div onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')" class="upperText">' + item.text + '</div>' + perServingHTML +
                '<span onclick="bigRemove(' + item.value + ')">' +
                '<span class="hideSt">' +
                '<label style = "margin-top:7px;" class= "switch" >' +
                '<input id="chk' + dishesid + '" type="checkbox" ' + isChecked + '>' +
                '<span class="slider round"></span>' +
                '</label>' +
                '</span>' +
                '</span>' +
                '<span class="hideSt"></span>' +
                '<span class="hideG" style="display:' + dnone + '" onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')"> Hide <i class="fas fa-angle-up" ></i ></span > ' +
                '<span class="displayG" style="display:' + dblock + '"></span>' +
                '<span class="ddlsitedish" style="float:right;margin-top:3px" id=' + dishesid + '></span>' +
                '</div >' +
                ' <div class="lower" style="display:' + dnone + '" id=' + subdishid + ' > </div >' +
                '</div> ');

        $('#dishCategory').append(sb.toString());

        if (user.SectorNumber !== "20") {
            populateUOMDropdown('#' + uomid, item.ucode);
            if (uomcode != "UOM-00003") {
                $("#" + wtspanid).hide();
            }
            if (uomcode == "UOM-00003") {

                $("#wtuom" + uomid).val(wtqty);
            }
        }

        populateDishDataDropdown('#' + dishesid, item.text, "#" + subdishid);

        //comeback
        currentMasterList.push({ "dropdownid": dishesid, "id": inputqty, "uomid": uomid, "uomcode": item.ucode });

        $("#" + inputqty).val(item.stdqty);
        currentMasterQtyList.push({
            "id": inputqty, "qty": item.stdqty, "uomid": uomid, "uomcode": item.ucode, "dishcategorycode": item.dishcategorycode, "dishcategoryid": item.DishCategory_ID
        });
        if (flagCurated) {
            $('#' + divid).find(".k-widget:last-child").hide();
        }
    }
    masterQty = currentMasterQtyList;
    CreateSiteDishTiles(siteCode, itemcode);
}

function CreateSiteDishTiles(siteCode, itemcode) {
        ingredientObject = [];
        var dataItems = [];
        finalModelExits = SiteItemDishMappingData;
        masterGroupList = [];
        currentMasterList = [];
        for (itemRec of finalModelExits) {
            var localdishdata = [];
            localdishdata = dishdata.filter(function (item) {
                return item.DishCode == itemRec.DishCode;
            });

            if (localdishdata != null && localdishdata.length > 0) {
                var text = localdishdata[0].text;   //Dish Name
                var dcat = localdishdata[0].DietCategoryCode;
                var idr = localdishdata[0].DishCategoryID;
                var smid = localdishdata[0].value;   //Dish ID
                var ingPrice = localdishdata[0].price; // Dishlevel Cost to be compued basis portion given
                var stwtPerUOM = itemRec.StandardWeightPerUOM;
                var serwtPerUOM = itemRec.ServedWeightPerUOM;
                var conwtPerUOM = itemRec.ContractWeightPerUOM;
                var dropdownid = "inputdish" + idr;   //Dish dropdown at DC level
                var subid = "#subinputdish" + idr;    //Container for all Dishes to keep the Dish tiles for a category
                var sid = "small" + smid;             // Dish Tile ID
                //  var inputqty = "inputqty" + idr;      // Dish Category Qty ID
                var ingID = smid;                     // DishID
                var qty = itemRec.StandardPortion;    //StandardPortion
                var subqty = itemRec.ContractPortion; //ContractPortion
                var subqtySer = itemRec.ServedPortion;//Served
                var costPerKg = localdishdata[0].price;
                var cuisine = localdishdata[0].CuisineName;
                var recipeMappedStatus = localdishdata[0].RecipeMapStatus;
                if (qty == null) {
                    qty = 0;
                }
                ingredientObject.push({
                    "ID": ingID,
                    "Name": text,
                    "Price": 0
                })
                //pura skeleton of Dish Category Section + Dish Tile Section ka information
                masterGroupList.push({
                    "dropdownid": dropdownid,
                    "subid": subid,
                    "sid": sid,
                    "ingPrice": ingPrice,
                    "ingID": ingID,
                    "text": text,
                    "smallinput": "inputsubqty" + sid,
                    "ContractPortion": subqty,
                    "StandardPortion": itemRec.StandardPortion,
                    "ServedPortion": subqtySer,
                    "StandardUOMCode": itemRec.StandardUOMCode,    //Dish's UOM Code
                    "ContractUOMCode": itemRec.ContractUOMCode,
                    "ServedUOMCode": itemRec.ServedUOMCode,
                    "DietCategoryCode": dcat,
                    "StandardWeigthPerUOM": stwtPerUOM,
                    "ServedWeigthPerUOM": serwtPerUOM,
                    "ContractWeigthPerUOM": conwtPerUOM,
                    "IsActive": itemRec.IsActive,
                    "ReasonCode": itemRec.ReasonCode,
                    "CostPerKg": costPerKg,
                    "CuisineName": cuisine,
                    "DishCategory_ID": idr,
                    "RecipeMapStatus": recipeMappedStatus
                });
            }
        }
        RecreateWholeExits();
}


function sameSource(ID, ItemCode, siteCode) {
    flagSite = 0;
    $("#dishCategory").empty();
    flagRegion = 0;
    flagSector = 0;
    CreateSiteDishCategoryOutline(ItemCode, siteCode);
}


function closeBelow(belowid, sameid, dishesid) {

    $("#" + belowid.id).toggle();
    if ($("#" + belowid.id).is(":visible")) {
        $("#" + sameid.id).find(".hideG").html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        $("#" + sameid.id).find(".displayG").hide()
        $("#" + sameid.id).find(".k-widget:last-child").show();
        $("#" + sameid.id).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" });
    } else {
        $("#" + sameid.id).find(".hideG").html(" Show <i class= 'fas fa-angle-down' ></i>");
        var count = $("#" + belowid.id).find(".smallIngredient").length;
        $("#" + sameid.id).find(".displayG").show().html(" " + count + " Dish(s) Added");
        $("#" + sameid.id).find(".k-widget:last-child").hide();
        $("#" + sameid.id).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
    }
}


function RecreateWholeExits() {
    for (item1 of masterGroupList) {
        var subid = item1.subid;
        var sid = item1.sid;
        var ingPrice = item1.ingPrice;
        var ingID = item1.ingID;
        var dtcat = item1.DietCategoryCode;//"DTC-00001";
        var text = item1.text;
        var qtysub = item1.ContractPortion;
        var qtysubser = item1.ServedPortion;
        var subInput = "inputsubqty" + sid;
        var isActive = item1.IsActive;
        var reasonid = "reason" + sid;
        var typeid = "type" + sid;
        var uomidtype = 'dishuomtype' + item1.sid;
        var uomid = 'dishuom' + item1.sid;
        var wtspanidtype = 'wtspan' + uomidtype;
        var wtspanid = 'wtspan' + uomid;
        var wtPerUOM = item1.ServedWeigthPerUOM;
        var uomcode = item1.ServedUOMCode;
        var wtPerUOMtype = item1.ContractWeigthPerUOM;
        var uomcodetype = item1.ContractUOMCode;
        var showsid = 'show' + sid;
        var hidesid = 'hide' + sid;
        var smallsiteid = 'site' + sid; //come
        var reasonCode = item1.ReasonCode;
        var costPerKg = item1.CostPerKg;
        var StOrConFlag = 0;
        var costpsid = 'costpsid' + sid;
        var costpkid = 'costpkid' + sid;
        if (uomcodetype == null) {
            StOrConFlag = 1;
            qtysub = item1.StandardPortion;
            wtPerUOMtype = item1.StandardWeigthPerUOM;
            uomcodetype = item1.StandardUOMCode;
        }
        var cuisinestring = item1.CuisineName == null ? "" : '<span class="cuisine-heading-site">' + item1.CuisineName + '</span>';

        var showPortionPopup = "";
        var recipeMapString = '<i style="color: green;font-size: 10px;" class="fas fa-check"></i>';
        if (item1.RecipeMapStatus == "Recipe Not Mapped") {
            recipeMapString = '<i style="color: red;font-size: 10px;" class="fas fa-times"></i>';
        }
        var dishDetails = '<div class="small">' +
        '             <span style="float:right;" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
            '                   <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
            '             </span ><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 7px!important;">' + recipeMapString + '</span>'
        '     </div>';
        
       
        if (user.SectorNumber !== "20") {
            showPortionPopup = '<span id="' + showsid + '" title="Update Portion" class="showportion" onclick="ShowPortionDetails(' + sid + ')">...</span>';
            dishDetails = '<div class="qserv" > Quantity Per Serving: <span id="qser' + uomid + '">' + qtysubser + '</span><span id="qseruom' + uomid + '"></span></div>' +
                '     <div id="outwtuom' + uomid + '" class="wtpc">wt/pc: <span id="qserwt' + uomid + '">' + wtPerUOM + '</span> kg</div>' +
                '     <div class="small">' +
                '             <span style="float:right;" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
                '                   <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
                '             </span ><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 7px!important;">' + recipeMapString + '</span>'+ cuisinestring + 
                '             <span id="' + costpsid + '" style="font-size:small;color:#595959;"> Cost Per Serving :' + Utility.AddCurrencySymbol(ingPrice, 2) + '</span>' +
                '     </div>';
        }
        var sb = new StringBuilder();
        if (!flagCurated) {
           // $(subid)
            sb.append(
                    '<div class="smallIngredient"   id=' + sid + '>' +

                    '<div class="tilebox">' +
                    '     <div title = "' + text + '" class= "supper suppertxt"><div class="topText">' + text + '</div>' + showPortionPopup+
                    '<div class="site_smallIngredient" id=' + smallsiteid + ' style="display:none;">' +
                    '     <div class="sitemodalhdr">' +
                    '         <div title="' + text + '" class="site_supper">' + text + ' - Portion Size Configuration<span class="site_cross" onclick="HidePortionDetails(' + smallsiteid + ')">x</span></div>' +
                    '     </div>' +
                    '     <div class="site_slower">' +
                    '          <div>' +
                    '                 <span style="">' +
                    '                       <span id=' + typeid + '></span><input  id=' + 'h' + typeid + ' type="hidden" value="' + StOrConFlag + '"/>' +
                    '                 </span >' +
                    '                 <input id="inputsubqty' + sid + '" class="qtyCon" type="Number" oninput="validateNumber(this);" value="' + qtysub + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
                    '                 <span id="' + uomidtype + '" class="colorGS bgcolortrans" ></span > <input id=' + 'h' + uomidtype + ' type="hidden" value="' + uomcodetype + '"/>' +
                    '                 <span id="' + wtspanidtype + '">' +
                    '                       <input id="wtuom' + uomidtype + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtPerUOMtype + '" min="0" /> ' +
                    '                       <span class="wtlabel" >wt. /pc (kg)</span>' +
                    '                 </span>' +
                    '          </div>' +
                    '          <div>' +
                    '                 <span class="served_site" >Served Quantity</span>' +
                    '                 <input id="serinputsubqty' + sid + '" class="qtySer" type="Number" oninput="validateNumber(this);" value="' + qtysubser + '" min="0" onchange="updateSubInputSer(' + sid + ')"/>' +
                    '                 <span id = "' + uomid + '" class= "colorGS bgcolortrans" ></span > <input  id=' + 'h' + uomid + ' type="hidden" value="' + uomcode + '"/>' +
                    '                 <span id="' + wtspanid + '">' +
                    '                       <input id="wtuom' + uomid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtPerUOM + '" min="0" onchange="updateSubInputSerwt(' + sid + ')"/><span id="out' + uomid + '"></span> ' +
                    '                       <span class="wtlabel" >wt. /pc (kg)</span>' +
                    '                 </span>' +
                    '          </div >' +
                    '     </div >' +
                    '     <div style = "margin:10px">' +
                    '           <span class="served_site_variation"> Variation Reason</span><span id=' + reasonid + '></span>  <input  id=' + 'h' + reasonid + ' type="hidden" value="' + reasonCode + '"/>' +
                    '           <span style="float:right;" id="' + hidesid + '" class="btn btn-publish popupokbtn" onclick="HidePortionDetails(' + smallsiteid + ')">Ok</span>' +

                    '     </div> ' +
                    '     <div>' +
                    '     </div> ' +
                    '</div>' +
                    '     </div> ' + dishDetails+
                    '<input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
                    '</div>' +
                    '</div>'
                );
        }
        else {

           // $(subid)
            sb.append(
                    '<div class="smallIngredient"  id=' + sid + '>' +

                    '<div class="tilebox">' +
                    '     <div title = "' + text + '" class= "supper suppertxt"> <div class="topText">' + text + '</div>' + showPortionPopup + 
                    '<div class="site_smallIngredient" id=' + smallsiteid + ' style="display:none;">' +
                    '     <div class="sitemodalhdr">' +
                    '         <div title="' + text + '" class="site_supper">' + text + ' - Portion Size Configuration<span class="site_cross" onclick="HidePortionDetails(' + smallsiteid + ')">x</span></div>' +
                    '     </div>' +
                    '     <div class="site_slower">' +
                    '          <div>' +
                    '                 <span>' +
                    '                       <span id=' + typeid + '></span><input  id=' + 'h' + typeid + ' type="hidden" value="' + StOrConFlag + '"/>' +
                    '                 </span>' +
                    '                 <input id="inputsubqty' + sid + '" class="qtyCon" type="Number" oninput="validateNumber(this);" value="' + qtysub + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
                    '                 <span id="' + uomidtype + '" class="colorGS bgcolortrans" ></span > <input id=' + 'h' + uomidtype + ' type="hidden" value="' + uomcodetype + '"/>' +
                    '                 <span id="' + wtspanidtype + '">' +
                    '                       <input id="wtuom' + uomidtype + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtPerUOMtype + '" min="0" />' +
                    '                       <span class="wtlabel">wt. /pc (kg)</span>' +
                    '                 </span>' +
                    '          </div>' +
                    '          <div>' +
                    '                 <span class="served_site" >Served Quantity</span>' +
                    '                 <input id="serinputsubqty' + sid + '" class="qtySer" type="Number" oninput="validateNumber(this);" value="' + qtysubser + '" min="0" onchange="updateSubInputSer(' + sid + ')"/>' +
                    '                 <span id = "' + uomid + '" class= "colorGS bgcolortrans" ></span > <input  id=' + 'h' + uomid + ' type="hidden" value="' + uomcode + '"/>' +
                    '                 <span id="' + wtspanid + '">' +
                    '                       <input id="wtuom' + uomid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtPerUOM + '" min="0" onchange="updateSubInputSerwt(' + sid + ')"/><span id="out' + uomid + '"></span> ' +
                    '                       <span class="wtlabel" >wt. /pc (kg)</span>' +
                    '                 </span>' +
                    '          </div >' +
                    '     </div >' +
                    '     <div style="margin:10px">' +
                    '           <span class="served_site_variation"> Variation Reason</span><span id=' + reasonid + '></span><input  id=' + 'h' + reasonid + ' type="hidden" value="' + reasonCode + '"/>' +
                    '           <span style="float:right;" id="' + hidesid + '" class="btn btn-publish popupokbtn" onclick="HidePortionDetails(' + smallsiteid + ')">Ok</span>' +
                    '     </div > ' +
                    '</div>' +
                    '     </div > '  + dishDetails+
                    '<input type = "hidden" id = "' + costpkid + '" value = "' + costPerKg + '" /> ' +
                    '</div>' +
                    '</div>'
                );

            $("#chk" + sid).prop("checked", isActive);
        }
        $(subid).append(sb.toString());
        if (user.SectorNumber != "20") {
            populateTypeDropdown('#' + typeid, StOrConFlag);

            populateReasonDropdown('#' + reasonid, reasonCode);
            populateUOMDropdown('#' + uomid, uomcode);
            if (uomcode != "UOM-00003") {
                $("#" + wtspanid).hide();
                $("#out" + wtspanid).hide();
            }
            if (uomcode == "UOM-00003") {

                $("#wtuom" + uomid).val(wtPerUOM);
            }

            if ($("#serinputsubqty" + sid).val() == $("#inputsubqty" + sid).val()) {

                $("#" + reasonid).data("kendoDropDownList").enable(false);
            } else {
                $("#" + reasonid).data("kendoDropDownList").enable(true);
            }
        }

        if (dtcat == "DTC-00001") {
            $('#' + sid).addClass("yellowborder");//mata
        } else if (dtcat == "DTC-00002") {
            $('#' + sid).addClass("redborder");
        } else if (dtcat == "DTC-00003") {
            $('#' + sid).addClass("greenborder");
        }
        if (user.SectorNumber != "20") {
            populateUOMDropdown('#' + uomidtype, uomcodetype);
            $('#' + uomidtype).val(uomcodetype);
            if (uomcodetype != "UOM-00003") {
                $("#" + wtspanidtype).hide();
                $("#outwtuomdishuom" + sid).css("visibility", "hidden");
                var outUOM = $('#' + uomidtype).data("kendoDropDownList").text();
                $('#qseruom' + uomid).text(outUOM);
            }
            if (uomcodetype == "UOM-00003") {
                console.log(wtPerUOMtype);
                $("#wtuom" + uomidtype).val(wtPerUOMtype);
            }
            populateUOMDropdown('#' + uomidtype, uomcodetype);
            $('#' + uomidtype).val(uomcodetype);
        }

        calculateCostPerServing(sid);
    }
    currentMasterList = masterGroupList;
    RecreateQuantity();
    disabledDishCategory();
}

function onShowPopulateDropdownData(typeid, StOrConFlag, reasonid, reasonCode, uomid, uomcode, wtspanid, wtPerUOM, sid, dtcat,
    uomidtype, uomcodetype, wtspanidtype, wtPerUOMtype) {
    populateTypeDropdown('#' + typeid, StOrConFlag);

    populateReasonDropdown('#' + reasonid, reasonCode);
    populateUOMDropdown('#' + uomid, uomcode);
    if (uomcode != "UOM-00003") {
        $("#" + wtspanid).hide();
        $("#out" + wtspanid).hide();
    }
    if (uomcode == "UOM-00003") {
        $("#" + wtspanid).show();
        $("#out" + wtspanid).show();
        $("#wtuom" + uomid).val(wtPerUOM);
    }

    if ($("#serinputsubqty" + sid).val() == $("#inputsubqty" + sid).val()) {

        $("#" + reasonid).data("kendoDropDownList").enable(false);
    } else {
        $("#" + reasonid).data("kendoDropDownList").enable(true);
    }
    if (dtcat == "DTC-00001") {
        $('#' + sid).addClass("yellowborder");//mata
    } else if (dtcat == "DTC-00002") {
        $('#' + sid).addClass("redborder");
    } else if (dtcat == "DTC-00003") {
        $('#' + sid).addClass("greenborder");
    }
    populateUOMDropdown('#' + uomidtype, uomcodetype);
    $('#' + uomidtype).val(uomcodetype);
    if (uomcodetype != "UOM-00003") {
        $("#" + wtspanidtype).hide();
        $("#outwtuomdishuom" + sid).css("visibility", "hidden");
        var outUOM = $('#' + uomidtype).data("kendoDropDownList").text();
        $('#qseruom' + uomid).text(outUOM);
    }
    if (uomcodetype == "UOM-00003") {
        $("#" + wtspanidtype).show();
        $("#outwtuomdishuom" + sid).css("visibility", "visible");
        $("#wtuom" + uomidtype).val(wtPerUOMtype);
    }


    populateUOMDropdown('#' + uomidtype, uomcodetype);
    $('#' + uomidtype).val(uomcodetype);
}

function disabledDishCategory() {
    if (dataItems1 != null && dataItems1.length > 0) {
        for (xi of dataItems1) {
            var isDishExists = masterGroupList.filter(m => m.DishCategory_ID == xi.DishCategory_ID);
            if (isDishExists == null || isDishExists == "" || isDishExists.length == 0) {
                bigRemoveDefault(xi.DishCategory_ID);
            }
        }

    }
}

function bigRemoveDefault(val) {

    var sid = "inputqty" + val;
    var did = "inputdish" + val;
    var si = "subinputdish" + val;
    var di = "dish" + val;
    var ind = "inputdish" + val;
    var dishesid = "#inputdish" + val;
    $("#chk" + did).prop("checked", false);
    $("#" + sid).prop("disabled", true);
    $("#" + di).find(".hideG").hide();//.html(" Hide <i class= 'fas fa-angle-up' ></i> ");
    var count = $("#" + si).find(".smallIngredient").length;
    $("#" + di).find(".displayG").show().html(" " + count + " Dish(s)    ");
    $("#" + si).hide();
    $("#" + di).find(".k-widget:last-child").hide();
    $("#" + di).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });


}

function RecreateQuantity() {
    for (m1 of masterQty) {
        for (m2 of currentMasterQtyList) {
            if (m1.id == m2.id) {
                m2.qty = m1.qty;
                $("#" + m2.id).val(m1.qty);
            }
        }
    }
}

//save
function SaveModel() {
    Utility.Loading();
    
        finalModel = [];
        dayPartArray = [];

        var multiselect = $("#inputdaypart").data("kendoMultiSelect");
        var dataItems = multiselect.dataItems();
        dayPartArray = [];
        for (itm of dataItems) {
            var dayPartModel = {
                "ID": 0,
                "SiteCode": SiteCode,
                "ItemCode": itemObject.Code,
                "DayPartCode": itm.code,
                "IsActive": 1
            }
            dayPartArray.push(dayPartModel);
        }
        if (dayPartArray.length < 1) {
            if (user.SectorNumber != "20")
                toastr.error("Please,select one or more Mealtime")
            else
                toastr.error("Please,select  one or more Service Type")
            return;
        }

        if (dayPartArray.length < 1) {
            finalModel = [];
            return;
        }

        var ContractPortion = 0;
        var uniqueArray = [...new Set(currentMasterQtyList)]
        currentMasterList = removeDuplicateObjectFromArray(currentMasterList, 'id');
        var dishcategory_id_coll = [];
       // var dishcategory_code_coll = [];  
        finalModelDishCategory = [];
        for (m1 of uniqueArray) {
            var idnumeric = m1.id.substr(8);
            dishcategory_id_coll.push(idnumeric);
           // dishcategory_code_coll.push(m1.DishCategoryCode);
        }

        var uniqueDishCategoryID = [...new Set(dishcategory_id_coll)]
       // var uniqueDishCategoryCode = [...new Set(dishcategory_code_coll)]
        var dropdownlist = $("#inputsite").data("kendoDropDownList");
        var siteCode = dropdownlist.value();
        var siteID = 0;
        for (var x of sitemasterdata) {
            if (siteCode == x.SiteCode) {
                siteID = x.SiteID;
                break;
            }
        }
        for (m1 of uniqueDishCategoryID) {
            var idnumeric = m1;
            var StandardPortion = $("#inputqty" + idnumeric).val();

            //loop array
            var len = $("#subinputdish" + idnumeric).find(".smallIngredient").length;
            var coll = $("#subinputdish" + idnumeric).find(".smallIngredient");
            var ismainActive = $("#chkinputdish" + idnumeric).is(':checked');
            if (len == 0 && !flagCurated && ismainActive) {
                // alert("Please Fill All Dish Category");
                toastr.error("Please Fill All Dish Category")
                finalModel = [];
                return;
            }
            var idAlready = 0;
            for (i = 0; i < len; i++) {
                dishid = coll[i].id.substr(5);
                var rid = 0;
                //var rc = $("#reasonsmall" + dishid).data("kendoDropDownList").value();
                var rc = $("#reasonsmall" + dishid).val();
                for (var x of reasondata) {
                    if (rc == x.value) {
                        rid = x.ID;
                        break;
                    }
                }
                // var serUOM = $("#dishuomsmall" + dishid).data("kendoDropDownList").value();
                var serUOM = $("#dishuomsmall" + dishid).val();
                var wtserUOM = 0;
                if (serUOM == "UOM-00003") {
                    wtserUOM = $("#wtuomdishuomsmall" + dishid).val()
                }

                //var csUOM = $("#dishuomtypesmall" + dishid).data("kendoDropDownList").value();
                var csUOM = $("#dishuomtypesmall" + dishid).val();
                var wtcsUOM = 0;
                if (csUOM == "UOM-00003") {
                    wtcsUOM = $("#wtuomdishuomtypesmall" + dishid).val()
                }
                //if ($("#typesmall" + dishid).data("kendoDropDownList").value() == 0) {
                if ($("#typesmall" + dishid).val() == 0) {
                    conUOM = csUOM;
                    standUOM = null;
                    wtconUOM = wtcsUOM;
                    wtstandUOM = null;
                    ContractPortion = $("#inputsubqtysmall" + dishid).val();
                    StandardPortion = null;
                } else {
                    standUOM = csUOM;
                    conUOM = null;
                    wtstandUOM = wtcsUOM;
                    wtconUOM = null;
                    ContractPortion = null;
                    StandardPortion = $("#inputsubqtysmall" + dishid).val();
                }


                var localdishdata = [];

                localdishdata = dishdata.filter(function (item) {
                    return item.value == dishid;
                });

                var dishcode = localdishdata[0].DishCode;


                ServedPortion = $("#serinputsubqtysmall" + dishid).val();
                var ismainActive = $("#chkinputdish" + idnumeric).is(':checked');
                for (item of finalModelExits) {
                    idAlready = 0;
                    if (dishcode == item.DishCode && siteID == item.Site_ID) {
                        idAlready = item.ID;
                        break;
                    }

                }

                var isActive = false;
                if (ismainActive) {
                    isActive = $("#chksmall" + dishid).is(':checked')
                }


                //if (flagSecIsActive && ismainActive) {
                //    isActive = true;
                //}
                if (ismainActive) {
                    isActive = true;
                }
                finalModel.push({
                    "ID": idAlready,
                    "Item_ID": itemObject.ID,
                    "ItemCode": itemObject.Code,
                    "Dish_ID": dishid,
                    "DishCode": dishcode,
                    "StandardPortion": StandardPortion,
                    "ContractPortion": ContractPortion,
                    "ServedPortion": ServedPortion,
                    "IsActive": isActive,
                    "CreatedBy": user.UserId,
                    "CreatedOn": Utility.CurrentDate(),
                    "Site_ID": siteID,
                    "SiteCode": siteCode,
                    "Reason_ID": rid,
                    "ReasonCode": rc,
                    "ServedWeightPerUOM": wtserUOM,
                    "StandardWeightPerUOM": wtstandUOM,
                    "StandardUOMCode": standUOM,
                    "ServedUOMCode": serUOM,
                    "ContractWeightPerUOM": wtconUOM,
                    "ContractUOMCode": conUOM,
                    "Status": isPublishBtnClick ? 3 : 2
                })
            }
            var StandardPortion = $("#inputqty" + m1).val();
            var DCUOMCode = user.SectorNumber !== "20"? $("#uom" + m1).data("kendoDropDownList").value():"";
            var wtperUOMcat =  user.SectorNumber !== "20" ?$("#wtuomuom" + m1).val():0;

            var dcatCode = currentMasterQtyList.filter(function (x) {
                return x.dishcategoryid == m1;
            });

            //active Inactive check
            var isActive = $("#chkinputdish" + m1).is(':checked');
            finalModelDishCategory.push({
                "ID": 0,
                "Item_ID": itemObject.ID,
                "Site_ID": siteID,
                "SiteCode": siteCode,
                "ItemCode": itemObject.Code,
                "DishCategory_ID": m1,
                "DishCategoryCode": dcatCode[0].dishcategorycode,
                "StandardPortion": StandardPortion,
                "IsActive": isActive,
                "CreatedBy": user.UserId,
                "CreatedOn": Utility.CurrentDate(),
                "UOMCode": DCUOMCode,
                "WeightPerUOM": wtperUOMcat
            })

        }

        if (finalModelDishCategory.length > 0) { SaveSiteDishCategory(finalModelDishCategory); }

        SaveDayPartMapping();
   
    
}
function GetDayPartMapping() {
    siteDayPartResult = SiteDayPartMappingData;
    var multiarray = [];
    if (siteDayPartResult == null || siteDayPartResult == "" || siteDayPartResult == undefined) {
        if (user.SectorNumber == 10) {
            multiarray = daypartdata.map(item => item.code);
        }
    } else {
        multiarray = siteDayPartResult.map(item => item.DayPartCode);
    }

    var multiselect = $("#inputdaypart").data("kendoMultiSelect");
    multiselect.value(multiarray);
}
function SaveDayPartMapping() {
    HttpClient.MakeSyncRequest(CookBookMasters.SaveSiteItemDayPartMappingDataList, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again.");
            dayPartArray = [];
            Utility.UnLoading();
        }
        else {

            // if (user.SectorNumber != "20")
            //toastr.success("Item Dish mapping published");
            if (statuscheck == 0) {
                Toast.fire({
                    type: 'success',
                    title: 'Changes have been saved. Click Publish to make the Menu Items available for Menu Planning in Cilantro',
                    timer: 5000
                });
            }
            else {
                toastr.success("Menu Item-Dish mapping complete");
            }
            //Toast.fire({
            //    type: 'success',
            //    title: 'Item Dish mapping saved',
            //    timer: 5000

            //});
            Utility.UnLoading();
        }
    }, {
        model: dayPartArray
    }, true);
}

function SaveSiteDishCategory(finalModelDishCategory) {

    //first check then remove return;
    HttpClient.MakeSyncRequest(CookBookMasters.SaveSiteDishCategoryMappingDataList, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again.");
        }
        else {
            if (!finalModel.length) {
                $(".k-overlay").hide();

                var msg = "Records have been updated successfully";
                Toast.fire({
                    type: 'success',
                    title: msg,
                    timer: 5000
                });
            }
        }
    }, {
        model: finalModelDishCategory
    }, true);

}


function onSave() {
    Utility.Loading();
    setTimeout(function () { 
    statuscheck = 0;
    isPublishBtnClick = false;
    RecreateQuantity();
    SaveModel();
    if (finalModel.length < 1) {
        Utility.UnLoading();
        return;
    }

    HttpClient.MakeSyncRequest(CookBookMasters.SaveSiteItemDishMappingDataList, function (result) {
        if (result.result == false) {
            toastr.error("Some error occured, please try again.");
        }
        else {
            $(".k-overlay").hide();
            //var msg = "Records have been updated successfully";
            //  toastr.success("Records have been updated successfully");
            //if (result.isCostUpdate) {
            //    setTimeout(function () {
            //        toastr.success("Listed items cost updation process started.");
            //    }, 2000);
            //}
            //Toast.fire({
            //    type: 'success',
            //    title: msg
            //});
            InheritedSiteItem(SiteCode);
        }
        Utility.UnLoading();
    }, {
        model: finalModel

    }, true);
    }, 1000);
}

function onPublish() {
    Utility.Loading();
    statuscheck = 1;
    setTimeout(function () {

        isPublishBtnClick = true;
        RecreateQuantity();
        SaveModel();
        if (finalModel.length < 1) {
            Utility.UnLoading();
            return;
        }
        Utility.Loading();
        if (finalModel.length > 0) {
            HttpClient.MakeSyncRequest(CookBookMasters.SaveSiteItemDishMappingDataList, function (result) {
                if (result.result == false) {
                    toastr.error("Some error occured, please try again.");
                }
                else {
                    $(".k-overlay").hide();
                    // toastr.success("Menu Item-Dish mapping complete");

                    $("#sourceName").text("Unit Menu Configuration");
                    InheritedSiteItem(SiteCode);
                }
            }, {
                model: finalModel
            }, true);
        }


        $("#configMaster").show();
        $("#mapMaster").hide();
        $("#topHeading").text("Unit Menu Configuration");
        populateItemGrid_IN();
        globalSearch($("#myInput").val());
        Utility.UnLoading();
    }, 2000);
}

function updateQuantity(iqty) {
    var qty = $("#" + iqty.id).val();
    var flag = 1;
    for (sqty of masterQty) {
        if (sqty.id == iqty.id) {
            sqty.qty = qty;

            flag = 0;
        }
    }
    if (flag) {
        masterQty.push({
            "id": iqty.id,
            "qty": qty
        });
    }

    //reflection update small input
    //loop array
    var idnum = iqty.id.substring(8);
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {
        var sm = subItem[i].id;
        $("#inputsubqty" + sm).val(qty);
        $("#serinputsubqty" + sm).val(qty);
        $("#qserdishuom" + sm).text(qty);
        calculateCostPerServing(sm);
    }

}

function calculateCostPerServing(sid) {

    var costPerKg = $("#costpkid" + sid).val();

    var serqty = $("#serinputsubqty" + sid).val();
    //  var seruom = $("#dishuom" + sid).val();
    var seruom = $("#dishuom" + sid).val();
    if (seruom == 'UOM-00001') {
        $("#costpsid" + sid).text("Cost / Serving: " + Utility.AddCurrencySymbol(costPerKg * serqty, 2));
    }
    else if (seruom == 'UOM-00003') {
        var uomQty = $("#wtuomdishuom" + sid).val();
        $("#costpsid" + sid).text("Cost / Serving: " + Utility.AddCurrencySymbol(costPerKg * serqty * uomQty, 2));
    }
}


function bigRemove(val) {

    var sid = "inputqty" + val;
    var did = "inputdish" + val;
    var si = "subinputdish" + val;
    var di = "dish" + val;
    if ($("#chk" + did).is(':checked')) {
        $("#" + sid).prop("disabled", false);
        $("#" + di).find(".hideG").show();//html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        $("#" + di).find(".displayG").hide();
        $("#" + si).show();
        $("#" + di).find(".k-widget").show();
        $("#" + di).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" });
    } else {
        $("#" + sid).prop("disabled", true);
        $("#" + di).find(".hideG").hide();//.html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        var count = $("#" + si).find(".smallIngredient").length;
        $("#" + di).find(".displayG").show().html(" " + count + " Dish(s)    ");
        $("#" + si).hide();
        $("#" + di).find(".k-widget:last-child").hide();
        $("#" + di).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
    }

}



function ShowPortionDetails(sid) {
    //if (sid.contains('small'))
    var item1list = masterGroupList.filter(m => m.sid == sid.id);
    var item1 = item1list[0];
    var sid = item1.sid;
    var dtcat = item1.DietCategoryCode;//"DTC-00001";
    var reasonid = "reason" + sid;
    var typeid = "type" + sid;
    var uomidtype = 'dishuomtype' + item1.sid;
    var uomid = 'dishuom' + item1.sid;
    var wtspanidtype = 'wtspan' + uomidtype;
    var wtspanid = 'wtspan' + uomid;
    var wtPerUOM = item1.ServedWeigthPerUOM;
    var uomcode = item1.ServedUOMCode;
    var wtPerUOMtype = item1.ContractWeigthPerUOM;
    var uomcodetype = item1.ContractUOMCode;
    var smallsiteid = 'site' + sid; //come
    var reasonCode = item1.ReasonCode;
    var StOrConFlag = 0;
    if (uomcodetype == null) {
        StOrConFlag = 1;
        qtysub = item1.StandardPortion;
        wtPerUOMtype = item1.StandardWeigthPerUOM;
        uomcodetype = item1.StandardUOMCode;
    }
    $("#" + smallsiteid).show();
    $(".ui-dialog-titlebar").hide();
    onShowPopulateDropdownData(typeid, StOrConFlag, reasonid, reasonCode, uomid, uomcode, wtspanid, wtPerUOM, sid, dtcat,
        uomidtype, uomcodetype, wtspanidtype, wtPerUOMtype);
}


function HidePortionDetails(smallsiteid) {
    $("#" + smallsiteid.id).hide();
}

function smallRemove(sid, ingID) {
    //Status Change to be saved
    console.log(sid);
    console.log(ingID);
    // if (!flagCurated && flagSec) {
    if (!flagCurated) {
        $("#" + sid.id).remove();
        masterGroupList = masterGroupList.filter(function (item) {
            return item.ingID !== ingID

        });
        ingredientObject = ingredientObject.filter(function (item) {
            return item.ID !== ingID

        });
        return;
    }
    if ($("#chksmall" + ingID).is(':checked')) {
        $("#inputsubqtysmall" + ingID).prop("disabled", false);
    } else {
        $("#inputsubqtysmall" + ingID).prop("disabled", true);
    }
}

function updateSubInput(inID, sid) {
    //Master to be updated
    var subinID = inID.id;
    var numeric = subinID.substring(16);
    //   alert(numeric);
    var spanid = "reasonsmall" + numeric;
    // $("#ser" + subinID).val($("#" + subinID).val());
    if ($("#inputsubqtysmall" + numeric).val() == $("#serinputsubqtysmall" + numeric).val()) {
        $("#" + spanid).data("kendoDropDownList").select(0);
        $("#" + spanid).data("kendoDropDownList").enable(false);

    } else {
        $("#" + spanid).data("kendoDropDownList").enable(true)
    }
    for (item of masterGroupList) {
        if (item.smallinput == subinID) {
            item.ContractPortion = $("#" + subinID).val();
            calculateCostPerServing(sid.id);
            //item.ServedPortion = $("#" + subinID).val();
        }
    }

}
function updateSubInputSer(sm) {
    //jai ho
    var sm = sm.id;
    var spanid = "reason" + sm;
    var qty = $("#serinputsubqty" + sm).val();
    $("#qserdishuom" + sm).text(qty);
    if ($("#inputsubqty" + sm).val() == $("#serinputsubqty" + sm).val()) {
        $("#" + spanid).data("kendoDropDownList").select(0);
        $("#" + spanid).data("kendoDropDownList").enable(false);

    } else {
        $("#" + spanid).data("kendoDropDownList").enable(true)
    }
    calculateCostPerServing(sm);
}
function updateSubInputSerwt(sm) {
    //jai ho
    var sm = sm.id;
    var qty = $("#wtuomdishuom" + sm).val();
    $("#qserwtdishuom" + sm).text(qty);
    calculateCostPerServing(sm);
}

function populateDishDataDropdown(dishid, dishcategory, subdishid) {
    //Filter for data based on category
    var localdishdata = [];

    if (selItemDietCategory == "DTC-00003") {
        localdishdata = dishdata.filter(item => item.DishCategoryName == dishcategory && item.DietCategoryCode == selItemDietCategory);
    }
    else if (selItemDietCategory == "DTC-00001") {
        localdishdata = dishdata.filter(item => item.DishCategoryName == dishcategory && (item.DietCategoryCode == selItemDietCategory || item.DietCategoryCode == "DTC-00003"));
    }
    else {
        localdishdata = dishdata.filter(item => item.DishCategoryName == dishcategory);
    }
    localdishdata.unshift({
        "value": 0, "text": "Select All", "DishCategoryName": dishcategory, "price": 0, "DietCategoryCode": selItemDietCategory
    });
    localdishdata.unshift({
        "value": 0, "text": "Select Dish(S)", "DishCategoryName": "Select", "price": 0
    });
    $(dishid).kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: localdishdata,
        autoClose: false,
        preventCloseCnt: 0,
        index: 0,
        close: function (e) {
            if (this.preventCloseCnt > 0) {//now the next time something tries to close it will work ( unless it's a change )
                e.preventDefault();
                this.preventCloseCnt--;
            } else {
                var dropdownlist = $(dishid).data("kendoDropDownList");
                dropdownlist.select(0);

            }
        }
    });
    var dropdownlist = $(dishid).data("kendoDropDownList");
    dropdownlist.bind("change", dropdownlist_select);
    dropdownlist.bind("open", dropdownlist_open);

}

function dropdownlist_open(e) {
    if (e.sender.dataSource._pristineTotal == 1) {
        toastr.error("No Dish exists for this dish category.");
    }
}

function dropdownlist_select(e) {
    if (e.sender.dataItem(e.item).text == "Select All") {
        selectAllDish(e);
    }
    else {
        this.preventCloseCnt = 1;
        if (e.sender.dataItem(e.item).value == 0) {
            return;
        }
        var item = e.sender.dataItem(e.item)
        var mid = e.sender.element[0].id;
        var text = item.text;
        var ingID = item.value;
        var ingPrice = item.price;
        var dropdownid = e.sender.element[0].id;
        var len = ingredientObject.length;
        var flag = 1;
        var subqty = $("#inputqty" + dropdownid.substring(9)).val();
        for (var i = 0; i < len; i++) {
            if (ingredientObject[i].ID == ingID) {
                flag = 0;
                break;
            }
        }
        if (flag == 1) {
            ingredientObject.push({
                "ID": ingID,
                "Name": text,
                "Price": ingPrice
            })
        }
        else {
            return;
        }


        var subInput = 'inputsubqty' + sid;
        var subid = "#sub" + mid;
        var sid = "small" + ingID;
        var dtcat = item.DietCategoryCode;
        // alert("dtcat" + dtcat);
        // item.price;
        var text = item.text;
        var qtysub = $("#inputqty" + dropdownid.substring(9)).val();//item1.ContractPortion;
       // var qtysubser = item.ServedPortion == undefined ? 0 : item.ServedPortion;
        var qtysubser = item.ServedPortion == undefined ? qtysub : item.ServedPortion;
        var subInput = "inputsubqty" + sid;
        var isActive = 1;
        var reasonid = "reason" + sid;
        var typeid = "type" + sid;
        var uomidtype = 'dishuomtype' + sid;
        var uomid = 'dishuom' + sid;
        var wtspanidtype = 'wtspan' + uomidtype;
        var wtspanid = 'wtspan' + uomid;
        var wtPerUOM = 0;
        var wtPerUOMSub = $("#wtuomuom" + dropdownid.substring(9)).val();
        wtPerUOM = item.ServedWeightPerUOM == undefined ? wtPerUOMSub : item.ServedWeightPerUOM;
        var uomcode = $("#uom" + dropdownid.substring(9)).val()//item.UOMCode;
        var wtPerUOMtype = 0;
        var uomcodetype = $("#uom" + dropdownid.substring(9)).val();
        var showsid = 'show' + sid;
        var hidesid = 'hide' + sid;
        var smallsiteid = 'site' + sid;
        var costpsid = 'costpsid' + sid;
        var cuisinestring = item.CuisineName == null ? "" : '<span class="cuisine-heading-site">' + item.CuisineName + '</span>';

        var showPortionPopup = "";
        var recipeMapString = '<i style="color: green;font-size: 10px;" class="fas fa-check"></i>';
        if (item.RecipeMapStatus == "Recipe Not Mapped") {
            recipeMapString = '<i style="color: red;font-size: 10px;" class="fas fa-times"></i>';
        }
        var dishDetails = '<div class="small">' +
            '             <span style="float:right;" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
            '                   <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
            '             </span><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 7px!important;">' + recipeMapString + '</span>'
        '     </div>';
       
        
        if (user.SectorNumber !== "20") {
            showPortionPopup = '<span id="' + showsid + '" title="Update Portion" class="showportion" onclick="ShowPortionDetails(' + sid + ')">...</span>';
            dishDetails = '<div class="qserv"> Quantity Per Serving: <span id="qser' + uomid + '">' + qtysubser + '</span><span id="qseruom' + uomid + '"></span></div>' +
                '     <div id="outwtuom' + uomid + '" class="wtpc">wt/pc: <span id="qserwt' + uomid + '">' + wtPerUOM + '</span> kg</div>' +
                '     <div class="small">' +
                '             <span style="float:right;" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
                '                   <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
                '             </span> <span> Recipe:</span> <span style="margin-left: 3px;margin-right: 7px!important;">' + recipeMapString + '</span>' + cuisinestring + 
                '             <span id="' + costpsid + '" style="font-size:small;color:#595959;"> Cost Per Serving :' + Utility.AddCurrencySymbol(ingPrice, 2) + '</span>' +
                '     </div>';
        }
        //  if (!flagCurated && flagSec) {
        if (!flagCurated) {
            $(subid)
                .prepend(
                    '<div class="smallIngredient" /*style="height:99px!important;vertical-align:top;"*/ id=' + sid + '>' +

                    '<div class="tilebox">' +
                    '     <div title = "' + text + '" class= "supper suppertxt"> <div class="topText">' + text + '</div>' + showPortionPopup +
                    '<div class="site_smallIngredient" id=' + smallsiteid + ' style="display:none;">' +
                    '     <div class="sitemodalhdr">' +
                    '         <div title="' + text + '" class="site_supper">' + text + ' - Portion Size Configuration<span class="site_cross" onclick="HidePortionDetails(' + smallsiteid + ')">x</span></div>' +
                    '     </div>' +
                    '     <div class="site_slower">' +
                    '          <div>' +
                    '                 <span style="">' +
                    '                       <span id=' + typeid + '></span>' +
                    '                 </span >' +
                    '                 <input id="inputsubqty' + sid + '" class="qtyCon" type="Number" oninput="validateNumber(this);" value="' + qtysub + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
                    '                 <span id="' + uomidtype + '" class="colorGS bgcolortrans" ></span > ' +
                    '                 <span id="' + wtspanidtype + '">' +
                    '                       <input id="wtuom' + uomidtype + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtPerUOMtype + '" min="0"  /> ' +
                    '                       <span class="wtlabel" >wt. /pc (kg)</span>' +
                    '                 </span>' +
                    '          </div>' +
                    '          <div>' +
                    '                 <span class="served_site" >Served Quantity</span>' +
                    '                 <input id="serinputsubqty' + sid + '" class="qtySer" type="Number" oninput="validateNumber(this);" value="' + qtysub + '" min="0" onchange="updateSubInputSer(' + sid + ')"/>' +
                    '                 <span id = "' + uomid + '" class= "colorGS bgcolortrans" ></span > ' +
                    '                 <span id="' + wtspanid + '">' +
                    '                       <input id="wtuom' + uomid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtPerUOM + '" min="0" onchange="updateSubInputSerwt(' + sid + ')"/><span id="out' + uomid + '"></span> ' +
                    '                       <span class="wtlabel" >wt. /pc (kg)</span>' +
                    '                 </span>' +
                    '          </div >' +
                    '     </div >' +
                    '     <div style="margin:10px">' +
                    '           <span class="served_site_variation"> Variation Reason</span><span id=' + reasonid + '></span>' +
                    '           <span style="float:right;" id="' + hidesid + '" class="btn btn-publish popupokbtn" onclick="HidePortionDetails(' + smallsiteid + ')">Ok</span>' +

                    '     </div > ' +
                    '     <div>' +
                    '     </div > ' +
                    '</div>' +
                    '     </div > ' +dishDetails + 
                    '</div>' +
                    '</div>'
                );

        } else {

            $(subid)
                .prepend(
                    '<div class="smallIngredient" style="height:99px!important;vertical-align:top;" id=' + sid + '>' +

                    '<div class="tilebox">' +
                    '     <div title = "' + text + '" class= "supper suppertxt"> <div class="topText">' + text + '</div>' + showPortionPopup +
                    '<div class="site_smallIngredient" id=' + smallsiteid + ' style="display:none;">' +
                    '     <div class="sitemodalhdr">' +
                    '         <div title="' + text + '" class="site_supper">' + text + ' - Portion Size Configuration<span class="site_cross" onclick="HidePortionDetails(' + smallsiteid + ')">x</span></div>' +
                    '     </div>' +
                    '     <div class="site_slower">' +
                    '          <div>' +
                    '                 <span>' +
                    '                       <span id=' + typeid + '></span>' +
                    '                 </span>' +
                    '                 <input id="inputsubqty' + sid + '" class="qtyCon" type=" " value="' + qtysub + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
                    '                 <span id="' + uomidtype + '" class="colorGS bgcolortrans" ></span > ' +
                    '                 <span id="' + wtspanidtype + '">' +
                    '                       <input id="wtuom' + uomidtype + '" class="qtyS" type="" value="' + wtPerUOMtype + '" min="0" />' +
                    '                       <span class="wtlabel">wt. /pc (kg)</span>' +
                    '                 </span>' +
                    '          </div>' +
                    '          <div>' +
                    '                 <span class="served_site" >Served Quantity</span>' +
                    '                 <input id="serinputsubqty' + sid + '" class="qtySer" type="Number" oninput="validateNumber(this);" value="' + qtysub + '" min="0" onchange="updateSubInputSer(' + sid + ')"/>' +
                    '                 <span id = "' + uomid + '" class= "colorGS bgcolortrans" ></span > ' +
                    '                 <span id="' + wtspanid + '">' +
                    '                       <input id="wtuom' + uomid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtPerUOM + '" min="0" onchange="updateSubInputSerwt(' + sid + ')"/><span id="out' + uomid + '"></span> ' +
                    '                       <span class="wtlabel" >wt. /pc (kg)</span>' +
                    '                 </span>' +
                    '          </div >' +
                    '     </div >' +
                    '     <div style="margin:10px">' +
                    '           <span class="served_site_variation"> Variation Reason</span><span id=' + reasonid + '></span>' +
                    '           <span style="float:right;" id="' + hidesid + '" class="btn btn-publish popupokbtn" onclick="HidePortionDetails(' + smallsiteid + ')">Ok</span>' +
                    '     </div > ' +
                    '</div>' +
                    '     </div >' +dishDetails +
                    '</div>' +
                    '</div>'
                );

            $("#chk" + sid).prop("checked", isActive);
        }
        if (user.SectorNumber == 10) {//for Google
            populateTypeDropdown('#' + typeid, -2);
        }

        else {
            populateTypeDropdown('#' + typeid, -1);
        }

        populateReasonDropdown('#' + reasonid, -1);
        populateUOMDropdown('#' + uomid, uomcode);
        if (uomcode != "UOM-00003") {
            $("#" + wtspanid).hide();
            $("#out" + wtspanid).hide();
        }
        if (uomcode == "UOM-00003") {
            $("#wtuom" + uomid).val(wtPerUOM);
            $("#" + wtspanid).show();
            $("#out" + wtspanid).show();
        }
        if ($("#serinputsubqty" + sid).val() == $("serinputsubqty" + sid).val()) {
            $("#" + reasonid).data("kendoDropDownList").enable(false);
        }
        if (dtcat == "DTC-00001") {
            $('#' + sid).addClass("yellowborder");//mata
        } else if (dtcat == "DTC-00002") {
            $('#' + sid).addClass("redborder");
        } else if (dtcat == "DTC-00003") {
            $('#' + sid).addClass("greenborder");
        }
        populateUOMDropdown('#' + uomidtype, uomcodetype);
        $('#' + uomidtype).val(uomcodetype);
        if (uomcodetype != "UOM-00003") {
            $("#" + wtspanidtype).hide();
            $("#outwtuomdishuom" + sid).css("visibility", "hidden");
            var outUOM = $('#' + uomidtype).data("kendoDropDownList").text();
            //console.log("outUOM" + outUOM);
            $('#qseruom' + uomid).text(outUOM);
        }
        if (uomcodetype == "UOM-00003") {
            $("#wtuom" + uomidtype).val(wtPerUOM);
        }


        populateUOMDropdown('#' + uomidtype, uomcodetype);
        $('#' + uomidtype).val(uomcodetype);

        //List containg Hare Rama
        masterGroupList.push({
            "dropdownid": dropdownid,
            "subid": subid,
            "sid": sid,
            "ingPrice": ingPrice,
            "ingID": ingID,
            "text": text,
            "smallinput": "inputsubqty" + sid,
            "ContractPortion": subqty,
            "StandardPortion": item.StandardPortion,
            "ServedPortion": subqty,
            "StandardUOMCode": uomcode,    //Dish's UOM Code
            "ContractUOMCode": uomcode,
            "ServedUOMCode": uomcode,
            "DietCategoryCode": dtcat,
            "StandardWeigthPerUOM": wtPerUOM,
            "ServedWeigthPerUOM": wtPerUOM,
            "ContractWeigthPerUOM": wtPerUOM,
            "IsActive": 1,
            "ReasonCode": -1
        });
    }


}

function selectAllDish(e) {
    var selItem = e.sender.dataItem(e.item)
    var localdishdata = [];

    if (selItem.DietCategoryCode == "DTC-00003") {
        localdishdata = dishdata.filter(item => item.DishCategoryName == selItem.DishCategoryName && item.DietCategoryCode == selItem.DietCategoryCode);
    }
    else if (selItem.DietCategoryCode == "DTC-00001") {
        localdishdata = dishdata.filter(item => item.DishCategoryName == selItem.DishCategoryName && (item.DietCategoryCode == selItem.DietCategoryCode || item.DietCategoryCode == "DTC-00003"));
    }
    else {
        localdishdata = dishdata.filter(item => item.DishCategoryName == selItem.DishCategoryName);
    }
    localdishdata.unshift({
        "value": 0, "text": "Select All", "DishCategoryName": "Select", "price": 0
    });
    localdishdata.unshift({
        "value": 0, "text": "Select Dish(s)", "DishCategoryName": "Select", "price": 0
    });

    if (localdishdata != null && localdishdata.length > 0) {
        localdishdata.forEach(function (item, index) {
            if (item.value > 0) {
                var mid = e.sender.element[0].id;
                var text = item.text;
                var ingID = item.value;
                var ingPrice = item.price;
                var dropdownid = e.sender.element[0].id;
                var len = ingredientObject.length;
                var flag = 1;
                var subqty = $("#inputqty" + dropdownid.substring(9)).val();
                for (var i = 0; i < len; i++) {
                    if (ingredientObject[i].ID == ingID) {
                        flag = 0;
                        break;
                    }
                }
                if (flag == 1) {
                    ingredientObject.push({
                        "ID": ingID,
                        "Name": text,
                        "Price": ingPrice
                    })
                }
                else {
                    return;
                }

                var subInput = 'inputsubqty' + sid;
                var subid = "#sub" + mid;
                var sid = "small" + ingID;
                var dtcat = item.DietCategoryCode;
                // alert("dtcat" + dtcat);
                // item.price;
                var text = item.text;
                var qtysub = $("#inputqty" + dropdownid.substring(9)).val();//item1.ContractPortion;
                // var qtysubser = item.ServedPortion == undefined ? 0 : item.ServedPortion;
                var qtysubser = item.ServedPortion == undefined ? qtysub : item.ServedPortion;
                var subInput = "inputsubqty" + sid;
                var isActive = 1;
                var reasonid = "reason" + sid;
                var typeid = "type" + sid;
                var uomidtype = 'dishuomtype' + sid;
                var uomid = 'dishuom' + sid;
                var wtspanidtype = 'wtspan' + uomidtype;
                var wtspanid = 'wtspan' + uomid;
                var wtPerUOM = 0;
                var wtPerUOMSub = $("#wtuomuom" + dropdownid.substring(9)).val();
                wtPerUOM = item.ServedWeightPerUOM == undefined ? wtPerUOMSub : item.ServedWeightPerUOM;
                var uomcode = $("#uom" + dropdownid.substring(9)).val()//item.UOMCode;
                var wtPerUOMtype = 0;
                var uomcodetype = $("#uom" + dropdownid.substring(9)).val();
                var showsid = 'show' + sid;
                var hidesid = 'hide' + sid;
                var smallsiteid = 'site' + sid;
                var costpsid = 'costpsid' + sid;
                var cuisinestring = item.CuisineName == null ? "" : '<span class="cuisine-heading-site">' + item.CuisineName + '</span>';
                var showPortionPopup = "";
                var recipeMapString = '<i style="color: green;font-size: 10px;" class="fas fa-check"></i>';
                if (item.RecipeMapStatus == "Recipe Not Mapped") {
                    recipeMapString = '<i style="color: red;font-size: 10px;" class="fas fa-times"></i>';
                }
                var dishDetails = '<div class="small">' +
                    '             <span style="float:right;" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
                    '                   <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
                    '             </span ><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 7px!important;">' + recipeMapString + '</span>'
                '     </div>';
               
                if (user.SectorNumber !== "20") {
                    showPortionPopup = '<span id="' + showsid + '" title="Update Portion" class="showportion" onclick="ShowPortionDetails(' + sid + ')">...</span>';
                    dishDetails = '<div class="qserv" > Quantity Per Serving: <span id="qser' + uomid + '">' + qtysubser + '</span><span id="qseruom' + uomid + '"></span></div>' +
                        '     <div id="outwtuom' + uomid + '" class="wtpc">wt/pc: <span id="qserwt' + uomid + '">' + wtPerUOM + '</span> kg</div>' +
                        '     <div class="small">' +
                        '             <span style="float:right;" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
                        '                   <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
                        '             </span> <span> Recipe:</span > <span style="margin-left: 3px;margin-right: 7px!important;">' + recipeMapString + '</span>' + cuisinestring +
                        '             <span id="' + costpsid + '" style="font-size:small;color:#595959;"> Cost Per Serving :' + Utility.AddCurrencySymbol(ingPrice, 2) + '</span>' +
                        '     </div>';
                }
                //  if (!flagCurated && flagSec) {
                if (!flagCurated) {
                    $(subid)
                        .prepend(
                            '<div class="smallIngredient" /*style="height:99px!important;vertical-align:top;"*/ id=' + sid + '>' +

                            '<div class="tilebox">' +
                            '     <div title = "' + text + '" class= "supper suppertxt"> <div class="topText">' + text + '</div>' + showPortionPopup +
                            '<div class="site_smallIngredient" id=' + smallsiteid + ' style="display:none;">' +
                            '     <div class="sitemodalhdr">' +
                            '         <div title="' + text + '" class="site_supper">' + text + ' - Portion Size Configuration<span class="site_cross" onclick="HidePortionDetails(' + smallsiteid + ')">x</span></div>' +
                            '     </div>' +
                            '     <div class="site_slower">' +
                            '          <div>' +
                            '                 <span style="">' +
                            '                       <span id=' + typeid + '></span>' +
                            '                 </span >' +
                            '                 <input id="inputsubqty' + sid + '" class="qtyCon" type="Number" oninput="validateNumber(this);" value="' + qtysub + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
                            '                 <span id="' + uomidtype + '" class="colorGS bgcolortrans" ></span > ' +
                            '                 <span id="' + wtspanidtype + '">' +
                            '                       <input id="wtuom' + uomidtype + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtPerUOMtype + '" min="0"  /> ' +
                            '                       <span class="wtlabel" >wt. /pc (kg)</span>' +
                            '                 </span>' +
                            '          </div>' +
                            '          <div>' +
                            '                 <span class="served_site" >Served Quantity</span>' +
                            '                 <input id="serinputsubqty' + sid + '" class="qtySer" type="Number" oninput="validateNumber(this);" value="' + qtysub + '" min="0" onchange="updateSubInputSer(' + sid + ')"/>' +
                            '                 <span id = "' + uomid + '" class= "colorGS bgcolortrans" ></span > ' +
                            '                 <span id="' + wtspanid + '">' +
                            '                       <input id="wtuom' + uomid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtPerUOM + '" min="0" onchange="updateSubInputSerwt(' + sid + ')"/><span id="out' + uomid + '"></span> ' +
                            '                       <span class="wtlabel" >wt. /pc (kg)</span>' +
                            '                 </span>' +
                            '          </div >' +
                            '     </div >' +
                            '     <div style="margin:10px">' +
                            '           <span class="served_site_variation"> Variation Reason</span><span id=' + reasonid + '></span>' +
                            '           <span style="float:right;" id="' + hidesid + '" class="btn btn-publish popupokbtn" onclick="HidePortionDetails(' + smallsiteid + ')">Ok</span>' +

                            '     </div > ' +
                            '     <div>' +
                            '     </div > ' +
                            '</div>' +
                            '     </div > ' + dishDetails +
                            '</div>' +
                            '</div>'
                        );

                } else {

                    $(subid)
                        .prepend(
                            '<div class="smallIngredient" style="height:99px!important;vertical-align:top;" id=' + sid + '>' +

                            '<div class="tilebox">' +
                            '     <div title = "' + text + '" class= "supper suppertxt"> <div class="topText">' + text + '</div>'  + showPortionPopup +
                            '<div class="site_smallIngredient" id=' + smallsiteid + ' style="display:none;">' +
                            '     <div class="sitemodalhdr">' +
                            '         <div title="' + text + '" class="site_supper">' + text + ' - Portion Size Configuration<span class="site_cross" onclick="HidePortionDetails(' + smallsiteid + ')">x</span></div>' +
                            '     </div>' +
                            '     <div class="site_slower">' +
                            '          <div>' +
                            '                 <span>' +
                            '                       <span id=' + typeid + '></span>' +
                            '                 </span>' +
                            '                 <input id="inputsubqty' + sid + '" class="qtyCon" type=" " value="' + qtysub + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
                            '                 <span id="' + uomidtype + '" class="colorGS bgcolortrans" ></span > ' +
                            '                 <span id="' + wtspanidtype + '">' +
                            '                       <input id="wtuom' + uomidtype + '" class="qtyS" type="" value="' + wtPerUOMtype + '" min="0" />' +
                            '                       <span class="wtlabel">wt. /pc (kg)</span>' +
                            '                 </span>' +
                            '          </div>' +
                            '          <div>' +
                            '                 <span class="served_site" >Served Quantity</span>' +
                            '                 <input id="serinputsubqty' + sid + '" class="qtySer" type="Number" oninput="validateNumber(this);" value="' + qtysub + '" min="0" onchange="updateSubInputSer(' + sid + ')"/>' +
                            '                 <span id = "' + uomid + '" class= "colorGS bgcolortrans" ></span > ' +
                            '                 <span id="' + wtspanid + '">' +
                            '                       <input id="wtuom' + uomid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtPerUOM + '" min="0" onchange="updateSubInputSerwt(' + sid + ')"/><span id="out' + uomid + '"></span> ' +
                            '                       <span class="wtlabel" >wt. /pc (kg)</span>' +
                            '                 </span>' +
                            '          </div >' +
                            '     </div >' +
                            '     <div style="margin:10px">' +
                            '           <span class="served_site_variation"> Variation Reason</span><span id=' + reasonid + '></span>' +
                            '           <span style="float:right;" id="' + hidesid + '" class="btn btn-publish popupokbtn" onclick="HidePortionDetails(' + smallsiteid + ')">Ok</span>' +
                            '     </div > ' +
                            '</div>' +
                            '     </div > ' + dishDetails +
                            '</div>' +
                            '</div>'
                        );

                    $("#chk" + sid).prop("checked", isActive);
                }
                if (user.SectorNumber == 10) {//for Google
                    populateTypeDropdown('#' + typeid, -2);
                }

                else {
                    populateTypeDropdown('#' + typeid, -1);
                }

                if (user.SectorNumber != "20") {
                    populateReasonDropdown('#' + reasonid, -1);
                    populateUOMDropdown('#' + uomid, uomcode);
                    if (uomcode != "UOM-00003") {
                        $("#" + wtspanid).hide();
                        $("#out" + wtspanid).hide();
                    }
                    if (uomcode == "UOM-00003") {
                        $("#" + wtspanid).show();
                        $("#out" + wtspanid).show();
                        $("#wtuom" + uomid).val(wtPerUOM);
                    }
                    if ($("#serinputsubqty" + sid).val() == $("serinputsubqty" + sid).val()) {
                        $("#" + reasonid).data("kendoDropDownList").enable(false);
                    }
                    if (dtcat == "DTC-00001") {
                        $('#' + sid).addClass("yellowborder");//mata
                    } else if (dtcat == "DTC-00002") {
                        $('#' + sid).addClass("redborder");
                    } else if (dtcat == "DTC-00003") {
                        $('#' + sid).addClass("greenborder");
                    }
                    populateUOMDropdown('#' + uomidtype, uomcodetype);
                    $('#' + uomidtype).val(uomcodetype);
                    if (uomcodetype != "UOM-00003") {
                        $("#" + wtspanidtype).hide();
                        $("#outwtuomdishuom" + sid).css("visibility", "hidden");
                        var outUOM = $('#' + uomidtype).data("kendoDropDownList").text();
                        $('#qseruom' + uomid).text(outUOM);
                    }
                    if (uomcodetype == "UOM-00003") {
                        $("#" + wtspanidtype).show();
                        $("#outwtuomdishuom" + sid).css("visibility", "visible");
                        $("#wtuom" + uomidtype).val(wtPerUOM);
                    }
                    populateUOMDropdown('#' + uomidtype, uomcodetype);
                    $('#' + uomidtype).val(uomcodetype);
                }

                //List containg Hare Rama
                masterGroupList.push({
                    "dropdownid": dropdownid,
                    "subid": subid,
                    "sid": sid,
                    "ingPrice": ingPrice,
                    "ingID": ingID,
                    "text": text,
                    "smallinput": "inputsubqty" + sid,
                    "ContractPortion": subqty,
                    "StandardPortion": item.StandardPortion,
                    "ServedPortion": subqty,
                    "StandardUOMCode": uomcode,    //Dish's UOM Code
                    "ContractUOMCode": uomcode,
                    "ServedUOMCode": uomcode,
                    "DietCategoryCode": dtcat,
                    "StandardWeigthPerUOM": wtPerUOM,
                    "ServedWeigthPerUOM": wtPerUOM,
                    "ContractWeigthPerUOM": wtPerUOM,
                    "IsActive": 1,
                    "ReasonCode": -1
                });
            }
        });
    }
}

function globalSearch(inputValue) {
    if (globalChecked) {
        $('#header-chb').attr("checked", false).triggerHandler('change');
    }

    var grid = $('#gridItem').data('kendoGrid');
    if (grid !== undefined && grid != null) {
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ItemCode" || x.field == "ItemName" || x.field == "VisualCategoryName" || x.field == "FoodProgramName" || x.field == "DietCategoryName"
                    || x.field == "HSNCode" || x.field == "CGST" || x.field == "SGST" || x.field == "CESS" || x.field == "Status" || x.field == "DayPartNames") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = inputValue;
                        if (x.field == "Status") {
                            var pendingString = "pending";
                            var savedString = "saved";
                            var mappedString = "mapped";
                            if (pendingString.includes(inputValue.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(inputValue.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(inputValue.toLowerCase())) {
                                targetValue = "3";
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(inputValue)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: inputValue
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(inputValue)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(inputValue) !== null) {
                        var bool = getBoolean(inputValue);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    }

}

$(document).ready(function () {
    if (user.SectorNumber != "30") {
        $("#fpname").hide();
        $("#fptype").hide();
        $("#pipeSpan").hide();
    }
    $("#btnExport").click(function (e) {
        var grid = $("#gridItem").data("kendoGrid");
        grid.saveAsExcel();
    });
    $("#windowEdit").kendoWindow({
        modal: true,
        width: "250px",
        height: "210px",
        title: "Site Item Details  ",
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#collapseIcon").click(function () {
        collapseAll();
    });

    $("#expandIcon").click(function () {
        expandAll();
    });
    HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
        user = result;
    }, null, false);
    $('#myInput').on('input', function (e) {
        globalSearch(e.target.value);
    });

    $("#btnpricesubmit").click(function () {
        modelprice.ItemPrice = $("#itemprice").val();
        if (modelprice.ItemPrice.trim() == "" || modelprice.ItemPrice.trim() == null) {
            toastr.error("Kindly Enter Item Price");
            return;
        }

        saveToSiteInheritance(modelprice);
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit").data("kendoWindow");
        orderWindow.close();

    });

    $("#btnpricecancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });



});

function expandAll() {
    var rows = $(".upper");
    $.each(rows, function () {
        var upperid = (this).id;
        var lowerid = (this).nextElementSibling.id;
        $("#" + upperid).find(".hideG").html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        $("#" + lowerid).show();
        $("#" + upperid).find(".k-widget").show();
        $("#" + upperid).find(".displayG").hide()
        $("#" + upperid).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" })
        $("#expandIcon").css({ "color": "#3f7990" });
        $("#collapseIcon").css({ "color": "#76b4b0" });
        $(".ddlsitedish").show()
    });
}

function collapseAll() {
    var rows = $(".upper");
    rows.each(function () {
        var upperid = (this).id;
        var lowerid = (this).nextElementSibling.id;
        $("#" + upperid).find(".hideG").html(" Show <i class= 'fas fa-angle-down' ></i>");
        var count = $("#" + lowerid).find(".smallIngredient").length;
        $("#" + lowerid).hide();
        $("#" + upperid).find(".displayG").show().html(" " + count + " Dish(s) Added");
        $("#" + upperid).find(".k-widget:last-child").hide();
        $("#" + upperid).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
        $("#collapseIcon").css({ "color": "#3f7990" });
        $("#expandIcon").css({ "color": "#76b4b0" });
        $(".ddlsitedish").hide();
    });
}




$(document).on('keydown', function (e) {
    if (e.keyCode === 27) { // ESC
        $(".site_smallIngredient").hide();
    }
});

function modifiedDataSource() {
    //HttpClient.MakeSyncRequest(CookBookMasters.GetSiteItemInheritanceMappingDataList, function (result) {
    //    if (result != null) {

    //        console.log("Inheritance" + result);
    //        InheritedItem = result;

    //    }
    //    else {

    //    }
    //}, { SiteCode: SiteCode }, true);

    //HttpClient.MakeSyncRequest(CookBookMasters.GetUniqueMappedItemCodesFromRegion, function (data) {
    //    var dataSource = data;
    //    UniqueItemCodes = dataSource;
    //}, { RegionID: regionID, SiteCode: SiteCode }, true);
    //HttpClient.MakeSyncRequest(CookBookMasters.GetItemDataList, function (data) {

    //    if (data != null) {
    //        data = data.filter(function (item) {
    //            if (UniqueItemCodes.indexOf(item.ItemCode) != -1 && item.Status == 3)
    //                return item;
    //        });
    //        // FOR GOGO/GOVO
    //        if (user.SectorNumber == 10) {
    //            var subSectorCode = $("#inputsite").data("kendoDropDownList").dataItem().SubSectorCode;
    //            var regionID = $("#inputsite").data("kendoDropDownList").dataItem().RegionID;
    //            HttpClient.MakeSyncRequest(CookBookMasters.GetRegionItemInheritanceMappingDataList, function (result) {
    //                if (result != null) {
    //                    var regionInheritedItem = result;
    //                    regionInheritedItem = regionInheritedItem.filter(function (item) {
    //                        if (item.SubSectorCode == subSectorCode)
    //                            return item;
    //                    });
    //                    data = data.filter(function (item) {
    //                        if (regionInheritedItem.filter(m => m.ItemCode == item.ItemCode).length > 0)
    //                            return item;
    //                    });
    //                }
    //            }, { regionID: regionID, SiteCode: SiteCode }, false);
    //        };

    //        if (InheritedItem.length > 0) {
    //            for (var x of data) {
    //                var fStatus = 1;
    //                for (y of InheritedItem) {
    //                    if (x.ItemCode == y.ItemCode) {
    //                        x.Status = y.Status;
    //                        x["ItemPrice"] = y.ItemPrice == null ? 0 : y.ItemPrice;
    //                        x["ID"] = y.ID;
    //                        x["Site_ID"] = y.Site_ID;
    //                        x["SiteCode"] = y.SiteCode;
    //                        x["Item_ID"] = y.Item_ID;
    //                        x["IsActive"] = y.IsActive;
    //                        x["CreatedBy"] = y.CreatedBy;
    //                        x["CreatedOn"] = y.CreatedOn;
    //                        x["Status"] = y.Status;
    //                        x["DayPartNames"] = y.DayPartNames

    //                        if (y.IsActive == 1)
    //                            x["MenuLevel"] = "Unit";
    //                        else
    //                            x["MenuLevel"] = "Draft";
    //                        fStatus = 0;
    //                        //  alert("Hare Krishna");
    //                        break;
    //                    }

    //                }
    //                if (fStatus) {
    //                    x["MenuLevel"] = "Region";
    //                }

    //            }
    //        }
    //        else {
    //            for (var x of data) {
    //                x["MenuLevel"] = "Region";
    //            }
    //        }

    //        siteMasterDataSource = data.filter(function (x) {
    //            if (user.SectorNumber == 30 || user.SectorNumber == 80) {
    //                return x.ItemCode > 69999 && x.ItemCode < 80000;
    //            }
    //            else {
    //                return x.ItemCode = x.ItemCode;
    //            }
    //        });

    //        siteNonInheritedMasterDataSource = siteMasterDataSource;

    //        siteDataSource = siteMasterDataSource.filter(function (item) {
    //            return item.MenuLevel == "Unit";
    //        });

    //    } else {

    //    }

    //}, { SiteCode: SiteCode }, true);

    //siteMasterDataSource = siteInheritanceData.filter(function (x) {
    //    if (user.SectorNumber == 30 || user.SectorNumber == 80) {
    //        return x.ItemCode > 69999 && x.ItemCode < 80000; 
    //    }
    //    else {
    //        return x.ItemCode = x.ItemCode;
    //    }
    //});

    siteNonInheritedMasterDataSource = siteMasterDataSource;
    siteDataSource = siteMasterDataSource.filter(function (item) {
        return item.MenuLevel == "Unit";
    });
}

function populateItemGrid() {

    Utility.Loading();
    var gridVariable = $("#gridItem");
    gridVariable.html("");
    grid = gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,

        columns: [


            {
                field: "ItemCode", title: "Item Code", width: "140px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                }
            },
            {
                field: "ItemName", title: "Item Name", attributes: {

                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                },
                template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
            },

            {
                field: "FoodProgramName", title: "Food Program", width: "160px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "VisualCategoryName", title: "Visual Category", width: "155px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "DayPartNames", title: "Service Type", width: "130px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MenuLevel", title: "Menu Level", width: "135px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                //template: "<input type='checkbox' class='checkbox' />",
                template: "<label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px' ><input type='checkbox' class='checkbox'><span class='checkmarkgrid'></span></label>",
                headerTemplate: "<span style='margin-right : 5px;'>Include All</span><label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px' ><input type='checkbox' id='header-chb' class='checkbox1'><span class='checkmarkgrid'></span></label>",

                width: "135px", title: "Include",
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center; vertical-align:middle"
                }
            }
        ],
        dataSource: {
            data: siteInheritanceData,

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ItemCode: { type: "string" },
                        ItemName: { type: "string" },
                        DietCategoryName: { type: "string" },
                        VisualCategoryName: { type: "string" },
                        FoodProgramName: { type: "string" },
                        HSNCode: { type: "string" },
                        CGST: { type: "string" },
                        SGST: { type: "string" },
                        CESS: { type: "string" },
                        DayPartNames: { type: "string" }
                    }

                }
            },
            pageSize: 150
        },
        columnResize: function (e) {
            //var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        //  height: 440,
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (globalChecked) {
                    $('#header-chb').attr("checked", "checked");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");
                    if (checkedIds[view[i].id] == false) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".checkbox")
                            .removeAttr("checked");
                        continue;

                    }
                    continue;
                }
                if (checkedIds[view[i].id]) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");
                    //.addClass("k-state-selected")
                }
                if (checkedIds[view[i].id] == false) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .removeAttr("checked");
                    continue;
                    //.addClass("k-state-selected")
                }

                if (view[i].MenuLevel == "Unit") {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");//.attr("disabled", "disabled");
                } else {
                    if (view[i].MenuLevel == "Draft") {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".checkbox")
                            .attr("checked", "checked");//.attr("disabled", "disabled");
                    }//Draft Unchecked
                    //k-grid-MapDishes
                    //this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                    //    .find(".k-grid-MapDishes")//color grey
                    //    .removeClass("k-grid-MapDishes");
                    // $(this).removeClass("k-grid-MapDishes");
                }
            }
            //var grid = this;
            //var rows = grid.items();

            //$(rows).each(function (e) {
            //    var row = this;
            //    var dataItem = grid.dataItem(row);

            //    if (dataItem.Status ==  "Unit") {
            //        grid.select(row);
            //    }
            //});

        },
        change: function (e) {

        },
        // filter: { field: "ItemCode", operator: "gt", value: "69999" }
    });
    //bind click event to the checkbox
    grid = grid.data("kendoGrid")

    if (user.SectorNumber != "30") {
        grid.hideColumn("FoodProgramName");
        grid.hideColumn("VisualCategoryName");
    }
    grid.table.on("click", ".checkbox", selectRow);
    $('#header-chb').change(function (ev) {
        var checked = ev.target.checked;
        globalChecked = checked;
        if (checked) {
            checkedItemCodes = $('#gridItem').data("kendoGrid").dataSource.dataFiltered().filter(item => item.MenuLevel == "Region").map(item => "1_" + item.ItemCode);

        } else {

            checkedItemCodes = [];
            checkedIds = {};
            populateItemGrid();
            return;
        }
        $('.checkbox').each(function (idx, item) {
            if (checked) {

                if (!($('.checkbox')[idx].checked)) {
                    //  $('.checkbox').find(".checkbox").attr("checked", "checked");

                    $(item).click();
                }
            }
        });

    });
    Utility.UnLoading();
    //$(".k-label")[0].innerHTML.replace("items", "records");
}
//bind click event to the checkbox
$("#showSelection").bind("click", function () {
    var checked = [];
    for (var i in checkedIds) {
        if (checkedIds[i]) {
            checked.push(i);
        }
    }


});


//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        td = $(this).closest("td"),
        row = $(this).closest("tr"),
        grid = $("#gridItem").data("kendoGrid"),
        dataItem = grid.dataItem(row);
    // td.addClass("top-arrow");
    checkedIds[dataItem.id] = checked;
    if (checked) {

        var filteredAry = checkedItemCodes.filter(function (e) { return e != "0_" + dataItem.ItemCode && e != "1_" + dataItem.ItemCode });
        checkedItemCodes = filteredAry;
        if (dataItem.MenuLevel == "Unit")
            return;
        checkedItemCodes.push("1_" + dataItem.ItemCode);
    }
    else {
        var filteredAry = checkedItemCodes.filter(function (e) { return e != "0_" + dataItem.ItemCode && e != "1_" + dataItem.ItemCode });
        checkedItemCodes = filteredAry;
        if (dataItem.MenuLevel == "Region")
            return;
        checkedItemCodes.push("0_" + dataItem.ItemCode);
        $('#header-chb').attr("checked", false);
    }


}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                .addClass("k-state-selected")
                .find(".checkbox")
                .attr("checked", "checked");
        }
    }
}

function InheritedMap() {
    Utility.Loading();
    var dropdownlist = $("#inputsite").data("kendoDropDownList");
    var siteCode = dropdownlist.value();
    SiteCode = siteCode;
    if (siteCode == 0 || siteCode == 'undefined') {

        return;
    }
    var siteName = dropdownlist.text();
    $("#location").text(" | " + siteName);
    regionID = sitemasterdata.filter(m => m.SiteCode == siteCode)[0].RegionID
    getSiteDataOnGo();
    //if (dishdata == null || dishdata.length == 0) {
    //    populateSiteDishDropdown();
    //}
   
    //InheritedSiteItem(siteCode);
    $("#actionRegion").show();
    $("#btnSaveChk").hide();
    $("#btnDrft").hide();
    $("#previewchk").hide();
    flagToggle = 0;
    toggleInherited();
    Utility.Loading();

}

function InheritedSiteItem(SiteCode, flag) {
    refreshSiteInheritanceData();
    //modifiedDataSource();
    if (flag) {
        populateItemGrid();
        return;
    }
    populateItemGrid_IN();


    //HttpClient.MakeSyncRequest(CookBookMasters.GetSiteItemInheritanceMappingDataList, function (result) {
    //    if (result != null) {
    //        console.log("Inheritance" + result);

    //        InheritedItem = result;
    //        modifiedDataSource();
    //        if (flag) {
    //            populateItemGrid();
    //            return;
    //        }
    //        populateItemGrid_IN();
    //    }
    //    else {

    //    }
    //}, { SiteCode: SiteCode }, true);
}

function populateItemGrid_IN() {

    //siteDataSource = siteMasterDataSource.filter(function (item) {
    //    return item.MenuLevel == "Unit";
    //});

    //if (siteDataSource.length == 0) {
    //    $("#toggleText").text("Build Menu");
    //    $("#myInput").hide();
    //    $("#btnSaveChk").hide();
    //    $("#btnDrft").hide();
    //    $("#previewchk").hide();
    //}
    //else {
    //    $("#myInput").show();
    //}

    $("#myInput").show();

    //grid.destroy();
    var gridVariable = $("#gridItem");
    gridVariable.html("");

    //grid.destroy();
    grid = gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,
        //selectable: true,
        //reorderable: true,
        //scrollable: true,
        columns: [

            {
                field: "ItemCode", title: "Item Code", width: "95px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                }
            },
            {
                field: "ItemName", title: "Item Name", attributes: {
                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                },
                template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
            },
            {
                field: "FoodProgramName", title: "Food Program", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "VisualCategoryName", title: "Visual Category", width: "155px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "DayPartNames", title: "Service Type", width: "130px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MenuLevel", title: "Menu Level", width: "135px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "Status", title: "Status", width: "80px", attributes: {
                    class: "mycustomstatus",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Mapped#}#',
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "DietCategoryName", title: "", width: "1px", attributes: {

                    style: "text-align: center; font-weight:normal;"
                },
                headerAttributes: {
                    style: "text-align: center; width:1px; display:none;"
                },
                template: '<span class="itemname"></span>',
            },

            {
                field: "Edit", title: "Action", width: "135px",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center; vertical-align:middle"
                },
                command: [
                    //{
                    //    title: 'Change Price',
                    //    name: 'Change Price',
                    //    click: function (e) {
                    //        Utility.Loading();
                    //        setTimeout(function () {
                    //            var dropdownlist = $("#inputsite").data("kendoDropDownList");
                    //            var siteName = dropdownlist.text();

                    //            var gridObj = $("#gridItem").data("kendoGrid");
                    //            console.log(gridObj);
                    //            if (typeof gridObj == 'undefined') {
                    //                return;
                    //            }
                    //            var x = $(e.currentTarget).closest("tr");
                    //            //    console.log(gridObj?.dataItem(x));
                    //            if (gridObj?.dataItem(x) != 'undefined')
                    //                tr = gridObj?.dataItem(x);
                    //            model = tr;
                    //            console.log(tr);
                    //            var dialog = $("#windowEdit").data("kendoWindow");

                    //            dialog.open();
                    //            dialog.center();

                    //            modelprice.ID = model.ID;
                    //            modelprice.Site_ID = model.Site_ID;
                    //            modelprice.ItemCode = model.ItemCode;
                    //            modelprice.ItemName = model.ItemName;
                    //            modelprice.ItemPrice = model.ItemPrice;
                    //            modelprice.Item_ID = model.Item_ID;
                    //            modelprice.SiteCode = model.SiteCode;
                    //            modelprice.Status = model.Status;
                    //            modelprice.CreatedOn = kendo.parseDate(model.CreatedOn);
                    //            modelprice.CreatedBy = model.CreatedBy;
                    //            modelprice.IsActive = model.IsActive;
                    //            $("#itemcode").val(model.ItemCode);
                    //            $("#itemname").val(model.ItemName);
                    //            $("#itemprice").val(model.ItemPrice);

                    //            $("#itemprice").focus();
                    //            Utility.UnLoading();
                    //        }, 2000)
                    //    }
                    //},
                    {
                        title: ' Map Dishes',
                        name: 'Map Dishes',
                        click: function (e) {
                            Utility.Loading();
                            setTimeout(function () {
                                var dropdownlist = $("#inputsite").data("kendoDropDownList");
                                var siteName = dropdownlist.text();
                                $("#topHeading").text("Unit Menu Item - Dish Mapping");//.append("( " + siteName + ")");
                                if (user.SectorNumber == "20") {
                                    $("#dayHeader").text("Start Typing / Choose Service Type for Mapping");
                                }

                                var gridObj = $("#gridItem").data("kendoGrid");
                                console.log(gridObj);
                                if (typeof gridObj == 'undefined') {
                                    return;
                                }
                                var x = $(e.currentTarget).closest("tr");
                                //    console.log(gridObj?.dataItem(x));
                                if (gridObj?.dataItem(x) != 'undefined')
                                    tr = gridObj?.dataItem(x);

                                itemObject.ItemName = tr.ItemName;
                                itemObject.ID = tr.Item_ID;
                                ID = tr.ID;
                                itemObject.Code = tr.ItemCode;
                                itemObject.ItemPrice = 100;
                                itemObject.DietCategorID = tr.DietCategory_ID;
                                selItemDietCategory = tr.DietCategoryCode;
                                itemObject.ImagePath = '../ItemImages/' + '/' + tr.ImageName;
                                $("#itemCode").text(tr.ItemCode);
                                $("#itemName").text(tr.ItemName);
                                $("#fpname").text(tr.FoodProgramName);
                                $("#fptype").text(tr.Foo);
                                $("#vcat").text(tr.VisualCategoryName);
                                //if (tr.AllergensName != "" && tr.AllergensName != null) {
                                //    var allergenText = " | Allergens: " + tr.AllergensName + "";
                                //    $("#inputItemAllergenLabel").text(allergenText);
                                //}
                                $("#iname").text(tr.ItemName + " (Item Code: " + tr.ItemCode + ")");
                                if (tr.ConceptType2Code == 'CT2-00001' || tr.ConceptType2Code == 'CT2-00003') {
                                    $("#fptype").text("Curated");
                                    flagCurated = 1;
                                } else {
                                    $("#fptype").text("Non-Curated");
                                    flagCurated = 0;
                                }
                                if (tr.DietCategory_ID == 3) {
                                    $("#itemDietCategoryStatus").removeClass("statusNonVeg statusEgg");
                                    $("#itemDietCategory").removeClass("squareNonVeg squareEgg");
                                    $("#itemDietCategoryStatus").addClass("statusVeg");
                                    $("#itemDietCategory").addClass("squareVeg")
                                } else if (tr.DietCategory_ID == 2) {
                                    $("#itemDietCategoryStatus").removeClass("statusVeg statusEgg");
                                    $("#itemDietCategory").removeClass("squareVeg squareEgg");
                                    $("#itemDietCategory").addClass("squareNonVeg")
                                    $("#itemDietCategoryStatus").addClass("statusNonVeg")
                                } else {
                                    $("#itemDietCategoryStatus").removeClass("statusNonVeg statusVeg");
                                    $("#itemDietCategory").removeClass("squareNonVeg squareVeg");
                                    $("#itemDietCategory").addClass("squareEgg")
                                    $("#itemDietCategoryStatus").addClass("statusEgg")

                                }
                                $("#itemPrice").text(itemObject.ItemPrice);
                                $("#ItemMaster").hide();
                                $("#ConfigMaster").show();
                                //$("#imageMenuItem").attr('src', itemObject.ImagePath);
                                $("#dishCategory").empty();
                                $("#configMaster").hide();
                                $("#mapMaster").show();
                                var dropdownlist = $("#inputsite").data("kendoDropDownList");
                                var siteCode = dropdownlist.value();
                                $("#configMaster").hide();
                                $("#mapMaster").show();
                                getSiteItemDishMpappingData(tr.ItemCode, siteCode)
                                sameSource(ID, tr.ItemCode, siteCode)
                                GetDayPartMapping();
                                grid.destroy();
                                $("#btnSubmitRecipe").css('display', 'inline');
                                $("#btnSubmitClose").css('display', 'inline');
                                Utility.UnLoading();
                            }, 2000)
                        }
                    }
                ]

            }
        ],
        dataSource: {
            data: siteInheritanceData.filter(m => m.IsInherited),

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ItemCode: { type: "string" },
                        ItemName: { type: "string" },
                        DietCategoryName: { type: "string" },
                        VisualCategoryName: { type: "string" },
                        FoodProgramName: { type: "string" },
                        HSNCode: { type: "string" },
                        CGST: { type: "string" },
                        SGST: { type: "string" },
                        CESS: { type: "string" },
                        Status: { type: "string" },
                        DayPartNames: { type: "string" },
                    }

                }
            },
            pageSize: 150
        },
        columnResize: function (e) {
            //var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var view = this.dataSource.view();

            for (var i = 0; i < view.length; i++) {
                if (view[i].Status == 1) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffe1e1");

                }
                else if (view[i].Status == 2) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffffbf");
                }
                else {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#d9ffb3");
                }

            }

            for (var i = 0; i < view.length; i++) {
                if (checkedIds[view[i].id]) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");
                    //.addClass("k-state-selected")
                }
                if (view[i].MenuLevel == "Unit" || view[i].MenuLevel == "Draft") {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");//.attr("disabled", "disabled");
                }
            }
            //var grid = this;
            //var rows = grid.items();

            //$(rows).each(function (e) {
            //    var row = this;
            //    var dataItem = grid.dataItem(row);

            //    if (dataItem.Status ==  "Unit") {
            //        grid.select(row);
            //    }
            //});

        },
        change: function (e) {
        },

        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var grid = $("#gridItem").data("kendoGrid");
            var data = grid.dataSource._data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }
    });
    //bind click event to the checkbox
    grid = grid.data("kendoGrid")

    if (user.SectorNumber != "30") {
        grid.hideColumn("FoodProgramName");
        grid.hideColumn("VisualCategoryName");
    }
    grid.table.on("click", ".checkbox", selectRow);
    //  grid.destroy();
    Utility.UnLoading();
}
function toggleInherited() {
    if (!flagToggle) {
        $("#toggleText").text("Add More");
        $("#btnSaveChk").hide();
        $("#btnDrft").hide();
        $("#previewchk").hide();
        $("#btnInherited").addClass("btn-info");
        $("#btnInherited").removeClass("btn-tool");
        $("#btnExport").show();
        $("#btnbackMain").hide();
        $("#btnInherited").text("Add More");
        $("#myInput").show();
        populateItemGrid_IN()
        flagToggle = 1;
        checkedItemCodes = [];
    }
    else {
        $("#myInput").show();
        $("#btnExport").hide();
        $("#btnSaveChk").show();
        $("#btnDrft").show();
        $("#previewchk").show();
        $("#toggleText").hide();
        $("#btnbackMain").show();
        $("#btnInherited").removeClass("btn-info");
        $("#btnInherited").addClass("btn-tool");
        $("#btnInherited").text("");
        populateItemGrid();
        flagToggle = 0;
        $('#header-chb').attr("checked", false).triggerHandler('change');
        checkedItemCodes = [];
    }

}

$("#toggleText").click(function () {
    if ($("#toggleText").text() == "Back") {
        // return false;
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
            function () {
                $("#btnSaveChk").hide();
                $("#btnDrft").hide();
                $("#previewchk").hide();
                $("#btnInherited").addClass("btn-info");
                $("#btnInherited").removeClass("btn-tool");
                $("#btnExport").show();
                populateItemGrid_IN()
                flagToggle = 1;
                checkedItemCodes = [];
            },
            function () {
            }
        );
    }
});


function back() {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
        function () {
            $("#btnInherited").text("Add More");
            $("#configMaster").show();
            $("#mapMaster").hide();
            $("#topHeading").text("Unit Menu Configuration");
            globalSearch($("#myInput").val());
            $("#btnSaveChk").hide();
            $("#btnbackMain").hide();
            $("#btnDrft").hide();
            $("#previewchk").hide();
            $("#btnInherited").addClass("btn-info");
            $("#btnInherited").removeClass("btn-tool");
            $("#btnExport").show();
            populateItemGrid_IN()
            flagToggle = 1;
            checkedItemCodes = [];
        },
        function () {
        }
    );

}

function saveProcedure(IsPublished) {
    //To check first Grid Preview
    if (globalChecked) {
        toastr.warning("Have patience ,this process might takes few minutes");
    }
    Utility.Loading();
    setTimeout(function () {
        var dropdownlist = $("#inputsite").data("kendoDropDownList");
        var siteCode = dropdownlist.value();

        itemcodes = "";
        Pub_checkedItemCodes = checkedItemCodes;
        if (IsPublished) {
            $('#gridItem').data("kendoGrid").dataSource.dataFiltered().filter(function (item) {

                if ((checkedItemCodes.indexOf("1_" + item.ItemCode) != -1) || (checkedItemCodes.indexOf("0_" + item.ItemCode) != -1)) {
                    return item;
                }
                else if (item.MenuLevel == "Draft") {
                    Pub_checkedItemCodes.push("1_" + item.ItemCode);
                }

            });
            checkedItemCodes = Pub_checkedItemCodes;
            $("#btnPreview").attr('checked', false);;
            //return;
        }
        for (item of checkedItemCodes) {
            if (itemcodes.length > 0) {
                itemcodes = itemcodes + "," + item;
            }
            else
                itemcodes = item;
        }
        if (itemcodes.length == 0) {
            toastr.error("Please select or de-select at least one Item to continue");
            Utility.UnLoading();
            return;
        }
        var saveModel = {
            "SiteCode": siteCode,
            "ItemCode": itemcodes,
            "IsActive": IsPublished ? 1 : 0,
            "Status": IsPublished ? 3 : 2,//Check if needed
        }

        HttpClient.MakeSyncRequest(CookBookMasters.SaveRegionToSiteInheritance, function (result) {
            if (result == false) {
                toastr.error("Some error occured, please try again.");
            }
            else {
                $(".k-overlay").hide();
                if (IsPublished) {
                    $("#topHeading").text("Unit Menu Configuration");
                    toastr.success("Selected Menu Items have been inherited. Do not forget to Publish the same");

                    checkedItemCodes = [];
                    checkedIds = {};
                    InheritedMap();
                    //refreshSiteInheritanceData();
                    //populateItemGrid_IN();
                } else {

                    Toast.fire({
                        type: 'success',
                        title: 'Item - Dish mapping saved',
                        timer: 5000
                    });
                    var dropdownlist = $("#inputsite").data("kendoDropDownList");
                    var SiteCode = dropdownlist.value();
                    if (SiteCode == 0 || SiteCode == 'undefined') {
                        return;
                    }

                    InheritedSiteItem(SiteCode, 1);
                }
            }
        }, {
            model: saveModel
        }, true);
        Utility.UnLoading();
    }, 2000);


}
function saveToSiteInheritance(saveModel) {
    Utility.Loading();
    setTimeout(function () {
        HttpClient.MakeSyncRequest(CookBookMasters.SaveToSiteInheritance, function (result) {
            if (result == false) {
                toastr.error("Some error occured, please try again.");
            }
            else {
                $(".k-overlay").hide();

                toastr.success("Unit Item Price is Saved");


                InheritedSiteItem(SiteCode);
            }

        }, {
            model: saveModel
        }, true);

        Utility.UnLoading();
    }, 2000);

}
function preview(e) {

    if (e.checked)
        populateItemGridPreview();
    else
        populateItemGrid();

}
function populateItemGridPreview() {

    var previewDataSource = $('#gridItem').data("kendoGrid").dataSource.dataFiltered().filter(function (item) {

        if ((checkedItemCodes.indexOf("1_" + item.ItemCode) != -1) || (checkedItemCodes.indexOf("0_" + item.ItemCode) != -1) || item.MenuLevel == "Draft") {
            return item;
        }

    });
    var gridVariable = $("#gridItem");
    gridVariable.html("");
    var grid1 = gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,

        columns: [

            {
                field: "ItemCode", title: "Item Code", width: "80px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                }
            },
            {
                field: "ItemName", title: "Item Name", attributes: {

                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                },
                template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
            },
            {
                field: "FoodProgramName", title: "Food Program", width: "160px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "VisualCategoryName", title: "Visual Category", width: "155px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MenuLevel", title: "Menu Level", width: "135px",
                attributes: {
                    class: "mycustomstatus",
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
        ],
        dataSource: {
            data: previewDataSource,
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ItemCode: { type: "string" },
                        ItemName: { type: "string" },
                        DietCategoryName: { type: "string" },
                        VisualCategoryName: { type: "string" },
                        FoodProgramName: { type: "string" },
                        HSNCode: { type: "string" },
                        CGST: { type: "string" },
                        SGST: { type: "string" },
                        CESS: { type: "string" },
                    }
                }
            },
            pageSize: 150
        },
        columnResize: function (e) {
            //var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (checkedIds[view[i].id] == false) {
                    var textValue = this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text() + " (Removed)";
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffe1e1");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text("Removed");

                } else {
                    var textValue = this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text() + " (Added)";
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text("Added");
                }

            }


        },
        change: function (e) {
        },
        //filter: { field: "ItemCode", operator: "gt", value: "69999" }
    });
    var grid = grid1.data("kendoGrid")
    if (user.SectorNumber != "30") {
        grid.hideColumn("FoodProgramName");
        grid.hideColumn("VisualCategoryName");
    }
}

function updateWtPerUOMQuantity(iqty) {
    var qty = $("#" + iqty.id).val();
    var idstobeupdated = [];
    var idnum = iqty.id.substring(8);
    console.log(idnum);
    for (item of finalModelExitsDishCategory) {
        if (idnum == item.DishCategory_ID) {
            item.WeightPerUOM = qty;
        }
    }

    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {
        var sm = subItem[i].id;
        $("#wtuomdishuom" + sm).val(qty);
        $("#qserwtdishuom" + sm).text(qty);
        $("#wtuomdishuomtype" + sm).val(qty);
        //var ins = "wtuomdishuom" + sm
        // calculateCostPerServing(sm);
        //idstobeupdated.push(ins);
    }
    //Master to be updated


    for (item of masterGroupList) {
        if (item.dropdownid == "inputdish" + idnum) {
            item.WeigthPerUOM = qty;

        }
    }

}

// Initializes a new instance of the StringBuilder class
// and appends the given value if supplied
function StringBuilder(value) {
    this.strings = new Array("");
    this.append(value);
}

// Appends the given value to the end of this instance.
StringBuilder.prototype.append = function (value) {
    if (value) {
        this.strings.push(value);
    }
}

// Clears the string buffer
StringBuilder.prototype.clear = function () {
    this.strings.length = 1;
}

// Converts this instance to a String.
StringBuilder.prototype.toString = function () {
    return this.strings.join("");
}