﻿//import { Debugger } from "../../Content/css/bootstrap-colorpicker/js/bootstrap-colorpicker";

var user;
var status = "";
var varname = "";
var datamodel;
var RecipeName = "";
var RecipeID = "";
var RecipeCode = "";
var uomdata = [];
var moguomdata = [];
var uommodulemappingdata = [];
var recipecategoryuomdata = [];
var basedata = [];
var basedataList = [];
var dishdata = [];
var baserecipedata = [];
var baserecipedatafiltered = [];
var mogdata = [];
var mogdataList = [];
var mogWiseAPLMasterdataSource = [];
var MOGCode = "MOG-00156";
var MOGName = "Test"
var IsMajor = 0;
var isBaseRecipe = 0;
var MajorPercentData = 5;
var x = 'x';
var isBRConfigurationChange = true;
var isBRNameChange = false;
var isPublishBtnClick = false;
var isStatusSave = false;
var applicationSettingData = [];
var resetID, gridIdin;
var copyData = [];
var mogtotal = 0;
var BaseRecipetotal = 0;
var ToTIngredients = 0;
var RecipeWisenutrientMasterdataSource = [];
var checkedNutrientCodes = [];
var nutrientFilterDataSource = [];
var nutrientMasterdataSource = [];
var RecipeWiseNutrientArray = [];
var isShowElmentoryRecipe = false;
var moguomdetailsdata = [];
var subsectordata = [];


$(function () {
    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    //array type id class  step1 multiple dics ids
    $("#windowEditx").kendoWindow({
        modal: true,
        width: "1200px",
        height: "550px",
        title: "New Base Recipe Creation",
        actions: ["Close"],
        visible: false,
        draggable: false,
        resizable: false,
        animation: false
    });


    $("#windowEdit1").kendoWindow({
        modal: true,
        width: "1200px",
        height: "550px",
        title: "Recipe Details - " + RecipeName,
        actions: ["Close"],
        visible: false,
        draggable: false,
        resizable: false,
        animation: false
    });

    $("#windowEditNutritionInfo").kendoWindow({
        modal: true,
        width: "800px",
        height: "450px",
        title: "Recipe Details - " + RecipeName,
        actions: ["Close"],
        visible: false,
        draggable: false,
        resizable: false,
        animation: false
    });

    $("#windowEdit2").kendoWindow({
        modal: true,
        width: "1200px",
        height: "550px",
        title: "Recipe Details - " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#windowEdit3").kendoWindow({
        modal: true,
        width: "1200px",
        height: "550px",
        title: "Recipe Details - " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#windowEdit4").kendoWindow({
        modal: true,
        width: "1200px",
        height: "550px",
        title: "Recipe Details - " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#windowEdit5").kendoWindow({
        modal: true,
        width: "1200px",
        height: "550px",
        title: "Recipe Details - " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#windowEditMOGWiseAPL").kendoWindow({
        modal: true,
        width: "900px",
        height: "300px",
        cssClass: 'aplPopup',
        top: '80px !important',
        title: "MOG APL Detials - " + MOGName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#windowEditRecipeDependencies").kendoWindow({
        modal: true,
        width: "1050px",
        height: "570px",
        left: '220px !important',
        top: '20px !important',
        title: "Base Recipe - " + RecipeName + " Dependencies",
        // actions: ["Close"],
        visible: false,
        animation: false,
        close: function (e) {
            if (isStatusSave) {
                $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsActive);
            }
        }
    });

    $("#windowEditRecipeWiseNutrient").kendoWindow({
        modal: true,
        width: "1050px",
        height: "480px",
        left: '220px !important',
        top: '80px !important',
        title: MOGName + " Details: ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#windowEditExportRecipe").kendoWindow({
        modal: true,
        width: "350px",
        height: "110px",
        //left: '220px !important',
        //top: '80px !important',
        title: RecipeName + " Export: ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $('#inputquantity').val(10);
    $('#inputquantity0').val(10);
    $('#inputquantity1').val(10);
    $('#inputquantity2').val(10);
    $('#inputquantity3').val(10);
    $('#inputquantity4').val(10);
    $('#inputquantity5').val(10);
    $('#inputquantityx').val(10);


    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }


    $(document).ready(function () {

        instructionTextEditor('inputinstruction');
        instructionTextEditor('inputinstruction0');
        instructionTextEditor('inputinstruction1');
        instructionTextEditor('inputinstruction2');
        instructionTextEditor('inputinstruction3');
        instructionTextEditor('inputinstruction4');
        instructionTextEditor('inputinstruction5');
        instructionTextEditor('inputinstructionx');



        var editor1 = $("#inputinstruction1").data("kendoEditor"),
            editorBody1 = $(editor1.body);
        editorBody1.removeAttr("contenteditable").find("a").on("click.readonly", false)
        var editor2 = $("#inputinstruction2").data("kendoEditor"),
            editorBody2 = $(editor2.body);
        editorBody2.removeAttr("contenteditable").find("a").on("click.readonly", false)
        var editor3 = $("#inputinstruction3").data("kendoEditor"),
            editorBody3 = $(editor3.body);
        editorBody3.removeAttr("contenteditable").find("a").on("click.readonly", false)
        var editor4 = $("#inputinstruction4").data("kendoEditor"),
            editorBody4 = $(editor4.body);
        editorBody4.removeAttr("contenteditable").find("a").on("click.readonly", false)
        var editor5 = $("#inputinstruction5").data("kendoEditor"),
            editorBody5 = $(editor5.body);
        editorBody5.removeAttr("contenteditable").find("a").on("click.readonly", false)


        $("#btnCancel").click(function (e) {
            Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
                function () {
                    $("#mainContent").show();
                    $("#windowEdit").hide();
                },
                function () {
                }
            );
        });

        $("#btnExport").click(function (e) {
            var grid = $("#gridRecipe").data("kendoGrid");
            grid.saveAsExcel();
        });
        //Krish 03-08-2022
        //loadFormData();
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {

            user = result;
            if (user.SectorNumber == "80") {
                $(".healthcareonly").show();

            }
            else {
                $(".healthcareonly").hide();
            }

            if (user.SectorNumber == "20") {
                $(".rcHide").show();
                $(".mfgHide").hide();
            }
            else {
                $(".rcHide").hide();
                $(".mfgHide").show();
            }

            if (user.SectorNumber == "10") {
                $(".googleOnly").show();
            }
            else {
                $(".googleOnly").hide();
            }
            //HttpClient.MakeRequest(CookBookMasters.GetApplicationSettingDataList, function (result) {

            //if (result != null) {
            //    applicationSettingData = result;
            //    for (item of applicationSettingData) {
            //        if (item.Code == "FLX-00004")//item.Name == "Major Identification Threshold" || 
            //            MajorPercentData = item.Value;
            //        $(".asPercent").text(MajorPercentData);
            //    }

            //}
            populateRecipeGrid();

            //}, null, false);

        }, null, true);

    });
    
    
    $("#InitiateBulkChanges").on("click", function () {
        $("#gridBulkChange").css("display", "block");
        $("#gridBulkChange").children(".k-grid-header").css("border", "none");
        $("#InitiateBulkChanges").css("display", "none");
        $("#CancelBulkChanges").css("display", "");
        $("#SaveBulkChanges").css("display", "");
        $("#bulkerror").css("display", "none");
        $("#success").css("display", "none");
        populateUOMDropdown_Bulk();
        populateBaseDropdown_Bulk();
    });



    $("#CancelBulkChanges").on("click", function () {
        $("#InitiateBulkChanges").css("display", "");
        $("#CancelBulkChanges").css("display", "none");
        $("#SaveBulkChanges").css("display", "none");
        $("#gridBulkChange").css("display", "none");
        $("#bulkerror").css("display", "none");
        $("#success").css("display", "none");
        populateRecipeGrid();
    });

    $("#SaveBulkChanges").on("click", function () {

        var neVal = $(this).val();
        var dataFiltered = $("#gridRecipe").data("kendoGrid").dataSource.dataFiltered();
        var model = dataFiltered;
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        var validChange = false;
        var uom = "";
        var quantity = "";
        var base = "";

        var uomspan = $("#uom");
        var quantityspan = $("#quantity");
        var basespan = $("#base");

        if (uomspan.val() != "Select") {
            validChange = true;
            uom = uomspan.val();
        }
        if (quantityspan.val() != "") {
            validChange = true;
            quantity = quantityspan.val();
        }

        if (basespan.val() != "Select") {
            validChange = true;
            base = basespan.val();
        }
        if (validChange == false) {
            toastr.error("Please change at least one attribute for Bulk Update.");
        } else {
            $("#bulkerror").css("display", "none");
            Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                function () {
                    $.each(model, function () {
                        this.CreatedOn = kendo.parseDate(this.CreatedOn);
                        this.ModifiedBy = user.UserId;
                        this.ModifiedOn = Utility.CurrentDate();
                        if (uom != "")
                            this.UOM_ID = uom;
                        if (quantity != "")
                            this.Quantity = quantity;
                        if (base != "")
                            if (base == "Yes")
                                this.IsBase = true;
                            else
                                this.IsBase = false;
                    });

                    HttpClient.MakeRequest(CookBookMasters.SaveRecipeDataList, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again.");
                        }
                        else {
                            toastr.success("Records have been updated successfully");
                            $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                            $("#gridRecipe").data("kendoGrid").dataSource.read();
                            $("#InitiateBulkChanges").css("display", "");
                            $("#CancelBulkChanges").css("display", "none");
                            $("#SaveBulkChanges").css("display", "none");
                            $("#gridBulkChange").css("display", "none");
                            setTimeout(fade_out, 10000);

                            function fade_out() {
                                $("#success").fadeOut();
                            }
                        }
                    }, {
                        model: model

                    }, true);

                    //populateSiteGrid();
                },
                function () {
                    maptype.focus();
                }
            );
        }
        Utility.UnLoading();
    });
    //Food Program Section Start
    $('#myInputAPL').on('input', function (e) {
        var grid = $('#gridMOGWiseAPL').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ArticleNumber" || x.field == "ArticleDescription" || x.field == "UOM" || x.field == "ArticleType"
                    || x.field == "Hierlevel3" || x.field == "MerchandizeCategoryDesc" || x.field == "MOGName") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });


    $('#myInput').on('input', function (e) {
        var grid = $('#gridRecipe').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "RecipeAlias" || x.field == "RecipeType" || x.field == "PublishedDateAsString" || x.field == "RecipeCode" || x.field == "Name"
                    || x.field == "UOMName" || x.field == "Quantity" || x.field == "IsBase" || x.field == "Status" || x.field == "IsActive" || x.field == "IsElementoryString") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        if (x.field == "Status") {
                            var pendingString = "pending";
                            var savedString = "saved";
                            var mappedString = "published";
                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "3";
                            }
                        }

                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    }
                    else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && e.target.value !== null) {
                        // var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: e.target.value
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });


    $("#addbr").on("click", function () {

        $("#gridBaseRecipe").css("display", "block");
        $("#emptybr").css("display", "none");
        $("#gridBaseRecipe").data("kendoGrid").addRow();
    })

    $("#addmog").on("click", function () {
        $("#gridMOG").css("display", "block");
        $("#emptymog").css("display", "none");
        $("#gridMOG").data("kendoGrid").addRow();
    })

    $("#addbrx").on("click", function () {

        $("#gridBaseRecipex").css("display", "block");
        $("#emptybrx").css("display", "none");
        $("#gridBaseRecipex").data("kendoGrid").addRow();
    })

    $("#addbr0").on("click", function () {

        $("#gridBaseRecipe0").css("display", "block");
        $("#emptybr0").css("display", "none");
        $("#gridBaseRecipe0").data("kendoGrid").addRow();
    })


    $("#addbr1").on("click", function () {
        return;
        $("#gridBaseRecipe1").css("display", "block");
        $("#emptybr1").css("display", "none");
        $("#gridBaseRecipe1").data("kendoGrid").addRow();
    })
    $("#addbr2").on("click", function () {
        return;
        $("#gridBaseRecipe2").css("display", "block");
        $("#emptybr2").css("display", "none");
        $("#gridBaseRecipe2").data("kendoGrid").addRow();
    })
    $("#addbr3").on("click", function () {
        return;
        $("#gridBaseRecipe3").css("display", "block");
        $("#emptybr3").css("display", "none");
        $("#gridBaseRecipe3").data("kendoGrid").addRow();
    })
    $("#addbr4").on("click", function () {
        return;
        $("#gridBaseRecipe4").css("display", "block");
        $("#emptybr4").css("display", "none");
        $("#gridBaseRecipe4").data("kendoGrid").addRow();
    })
    $("#addbr5").on("click", function () {
        return;
        $("#gridBaseRecipe5").css("display", "block");
        $("#emptybr5").css("display", "none");
        $("#gridBaseRecipe5").data("kendoGrid").addRow();
    })
    $("#addmogx").on("click", function () {

        $("#gridMOGx").css("display", "block");
        $("#emptymogx").css("display", "none");
        $("#gridMOGx").data("kendoGrid").addRow();
    })
    $("#addmog0").on("click", function () {

        $("#gridMOG0").css("display", "block");
        $("#emptymog0").css("display", "none");
        $("#gridMOG0").data("kendoGrid").addRow();
    })
    $("#addmog1").on("click", function () {
        return;
        $("#gridMOG1").css("display", "block");
        $("#emptymog1").css("display", "none");
        $("#gridMOG1").data("kendoGrid").addRow();
    })
    $("#addmog2").on("click", function () {
        return;
        $("#gridMOG2").css("display", "block");
        $("#emptymog2").css("display", "none");
        $("#gridMOG2").data("kendoGrid").addRow();
    })
    $("#addmog3").on("click", function () {
        return;
        $("#gridMOG3").css("display", "block");
        $("#emptymog3").css("display", "none");
        $("#gridMOG3").data("kendoGrid").addRow();
    })
    $("#addmog4").on("click", function () {
        return;
        $("#gridMOG4").css("display", "block");
        $("#emptymog4").css("display", "none");
        $("#gridMOG4").data("kendoGrid").addRow();
    })
    $("#addmog5").on("click", function () {
        return;
        $("#gridMOG5").css("display", "block");
        $("#emptymog5").css("display", "none");
        $("#gridMOG5").data("kendoGrid").addRow();
    })
    

    $("#AddNew").on("click", function () {
        //Krish 03-08-2022
        loadFormData();
        //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {

        //    var dataSource = data;

        //    baserecipedata = [];
        //    baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        if (dataSource[i].CostPerUOM == null) {
        //            dataSource[i].CostPerUOM = 0;
        //        }
        //        if (dataSource[i].CostPerKG == null) {
        //            dataSource[i].CostPerKG = 0;
        //        }
        //        if (dataSource[i].TotalCost == null) {
        //            dataSource[i].TotalCost = 0;
        //        }
        //        if (dataSource[i].Quantity == null) {
        //            dataSource[i].Quantity = 0;
        //        }
        //        if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
        //            baserecipedata.push({
        //                "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
        //            });
        //        else
        //            baserecipedata.push({
        //                "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
        //            });

        //    }
        //    baserecipedatafiltered = baserecipedata;
        //    basedataList = baserecipedata;
        //}, { condition: "Base", DishCode: "0" }, true);
        setTimeout(function () {
            $("#gridBaseRecipe0").css("display", "None");
            $("#gridMOG0").css("display", "None");
            $("#success").css("display", "none");
            $("#error0").css("display", "none");
         
            var editor = $("#inputinstruction0").data("kendoEditor");
            editor.value('');
            var model;

            //$(".k-overlay").css("display", "block");
            // $(".k-overlay").css("opacity", "0.5");

            $("#windowEdit0").css("display", "block");

            populateUOMDropdown0();
            // if (user.SectorNumber == "20" ){
          
            if (user.SectorNumber == "20") {
                populateRecipeCategoryUOMDropdown0();
                populateWtPerUnitUOMDropdown0();
                populateGravyWtPerUnitUOMDropdown0();
                //populateRecipeCategoryUOMDropdown("0")
                $("#inputwtperunituom0").closest(".k-widget").hide();
                $("#inputgravywtperunituom0").closest(".k-widget").hide();
                $("#inputuom0").closest(".k-widget").hide();
            }
            else if (user.SectorNumber == "10") {
                populateSubSectorDropdown("0");
            }
            populateBaseDropdown0();
            populateBaseRecipeGrid0();
            populateMOGGrid0();
            $("#mainContent").hide();
            $("#windowEdit0").show();

            datamodel = model;
            $("#inputinstruction0").val("");
            $("#recipeid0").text("");
            $("#inputrecipename0").val("");
            $("#inputrecipealiasname0").val("");
            $("#grandTotal0").text("");
            $("#grandCostPerKG0").text("");
            $("#ToTIngredients0").text("");

            // if (user.SectorNumber == "20" ){
            if (user.SectorNumber == "20") {
                $("#inputwtperunit0").val("");
                $("#inputgravywtperunit0").val("");
                $("#inputuom0").data("kendoDropDownList").value("Select");
                $("#inputrecipecum0").data("kendoDropDownList").value("Select");
                $("#inputwtperunituom0").data("kendoDropDownList").value("Select");
                $("#inputgravywtperunituom0").data("kendoDropDownList").value("Select");
                $("#inputbase0").data('kendoDropDownList').value("Yes");
                var recipeCategory = "";
                var rCatData = $("#inputrecipecum0").data('kendoDropDownList')
                if (rCatData != null && rCatData != "Select" && rCatData != "" && rCatData.dataItem() != null && rCatData.dataItem().UOMCode != "Select") {
                    recipeCategory = rCatData.dataItem().UOMCode
                }
                OnRecipeUOMCategoryChange(recipeCategory, "0");
            }
            else if (user.SectorNumber == "10") {
                var inputwtperunituom0 = $("#inputuom0").data("kendoDropDownList");
                inputwtperunituom0.enable(false);
                $("#inputsubsector0").data('kendoDropDownList').value("Select Sub Sector");
            }
            else {
                var inputwtperunituom0 = $("#inputuom0").data("kendoDropDownList");
                inputwtperunituom0.enable(false);
            }
            $("#copyRecipe").data("kendoDropDownList").value(0);
            $("#inputquantity0").val("10");

            // $("#gridBaseRecipe0").data("kendoGrid").addRow();
            // $("#gridMOG0").data("kendoGrid").addRow();
            // hideGrid('gridBaseRecipe0', 'emptybr0', 'baseRecipePlus0', 'baseRecipeMinus0');
            // hideGrid('gridMOG0', 'emptymog0', 'mogPlus0', 'mogMinus0');
            $("#gridMOG").css("display", "none");
            $("#emptymog0").css("display", "block");
            $("#gridBaseRecipe").css("display", "none");
            $("#emptybr0").css("display", "block");
            // if (user.SectorNumber == "20" ){
            if (user.SectorNumber == "20") {
                $('#inputquantity0').removeAttr("disabled");
                $("#inputquantity0").val("");
            }
            else {
                $('#inputquantity0').attr('disabled', 'disabled');
            }
            $("#inputbase0").data('kendoDropDownList').value("Yes");
            Utility.UnLoading();
        } , 2000);
    })

    $("#btnCancel0").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                $("#emptybr0").css("display", "block");
                $("#gridBaseRecipe0").css("display", "none");
                $("#mainContent").show();
                $("#windowEdit0").hide();
                $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                $("#gridRecipe").data("kendoGrid").dataSource.read();
            },
            function () {
            }
        );
    });

    $("#btnCancelx").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEditx").data("kendoWindow");
                $("#emptybrx").css("display", "block");
                $("#gridBaseRecipex").css("display", "none");
                orderWindow.close();
            },
            function () {
            }
        );
    });


    $("#btnCancel1").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit1").data("kendoWindow");
        $("#emptybr1").css("display", "block");
        $("#gridBaseRecipe1").css("display", "none");
        orderWindow.close();


    });

    $("#btnNutritionData").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditNutritionInfo").data("kendoWindow");
        orderWindow.open();

    });


    $("#btnCancel2").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit2").data("kendoWindow");
        $("#emptybr2").css("display", "block");
        $("#gridBaseRecipe2").css("display", "none");
        orderWindow.close();
        $("#windowEdit1").parent().show();
    });
    $("#btnCancel3").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit3").data("kendoWindow");
        $("#emptybr3").css("display", "block");
        $("#gridBaseRecipe3").css("display", "none");
        orderWindow.close();
        $("#windowEdit2").parent().show();
    });

    $("#btnCancel4").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit4").data("kendoWindow");
        $("#emptybr4").css("display", "block");
        $("#gridBaseRecipe4").css("display", "none");
        orderWindow.close();
        $("#windowEdit3").parent().show();
    });
    $("#btnCancel5").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit5").data("kendoWindow");
        $("#emptybr5").css("display", "block");
        $("#gridBaseRecipe5").css("display", "none");
        orderWindow.close();
        $("#windowEdit4").parent().show();
    });
    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();
        // var orderWindow = $("#windowEdit1").data("kendoWindow");
        // "none");
        // orderWindow.close();
    });
    //$("#btnCancel").on("click", function () {
    //    $(".k-overlay").hide();
    //    var orderWindow = $("#windowEdit").data("kendoWindow");
    //    orderWindow.close();
    //});
    $("#btnSubmit0").click(function () {
        isPublishBtnClick = false;
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie
        //if (!validateDataBeforeSave("gridBaseRecipe0", 'gridMOG0')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        if ($("#inputrecipename0").val() == "") {
            toastr.error("Pleaes enter Recipe Name");
            $("#inputrecipename0").focus();
            Utility.UnLoading();
            return false;
        }


        if (user.SectorNumber == "20") {

            if ($("#inputrecipecum0").val() == null || $("#inputrecipecum0").val() == "" || $("#inputrecipecum0").val() == undefined || $("#inputrecipecum0").val() == "Select") {
                toastr.error("Please select Recipe UOM Category");
                Utility.UnLoading();
                return false;
            }
        }
        if ($("#inputquantity0").val() == null || $("#inputquantity0").val() == "" || $("#inputquantity0").val() == undefined) {
            toastr.error("Please enter Recipe Quantity");
            Utility.UnLoading();
            return false;
        }

        if ($("#inputuom0").val() == null || $("#inputuom0").val() == "" || $("#inputuom0").val() == undefined || $("#inputuom0").val() == "Select") {
            toastr.error("Please select UOM");
            Utility.UnLoading();
            return false;
        }


        var base;
        if ($("#inputbase0").val() == "Yes")
            base = true
        else if ($("#inputbase0").val() == "No")
            base = false
        else {
            toastr.error("Select Recipe Type!");
            Utility.UnLoading();
            return false;
        }

        var brgrid = $("#gridBaseRecipe0").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= MajorPercentData) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode").val(),
                "Recipe_ID": $("#recipeid").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy,
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG0").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= MajorPercentData) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode0").val(),
                "Recipe_ID": $("#recipeid0").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "DKgValue": data[i].DKgTotal,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie

        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom0").val();
        });
        var dkgPerUOM = 0;
        if ($("#ToTIngredients").text() != undefined && $("#ToTIngredients").text() != "") {
            var totalDKG = parseFloat($("#ToTIngredients").text());
            dkgPerUOM = totalDKG / $("#inputquantity0").val();
        }

        var model = {
            "RecipeCode": $("#inputrecipecode0").val(),
            "ID": $("#recipeid0").val(),
            "Name": titleCase($("#inputrecipename0").val()),
            "RecipeAlias": titleCase($("#inputrecipealiasname0").val()),
            "UOMCode": objUOM[0].UOMCode,
            "UOM_ID": $("#inputuom0").val(),
            "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum0").val() : "",
            "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit0").val() : "",
            "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom0").val() : "",
            "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit0").val() : "",
            "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom0").val() : "",
            "SubSectorCode": user.SectorNumber == "10" ? $("#inputsubsector0").val() : "",
            "TotalIngredientWeight": $("#ToTIngredients").text(),
            "DKgPerUOM": dkgPerUOM,
            "Quantity": $("#inputquantity0").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG0").text().substring(1).replace(",", ""),
            "CostPerUOM": $("#grandCostPerKG0").text().substring(1).replace(",", ""),
            "TotalCost": $('#grandTotal0').text().substring(1).replace(",", ""),
            "Quantity": $('#inputquantity0').val(),
            "Instructions": $("#inputinstruction0").val(),
            "IsActive": 1,
            "Status": isPublishBtnClick ? 3 : 2,
            "CreatedOn": (datamodel != undefined && datamodel != null && datamodel.CreatedOn != undefined) ? kendo.parseDate(datamodel.CreatedOn) : Utility.CurrentDate(),
            "CreatedBy": (datamodel != undefined && datamodel != null && datamodel.CreatedBy != undefined) ? datamodel.CreatedBy : user.UserId
        }

        if ($("#inputrecipename0").val() === "") {
            // $("#error").css("display", "flex");
            //    $("#error").find("p").text("Please provide valid input(s)");
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename0").focus();
        }
        else {
            // $("#error").css("display", "none");
            var base;
            if ($("#inputbase0").val() == "Yes")
                base = true
            else if ($("#inputbase0").val() == "No")
                base = false
            else {
                toastr.error("Select Recipe Type!");
                return false;
            }

            $("#btnSubmit0").attr('disabled', 'disabled');
            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnSubmit0').removeAttr("disabled");
                    $("#inputrecipename0").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnSubmit0').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        $("#inputrecipename0").focus();
                    }
                    else if (result == "Duplicate") {
                        $('#btnSubmit0').removeAttr("disabled");
                        toastr.error("Recipe Name already exits , kindly check");
                        $("#inputrecipename").focus();
                    }
                    else {
                        $(".k-overlay").hide();
                        $('#btnSubmit0').removeAttr("disabled");
                        Toast.fire({
                            type: 'success',
                            title: 'Recipe data saved'
                        });
                        if (model.ID == "") {
                            $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                            $("#gridRecipe").data("kendoGrid").dataSource.read();
                            onSaveRecipeSetData();
                        }
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });
    $("#btnPublish0").click(function () {

        HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {

            var dataSource = data;

            baserecipedata = [];
            baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].CostPerUOM == null) {
                    dataSource[i].CostPerUOM = 0;
                }
                if (dataSource[i].CostPerKG == null) {
                    dataSource[i].CostPerKG = 0;
                }
                if (dataSource[i].TotalCost == null) {
                    dataSource[i].TotalCost = 0;
                }
                if (dataSource[i].Quantity == null) {
                    dataSource[i].Quantity = 0;
                }
                if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });
                else
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });

            }
            baserecipedatafiltered = baserecipedata;
            basedataList = baserecipedata;
        }, { condition: "Base", DishCode: "0" }, true);
        isPublishBtnClick = true;
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie
        //if (!validateDataBeforeSave("gridBaseRecipe0", 'gridMOG0')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe0").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= MajorPercentData) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode").val(),
                "Recipe_ID": $("#recipeid").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG0").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode0").val(),
                "Recipe_ID": $("#recipeid0").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "DKgValue": data[i].DKgTotal,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1,
                "Status": isPublishBtnClick ? 3 : 2,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputrecipename0").val() === "") {
            // $("#error").css("display", "flex");
            //    $("#error").find("p").text("Please provide valid input(s)");
            toastr.error("Please provide Recipe Name");
            $("#inputrecipename0").focus();
            Utility.UnLoading();
            return;
        }
        if (user.SectorNumber == "20") {

            if ($("#inputrecipecum0").val() == null || $("#inputrecipecum0").val() == "" || $("#inputrecipecum0").val() == undefined || $("#inputrecipecum0").val() == "Select") {
                toastr.error("Please select Recipe UOM Category");
                Utility.UnLoading();
                return false;
            }
        }

        if ($("#inputuom0").val() == null || $("#inputuom0").val() == "" || $("#inputuom0").val() == undefined || $("#inputuom0").val() == "Select") {
            toastr.error("Please select UOM");
            Utility.UnLoading();
            return false;
        }
        if ($("#inputbase0").val() == "Yes")
            base = true
        else if ($("#inputbase0").val() == "No")
            base = false
        else {
            toastr.error("Select Recipe Type");
            Utility.UnLoading();
            return false;
        }
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom0").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode0").val(),
            "ID": $("#recipeid0").val(),
            "Name": titleCase($("#inputrecipename0").val()),
            "RecipeAlias": titleCase($("#inputrecipealiasname0").val()),
            "UOM_ID": $("#inputuom0").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity0").val(),
            "IsBase": base,
            "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum0").val() : "",
            "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit0").val() : "",
            "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom0").val() : "",
            "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit0").val() : "",
            "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom0").val() : "",
            "SubSectorCode": user.SectorNumber == "10" ? $("#inputsubsector0").val() : "",
            "TotalIngredientWeight": $("#ToTIngredients").text(),
            "CostPerKG": $("#grandCostPerKG0").text().substring(1).replace(",", ""),
            "CostPerUOM": $("#grandCostPerKG0").text().substring(1).replace(",", ""),
            "TotalCost": $('#grandTotal0').text().substring(1).replace(",", ""),
            "Quantity": $('#inputquantity0').val(),
            "Instructions": $("#inputinstruction0").val(),
            "IsActive": 1,
            "Status": isPublishBtnClick ? 3 : 2,
            "CreatedOn": (datamodel != undefined && datamodel != null && datamodel.CreatedOn != undefined) ? kendo.parseDate(datamodel.CreatedOn) : Utility.CurrentDate(),
            "CreatedBy": (datamodel != undefined && datamodel != null && datamodel.CreatedBy != undefined) ? datamodel.CreatedBy : user.UserId
        }

        if ($("#inputrecipename0").val() === "") {
            toastr.error("Please provide Recipe Name");
            $("#inputrecipename0").focus();
            return;
        }
        else {
            // $("#error").css("display", "none");
            var base;
            if ($("#inputbase0").val() == "Yes")
                base = true
            else if ($("#inputbase0").val() == "No")
                base = false
            else {
                toastr.error("Please select Recipe Type");
                $("#inputbase0").focus();
                return;
            }
            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnPublish0").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnPublish0').removeAttr("disabled");
                    $("#inputrecipename0").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnPublish0').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        $("#inputrecipename0").focus();
                    } else if (result == "Duplicate") {
                        $('#btnSubmit0').removeAttr("disabled");
                        toastr.error("Recipe Name already exits , kindly check");
                        $("#inputrecipename").focus();
                    }
                    else {
                        $(".k-overlay").hide();
                        $('#btnPublish0').removeAttr("disabled");
                        toastr.success("Recipe configuration published successfully. Remember to map this to relevent Dish");
                        $(".k-overlay").hide();
                        $("#emptybr0").css("display", "block");
                        $("#gridBaseRecipe0").css("display", "none");
                        $("#mainContent").show();
                        $("#windowEdit0").hide();
                        $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                        $("#gridRecipe").data("kendoGrid").dataSource.read();

                    }


                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }


    });

    $("#btnSubmitx").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie
        //if (!validateDataBeforeSave("gridBaseRecipex", 'gridMOGx')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipex").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= MajorPercentData) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode").val(),
                "Recipe_ID": $("#recipeid").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOGx").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecodex").val(),
                "Recipe_ID": $("#recipeidx").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOM_ID": data[i].UOM_ID,
                "UOMCode": data[i].UOMCode,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbasex").val() == "Yes")
            base = true
        else if ($("#inputbasex").val() == "No")
            base = false
        else {
            toastr.error("Select Recipe Type!");
            Utility.UnLoading();
            return false;
        }
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuomx").val();
        });

        var model = {
            "RecipeCode": $("#inputrecipecodex").val(),
            "ID": $("#recipeidx").val(),
            "Name": titleCase($("#inputrecipenamex").val()),
            "UOM_ID": $("#inputuomx").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantityx").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKGx").text(),
            "CostPerUOM": $("#grandCostPerKGx").text(),
            "TotalCost": $('#grandTotalx').text(),
            "Quantity": $('#inputquantityx').val(),
            "Instructions": $("#inputinstructionx").val(),
            "IsActive": 1
        }

        if ($("#inputrecipenamex").val() === "") {
            // $("#error").css("display", "flex");
            //    $("#error").find("p").text("Please provide valid input(s)");
            toastr.error("Please provide valid input(s)");
            $("#inputrecipenamex").focus();
            Utility.UnLoading();
        }
        else {
            // $("#error").css("display", "none");
            var base;
            if ($("#inputbasex").val() == "Yes")
                base = true
            else if ($("#inputbasex").val() == "No")
                base = false
            else {
                toastr.error("Select Recipe Type!");
                Utility.UnLoading();
                return false;
            }
            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnSubmitx").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnSubmitx').removeAttr("disabled");
                    $("#inputrecipenamex").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnSubmitx').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        Utility.UnLoading();
                        $("#inputrecipenamex").focus();
                    }
                    else {
                        $(".k-overlay").hide();
                        $('#btnSubmitx').removeAttr("disabled");
                        Toast.fire({
                            type: 'success',
                            title: 'Recipe data saved'
                        });
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnPublishx").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie
        //if (!validateDataBeforeSave("gridBaseRecipex", 'gridMOGx')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipex").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= MajorPercentData) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode").val(),
                "Recipe_ID": $("#recipeid").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOGx").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecodex").val(),
                "Recipe_ID": $("#recipeidx").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOM_ID": data[i].UOM_ID,
                "UOMCode": data[i].UOMCode,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbasex").val() == "Yes")
            base = true
        else if ($("#inputbasex").val() == "No")
            base = false

        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuomx").val();
        });

        var model = {
            "RecipeCode": $("#inputrecipecodex").val(),
            "ID": $("#recipeidx").val(),
            "Name": titleCase($("#inputrecipenamex").val()),
            "UOM_ID": $("#inputuomx").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantityx").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKGx").text(),
            "CostPerUOM": $("#grandCostPerKGx").text(),
            "TotalCost": $('#grandTotalx').text(),
            "Quantity": $('#inputquantityx').val(),
            "Instructions": $("#inputinstructionx").val(),
            "IsActive": 1
        }

        if ($("#inputrecipenamex").val() === "") {
            // $("#error").css("display", "flex");
            //    $("#error").find("p").text("Please provide valid input(s)");
            toastr.error("Please provide valid input(s)");
            $("#inputrecipenamex").focus();
            Utility.UnLoading();
        }
        else {
            // $("#error").css("display", "none");
            var base;
            if ($("#inputbasex").val() == "Yes")
                base = true
            else if ($("#inputbasex").val() == "No")
                base = false

            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnPublishx").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnPublishx').removeAttr("disabled");
                    $("#inputrecipenamex").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnPublishx').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        $("#inputrecipenamex").focus();
                        Utility.UnLoading();
                    }
                    else {
                        $(".k-overlay").hide();
                        $('#btnPublishx').removeAttr("disabled");
                        Toast.fire({
                            type: 'success',
                            title: 'Recipe data saved'
                        });
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnPublish1").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        //if (!validateDataBeforeSave("gridBaseRecipe1", 'gridMOG1')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe1").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode1").val(),
                "Recipe_ID": $("#recipeid1").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG1").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode1").val(),
                "Recipe_ID": $("#recipeid1").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase1").val() == "Yes")
            base = true
        else if ($("#inputbase1").val() == "No")
            base = false
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom1").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode1").val(),
            "ID": $("#recipeid1").val(),
            "Name": titleCase($("#inputrecipename1").val()),
            "UOM_ID": $("#inputuom1").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity1").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG1").text(),
            "CostPerUOM": $("#grandCostPerKG1").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#inputquantity1').val(),
            "Instructions": $("#inputinstruction1").val(),
            "IsActive": 1
        }

        if ($("#inputrecipename1").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename1").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase1").val() == "Yes")
                base = true
            else if ($("#inputbase1").val() == "No")
                base = false

            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnPublish1").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnPublish1').removeAttr("disabled");
                    $("#inputrecipename1").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnPublish1').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        $("#inputrecipename1").focus();
                        Utility.UnLoading();
                    }
                    else {
                        $(".k-overlay").hide();
                        var orderWindow = $("#windowEdit1").data("kendoWindow");
                        orderWindow.close();
                        $('#btnPublish1').removeAttr("disabled");

                        toastr.success("Changes have been saved. Click Publish to make the Recipe available for Dish mapping");
                        $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                        $("#gridRecipe").data("kendoGrid").dataSource.read();
                        $(".k-overlay").hide();

                        orderWindow.close();
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnSubmit1").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        //if (!validateDataBeforeSave("gridBaseRecipe1", 'gridMOG1')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe1").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode1").val(),
                "Recipe_ID": $("#recipeid1").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG1").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode1").val(),
                "Recipe_ID": $("#recipeid1").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase1").val() == "Yes")
            base = true
        else if ($("#inputbase1").val() == "No")
            base = false
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom1").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode1").val(),
            "ID": $("#recipeid1").val(),
            "Name": titleCase($("#inputrecipename1").val()),
            "UOM_ID": $("#inputuom1").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity1").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG1").text(),
            "CostPerUOM": $("#grandCostPerKG1").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#inputquantity1').val(),
            "Instructions": $("#inputinstruction1").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy
        }

        if ($("#inputrecipename1").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename1").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase1").val() == "Yes")
                base = true
            else if ($("#inputbase1").val() == "No")
                base = false

            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnSubmit1").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnSubmit1').removeAttr("disabled");
                    $("#inputrecipename1").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnSubmit1').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        $("#inputrecipename1").focus();
                        Utility.UnLoading();
                    }
                    else {
                        $(".k-overlay").hide();
                        //var orderWindow = $("#windowEdit1").data("kendoWindow");
                        //orderWindow.close();
                        $('#btnSubmit1').removeAttr("disabled");
                        Toast.fire({
                            type: 'success',
                            title: 'Recipe data saved'
                        });
                        $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                        $("#gridRecipe").data("kendoGrid").dataSource.read();
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnPublish2").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        //if (!validateDataBeforeSave("gridBaseRecipe2", 'gridMOG2')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe2").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode2").val(),
                "Recipe_ID": $("#recipeid2").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG2").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode2").val(),
                "Recipe_ID": $("#recipeid2").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase2").val() == "Yes")
            base = true
        else if ($("#inputbase2").val() == "No")
            base = false

        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom2").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode2").val(),
            "ID": $("#recipeid2").val(),
            "Name": titleCase($("#inputrecipename2").val()),
            "UOM_ID": $("#inputuom2").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity2").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG2").text(),
            "CostPerUOM": $("#grandCostPerKG2").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#inputquantity1').val(),
            "Instructions": $("#inputinstruction2").val(),
            "IsActive": 1
        }

        if ($("#inputrecipename2").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename2").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase2").val() == "Yes")
                base = true
            else if ($("#inputbase2").val() == "No")
                base = false

            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnPublish2").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnPublish2').removeAttr("disabled");
                    $("#inputrecipename2").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnPublish2').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        $("#inputrecipename2").focus();
                        Utility.UnLoading();
                    }
                    else {
                        $(".k-overlay").hide();
                        var orderWindow = $("#windowEdit2").data("kendoWindow");
                        orderWindow.close();
                        $('#btnPublish2').removeAttr("disabled");

                        toastr.success("Recipe configuration updated successfully");
                        $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                        $("#gridRecipe").data("kendoGrid").dataSource.read();
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
        $("#windowEdit1").parent('.k-widget').css("display", "block");
    });
    $("#btnSubmit2").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        //if (!validateDataBeforeSave("gridBaseRecipe2", 'gridMOG2')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe2").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode2").val(),
                "Recipe_ID": $("#recipeid2").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG2").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode2").val(),
                "Recipe_ID": $("#recipeid2").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase2").val() == "Yes")
            base = true
        else if ($("#inputbase2").val() == "No")
            base = false

        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom2").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode2").val(),
            "ID": $("#recipeid2").val(),
            "Name": titleCase($("#inputrecipename2").val()),
            "UOM_ID": $("#inputuom2").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity2").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG2").text(),
            "CostPerUOM": $("#grandCostPerKG2").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#inputquantity1').val(),
            "Instructions": $("#inputinstruction2").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy
        }

        if ($("#inputrecipename2").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename2").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase2").val() == "Yes")
                base = true
            else if ($("#inputbase2").val() == "No")
                base = false


            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnSubmit2").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnSubmit2').removeAttr("disabled");
                    $("#inputrecipename2").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnSubmit2').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        $("#inputrecipename2").focus();
                        Utility.UnLoading();
                    }
                    else {
                        $(".k-overlay").hide();
                        var orderWindow = $("#windowEdit2").data("kendoWindow");
                        orderWindow.close();
                        $('#btnSubmit2').removeAttr("disabled");
                        Toast.fire({
                            type: 'success',
                            title: 'Recipe data saved'
                        });
                        $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                        $("#gridRecipe").data("kendoGrid").dataSource.read();
                        setTimeout(fade_out, 10000);

                        function fade_out() {
                            $("#success").fadeOut();
                        }
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
        $("#windowEdit1").parent('.k-widget').css("display", "block");
    });
    $("#btnPublish3").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        //if (!validateDataBeforeSave("gridBaseRecipe3", 'gridMOG3')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe3").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode3").val(),
                "Recipe_ID": $("#recipeid3").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG3").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode3").val(),
                "Recipe_ID": $("#recipeid3").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase3").val() == "Yes")
            base = true
        else if ($("#inputbase3").val() == "No")
            base = false
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom3").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode3").val(),
            "ID": $("#recipeid3").val(),
            "Name": $("#inputrecipename3").val(),
            "UOM_ID": $("#inputuom3").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity3").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG3").text(),
            "CostPerUOM": $("#grandCostPerKG3").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#inputquantity1').val(),
            "Instructions": $("#inputinstruction3").val(),
            "IsActive": 1
        }

        if ($("#inputrecipename3").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename3").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase3").val() == "Yes")
                base = true
            else if ($("#inputbase3").val() == "No")
                base = false


            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnPublish3").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnPublish3').removeAttr("disabled");
                    $("#inputrecipename3").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnSubmit1').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        $("#inputrecipename3").focus();
                        Utility.UnLoading();
                    }
                    else {
                        $(".k-overlay").hide();

                        var orderWindow = $("#windowEdit3").data("kendoWindow");
                        orderWindow.close();
                        $('#btnPublish3').removeAttr("disabled");
                        $("#windowEdit2").parent('.k-widget').css("display", "block");
                        toastr.success('Recipe configuration updated successfully')
                        $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                        $("#gridRecipe").data("kendoGrid").dataSource.read();
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnSubmit3").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        //if (!validateDataBeforeSave("gridBaseRecipe3", 'gridMOG3')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe3").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode3").val(),
                "Recipe_ID": $("#recipeid3").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG3").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode3").val(),
                "Recipe_ID": $("#recipeid3").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1,

            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase3").val() == "Yes")
            base = true
        else if ($("#inputbase3").val() == "No")
            base = false
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom3").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode3").val(),
            "ID": $("#recipeid3").val(),
            "Name": titleCase($("#inputrecipename3").val()),
            "UOM_ID": $("#inputuom3").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity3").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG3").text(),
            "CostPerUOM": $("#grandCostPerKG3").text(),
            "TotalCost": $('#grandTotal3').text(),
            "Quantity": $('#inputquantity3').val(),
            "Instructions": $("#inputinstruction3").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy
        }

        if ($("#inputrecipename3").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename3").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase3").val() == "Yes")
                base = true
            else if ($("#inputbase3").val() == "No")
                base = false


            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnSubmit3").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData,
                function (result) {
                    if (result.xsssuccess !== undefined && !result.xsssuccess) {
                        toastr.error(result.message);
                        $('#btnSubmit3').removeAttr("disabled");
                        $("#inputrecipename3").focus();
                        Utility.UnLoading();
                    }
                    else {
                        if (result == false) {
                            $('#btnSubmit3').removeAttr("disabled");
                            toastr.error("Some error occured, please try again");
                            $("#inputrecipename3").focus();
                            Utility.UnLoading();
                        }
                        else {
                            $(".k-overlay").hide();

                            var orderWindow = $("#windowEdit3").data("kendoWindow");
                            orderWindow.close();
                            $('#btnSubmit2').removeAttr("disabled");
                            Toast.fire({
                                type: 'success',
                                title: 'Recipe data saved'
                            });
                            $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                            $("#gridRecipe").data("kendoGrid").dataSource.read();
                        }
                    }
                }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnSubmit4").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        //if (!validateDataBeforeSave("gridBaseRecipe4", 'gridMOG4')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe4").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode4").val(),
                "Recipe_ID": $("#recipeid4").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG4").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode4").val(),
                "Recipe_ID": $("#recipeid4").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase4").val() == "Yes")
            base = true
        else if ($("#inputbase4").val() == "No")
            base = false
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom4").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode4").val(),
            "ID": $("#recipeid4").val(),
            "Name": titleCase($("#inputrecipename4").val()),
            "UOM_ID": $("#inputuom4").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity4").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG4").text(),
            "CostPerUOM": $("#grandCostPerKG4").text(),
            "TotalCost": $('#grandTotal4').text(),
            "Quantity": $('#inputquantity4').val(),
            "Instructions": $("#inputinstruction4").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy
        }

        if ($("#inputrecipename4").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename4").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase4").val() == "Yes")
                base = true
            else if ($("#inputbase4").val() == "No")
                base = false


            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnSubmit4").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData,
                function (result) {
                    if (result.xsssuccess !== undefined && !result.xsssuccess) {
                        toastr.error(result.message);
                        $('#btnSubmit4').removeAttr("disabled");
                        $("#inputrecipename4").focus();
                        Utility.UnLoading();
                    }
                    else {
                        if (result == false) {
                            $('#btnSubmit4').removeAttr("disabled");
                            toastr.error("Some error occured, please try again");
                            $("#inputrecipename4").focus();
                            Utility.UnLoading();
                        }
                        else {
                            $(".k-overlay").hide();

                            //  var orderWindow = $("#windowEdit4").data("kendoWindow");
                            // orderWindow.close();
                            $('#btnSubmit4').removeAttr("disabled");
                            Toast.fire({
                                type: 'success',
                                title: 'Recipe data saved'
                            });
                            $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                            $("#gridRecipe").data("kendoGrid").dataSource.read();
                        }
                    }
                }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnPublish4").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        //if (!validateDataBeforeSave("gridBaseRecipe4", 'gridMOG4')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe4").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode4").val(),
                "Recipe_ID": $("#recipeid4").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG4").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode4").val(),
                "Recipe_ID": $("#recipeid4").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase4").val() == "Yes")
            base = true
        else if ($("#inputbase4").val() == "No")
            base = false
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom4").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode4").val(),
            "ID": $("#recipeid4").val(),
            "Name": titleCase($("#inputrecipename4").val()),
            "UOM_ID": $("#inputuom4").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity4").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG4").text(),
            "CostPerUOM": $("#grandCostPerKG4").text(),
            "TotalCost": $('#grandTotal4').text(),
            "Quantity": $('#inputquantity4').val(),
            "Instructions": $("#inputinstruction4").val(),
            "IsActive": 1
        }

        if ($("#inputrecipename4").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename4").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase4").val() == "Yes")
                base = true
            else if ($("#inputbase4").val() == "No")
                base = false


            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnPublish4").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData,
                function (result) {
                    if (result.xsssuccess !== undefined && !result.xsssuccess) {
                        toastr.error(result.message);
                        $('#btnPublish4').removeAttr("disabled");
                        $("#inputrecipename4").focus();
                        Utility.UnLoading();
                    }
                    else {
                        if (result == false) {
                            $('#btnPublish4').removeAttr("disabled");
                            toastr.error("Some error occured, please try again");
                            $("#inputrecipename4").focus();
                            Utility.UnLoading();
                        }
                        else {
                            $(".k-overlay").hide();

                            var orderWindow = $("#windowEdit4").data("kendoWindow");
                            orderWindow.close();
                            $('#btnPublish4').removeAttr("disabled");

                            toastr.success('Recipe configuration updated successfully')
                            $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                            $("#gridRecipe").data("kendoGrid").dataSource.read();
                        }
                    }
                }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
        $("#windowEdit3").parent('.k-widget').css("display", "block")
    });


    $("#btnSubmit5").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        //if (!validateDataBeforeSave("gridBaseRecipe5", 'gridMOG5')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe5").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode5").val(),
                "Recipe_ID": $("#recipeid5").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG5").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode4").val(),
                "Recipe_ID": $("#recipeid4").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase5").val() == "Yes")
            base = true
        else if ($("#inputbase5").val() == "No")
            base = false
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom5").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode5").val(),
            "ID": $("#recipeid5").val(),
            "Name": titleCase($("#inputrecipename5").val()),
            "UOM_ID": $("#inputuom5").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity5").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG5").text(),
            "CostPerUOM": $("#grandCostPerKG5").text(),
            "TotalCost": $('#grandTotal5').text(),
            "Quantity": $('#inputquantity5').val(),
            "Instructions": $("#inputinstruction5").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy
        }

        if ($("#inputrecipename5").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename5").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase5").val() == "Yes")
                base = true
            else if ($("#inputbase5").val() == "No")
                base = false


            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnSubmit5").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData,
                function (result) {
                    if (result.xsssuccess !== undefined && !result.xsssuccess) {
                        toastr.error(result.message);
                        $('#btnSubmit5').removeAttr("disabled");
                        $("#inputrecipename5").focus();
                        Utility.UnLoading();
                    }
                    else {
                        if (result == false) {
                            $('#btnSubmit5').removeAttr("disabled");
                            toastr.error("Some error occured, please try again");
                            $("#inputrecipename5").focus();
                            Utility.UnLoading();
                        }
                        else {
                            $(".k-overlay").hide();

                            //  var orderWindow = $("#windowEdit4").data("kendoWindow");
                            // orderWindow.close();
                            $('#btnSubmit5').removeAttr("disabled");
                            Toast.fire({
                                type: 'success',
                                title: 'Recipe data saved'
                            });
                            $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                            $("#gridRecipe").data("kendoGrid").dataSource.read();
                        }
                    }
                }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnPublish5").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        //if (!validateDataBeforeSave("gridBaseRecipe5", 'gridMOG5')) {
        //    toastr.error("Invalid Data!");
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe5").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode5").val(),
                "Recipe_ID": $("#recipeid5").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG5").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode4").val(),
                "Recipe_ID": $("#recipeid4").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase5").val() == "Yes")
            base = true
        else if ($("#inputbase5").val() == "No")
            base = false
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom5").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode5").val(),
            "ID": $("#recipeid5").val(),
            "Name": titleCase($("#inputrecipename5").val()),
            "UOM_ID": $("#inputuom5").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity5").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG5").text(),
            "CostPerUOM": $("#grandCostPerKG5").text(),
            "TotalCost": $('#grandTotal5').text(),
            "Quantity": $('#inputquantity5').val(),
            "Instructions": $("#inputinstruction5").val(),
            "IsActive": 1
        }

        if ($("#inputrecipename5").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename5").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase5").val() == "Yes")
                base = true
            else if ($("#inputbase5").val() == "No")
                base = false


            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#btnPublish5").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData,
                function (result) {
                    if (result.xsssuccess !== undefined && !result.xsssuccess) {
                        toastr.error(result.message);
                        $('#btnPublish5').removeAttr("disabled");
                        $("#inputrecipename5").focus();
                        Utility.UnLoading();
                    }
                    else {
                        if (result == false) {
                            $('#btnPublish5').removeAttr("disabled");
                            toastr.error("Some error occured, please try again");
                            $("#inputrecipename5").focus();
                            Utility.UnLoading();
                        }
                        else {
                            $(".k-overlay").hide();

                            //  var orderWindow = $("#windowEdit4").data("kendoWindow");
                            // orderWindow.close();
                            $('#btnPublish5').removeAttr("disabled");

                            toastr.success('Recipe configuration updated successfully');
                            $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                            $("#gridRecipe").data("kendoGrid").dataSource.read();
                        }
                    }
                }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
        $("#windowEdit4").parent('.k-widget').css("display", "block")
    });
    $("#btnSubmit").click(function () {
        isPublishBtnClick = false;
        // saveRecipeOnBtnSubmit();
        getRecipeDependenciesData();
        checkIsBRNameChange();
        showRecipeImpactedMessage();
        if (isBRNameChange) {
            $("#btnAgree").css('display', 'block');
            $("#btnDoLater").text("No Shall do it later");
            $("#btnDoLater").removeClass("btn-info");
            $("#btnDoLater").addClass("btn-tool");
        }
        else {
            $("#btnAgree").css('display', 'none');
            $("#btnDoLater").text("Ok");
            $("#btnDoLater").addClass("btn-info");
            $("#btnDoLater").removeClass("btn-tool");
            checkIsBRConfigurationChange();
        }

        if (recipeImpactedData.IsMOGImpacted && (isBRConfigurationChange || isBRNameChange)) {
            showRecipeDependenciesPopUp();
        }
        else {
            saveRecipeOnBtnSubmit();
        }
    });


    $("#btnPublish").click(function () {
        Utility.Loading();
        setTimeout(function () {
            isPublishBtnClick = true;
            // saveReceipeOnPublishBtn();
            getRecipeDependenciesData();
            checkIsBRNameChange();
            showRecipeImpactedMessage();


            if (isBRNameChange) {
                $("#btnAgree").css('display', 'block');
                $("#btnDoLater").text("No Shall do it later");
                $("#btnDoLater").removeClass("btn-info");
                $("#btnDoLater").addClass("btn-tool");
            }
            else {
                $("#btnAgree").css('display', 'none');
                $("#btnDoLater").text("Ok");
                $("#btnDoLater").addClass("btn-info");
                $("#btnDoLater").removeClass("btn-tool");
                checkIsBRConfigurationChange();
            }

            if (recipeImpactedData.IsMOGImpacted && (isBRConfigurationChange || isBRNameChange)) {
                showRecipeDependenciesPopUp();
            }
            else {
                saveReceipeOnPublishBtn();
            }
        }
            , 1000);
    });



    $("#btnDoLater").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditRecipeDependencies").data("kendoWindow");
        orderWindow.close();
        if (isStatusSave) {
            $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsActive);
        }
    });

    $("#btnAgree").on("click", function () {
        if (isPublishBtnClick) { saveReceipeOnPublishBtn(); }
        else { saveRecipeOnBtnSubmit(); }
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditRecipeDependencies").data("kendoWindow");
        orderWindow.close();
    });


});

function saveReceipeOnPublishBtn() {
    //Model Creation
    //insertion or updation in  two tables
    //Base recipie
    Utility.Loading();
    //if (!validateDataBeforeSave("gridBaseRecipe", 'gridMOG')) {
    //    toastr.error("Invalid Data!");
    //    Utility.UnLoading();
    //    return false;
    //}

    var rName = $("#inputrecipename").val();
    var rAlias = $("#inputrecipealiasname").val();
    if (rName === rAlias) {
        Utility.UnLoading();
        toastr.error("Recipe Name and Recipe Alias should be different");
        return;
    }

    if (user.SectorNumber == "20") {

        if ($("#inputrecipecum").val() == null || $("#inputrecipecum").val() == "" || $("#inputrecipecum").val() == undefined || $("#inputrecipecum").val() == "Select") {
            toastr.error("Please select Recipe UOM Category");
            Utility.UnLoading();
            return false;
        }
    }

    if ($("#inputuom").val() == null || $("#inputuom").val() == "" || $("#inputuom").val() == undefined) {
        toastr.error("Please select UOM");
        Utility.UnLoading();
        return false;
    }

    if ($("#inputquantity").val() == null || $("#inputquantity").val() == "" || $("#inputquantity").val() == undefined || $("#inputquantity").val() == 0 || $("#inputquantity").val() == "Select") {
        toastr.error("Please enter Recipe Quantity");
        Utility.UnLoading();
        return false;
    }


    var brgrid = $("#gridBaseRecipe").data("kendoGrid").dataSource;
    var data = brgrid._data;
    var baseRecipes = [];

    for (var i = 0; i < brgrid._data.length; i++) {
        var iMajor = false;
        if (data[i].IngredientPerc >= MajorPercentData) {
            iMajor = true;
        }
        var obj = {
            "RecipeCode": $("#inputrecipecode").val(),
            "Recipe_ID": $("#recipeid").val(),
            "BaseRecipe_ID": data[i].BaseRecipe_ID,
            "BaseRecipeCode": data[i].BaseRecipeCode,
            "UOMCode": data[i].UOMCode,
            "UOM_ID": data[i].UOM_ID,
            "CostPerUOM": data[i].CostPerUOM,
            "CostPerKG": data[i].CostPerKG,
            "TotalCost": data[i].TotalCost,
            "Quantity": data[i].Quantity,
            "IngredientPerc": data[i].IngredientPerc,
            "IsMajor": iMajor,
            "IsActive": 1,
            "CreatedOn": data[i].BaseRecipe_ID == 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].CreatedOn),
            "CreatedBy": data[i].BaseRecipe_ID == 0 ? user.UserId : data[i].CreatedBy,
            "ModifiedOn": data[i].BaseRecipe_ID > 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].ModifiedOn),
            "ModifiedBy": data[i].BaseRecipe_ID > 0 ? user.UserId : data[i].ModifiedBy,
        }
        baseRecipes.push(obj);
    }
    //MOG
    var mogGrid = $("#gridMOG").data("kendoGrid").dataSource;
    data = mogGrid._data;
    var mogs = [];
    for (var i = 0; i < mogGrid._data.length; i++) {
        var iMajor = false;
        if (data[i].IngredientPerc >= 5) {
            iMajor = true;
        }
        var obj = {
            "RecipeCode": $("#inputrecipecode").val(),
            "Recipe_ID": $("#recipeid").val(),
            "MOG_ID": data[i].MOG_ID,
            "MOGCode": data[i].MOGCode,
            "UOMCode": data[i].UOMCode,
            "UOM_ID": data[i].UOM_ID,
            "CostPerUOM": data[i].CostPerUOM,
            "CostPerKG": data[i].CostPerKG,
            "TotalCost": data[i].TotalCost,
            "DKgValue": data[i].DKgTotal,
            "Quantity": data[i].Quantity,
            "IngredientPerc": data[i].IngredientPerc,
            "IsMajor": iMajor,
            "IsActive": 1
        }
        mogs.push(obj);
    }

    //Top Level Actual Recipie
    var base;
    if ($("#inputbase").val() == "Yes")
        base = true
    else if ($("#inputbase").val() == "No")
        base = false
    var objUOM = uomdata.filter(function (uomObj) {
        return uomObj.value == $("#inputuom").val();
    });

    var model = {
        "RecipeCode": $("#inputrecipecode").val(),
        "ID": $("#recipeid").val(),
        "Name": $("#inputrecipename").val(),
        "RecipeAlias": $("#inputrecipealiasname").val(),
        "OldRecipeName": RecipeName,
        "UOM_ID": $("#inputuom").val(),
        "UOMCode": objUOM[0].UOMCode,
        "Quantity": $("#inputquantity").val(),
        "IsBase": base,
        "CostPerKG": $("#grandCostPerKG").text().substring(1).replace(",", ""),
        "CostPerUOM": $("#grandCostPerKG").text().substring(1).replace(",", ""),
        "TotalCost": $('#grandTotal').text().substring(1).replace(",", ""),
        "Quantity": $('#inputquantity').val(),
        "Instructions": $("#inputinstruction").val(),
        "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum").val() : "",
        "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit").val() : "",
        "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom").val() : "",
        "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit").val() : "",
        "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom").val() : "",
        "SubSectorCode": user.SectorNumber == "10" ? $("#inputsubsector").val() : "",
        "TotalIngredientWeight": $("#ToTIngredients").text(),
        "IsActive": 1,
        "IsRecipeNameChange": isBRNameChange,
        "IsRecipeStatusChange": isStatusSave,
        "Status": isPublishBtnClick ? 3 : 2

    }

    if ($("#inputrecipename").val() === "") {
        $("#error").css("display", "flex");
        $("#error").find("p").text("Please provide valid input(s)");
        $("#inputrecipename").focus();
        Utility.UnLoading();
    }
    else {
        $("#error").css("display", "none");
        var base;
        if ($("#inputbase").val() == "Yes")
            base = true
        else if ($("#inputbase").val() == "No")
            base = false
        //console.log("model");
        //console.log(model);
        //if (!sanitizeAndSend(model)) {
        //    return;
        //}
        $("#btnPublish").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.SaveRecipeData,
            function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnPublish').removeAttr("disabled");
                    $("#inputrecipename").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnPublish').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        $("#inputrecipename").focus();
                        Utility.UnLoading();
                    }
                    else {
                        $(".k-overlay").hide();
                        $('#btnPublish').removeAttr("disabled");
                        toastr.success("Recipe configuration published successfully. Remember to map this to relevent Dish");
                        $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                        $("#gridRecipe").data("kendoGrid").dataSource.read();
                        datamodel = model;
                        if (recipeImpactedData.IsMOGImpacted) {
                            sendRecipeEmail();
                        }
                        $("#mainContent").show();

                        $("#windowEdit").hide();
                    }
                }
            }, {
            model: model,
            baseRecipes: baseRecipes,
            mogs: mogs

        }, true);
    }
    console.log(model);
}

function sendRecipeEmail() {
    Utility.Loading();
    HttpClient.MakeRequest(CookBookMasters.SendRecipeEmail, function (result) {
        if (result == false) {
            // toastr.error("Some error occured, please try again");
        }
        else {
        }
    }, {
        model: datamodel
    }, true);
}

function SaveRecipeElmentoryStatus() {
    datamodel.IsRecipeStatusChange = true;
    datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
    HttpClient.MakeSyncRequest(CookBookMasters.SaveRecipeData, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again");
        }
        else {
            toastr.success("Recipe status updated successfully");
            $("#gridRecipe").data("kendoGrid").dataSource.data([]);
            $("#gridRecipe").data("kendoGrid").dataSource.read();
            if (recipeImpactedData.IsMOGImpacted) {
                //   sendRecipeEmail();
            }
        }
    }, {
        model: datamodel
    }, true);
}

function populateBulkChangeControls() {

    var gridVariable = $("#gridBulkChange");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: false,
        filterable: false,
        //reorderable: true,
        //scrollable: true,
        columns: [
            {
                field: "RecipeCode", title: "Recipe Code", width: "40px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Recipe Name", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOMName", title: "UOM", width: "60px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },

            },
            {
                field: "IsBase", title: "Base Recipe", width: "80px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "", title: "", width: "50px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "", title: "", width: "30px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },

            {
                field: "", title: "", width: "20px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            }
        ],
        columnResize: function (e) {

            e.preventDefault();
        },
        dataBound: function (e) {
        },
        change: function (e) {
        },
    });
}

function SaveRecipeStatus() {
    datamodel.IsRecipeStatusChange = true;
    datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
    HttpClient.MakeSyncRequest(CookBookMasters.SaveRecipeData, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again");
        }
        else {
            toastr.success("Recipe status updated successfully");
            $("#gridRecipe").data("kendoGrid").dataSource.data([]);
            $("#gridRecipe").data("kendoGrid").dataSource.read();
            if (recipeImpactedData.IsMOGImpacted) {
                sendRecipeEmail();
            }
        }
    }, {
        model: datamodel
    }, true);
}

function showRecipeImpactedMessage() {
    if (isStatusSave) {
        $("#recipeNameChangeMsgDiv").css('display', 'none');
        $("#recipeConfigChangeMsgDiv").css('display', 'none');
        $("#recipeInactiveMsgDiv").css('display', 'block');
    }
    else if (isBRNameChange) {
        $("#recipeNameChangeMsgDiv").css('display', 'block');
        $("#recipeConfigChangeMsgDiv").css('display', 'none');
        $("#recipeInactiveMsgDiv").css('display', 'none');
    }
    else {
        $("#recipeNameChangeMsgDiv").css('display', 'none');
        $("#recipeInactiveMsgDiv").css('display', 'none');
        $("#recipeConfigChangeMsgDiv").css('display', 'block');
    }
}

function showRecipeDependenciesPopUp() {
    var dialog = $("#windowEditRecipeDependencies").data("kendoWindow");

    dialog.title("Base Recipe - " + RecipeName + " Dependencies");
    dialog.open();
    dialog.center();
    populateRecipeDependencies();
}

$("#inputrecipealiasname").focusout(function () {
    var rName = $("#inputrecipename").val();
    var rAlias = $("#inputrecipealiasname").val();
    if (rName === rAlias) {
        Utility.UnLoading();
        toastr.error("Recipe Name and Recipe Alias should be different");
        return;
    }
});

$('#inputWeightPerPc0').change(function () {
    var wtperPc = $("#inputWeightPerPc0").val();
    if (wtperPc !== undefined && wtperPc !== null && wtperPc !== "" && wtperPc > 0) {
        var noofPcs = 0;
        noofPcs = 10 * 1000 / wtperPc;
        $("#lblNumberofPc0").text(noofPcs);
    };
});

$('#inputWeightPerPc').change(function () {
    var wtperPc = $("#inputWeightPerPc").val();
    if (wtperPc !== undefined && wtperPc !== null && wtperPc !== "" && wtperPc > 0) {
        var noofPcs = 0;
        noofPcs = 10 * 1000 / wtperPc;
        $("#lblNumberofPc").text(noofPcs);
    };
});

function saveRecipeOnBtnSubmit() {
    //Model Creation
    //insertion or updation in  two tables
    //Base recipie
    //if (!validateDataBeforeSave("gridBaseRecipe", 'gridMOG')) {
    //    toastr.error("Invalid Data!");
    //    Utility.UnLoading();
    //    return false;
    //}

    var rName = $("#inputrecipename").val();
    var rAlias = $("#inputrecipealiasname").val();
    if (rName === rAlias) {
        Utility.UnLoading();
        toastr.error("Recipe Name and Recipe Alias should be different");
        return;
    }
    if ($("#inputquantity").val() == null || $("#inputquantity").val() == "" || $("#inputquantity").val() == undefined || $("#inputquantity").val() == 0) {
        toastr.error("Please enter Recipe Quantity");
        Utility.UnLoading();
        return false;
    }

    if (user.SectorNumber == "20") {

        if ($("#inputrecipecum").val() == null || $("#inputrecipecum").val() == "" || $("#inputrecipecum").val() == undefined || $("#inputrecipecum").val() == "Select") {
            toastr.error("Please select Recipe UOM Category");
            Utility.UnLoading();
            return false;
        }
    }

    if ($("#inputuom").val() == null || $("#inputuom").val() == "" || $("#inputuom").val() == undefined || $("#inputuom").val() == "Select") {
        toastr.error("Please select UOM");
        Utility.UnLoading();
        return false;
    }

    var brgrid = $("#gridBaseRecipe").data("kendoGrid").dataSource;
    var data = brgrid._data;
    var baseRecipes = [];

    for (var i = 0; i < brgrid._data.length; i++) {
        var iMajor = false;
        if (data[i].IngredientPerc >= MajorPercentData) {
            iMajor = true;
        }
        var obj = {
            "RecipeCode": $("#inputrecipecode").val(),
            "Recipe_ID": $("#recipeid").val(),
            "BaseRecipe_ID": data[i].BaseRecipe_ID,
            "BaseRecipeCode": data[i].BaseRecipeCode,
            "UOMCode": data[i].UOMCode,
            "UOM_ID": data[i].UOM_ID,
            "CostPerUOM": data[i].CostPerUOM,
            "CostPerKG": data[i].CostPerKG,
            "TotalCost": data[i].TotalCost,
            "Quantity": data[i].Quantity,
            "IngredientPerc": data[i].IngredientPerc,
            "IsMajor": iMajor,
            "IsActive": 1,
            "CreatedOn": data[i].BaseRecipe_ID == 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].CreatedOn),
            "CreatedBy": data[i].BaseRecipe_ID == 0 ? user.UserId : data[i].CreatedBy,
            "ModifiedOn": data[i].BaseRecipe_ID > 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].ModifiedOn),
            "ModifiedBy": data[i].BaseRecipe_ID > 0 ? user.UserId : data[i].ModifiedBy,

        }
        baseRecipes.push(obj);
    }
    //MOG
    var mogGrid = $("#gridMOG").data("kendoGrid").dataSource;
    data = mogGrid._data;
    var mogs = [];
    for (var i = 0; i < mogGrid._data.length; i++) {
        var iMajor = false;
        if (data[i].IngredientPerc >= MajorPercentData) {
            iMajor = true;
        }
        var obj = {
            "RecipeCode": $("#inputrecipecode").val(),
            "Recipe_ID": $("#recipeid").val(),
            "MOG_ID": data[i].MOG_ID,
            "MOGCode": data[i].MOGCode,
            "UOMCode": data[i].UOMCode,
            "UOM_ID": data[i].UOM_ID,
            "CostPerUOM": data[i].CostPerUOM,
            "CostPerKG": data[i].CostPerKG,
            "TotalCost": data[i].TotalCost,
            "DKgValue": data[i].DKgTotal,
            "Quantity": data[i].Quantity,
            "IngredientPerc": data[i].IngredientPerc,
            "IsMajor": iMajor,
            "IsActive": 1,
            "CreatedOn": data[i].MOG_ID == 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].CreatedOn),
            "CreatedBy": data[i].MOG_ID == 0 ? user.UserId : data[i].CreatedBy,
            "ModifiedOn": data[i].MOG_ID > 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].ModifiedOn),
            "ModifiedBy": data[i].MOG_ID > 0 ? user.UserId : data[i].ModifiedBy
        }
        mogs.push(obj);
    }

    //Top Level Actual Recipie
    var base;
    if ($("#inputbase").val() == "Yes")
        base = true
    else if ($("#inputbase").val() == "No")
        base = false
    var objUOM = uomdata.filter(function (uomObj) {
        return uomObj.value == $("#inputuom").val();
    });

    var dkgPerUOM = 0;
    if ($("#ToTIngredients").text() != undefined && $("#ToTIngredients").text() != "") {
        var totalDKG = parseFloat($("#ToTIngredients").text());
        dkgPerUOM = totalDKG / $("#inputquantity").val();
    }

    var model = {
        "RecipeCode": $("#inputrecipecode").val(),
        "ID": $("#recipeid").val(),
        "Name": $("#inputrecipename").val(),
        "RecipeAlias": $("#inputrecipealiasname").val(),
        "OldRecipeName": RecipeName,
        "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum").val() : "",
        "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit").val() : "",
        "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom").val() : "",
        "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit").val() : "",
        "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom").val() : "",
        "SubSectorCode": user.SectorNumber == "10" ? $("#inputsubsector").val() : "",
        "TotalIngredientWeight": $("#ToTIngredients").text(),
        "UOM_ID": $("#inputuom").val(),
        "UOMCode": objUOM[0].UOMCode,
        "Quantity": $("#inputquantity").val(),
        "IsBase": base,
        "CostPerKG": $("#grandCostPerKG").text().substring(1).replace(",", ""),
        "CostPerUOM": $("#grandCostPerKG").text().substring(1).replace(",", ""),
        "TotalCost": $('#grandTotal').text().substring(1).replace(",", ""),
        "Quantity": $('#inputquantity').val(),
        "Instructions": $("#inputinstruction").val(),
        "DKgPerUOM": dkgPerUOM,
        "IsActive": 1,
        "IsRecipeNameChange": isBRNameChange,
        "IsRecipeStatusChange": isStatusSave,
        "Status": isPublishBtnClick ? 3 : 2,
        "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
        "CreatedBy": datamodel.CreatedBy
    }

    if ($("#inputrecipename").val() === "") {
        toastr.error("Please provide Recipe Name");
        $("#inputrecipename").focus();
        return;
    }
    else {
        var base;
        if ($("#inputbase").val() == "Yes")
            base = true
        else if ($("#inputbase").val() == "No")
            base = false

        //if (!sanitizeAndSend(model)) {
        //    return;
        //}
        $("#btnSubmit").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.SaveRecipeData, function (result) {
            //alert("Result" + result);
            if (result.xsssuccess !== undefined && !result.xsssuccess) {
                toastr.error(result.message);
                $('#btnSubmit').removeAttr("disabled");
                $("#inputrecipename").focus();
                Utility.UnLoading();
            }
            else {
                if (result == false) {
                    $('#btnSubmit').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputrecipename").focus();
                }
                else if (result == "Duplicate") {
                    $('#btnSubmit').removeAttr("disabled");
                    toastr.error("Recipe Name already exits , kindly check");
                    $("#inputrecipename").focus();
                }
                else {
                    $(".k-overlay").hide();
                    $('#btnSubmit').removeAttr("disabled");


                    Toast.fire({
                        type: 'success',
                        title: 'Recipe data saved'
                    });
                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();
                    datamodel = model;
                    if (recipeImpactedData.IsMOGImpacted) {
                        sendRecipeEmail();
                    }
                }
            }
        }, {
            model: model,
            baseRecipes: baseRecipes,
            mogs: mogs

        }, true);
    }
}
function populateRecipeCopyDropDown() {
    $("#copyRecipe").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "ID",
        dataSource: copyData,
        index: 0,
    });
    var dropdownlist = $("#copyRecipe").data("kendoDropDownList");
    dropdownlist.bind("change", dropdownlist_selectMain);
}
function dropdownlist_selectMain(eid) {
    var ID = eid.sender.dataItem(eid.item).ID;
    
    if (ID == 0) {
        return;
    }
    $("#inputinstruction0").val("");
    $("#recipeid0").text("");
    $("#inputrecipename0").val("");
    $("#inputrecipealiasname0").val("");
    $("#grandTotal0").text("");
    $("#grandCostPerKG0").text("");
    $("#ToTIngredients0").text("");
    var tr = eid.sender.dataItem(eid.item);
    if (user.SectorNumber == "20") {
        $('#inputquantity0').removeAttr("disabled");
        $("#inputquantity0").val("");
        $("#inputwtperunit0").val("");
        $("#inputgravywtperunit0").val("");
        $("#inputuom0").data("kendoDropDownList").value("Select");
        $("#inputrecipecum0").data("kendoDropDownList").value("Select");
        $("#inputwtperunituom0").data("kendoDropDownList").value("Select");
        $("#inputgravywtperunituom0").data("kendoDropDownList").value("Select");
        $("#inputbase0").data('kendoDropDownList').value("Yes");
        var recipeCategory = "";
        var rCatData = $("#inputrecipecum0").data('kendoDropDownList')
        if (rCatData != null && rCatData != "Select" && rCatData != "" && rCatData.dataItem() != null && rCatData.dataItem().UOMCode != "Select") {
            recipeCategory = rCatData.dataItem().UOMCode
        }
        OnRecipeUOMCategoryChange(recipeCategory, "0");
    }
    else if (user.SectorNumber == "10") {
        var inputwtperunituom0 = $("#inputuom0").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        if (tr.IsBase == true)
            $("#inputbase0").data('kendoDropDownList').value("Yes");
        else
            $("#inputbase0").data('kendoDropDownList').value("No");
        $("#inputsubsector0").data('kendoDropDownList').value("Select Sub Sector");
        if (tr.SubSectorCode)
            $("#inputsubsector0").data('kendoDropDownList').value(tr.SubSectorCode);
    }
    else {
        var inputwtperunituom0 = $("#inputuom0").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        if (tr.IsBase == true)
            $("#inputbase0").data('kendoDropDownList').value("Yes");
        else
            $("#inputbase0").data('kendoDropDownList').value("No");
    }
   
   // RecipeName = tr.Name;
    recipeCategoryHTMLPopulateOnEditRecipe("0", tr);
    isBaseRecipe = tr.IsBase;
    //RecipeCode = tr.RecipeCode;
   // $("#hiddenRecipeID0").val(tr.RecipeCode);
   // $("#recipeid0").val(tr.RecipeCode);
   // $("#inputrecipecode0").val(tr.RecipeCode);
   // $("#inputrecipename0").val(tr.Name);
   // $("#inputrecipealiasname0").val(tr.RecipeAlias);
    $("#inputuom0").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity0").val(tr.Quantity);
    if (user.SectorNumber == "20") {
        $("#totmog0").show();
        $(".totmoglbl").show();
        $('#inputquantity0').removeAttr("disabled");
        $("#ToTIngredientslbl0").text("Total Qty of Ingredients");
        if (user.SectorNumber == "20") {
            $("#totmog0").hide();
            $(".totmoglbl").hide();
            $("#ToTIngredientslbl0").text("Total Qty of Ingredients (DKG) : ");
        }
    }
    else {
        $("#ToTIngredientslbl0").text("Total Qty of Ingredients");
        $("#totmog0").show();
        $(".totmoglbl").hide();
        var inputwtperunituom = $("#inputuom0").data("kendoDropDownList");
        inputwtperunituom.enable(false);
        $('#inputquantity0').attr('disabled', 'disabled');
    }
   
    $('#grandTotal0').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG0').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    var editor = $("#inputinstruction0").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    populateBaseRecipeGridCopy(tr.RecipeCode);
    populateMOGGridCopy(tr.RecipeCode);
}
function populateRecipeGrid() {

    Utility.Loading();
    var gridVariable = $("#gridRecipe");//Hare Krishna!!
    gridVariable.html("");
    if (user.SectorNumber == "80") {
        gridVariable.kendoGrid({
            excel: {
                fileName: "Recipe.xlsx",
                filterable: true,
                allPages: true
            },
            //selectable: "cell",
            sortable: true,
            noRecords: {
                template: "No Records Available"
            },
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            //pageable: true,
            pageable: {
                pageSize: 150,
                messages: {
                    display: "{0}-{1} of {2} records"
                }
            },
            //reorderable: true,
            scrollable: true,
            columns: [
                {
                    field: "RecipeCode", title: "Code", width: "25px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Recipe Name", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "RecipeAlias", title: "Recipe Alias", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "20px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "Quantity", title: "Qty", width: "20px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "RecipeType", title: "Type", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    template: '# if (IsBase == true) {#<div>Base Recipe</div>#} else {#<div>Final Recipe</div>#}#',
                },
                {
                    field: "Impact", title: "Impacts", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    template: '# if (IsBase == true) { #<div>#:Impact# Recipe(s)</div>#} else {#<div>#:Impact# Dish</div>#}#',
                },

                {
                    field: "PublishedDateAsString", title: "Last Updated", width: "35px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    // template: '# if (PublishedDate == null) {#<span>-</span>#} else{#<span>#: kendo.toString(PublishedDate, "dd-MMM-yy")#</span>#}#',
                    headerAttributes: {
                        style: "text-align: center;"
                    }

                },
                {
                    field: "Status", title: "Status", width: "30px", attributes: {
                        class: "mycustomstatus",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Published#}#',
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },
                {
                    field: "Edit", title: "Configure", width: "30px",
                    attributes: {
                        style: "text-align: left; font-weight:normal;cursor: pointer;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            template: function (dataItem) {
                                var html = `<div title="Edit" onClick="recipeGridEdit(this)"  style='color:#8f9090; margin-left:20px' class='fa fa-pen EditRecipe'></div>`;
                                return html;
                            },

                        },

                        {
                            name: 'Print',
                            template: function (dataItem) {

                                var html = '<div title="Print" onClick="recipeGridPrint(this)" style="color:#8f9090; margin-left:8px" class="fa fa-print PrintRecipe"></div>';
                                return html;
                            },

                        },
                        {
                            name: 'Info',
                            template: function (dataItem) {
                                var html = '<div title="View" onClick="recipeGridInfo(this)"  style="color:#8f9090; margin-left:8px" class="fa fa-eye InfoRecipe"></div>';
                                return html;
                            },

                        },
                        //{
                        //    name: 'Map Nutrients',
                        //    template: function (dataItem) {
                        //        var html = `  <div title="Map Nutrients" onClick="mapNutrients(this)"  style='color:#8f9090; margin-left:20px' class='fa fa-nutrition MapNutrients'></div>`;
                        //        return html;
                        //    }
                        //}
                    ],
                }
            ],
            dataSource: {
                sort: { field: "IsBase", dir: "asc" },
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        //Krish 03-08-2022
                        //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (result) {
                        //    Utility.UnLoading();
                        //    if (result != null) {
                        //        copyData = result;
                        //        if (isShowElmentoryRecipe) {
                        //            result = result.filter(m => m.IsElementory);
                        //        }
                        //        options.success(result);

                        //        copyData.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
                        //        populateRecipeCopyDropDown();

                        //    }
                        //    else {
                        //        options.success("");
                        //    }
                        //},
                        //    {
                        //        condition: "All",
                        //        DishCode: "0"
                        //    }
                        //    , true);
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSectorRecipeDataList, function (data) {
                            //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (result) {
                            Utility.UnLoading();
                            var result = data.RecipeData;
                            if (result != null) {
                                result = result.filter(m => m.RecipeType == "Base Recipe");
                                copyData = result;
                                if (isShowElmentoryRecipe) {
                                    result = result.filter(m => m.IsElementory);
                                }
                                options.success(result);

                                copyData.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
                                populateRecipeCopyDropDown();

                            }
                            else {
                                options.success("");
                            }
                            var result1 = data.ApplicationSettingData;
                            if (result1 != null) {
                                applicationSettingData = result1;
                                for (item of applicationSettingData) {
                                    if (item.Code == "FLX-00004")//item.Name == "Major Identification Threshold" || 
                                        MajorPercentData = item.Value;
                                    $(".asPercent").text(MajorPercentData);
                                }
                            }
                            //}, { condition: "All", DishCode: "0" }, true);
                        }, { onLoad: true }, true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            RecipeCode: { type: "string", editable: false },
                            Name: { type: "string", editable: false },
                            RecipeAlias: { type: "string", editable: false },
                            UOMName: { type: "string", editable: false },
                            IsBase: { type: "boolean", editable: false },
                            Quantity: { type: "int", editable: false },
                            IsActive: { type: "boolean", editable: false },

                            Status: { type: "string", editable: false },
                            RecipeType: { type: "string", editable: false },
                            PublishedDate: { type: "date" },
                            PublishedDateAsString: { type: "string", editable: false },
                            IsElementoryString: { type: "string", editable: false },
                            IsElementory: { type: "boolean", editable: false },

                        }
                    }
                },
                // pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            // height: 500,
            dataBound: function (e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }
                    else {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    }

                }

                var items = e.sender.items();
                var grid = this;

                //grid.tbody.find("tr[role='row']").each(function () {
                //    var model = grid.dataItem(this);

                //    if (!model.IsElementory) {
                //        $(this).find(".MapNutrients").addClass("k-state-disabled");
                //    }
                //});

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".EditRecipe").addClass("k-state-disabled");
                    }
                });


                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".EditRecipe").addClass("k-state-disabled");
                    }

                    //Krish
                    for (item of applicationSettingData) {
                        var code = "FLX-00009";
                        if (user.SectorNumber == "80")  //Healthcare
                            code = "FLX-00009";
                        else if (user.SectorNumber == "30")  //Core
                            code = "FLX-00009";
                        else if (user.SectorNumber == "20")  //Manufac
                            code = "FLX-00009";

                        if (item.Code == code && item.IsActive == true && item.Value == "0")//item.Name == "Print Recipe Button Display Sector"
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == code && item.IsActive != true)
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == code && item.IsActive == true && item.Value == "1")
                            $(this).find(".PrintRecipe").show();
                        //Krish 20-07-2022
                        code = "FLX-00012";
                        if (user.SectorNumber == "80")  //Healthcare
                            code = "FLX-00012";
                        else if (user.SectorNumber == "30")  //Core
                            code = "FLX-00016";
                        else if (user.SectorNumber == "20")  //Manufac
                            code = "FLX-00016";
                        if (item.Code == code && item.IsActive == true && item.Value == "0")//item.Name == "Info Recipe Button Display Sector"
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == code && item.IsActive != true)
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == code && item.IsActive == true && item.Value == "1")
                            $(this).find(".InfoRecipe").show();
                    }
                });


                //items.each(function (e) {

                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //    }
                //});


                $(".chkbox").on("change", function (e) {
                    // 
                    isStatusSave = true;
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridRecipe").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    RecipeName = tr.Name;
                    RecipeCode = tr.RecipeCode;
                    Recipe_ID = tr.ID;
                    $("#hiddenRecipeID").val(tr.ID);
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }
                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Recipe " + active + "?", "Recipe Update Confirmation", "Yes", "No", function () {
                        //  SaveRecipeStatus();
                        getRecipeDependenciesData();
                        if (recipeImpactedData.IsMOGImpacted) {
                            $(".recipeDepStatus").text(datamodel.IsActive ? "active" : "inactive");
                            showRecipeImpactedMessage();
                            showRecipeDependenciesPopUp();
                        }
                        else {
                            SaveRecipeStatus();
                        }
                    }, function () {
                        $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsActive);
                    });
                    return true;
                });


                $(".chkboxElementory").on("change", function (e) {
                    // 
                    // isStatusSave = true;
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridRecipe").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    RecipeName = tr.Name;
                    RecipeCode = tr.RecipeCode;
                    Recipe_ID = tr.ID;
                    $("#hiddenRecipeID").val(tr.ID);
                    datamodel.IsElementory = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsElementory) {
                        active = "Elementary";
                    } else {
                        active = "Non Elementary";
                    }
                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Recipe " + active + "?", "Recipe Update Confirmation", "Yes", "No", function () {
                        SaveRecipeElmentoryStatus();
                    }, function () {
                        $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsElementory);
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });

                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }
        })
    }
    else {
        gridVariable.kendoGrid({
            excel: {
                fileName: "Recipe.xlsx",
                filterable: true,
                allPages: true
            },
            //selectable: "cell",
            sortable: true,
            noRecords: {
                template: "No Records Available"
            },
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: {
                pageSize: 150,
                messages: {
                    display: "{0}-{1} of {2} records"
                }
            },
            //reorderable: true,
            scrollable: true,
            columns: [
                {
                    field: "RecipeCode", title: "Code", width: "25px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Recipe Name", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "RecipeAlias", title: "Recipe Alias", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "20px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "Quantity", title: "Qty", width: "20px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "RecipeType", title: "Type", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    template: '# if (IsBase == true) {#<div>Base Recipe</div>#} else {#<div>Final Recipe</div>#}#',
                },
                {
                    field: "Impact", title: "Impacts", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    template: '# if (IsBase == true) { #<div>#:Impact# Recipe(s)</div>#} else {#<div>#:Impact# Dish</div>#}#',
                },

                //{
                //    field: "IsActive",
                //    title: "Active", width: "25px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" id="chkbox-#:ID#" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "PublishedDateAsString", title: "Last Updated", width: "35px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    // template: '# if (PublishedDate == null) {#<span>-</span>#} else{#<span>#: kendo.toString(PublishedDate, "dd-MMM-yy")#</span>#}#',
                    headerAttributes: {
                        style: "text-align: center;"
                    }

                },
                {
                    field: "Status", title: "Status", width: "30px", attributes: {
                        class: "mycustomstatus",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Published#}#',
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },
                {
                    field: "Edit", title: "Configure", width: "30px",
                    attributes: {
                        style: "text-align: left; font-weight:normal;cursor: pointer;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            template: function (dataItem) {
                                var html = `  <div title="Edit" onClick="recipeGridEdit(this)"  style='color:#8f9090; margin-left:20px' class='fa fa-pen EditRecipe'></div>`;
                                return html;
                            },

                        },
                        {
                            name: 'Print',
                            template: function (dataItem) {

                                var html = '<div title="Print" onClick="recipeGridPrint(this)" style="color:#8f9090; margin-left:8px" class="fa fa-print PrintRecipe"></div>';
                                return html;
                            },

                        },
                        {
                            name: 'Info',
                            template: function (dataItem) {
                                var html = '<div title="View" onClick="recipeGridInfo(this)"  style="color:#8f9090; margin-left:8px" class="fa fa-eye InfoRecipe"></div>';
                                return html;
                            },

                        },
                    ],
                }
            ],
            dataSource: {
                sort: { field: "IsBase", dir: "asc" },
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        //Krish 03-08-2022
                        //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (result) {
                        //    Utility.UnLoading();
                        //    if (result != null) {
                        //        copyData = result;
                        //        if (isShowElmentoryRecipe) {
                        //            result = result.filter(m => m.IsElementory);
                        //        }
                        //        options.success(result);

                        //        copyData.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
                        //        populateRecipeCopyDropDown();

                        //    }
                        //    else {
                        //        options.success("");
                        //    }
                        //},
                        //    {
                        //        condition: "All",
                        //        DishCode: "0"
                        //    }
                        //    , true);
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSectorRecipeDataList, function (data) {
                            //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (result) {
                            Utility.UnLoading();
                            var result = data.RecipeData;
                            if (result != null) {
                                result = result.filter(m => m.RecipeType == "Base Recipe");
                                copyData = result;
                                if (isShowElmentoryRecipe) {
                                    result = result.filter(m => m.IsElementory);
                                }
                                options.success(result);

                                copyData.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
                                populateRecipeCopyDropDown();

                            }
                            else {
                                options.success("");
                            }
                            var result1 = data.ApplicationSettingData;
                            if (result1 != null) {
                                applicationSettingData = result1;
                                for (item of applicationSettingData) {
                                    if (item.Code == "FLX-00004")//item.Name == "Major Identification Threshold" || 
                                        MajorPercentData = item.Value;
                                    $(".asPercent").text(MajorPercentData);
                                }
                            }
                            //}, { condition: "All", DishCode: "0" }, true);
                        }, { onLoad: true }, true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            RecipeCode: { type: "string", editable: false },
                            Name: { type: "string", editable: false },
                            RecipeAlias: { type: "string", editable: false },
                            UOMName: { type: "string", editable: false },
                            IsBase: { type: "boolean", editable: false },
                            Quantity: { type: "int", editable: false },
                            IsActive: { type: "boolean", editable: false },

                            Status: { type: "string", editable: false },
                            RecipeType: { type: "string", editable: false },
                            PublishedDate: { type: "date" },
                            PublishedDateAsString: { type: "string", editable: false },
                            IsElementoryString: { type: "string", editable: false },
                            IsElementory: { type: "boolean", editable: false },

                        }
                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            // height: 500,
            dataBound: function (e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }
                    else {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    }

                }


                var items = e.sender.items();
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsElementory) {
                        $(this).find(".MapNutrients").addClass("k-state-disabled");
                    }
                });

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".EditRecipe").addClass("k-state-disabled");
                    }

                    //Krish
                    for (item of applicationSettingData) {
                        if (item.Code == "FLX-00009" && item.IsActive == true && item.Value == "0")//item.Name == "Print Recipe Button Display Sector"
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == "FLX-00009" && item.IsActive != true)
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == "FLX-00009" && item.IsActive == true && item.Value == "1")
                            $(this).find(".PrintRecipe").show();
                        //Krish 20-07-2022
                        if (item.Code == "FLX-00012" && item.IsActive == true && item.Value == "0")//item.Name == "Info Recipe Button Display Sector"
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == "FLX-00012" && item.IsActive != true)
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == "FLX-00012" && item.IsActive == true && item.Value == "1")
                            $(this).find(".InfoRecipe").show();
                    }
                });



                //items.each(function (e) {

                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //    }
                //});


                $(".chkbox").on("change", function (e) {
                    // 
                    isStatusSave = true;
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridRecipe").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    RecipeName = tr.Name;
                    RecipeCode = tr.RecipeCode;
                    Recipe_ID = tr.ID;
                    $("#hiddenRecipeID").val(tr.ID);
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }
                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Recipe " + active + "?", "Recipe Update Confirmation", "Yes", "No", function () {
                        //  SaveRecipeStatus();
                        getRecipeDependenciesData();
                        if (recipeImpactedData.IsMOGImpacted) {
                            $(".recipeDepStatus").text(datamodel.IsActive ? "active" : "inactive");
                            showRecipeImpactedMessage();
                            showRecipeDependenciesPopUp();
                        }
                        else {
                            SaveRecipeStatus();
                        }
                    }, function () {
                        $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsActive);
                    });
                    return true;
                });


                $(".chkboxElementory").on("change", function (e) {
                    // 
                    // isStatusSave = true;
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridRecipe").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    RecipeName = tr.Name;
                    RecipeCode = tr.RecipeCode;
                    Recipe_ID = tr.ID;
                    $("#hiddenRecipeID").val(tr.ID);
                    datamodel.IsElementory = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsElementory) {
                        active = "Elementary";
                    } else {
                        active = "Non Elementary";
                    }
                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Recipe " + active + "?", "Recipe Update Confirmation", "Yes", "No", function () {
                        SaveRecipeElmentoryStatus();
                    }, function () {
                        $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsElementory);
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });

                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }
        })
    }

    //$(".k-label")[0].innerHTML.replace("items", "records");
}

function recipeGridEdit(e) {
    Utility.Loading();
    //Krish 03-08-2022
    loadFormData();
    setTimeout(function () {
        //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {

        //    var dataSource = data;

        //    baserecipedata = [];
        //    baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        if (dataSource[i].CostPerUOM == null) {
        //            dataSource[i].CostPerUOM = 0;
        //        }
        //        if (dataSource[i].CostPerKG == null) {
        //            dataSource[i].CostPerKG = 0;
        //        }
        //        if (dataSource[i].TotalCost == null) {
        //            dataSource[i].TotalCost = 0;
        //        }
        //        if (dataSource[i].Quantity == null) {
        //            dataSource[i].Quantity = 0;
        //        }
        //        if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
        //            baserecipedata.push({
        //                "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
        //            });
        //        else
        //            baserecipedata.push({
        //                "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
        //            });

        //    }
        //    baserecipedatafiltered = baserecipedata;
        //    basedataList = baserecipedata;
        //}, { condition: "Base", DishCode: "0" }, true);

        var gridObj = $("#gridRecipe").data("kendoGrid");
        var tr = gridObj.dataItem($(e).closest("tr")); // 'e' is the HTML element <a>
        if (!tr.IsActive) {
            return;
        }
        Utility.Loading();
        setTimeout(function () {
            $("#success").css("display", "none");
            $("#error").css("display", "none");

            // var tr = gridObj.dataItem($(e).closest("tr")); // 'e' is the HTML element <a>
            onEditRecipeGrid(tr, false);
            return true;
            Utility.UnLoading();
        }, 2000);
    }, 2000);
}

function loadFormData() {

    //Krish 03-08-2022
    //HttpClient.MakeRequest(CookBookMasters.GetApplicationSettingDataList, function (result) {

    //    if (result != null) {
    //        applicationSettingData = result;
    //        for (item of applicationSettingData) {
    //            if (item.Code == "FLX-00004")//item.Name == "Major Identification Threshold" || 
    //                MajorPercentData = item.Value;
    //            $(".asPercent").text(MajorPercentData);
    //        }

    //    }
    //    else {

    //    }
    //}, null

    //    , false);
    //HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {

    //    user = result;
    //    if (user.SectorNumber == "80") {
    //        $(".healthcareonly").show();

    //    }
    //    else {
    //        $(".healthcareonly").hide();
    //    }

    //    if (user.SectorNumber == "20") {
    //        $(".rcHide").show();
    //        $(".mfgHide").hide();
    //    }
    //    else {
    //        $(".rcHide").hide();
    //        $(".mfgHide").show();
    //    }

    //    if (user.SectorNumber == "10") {
    //        $(".googleOnly").show();
    //    }
    //    else {
    //        $(".googleOnly").hide();
    //    }
    //    populateRecipeGrid();
    //}, null, true);

    HttpClient.MakeRequest(CookBookMasters.GetSectorRecipeDataList, function (result) {
        //HttpClient.MakeRequest(CookBookMasters.GetMOGDataList, function (data) {

        var dataSource = result.MOGData;

        mogdata = [];
        mogdata.push({ "MOG_ID": "Select MOG", "MOGName": "Select MOG", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "DKgValue": 0, "MOG_Code": "", "IsActive": false });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].CostPerUOM == null) {
                dataSource[i].CostPerUOM = 0;
            }
            if (dataSource[i].CostPerKG == null) {
                dataSource[i].CostPerKG = 0;
            }
            if (dataSource[i].TotalCost == null) {
                dataSource[i].TotalCost = 0;
            }
            if (dataSource[i].Alias != null && dataSource[i].Alias.trim() != "" && dataSource[i].Alias != dataSource[i].Name)
                mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name + " (" + dataSource[i].Alias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "DKgValue": dataSource[i].DKgValue, "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive });
            else
                mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode, "DKgValue": dataSource[i].DKgValue, "IsActive": dataSource[i].IsActive });
        }

        mogdataList = mogdata;

        //}, null, true);
        //HttpClient.MakeRequest(CookBookMasters.GetDishDataList, function (data) {

        var dataSource = result.DishData;
        dishdata = [];
        dishdata.push({ "value": "Select Dish", "text": "Select Dish" });
        for (var i = 0; i < dataSource.length; i++) {
            dishdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name });
        }
        //}, null, true);
        //HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
        var dataSource = result.UOMModuleMappingData;
        uommodulemappingdata = [];
        uommodulemappingdata.push({ "value": "Select", "text": "Select", "UOMCode": "Select" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].IsActive) {
                uommodulemappingdata.push({
                    "value": dataSource[i].UOM_ID, "UOM_ID": dataSource[i].UOM_ID, "UOMName": dataSource[i].UOMName, "text": dataSource[i].UOMName,
                    "UOMModuleCode": dataSource[i].UOMModuleCode, "UOMCode": dataSource[i].UOMCode
                });
            }
        }
        moguomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001');
        uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");
        recipecategoryuomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00006' || m.text == "Select");
        populateUOMDropdown0();
        populateDishDropdown();

        //}, null, false);

        //HttpClient.MakeSyncRequest(CookBookMasters.GetNutritionMasterDataList, function (data) {
        var data = result.NutritionElementMasterData;
        nutrientMasterdataSource = data.filter(m => m.IsActive);

        //}, null, true);

        //HttpClient.MakeRequest(CookBookMasters.GetSubSectorMasterList, function (data) {
        var data = result.SubSectorData;
        if (data.length > 1) {
            isSubSectorApplicable = true;
            data.unshift({ "SubSectorCode": 'Select', "Name": "Select Sub Sector" })
            subsectordata = data;
            populateSubSectorDropdown("");
        }
        else {
            isSubSectorApplicable = false;
        }
        //}, { isAllSubSector: true }, false);
        //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {

        var dataSource = result.RecipeData;

        baserecipedata = [];
        baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].CostPerUOM == null) {
                dataSource[i].CostPerUOM = 0;
            }
            if (dataSource[i].CostPerKG == null) {
                dataSource[i].CostPerKG = 0;
            }
            if (dataSource[i].TotalCost == null) {
                dataSource[i].TotalCost = 0;
            }
            if (dataSource[i].Quantity == null) {
                dataSource[i].Quantity = 0;
            }
            if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
                baserecipedata.push({
                    "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                });
            else
                baserecipedata.push({
                    "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                });

        }
        baserecipedatafiltered = baserecipedata;
        basedataList = baserecipedata;
        //}, { condition: "Base", DishCode: "0" }, true);
    }, { isAddEdit: true }, true);

    //HttpClient.MakeRequest(CookBookMasters.GetMOGDataList, function (data) {

    //    var dataSource = data;

    //    mogdata = [];
    //    mogdata.push({ "MOG_ID": "Select MOG", "MOGName": "Select MOG", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "DKgValue": 0, "MOG_Code": "", "IsActive": false });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        if (dataSource[i].CostPerUOM == null) {
    //            dataSource[i].CostPerUOM = 0;
    //        }
    //        if (dataSource[i].CostPerKG == null) {
    //            dataSource[i].CostPerKG = 0;
    //        }
    //        if (dataSource[i].TotalCost == null) {
    //            dataSource[i].TotalCost = 0;
    //        }
    //        if (dataSource[i].Alias != null && dataSource[i].Alias.trim() != "" && dataSource[i].Alias != dataSource[i].Name)
    //            mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name + " (" + dataSource[i].Alias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "DKgValue": dataSource[i].DKgValue, "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive });
    //        else
    //            mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode, "DKgValue": dataSource[i].DKgValue, "IsActive": dataSource[i].IsActive });
    //    }

    //    mogdataList = mogdata;

    //}, null, true);

    ////HttpClient.MakeRequest(CookBookMasters.GetMOGUOMDataList, function (data) {
    ////    moguomdetailsdata = data;
    ////}, null, true);

    ////HttpClient.MakeRequest(CookBookMasters.GetRecipeDataList, function (data) {

    ////    var dataSource = data;

    ////    baserecipedata = [];
    ////    baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
    ////    for (var i = 0; i < dataSource.length; i++) {
    ////        if (dataSource[i].CostPerUOM == null) {
    ////            dataSource[i].CostPerUOM = 0;
    ////        }
    ////        if (dataSource[i].CostPerKG == null) {
    ////            dataSource[i].CostPerKG = 0;
    ////        }
    ////        if (dataSource[i].TotalCost == null) {
    ////            dataSource[i].TotalCost = 0;
    ////        }
    ////        if (dataSource[i].Quantity == null) {
    ////            dataSource[i].Quantity = 0;
    ////        }
    ////        if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
    ////            baserecipedata.push({
    ////                "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
    ////            });
    ////        else
    ////            baserecipedata.push({
    ////                "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
    ////            });

    ////    }
    ////    baserecipedatafiltered = baserecipedata;
    ////    basedataList = baserecipedata;
    ////}, { condition: "Base", DishCode: "0" }, true);

    //HttpClient.MakeRequest(CookBookMasters.GetDishDataList, function (data) {

    //    var dataSource = data;
    //    dishdata = [];
    //    dishdata.push({ "value": "Select Dish", "text": "Select Dish" });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        dishdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name });
    //    }
    //}, null, true);

    ////HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

    ////    var dataSource = data;
    ////    uomdata = [];
    ////    uomdata.push({ "value": "Select", "text": "Select", "UOMCode": "" });
    ////    for (var i = 0; i < dataSource.length; i++) {
    ////        uomdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode });
    ////    }
    ////    populateDishDropdown();
    ////}, null, false);

    //HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
    //    var dataSource = data;
    //    uommodulemappingdata = [];
    //    uommodulemappingdata.push({ "value": "Select", "text": "Select", "UOMCode": "Select" });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        if (dataSource[i].IsActive) {
    //            uommodulemappingdata.push({
    //                "value": dataSource[i].UOM_ID, "UOM_ID": dataSource[i].UOM_ID, "UOMName": dataSource[i].UOMName, "text": dataSource[i].UOMName,
    //                "UOMModuleCode": dataSource[i].UOMModuleCode, "UOMCode": dataSource[i].UOMCode
    //            });
    //        }
    //    }
    //    moguomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001');
    //    uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");
    //    recipecategoryuomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00006' || m.text == "Select");

    //    populateDishDropdown();

    //}, null, false);

    //HttpClient.MakeSyncRequest(CookBookMasters.GetNutritionMasterDataList, function (data) {
    //    nutrientMasterdataSource = data.filter(m => m.IsActive);

    //}, null, true);

    //HttpClient.MakeRequest(CookBookMasters.GetSubSectorMasterList, function (data) {

    //    if (data.length > 1) {
    //        isSubSectorApplicable = true;
    //        data.unshift({ "SubSectorCode": 'Select', "Name": "Select Sub Sector" })
    //        subsectordata = data;
    //        populateSubSectorDropdown("");   
    //    }
    //    else {
    //        isSubSectorApplicable = false;
    //    }
    //}, { isAllSubSector: true }, false);


    basedata = [{ "value": "Select", "text": "Select" }, { "value": "No", "text": "Final Recipe" },
    { "value": "Yes", "text": "Base Recipe" }];

    $("#gridBulkChange").css("display", "none");
    populateBulkChangeControls();
    var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
    changeControls.css("background-color", "#fff");
    changeControls.css("border", "none");
    changeControls.eq(2).text("");
    changeControls.eq(2).append("<div id='uom' style='width:100%; font-size:10.5px!important'></div>");
    changeControls.eq(3).text("");
    changeControls.eq(3).append("<input id='quantity' style='width:90%; margin-left:3px; font-size:10.5px!important'></div>");
    changeControls.eq(4).text("");
    changeControls.eq(4).append("<div id='base' style='width:100%; font-size:10.5px!important'></div>");
}
function elementoryRecipe(e) {
    isShowElmentoryRecipe = e.checked;
    populateRecipeGrid();
}

function mapNutrients(e) {
    Utility.Loading();
    setTimeout(function () {
        $("#inputNutrientClear").empty();
        $("#inputNutrientClear").append('<div style="display: none; text - align: left; " id="inputNutrient" ></div>');
        checkedNutrientCodes = [];
        checkedIds = [];
        CheckedTrueAPLCodes = [];
        RecipeWisenutrientMasterdataSource = [];
        isShowPreview = false;
        $("#btnShowChecked").prop('checked', false);
        $("#btnPreview").prop('checked', false);
        var gridObj = $("#gridRecipe").data("kendoGrid");
        var tr = gridObj.dataItem($(e).closest("tr"));

        datamodel = tr;
        RecipeName = tr.Name;
        RecipeCode = tr.RecipeCode;
        RecipeID = tr.ID;
        $("#nRecipecodelabel").text(tr.RecipeCode);
        $("#ninputRecipenamelabel").text(tr.Name);
        $("#ninputuomnamelabel").text(tr.UOMName);
        $("#ninputRecipename").val(tr.Name);
        $("#ninputRecipename").attr("disabled", "disabled");
        $("#ninputRecipealiaslabel").val(tr.Alias);
        $("#nRecipecode").val(tr.RecipeCode);
        $("#Recipequantity").val(tr.NutrientRecipeQuantity);
        var dialog = $("#windowEditRecipeWiseNutrient").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();
        $("#Recipeid").val(tr.ID);
        dialog.title(RecipeName + ": ");
        $("#btnGo").css("display", "inline-block");
        RecipeCode = tr.RecipeCode;

        if (tr.NutritionData != null && tr.NutritionData != "") {
            RecipeWisenutrientMasterdataSource = JSON.parse(tr.NutritionData);
            var nData = JSON.parse(tr.NutritionData);
            for (dataItem of nutrientMasterdataSource) {
                var notExists = nData.filter(m => m.ElementCode == dataItem.NutritionElementCode);
                if (notExists == null || notExists.length == 0) {
                    var nutmodel = {
                        "TypeCode": dataItem.NutritionElementTypeCode,
                        "TypeName": dataItem.NutritionElementTypeName,
                        "ElementCode": dataItem.NutritionElementCode,
                        "ElementName": dataItem.Name,
                        "Quantity": 0,
                        "UOMCode": dataItem.NutritionElementUOMCode,
                        "UOMName": dataItem.NutritionElementUOMName,
                        "DisplayOrder": dataItem.DisplayOrder,
                        "IsDisplay": dataItem.IsDisplay
                    }
                    RecipeWisenutrientMasterdataSource.push(nutmodel);
                }
            }

        }
        else {
            for (dataItem of nutrientMasterdataSource) {
                var nutmodel = {
                    "TypeCode": dataItem.NutritionElementTypeCode,
                    "TypeName": dataItem.NutritionElementTypeName,
                    "ElementCode": dataItem.NutritionElementCode,
                    "ElementName": dataItem.Name,
                    "Quantity": 0,
                    "UOMCode": dataItem.NutritionElementUOMCode,
                    "UOMName": dataItem.NutritionElementUOMName,
                    "DisplayOrder": dataItem.DisplayOrder,
                    "IsDisplay": dataItem.IsDisplay
                }
                RecipeWisenutrientMasterdataSource.push(nutmodel);
                checkedNutrientCodes.push("1_" + dataItem.NutritionElementCode);
            }
        }
        populateNutrientMasterGrid();

        Utility.UnLoading();
    }, 2000);
}

function populateNutrientMasterGrid() {
    Utility.Loading();


    var gridVariable = $("#gridRecipeWiseNutrients");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "RecipeMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: {
            pageSize: 100,
            messages: {
                display: "{0}-{1} of {2} records"
            }
        },
        groupable: false,
        //reorderable: true,
        //scrollable: true,
        columns: [
            {
                field: "ElementCode", title: "Nutrient Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ElementName", title: "Nutrient", width: "150px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "TypeName", title: "Nutrient Type", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "Nutrient UOM", width: "40px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Nutrient Quantity", width: "60px",
                template: '<input type="number" class="inputbrqty"  value="Quantity" name="ElementQuantity" oninput="savePriceChange(this,true)"/>',
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            }
        ],
        dataSource: {
            data: RecipeWisenutrientMasterdataSource.sort((a, b) => a.DisplayOrder - b.DisplayOrder),
            schema: {
                model: {
                    fields: {
                        ElementCode: { type: "string" },
                        ElementName: { type: "string" },
                        UOMName: { type: "string" },
                        TypeName: { type: "string" },
                        Quantity: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
        height: 415,
        minHeight: 200,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();
            items.each(function (e) {
                var dataItem = grid.dataItem(this);
                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
}


var lastValid = 0;
var validNumber = new RegExp(/^\d*\.?\d*$/);
function validateNumber(elem) {

    if (validNumber.test(elem.value)) {
        lastValid = elem.value;
    } else {
        elem.value = lastValid;
    }
}

function savePriceChange(e, isSavePrice) {
    validateNumber(e);
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridRecipeWiseNutrients").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var ElementCode = dataItem.ElementCode;
    if (dataItem.Quantity == $(e).val()) {
        return;
    }
    dataItem.Quantity = $(e).val();
    dataItem.set('Quantity', $(e).val());
    //RecipeWisenutrientMasterdataSource = RecipeWisenutrientMasterdataSource.forEach(function (item, index) {
    //    if (item.ElementCode == ElementCode) {
    //        item.Quantity = $(e).val();
    //    };
    //});
}

function saveRecipeNutrientMapping() {
    var RecipeQuantity = $("#Recipequantity").text();
    if (RecipeQuantity == null || RecipeQuantity == "" || RecipeQuantity == undefined || RecipeQuantity == "0") {
        toastr.error("Please enter Recipe Quantity for Nutrients First.");
        Utility.UnLoading();
        return;
    }
    var grid = $("#gridRecipeWiseNutrients").data("kendoGrid");
    var nutritionData = JSON.stringify(grid._data);

    var model;
    if (datamodel != null) {
        model = datamodel;
        model.RecipeCode = RecipeCode;
        model.NutrientRecipeQuantity = RecipeQuantity;
        model.NutritionData = nutritionData;
        model.CreatedOn = kendo.parseDate(model.CreatedOn);
        model.IsRecipeNutrientMappingData = true;
    }

    //if (filterData == null || filterData.length == 0) {
    //    toastr.error("Please update or map Nutrients data First.");
    //    return;
    //}
    //else {

    $("#NatiRecipebtnSubmit").attr('disabled', 'disabled');
    HttpClient.MakeSyncRequest(CookBookMasters.SaveRecipeNutrientMappingData, function (result) {
        if (result == false) {
            $('#NatiRecipebtnSubmit').removeAttr("disabled");
            toastr.error("Some error occured, please try again");
            $("#inputRecipe").focus();
            Utility.UnLoading();
        }
        else {
            $(".k-overlay").hide();
            var orderWindow = $("#windowEditRecipeWiseNutrient").data("kendoWindow");
            orderWindow.close();
            $('#NatiRecipebtnSubmit').removeAttr("disabled");
            toastr.success("Recipe Nutrient  mapping saved successfully");
            Utility.UnLoading();
        }
    }, {
        model: datamodel

    }, true);
    // }
}

function populateNutrientsMasterDropdown() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetNutritionMasterDataList, function (data) {
        nutrientMasterdataSource = data.filter(m => m.IsActive);
        //nutrientMasterdataSource.unshift({ "NutritionElementCode": "Select", "Name": "Select Nutrient" })

    }, null, true);

    nutrientFilterDataSource = nutrientMasterdataSource.filter(function (item) {
        if (!item.RecipeCode)
            return item;
    });


    $("#inputNutrient").kendoMultiSelect({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "NutritionElementCode",
        dataSource: nutrientFilterDataSource,
        placeholder: " ",
        index: 0,
        autoBind: true,
        autoClose: false,
    });
    var multiselect = $("#inputNutrient").data("kendoMultiSelect");

    //clear filter
    multiselect.dataSource.filter({});

    //set value
    // multiselect.value(RecipeWiseNutrientArray);

}

function addNutrient() {
    var dropdownlist = $("#inputNutrient").data("kendoMultiSelect");

    var dataItems = dropdownlist.dataItems();
    for (dataItem of dataItems) {
        var nutmodel = {
            "TypeCode": dataItem.NutritionElementTypeCode,
            "TypeName": dataItem.NutritionElementTypeName,
            "ElementCode": dataItem.NutritionElementCode,
            "ElementName": dataItem.Name,
            "Quantity": 0,
            "UOMCode": dataItem.NutritionElementUOMCode,
            "UOMName": dataItem.NutritionElementUOMName,
            "DisplayOrder": dataItem.DisplayOrder,
            "IsDisplay": dataItem.IsDisplay
        }
        RecipeWisenutrientMasterdataSource.unshift(nutmodel);
        checkedNutrientCodes.push("1_" + dataItem.NutritionElementCode);
        var grid = $("#gridRecipeWiseNutrients").data("kendoGrid");
        grid.dataSource.insert(0, nutmodel);
    }
    filterMultiSelectDropdown();
}

function filterMultiSelectDropdown() {
    var grid = $("#gridRecipeWiseNutrients").data("kendoGrid");
    var multiselect = $("#inputNutrient").data("kendoMultiSelect");

    var modifiedDataSource = nutrientMasterdataSource.filter(function (array_el) {
        return grid.dataSource._data.filter(function (anotherOne_el) {
            return anotherOne_el.ElementCode == array_el.NutritionElementCode;
        }).length == 0
    });

    multiselect.value([]);
    multiselect.setDataSource(modifiedDataSource);
}



function checkRecipeNutrientChange() {
    var isChanged = true;

    return isChanged;
}

function saveAll() {
    var RecipeQuantity = $("#Recipequantity").text();
    if (RecipeQuantity == null || RecipeQuantity == "" || RecipeQuantity == undefined || RecipeQuantity == "0") {
        toastr.error("Please enter Recipe Quantity for each Nutrients First.");
        Utility.UnLoading();
        return;
    }
    if (checkRecipeNutrientChange()) {
        $("#confirmSave_No").css('display', 'block');
        $("#confirmSave_Ok").addClass('white-btn');
        $("#confirmSave_Ok").addClass('white-btn');
        $("#confirmSave_Ok").removeClass('colored-btn');
        $("#confirmSave_Box").css('z-index', '99999');
        Utility.UnLoading();
        Utility.Page_Alert_Save("Proceeding will perform change Recipe Nutrients mapping. Are you sure to proceed?", "Recipe-Nutrients Mapping Change Confirmation", "Yes", "No",
            function () {
                saveRecipeNutrientMapping();
            },
            function () {

            }
        );
    }

    Utility.UnLoading();
}

function onMOGDDLChange(e) {
    
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG', false)) {
        toastr.error("Mog is already selected");
        var dropdownlist = $("#ddlMOG").data("kendoDropDownList");
        dropdownlist.select("");
        //dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    console.log("Data Item ")
    console.log(dataItem)

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);

    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    // var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.CostPerUOM = UnitCost;
    dataItem.MOGCode = Obj[0].MOG_Code;

    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);

    var dkgtotal = 0;
    if (user.SectorNumber == '20') {
        dkgtotal = e.sender.dataItem().DKgValue * dataItem.Quantity;
        dataItem.set("DKgValue", dkgtotal);
        dataItem.DKgValue = e.sender.dataItem().DKgValue;
        $(element).closest('tr').find('.dkgtotal')[0].innerHTML = dkgtotal;
        dataItem.TotalCost = dataItem.CostPerUOM * dkgtotal;
    }
    else {
        dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    }
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('');
};

function onMOGUOMDDLChange(e) {
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    console.log("Data Item ")
    console.log(dataItem)

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("UOM_ID", e.sender.dataItem().UOM_ID);
    dataItem.set("UOMName", data);
    dataItem.set("UOMCode", e.sender.value());
    dataItem.UOMName = data;

    //var costperkg = 0;
    //var costperuom = 0;
    //costperkg = e.sender.dataItem().CostPerKG;
    //costperuom = e.sender.dataItem().CostPerUOM;

    dataItem.UOM_ID = e.sender.dataItem().UOM_ID;
    dataItem.UOMCode = e.sender.value();
    //dataItem.CostPerKG = costperkg;
    //dataItem.CostPerUOM = costperuom;

    //dataItem.set("CostPerKG", costperkg);
    //dataItem.set("CostPerUOM", costperuom);
    //var dkgtotal = 0;
    //if (user.SectorNumber == '20') {
    //    dkgtotal = e.sender.dataItem().DKgValue * dataItem.Quantity;
    //    dataItem.set("DKgValue", dkgtotal);
    //    dataItem.DKgValue = e.sender.dataItem().DKgValue;
    //    $(element).closest('tr').find('.dkgtotal')[0].innerHTML = dkgtotal;
    //    $(element).closest('tr').find('.uomcost')[0].innerHTML = costperuom;
    //    dataItem.TotalCost = dataItem.CostPerUOM * dkgtotal ;
    //}
    //else {
    //    $(element).closest('tr').find('.uomcost')[0].innerHTML = costperuom;
    //    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    //}

    //$(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    //CalculateGrandTotal('');
}

function onMOGUOMDDLChange0(e) {
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    console.log("Data Item ")
    console.log(dataItem)

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("UOM_ID", e.sender.dataItem().UOM_ID);
    dataItem.set("UOMName", data);
    dataItem.set("UOMCode", e.sender.value());
    dataItem.UOMName = data;

    dataItem.UOM_ID = e.sender.dataItem().UOM_ID;
    dataItem.UOMCode = e.sender.value();
    dataItem.CostPerKG = costperkg;
    dataItem.CostPerUOM = costperuom;

    dataItem.set("CostPerKG", costperkg);
    dataItem.set("CostPerUOM", costperuom);
    var dkgtotal = 0;
    if (user.SectorNumber == '20') {
        dkgtotal = e.sender.dataItem().DKgValue * dataItem.Quantity;
        dataItem.set("DKgValue", dkgtotal);
        dataItem.DKgValue = e.sender.dataItem().DKgValue;
        $(element).closest('tr').find('.dkgtotal')[0].innerHTML = dkgtotal;
        $(element).closest('tr').find('.uomcost')[0].innerHTML = costperuom;
        dataItem.TotalCost = dataItem.CostPerUOM * dkgtotal;
    }
    else {
        $(element).closest('tr').find('.uomcost')[0].innerHTML = costperuom;
        dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    }

    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('0');
}

function populateBaseRecipeGrid(recipeCode) {
    // alert(recipeCode);
    if (recipeCode == null)
        recipeCode ="";
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe");
    gridVariable.html("");

    if (user.SectorNumber == '20') {
        gridVariable.kendoGrid({
            toolbar: [{ name: "clearall", text: "Clear All" },
            { name: "cancel", text: "Reset" }, {
                name: "create", text: "Add"
            }
            ],
            groupable: false,
            editable: true,
            columns: [
                {
                    field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change Base Recipe" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>`
                            + '<span class="brspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                            + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails(this)'></i>`;
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "BaseRecipeName",
                                dataValueField: "BaseRecipe_ID",
                                autoBind: true,
                                dataSource: refreshBaseRecipeDropDownData(options.model.BaseRecipeName),
                                value: options.field,
                                change: onBRDDLChange
                            });
                    }
                },

                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "Quantity", title: "Quantity", width: "35px",
                    template: function (dataItem) {
                        var html = '<input  type="number" class="inputbrqty" style="text - align: center;" name="Quantity" oninput="calculateItemTotal(this,null)"/>';
                        return html;
                    },

                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridBaseRecipe").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (selectedRow.IsMajor) {
                                    Utility.Page_Alert_Save("This is a major Ingredient. Do you still want to remove it from the Recipe?", "Delete Confirmation", "Yes", "No",
                                        function () {
                                            gridObj.dataSource._data.remove(selectedRow);
                                            CalculateGrandTotal('');
                                            return true;
                                        },
                                        function () {
                                        }
                                    );
                                }
                                else {
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal('');
                                    return true;
                                }
                            }
                        },
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                BaseRecipetotal = 0.0;
                                $.each(obj, function (key, value) {

                                    BaseRecipetotal = parseFloat(BaseRecipetotal) + parseFloat(value.Quantity);
                                });
                                $("#totBaseRecpQty").text(Utility.MathRound(BaseRecipetotal, 2));

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridBaseRecipe").css("display", "block");
                                $("#emptybr").css("display", "none");
                            } else {
                                $("#gridBaseRecipe").css("display", "none");
                                $("#emptybr").css("display", "block");
                            }

                        },
                            {recipeCode: RecipeCode}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false },
                            BaseRecipe_ID: { type: "number", validation: { required: true } },
                            BaseRecipeName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },

            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },

            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.brTemplate');
                    $(ddt).kendoDropDownList({
                        index: 0,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "BaseRecipeName",
                        dataValueField: "BaseRecipe_ID",
                        change: onBRDDLChange,
                        //  autoBind: true,
                    });
                    //  ddt.find(".k-input").val(dataItem.BaseRecipeName);
                    $(this).find('.inputbrqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
    }
    else {
        gridVariable.kendoGrid({
            toolbar: [{ name: "clearall", text: "Clear All" },
            { name: "cancel", text: "Reset" }, {
                name: "create", text: "Add"
            }
            ],
            groupable: false,
            editable: true,
            columns: [
                {
                    field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change Base Recipe" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>`
                            + '<span class="brspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                            + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails(this)'></i>`;
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "BaseRecipeName",
                                dataValueField: "BaseRecipe_ID",
                                autoBind: true,
                                dataSource: refreshBaseRecipeDropDownData(options.model.BaseRecipeName),
                                value: options.field,
                                change: onBRDDLChange
                            });
                    }
                },

                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "Quantity", title: "Quantity", width: "35px",
                    template: function (dataItem) {
                        var html = '<input  type="number" class="inputbrqty" style="text - align: center;" name="Quantity" oninput="calculateItemTotal(this,null)"/>';
                        return html;
                    },

                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "major",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
                },
                {
                    field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "minor",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridBaseRecipe").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (selectedRow.IsMajor) {
                                    Utility.Page_Alert_Save("This is a major Ingredient. Do you still want to remove it from the Recipe?", "Delete Confirmation", "Yes", "No",
                                        function () {
                                            gridObj.dataSource._data.remove(selectedRow);
                                            CalculateGrandTotal('');
                                            return true;
                                        },
                                        function () {
                                        }
                                    );
                                }
                                else {
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal('');
                                    return true;
                                }
                            }
                        },
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                BaseRecipetotal = 0.0;
                                $.each(obj, function (key, value) {

                                    BaseRecipetotal = parseFloat(BaseRecipetotal) + parseFloat(value.Quantity);
                                });
                                $("#totBaseRecpQty").text(Utility.MathRound(BaseRecipetotal, 2));

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridBaseRecipe").css("display", "block");
                                $("#emptybr").css("display", "none");
                            } else {
                                $("#gridBaseRecipe").css("display", "none");
                                $("#emptybr").css("display", "block");
                            }

                        },
                            {
                                recipeCode: recipeCode
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false },
                            BaseRecipe_ID: { type: "number", validation: { required: true } },
                            BaseRecipeName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },

            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },

            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.brTemplate');
                    $(ddt).kendoDropDownList({
                        index: 0,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "BaseRecipeName",
                        dataValueField: "BaseRecipe_ID",
                        change: onBRDDLChange,
                        //  autoBind: true,
                    });
                    //  ddt.find(".k-input").val(dataItem.BaseRecipeName);
                    $(this).find('.inputbrqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
    }



    //  cancelChangesConfirmation('gridBaseRecipe');
    var toolbar = $("#gridBaseRecipe").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.find(".k-grid-clearall").addClass("gridButton");

    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe .k-grid-content").addClass("gridInside");
    toolbar.find(".k-grid-clearall").addClass("gridButton");
    $('#gridBaseRecipe a.k-grid-clearall').click(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                $("#gridBaseRecipe").data('kendoGrid').dataSource.data([]);
                gridIdin = 'gridBaseRecipe';
                // showHideGrid('gridBaseRecipe', 'emptybr', 'bup_gridBaseRecipe');
                hideGrid('emptybr', 'gridBaseRecipe');
            },
            function () {
            }
        );

    });
    $("#gridBaseRecipe .k-grid-cancel-changes").click(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                // var grid = $("#" + gridIdin + "").data("kendoGrid");
                if (gridIdin == 'gridBaseRecipe') {
                    populateBaseRecipeGrid(resetID);
                    return;
                } else if (gridIdin == 'gridMOG') {
                    populateMOGGrid(resetID)
                    return;
                }
                // grid.cancelChanges();
            },
            function () {
            }
        );

    });

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
        Utility.UnLoading();
    });
}

function refreshBaseRecipeDropDownData(x) {
    // alert("call");
    if (basedataList != null && basedataList.length > 0) {
        var baseGridData = $("#gridBaseRecipe").data('kendoGrid').dataSource._data;
        if (baseGridData != null && baseGridData.length > 0) {
            baseGridData = baseGridData.filter(m => m.BaseRecipeName != "" && m.BaseRecipeName != x);
            baserecipedata = basedataList;
            var basedataListtemp = basedataList;
            basedataListtemp = basedataListtemp.filter(m => m.BaseRecipe_Code != "");
            basedataListtemp.forEach(function (item, index) {
                for (var i = 0; i < baseGridData.length; i++) {
                    if (baseGridData[i].BaseRecipeCode !== "" && item.BaseRecipe_Code === baseGridData[i].BaseRecipeCode) {
                        baserecipedata.splice(index, 1);
                        baserecipedata = baserecipedata.filter(r => r.BaseRecipe_Code !== item.BaseRecipe_Code && r.BaseRecipe_Code != RecipeCode);
                        break;
                    }
                }
                return true;
            });
        }
    }
    baserecipedata = baserecipedata.filter(r => r.BaseRecipe_Code != RecipeCode);
    return baserecipedata;
};

function populateMOGGrid(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG");
    gridVariable.html("");
    if (user.SectorNumber == '20') {

        gridVariable.kendoGrid({
            toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,
            //navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: refreshMOGDropDownData("", options.model.MOGName),
                                value: options.field,
                                change: onMOGDDLChange,
                            });

                    }
                },
                //{
                //    field: "UOMName", title: "UOM", width: "50px",
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {

                //        class: 'uomname',
                //        style: "text-align: center; font-weight:normal"
                //    },
                //},
                {
                    field: "UOMCode", title: "UOM", width: "40px",
                    attributes: {
                        style: "text-align: left; font-weight:normal",
                        class: ''
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="uomname" style="margin-left:8px">' + kendo.htmlEncode(dataItem.UOMName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOGUOM" text="' + options.model.UOMName + '" data-text-field="text" data-value-field="UOMCode" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "text",
                                dataValueField: "UOMCode",
                                autoBind: true,
                                dataSource: moguomdata,
                                // dataSource: refreshMOGUOMDropDownData(options.model.UOMName),
                                value: options.field,
                                change: onMOGUOMDDLChange,
                            });

                    }
                },

                {
                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },

                    template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,null)"/>',
                    attributes: {

                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "DKgTotal", title: "DKG", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },
                    attributes: {
                        class: "dkgtotal",
                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "uomcost",

                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }

                                var gridObj = $("#gridMOG").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (selectedRow.IsMajor) {
                                    Utility.Page_Alert_Save("This is a major Ingredient. Do you still want to remove it from the Recipe?", "Delete Confirmation", "Yes", "No",
                                        function () {
                                            gridObj.dataSource._data.remove(selectedRow);
                                            CalculateGrandTotal('');
                                            return true;
                                        },
                                        function () {
                                        }
                                    );
                                }
                                else {
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal('');
                                    return true;
                                }

                            }
                        },
                        //{
                        //    name: 'View APL',
                        //    click: function (e) {

                        //        var gridObj = $("#gridMOG").data("kendoGrid");
                        //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                        //        datamodel = tr;
                        //        console.log("MOG APL");
                        //        console.log(tr);
                        //        MOGName = tr.MOGName;

                        //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                        //        $("#windowEditMOGWiseAPL").kendoWindow({
                        //            animation: false
                        //        });
                        //        $(".k-overlay").css("display", "block");
                        //        $(".k-overlay").css("opacity", "0.5");
                        //        dialog.open();
                        //        dialog.center();
                        //        dialog.title("MOG APL Details - " + MOGName);
                        //        MOGCode = tr.MOGCode;
                        //        mogWiseAPLMasterdataSource = [];
                        //        getMOGWiseAPLMasterData();
                        //        populateAPLMasterGrid();
                        //        return true;
                        //    }
                        //}
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                if (user.SectorNumber == "20") {
                                    $.each(obj, function (key, value) {
                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.DKgValue * value.Quantity);
                                    });
                                }
                                else {
                                    $.each(obj, function (key, value) {
                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                    });
                                }


                                $("#totmog").text(mogtotal.toFixed(2));
                                var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                $("#ToTIngredients").text(Utility.MathRound(tot, 2));
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {
                                recipeCode: recipeCode
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "MOG_ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            Quantity: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false },
                            DKgValue: { editable: false },
                            DKgTotal: { editable: false },

                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {

                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        index: 0,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });


            },
            change: function (e) {
                if (e.action == "itemchange") {
                    e.items[0].dirtyFields = e.items[0].dirtyFields || {};
                    e.items[0].dirtyFields[e.field] = true;
                }
            },
        });
    }
    else {
        gridVariable.kendoGrid({
            toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,
            //navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: refreshMOGDropDownData("", options.model.MOGName),
                                value: options.field,
                                change: onMOGDDLChange,
                            });

                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {

                        class: 'uomname',
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },

                    template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,null)"/>',
                    attributes: {

                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "major",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
                },
                {
                    field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "minor",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "uomcost",

                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridMOG").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (selectedRow.IsMajor) {
                                    Utility.Page_Alert_Save("This is a major Ingredient. Do you still want to remove it from the Recipe?", "Delete Confirmation", "Yes", "No",
                                        function () {
                                            gridObj.dataSource._data.remove(selectedRow);
                                            CalculateGrandTotal('');
                                            return true;
                                        },
                                        function () {
                                        }
                                    );
                                }
                                else {
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal('');
                                    return true;
                                }
                            }
                        },
                        //{
                        //    name: 'View APL',
                        //    click: function (e) {

                        //        var gridObj = $("#gridMOG").data("kendoGrid");
                        //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                        //        datamodel = tr;
                        //        console.log("MOG APL");
                        //        console.log(tr);
                        //        MOGName = tr.MOGName;

                        //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                        //        $("#windowEditMOGWiseAPL").kendoWindow({
                        //            animation: false
                        //        });
                        //        $(".k-overlay").css("display", "block");
                        //        $(".k-overlay").css("opacity", "0.5");
                        //        dialog.open();
                        //        dialog.center();
                        //        dialog.title("MOG APL Details - " + MOGName);
                        //        MOGCode = tr.MOGCode;
                        //        mogWiseAPLMasterdataSource = [];
                        //        getMOGWiseAPLMasterData();
                        //        populateAPLMasterGrid();
                        //        return true;
                        //    }
                        //}
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                if (user.SectorNumber == "20") {
                                    $.each(obj, function (key, value) {
                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.DKgValue * value.Quantity);
                                    });
                                }
                                else {
                                    $.each(obj, function (key, value) {
                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                    });
                                }

                                $("#totmog").text(mogtotal.toFixed(2));
                                var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                $("#ToTIngredients").text(Utility.MathRound(tot, 2));
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {
                                recipeCode: recipeCode
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "MOG_ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            Quantity: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {

                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        index: 0,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });


            },
            change: function (e) {
                if (e.action == "itemchange") {
                    e.items[0].dirtyFields = e.items[0].dirtyFields || {};
                    e.items[0].dirtyFields[e.field] = true;
                }
            },
        });
    }

    // cancelChangesConfirmation('gridMOG');
    var toolbar = $("#gridMOG").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG .k-grid-content").addClass("gridInside");
    toolbar.find(".k-grid-clearall").addClass("gridButton");
    $('#gridMOG a.k-grid-clearall').click(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                $("#gridMOG").data('kendoGrid').dataSource.data([]);
                showHideGrid('gridMOG', 'emptymog', 'mup_gridBaseRecipe');
                gridIdin = 'gridMOG';
            },
            function () {
            }
        );

    });

    $("#gridMOG .k-grid-cancel-changes").click(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                // var grid = $("#" + gridIdin + "").data("kendoGrid");
                if (gridIdin == 'gridBaseRecipe') {
                    populateBaseRecipeGrid(resetID);
                    return;
                } else if (gridIdin == 'gridMOG') {
                    populateMOGGrid(resetID)
                    return;
                }
                // grid.cancelChanges();
            },
            function () {
            }
        );

    });
}

function refreshMOGDropDownData(level, x) {

    if (mogdataList != null && mogdataList.length > 0) {
        var mogGridData = $("#gridMOG" + level + "").data('kendoGrid').dataSource._data;

        if (mogGridData != null && mogGridData.length > 0) {
            mogGridData = mogGridData.filter(m => m.MOGName != "" && m.MOGName != x);
            mogdata = mogdataList;
            var mogdataListtemp = mogdataList;
            mogdataListtemp = mogdataListtemp.filter(m => m.MOG_Code != "");
            mogdataListtemp.forEach(function (item, index) {
                for (var i = 0; i < mogGridData.length; i++) {
                    if (mogGridData[i].MOGCode !== "" && item.MOG_Code === mogGridData[i].MOGCode) {
                        // mogdata.splice(index, 1);
                        mogdata = mogdata.filter(mog => mog.MOG_Code !== item.MOG_Code);
                        break;
                    }
                }
                return true;
            });
        }
    }
    return mogdata.filter(m => m.IsActive == true);
};

function refreshMOGUOMDropDownData(x) {
    return moguomdata.filter(m => m.text != x);
};


function populateDishDropdown() {
    $("#inputdish").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: dishdata,
        index: 0,
    });
}

function populateSubSectorDropdown(id) {
    $("#inputsubsector" + id + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "SubSectorCode",
        dataSource: subsectordata,
        index: 0,
    });
}

function populateUOMDropdown_Bulk() {
    $("#uom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}

function populateUOMDropdown() {

    $("#inputuom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}

function populateMOGUOMDropdown() {

    $("#inputmoguom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: moguomdata,
        index: 0,
    });
}

function populateBaseDropdown_Bulk() {
    $("#base").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateBaseDropdown() {
    $("#inputbase").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}

function onEditRecipeGrid(tr, isSavedData) {

    datamodel = tr;
    RecipeName = tr.Name;
    populateUOMDropdown();
    recipeCategoryHTMLPopulateOnEditRecipe("", tr);
    isBaseRecipe = tr.IsBase;
    RecipeCode = tr.RecipeCode;
    if (isSavedData) {
        $("#windowEdit0").hide();
    }
    else {
        $("#mainContent").hide();
    }
    $("#windowEdit").show();


    populateBaseDropdown();
    resetID = tr.ID;
    populateBaseRecipeGrid(tr.RecipeCode);
    populateMOGGrid(tr.RecipeCode);
    $("#hiddenRecipeID").val(tr.RecipeCode);
    $("#recipeid").val(tr.ID);
    $("#inputrecipecode").val(tr.RecipeCode);
    $("#inputrecipename").val(tr.Name);
    $("#inputrecipealiasname").val(tr.RecipeAlias);
    $("#copyRecipe").data("kendoDropDownList").value(0);
    $("#inputuom").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity").val(tr.Quantity);
    if (user.SectorNumber == "20") {
        $("#totmog").show();
        $(".totmoglbl").show();
        $('#inputquantity').removeAttr("disabled");
        $("#ToTIngredientslbl").text("Total Qty of Ingredients");
        // $("#inputbase").data('kendoDropDownList').value("No");
        if (user.SectorNumber == "20") {
            $("#totmog").hide();
            $(".totmoglbl").hide();
            $("#ToTIngredientslbl").text("Total Qty of Ingredients (DKG) : ");
        }
    }
    else if (user.SectorNumber == "10") {
        $("#ToTIngredientslbl").text("Total Qty of Ingredients");
        $("#totmog").show();
        $(".totmoglbl").hide();
        var inputwtperunituom = $("#inputuom").data("kendoDropDownList");
        inputwtperunituom.enable(false);
        $('#inputquantity').attr('disabled', 'disabled');
        $("#inputsubsector").data('kendoDropDownList').value("Select Sub Sector");
        if (tr.SubSectorCode)
            $("#inputsubsector").data('kendoDropDownList').value(tr.SubSectorCode);
    }
    else {
        $("#ToTIngredientslbl").text("Total Qty of Ingredients");
        $("#totmog").show();
        $(".totmoglbl").hide();
        var inputwtperunituom = $("#inputuom").data("kendoDropDownList");
        inputwtperunituom.enable(false);
        $('#inputquantity').attr('disabled', 'disabled');
    }
    if (tr.IsBase == true)
        $("#inputbase").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase").data('kendoDropDownList').value("No");
    $('#grandTotal').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    var editor = $("#inputinstruction").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
}

function recipeCategoryHTMLPopulateOnEditRecipe(level, tr) {
    if (user.SectorNumber == "20") {
        populateRecipeCategoryUOMDropdown(level);
        populateWtPerUnitUOMDropdown(level);
        populateGravyWtPerUnitUOMDropdown(level);
        if (tr.RecipeUOMCategory)
            $("#inputrecipecum" + level + "").data('kendoDropDownList').value(tr.RecipeUOMCategory);
        var recipeCategory = tr.RecipeUOMCategory;
        if (tr.RecipeUOMCategory == undefined || tr.RecipeUOMCategory == null || tr.RecipeUOMCategory == "") {
            recipeCategory = "UOM-00001";
            $("#inputrecipecum" + level + "").data('kendoDropDownList').value("UOM-00001");
        }
        OnRecipeUOMCategoryChange(recipeCategory, level);
        if (tr.WeightPerUnit)
            $("#inputwtperunit" + level + "").val(tr.WeightPerUnit);
        if (tr.WeightPerUnitGravy)
            $("#inputgravywtperunit" + level + "").val(tr.WeightPerUnitGravy);
        if (tr.WeightPerUnitUOM)
            $("#inputwtperunituom" + level + "").data("kendoDropDownList").value(tr.WeightPerUnitUOM);
        if (tr.WeightPerUnitUOMGravy)
            $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value(tr.WeightPerUnitUOMGravy);
    }
}



function onSaveRecipeSetData() {
    var gridObj = $("#gridRecipe").data("kendoGrid");
    if (gridObj.dataSource != undefined && gridObj.dataSource._total > 0) {
        var recipeName = titleCase($("#inputrecipename0").val());
        var tr1 = gridObj.dataSource._data.filter(m => m.Name == recipeName);
        //var tr = gridObj.dataSource._data[parseInt(gridObj.dataSource._total) - 1];
        onEditRecipeGrid(tr1[0], true);
    }
    return true;
}
//Hare Ram
function onBRDDLChange(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    //baserecipedatafiltered = baserecipedata.filter(function (recipeObj) {
    //    return recipeObj.BaseRecipe_ID != e.sender.value();
    //});

    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    // $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onBRDDLChange0(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe0', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe0").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(0);

};

function onBRDDLChangex(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipex', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipex").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipex").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('x');

};


function onBRDDLChange1(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe1', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe1").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(1);;
};
function onBRDDLChange2(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe2', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe2").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(2);
};

function onBRDDLChange3(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe3', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe3").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(3);
};

function onBRDDLChange4(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe4', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe4").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(4);
};

function onBRDDLChange5(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe5', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe5").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('');
};


function populateBaseRecipeGridCopy(recipeCode) {

    if (recipeCode == null)
        recipeCode ="";
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe0");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [
            //    { name: "cancel", text: "Reset" }, {
            //    name: "create", text: "Add"
            //},
            //{
            //    name: "new", text: "Create New"
            //},

        ],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails1(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input tabindex="0" id="ddlBaseRecipe0" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange0,
                            //open: function (e) { e.preventDefault();}
                        });
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,0)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    {
                        name: 'Delete',
                        text: "Delete",

                        click: function (e) {
                            if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                return false;
                            }
                            var gridObj = $("#gridBaseRecipe0").data("kendoGrid");
                            var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            gridObj.dataSource._data.remove(selectedRow);
                            CalculateGrandTotal(0);
                            return true;
                        }
                    }
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe0").css("display", "block");
                            $("#emptybr0").css("display", "none");
                            CalculateGrandTotal(0);
                        } else {
                            $("#gridBaseRecipe0").css("display", "none");
                            $("#emptybr0").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe0');
    var toolbar = $("#gridBaseRecipe1").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe1 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");
        $("#windowEditx").kendoWindow({
            animation: false
        });
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");

        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}
function populateBaseRecipeGrid0() {

    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe0");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [
            { name: "clearall", text: "Clear All" },
            { name: "cancel", text: "Reset" }, {
                name: "create", text: "Add"
            }
        ],
        groupable: false,
        editable: true,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails1(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input tabindex="0" id="ddlBaseRecipe0" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange0,
                            //open: function (e) { e.preventDefault();}
                        });
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,0)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    {
                        name: 'Delete',
                        text: "Delete",

                        click: function (e) {
                            if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                return false;
                            }
                            var gridObj = $("#gridBaseRecipe0").data("kendoGrid");
                            var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            gridObj.dataSource._data.remove(selectedRow);
                            CalculateGrandTotal(0);
                            return true;
                        }
                    }
                ],
            }
        ],
        dataSource: {

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false }, IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },

                        TotalCost: { editable: false }
                    }
                }
            }
        },

        columnResize: function (e) {

            e.preventDefault();
        },

        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
                $(this).next().focus();
            });



        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe0');
    var toolbar = $("#gridBaseRecipe0").find(".k-grid-toolbar");

    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe0 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
    toolbar.find(".k-grid-clearall").addClass("gridButton");
    $('#gridBaseRecipe0 a.k-grid-clearall').click(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                $("#gridBaseRecipe0").data('kendoGrid').dataSource.data([]);
                showHideGrid('gridBaseRecipe0', 'emptybr0', 'bup_gridBaseRecipe0');
            },
            function () {
            }
        );

    });
}
function populateBaseRecipeGridx() {

    Utility.Loading();
    var gridVariable = $("#gridBaseRecipex");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, {
            name: "create", text: "Add"
        },

        ],
        groupable: false,
        editable: true,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetailsx(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input tabindex="0" id="ddlBaseRecipex" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChangex,
                            //open: function (e) { e.preventDefault();}
                        });
                }
            },

            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                template: '<input type="number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,' + 'x' + ')"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    {
                        name: 'Delete',
                        text: "Delete",

                        click: function (e) {
                            if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                return false;
                            }
                            var gridObj = $("#gridBaseRecipex").data("kendoGrid");
                            var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            gridObj.dataSource._data.remove(selectedRow);
                            CalculateGrandTotal('x');
                            return true;
                        }
                    }
                ],
            }
        ],
        dataSource: {

            schema: {
                model: {
                    id: "ID",

                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }

                }
            }
        },

        columnResize: function (e) {

            e.preventDefault();
        },

        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChangex
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
                $(this).next().focus();
            });

            $('.k-grid-add').unbind("click");

            $('.k-grid-add').bind("click", function (e) {
                // var di = grid.dataItem(e);
                console.log(e);
                console.log(items);
                console.log("Handle the add button click")
            });

        },
        change: function (e) {
        },
    });

    cancelChangesConfirmation('gridBaseRecipex');
    var toolbar = $("#gridBaseRecipex").find(".k-grid-toolbar");

    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipex .k-grid-content").addClass("gridInside");
}

function onMOGDDLChange0(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG0', false)) {
        toastr.error("MOG is already selected.");
        var dropdownlist = $("#ddlMOG0").data("kendoDropDownList");
        dropdownlist.select("");
        //  dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    var dkgtotal = 0;
    if (user.SectorNumber == '20') {
        dkgtotal = e.sender.dataItem().DKgValue * dataItem.Quantity;
        dataItem.set("DKgValue", dkgtotal);
        dataItem.DKgValue = e.sender.dataItem().DKgValue;
        $(element).closest('tr').find('.dkgtotal')[0].innerHTML = dkgtotal;
        dataItem.TotalCost = dataItem.CostPerUOM * dkgtotal;
    }
    else {
        dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    }
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(0);

};
function onMOGDDLChangex(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOGx', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOGx").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOGx").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('x');
};
function populateUOMDropdown0() {
    $("#inputuom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 1
    });
}

function populateWtPerUnitUOMDropdown0() {
    $("#inputwtperunituom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}

function populateGravyWtPerUnitUOMDropdown0() {
    $("#inputgravywtperunituom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}

function populateRecipeCategoryUOMDropdown0() {
    $("#inputrecipecum0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: recipecategoryuomdata,
        index: 1,
        change: function (e) {
            OnRecipeUOMCategoryChange(e.sender.dataItem().UOMCode, "0")

        }
    });
}

function populateWtPerUnitUOMDropdown(level) {
    $("#inputwtperunituom" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}

function populateGravyWtPerUnitUOMDropdown(level) {
    $("#inputgravywtperunituom" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}

function populateRecipeCategoryUOMDropdown(level) {
    $("#inputrecipecum" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: recipecategoryuomdata,
        index: 1,
        change: function (e) {
            OnRecipeUOMCategoryChange(e.sender.dataItem().UOMCode, level)

        }
    });
}

function OnRecipeUOMCategoryChange(recipeCategory, level) {
    $("#inputwtperunituom" + level + "").closest(".k-widget").hide();
    $("#inputgravywtperunituom" + level + "").closest(".k-widget").hide();
    $("#inputuom" + level + "").closest(".k-widget").hide();
    $("#linputwtperunituom" + level + "").text("-");
    $("#linputgravywtperunituom" + level + "").text("-");
    $("#linputuom" + level + "").text("-");

    $("#inputwtperunit" + level + "").val("");

    if ($("#inputuom" + level + "").data("kendoDropDownList") != undefined) {
        $("#inputuom" + level + "").data("kendoDropDownList").value("Select");
    }
    $("#inputgravywtperunit" + level + "").val("");
    $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("Select");
    $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("Select");
    var inputwtperunituom0 = $("#inputwtperunituom" + level + "").data("kendoDropDownList");
    inputwtperunituom0.enable(false);
    var inputgravywtperunituom0 = $("#inputgravywtperunituom" + level + "").data("kendoDropDownList");
    inputgravywtperunituom0.enable(false);

    if (recipeCategory == "UOM-00005" || recipeCategory == "UOM-00006") {
        var rUOM = recipeCategory == "UOM-00005" ? "1" : "2";
        var rUOMText = uomdata.find(m => m.UOM_ID == rUOM).UOMName;
        $("#inputwtperunit" + level + "").prop("disabled", true);
        $("#inputgravywtperunit" + level + "").prop("disabled", true);
        $("#inputuom" + level + "").data("kendoDropDownList").value(rUOM);
        $("#linputuom" + level + "").text(rUOMText);
        var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputwtperunituom" + level + "").text("-");
        $("#linputgravywtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00007") {
        $("#inputwtperunit" + level + "").prop("disabled", false);
        $("#inputgravywtperunit" + level + "").prop("disabled", true);
        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
        var rUOMText = uomdata.find(m => m.UOM_ID == 3).UOMName;
        $("#linputuom" + level + "").text(rUOMText);
        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").UOMName;
        $("#linputwtperunituom" + level + "").text(rUText);
        var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputgravywtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00009") {
        $("#inputwtperunit" + level + "").prop("disabled", true);
        $("#inputgravywtperunit" + level + "").prop("disabled", false);
        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
        var rUOMText = uomdata.find(m => m.UOM_ID == 3).UOMName;
        $("#linputuom" + level + "").text(rUOMText);
        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").UOMName;
        $("#linputgravywtperunituom" + level + "").text(rUText);
        var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputwtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00008") {
        $("#inputwtperunit" + level + "").prop("disabled", false);
        $("#inputgravywtperunit" + level + "").prop("disabled", false);
        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").UOMName;
        $("#linputgravywtperunituom" + level + "").text(rUText);
        $("#linputwtperunituom" + level + "").text(rUText);
        var rUOMText = uomdata.find(m => m.UOM_ID == 3).UOMName;
        $("#linputuom" + level + "").text(rUOMText);
    }
};

function populateUOMDropdownx() {
    $("#inputuomx").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 1,
    });
}
function populateBaseDropdown0() {
    $("#inputbase0").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateBaseDropdownx() {
    $("#inputbasex").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}


function calculateItemTotal0(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObjforBR = $("#gridBaseRecipe0.totalcost");
    var gtotalObjforMOG = $("#gridBaseRecipeMOG0.totalcost");
    var len = $('#gridBaseRecipe0.totalcost').length;
    var len1 = $('#gridBaseRecipeMOG0.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObjforBR[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    for (i = 0; i < len1; i++) {
        if (gtotalObjforMOG[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal0').html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#inputquantity0').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG0').html(Utility.AddCurrencySymbol(costPerKG, 2));
}
function mogtotalQTYDelete(level) {

    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var gTotal = 0.0;
    var len1 = grid._data.length;
    for (i = 0; i < len1; i++) {
        var totalCostMOG = user.SectorNumber == "20" ? grid._data[i].DKgValue * grid._data[i].Quantity : grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {
        $("#totmog").text(Utility.MathRound(gTotal, 2));
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
        if (level == "" || levle == "0") {
            $("#ToTIngredients" + level).text(Utility.MathRound(tot, 2));
        }
    }

}

function BaseRecipetotalQTYDelete(level) {

    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");

    var gTotal = 0.0;
    var len1 = grid._data.length;

    for (i = 0; i < len1; i++) {
        var totalCostMOG = user.SectorNumber == "20" ? grid._data[i].DKgValue * grid._data[i].Quantity : grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {

        $("#totBaseRecpQty").text(Utility.MathRound(gTotal, 2));
        // parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))).toFixed(2);
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))
        if (level == "" || levle == "0") {
            $("#ToTIngredients" + level).text(Utility.MathRound(tot, 2));
        }
    }


}
function CalculateGrandTotal(level) {
    var gtotalObjforBR = $("#gridBaseRecipe" + level + " .totalcost");
    var gtotalObjforMOG = $("#gridMOG" + level + " .totalcost");
    var len = gtotalObjforBR.length;
    var len1 = gtotalObjforMOG.length;
    var gTotal = 0.0;

    for (i = 0; i < len; i++) {
        var totalCostBR = gtotalObjforBR[i].innerText;
        totalCostBR = totalCostBR.substring(1).replace(',', '');
        if (totalCostBR == "") {
            continue;
        }
        gTotal += Number(totalCostBR);
    }
    for (i = 0; i < len1; i++) {
        var totalCostMOG = gtotalObjforMOG[i].innerText;
        totalCostMOG = totalCostMOG.substring(1).replace(',', '');
        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {
        $("#grandTotal" + level).html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#inputquantity' + level).val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG' + level).html(Utility.AddCurrencySymbol(costPerKG, 2));
    mogtotalQTYDelete(level);
    BaseRecipetotalQTYDelete(level);
}

function calculateItemTotalx(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipex").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotalx').html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#inputquantityx').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKGx').html(Utility.AddCurrencySymbol(costPerKG, 2));

}


function calculateItemTotalMOG0(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal0').html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#inputquantity0').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG0').html(Utility.AddCurrencySymbol(costPerKG, 2));
}

function calculateItemTotalMOGx(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOGx").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotalx').html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#inputquantityx').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKGx').html(Utility.AddCurrencySymbol(costPerKG, 2));
}



function populateBaseRecipeGrid5(recipeCode) {

    if (recipeCode == null)
        recipeCode ="";
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe5");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [
            //{ name: "cancel", text: "Reset" }, {
            //     name: "create", text: "Add"
            //  },
            {
                name: "new", text: "Create New"
            },

        ],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails6(this)'></i>`;
                    return html;
                },
                //editor: function (container, options) {
                //    $('<input tabindex="0" id="ddlBaseRecipe5" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 1,
                //            filter: "contains",
                //            dataTextField: "BaseRecipeName",
                //            dataValueField: "BaseRecipe_ID",
                //            autoBind: true,
                //            dataSource: baserecipedata,
                //            value: options.field,
                //            change: onBRDDLChange5,
                //            //open: function (e) { e.preventDefault();}
                //        });
                //}
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                //    template: '<input type="number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,5)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;"
            //    },
            //    command: [
            //        {
            //            name: 'Delete',
            //            text: "Delete",

            //            click: function (e) {
            //                if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //                    return false;
            //                }
            //                var gridObj = $("#gridBaseRecipe5").data("kendoGrid");
            //                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                gridObj.dataSource._data.remove(selectedRow);
            //                CalculateGrandTotal(5);
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe5").css("display", "block");
                            $("#emptybr5").css("display", "none");
                        } else {
                            $("#gridBaseRecipe5").css("display", "none");
                            $("#emptybr5").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange5
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe5');
    var toolbar = $("#gridBaseRecipe5").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe5 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}

function populateBaseRecipeGrid4(recipeCode) {

    if (recipeCode == null)
        recipeCode ="";
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe4");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" }, {
        //    name: "create", text: "Add"
        //},
        //{
        //    name: "new", text: "Create New"
        //},

        //],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails5(this)'></i>`;
                    return html;
                },
                //editor: function (container, options) {
                //    $('<input tabindex="0" id="ddlBaseRecipe4" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 1,
                //            filter: "contains",
                //            dataTextField: "BaseRecipeName",
                //            dataValueField: "BaseRecipe_ID",
                //            autoBind: true,
                //            dataSource: baserecipedata,
                //            value: options.field,
                //            change: onBRDDLChange4,
                //            //open: function (e) { e.preventDefault();}
                //        });
                //    }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                //       template: '<input type="number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,4)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;"
            //    },
            //    command: [
            //        {
            //            name: 'Delete',
            //            text: "Delete",

            //            click: function (e) {
            //                if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //                    return false;
            //                }
            //                var gridObj = $("#gridBaseRecipe4").data("kendoGrid");
            //                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                gridObj.dataSource._data.remove(selectedRow);
            //                CalculateGrandTotal(4);
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe4").css("display", "block");
                            $("#emptybr4").css("display", "none");
                        } else {
                            $("#gridBaseRecipe4").css("display", "none");
                            $("#emptybr4").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false }, IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange4
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe4');
    var toolbar = $("#gridBaseRecipe4").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe4 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}

function populateBaseRecipeGrid3(recipeCode) {

    if (recipeCode == null)
        recipeCode ="";
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe3");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" }, {
        //    name: "create", text: "Add"
        //},
        //{
        //    name: "new", text: "Create New"
        //},

        //],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails4(this)'></i>`;
                    return html;
                },
                //editor: function (container, options) {
                //    $('<input tabindex="0" id="ddlBaseRecipe3" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 1,
                //            filter: "contains",
                //            dataTextField: "BaseRecipeName",
                //            dataValueField: "BaseRecipe_ID",
                //            autoBind: true,
                //            dataSource: baserecipedata,
                //            value: options.field,
                //            change: onBRDDLChange3,
                //            //open: function (e) { e.preventDefault();}
                //        });
                //}
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                //   template: '<input type="number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,3)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },


            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;"
            //    },
            //    command: [
            //        {
            //            name: 'Delete',
            //            text: "Delete",

            //            click: function (e) {
            //                if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //                    return false;
            //                }
            //                var gridObj = $("#gridBaseRecipe3").data("kendoGrid");
            //                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                gridObj.dataSource._data.remove(selectedRow);
            //                CalculateGrandTotal(3);
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe3").css("display", "block");
                            $("#emptybr3").css("display", "none");
                        } else {
                            $("#gridBaseRecipe3").css("display", "none");
                            $("#emptybr3").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false }, IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange2
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe3');
    var toolbar = $("#gridBaseRecipe3").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe3 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}

function populateBaseRecipeGrid2(recipeCode) {

    if (recipeCode == null)
        recipeCode ="";
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe2");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" },
        ////    {
        ////    name: "create", text: "Add"
        ////},
        //{
        //    name: "new", text: "Create New"
        //},

        //],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails3(this)'></i>`;
                    return html;
                },
                //editor: function (container, options) {
                //    $('<input tabindex="0" id="ddlBaseRecipe2" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 1,
                //            filter: "contains",
                //            dataTextField: "BaseRecipeName",
                //            dataValueField: "BaseRecipe_ID",
                //            autoBind: true,
                //            dataSource: baserecipedata,
                //            value: options.field,
                //            change: onBRDDLChange2,
                //            //open: function (e) { e.preventDefault();}
                //        });
                //    }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                // template: '<input type="Number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,2)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;display:none;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;display:none"
            //    },
            //    command: [
            //        {
            //            name: 'Delete',
            //            text: "Delete",

            //            click: function (e) {
            //                if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //                    return false;
            //                }
            //                var gridObj = $("#gridBaseRecipe2").data("kendoGrid");
            //                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                gridObj.dataSource._data.remove(selectedRow);
            //                CalculateGrandTotal(2);
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe2").css("display", "block");
                            $("#emptybr2").css("display", "none");
                        } else {
                            $("#gridBaseRecipe2").css("display", "none");
                            $("#emptybr2").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false }, IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange2
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe2');
    var toolbar = $("#gridBaseRecipe2").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe2 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}

function populateBaseRecipeGrid1(recipeCode) {

    if (recipeCode == null)
        recipeCode = "";
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe1");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [
            //    { name: "cancel", text: "Reset" }, {
            //    name: "create", text: "Add"
            //},
            //{
            //    name: "new", text: "Create New"
            //},

        ],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails2(this)'></i>`;
                    return html;
                },
                //editor: function (container, options) {
                //    $('<input tabindex="0" id="ddlBaseRecipe1" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 1,
                //            filter: "contains",
                //            dataTextField: "BaseRecipeName",
                //            dataValueField: "BaseRecipe_ID",
                //            autoBind: true,
                //            dataSource: baserecipedata,
                //            value: options.field,
                //            change: onBRDDLChange1,
                //            //open: function (e) { e.preventDefault();}
                //        });
                // }
            },

            {
                field: "UOMName", title: "UOM", width: "40px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "30px",
                //  template: '<input type="number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,1)" readonly/>',

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "IngredientPerc", title: "Major %", width: "40px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "40px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;"
            //    },
            //    command: [
            //        {
            //            name: 'Delete',
            //            text: "Delete",

            //            click: function (e) {
            //                if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //                    return false;
            //                }
            //                var gridObj = $("#gridBaseRecipe1").data("kendoGrid");
            //                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                gridObj.dataSource._data.remove(selectedRow);
            //                CalculateGrandTotal(1);
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe1").css("display", "block");
                            $("#emptybr1").css("display", "none");
                        } else {
                            $("#gridBaseRecipe1").css("display", "none");
                            $("#emptybr1").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe1');
    var toolbar = $("#gridBaseRecipe1").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe1 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");
        $("#windowEditx").kendoWindow({
            animation: false
        });
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}


function onMOGDDLChange5(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG5', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG5").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(5);
};
function onMOGDDLChange4(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG4', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG4").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(4)
};
function onMOGDDLChange3(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG3', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG3").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(3)
};
function onMOGDDLChange2(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG2', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG2").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(2)
};
function onMOGDDLChange1(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG1', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG1").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(1);
};

function populateMOGGrid0() {

    Utility.Loading();
    var gridVariable = $("#gridMOG0");
    gridVariable.html("");
    if (user.SectorNumber == "20") {
        gridVariable.kendoGrid({
            toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,

            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = ` <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {

                        $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: mogdata,
                                value: options.field,
                                change: onMOGDDLChange0,
                            });

                    }
                },

                {
                    field: "UOMCode", title: "UOM", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal",
                        class: ''
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="uomname" style="margin-left:8px">' + kendo.htmlEncode(dataItem.UOMName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOGUOM0" text="' + options.model.UOMName + '" data-text-field="text" data-value-field="UOMCode" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "text",
                                dataValueField: "UOMCode",
                                autoBind: true,
                                dataSource: moguomdata,
                                // dataSource: refreshMOGUOMDropDownData(options.model.UOMName),
                                value: options.field,
                                change: onMOGUOMDDLChange0,
                            });

                    }
                },

                {
                    field: "Quantity", title: "Quantity", width: "35px",
                    headerAttributes: {
                        style: "text-align: right;"
                    },

                    template: '<input type="number" min="0" name="Quantity" class="inputmogqty"   oninput="calculateItemTotalMOG(this,0)"/>',
                    attributes: {

                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "DKgTotal", title: "DKG", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },
                    attributes: {
                        class: "dkgtotal",
                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "uomcost",

                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",

                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridMOG0").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                gridObj.dataSource._data.remove(selectedRow);
                                CalculateGrandTotal(0);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false },
                            DKgValue: { editable: false },
                            DKgTotal: { editable: false },
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();

                items.each(function (e) {

                    var dataItem = grid.dataItem(this);

                    var ddt = $(this).find('.mogTemplate');

                    $(ddt).kendoDropDownList({
                        index: 0,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: mogdata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange0
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
    }
    else {
        gridVariable.kendoGrid({
            toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,

            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {

                        $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: mogdata,
                                value: options.field,
                                change: onMOGDDLChange0,
                            });

                    }
                },

                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {

                        class: 'uomname',
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "Quantity", title: "Quantity", width: "35px",
                    headerAttributes: {
                        style: "text-align: right;"
                    },

                    template: '<input type="number" min="0" name="Quantity" class="inputmogqty"   oninput="calculateItemTotalMOG(this,0)"/>',
                    attributes: {

                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "IngredientPerc", title: "Major %", width: "50px", format: "{0:0.000}", editable: false,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "major",
                        style: "text-align: right; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
                },
                {
                    field: "IngredientPerc", title: "Minor %", width: "50px", format: "{0:0.000}", editable: false,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "minor",
                        style: "text-align: right; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "uomcost",

                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",

                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridMOG0").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                gridObj.dataSource._data.remove(selectedRow);
                                CalculateGrandTotal(0);
                                return true;
                            }
                        },
                        //{
                        //    name: 'View APL',
                        //    click: function (e) {

                        //        var gridObj = $("#gridMOG0").data("kendoGrid");
                        //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                        //        datamodel = tr;
                        //        MOGName = tr.MOGName;
                        //        if (MOGName == "") {
                        //            return false;
                        //        }
                        //        else {
                        //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                        //            //$("#windowEditMOGWiseAPL").kendoWindow({
                        //            //    animation: false
                        //            //});
                        //            $(".k-overlay").css("display", "block");
                        //            $(".k-overlay").css("opacity", "0.5");
                        //            dialog.open();
                        //            dialog.center();

                        //            dialog.title("MOG APL Details - " + MOGName);
                        //            MOGCode = tr.MOGCode;
                        //            mogWiseAPLMasterdataSource = [];
                        //            getMOGWiseAPLMasterData();
                        //            populateAPLMasterGrid();
                        //            return true;
                        //        }
                        //    }
                        //}
                    ],
                }
            ],
            dataSource: {
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();

                items.each(function (e) {

                    var dataItem = grid.dataItem(this);

                    var ddt = $(this).find('.mogTemplate');

                    $(ddt).kendoDropDownList({
                        index: 0,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: mogdata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange0
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
    }

    cancelChangesConfirmation('gridMOG0');
    var toolbar = $("#gridMOG0").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG0 .k-grid-content").addClass("gridInside");
    $("#gridMOG0 .k-grid-clearall").addClass("gridInside");
    toolbar.find(".k-grid-clearall").addClass("gridButton");
    $('#gridMOG0 a.k-grid-clearall').click(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                $("#gridMOG0").data('kendoGrid').dataSource.data([]);
                showHideGrid('gridMOG0', 'emptymog0', 'mup_gridBaseRecipe0');
            },
            function () {
            }
        );

    });
}

function populateMOGGridCopy(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG0");
    gridVariable.html("");
    if (user.SectorNumber == '20') {

        gridVariable.kendoGrid({
            toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,
            //navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: refreshMOGDropDownData("0", options.model.MOGName),
                                value: options.field,
                                change: onMOGDDLChange0,
                            });

                    }
                },
                //{
                //    field: "UOMName", title: "UOM", width: "50px",
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {

                //        class: 'uomname',
                //        style: "text-align: center; font-weight:normal"
                //    },
                //},
                {
                    field: "UOMCode", title: "UOM", width: "40px",
                    attributes: {
                        style: "text-align: left; font-weight:normal",
                        class: ''
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="uomname" style="margin-left:8px">' + kendo.htmlEncode(dataItem.UOMName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOGUOM0" text="' + options.model.UOMName + '" data-text-field="text" data-value-field="UOMCode" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "text",
                                dataValueField: "UOMCode",
                                autoBind: true,
                                dataSource: moguomdata,
                                // dataSource: refreshMOGUOMDropDownData(options.model.UOMName),
                                value: options.field,
                                change: onMOGUOMDDLChange0,
                            });

                    }
                },

                {
                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },

                    template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,0)"/>',
                    attributes: {

                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "DKgTotal", title: "DKG", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },
                    attributes: {
                        class: "dkgtotal",
                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "uomcost",

                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }

                                var gridObj = $("#gridMOG0").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (selectedRow.IsMajor) {
                                    Utility.Page_Alert_Save("This is a major Ingredient. Do you still want to remove it from the Recipe?", "Delete Confirmation", "Yes", "No",
                                        function () {
                                            gridObj.dataSource._data.remove(selectedRow);
                                            CalculateGrandTotal(0);
                                            return true;
                                        },
                                        function () {
                                        }
                                    );
                                }
                                else {
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal(0);
                                    return true;
                                }

                            }
                        },
                        //{
                        //    name: 'View APL',
                        //    click: function (e) {

                        //        var gridObj = $("#gridMOG0").data("kendoGrid");
                        //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                        //        datamodel = tr;
                        //        console.log("MOG APL");
                        //        console.log(tr);
                        //        MOGName = tr.MOGName;

                        //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                        //        $("#windowEditMOGWiseAPL").kendoWindow({
                        //            animation: false
                        //        });
                        //        $(".k-overlay").css("display", "block");
                        //        $(".k-overlay").css("opacity", "0.5");
                        //        dialog.open();
                        //        dialog.center();
                        //        dialog.title("MOG APL Details - " + MOGName);
                        //        MOGCode = tr.MOGCode;
                        //        mogWiseAPLMasterdataSource = [];
                        //        getMOGWiseAPLMasterData();
                        //        populateAPLMasterGrid();
                        //        return true;
                        //    }
                        //}
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG0").css("display", "block");
                                $("#emptymog0").css("display", "none");
                            } else {
                                $("#gridMOG0").css("display", "none");
                                $("#emptymog0").css("display", "block");
                            }
                        },
                            {
                                recipeCode: recipeCode
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "MOG_ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            Quantity: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false },
                            DKgValue: { editable: false },
                            DKgTotal: { editable: false },

                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {

                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        index: 0,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange0
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });


            },
            change: function (e) {
                if (e.action == "itemchange") {
                    e.items[0].dirtyFields = e.items[0].dirtyFields || {};
                    e.items[0].dirtyFields[e.field] = true;
                }
            },
        });
    }
    else {
        gridVariable.kendoGrid({
            // toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,
            //navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: refreshMOGDropDownData("0", options.model.MOGName),
                                value: options.field,
                                change: onMOGDDLChange0,
                            });

                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {

                        class: 'uomname',
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },

                    template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,0)"/>',
                    attributes: {

                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "major",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
                },
                {
                    field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "minor",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "uomcost",

                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridMOG0").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (selectedRow.IsMajor) {
                                    Utility.Page_Alert_Save("This is a major Ingredient. Do you still want to remove it from the Recipe?", "Delete Confirmation", "Yes", "No",
                                        function () {
                                            gridObj.dataSource._data.remove(selectedRow);
                                            CalculateGrandTotal(0);
                                            return true;
                                        },
                                        function () {
                                        }
                                    );
                                }
                                else {
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal('');
                                    return true;
                                }
                            }
                        },
                        //{
                        //    name: 'View APL',
                        //    click: function (e) {

                        //        var gridObj = $("#gridMOG0").data("kendoGrid");
                        //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                        //        datamodel = tr;
                        //        console.log("MOG APL");
                        //        console.log(tr);
                        //        MOGName = tr.MOGName;

                        //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                        //        $("#windowEditMOGWiseAPL").kendoWindow({
                        //            animation: false
                        //        });
                        //        $(".k-overlay").css("display", "block");
                        //        $(".k-overlay").css("opacity", "0.5");
                        //        dialog.open();
                        //        dialog.center();
                        //        dialog.title("MOG APL Details - " + MOGName);
                        //        MOGCode = tr.MOGCode;
                        //        mogWiseAPLMasterdataSource = [];
                        //        getMOGWiseAPLMasterData();
                        //        populateAPLMasterGrid();
                        //        return true;
                        //    }
                        //}
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG0").css("display", "block");
                                $("#emptymog0").css("display", "none");
                                CalculateGrandTotal(0);
                            } else {
                                $("#gridMOG0").css("display", "none");
                                $("#emptymog0").css("display", "block");
                            }
                        },
                            {
                                recipeCode: recipeCode
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "MOG_ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            Quantity: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {

                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        index: 0,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange0
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });


            },
            change: function (e) {
                if (e.action == "itemchange") {
                    e.items[0].dirtyFields = e.items[0].dirtyFields || {};
                    e.items[0].dirtyFields[e.field] = true;
                }
            },
        });
    }

    // cancelChangesConfirmation('gridMOG0');
    var toolbar = $("#gridMOG0").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG0 .k-grid-content").addClass("gridInside");
    toolbar.find(".k-grid-clearall").addClass("gridButton");

    $("#gridMOG0").on("mousedown", ".k-grid-cancel-changes", function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                var grid = $("#gridMOG0").data("kendoGrid");
                grid.cancelChanges();
            },
            function () {
            }
        );
    });


    $('#gridMOG0 a.k-grid-clearall').click(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                $("#gridMOG0").data('kendoGrid').dataSource.data([]);
                showHideGrid('gridMOG0', 'emptymog0', 'mup_gridBaseRecipe0');
                gridIdin = 'gridMOG0';
            },
            function () {
            }
        );

    });


};



function populateMOGGridx() {

    Utility.Loading();
    var gridVariable = $("#gridMOGx");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: true,

        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOGx" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChangex,
                        });

                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                //editor: function (container, options) {
                //    $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" oninput="calculateItemTotalMOG(this,x)"/>')
                //        .appendTo(container)
                //        .kendoNumericTextBox({
                //            spinners: false,
                //            decimals: 4
                //        });
                //},
                template: '<input type="number" class="inputmogqty"  name="Quantity" oninput="calculateItemTotalMOG(this,' + 'x' + ')"/>',
                attributes: {

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    {
                        name: 'Delete',
                        text: "Delete",

                        click: function (e) {
                            if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                return false;
                            }
                            var gridObj = $("#gridMOGx").data("kendoGrid");
                            var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            gridObj.dataSource._data.remove(selectedRow);
                            CalculateGrandTotal('x');
                            return true;
                        }
                    },
                    //{
                    //    name: 'View APL',
                    //    click: function (e) {
                    //        var gridObj = $("#gridMOGx").data("kendoGrid");
                    //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        datamodel = tr;
                    //        MOGName = tr.MOGName;
                    //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                    //        $("#windowEditMOGWiseAPL").kendoWindow({
                    //            animation: false
                    //        });
                    //        $(".k-overlay").css("display", "block");
                    //        $(".k-overlay").css("opacity", "0.5");
                    //        dialog.open();
                    //        dialog.center();
                    //        dialog.title("MOG APL Details - " + MOGName);
                    //        MOGCode = tr.MOGCode;
                    //        mogWiseAPLMasterdataSource = [];
                    //        getMOGWiseAPLMasterData();
                    //        populateAPLMasterGrid();
                    //        return true;
                    //    }
                    //}
                ],
            }
        ],
        dataSource: {
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: mogdata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChangex
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridMOGx');
    var toolbar = $("#gridMOGx").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOGx .k-grid-content").addClass("gridInside");
}


function populateUOMDropdown1() {
    $("#inputuom1").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateBaseDropdown1() {
    $("#inputbase1").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateUOMDropdown2() {
    $("#inputuom2").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateBaseDropdown2() {
    $("#inputbase2").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateUOMDropdown3() {
    $("#inputuom3").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateBaseDropdown3() {
    $("#inputbase3").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateUOMDropdown4() {
    $("#inputuom4").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateBaseDropdown4() {
    $("#inputbase4").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateUOMDropdown5() {
    $("#inputuom5").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateBaseDropdown5() {
    $("#inputbase5").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}




function populateMOGGrid1(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG1");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" }, //{ name: "create", text: "Add" }
        //],
        groupable: false,
        editable: true,

        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG1" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange1,
                        });

                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },

                // template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    oninput="calculateItemTotalMOG(this,1)" readonly/>',
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    //{
                    //    name: 'Delete',
                    //    text: "Delete",

                    //    click: function (e) {
                    //        if ($(e.currentTarget).hasClass("k-state-disabled")) {
                    //            return false;
                    //        }
                    //        var gridObj = $("#gridMOG1").data("kendoGrid");
                    //        var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        gridObj.dataSource._data.remove(selectedRow);
                    //        CalculateGrandTotal(1);
                    //        return true;
                    //    }
                    //},
                    //{
                    //    name: 'View APL',
                    //    click: function (e) {
                    //        var gridObj = $("#gridMOG1").data("kendoGrid");
                    //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        datamodel = tr;
                    //        MOGName = tr.MOGName;
                    //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                    //        $("#windowEditMOGWiseAPL").kendoWindow({
                    //            animation: false
                    //        });
                    //        $(".k-overlay").css("display", "block");
                    //        $(".k-overlay").css("opacity", "0.5");
                    //        dialog.open();
                    //        dialog.center();
                    //        dialog.title("MOG APL Details - " + MOGName);
                    //        MOGCode = tr.MOGCode;
                    //        mogWiseAPLMasterdataSource = [];
                    //        getMOGWiseAPLMasterData();
                    //        populateAPLMasterGrid();
                    //        return true;
                    //    }
                    //}
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG1").css("display", "block");
                            $("#emptymog1").css("display", "none");
                        } else {
                            $("#gridMOG1").css("display", "none");
                            $("#emptymog1").css("display", "block");
                        }
                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange1
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridMOG1');
    var toolbar = $("#gridMOG1").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG1 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid2(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG2");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //   toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: false,

        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG2" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange2,
                        });

                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },

                //    template: '<input type="number"  class="inputmogqty"  name="Quantity" oninput="calculateItemTotalMOG(this,2) readonly"/>',
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;display:block;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;display:block"
                },
                command: [
                    //{
                    //    name: 'Delete',
                    //    text: "Delete",

                    //    click: function (e) {
                    //        if ($(e.currentTarget).hasClass("k-state-disabled")) {
                    //            return false;
                    //        }
                    //        var gridObj = $("#gridMOG2").data("kendoGrid");
                    //        var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        gridObj.dataSource._data.remove(selectedRow);
                    //        CalculateGrandTotal(2);
                    //        return true;
                    //    }
                    //},
                    //{
                    //    name: 'View APL',
                    //    click: function (e) {
                    //        var gridObj = $("#gridMOG2").data("kendoGrid");
                    //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        datamodel = tr;
                    //        MOGName = tr.MOGName;
                    //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                    //        $("#windowEditMOGWiseAPL").kendoWindow({
                    //            animation: false
                    //        });
                    //        $(".k-overlay").css("display", "block");
                    //        $(".k-overlay").css("opacity", "0.5");
                    //        dialog.open();
                    //        dialog.center();
                    //        dialog.title("MOG APL Details - " + MOGName);
                    //        MOGCode = tr.MOGCode;
                    //        mogWiseAPLMasterdataSource = [];
                    //        getMOGWiseAPLMasterData();
                    //        populateAPLMasterGrid();
                    //        return true;
                    //    }
                    //}
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG2").css("display", "block");
                            $("#emptymog2").css("display", "none");
                        } else {
                            $("#gridMOG2").css("display", "none");
                            $("#emptymog2").css("display", "block");
                        }
                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",

                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }

                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange2
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridMOG2');
    var toolbar = $("#gridMOG2").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG2 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid3(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG3");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //  toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: false,

        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG3" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange3,
                        });

                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },

                //   template: '<input type="number" class="inputmogqty"  name="Quantity" oninput="calculateItemTotalMOG(this,3) readonly"/>',
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    //{
                    //    name: 'Delete',
                    //    text: "Delete",
                    //    click: function (e) {
                    //        if ($(e.currentTarget).hasClass("k-state-disabled")) {
                    //            return false;
                    //        }
                    //        var gridObj = $("#gridMOG3").data("kendoGrid");
                    //        var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        gridObj.dataSource._data.remove(selectedRow);
                    //        CalculateGrandTotal(3);
                    //        return true;
                    //    }
                    //},
                    //{
                    //    name: 'View APL',
                    //    click: function (e) {
                    //        var gridObj = $("#gridMOG3").data("kendoGrid");
                    //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        datamodel = tr;
                    //        MOGName = tr.MOGName;
                    //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                    //        $("#windowEditMOGWiseAPL").kendoWindow({
                    //            animation: false
                    //        });
                    //        $(".k-overlay").css("display", "block");
                    //        $(".k-overlay").css("opacity", "0.5");
                    //        dialog.open();
                    //        dialog.center();
                    //        dialog.title("MOG APL Details - " + MOGName);
                    //        MOGCode = tr.MOGCode;
                    //        mogWiseAPLMasterdataSource = [];
                    //        getMOGWiseAPLMasterData();
                    //        populateAPLMasterGrid();
                    //        return true;
                    //    }
                    //}
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG3").css("display", "block");
                            $("#emptymog3").css("display", "none");
                        } else {
                            $("#gridMOG3").css("display", "none");
                            $("#emptymog3").css("display", "block");
                        }
                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false }, IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange3
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridMOG3');
    var toolbar = $("#gridMOG3").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG3 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid4(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG4");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" }
        //    //, { name: "create", text: "Add" }
        //],
        groupable: false,
        editable: false,

        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG4" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange4,
                        });

                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                //editor: function (container, options) {
                //    $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" oninput="calculateItemTotalMOG(this,4)"/>')
                //        .appendTo(container)
                //        .kendoNumericTextBox({
                //            spinners: false,
                //            decimals: 4
                //        });
                //},
                // template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    oninput="calculateItemTotalMOG(this)"/>',
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    //{
                    //    name: 'Delete',
                    //    text: "Delete",

                    //    click: function (e) {
                    //        if ($(e.currentTarget).hasClass("k-state-disabled")) {
                    //            return false;
                    //        }
                    //        var gridObj = $("#gridMOG4").data("kendoGrid");
                    //        var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        gridObj.dataSource._data.remove(selectedRow);
                    //        CalculateGrandTotal(4);
                    //        return true;
                    //    }
                    //},
                    //{
                    //    name: 'View APL',
                    //    click: function (e) {
                    //        var gridObj = $("#gridMOG4").data("kendoGrid");
                    //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        datamodel = tr;
                    //        MOGName = tr.MOGName;
                    //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                    //        $("#windowEditMOGWiseAPL").kendoWindow({
                    //            animation: false
                    //        });
                    //        $(".k-overlay").css("display", "block");
                    //        $(".k-overlay").css("opacity", "0.5");
                    //        dialog.open();
                    //        dialog.center();
                    //        dialog.title("MOG APL Details - " + MOGName);
                    //        MOGCode = tr.MOGCode;
                    //        mogWiseAPLMasterdataSource = [];
                    //        getMOGWiseAPLMasterData();
                    //        populateAPLMasterGrid();
                    //        return true;
                    //    }
                    //}
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG4").css("display", "block");
                            $("#emptymog4").css("display", "none");
                        } else {
                            $("#gridMOG4").css("display", "none");
                            $("#emptymog4").css("display", "block");
                        }
                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false }, IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange4
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridMOG4');
    var toolbar = $("#gridMOG4").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG4 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid5(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG5");
    gridVariable.html("");
    gridVariable.kendoGrid({
        // toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: false,

        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG5" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange5,
                        });

                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                //  editor: function (container, options) {
                //      $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" oninput="calculateItemTotalMOG(this,5)"/>')
                //           .appendTo(container)
                //          .kendoNumericTextBox({
                //              spinners: false,
                //             decimals: 4
                //         });
                //},
                // template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    oninput="calculateItemTotalMOG(this)"/>',
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    //{
                    //    name: 'Delete',
                    //    text: "Delete",

                    //    click: function (e) {
                    //        if ($(e.currentTarget).hasClass("k-state-disabled")) {
                    //            return false;
                    //        }
                    //        var gridObj = $("#gridMOG5").data("kendoGrid");
                    //        var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        gridObj.dataSource._data.remove(selectedRow);
                    //        CalculateGrandTotal(5);
                    //        return true;
                    //    }
                    //},
                    //{
                    //    name: 'View APL',
                    //    click: function (e) {
                    //        var gridObj = $("#gridMOG5").data("kendoGrid");
                    //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        datamodel = tr;
                    //        MOGName = tr.MOGName;
                    //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                    //        $("#windowEditMOGWiseAPL").kendoWindow({
                    //            animation: false
                    //        });
                    //        $(".k-overlay").css("display", "block");
                    //        $(".k-overlay").css("opacity", "0.5");
                    //        dialog.open();
                    //        dialog.center();
                    //        dialog.title("MOG APL Details - " + MOGName);
                    //        MOGCode = tr.MOGCode;
                    //        mogWiseAPLMasterdataSource = [];
                    //        getMOGWiseAPLMasterData();
                    //        populateAPLMasterGrid();
                    //        return true;
                    //    }
                    //}
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG5").css("display", "block");
                            $("#emptymog5").css("display", "none");
                        } else {
                            $("#gridMOG5").css("display", "none");
                            $("#emptymog5").css("display", "block");
                        }
                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false }, IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange5
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridMOG5');
    var toolbar = $("#gridMOG5").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG5 .k-grid-content").addClass("gridInside");
}


function showDetails(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");

    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    });

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    $("#hiddenRecipeID").val(tr.Recipe_ID);
    var dialog = $("#windowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.RecipeCode);
    populateMOGGrid1(tr.RecipeCode);

    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputrecipealiasname1").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("1", tr);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");
    $('#grandTotal1').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG1').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));

    var editor = $("#inputinstruction1").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename1").removeAttr('disabled');
    //    $("#inputuom1").removeAttr('disabled');
    //    $("#inputquantity1").removeAttr('disabled');
    //    $("#inputbase1").removeAttr('disabled');
    //   // $("#btnSubmit1").css('display', '');
    //    $("#btnCancel1").css('display', '');
    //} else {
    //    $("#inputrecipename1").attr('disabled', 'disabled');
    //    $("#inputuom1").attr('disabled', 'disabled');
    //    $("#inputquantity1").attr('disabled', 'disabled');
    //    $("#inputbase1").attr('disabled', 'disabled');
    //   //$("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details  - " + RecipeName);
    return true;

}


function showDetailsx(e) {
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipex").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#windowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.RecipeCode);
    populateMOGGrid1(tr.RecipeCode);

    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");

    $('#grandTotal1').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG1').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    var editor = $("#inputinstruction1").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename1").removeAttr('disabled');
    //    $("#inputuom1").removeAttr('disabled');
    //    $("#inputquantity1").removeAttr('disabled');
    //    $("#inputbase1").removeAttr('disabled');
    //  //  $("#btnSubmit1").css('display', '');
    //    $("#btnCancel1").css('display', '');
    //} else {
    //    $("#inputrecipename1").attr('disabled', 'disabled');
    //    $("#inputuom1").attr('disabled', 'disabled');
    //    $("#inputquantity1").attr('disabled', 'disabled');
    //    $("#inputbase1").attr('disabled', 'disabled');
    // //   $("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details  - " + RecipeName);
    return true;

}


function showDetails1(e) {



    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#windowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.RecipeCode);
    populateMOGGrid1(tr.RecipeCode);

    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputrecipealiasname1").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("1", tr);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");

    $('#grandTotal1').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG1').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));

    var editor = $("#inputinstruction1").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));

    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename1").removeAttr('disabled');
    //    $("#inputuom1").removeAttr('disabled');
    //    $("#inputquantity1").removeAttr('disabled');
    //    $("#inputbase1").removeAttr('disabled');
    //  //  $("#btnSubmit1").css('display', '');
    //    $("#btnCancel1").css('display', '');
    //} else {
    //    $("#inputrecipename1").attr('disabled', 'disabled');
    //    $("#inputuom1").attr('disabled', 'disabled');
    //    $("#inputquantity1").attr('disabled', 'disabled');
    //    $("#inputbase1").attr('disabled', 'disabled');
    //    $("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details  - " + RecipeName);
    return true;

}


function showDetails2(e) {
    $("#windowEdit1").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe1").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    // $("#windowEdit1").parent('.k-widget').css("display", "none");
    var dialog = $("#windowEdit2").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown2();
    populateBaseDropdown2();
    populateBaseRecipeGrid2(tr.RecipeCode);
    populateMOGGrid2(tr.RecipeCode);

    $("#recipeid2").val(tr.ID);
    $("#inputrecipecode2").val(tr.RecipeCode);
    $("#inputrecipename2").val(tr.Name);
    $("#inputrecipealiasname2").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("2", tr);
    $("#inputuom2").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity2").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase2").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase2").data('kendoDropDownList').value("No");
    $('#grandTotal2').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG2').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    var editor = $("#inputinstruction2").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename2").removeAttr('disabled');
    //    $("#inputuom2").removeAttr('disabled');
    //    $("#inputquantity2").removeAttr('disabled');
    //    $("#inputbase2").removeAttr('disabled');
    //  //  $("#btnSubmit2").css('display', '');
    //    $("#btnCancel2").css('display', '');
    //} else {
    //    $("#inputrecipename2").attr('disabled', 'disabled');
    //    $("#inputuom2").attr('disabled', 'disabled');
    //    $("#inputquantity2").attr('disabled', 'disabled');
    //    $("#inputbase2").attr('disabled', 'disabled');
    //    $("#btnSubmit2").css('display', 'none');
    //    $("#btnCancel2").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}

function showDetails3(e) {
    $("#windowEdit2").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe2").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error3").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#windowEdit3").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown3();
    populateBaseDropdown3();
    populateBaseRecipeGrid3(tr.RecipeCode);
    populateMOGGrid3(tr.RecipeCode);

    $("#recipeid3").val(tr.ID);
    $("#inputrecipecode3").val(tr.RecipeCode);
    $("#inputrecipename3").val(tr.Name);
    $("#inputrecipealiasname3").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("3", tr);
    $("#inputuom3").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity3").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase3").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase3").data('kendoDropDownList').value("No");
    $('#grandTotal3').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG3').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    var editor = $("#inputinstruction3").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename3").removeAttr('disabled');
    //    $("#inputuom3").removeAttr('disabled');
    //    $("#inputquantity3").removeAttr('disabled');
    //    $("#inputbase3").removeAttr('disabled');
    //   // $("#btnSubmit3").css('display', '');
    //    $("#btnCancel3").css('display', '');
    //} else {
    //    $("#inputrecipename3").attr('disabled', 'disabled');
    //    $("#inputuom3").attr('disabled', 'disabled');
    //    $("#inputquantity3").attr('disabled', 'disabled');
    //    $("#inputbase3").attr('disabled', 'disabled');
    //    $("#btnSubmit3").css('display', 'none');
    //    $("#btnCancel3").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}


function showDetails4(e) {
    $("#windowEdit3").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe3").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error4").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#windowEdit4").data("kendoWindow");

    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown4();
    populateBaseDropdown4();
    populateBaseRecipeGrid4(tr.RecipeCode);
    populateMOGGrid4(tr.RecipeCode);

    $("#recipeid4").val(tr.ID);
    $("#inputrecipecode4").val(tr.RecipeCode);
    $("#inputrecipename4").val(tr.Name);
    $("#inputrecipealiasname4").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("4", tr);
    $("#inputuom4").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity4").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase4").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase4").data('kendoDropDownList').value("No");
    $('#grandTotal4').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG4').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));

    var editor = $("#inputinstruction4").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename4").removeAttr('disabled');
    //    $("#inputuom4").removeAttr('disabled');
    //    $("#inputquantity4").removeAttr('disabled');
    //    $("#inputbase4").removeAttr('disabled');
    //  //  $("#btnSubmit4").css('display', '');
    //    $("#btnCancel4").css('display', '');
    //} else {
    //    $("#inputrecipename4").attr('disabled', 'disabled');
    //    $("#inputuom4").attr('disabled', 'disabled');
    //    $("#inputquantity4").attr('disabled', 'disabled');
    //    $("#inputbase4").attr('disabled', 'disabled');
    //    $("#btnSubmit4").css('display', 'none');
    //    $("#btnCancel4").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}

function showDetails5(e) {
    $("#windowEdit4").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe4").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error4").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#windowEdit5").data("kendoWindow");

    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown5();
    populateBaseDropdown5();
    populateBaseRecipeGrid5(tr.RecipeCode);
    populateMOGGrid5(tr.RecipeCode);

    $("#recipeid5").val(tr.ID);
    $("#inputrecipecode5").val(tr.RecipeCode);
    $("#inputrecipename5").val(tr.Name);
    $("#inputrecipealiasname5").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("5", tr);
    $("#inputuom5").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity5").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase5").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase5").data('kendoDropDownList').value("No");
    $('#grandTotal5').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG5').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    var editor = $("#inputinstruction5").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename5").removeAttr('disabled');
    //    $("#inputuom5").removeAttr('disabled');
    //    $("#inputquantity5").removeAttr('disabled');
    //    $("#inputbase5").removeAttr('disabled');
    //    //$("#btnSubmit5").css('display', '');
    //    $("#btnCancel5").css('display', '');
    //} else {
    //    $("#inputrecipename5").attr('disabled', 'disabled');
    //    $("#inputuom5").attr('disabled', 'disabled');
    //    $("#inputquantity5").attr('disabled', 'disabled');
    //    $("#inputbase5").attr('disabled', 'disabled');
    //    $("#btnSubmit5").css('display', 'none');
    //    $("#btnCancel5").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}

function showDetails6(e) {
    alert("Close Previous then Proceed!!!");
}


function BaseRecipetotalQTY(e, level) {
    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var gTotal = 0.0;
    var len1 = grid._data.length;

    for (i = 0; i < len1; i++) {
        var totalCostMOG = grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {

        $("#totBaseRecpQty").text(Utility.MathRound(gTotal, 2));
        // parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))).toFixed(2);
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
        $("#ToTIngredients").text(tot);
    }


}
function calculateItemTotal(e, level) {


    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");
    if (level == null || typeof level == 'undefined')
        level = '';
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    var recipeQty = $("#inputquantity" + level).val();
    var perc = (qty / recipeQty) * 100;
    dataItem.IngredientPerc = Utility.MathRound(perc, 2);
    //  if (level == '' || level == '0') {
    if (user.SectorNumber !== "20") {
        if (perc >= MajorPercentData) {
            dataItem.IsMajor = 1;
            $(element).closest('tr').find('.major')[0].innerHTML = dataItem.IngredientPerc;
            $(element).closest('tr').find('.minor')[0].innerHTML = "-";
        } else {
            dataItem.IsMajor = 0;
            $(element).closest('tr').find('.major')[0].innerHTML = "-"
            $(element).closest('tr').find('.minor')[0].innerHTML = dataItem.IngredientPerc;
        }
    }
    // }
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    CalculateGrandTotal(level);
    BaseRecipetotalQTY(e, level);
}

function mogtotalQTY(e, level) {
    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var gTotal = 0.0;

    var len1 = grid._data.length;



    for (i = 0; i < len1; i++) {
        var totalCostMOG = user.SectorNumber == "20" ? grid._data[i].DKgValue * grid._data[i].Quantity : grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {
        $("#totmog").text(Utility.MathRound(gTotal, 2));
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
        $("#ToTIngredients").text(tot);
    }

}

function calculateItemTotalMOG(e, level) {
    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    var recipeQty = $("#inputquantity" + level).val();
    var perc = (qty / recipeQty) * 100;
    dataItem.Quantity = qty;
    dataItem.IngredientPerc = Utility.MathRound(perc, 2);

    if (user.SectorNumber !== "20") {
        if (perc >= MajorPercentData) {
            dataItem.IsMajor = 1;
            $(element).closest('tr').find('.major')[0].innerHTML = dataItem.IngredientPerc;
            $(element).closest('tr').find('.minor')[0].innerHTML = "-";
        }
        else {
            dataItem.IsMajor = 0;
            $(element).closest('tr').find('.major')[0].innerHTML = "-"
            $(element).closest('tr').find('.minor')[0].innerHTML = dataItem.IngredientPerc;
        }
        dataItem.TotalCost = qty * dataItem.CostPerUOM;
    }
    else {
        dkgtotal = dataItem.DKgValue * qty;
        dataItem.DKgTotal = dkgtotal;
        dataItem.set("DKgTotal", dkgtotal);
        $(element).closest('tr').find('.dkgtotal')[0].innerHTML = dkgtotal;
        dataItem.TotalCost = qty * dataItem.CostPerUOM * dataItem.DKgValue;
    }

    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    CalculateGrandTotal(level);
    mogtotalQTY(e, level);
}

function changeMajorToMinorConfirmation() {
    Utility.Page_Alert_Save("Ingredient weight will change from Major to Minor. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
        function () {
        },
        function () {
        }
    );
}


function showHideGrid(gridId, emptyGrid, uid) {

    var gridLength = $("#" + gridId).data().kendoGrid.dataSource.data().length;

    if (gridLength == 0) {
        $("#" + emptyGrid).toggle();
        $("#" + gridId).hide();
    }
    else {
        $("#" + emptyGrid).hide();
        $("#" + gridId).toggle();
    }

    if (!($("#" + gridId).is(":visible") || $("#" + emptyGrid).is(":visible"))) {
        $("#" + uid).addClass("bottomCurve");

    } else {
        $("#" + uid).removeClass("bottomCurve");

    }

}

function hideGrid(gridId, emptyGrid) {
    $("#" + emptyGrid).hide();
    $("#" + gridId).show();
}

function instructionTextEditor(divid) {

    $("#" + divid).kendoEditor({
        stylesheets: [
            // "../content/shared/styles/editor.css",
        ],
        tools: [
            "bold",
            "italic",
            "underline",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "insertUnorderedList",
            "tableWizard",
            "createTable",
            "addRowAbove",
            "addRowBelow",
            "addColumnLeft",
            "addColumnRight",
            "deleteRow",
            "deleteColumn",
            "mergeCellsHorizontally",
            "mergeCellsVertically",
            "splitCellHorizontally",
            "splitCellVertically",
            "createLink",
            "unlink",

            {
                name: "fontName",
                items: [
                    { text: "Andale Mono", value: "Andale Mono" },
                    { text: "Arial", value: "Arial" },
                    { text: "Arial Black", value: "Arial Black" },
                    { text: "Book Antiqua", value: "Book Antiqua" },
                    { text: "Comic Sans MS", value: "Comic Sans MS" },
                    { text: "Courier New", value: "Courier New" },
                    { text: "Georgia", value: "Georgia" },
                    { text: "Helvetica", value: "Helvetica" },
                    //{ text: "Impact", value: "Impact" },
                    { text: "Symbol", value: "Symbol" },
                    { text: "Tahoma", value: "Tahoma" },
                    { text: "Terminal", value: "Terminal" },
                    { text: "Times New Roman", value: "Times New Roman" },
                    { text: "Trebuchet MS", value: "Trebuchet MS" },
                    { text: "Verdana", value: "Verdana" },
                ]
            },
            "fontSize",
            "foreColor",
            "backColor",
        ]
    });

}


function decodeHTMLEntities(text) {
    if (text == null || text == '' || text == 'undefined')
        return "";
    var entities = [
        ['amp', '&'],
        ['apos', '\''],
        ['#x27', '\''],
        ['#x2F', '/'],
        ['#39', '\''],
        ['#47', '/'],
        ['lt', '<'],
        ['gt', '>'],
        ['nbsp', ' '],
        ['quot', '"']
    ];

    for (var i = 0, max = entities.length; i < max; ++i)
        text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1]);

    return text;
}
$("document").ready(function () {
    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();

    });

});
function getMOGWiseAPLMasterData() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG, function (result) {
        Utility.UnLoading();

        if (result != null) {
            mogWiseAPLMasterdataSource = result;
            console.log(result);
        }
        else {
        }
    }, { mogCode: MOGCode }
        , true);
}
function populateAPLMasterGrid() {
    Utility.Loading();


    var gridVariable = $("#gridMOGWiseAPL");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "APLMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        // pageable: true,
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Total: {2} records"
            }
        },
        groupable: false,
        //reorderable: true,
        scrollable: true,
        columns: [
            {
                field: "ArticleNumber", title: "Article Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ArticleDescription", title: "Article Name", width: "150px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOM", title: "UOM", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            },
            {
                field: "SiteName", title: "SiteName", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            }
            ,
            {
                field: "ArticleCost", title: "Standard Cost", width: "50px", format: Utility.Cost_Format,
                attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            },
            {
                field: "StandardCostPerKg", title: "Cost Per KG", width: "50px", format: Utility.Cost_Format,
                attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            }

        ],
        dataSource: {
            data: mogWiseAPLMasterdataSource,
            schema: {
                model: {
                    id: "ArticleID",
                    fields: {
                        ArticleNumber: { type: "string" },
                        ArticleDescription: { type: "string" },
                        UOM: { type: "string" },
                        ArticleCost: { type: "string" },
                        StandardCostPerKg: { type: "string" },
                        MerchandizeCategoryDesc: { type: "string" },
                        MOGName: { type: "string" },
                        StandardCostPerKg: { editable: false },
                        ArticleCost: { editable: false },
                        SiteCode: { type: "string" },
                        SiteName: { type: "string" }

                    }
                }
            },
            // pageSize: 5,
        },
        // height: 404,
        noRecords: {
            template: "No Records Available"
        },

        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {


        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
    $(".k-label")[0].innerHTML.replace("items", "records");
}


function checkItemAlreadyExist(selectedId, gridID, isBR) {
    var dataExists = false;
    var gridObj = $("#" + gridID).data("kendoGrid");
    var data = gridObj.dataSource.data();
    var existingData = [];
    if (isBR) { existingData = data.filter(m => m.BaseRecipe_ID == selectedId); }
    else {
        existingData = data.filter(m => m.MOG_ID == selectedId);
        //if (!existingData[0]?.IsActive)
        //    return true;
    }
    if (existingData.length == 2) {
        dataExists = true;
    }
    return dataExists;
}

function validateDataBeforeSave(brGridID, mogGridId) {
    var isValid = true;
    var brGridObj = $("#" + brGridID + "").data("kendoGrid");
    var data = brGridObj.dataSource.data();
    if (data == undefined || data == null || data.length == 0) {
        isValid = false;
    }
    for (var i = 0; i < data.length; i++) {
        if (data[i].BaseRecipe_ID === '' || data[i].BaseRecipe_ID === null || data[i].Quantity === '' || data[i].Quantity === 0 || data[i].Quantity === "0") {
            isValid = false;
        }
    }
    var mogGridObj = $("#" + mogGridId + "").data("kendoGrid");
    var mogdata = mogGridObj.dataSource.data();
    if (mogdata == undefined || mogdata == null || mogdata.length == 0) {
        isValid = false;
    }
    for (var i = 0; i < mogdata.length; i++) {
        if (mogdata[i].MOG_ID === '' || mogdata[i].MOG_ID === null || mogdata[i].UOMName === '' || mogdata[i].UOMName === 'Select' || mogdata[i].UOMName === null ||
            mogdata[i].Quantity === '' || mogdata[i].Quantity === 0 || mogdata[i].Quantity === "0") {
            isValid = false;
        }
    }
    return isValid;
}

function hideShowRecipeWindow(hideWIndow, showWindow) {
    $("#" + hideWIndow + "").hide();
    $("#" + showWindow + "").show();

    var dialog = $("#windowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
}

function checkIsBRConfigurationChange() {
    var gridArray = ["gridMOG", "gridBaseRecipe"];
    for (var i = 0, len = gridArray.length; i < len; i++) {
        isBRConfigurationChange = doesDataSourceHaveChanges(gridArray[i]);
        if (isBRConfigurationChange) {
            break;
        }
    };
}

function checkIsBRNameChange() {
    var brOldName = RecipeName;
    var brNewName = $("#inputrecipename").val();
    if (brOldName != brNewName) {
        isBRNameChange = true;
    }
    else {
        isBRNameChange = false;
    }
}

function doesDataSourceHaveChanges(gridID) {
    var dirty = false;
    var gridObj = $("#" + gridID + "").data("kendoGrid");
    if (gridObj != undefined) {
        dirty = gridObj.dataSource.hasChanges();
        if (!dirty) {
            var previousData = gridObj.dataSource._pristineData;
            var changedData = gridObj.dataSource._data;
            if (previousData.length > 0 && changedData.length > 0) {
                for (var i = 0, len = previousData.length; i < len; i++) {
                    if (previousData[i].Quantity != changedData[i].Quantity) {
                        dirty = true;
                        break;
                    }
                }

            }
        }
    }
    return dirty;
}

function capitalizeFirstLetter(string) {
    if (string != null && string != "") {
        return string[0].toUpperCase() + string.slice(1).toLowerCase();
    }
}

function titleCase(string) {
    if (string.length > 0)
        return string.split(" ").map(x => capitalizeFirstLetter(x)).join(" ");
    else return "";
}

function cancelChangesConfirmation(gridId) {
    //  return;
    $("#" + gridId + " .k-grid-cancel-changes").unbind("mousedown");
    $("#" + gridId + " .k-grid-cancel-changes").mousedown(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                var grid = $("#" + gridId + "").data("kendoGrid");
                //  if (gridIdin == 'gridBaseRecipe') {
                //      populateBaseRecipeGrid(resetID);
                //      return;
                //  } else if (gridIdin == 'gridMOG') {
                //      populateMOGGrid(resetID)
                //      return;
                //}
                grid.cancelChanges();
            },
            function () {
            }
        );

    });
}

//Krish
function recipeGridPrint(e) {
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var tr = gridObj.dataItem($(e).closest("tr"))
    console.log(tr);
    Utility.Loading();
    setTimeout(function () {
        gridPrint(tr);

    }, 0);
    //var restorepage = document.body.innerHTML;
    //var printcontent = document.getElementById("printRecipe").innerHTML;
    ////document.body.innerHTML = printcontent;
    //window.print();
    ////document.body.innerHTML = restorepage;
}
function gridPrint(tr) {
    HttpClient.MakeSyncRequest(CookBookMasters.GetRecipePrintingData, function (data) {
        //console.log(data);
        $("#spanRecipeName").html(tr.RecipeCode + " " + tr.Name + " | ");
        $("#spanDishName").html(data.DishName + " | ");

        //if (tr.Yield == null || tr.Yield == "null") {
        //$("#spanYield").html("");
        //$("#spanDishName").html(data.DishName);
        //}
        //else {            
        $("#spanYield").html(tr.Quantity + " " + tr.UOMName);
        //}

        //document.getElementById("txtaInstructions").innerHTML = tr.Instructions

        $("#txtaInstructions1").html(tr.Instructions);
        $("#txtaInstructions").html($("#txtaInstructions1").text());
        //$("#txtaInstructions").html($("#txtaInstructions2").text());

        //Table 
        var table = document.getElementById("tblRecipePrint");
        table.innerHTML = "";
        var row = table.insertRow(-1);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "MOG Code";
        row.appendChild(headerCell);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "MOG Name";
        row.appendChild(headerCell);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "UOM";
        row.appendChild(headerCell);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "Quantity";
        row.appendChild(headerCell);


        for (var i = 0; i < data.Mogs.length; i++) {
            var row1 = table.insertRow(-1);
            var innerCell = document.createElement("TD");
            innerCell.innerHTML = data.Mogs[i].MOGCode;
            row1.appendChild(innerCell);
            innerCell = document.createElement("TD");
            innerCell.innerHTML = data.Mogs[i].MOGName;
            row1.appendChild(innerCell);
            innerCell = document.createElement("TD");
            innerCell.innerHTML = data.Mogs[i].UOMName;
            row1.appendChild(innerCell);
            innerCell = document.createElement("TD");
            //var mogqty = data.Mogs[i].MOGQty.toString();
            //var qty = mogqty.slice(0, (mogqty.indexOf(".")) + 2);
            if (data.Mogs[i].MOGQty == null) { innerCell.innerHTML = "0.00"; }
            else {
                var qty = data.Mogs[i].MOGQty.toFixed(2);
                if (parseFloat(qty) == 0)
                    qty = data.Mogs[i].MOGQty.toFixed(3);
                innerCell.innerHTML = qty;
            }
            row1.appendChild(innerCell);
        }

        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById("printRecipe").innerHTML;
        //document.body.innerHTML = printcontent;
        setTimeout(function () { document.title = "CookBook Recipe - " + tr.Name; window.print(); }, 500);
        //$("#printRecipe").show();
        //$("#mainContent").hide();

        //document.body.innerHTML = restorepage;
        //$("#printRecipe").print();
        //var divToPrint = document.getElementById("printRecipe");
        //newWin = window.open("");
        //console.log(printcontent);
        //newWin.document.write(printcontent);
        //newWin.print();
        //newWin.close();

        //$("#printRecipe").printThis();             

        Utility.UnLoading();

    }, { recipeCode: tr.RecipeCode, siteCode: "", regionId: 0 }, true);
}

var DishName = "";
//Krish 20-07-2022
function recipeGridInfo(e) {
    $("#displayRecipeDetails").show();
    $("#mainContent").hide();

    var gridObj = $("#gridRecipe").data("kendoGrid");
    var tr = gridObj.dataItem($(e).closest("tr"));
    
    //console.log(tr);
    Utility.Loading();
    setTimeout(function () {
        HttpClient.MakeSyncRequest(CookBookMasters.GetRecipePrintingData, function (data) {
            DishName = data.DishName;
            $("#hdnRecipeCodeForPrint").val(tr.RecipeCode);
            populateMOGGridNutri(data.Mogs);

            setTimeout(function () {
                var html = $("#recipeDetails").html();
                html = html.replace("##RecipeCode##", tr.RecipeCode);
                html = html.replace("##RecipeName##", tr.Name);
                html = html.replace("##Quantity##", tr.Quantity);
                html = html.replace("##RecipeType##", tr.RecipeType);
                html = html.replace("##Allergen##", tr.AllergensName);
                html = html.replace("##AllergenNG##", tr.NGAllergens);
                //setTimeout(function () { 
                //var gridHtml = $("#gridContent").html();
                var gridHtml = $("#gridMOGNutri").html();
                //html = html.replace("##RecipeGrid##", gridHtml);
                html = html.replace("##RecipeGrid##", gridHtml);
                //console.log(gridHtml)
                var instr = tr.Instructions;
                if (tr.Instructions == "null")
                    instr = "";
                $("#instructionContent").html(instr);
                html = html.replace("##Instructions##", $("#instructionContent").text());

                $("#recipeDetailsShell").html(html);

            }, 300)

            //$("#spanRecipeName").html(tr.RecipeCode + " " + tr.Name + " | ");
            //$("#spanDishName").html(data.DishName + " | ");

            ////if (tr.Yield == null || tr.Yield == "null") {
            ////$("#spanYield").html("");
            ////$("#spanDishName").html(data.DishName);
            ////}
            ////else {            
            //$("#spanYield").html(tr.Quantity + " " + tr.UOMName);
            ////}

            ////document.getElementById("txtaInstructions").innerHTML = tr.Instructions

            //$("#txtaInstructions1").html(tr.Instructions);
            //$("#txtaInstructions").html($("#txtaInstructions1").text());
            ////$("#txtaInstructions").html($("#txtaInstructions2").text());

            ////Table 
            //var table = document.getElementById("tblRecipePrint");
            //table.innerHTML = "";
            //var row = table.insertRow(-1);
            //var headerCell = document.createElement("TH");
            //headerCell.innerHTML = "MOG Code";
            //row.appendChild(headerCell);
            //var headerCell = document.createElement("TH");
            //headerCell.innerHTML = "MOG Name";
            //row.appendChild(headerCell);
            //var headerCell = document.createElement("TH");
            //headerCell.innerHTML = "UOM";
            //row.appendChild(headerCell);
            //var headerCell = document.createElement("TH");
            //headerCell.innerHTML = "Quantity";
            //row.appendChild(headerCell);


            //for (var i = 0; i < data.Mogs.length; i++) {
            //    var row1 = table.insertRow(-1);
            //    var innerCell = document.createElement("TD");
            //    innerCell.innerHTML = data.Mogs[i].MOGCode;
            //    row1.appendChild(innerCell);
            //    innerCell = document.createElement("TD");
            //    innerCell.innerHTML = data.Mogs[i].MOGName;
            //    row1.appendChild(innerCell);
            //    innerCell = document.createElement("TD");
            //    innerCell.innerHTML = data.Mogs[i].UOMName;
            //    row1.appendChild(innerCell);
            //    innerCell = document.createElement("TD");
            //    //var mogqty = data.Mogs[i].MOGQty.toString();
            //    //var qty = mogqty.slice(0, (mogqty.indexOf(".")) + 2);
            //    if (data.Mogs[i].MOGQty == null) { innerCell.innerHTML = "0.00"; }
            //    else {
            //        var qty = data.Mogs[i].MOGQty.toFixed(2);
            //        if (parseFloat(qty) == 0)
            //            qty = data.Mogs[i].MOGQty.toFixed(3);
            //        innerCell.innerHTML = qty;
            //    }
            //    row1.appendChild(innerCell);
            //}

            //var restorepage = document.body.innerHTML;
            //var printcontent = document.getElementById("printRecipe").innerHTML;
            ////document.body.innerHTML = printcontent;
            //setTimeout(function () { document.title = "CookBook Recipe - " + tr.Name; window.print(); }, 500);
            ////$("#printRecipe").show();
            ////$("#mainContent").hide();

            ////document.body.innerHTML = restorepage;
            ////$("#printRecipe").print();
            ////var divToPrint = document.getElementById("printRecipe");
            ////newWin = window.open("");
            ////console.log(printcontent);
            ////newWin.document.write(printcontent);
            ////newWin.print();
            ////newWin.close();

            ////$("#printRecipe").printThis();             

            Utility.UnLoading();

        }, { recipeCode: tr.RecipeCode, siteCode: "", regionId: 0 }, true);

    }, 0);
}
function populateMOGGridNutri(recipeResult) {
    console.log(recipeResult)
    Utility.Loading();
    //$("#gridContent").html("");
    //$("#gridContent").html("<div id='gridMOGNutri' style='overflow: visible!important;'></div>")
    var gridVariable = $("#gridMOGNutri");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" }, //{ name: "create", text: "Add" }
        //],
        groupable: false,
        editable: false,

        columns: [
            {
                field: "MOGCode", title: "MOG Code", width: "100px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                }

            },
            {
                field: "MOG_ID", title: "MOG", width: "100px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },

            },
            //{
            //    field: "ColorCode", title: "Color", width: "30px", attributes: {

            //        style: "text-align: center; font-weight:normal"
            //    },
            //    headerAttributes: {
            //        style: "text-align: center"
            //    },
            //    template: '<div class="colortag"></div>',
            //},
            //{
            //    field: "AllergensName", title: "Contains", width: "50px",
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "AllergensName",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //},
            //{
            //    field: "NGAllergen", title: "May Contains (NG)", width: "70px",
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "AllergensName",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //},
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "MOGQty", title: "Quantity", width: "35px",

                headerAttributes: {
                    style: "text-align: center;width:35px;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                template: function (dataItem) {
                    var incel = "";
                    if (dataItem.MOGQty == null) { incel = "0.00"; }
                    else {
                        var qty = dataItem.MOGQty.toFixed(2);
                        if (parseFloat(qty) == 0)
                            qty = dataItem.MOGQty.toFixed(3);
                        incel = qty;
                    }
                    var html = '<span class="mogquant">' + incel + '</span>';
                    return html;
                },
            },

        ],
        dataSource: recipeResult,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {

            var grid = this;

            //grid.tbody.find("tr[role='row']").each(function () {
            //    var model = grid.dataItem(this);

            //    if (model.AllergensName != "None" && model.NGAllergen != "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00012" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //    else if (model.AllergensName == "None" && model.NGAllergen != "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00013" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //    else if (model.AllergensName != "None" && model.NGAllergen == "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00014" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //    else if (model.AllergensName == "None" && model.NGAllergen == "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00015" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //});
        },
        change: function (e) {
        },
    });

}
function doBackFromDetails() {
    $("#displayRecipeDetails").hide();
    $("#mainContent").show();
}
function doPrintFromDetails() {
    var tr = copyData.filter(a => a.RecipeCode == $("#hdnRecipeCodeForPrint").val())[0];
    gridPrint(tr);
    //var btnInfo = localStorage.getItem("infobtn");
    //var gridObj = $("#gridRecipe").data("kendoGrid");
    //var tr = gridObj.dataItem($(btnInfo).closest("tr"));
    //console.log(tr)
    //recipeGridPrint(btnInfo);
}

function doExportFromDetails() {
    $("#inputRecipeYield").val("");
    var tr = copyData.filter(a => a.RecipeCode == $("#hdnRecipeCodeForPrint").val())[0];
    var dialog = $("#windowEditExportRecipe").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
    var dName = "";
    if (DishName == undefined || DishName == "" || DishName.length ==0) {
        dName = tr.Name;
    }
    else {
        dName = DishName.substring(10, DishName.length);
    }
    dialog.title("Yield for " + dName+" for Recipe Export");
}

function recipeExportSubmit() {
    
    if ($("#inputRecipeYield").val() == null || $("#inputRecipeYield").val() == "" || $("#inputRecipeYield").val() == undefined) {
        toastr.error("Please enter Recipe Yeild");
        Utility.UnLoading();
        return false;
    }
    var tr = copyData.filter(a => a.RecipeCode == $("#hdnRecipeCodeForPrint").val())[0];
    setTimeout(Utility.Loading(), 5000)

    HttpClient.MakeRequest(CookBookMasters.ExportRecipeData, function (result) {
        if (result != null) {
            var bytes = new Uint8Array(result.FileContents);
            var blob = new Blob([bytes], { type: "application/vnd.ms-excel" });
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "" + tr.Name + ".xlsx";
            link.click();
            $(".k-overlay").hide();
            var orderWindow = $("#windowEditExportRecipe").data("kendoWindow");
            orderWindow.close();
            $("#inputRecipeYield").val("");
        }
        else {
        }
    }, 
        {
            recipeCode: tr.RecipeCode,
            recipeName: tr.Name,
            recipeYield: $("#inputRecipeYield").val()
        }
        , true);
}

function recipeExportCancel() {
    $(".k-overlay").hide();
    var orderWindow = $("#windowEditExportRecipe").data("kendoWindow");
    orderWindow.close();
}

function recipeGridEdit(e) {
    Utility.Loading();
    //Krish 03-08-2022
    loadFormData();
    setTimeout(function () {
        //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {

        //    var dataSource = data;

        //    baserecipedata = [];
        //    baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        if (dataSource[i].CostPerUOM == null) {
        //            dataSource[i].CostPerUOM = 0;
        //        }
        //        if (dataSource[i].CostPerKG == null) {
        //            dataSource[i].CostPerKG = 0;
        //        }
        //        if (dataSource[i].TotalCost == null) {
        //            dataSource[i].TotalCost = 0;
        //        }
        //        if (dataSource[i].Quantity == null) {
        //            dataSource[i].Quantity = 0;
        //        }
        //        if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
        //            baserecipedata.push({
        //                "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
        //            });
        //        else
        //            baserecipedata.push({
        //                "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
        //            });

        //    }
        //    baserecipedatafiltered = baserecipedata;
        //    basedataList = baserecipedata;
        //}, { condition: "Base", DishCode: "0" }, true);

        var gridObj = $("#gridRecipe").data("kendoGrid");
        var tr = gridObj.dataItem($(e).closest("tr")); // 'e' is the HTML element <a>
        if (!tr.IsActive) {
            return;
        }
        Utility.Loading();
        setTimeout(function () {
            $("#success").css("display", "none");
            $("#error").css("display", "none");

            // var tr = gridObj.dataItem($(e).closest("tr")); // 'e' is the HTML element <a>
            onEditRecipeGrid(tr, false);
            return true;
            Utility.UnLoading();
        }, 2000);
    }, 2000);
}
function loadFormData() {

    //Krish 03-08-2022
    //HttpClient.MakeRequest(CookBookMasters.GetApplicationSettingDataList, function (result) {

    //    if (result != null) {
    //        applicationSettingData = result;
    //        for (item of applicationSettingData) {
    //            if (item.Code == "FLX-00004")//item.Name == "Major Identification Threshold" || 
    //                MajorPercentData = item.Value;
    //            $(".asPercent").text(MajorPercentData);
    //        }

    //    }
    //    else {

    //    }
    //}, null

    //    , false);
    //HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {

    //    user = result;
    //    if (user.SectorNumber == "80") {
    //        $(".healthcareonly").show();

    //    }
    //    else {
    //        $(".healthcareonly").hide();
    //    }

    //    if (user.SectorNumber == "20") {
    //        $(".rcHide").show();
    //        $(".mfgHide").hide();
    //    }
    //    else {
    //        $(".rcHide").hide();
    //        $(".mfgHide").show();
    //    }

    //    if (user.SectorNumber == "10") {
    //        $(".googleOnly").show();
    //    }
    //    else {
    //        $(".googleOnly").hide();
    //    }
    //    populateRecipeGrid();
    //}, null, true);

    HttpClient.MakeRequest(CookBookMasters.GetSectorRecipeDataList, function (result) {
        //HttpClient.MakeRequest(CookBookMasters.GetMOGDataList, function (data) {

        var dataSource = result.MOGData;

        mogdata = [];
        mogdata.push({ "MOG_ID": "Select MOG", "MOGName": "Select MOG", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "DKgValue": 0, "MOG_Code": "", "IsActive": false });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].CostPerUOM == null) {
                dataSource[i].CostPerUOM = 0;
            }
            if (dataSource[i].CostPerKG == null) {
                dataSource[i].CostPerKG = 0;
            }
            if (dataSource[i].TotalCost == null) {
                dataSource[i].TotalCost = 0;
            }
            if (dataSource[i].Alias != null && dataSource[i].Alias.trim() != "" && dataSource[i].Alias != dataSource[i].Name)
                mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name + " (" + dataSource[i].Alias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "DKgValue": dataSource[i].DKgValue, "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive });
            else
                mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode, "DKgValue": dataSource[i].DKgValue, "IsActive": dataSource[i].IsActive });
        }

        mogdataList = mogdata;

        //}, null, true);
        //HttpClient.MakeRequest(CookBookMasters.GetDishDataList, function (data) {

        var dataSource = result.DishData;
        dishdata = [];
        dishdata.push({ "value": "Select Dish", "text": "Select Dish" });
        for (var i = 0; i < dataSource.length; i++) {
            dishdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name });
        }
        //}, null, true);
        //HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
        var dataSource = result.UOMModuleMappingData;
        uommodulemappingdata = [];
        uommodulemappingdata.push({ "value": "Select", "text": "Select", "UOMCode": "Select" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].IsActive) {
                uommodulemappingdata.push({
                    "value": dataSource[i].UOM_ID, "UOM_ID": dataSource[i].UOM_ID, "UOMName": dataSource[i].UOMName, "text": dataSource[i].UOMName,
                    "UOMModuleCode": dataSource[i].UOMModuleCode, "UOMCode": dataSource[i].UOMCode
                });
            }
        }
        moguomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001');
        uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");
        recipecategoryuomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00006' || m.text == "Select");
        populateUOMDropdown0();
        populateDishDropdown();

        //}, null, false);

        //HttpClient.MakeSyncRequest(CookBookMasters.GetNutritionMasterDataList, function (data) {
        var data = result.NutritionElementMasterData;
        nutrientMasterdataSource = data.filter(m => m.IsActive);

        //}, null, true);

        //HttpClient.MakeRequest(CookBookMasters.GetSubSectorMasterList, function (data) {
        var data = result.SubSectorData;
        if (data.length > 1) {
            isSubSectorApplicable = true;
            data.unshift({ "SubSectorCode": 'Select', "Name": "Select Sub Sector" })
            subsectordata = data;
            populateSubSectorDropdown("");
        }
        else {
            isSubSectorApplicable = false;
        }
        //}, { isAllSubSector: true }, false);
        //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {

        var dataSource = result.RecipeData;

        baserecipedata = [];
        baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].CostPerUOM == null) {
                dataSource[i].CostPerUOM = 0;
            }
            if (dataSource[i].CostPerKG == null) {
                dataSource[i].CostPerKG = 0;
            }
            if (dataSource[i].TotalCost == null) {
                dataSource[i].TotalCost = 0;
            }
            if (dataSource[i].Quantity == null) {
                dataSource[i].Quantity = 0;
            }
            if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
                baserecipedata.push({
                    "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                });
            else
                baserecipedata.push({
                    "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                });

        }
        baserecipedatafiltered = baserecipedata;
        basedataList = baserecipedata;
        //}, { condition: "Base", DishCode: "0" }, true);
    }, { isAddEdit: true }, true);

    //HttpClient.MakeRequest(CookBookMasters.GetMOGDataList, function (data) {

    //    var dataSource = data;

    //    mogdata = [];
    //    mogdata.push({ "MOG_ID": "Select MOG", "MOGName": "Select MOG", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "DKgValue": 0, "MOG_Code": "", "IsActive": false });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        if (dataSource[i].CostPerUOM == null) {
    //            dataSource[i].CostPerUOM = 0;
    //        }
    //        if (dataSource[i].CostPerKG == null) {
    //            dataSource[i].CostPerKG = 0;
    //        }
    //        if (dataSource[i].TotalCost == null) {
    //            dataSource[i].TotalCost = 0;
    //        }
    //        if (dataSource[i].Alias != null && dataSource[i].Alias.trim() != "" && dataSource[i].Alias != dataSource[i].Name)
    //            mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name + " (" + dataSource[i].Alias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "DKgValue": dataSource[i].DKgValue, "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive });
    //        else
    //            mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode, "DKgValue": dataSource[i].DKgValue, "IsActive": dataSource[i].IsActive });
    //    }

    //    mogdataList = mogdata;

    //}, null, true);

    ////HttpClient.MakeRequest(CookBookMasters.GetMOGUOMDataList, function (data) {
    ////    moguomdetailsdata = data;
    ////}, null, true);

    ////HttpClient.MakeRequest(CookBookMasters.GetRecipeDataList, function (data) {

    ////    var dataSource = data;

    ////    baserecipedata = [];
    ////    baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
    ////    for (var i = 0; i < dataSource.length; i++) {
    ////        if (dataSource[i].CostPerUOM == null) {
    ////            dataSource[i].CostPerUOM = 0;
    ////        }
    ////        if (dataSource[i].CostPerKG == null) {
    ////            dataSource[i].CostPerKG = 0;
    ////        }
    ////        if (dataSource[i].TotalCost == null) {
    ////            dataSource[i].TotalCost = 0;
    ////        }
    ////        if (dataSource[i].Quantity == null) {
    ////            dataSource[i].Quantity = 0;
    ////        }
    ////        if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
    ////            baserecipedata.push({
    ////                "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
    ////            });
    ////        else
    ////            baserecipedata.push({
    ////                "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
    ////            });

    ////    }
    ////    baserecipedatafiltered = baserecipedata;
    ////    basedataList = baserecipedata;
    ////}, { condition: "Base", DishCode: "0" }, true);

    //HttpClient.MakeRequest(CookBookMasters.GetDishDataList, function (data) {

    //    var dataSource = data;
    //    dishdata = [];
    //    dishdata.push({ "value": "Select Dish", "text": "Select Dish" });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        dishdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name });
    //    }
    //}, null, true);

    ////HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

    ////    var dataSource = data;
    ////    uomdata = [];
    ////    uomdata.push({ "value": "Select", "text": "Select", "UOMCode": "" });
    ////    for (var i = 0; i < dataSource.length; i++) {
    ////        uomdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode });
    ////    }
    ////    populateDishDropdown();
    ////}, null, false);

    //HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
    //    var dataSource = data;
    //    uommodulemappingdata = [];
    //    uommodulemappingdata.push({ "value": "Select", "text": "Select", "UOMCode": "Select" });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        if (dataSource[i].IsActive) {
    //            uommodulemappingdata.push({
    //                "value": dataSource[i].UOM_ID, "UOM_ID": dataSource[i].UOM_ID, "UOMName": dataSource[i].UOMName, "text": dataSource[i].UOMName,
    //                "UOMModuleCode": dataSource[i].UOMModuleCode, "UOMCode": dataSource[i].UOMCode
    //            });
    //        }
    //    }
    //    moguomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001');
    //    uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");
    //    recipecategoryuomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00006' || m.text == "Select");

    //    populateDishDropdown();

    //}, null, false);

    //HttpClient.MakeSyncRequest(CookBookMasters.GetNutritionMasterDataList, function (data) {
    //    nutrientMasterdataSource = data.filter(m => m.IsActive);

    //}, null, true);

    //HttpClient.MakeRequest(CookBookMasters.GetSubSectorMasterList, function (data) {

    //    if (data.length > 1) {
    //        isSubSectorApplicable = true;
    //        data.unshift({ "SubSectorCode": 'Select', "Name": "Select Sub Sector" })
    //        subsectordata = data;
    //        populateSubSectorDropdown("");   
    //    }
    //    else {
    //        isSubSectorApplicable = false;
    //    }
    //}, { isAllSubSector: true }, false);


    basedata = [{ "value": "Select", "text": "Select" }, { "value": "No", "text": "Final Recipe" },
    { "value": "Yes", "text": "Base Recipe" }];

    $("#gridBulkChange").css("display", "none");
    populateBulkChangeControls();
    var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
    changeControls.css("background-color", "#fff");
    changeControls.css("border", "none");
    changeControls.eq(2).text("");
    changeControls.eq(2).append("<div id='uom' style='width:100%; font-size:10.5px!important'></div>");
    changeControls.eq(3).text("");
    changeControls.eq(3).append("<input id='quantity' style='width:90%; margin-left:3px; font-size:10.5px!important'></div>");
    changeControls.eq(4).text("");
    changeControls.eq(4).append("<div id='base' style='width:100%; font-size:10.5px!important'></div>");
}
