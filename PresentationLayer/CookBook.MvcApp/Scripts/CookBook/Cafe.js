﻿$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var CafeName = "";
    var sitedata = [];
    var dkdata = [];

    $("#CafewindowEdit").kendoWindow({
        modal: true,
        width: "680px",
        height: "175px",
        title: "Cafe Details - " + CafeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });



    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $('#myInput').on('input', function (e) {
        var grid = $('#gridCafe').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "CafeCode" || x.field == "CafeName" || x.field == "SiteName" || x.field == "SiteCode" || x.field == "SQCafe_ID"
                    || x.field == "SQSecureCode") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    var user;
    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#btnExport").click(function (e) {
            var grid = $("#gridCafe").data("kendoGrid");
            grid.saveAsExcel();
        });
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            if (user.UserRoleId === 1) {
                $("#InitiateBulkChanges").css("display", "none");
                $("#AddNew").css("display", "inline");
            }
            if (user.UserRoleId === 2) {
                $("#InitiateBulkChanges").css("display", "none");
                $("#AddNew").css("display", "none");
            }
            populateCafeGrid();
        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetSiteDataList, function (data) {
            
            var dataSource = data;
            sitedata = [];
           
            sitedata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].IsActive) {
                    var SiteName = dataSource[i].SiteAlias;
                    if (SiteName == null)
                        SiteName = dataSource[i].SiteName;
                    sitedata.push({ "value": dataSource[i].SiteCode, "text": SiteName + " - " + dataSource[i].SiteCode });
                }
            }
            $("#inputsite").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: sitedata,
                index: 0,
            });

            $("#inputsite").data("kendoDropDownList").select(function (dataItem) {
                return dataItem.value === dataSource[0].value;
            });
            $("#inputsite").data("kendoDropDownList").select(0);
        }, null, false);

    });

    $("#InitiateBulkChanges").on("click", function () {
        populateBulkChangeControls();
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        changeControls.css("background-color", "#fff");
        changeControls.css("border", "none");
        $("#gridBulkChange").css("display", "block");
        $("#gridBulkChange").children(".k-grid-header").css("border", "none");
        $("#InitiateBulkChanges").css("display", "none");
        $("#CancelBulkChanges").css("display", "inline");
        $("#SaveBulkChanges").css("display", "inline");
        changeControls.eq(2).text("");
        changeControls.eq(2).append("<div id='site' style='width:90%; margin-left:3px; font-size:10.5px!important'></div>");
        Utility.UnLoading();
    });


    $("#CancelBulkChanges").on("click", function () {
        $("#InitiateBulkChanges").css("display", "inline");
        $("#CancelBulkChanges").css("display", "none");
        $("#SaveBulkChanges").css("display", "none");
        $("#gridBulkChange").css("display", "none");
        populateCafeGrid();
    });

    $("#SaveBulkChanges").on("click", function () {
        
        var neVal = $(this).val();
        var dataFiltered = $("#gridCafe").data("kendoGrid").dataSource.dataFiltered();
        
        
        var model = dataFiltered;
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        var validChange = false;
        var site = "";

        var sitespan = $("#site");//  changeControls.eq(4).children("span").children("span").children(".k-input");
        
        if (sitespan.val() != "Select") {
            validChange = true;
            site = sitespan.val();
        }
        
        if (validChange == false) {
            toastr.error("Please change at least one attribute for Bulk Update");
        } else {
            Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                function () {
                    $.each(model, function () {
                        if (site.length > 0)
                            this.SiteCode = site;
                            this.CreatedOn = kendo.parseDate(this.CreatedOn); 
                    });
                    HttpClient.MakeRequest(CookBookMasters.SaveCafeDataList, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again.");
                        }
                        else {
                            $(".k-overlay").hide();
                            toastr.success("Records have been updated successfully");
                            $("#gridCafe").data("kendoGrid").dataSource.data([]);
                            $("#gridCafe").data("kendoGrid").dataSource.read();
                            $("#InitiateBulkChanges").css("display", "inline");
                            $("#CancelBulkChanges").css("display", "none");
                            $("#SaveBulkChanges").css("display", "none");
                            $("#gridBulkChange").css("display", "none");
                        }
                    }, {
                        model: model

                    }, true);

                    //populateCafeGrid();
                },
                function () {
                    maptype.focus();
                }
            );
        }
    });
    //Food Program Section Start

    function populateCafeGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridCafe");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "Cafe.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "CafeCode", title: "Cafe Code", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "CafeName", title: "Cafe Name", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "SiteCode", title: "Site Code", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "SiteName", title: "Site Name", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "SQCafe_ID", title: "SQ Cafe ID", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "SQSecureCode", title: "Secure Code", width: "90px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                    template: '# if (SQSecureCode!=null && SQSecureCode!="null") {#<div>#: SQSecureCode#</div>#} else  {#<div>-</div>#}#',

                },
                //{
                //    field: "IsActive",
                //    title: "Status", width: "50px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = $("#gridCafe").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (!tr.IsActive) {
                                    return;
                                }
                                datamodel = tr;
                                CafeName = tr.CafeName + " (" + tr.SiteCode + ")";
                                var dialog = $("#CafewindowEdit").data("kendoWindow");
                                
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open();
                                dialog.center();
                                // 
                                $("#cafeid").val(tr.ID);
                                $("#cafecode ").val(tr.CafeCode);
                                $("#cafename").val(tr.CafeName).focus();
                                $("#description").val(tr.CafeDescription);
                                $("#inputsqcafeid").val(tr.SQCafe_ID);
                                $("#inputsqsecurecode").val(tr.SQSecureCode);
                                $("#inputsite").data('kendoDropDownList').value(tr.SiteCode);
                                if (user.UserRoleId === 1) {
                                    $("#cafename").removeAttr('disabled');
                                    $("#description").removeAttr('disabled');
                                    $("#inputsqcafeid").removeAttr('disabled');
                                    $("#inputsqsecurecode").removeAttr('disabled');
                                    $("#inputsite").removeAttr('disabled');
                                    $("#OthCafebtnSubmit").css('display', 'inline');
                                    $("#OthCafebtnCancel").css('display', 'inline');
                                } else {
                                    $("#sorg").attr('disabled', 'disabled');
                                    ("#cafename").attr('disabled', 'disabled');
                                    $("#description").attr('disabled', 'disabled');
                                    $("#inputsqcafeid").attr('disabled', 'disabled');
                                    $("#inputsqsecurecode").attr('disabled', 'disabled');
                                    $("#inputsite").attr('disabled', 'disabled');
                                    $("#OthCafebtnSubmit").css('display', 'none');
                                    $("#OthCafebtnCancel").css('display', 'none');
                                }

                                dialog.title("Cafe Details - " + CafeName);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetCafeDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            CafeCode: { type: "string" },
                            CafeName: { type: "string" },
                            SiteName: { type: "string" },
                            SQCafe_ID: { type: "string" },
                            SQSecureCode: { type: "string" },
                            IsActive: { editable: false }
                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this; 
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                       // $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        //$(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });

                $(".chkbox").on("change", function () {
                    var gridObj = $("#gridCafe").data("kendoGrid");
                    var trEdit = $(this).closest("tr");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    var th = this;

                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.CafeName + "</b> Cafe " + active + "?", "Cafe Update Confirmation", "Yes", "No", function () {
                        HttpClient.MakeRequest(CookBookMasters.ChangeStatus, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                toastr.success("Cafe configuration updated successfully");
                                $(th)[0].checked = datamodel.IsActive;
                                if ($(th)[0].checked) {
                                    $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                } else {
                                    $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                }

                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {
                            $(th)[0].checked = !datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                            
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function populateBulkChangeControls() {
        
        Utility.Loading();
        var gridVariable = $("#gridBulkChange");
        gridVariable.html("");
        gridVariable.kendoGrid({
            selectable: "cell",
            sortable: false,
            filterable: false,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "", title: "", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "", title: "", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "", title: "Site Name", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "90px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                }
            ],
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
            },
            change: function (e) {
            },
        });
    }

    $("#AddNew").on("click", function () {
        var model;
        var dialog = $("#CafewindowEdit").data("kendoWindow");
       
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open().element.closest(".k-window").css({
            top: 167,
            
        });
        dialog.center();

        datamodel = model;
        $("#cafeid").val("");
        $("#cafecode ").val("");
        $("#cafename").val("").focus();
        $("#description").val("");
        $("#inputsqcafeid").val("");
        $("#inputsqsecurecode").val("");
        $("#inputsite").data('kendoDropDownList').value("Select");

        dialog.title("Create New Cafe");
    })

    $("#OthCafebtnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#CafewindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    
    //$(".switch").on("click", function () {
    //    // 
    //    $("#success").css("display", "none");
    //    $("#error").css("display", "none");
    //    var gridObj = $("#gridCafe").data("kendoGrid");
    //    var tr = gridObj.dataItem($(this).closest("tr"));
    //    datamodel = tr;
    //    //sitename = tr.SiteName + " (" + tr.SiteCode + ")";
    //    //var dialog = $("#CafewindowEdit").data("kendoWindow");
    //    //$("#CafewindowEdit").kendoWindow({
    //    //    animation: false
    //    //});
    //    //$(".k-overlay").css("display", "block");
    //    //$(".k-overlay").css("opacity", "0.5");
    //    //dialog.open();
    //    //dialog.center();

    //    //dialog.title("Site Details - " + sitename);
    //    return true;
    //});

    
    $("#OthCafebtnSubmit").click(function () {
        if ($("#cafename").val() === "") {
            toastr.error("Please provide Cafe Name");
            $("#cafename").focus();
            return;
        }
        if ($("#inputsite").val() === "Select") {
            toastr.error("Please select Site");
            $("#inputsite").focus();
            return;
        }
        else {
            var model;
            if (datamodel != null) {
                model = datamodel;
                model.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
                model.CafeCode = $("#cafecode").val();
                model.CafeName = $("#cafename").val();
                model.CafeDescription = $("#description").val();
                model.SiteCode = $("#inputsite").val();
                model.SQCafe_ID = $("#inputsqcafeid").val();
                model.SQSecureCode = $("#inputsqsecurecode").val();
            }
            else {
                model = {
                    "ID": $("#cafeid").val(),
                    "CafeCode": $("#cafecode").val(),
                    "CafeName": $("#cafename").val(),
                    "CafeDescription": $("#description").val(),
                    "SiteCode": $("#inputsite").val(),
                    "SQCafe_ID": $("#inputsqcafeid").val(),
                    "SQSecureCode": $("#inputsqsecurecode").val(),
                    "IsActive" : true,
                }
            }

            if (!sanitizeAndSend(model)) {
                return;
            }
            $("#OthCafebtnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveCafeData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#OthCafebtnSubmit').removeAttr("disabled");
                    $("#sitealias").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#OthCafebtnSubmit').removeAttr("disabled");
                        $("#sitealias").focus();
                        toastr.error(result.message);
                        Utility.UnLoading();
                        return;
                    }
                    else {
                        if (result == false) {
                            $('#OthCafebtnSubmit').removeAttr("disabled");
                            toastr.error("Some error occured, please try again");
                            $("#sitealias").focus();
                        }
                        else if (result == "Duplicate") {
                            $('#OthCafebtnSubmit').removeAttr("disabled");
                            toastr.error("Duplicate Name, Kindly change");
                            $("#sitealias").focus();

                        }
                        else {
                            $(".k-overlay").hide();
                            var orderWindow = $("#CafewindowEdit").data("kendoWindow");
                            orderWindow.close();
                            $('#OthCafebtnSubmit').removeAttr("disabled");
                            toastr.success("Cafe configuration updated successfully");
                            $("#gridCafe").data("kendoGrid").dataSource.data([]);
                            $("#gridCafe").data("kendoGrid").dataSource.read();
                        }
                    }
                }
            }, {
                model: model
            }, false);
        }
    });
});