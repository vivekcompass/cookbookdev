﻿
var user;
var status = "";
var varname = "";
var datamodel;

var BATCHNUMBER = "";












$(document).ready(function () {
    $("#UploadExcelFile").kendoWindow({
        modal: true,
        width: "575px",
        height: "105px",
        title: "Patient Master EXCEL",
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#GETALLPmViewALLHistoryBATCHWISE").kendoWindow({
        modal: true,
        width: "1005px",
        height: "115px",
        title: "Patient Master EXCEL",
        actions: ["Close"],
        visible: false,
        animation: false

    });
    $('#myInput').on('input', function (e) {
        var grid = $('#gridPatientMaster').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "PatientID" || x.field == "PatientName" ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $(".k-window").hide();
    $(".k-overlay").hide();
    Utility.Loading();
    setTimeout(function () {
        onLoad();
        Utility.UnLoading();
    }, 500);



    //populatePatientMasterGrid();
});

function onLoad() {
    
    

    $("#btnExport").click(function (e) {
        var grid = $("#gridPatientMaster").data("kendoGrid");
        grid.saveAsExcel();
    });
    $("#BtnExportTemp").click(function (e) {
        //$.ajax({
        //    url:  '/MOG/DownloadExcTemplate',
        //    type: 'GET',       
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (data) {
        //        alert("success");
        //    },
        //    error: function (data) {
        //        alert(data);
        //    }
        //});

        window.location.href = baseUrl + 'PatientMaster/DownloadExcTemplate';
    });
    $("#UploadPmExcel").click(function (e) {

        if ($("#file").val() != "") {
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls|.csv|.json)$/;
            /*Checks whether the file is a valid excel file*/
            if (!regex.test($("#file").val().toLowerCase())) {
                toastr.error("Please upload a valid file!");
                return false;
            }
            else {
                UploadSelectedExcelsheet();

            }
        }
        else {
            toastr.error("Please upload a valid file!");
            return false;
        }
    });
    $("#Pmbtnback").click(function () {
        //alert("call");
        $("#PMMaster").show();
        $("#pmviewhistorymain").hide();
    });
    $("#Btnviewuploadhistory").click(function (e) {
        // window.location.href = 'MOG/GetMOGAPLHISTORY';
        populateCafeGrid();
        populateCafeGridDetails();

        $("#PMMaster").hide();
        $("#pmviewhistorymain").show();
        $("#gridPMViewALLHistory").hide();



        $("#gridPMViewCustomHistory").show();
        $("#gridPMViewCustomHistoryDetails").show();
        $("#ALLBATCHVIEW").show();
        $("#batchsearch").hide();
        $("#GETALLPmViewALLHistoryBATCHWISE").hide();

    });
    $("#ALLBATCHVIEW").click(function (e) {
        ViewALLHistoryBatch();
        $("#gridPMViewALLHistory").show();
        $("#gridPMViewCustomHistory").hide();
        $("#gridPMViewCustomHistoryDetails").hide();

        $("#ALLBATCHVIEW").hide();
        $("#batchsearch").show();
        $("#GETALLPmViewALLHistoryBATCHWISE").show();
    });
    $('#BatchmyInput').on('input', function (e) {
        var grid = $('#gridPMViewALLHistory').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "BATCHNUMBER" || x.field == "FLAG" || x.field == "CreationTime" || x.field == "TOTALRECORDS"
                    || x.field == "FAILED" || x.field == "CreationTime" || x.field == "UPLOADBY") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $("#ExcelUpload").on("click", function () {

        if ($("#file").val() != "") {
            $("#file").val('') ;
        }
        var dialogs = $("#UploadExcelFile").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialogs.open().element.closest(".k-window").css({
            top: 167,
        });
        dialogs.center();
        
        dialogs.title("Upload File");
    })

    function populateCafeGrid() {

        Utility.Loading();
        var gridVariable = $("#gridPMViewCustomHistory");
            //.height(250);
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "PatientMasterHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            //height: "180px",
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "PatientID", title: "Patient ID", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "PatientName", title: "Patient Name", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "BATCHSTATUS", title: "Error Message", width: "100px", attributes: {
                        "class": "FLAGCELL",
                        style: "text-align: left; font-weight:normal"
                    },
                }


                //{
                //    field: "Edit", title: "Action", width: "50px",

                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    command: [
                //        {
                //            name: 'Edit'
                //        }
                //    ],
                //}
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetPMHISTORY, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            PatientID: { type: "string" },
                            PatientName: { type: "string" },
                            BATCHSTATUS: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.BATCHSTATUS == "Uploaded") {

                        $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //alert("call");
                    }
                    else {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolorred");

                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    $(document).ready(function () {
        var content = "";
        $("#gridPMViewALLHistory").kendoTooltip({
            filter: "td:nth-child(2), th:nth-child(2)",
            position: "center",


            content: function (e) {

                if (e.target.is("th")) {

                    return e.target.text();
                }
                var dataItem = $("#gridPMViewALLHistory").data("kendoGrid").dataItem(e.target.closest("tr"));
                console.log(dataItem);
                if (dataItem.FLAG == "1") {
                    content = "Success";
                }
                else if (dataItem.FLAG == "2") {
                    content = "Partially Success";
                }
                else {
                    content = "Failed";
                }
                return content;
            }
        }).data("kendoTooltip");
        $("#gridPMViewCustomHistoryDetails").kendoTooltip({
            filter: "td:nth-child(2), th:nth-child(2)",
            position: "center",


            content: function (e) {

                if (e.target.is("th")) {

                    return e.target.text();
                }

                var dataItem = $("#gridPMViewCustomHistoryDetails").data("kendoGrid").dataItem(e.target.closest("tr"));
                console.log(dataItem);
                if (dataItem.FLAG == "1") {
                    content = "Success";
                }
                else if (dataItem.FLAG == "2") {
                    content = "Partially Success";
                }
                else {
                    content = "Failed";
                }
                return content;
            }
        }).data("kendoTooltip");
    });

    function populateCafeGridDetails() {

        Utility.Loading();
        var gridVariable = $("#gridPMViewCustomHistoryDetails");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "PatientMasterHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FLAG", title: "Batch Status", width: "80px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    }, template: '<div class="colortag"></div>'
                },
                {
                    field: "TOTALRECORDS", title: "Records", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FAILED", title: "Failed", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    }
                },


                //{
                //    field: "View", title: "View Details", width: "50px",

                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    command: [
                //        {
                //            name: 'View',
                //            click: function (e) {

                //                var gridObj = $("#gridPMViewCustomHistoryDetails").data("kendoGrid");
                //                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //                datamodel = tr;
                //                BATCHNUMBER = tr.BATCHNUMBER;

                //                alert(BATCHNUMBER);
                //                return true;
                //            }
                //        }
                //    ],
                //}
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetPMHISTORYDetails, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            FLAG: { type: "string" },
                            TOTALRECORDS: { type: "string" },
                            FAILED: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.FLAG == "1") {
                        console.log($(this));
                        console.log($(this).find(".colortag"));
                        $(this).find(".colortag").css("background-color", "green");

                    }
                    else if (model.FLAG == "2") {
                        $(this).find(".colortag").css("background-color", "orange");

                    }
                    else {

                        $(this).find(".colortag").css("background-color", "red");
                    }

                    //if (model.FLAG != "Uploaded") {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    //    //alert("call");
                    //}
                    //else {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                    //}


                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function populateGetallBatchwishDetails(BATCHNUMBER) {

        Utility.Loading();
        var gridVariable = $("#GETALLPmViewALLHistoryBATCHWISE");
            //.height(180);
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "PatientMasterLHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            scrollable: true,
            // height: "20px",
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "PatientID", title: "Patient ID", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "PatientName", title: "Patient Name", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FLAG", title: "Record Status", width: "100px", attributes: {
                        "class": "FLAGCELL",
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "CreationTime", title: "Upload Time", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    }
                }



            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetshowGetALLPMHISTORYBATCHWISE, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, {
                            BATCHNUMBER: BATCHNUMBER
                        }
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            PatientID: { type: "string" },
                            PatientName: { type: "string" },
                            FLAG: { type: "string" },
                            CreationTime: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.FLAG == "Success") {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //alert("call");
                    }
                    else if (model.FLAG == "Uploaded" || model.FLAG == "Uploaded") {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //alert("call");
                    }
                    else {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }



    function ViewALLHistoryBatch() {

        Utility.Loading();
        var gridVariable = $("#gridPMViewALLHistory");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "PatientMasterHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "120px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FLAG", title: "Batch Status", width: "60px", attributes: {
                        style: "text-align: center; font-weight:normal",

                    }, template: '<div class="colortag"></div>',
                    headerAttributes: {
                        style: "text-align: center;"
                    }
                },
                {
                    field: "TOTALRECORDS", title: "Records", width: "60px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    }
                },
                {
                    field: "CreationTime", title: "Batch Upload Time", width: "80px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    }
                },
                {
                    field: "FAILED", title: "Failed", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    }
                },
                {
                    field: "UPLOADBY", title: "Upload By", width: "80px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    }
                },


                {
                    field: "View", title: "View Details", width: "50px",

                    attributes: {
                        style: "text-align: center; font-weight:normal;color:red!important",
                        "class": "ViewBatchcell"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'View',
                            click: function (e) {
                                //debugger;
                                var MDgridObj = $("#gridPMViewALLHistory").data("kendoGrid");
                                var tr = MDgridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                BATCHNUMBER = tr.BATCHNUMBER;
                                console.log(e.currentTarget);

                                var trEdit = $(this).closest("tr");
                                $(trEdit).find(".k-grid-View").addClass("k-state-disabled");


                                var dialogs = $("#GETALLPmViewALLHistoryBATCHWISE").data("kendoWindow");
                                populateGetallBatchwishDetails(BATCHNUMBER);
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialogs.open().element.closest(".k-window").css({
                                    left: 305,
                                    top: 215
                                });
                                // dialogs.center();


                                dialogs.title("Batch Details");

                                return false;

                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetALLHISTORYDetailsPatientMaster, function (result) {
                            Utility.UnLoading();
                            
                            if (result != null ) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            FLAG: { type: "string" },
                            TOTALRECORDS: { type: "string" },
                            FAILED: { type: "string" },
                            CreationTime: { type: "string" },
                            UPLOADBY: { type: "string" }

                        }
                    }
                },
                sort: {
                    field: "CreationTime",
                    dir: "desc"
                }
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);
                //$(this).find(".k-grid-View").addClass("ViewBatch");
                    $(this).find(".k-grid-View").addClass("k-grid-Edit");
                    
                    //if (model.FLAG != "Uploaded") {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    //    //alert("call");
                    //}
                    //else {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                    //}
                    if (model.FLAG == "1") {

                        $(this).find(".colortag").css("background-color", "green");

                    }
                    else if (model.FLAG == "2") {
                        $(this).find(".colortag").css("background-color", "orange");

                    }
                    else {

                        $(this).find(".colortag").css("background-color", "red");
                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function UploadSelectedExcelsheet() {

        var data = new FormData();
        var i = 0;
        var fl = $("#file").get(0).files[0];

        if (fl != undefined) {

            data.append("file", fl);

        }
        //HttpClient.MakeSyncRequest(CookBookMasters.UploadExcelsheet, function () { },
        //    {
        //        data: data
        //    } ,
        // null, true);
        Utility.Loading();
        $.ajax({
            type: "POST",
            url: CookBookMasters.UploadExcelsheetPatientMaster,
            contentType: false,
            processData: false,
            async: false,
            data: data,
            success: function (result) {
                if (result == 'PatientID is null') {
                    toastr.error("Can not be PatientID is Null");
                    $(".k-window").hide();
                    $(".k-overlay").hide();


                    //return true;
                    Utility.UnLoading();
                }

                else if (result =="The uploaded file is empty")
                {
                    toastr.error(result);
                    $(".k-window").hide();
                    $(".k-overlay").hide();
                    Utility.UnLoading();
                }
                //else if (result == 'Artical Number is Dupliacte') {
                //    toastr.error("Artical Number is Dupliacte");
                //    $(".k-window").hide();
                //    $(".k-overlay").hide();

                //    //return true;
                //    Utility.UnLoading();
                //}

                else if (result == 'Success') {
                    toastr.success("Patient Master data upload successfully");
                    $(".k-window").hide();
                    $(".k-overlay").hide();
                    //debugger;
                    $("#gridPatientMaster").data("kendoGrid").dataSource.data([]);
                    $("#gridPatientMaster").data("kendoGrid").dataSource.read();
                    // return true;
                    Utility.UnLoading();
                }
                else if (result == 'PatientID does not exits in Patient Master') {
                    toastr.error("PatientID does not exits in Patient Master");
                    $(".k-window").hide();
                    $(".k-overlay").hide();

                    // return true;
                    Utility.UnLoading();
                }
                result = "";
                return false;
            },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3 + " " + p4;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).Message;
                alert(err);
                Utility.UnLoading();
                return false;
            }
        });
    }


    //aplMasterdataSource.push({ "ArticleID": 0, "ArticleDescription": "Select APL" })
    //populateAPLMasterDropdown();

    HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
        user = result;
        //if (user.UserRoleId === 1) {
        //    $("#InitiateBulkChanges").css("display", "inline");
        //    $("#AddNew").css("display", "inline");
        //}
        //if (user.UserRoleId === 2) {
        //    $("#InitiateBulkChanges").css("display", "none");
        //    $("#AddNew").css("display", "none");
        //}
        populatePatientMasterGrid();
        //HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataList, function (data) {
        //    aplMasterdataSource = data.filter(m => m.ArticleType == 'ZFOD' || m.ArticleType == 'ZPRP');
        //    aplMasterdataSource.unshift({ "ArticleID": 0, "ArticleDescription": "Select APL" })



        //}, null, true);
    }, null, true);

    //HttpClient.MakeRequest(CookBookMasters.GetSectorDataList, function (data) {
    //    var dataSource = data;
    //    sectorMasterdataSource = [];
    //    for (var i = 0; i < dataSource.length; i++) {
    //        sectorMasterdataSource.push({ "value": dataSource[i].ID, "text": dataSource[i].SectorName, "SectorNumber": dataSource[i].SectorNumber });
    //    }
    //    populateSectorMasterDropdown();

    //}, null, true);

    //HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

    //    var dataSource = data;
    //    uomdata = [];
    //    uomdata.push({ "value": "Select", "text": "Select" });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        uomdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].Name });
    //    }
    //    console.log(uomdata)
    //    populateUOMDropdown();
    //    populateUOMDropdown_Bulk();
    //}, null, true);

    //$("#gridBulkChange").css("display", "none");
    //populateBulkChangeControls();
    //var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
    //changeControls.css("background-color", "#fff");
    //changeControls.css("border", "none");
    //changeControls.eq(3).text("");
    //changeControls.eq(3).append("<div id='uom' style='width:100%; font-size:10.5px!important'></div>");

    //HttpClient.MakeRequest(CookBookMasters.GetAPLMasterDataList, function (data) {
    //    aplMasterdataSource = data.filter(m => m.ArticleType == 'ZFOD' && m.ArticleType == 'ZPRP');
    //    aplMasterdataSource.unshift({ "ArticleID": 0, "ArticleDescription": "Select APL" })

    //    //populateRegionMasterDropdown();
    //    //$("#btnGo").css("display", "inline-block");

    //}, null , true);
}



function populatePatientMasterGrid() {

    var gridVariable = $("#gridPatientMaster");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "PatientMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        //pageable: true,
        pageable: {
            messages: {
                display: "{0}-{1} of {2} records"
            }
        },
        
        groupable: false,
        height: 485,
        width: 1022,
        //reorderable: true,
        //scrollable: true,
        columns: [
            //{
            //    field: "SimulationCode", title: "Simulation Code", width: "40px", attributes: {
            //        style: "text-align: left; font-weight:normal"
            //    }
            //},
            {
                field: "PatientID",
                title: "Patient ID", width: "85px",
                attributes: {
                    style: "text-align: left; font-weight:normal;text-transform: uppercase;"
                },
                headerAttributes: {
                    style: "text-align: left;"
                }
            },
            {
                field: "PatientName", title: "Patient Name", width: "150px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
         
            {
                field: "IsActive",
                title: "Active", width: "75px",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center;"
                },
                template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
            },
          
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetPMDataList, function (result) {
                        Utility.UnLoading();
                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }
                    }, null
                        //{
                        //filter: mdl
                        //}
                        , false);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        PatientName: { type: "string" },
                        PatientID: { type: "string" },
                        IsActive: { editable: false },
                        //Alias: { type: "string" },
                        //Status: { type: "string" },
                        //PublishedDate: { type: "date" },
                        CreatedOn: { type: "date" },
                        ModifiedOn: { type: "date" },
                        //Sectorids: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
        dataBound: function (e) {
            var items = e.sender.items();
            var grid = this;

            //grid.tbody.find("tr[role='row']").each(function () {
            //    var model = grid.dataItem(this);

            //    if (!model.IsActive || model.Status == "Final") {
            //        //$(this).find(".k-grid-Edit").addClass("k-state-disabled");
            //        $(this).find(".k-grid-Edit").hide();
            //    }
            //    if (model.Status == "Final") {
            //        $(this).find(".k-grid-View").show();
            //    }
            //    else {
            //        $(this).find(".k-grid-View").hide();
            //    }
            //});

            $(".chkbox").on("change", function () {
                // 
                $("#success").css("display", "none");
                $("#error").css("display", "none");
                var gridObj = $("#gridPatientMaster").data("kendoGrid");
                var tr = gridObj.dataItem($(this).closest("tr"));
                datamodel = tr;
                var th = this;
                datamodel.IsActive = $(this)[0].checked;
                var active = "";
                if (datamodel.IsActive) {
                    active = "Active";
                } else {
                    active = "Inactive";
                }
                //console.log("statusmodel")
                //console.log(datamodel)
                Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.PatientName + "</b> " + active + "?", "Patient Master Update Confirmation", "Yes", "No", function () {
                    $("#error").css("display", "none");
                    HttpClient.MakeRequest(CookBookMasters.SaveStatusPatientMasterData, function (result) {
                        
                        if (result != "Success") {
                            //$("#error").css("display", "flex");
                            //$("#error").find("p").text("Some error occured, please try again");
                            toastr.error("Error occured, please try again");
                        }
                        else {
                            //$("#error").css("display", "flex");
                            //$("#success").css("display", "flex");
                            //$("#success").find("p").text("Color updated successfully");
                            toastr.success("Updated successfully");

                        }
                        
                    }, {
                            id: datamodel.ID
                    }, false);
                }, function () {
                    $(th)[0].checked = !datamodel.IsActive;
                    //$("#gridColor").data("kendoGrid").dataSource.data([]);
                    //$("#gridColor").data("kendoGrid").dataSource.read();
                });
                //return true;
            });
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        excelExport: function onExcelExport(e) {
            //var sheet = e.workbook.sheets[0];
            //var data = e.data;
            //var cols = Object.keys(data[0])
            //var columns = cols.filter(function (col) {
            //    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
            //    else
            //        return col;
            //});
            //var columns1 = columns.map(function (col) {
            //    return {
            //        value: col,
            //        autoWidth: true,
            //        background: "#7a7a7a",
            //        color: "#fff"
            //    };
            //});
            //console.log(columns1);
            //var rows = [{ cells: columns1, type: "header" }];

            //for (var i = 0; i < data.length; i++) {
            //    var rowCells = [];
            //    for (var j = 0; j < columns.length; j++) {
            //        var cellValue = data[i][columns[j]];
            //        rowCells.push({ value: cellValue });
            //    }
            //    rows.push({ cells: rowCells, type: "data" });
            //}
            //sheet.rows = rows;
        }
        //,
        //search: {
        //    fields: ["SimulationCode", "Name", "Status", "Version", "CreateOnFormatted"]
        //},        
        //toolbar: ["search"],
    })
    //$(".k-label")[0].innerHTML.replace("items", "records");
    //getDishCategoriesWithDishes();
    //Utility.UnLoading();

    //$("#topHeading").text("My Simulations");
}