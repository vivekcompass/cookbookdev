﻿var user;
var conceptype2data;
var tileImage = null;
var item = null;
var DishCategoryCodeIntial = null;
var datamodel;
$(function () {

    $('#myInput').on('input', function (e) {
        var grid = $('#gridDishCategory').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "DishCategoryCode" || x.field == "Name" || x.field == "SectorName") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#DishwindowEdit").kendoWindow({
            modal: true,
            width: "250px",
            height: "166px",
            title: "Dish Category Details  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });

        $("#btnExportDish").click(function (e) {
            var grid = $("#gridDishCategory").data("kendoGrid");
            grid.saveAsExcel();
        });

        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;

            populateDishCategoryGrid();
        }, null, false);
    });

    function dcvalidate() {
        var valid = true;

        if ($("#dcname").val() === "") {
            toastr.error("Please provide input");
            $("#dcname").addClass("is-invalid");
            valid = false;
            $("#dcname").focus();
        }
        if (($.trim($("#dcname").val())).length > 30) {
            toastr.error("Dish Category accepts upto 30 charactre");

            $("#dcname").addClass("is-invalid");
            valid = false;
            $("#dcname").focus();
        }
        return valid;
    }
    //Dish Category Section Start

    function populateDishCategoryGrid() {

        Utility.Loading();
        var gridVariable = $("#gridDishCategory");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "SectorName", title: "Sector Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "DishCategoryCode", title: "Dish Category Code", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                
                //{
                //    field: "IsActive", title: "Active", width: "75px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" disabled class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //}
                //{
                //    field: "Edit", title: "Action", width: "50px",
                //    attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    },
                //    command: [
                //        {
                //            name: 'Edit',
                //            click: function (e) {
                //                var dialog = $("#DishwindowEdit").data("kendoWindow");
                //                //var gridObj = gridVariable.data("kendoGrid");
                //                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //                var tr = $(e.target).closest("tr");
                //                // get the current table row (tr)

                //                var item = this.dataItem(tr);          // get the date of this row
                //                if (!item.IsActive) {
                //                    return;
                //                }// get the current table row (tr)
                //                var model = {
                //                    "ID": item.ID,
                //                    "Name": item.Name,
                //                    "IsActive": item.IsActive,
                //                    "DishCategoryCode": item.DishCategoryCode
                //                };
                //                DishCategoryCodeIntial = item.DishCategoryCode;


                //                dialog.open();
                //                dialog.center();

                //                datamodel = model;

                //                $("#dcid").val(model.ID);
                //                $("#dccode").val(model.DishCategoryCode);
                //                $("#dcname").val(model.Name);

                //                $("#dcname").focus();
                //                return true;
                //            }
                //        }
                //    ],
                //}
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetDishCategoryDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { type: "string" },
                            DishCategoryCode: { type: "string" },
                            SectorName: { type: "string" },
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
               
                $(".chkbox").on("change", function () {
                    var gridObj = $("#gridDishCategory").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    var trEdit = $(this).closest("tr");
                    var th = this;
                    datamodel = tr;
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Dish Category " + active + "?", " Dish Category Update Confirmation", "Yes", "No", function () {
                        HttpClient.MakeRequest(CookBookMasters.SaveDishCategory, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                toastr.success("Dish Category updated successfully");
                                $(th)[0].checked = datamodel.IsActive;
                                if ($(th)[0].checked) {
                                    $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                } else {
                                    $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                }
                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {

                        $(th)[0].checked = !datamodel.IsActive;
                        if ($(th)[0].checked) {
                            $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                        } else {
                            $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                        }
                    });
                    return true;
                });

            },
            change: function (e) {
            },
        })

        //  var gridVariable = $("#gridDishCategory").data("kendoGrid");
        //sort Grid's dataSource
        gridVariable.data("kendoGrid").dataSource.sort({ field: "DishCategoryCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }


    $("#dccancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#DishwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#dcaddnew").on("click", function () {
        //
        // var jq = jQuery.noConflict();
        // alert("calick");
        var model;

        var dialog = $("#DishwindowEdit").data("kendoWindow");

        //dialog.open();
        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });
        // dialog.center();

        datamodel = model;

        dialog.title("New Dish Category Creation");

        $("#dcname").removeClass("is-invalid");
        $('#dcsubmit').removeAttr("disabled");
        $("#dcid").val("");
        $("#dcname").val("");
        $("#dccode").val("");
        $("#dcname").focus();
        DishCategoryCodeIntial = null;

    });





    $("#dcsubmit").click(function () {
        if ($("#dcname").val() === "") {
            toastr.error("Please provide Dish Category Name");
            $("#dcname").focus();
            return;
        }

        if (dcvalidate() === true) {
            $("#dcname").removeClass("is-invalid");

            var model = datamodel;

            model = {
                "ID": $("#dcid").val(),
                "Name": $("#dcname").val(),
                "DishCategoryCode": DishCategoryCodeIntial,

            }

            if (DishCategoryCodeIntial == null) {
                model.IsActive = 1;
            }
            else {
                model.IsActive = datamodel.IsActive;
            }
            $("#dcsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveDishCategory, function (result) {
                if (result == false) {
                    $('#dcsubmit').removeAttr("disabled");
                    toastr.success("There was some error, the record cannot be saved");

                }
                else {
                    $(".k-overlay").hide();
                    $('#dcsubmit').removeAttr("disabled");

                    if (model.ID > 0)
                        toastr.success("Dish Category record updated successfully");
                    else
                        toastr.success("New Dish Category record added successfully");
                    $(".k-overlay").hide();
                    var orderWindow = $("#DishwindowEdit").data("kendoWindow");
                    orderWindow.close();
                    $("#gridDishCategory").data("kendoGrid").dataSource.data([]);
                    $("#gridDishCategory").data("kendoGrid").dataSource.read();
                    $("#gridDishCategory").data("kendoGrid").dataSource.sort({ field: "DishCategoryCode", dir: "asc" });



                }
            }, {
                model: model

            }, true);
        }
    });



});