﻿//import { Util } from "../bootstrap/js/bootstrap.bundle";

var mogWiseAPLMasterdataSource = [];
var checkedIds = {};
var checkedAPLCodes = [];
var user;
var status = "";
var varname = "";
var datamodel;
var MOGName = "";
var BATCHNUMBER = "";
var NewMOGName = "";
var MOGCode = "";
var MOGID = 0;
var uomdata = [];
var aplMasterdataSource = [];
var sectorMasterdataSource = [];
var isShowPreview = false;
var CheckedTrueAPLCodes = [];
var mogWiseAPLArray = [];
var mogImpactedData = [];
var isStatusSave = false;
var mogStatus = false;
var isRIEnabled = false;
var isMOGAPLMappingSave = false;
var aplFilterDataSource = [];

$(function () {

    


    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        Utility.Loading();
        setTimeout(function () {
            populateMOGGrid();
            Utility.UnLoading();
        }, 2000);
    });


    $("#functionwindowEdit").kendoWindow({
        modal: true,
        width: "475px",
        height: "154px",
        title: "Function Details Add ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#functionwindowEdit1").kendoWindow({
        modal: true,
        width: "475px",
        height: "154px",
        title: "Function Details Update",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#Functioncancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#functionwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });
    $("#FunctioncancelEdit1").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#functionwindowEdit1").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });
    
    $('#myInput').on('input', function (e) {
        var grid = $('#gridFunctionList').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "Code" || x.field == "Name" || x.field == "Description" || x.field == "Remarks") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        if (x.field == "Status") {
                            var pendingString = "pending";
                            var savedString = "saved";
                            var mappedString = "mapped";
                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "3";
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
    $("#btnExport").click(function (e) {
        var grid = $("#gridFunctionList").data("kendoGrid");
        grid.saveAsExcel();
    });

        function populateMOGGrid() {

            debugger;
            var gridVariable = $("#gridFunctionList");
            gridVariable.html("");
            gridVariable.kendoGrid({
                excel: {
                    fileName: "MOG.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                groupable: false,

                //pageable: true,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    {
                        field: "Code", title: "Function Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: left;"
                        }
                    },
                    {
                        field: "Name", title: "Function Name", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },

                    },
                    {
                        field: "Description", title: "Description", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },

                    {
                        field: "Remarks", title: "Remarks", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },

                    //{
                    //    field: "IsActive", title: "Active", width: "75px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                    //},



                    {
                        field: "Edit", title: "Action", width: "35px",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                        command: [
                            {
                                name: 'Edit',
                                click: function (e) {

                                    var dialog = $("#functionwindowEdit1").data("kendoWindow");
                                    var tr = $(e.target).closest("tr");
                                    var gridObj = $("#gridFunctionList").data("kendoGrid");
                                    tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    // get the date of this row
                                    $("#functionid").val(tr.ID);



                                    var model = {
                                        "ID": tr.ID,
                                        "Name": tr.Name,
                                        "Description": tr.Description,
                                        "Remarks": tr.Remarks,

                                    };
                                    dialog.open();
                                    dialog.center();

                                    datamodel = model;


                                    $("#txt_FunctionnameEdit1").val(model.Name);
                                    $("#txt_FunctiondescEdit1").val(model.Description);
                                    $("#txt_FunctionRemarksEdit1").val(model.Remarks);

                                    return true;
                                }
                            }
                            ,

                        ],
                    },


                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetFunctionMasterList, function (result) {
                                Utility.UnLoading();
                                if (result != null) {

                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , false);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                ID: { type: "string" },
                                Code: { type: "string" },
                                Name: { type: "string" },
                                Description: { type: "string" },
                                IsActive: { type: "string" },
                                Remarks: { type: "string" },
                                
                            }
                        }
                    },
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {


                    var items = e.sender.items();
                    var grid = this;
                    grid.tbody.find("tr[role='gridcell']").each(function () {
                        var model = grid.dataItem(this);

                        if (!model.IsActive) {
                            $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                        }
                    });
                    var items = e.sender.items();
                    $(".chkbox").on("change", function () {

                        var gridObj = $("#gridFunctionList").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        var trEdit = $(this).closest("tr");
                        var th = this;
                        datamodel = tr;

                        if ($(this)[0].checked == false) {
                            datamodel.IsActive = 0;
                        }
                        else {
                            datamodel.IsActive = 1;
                        }

                        var active = "";
                        if (datamodel.IsActive) {
                            active = "Active";
                        } else {
                            active = "Inactive";
                        }

                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Sector " + active + "?", " Sector Update Confirmation", "Yes", "No", function () {
                            HttpClient.MakeRequest(CookBookMasters.UpdatefunctionInactiveActive, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again");
                                }
                                else {
                                    toastr.success("Function updated successfully");
                                    $(th)[0].checked = datamodel.IsActive;
                                    if ($(th)[0].checked) {
                                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                    } else {
                                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                    }
                                }
                            }, {
                                model: datamodel
                            }, false);
                        }, function () {

                            $(th)[0].checked = !datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        });
                        return true;
                    });
                   
                    
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
            //    if ($(".k-label")[0]!= null)
            //$(".k-label")[0].innerHTML.replace("items", "records");
        }
    $("#FunctionAddNew").on("click", function () {

        var model;

        var dialog = $("#functionwindowEdit").data("kendoWindow");

        //dialog.open();
        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });
        dialog.center();

        datamodel = model;

        dialog.title("New Function Details");



    });
    //on click of the checkbox:
    $("#FunctionSubmit").click(function () {

        if ($("#txt_Functionname").val() === "") {
            toastr.error("Please Provide Function Name");
            $("#txt_Functionname").focus();
            return false;
        }
        else if ($("#txt_Functiondesc").val() === "") {
            toastr.error("Please Provide Description");
            $("#txt_Functiondesc").focus();
            return false;
        }
        else if ($("#txt_FunctionRemarks").val() === "") {
            toastr.error("Please Provide Remarks");
            $("#txt_FunctionRemarks").focus();
            return false;
        }


        var model = datamodel;

        model = {

            "Name": $("#txt_Functionname").val(),
            "Description": $("#txt_Functiondesc").val(),
            "Remarks": $("#txt_FunctionRemarks").val(),
        }

        $("#FunctionSubmit").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.SaveFunctionLevelInfo, function (result) {

            if (result.Data == 'Success') {
                $('#FunctionSubmit').removeAttr("disabled");
                toastr.success("New function record added successfully");
                $(".k-window").hide();
                $(".k-overlay").hide();


                //return true;

                var orderWindow = $("#functionwindowEdit").data("kendoWindow");
                orderWindow.close();
                $("#gridFunctionList").data("kendoGrid").dataSource.data([]);
                $("#gridFunctionList").data("kendoGrid").dataSource.read();
                Utility.UnLoading();
                $("#txt_Functionname").val('');
                $("#txt_Functiondesc").val('');
                $("#txt_FunctionRemarks").val('');
                return;
            }
            else {
                $(".k-overlay").hide();

                toastr.warning("Something is wrong");
                $(".k-window").hide();
                $(".k-overlay").hide();
                return;
            }
        }, {
            model: model

        }, true);

        Utility.UnLoading();
        //populateCafeGrid();
    }); 


    $("#FunctionSubmitEdit1").click(function () {

        if ($("#txt_FunctionnameEdit1").val() === "") {
            toastr.error("Please Provide Function Name");
            $("#txt_FunctionnameEdit1").focus();
            return false;
        }
        else if ($("#txt_FunctiondescEdit1").val() === "") {
            toastr.error("Please Provide Description");
            $("#txt_FunctiondescEdit1").focus();
            return false;
        }
        else if ($("#txt_FunctionRemarksEdit1").val() === "") {
            toastr.error("Please Provide Remarks");
            $("#txt_FunctionRemarksEdit1").focus();
            return false;
        }


        var model = datamodel;

        model = {
            "ID": $("#functionid").val(),
            "Name": $("#txt_FunctionnameEdit1").val(),
            "Description": $("#txt_FunctiondescEdit1").val(),
            "Remarks": $("#txt_FunctionRemarksEdit1").val(),
        }

        $("#FunctionSubmitEdit1").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.UpdateFunctionLevelInfo, function (result) {

            if (result.Data == 'Success') {
                $('#FunctionSubmitEdit1').removeAttr("disabled");
                toastr.success("function record update successfully");
                $(".k-window").hide();
                $(".k-overlay").hide();


                //return true;

                var orderWindow = $("#functionwindowEdit1").data("kendoWindow");
                orderWindow.close();
                $("#gridFunctionList").data("kendoGrid").dataSource.data([]);
                $("#gridFunctionList").data("kendoGrid").dataSource.read();
                Utility.UnLoading();
                $("#txt_FunctionnameEdit1").val('');
                $("#txt_FunctiondescEdit1").val('');
                $("#txt_FunctionRemarksEdit1").val('');
                return;
            }
            else {
                $(".k-overlay").hide();

                toastr.warning("Something is wrong");
                $(".k-window").hide();
                $(".k-overlay").hide();
                return;
            }
        }, {
            model: model

        }, true);

        Utility.UnLoading();
        //populateCafeGrid();
    }); 

});













$("document").ready(function () {
    // showMOGAPLDependencies()
    $(".k-window").hide();
    $(".k-overlay").hide();
    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();

    });
});
