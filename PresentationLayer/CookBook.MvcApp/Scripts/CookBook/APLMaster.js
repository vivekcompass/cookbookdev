﻿var datamodel;
var aplMasterDataSource = [];
var allergenMasterDataSource = [];
var allergenTop8MasterDataSource = [];
var allergenOtherMasterDataSource = [];
var filtteredAplMasterDataSource = [];
//Krish 11-10-2022
var rangeTypeData = [];
$(function () {
    var user;
    var status = "";
    var varname = "";
    var ArticleName = "";
    var mogdata = [];
    var dkdata = [];
    var isMappedAPL = false;

    $("#APLwindowEdit").kendoWindow({
        modal: true,
        width: "720px",
        height: "245px",
        title: "APL Details - " + ArticleName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#UploadExcelFileAPL").kendoWindow({
        modal: true,
        width: "575px",
        height: "105px",
        title: "MOG EXCEL",
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#GETALLMogViewALLHistoryBATCHWISEAPL").kendoWindow({
        modal: true,
        width: "975px",
        height: "105px",
        title: "MOG EXCEL",
        actions: ["Close"],
        visible: false,
        animation: false

    });
    //Krish 11-10-2022
    $("#RangeTypeAPLwindowEdit").kendoWindow({
        modal: true,
        width: "500px",
        height: "220px",
        title: "", 
        actions: ["Close"],
        visible: false,
        animation: false,
        resizable: false
    });
    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        Utility.Loading();
        setTimeout(function () {
            onLoad();
            Utility.UnLoading();
        }, 2000);
    });
    function onLoad() {
        $('#BatchmyInputAPL').on('input', function (e) {
            var grid = $('#gridMogViewALLHistoryAPL').data('kendoGrid');
            var columns = grid.columns;

            var filter = { logic: 'or', filters: [] };
            columns.forEach(function (x) {
                if (x.field) {
                    if (x.field == "BATCHNUMBER" || x.field == "FLAG" || x.field == "CreationTime" || x.field == "TOTALRECORDS"
                        || x.field == "FAILED" || x.field == "CreationTime" || x.field == "UPLOADBY") {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;

                        if (type == 'string') {
                            var targetValue = e.target.value;
                            filter.filters.push({
                                field: x.field,
                                operator: 'contains',
                                value: targetValue
                            })
                        }
                        else if (type == 'number') {

                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        } else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format(x.format, data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                }
            });
            grid.dataSource.filter(filter);
        });

        HttpClient.MakeRequest(CookBookMasters.GetAllergenDataList, function (data) {
            var dataSource = data;
            allergenMasterDataSource = [];
            for (var i = 0; i < dataSource.length; i++) {
                allergenMasterDataSource.push({
                    "value": dataSource[i].AllergenCode, "text": dataSource[i].Name, "AllergenCode": dataSource[i].AllergenCode, "Top8": dataSource[i].Top8
                });
            }
            allergenTop8MasterDataSource = allergenMasterDataSource.filter(m => m.Top8);
            allergenOtherMasterDataSource = allergenMasterDataSource.filter(m => !m.Top8);
            populateAllergenMasterDropdown();
            populateOtherAllergenMasterDropdown();

        }, null, true);
    }
    $("#APLMOGbtnbackAPL").click(function () {
        //alert("call");
        $("#SiteMaster").show();
        $("#mogviewhistorymainAPL").hide();
    });
    $("#BtnviewuploadhistoryAPL").click(function (e) {
       
        populateCafeGrid();
        populateCafeGridDetails();

        $("#SiteMaster").hide();
        $("#mogviewhistorymainAPL").show();
        $("#gridMogViewALLHistoryAPL").hide();



        $("#gridMogViewCustomHistoryAPL").show();
        $("#gridMogViewCustomHistoryDetailsAPL").show();
        $("#ALLBATCHVIEWAPL").show();
        $("#batchsearchAPL").hide();
        $("#GETALLMogViewALLHistoryBATCHWISEAPL").hide();

    });
    $("#BtnExportTempAPL").click(function (e) {        
        window.location.href = 'APLMaster/DownloadExcTemplate';
    });
    $("#ExcelUploadAPL").on("click", function () {

        if ($("#fileAPL").val() != "") {
            $("#fileAPL").val('');
        }
        var dialogs = $("#UploadExcelFileAPL").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialogs.open().element.closest(".k-window").css({
            top: 167,
        });
        dialogs.center();

        dialogs.title("Upload Excel");
    })

    $('#file').change(function () {

        var that = this;
        if (validateImportFile(that.value)) {
            if (that.files[0]) {
                var uploadimg = new FileReader();
                uploadimg.onload = function (displayimg) {
                    $("#image").attr('src', displayimg.target.result);
                    $("#removeImage").css("display", "block");
                }
            }
        }
        else {
            $("#removeImage").css("display", "none");
            //ImportReport.refreshPage();
        }
    });

    function validateImportFile(filename) {

        if (filename != '') {
            var fileExtension = ['xlsx'];
            if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == -1) {
                $("#file").val('')
                toastr.error("The uploader accepts only xlsx files.");
                return false;
            }
        }
        return true;
    };

    $("#UploadMOGExcelAPL").click(function (e) {
        if ($("#fileAPL").val() != "") {
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
            /*Checks whether the file is a valid excel file*/
            if (!regex.test($("#fileAPL").val().toLowerCase())) {
                toastr.error("Please upload a valid Excel file!");
                return false;
            }
            else {
                UploadSelectedExcelsheet();

            }
        }
        else {
            toastr.error("Please upload a Excel file!");
            return false;
        }
    });
    $("#ALLBATCHVIEWAPL").click(function (e) {
        ViewALLHistoryBatch();
        $("#gridMogViewALLHistoryAPL").show();
        $("#gridMogViewCustomHistoryAPL").hide();
        $("#gridMogViewCustomHistoryDetailsAPL").hide();

        $("#ALLBATCHVIEWAPL").hide();
        $("#batchsearchAPL").show();
        $("#GETALLMogViewALLHistoryBATCHWISEAPL").show();
    });
    function populateCafeGrid() {

        Utility.Loading();
        var gridVariable = $("#gridMogViewCustomHistoryAPL").height(180);
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "MogAPLHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            height: "180px",
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "MOGCode", title: "MOG Code", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ArticalNumber", title: "Article Number", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "BATCHSTATUS", title: "Error Message", width: "100px", attributes: {
                        "class": "FLAGCELL",
                        style: "text-align: left; font-weight:normal"
                    },
                }

            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGAPLHISTORYAPL, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            MOGCode: { type: "string" },
                            ArticalNumber: { type: "string" },
                            BATCHSTATUS: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.BATCHSTATUS == "Uploaded") {

                        $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //alert("call");
                    }
                    else {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolorred");

                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                debugger;
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid"
                        || col == "SiteDescription" || col == "CreatedBy" || col == "CreatedDate" || col == "ModifiedBy" || col == "ModifiedDate"
                        || col == "BrandName" || col == "StandardCostPerKg" || col == "ArticleCost" || col == "MOGStatus" || col == "SiteName"
                        || col == "SectorName" || col == "AllergensName" || col == "AllergensCode" || col == "NumberOfRows" || col == "OtherAllergensName"
                        || col == "OtherAllergensCode" || col == "IsMaxAPLCost") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }  

    function populateCafeGridDetails() {

        Utility.Loading();
        var gridVariable = $("#gridMogViewCustomHistoryDetailsAPL");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "MogAPLHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FLAG", title: "Batch Status", width: "80px", attributes: {
                        "class": "FLAGCELL",
                        style: "text-align: left; font-weight:normal"
                    }, template: '<div class="colortag"></div>',
                },
                {
                    field: "TOTALRECORDS", title: "Records", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "FAILED", title: "Failed", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },


            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGAPLHISTORYDetailsAPL, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            FLAG: { type: "string" },
                            TOTALRECORDS: { type: "string" },
                            FAILED: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    //if (model.FLAG != "Uploaded") {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    //    //alert("call");
                    //}
                    //else {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                    //}
                    if (model.FLAG == "1") {

                        $(this).find(".colortag").css("background-color", "green");

                    }
                    else if (model.FLAG == "2") {
                        $(this).find(".colortag").css("background-color", "orange");

                    }
                    else {

                        $(this).find(".colortag").css("background-color", "red");
                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                debugger;
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }  

    function populateAllergenMasterDropdown() {

        $("#inputAllergen").kendoMultiSelect({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: allergenTop8MasterDataSource,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
        });
        var multiselect = $("#inputAllergen").data("kendoMultiSelect");

        //clear filter
        //multiselect.dataSource.filter({});

        //set value
        // multiselect.value(mogWiseAPLArray);

    }

    function populateOtherAllergenMasterDropdown() {

        $("#inputOtherAllergen").kendoMultiSelect({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: allergenOtherMasterDataSource,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
        });
        var multiselect = $("#inputOtherAllergen").data("kendoMultiSelect");

        //clear filter
        //multiselect.dataSource.filter({});

        //set value
        // multiselect.value(mogWiseAPLArray);

    }

    function UploadSelectedExcelsheet() {

        var data = new FormData();
        var i = 0;
        var fl = $("#fileAPL").get(0).files[0];

        if (fl != undefined) {

            data.append("file", fl);

        }
        //HttpClient.MakeSyncRequest(CookBookMasters.UploadExcelsheet, function () { },
        //    {
        //        data: data
        //    } ,
        // null, true);
        Utility.Loading();
        $.ajax({
            type: "POST",
            url: CookBookMasters.UploadExcelsheet,
            contentType: false,
            processData: false,
            async: false,
            data: data,
            success: function (result) {
                if (result == 'MOG Number is null') {
                    toastr.error("Can not be MOG Number is Null");
                    $(".k-window").hide();
                    $(".k-overlay").hide();


                    //return true;
                    Utility.UnLoading();
                }
                else if (result == 'Success') {
                    toastr.success("APL bulk data upload successfully");
                    $(".k-window").hide();
                    $(".k-overlay").hide();

                    // return true;
                    Utility.UnLoading();
                }
                else if (result == 'MOG Number does not exits in APL Master') {
                    toastr.error("MOG Number does not exits in APL Master");
                    $(".k-window").hide();
                    $(".k-overlay").hide();

                    // return true;
                    Utility.UnLoading();
                }
                else {
                    toastr.error("Something want Wrong.");
                    $(".k-window").hide();
                    $(".k-overlay").hide();

                    // return true;
                    Utility.UnLoading();
                }
                result = "";
                return false;
            },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3 + " " + p4;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).Message;
                alert(err);
                Utility.UnLoading();
                return false;
            }
        });
    }
    $(document).ready(function () {
        var content = "";
        $("#gridMogViewALLHistoryAPL").kendoTooltip({
            filter: "td:nth-child(2), th:nth-child(2)", 
            position: "center",
            
            
            content: function (e) {
                
                if (e.target.is("th")) {
                   
                    return e.target.text();
                }

                var dataItem = $("#gridMogViewALLHistoryAPL").data("kendoGrid").dataItem(e.target.closest("tr"));
                console.log(dataItem);
                if (dataItem.FLAG == "1") {
                    content = "Success";
                }
                else if (dataItem.FLAG == "2") {
                    content = "Partially Success";
                }
                else  {
                    content = "Failed";
                }
                return content;
            }
        }).data("kendoTooltip");
        $("#gridMogViewCustomHistoryDetailsAPL").kendoTooltip({
            filter: "td:nth-child(2), th:nth-child(2)",
            position: "center",


            content: function (e) {

                if (e.target.is("th")) {

                    return e.target.text();
                }

                var dataItem = $("#gridMogViewCustomHistoryDetailsAPL").data("kendoGrid").dataItem(e.target.closest("tr"));
                console.log(dataItem);
                if (dataItem.FLAG == "1") {
                    content = "Success";
                }
                else if (dataItem.FLAG == "2") {
                    content = "Partially Success";
                }
                else {
                    content = "Failed";
                }
                return content;
            }
        }).data("kendoTooltip");
    });

    
    function ViewALLHistoryBatch() {

        Utility.Loading();
        var gridVariable = $("#gridMogViewALLHistoryAPL");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "MogAPLHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FLAG", title: "Batch Status", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal",
                        
                    }, template: '<div class="colortag"></div>',
                },
                {
                    field: "TOTALRECORDS", title: "Records", width: "80px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "CreationTime", title: "Batch Upload Time", width: "80px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "FAILED", title: "Failed", width: "80px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "UPLOADBY", title: "Upload By", width: "80px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },


                {
                    field: "View", title: "View Details", width: "50px",

                    attributes: {
                        style: "text-align: center; font-weight:normal;color:red!important",
                        "class": "ViewBatchcell"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'View',
                            click: function (e) {
                                debugger;
                                var MDgridObj = $("#gridMogViewALLHistoryAPL").data("kendoGrid");
                                var tr = MDgridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                BATCHNUMBER = tr.BATCHNUMBER;
                                console.log(e.currentTarget);

                                var trEdit = $(this).closest("tr");
                                $(trEdit).find(".k-grid-View").addClass("k-state-disabled");


                                var dialogs = $("#GETALLMogViewALLHistoryBATCHWISEAPL").data("kendoWindow");
                                populateGetallBatchwishDetails(BATCHNUMBER);
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialogs.open().element.closest(".k-window").css({
                                    left: 305,
                                    top: 215
                                });
                                // dialogs.center();


                                dialogs.title("Batch Details");

                                return false;

                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetALLHISTORYDetailsAPL, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            FLAG: { type: "string" },
                            TOTALRECORDS: { type: "string" },
                            FAILED: { type: "string" },
                            CreationTime: { type: "string" },
                            UPLOADBY: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);
                    $(this).find(".k-grid-View").addClass("ViewBatch");
                    //if (model.FLAG != "Uploaded") {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    //    //alert("call");
                    //}
                    //else {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                    //}
                    if (model.FLAG == "1") {

                        $(this).find(".colortag").css("background-color", "green");
                       
                        
                    }
                    else if (model.FLAG == "2") {
                        $(this).find(".colortag").css("background-color", "orange");

                    }
                    else {

                        $(this).find(".colortag").css("background-color", "red");
                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                debugger;
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }  

    function populateGetallBatchwishDetails(BATCHNUMBER) {

        Utility.Loading();
        var gridVariable = $("#GETALLMogViewALLHistoryBATCHWISEAPL").height(180);
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "MogAPLHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            scrollable: true,
            // height: "20px",
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "MOGCode", title: "MOG Code", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ArticalNumber", title: "Article Number", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "FLAG", title: "Record Status", width: "100px", attributes: {
                        "class": "FLAGCELL",
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "CreationTime", title: "Upload Time", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                }



            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetshowGetALLHISTORYBATCHWISEAPL, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, {
                            BATCHNUMBER: BATCHNUMBER
                        }
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            MOGCode: { type: "string" },
                            ArticalNumber: { type: "string" },
                            FLAG: { type: "string" },
                            CreationTime: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.FLAG == "Success") {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //alert("call");
                    }
                    else if (model.FLAG == "Uploaded" || model.FLAG == "Uploaded") {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //alert("call");
                    }
                    else {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                debugger;
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }  

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }


    $('#myInput').on('input', function (e) {
        var grid = $('#gridAPLMaster').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ArticleNumber" || x.field == "ArticleDescription" || x.field == "UOM" || x.field == "ArticleType"
                    || x.field == "Hierlevel3" || x.field == "MerchandizeCategoryDesc" || x.field == "MOGName" || x.field == "AllergensName"
                    || x.field == "OtherAllergensName" || x.field =="RangeTypeNameFormatted") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    var user;
    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#btnExport").click(function (e) {
            debugger;
            setTimeout(Utility.Loading(),1000)
            var grid = $("#gridAPLMaster").data("kendoGrid");
            setTimeout(function () {
                Utility.UnLoading();
                grid.saveAsExcel();
            }, 2000)
            //Utility.UnLoading();
            //grid.saveAsExcel();
        });
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            if (user.UserRoleId === 1) {
                $("#InitiateBulkChanges").css("display", "inline");
                $("#AddNew").css("display", "inline");
            }
            if (user.UserRoleId === 2) {
                $("#InitiateBulkChanges").css("display", "none");
                $("#AddNew").css("display", "none");
            }
            //Krish 11-10-2022
            if (user.UserRoleId ===14) {
                $("#btnExport").css("display", "none");
                $("#BtnExportTempAPL").css("display", "none");
                $("#BtnviewuploadhistoryAPL").css("display", "none");
                $("#ExcelUploadAPL").css("display", "none");
                $("#CancelBulkChanges").css("display", "none");
                $("#SaveBulkChanges").css("display", "none");
                $("#previewchk").css("display", "none");
            }
        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetMOGDataList, function (data) {
            
            var dataSource = data;
            mogdata = [];
            mogdata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                mogdata.push({ "value": dataSource[i].MOGCode, "text": dataSource[i].Name });
            }
            $("#inputmog").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: mogdata,
                index: 0,
            });

            $("#mog").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: mogdata,
                index: 0,
            });


            $("#inputmog").data("kendoDropDownList").select(function (dataItem) {
                return dataItem.value === dataSource[0].value;
            });
            $("#inputmog").data("kendoDropDownList").select(0);
        }, null, false);

        HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataList, function (result) {
           
            if (result != null) {
                aplMasterDataSource = result;
                filtteredAplMasterDataSource = aplMasterDataSource;
                populateAPLMasterGrid();
            }
        }, null
            , true);

        $("#gridBulkChange").css("display", "none");
        populateBulkChangeControls();
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        changeControls.css("background-color", "#fff");
        changeControls.css("border", "none");
       
        //$("#gridBulkChange").children(".k-grid-header").css("border", "none");
        //$("#InitiateBulkChanges").css("display", "inline");
        //$("#CancelBulkChanges").css("display", "none");
        //$("#SaveBulkChanges").css("display", "none");
        //$("#bulkerror").css("display", "none");
        //$("#success").css("display", "none");
        changeControls.eq(6).text("");
        changeControls.eq(6).append("<div id='mog' style='width:100%; margin-left:3px; font-size:10.5px!important'></div>");
        //$(".k-label")[0].innerHTML.replace("items", "records");
    });

    $("#InitiateBulkChanges").on("click", function () {
        $("#gridBulkChange").css("display", "block");
        $("#gridBulkChange").children(".k-grid-header").css("border", "none");
        $("#InitiateBulkChanges").css("display", "none");
        $("#CancelBulkChanges").css("display", "inline");
        $("#SaveBulkChanges").css("display", "inline");
    });

    $("#CancelBulkChanges").on("click", function () {
        $("#InitiateBulkChanges").css("display", "inline");
        $("#CancelBulkChanges").css("display", "none");
        $("#SaveBulkChanges").css("display", "none");
        $("#gridBulkChange").css("display", "none");
        populateAPLMasterGrid();
    });

    $("#SaveBulkChanges").on("click", function () {
        
        var neVal = $(this).val();
        var dataFiltered = $("#gridAPLMaster").data("kendoGrid").dataSource.dataFiltered();
        
        
        var model = dataFiltered;
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        var validChange = false;
        var mog = "";

        var mogspan = $("#mog");//  changeControls.eq(4).children("span").children("span").children(".k-input");
        
        if (mogspan.val() != "Select") {
            validChange = true;
            mog = mogspan.val();
        }
        
        if (validChange == false) {
            toastr.error("Please change at least one attribute for Bulk Update");
        } else {
            Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                function () {
                    $.each(model, function () {
                        this.CreatedDate = kendo.parseDate(this.CreatedDate)
                        if (mog.length > 0)
                            this.MOGCode = mog;
                    });
                    debugger;
                    HttpClient.MakeSyncRequest(CookBookMasters.SaveAPLMasterDataList, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again.");
                        }
                        else {
                            $(".k-overlay").hide();
                            toastr.success("MOG configuration updated successfully");
                            $("#gridAPLMaster").data("kendoGrid").dataSource.data([]);
                            $("#gridAPLMaster").data("kendoGrid").dataSource.read();
                            $("#InitiateBulkChanges").css("display", "inline");
                            $("#CancelBulkChanges").css("display", "none");
                            $("#SaveBulkChanges").css("display", "none");
                            $("#gridBulkChange").css("display", "none");
                            // window.location.href = "/APLMaster/Index";
                            Utility.Loading();
                        }
                    },
                        {
                        model: model
                        }, true);
                    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataList, function (result) {
                        if (result != null) {
                            aplMasterDataSource = result;
                            filtteredAplMasterDataSource = aplMasterDataSource;
                            populateAPLMasterGrid();
                            Utility.UnLoading();
                        }
                    }, null
                        , true);
                    //populateAPLMasterGrid();
                    Utility.UnLoading();
                },
                function () {
                    maptype.focus();
                }
            );
        }
        
    });
    //Food Program Section Start

    $("#APLbtnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#APLwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#APLbtnSubmit").click(function () {
       
        if ($("#inputmog").val() === "Select") {
            toastr.error("Please select the MOG to proceed");
            $("#inputmog").focus();
        }
        else {
            
            $("#error").css("display", "none");

            var Allergendata = [];
            var Allergens = "";
            var dropdownlist = $("#inputAllergen").data("kendoMultiSelect");
            var dataItems = dropdownlist.dataItems();
            if (dataItems != undefined && dataItems != null) {
                for (dataItem of dataItems) {
                    Allergendata.push(dataItem.value);
                }
            }

            var dropdownlistOther = $("#inputOtherAllergen").data("kendoMultiSelect");
            var dataItemsOther = dropdownlistOther.dataItems();
            if (dataItemsOther != undefined && dataItemsOther != null) {
                for (dataItem of dataItemsOther) {
                    Allergendata.push(dataItem.value);
                }
            }
            Allergens = Allergendata.toString();

            var model;
            if (datamodel != null) {
                model = datamodel;
                model.MOGCode = $("#inputmog").val();
                model.CreatedDate = kendo.parseDate(datamodel.CreatedDate);
                model.AllergensCode = Allergens;
            }

           
            $("#APLbtnSubmit").attr('disabled', 'disabled');
            Utility.Loading();
            HttpClient.MakeRequest(CookBookMasters.SaveAPLMasterData, function (result) {
                
                if (result == false) {
                    $('#APLbtnSubmit').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputmog").focus();
                    Utility.UnLoading();
                }
                else {
                    
                    $(".k-overlay").hide();
                    var orderWindow = $("#APLwindowEdit").data("kendoWindow");
                    orderWindow.close();
                    $('#APLbtnSubmit').removeAttr("disabled");
                    toastr.success("APL changes have been successfully updated");

                    HttpClient.MakeRequest(CookBookMasters.GetAPLMasterDataList, function (result) {
                        if (result != null) {
                            aplMasterDataSource = result;
                            filtteredAplMasterDataSource = aplMasterDataSource;
                            populateAPLMasterGrid();
                        }
                    }, null
                        , true);
                    Utility.UnLoading();
                }
            }, {
                model: model

            }, true);
        }
    });
    
});

function mappedApl(e) {
    isMappedAPL = e.checked;
    if (aplMasterDataSource != null) {
        if (e.checked) {
            filtteredAplMasterDataSource = aplMasterDataSource.filter(m => m.MOGCode == null);
        }
        else {
            filtteredAplMasterDataSource = aplMasterDataSource;
        }
        populateAPLMasterGrid();
        
    }
}

function populateAPLMasterGrid() {

   // Utility.Loading();
    var gridVariable = $("#gridAPLMaster");
    gridVariable.html("");
    if (user.UserRoleId == 14) {
        gridVariable.kendoGrid({
            excel: {
                fileName: "APLMaster.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "ArticleNumber", title: "Article Number", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ArticleDescription", title: "Article Description", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOM", title: "UOM", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "ArticleType", title: "Article Type", width: "40px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Hierlevel3", title: "Category", width: "50px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "MerchandizeCategoryDesc", title: "Merchandize Category", width: "80px", attributes: {

                //        style: "text-align: left; font-weight:normal"
                //    },
                //},
                {
                    field: "RangeTypeNameFormatted", title: "Range Type Name", width: "80px", attributes: {
                        class: "mycustomrangename",
                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "AllergensName", title: "Top 8 Allergens", width: "80px", attributes: {

                //        style: "text-align: left; font-weight:normal"
                //    },
                //},
                //{
                //    field: "OtherAllergensName", title: "Other Allergens", width: "80px", attributes: {

                //        style: "text-align: left; font-weight:normal"
                //    },
                //},

                //{
                //    template: templateFunction,
                //    headerTemplate: "<span style='margin-right : 5px;'>Top 8 Allergens</span>",

                //    width: "110px", title: "Include",
                //    attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: left; vertical-align:middle"
                //    }
                //},

                //{
                //    template: templateFunctionOtherAllergen,
                //    headerTemplate: "<span style='margin-right : 5px;'>Other Allergens</span>",

                //    width: "70px", title: "Include",
                //    attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: left; vertical-align:middle"
                //    }
                //},
                //{
                //    field: "ModifiedDate", title: "Last Updated", width: "60px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    template: '# if (ModifiedDate == null) {#<span>-</span>#} else{#<span>#: kendo.toString(ModifiedDate, "dd-MMM-yy")#</span>#}#',
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    }
                //},

                {
                    field: "Edit", title: "Action", width: "30px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Map Temp',
                            click: function (e) {
                                //var gridObj = $("#gridAPLMaster").data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                //datamodel = tr;
                                //APLName = tr.ArticleDescription + " (" + tr.ArticleNumber + ")";
                                //var dialog = $("#APLwindowEdit").data("kendoWindow");

                                //$(".k-overlay").css("display", "block");
                                //$(".k-overlay").css("opacity", "0.5");
                                //dialog.open().element.closest(".k-window").css({
                                //    top: 167,
                                //    //left: 558

                                //});
                                ////dialog.open();
                                //dialog.center();
                                //// 
                                //$("#articleid").val(tr.ArticleID);
                                //$("#articlenumber").text(tr.ArticleNumber);
                                //$("#articledescription").text(tr.ArticleDescription);
                                //$("#uom").text(tr.UOM);
                                //$("#type").text(tr.ArticleType);
                                //$("#category").text(tr.Hierlevel3);
                                //$("#mcategory").text(tr.MerchandizeCategoryDesc)
                                //if (tr.MOGCode) {
                                //    $("#inputmog").data('kendoDropDownList').value(tr.MOGCode);
                                //}
                                //else {
                                //    $("#inputmog").data('kendoDropDownList').value("Select");
                                //}
                                //$("#lastupdated").text(kendo.toString(tr.ModifiedDate, "dd-MMM-yy"));
                                ////if (user.UserRoleId === 1) {
                                ////    $("#inputmog").removeAttr('disabled');
                                ////    $("#APLbtnSubmit").css('display', 'inline');
                                ////    $("#APLbtnCancel").css('display', 'inline');
                                ////} else {
                                ////    $("#inputmog").attr('disabled', 'disabled');
                                ////    $("#APLbtnSubmit").css('display', 'none');
                                ////    $("#APLbtnCancel").css('display', 'none');
                                ////}
                                //var Allergendata = [];
                                //var multiselect = $("#inputAllergen").data("kendoMultiSelect");
                                //if (multiselect != null && multiselect.dataSource != null) {
                                //    multiselect.dataSource.filter({});
                                //    multiselect.value([""]);
                                //    if (tr.AllergensCode != undefined && tr.AllergensCode != null) {
                                //        tr.AllergensCode = tr.AllergensCode.replaceAll(" ", "");
                                //        var array = tr.AllergensCode.split(',');
                                //        if (tr.AllergensCode != undefined && tr.AllergensCode != null) {
                                //            for (alergen of array) {
                                //                Allergendata.push(alergen.toString());
                                //            }
                                //        }
                                //        multiselect.value(Allergendata);
                                //    }
                                //}


                                //var OtherAllergendata = [];
                                //var multiselectother = $("#inputOtherAllergen").data("kendoMultiSelect");
                                //if (multiselectother != null && multiselectother.dataSource != null) {

                                //    multiselectother.dataSource.filter({});
                                //    multiselectother.value([""]);
                                //    if (tr.OtherAllergensCode != undefined && tr.OtherAllergensCode != null) {
                                //        tr.OtherAllergensCode = tr.OtherAllergensCode.replaceAll(" ", "");
                                //        var arrayother = tr.OtherAllergensCode.split(',');
                                //        if (tr.OtherAllergensCode != undefined && tr.OtherAllergensCode != null) {
                                //            for (alergen of arrayother) {
                                //                OtherAllergendata.push(alergen.toString());
                                //            }
                                //        }
                                //        multiselectother.value(OtherAllergendata);
                                //    }
                                //}
                                Utility.Loading();
                                //Krish 11-10-2022
                                HttpClient.MakeRequest(CookBookMasters.GetRangeTypeDataList, function (result) {
                                    rangeTypeData = [];
                                    rangeTypeData.push({ "value": "Select", "text": "Select" });
                                    //rangeTypeData = result;

                                    var dataSource = result.filter(a => a.IsActive == true);

                                    for (var i = 0; i < dataSource.length; i++) {
                                        rangeTypeData.push({ "value": dataSource[i].Code, "text": dataSource[i].Name + " (" + dataSource[i].StdTempFrom + " - " + dataSource[i].StdTempTo + ")" });
                                    }
                                    populateRangeTypeDropdown();

                                    var gridObj = $("#gridAPLMaster").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    APLName = tr.ArticleDescription ;
                                    $("#rtArticleNumber").text(tr.ArticleNumber);
                                    $("#rtArticleDescription").text(tr.ArticleDescription);
                                    $("#rtUom").text(tr.UOM);
                                    $("#rtArticleType").text(tr.ArticleType);
                                    $("#rtCategory").text(tr.Hierlevel3);
                                    
                                    $("#rtMerchandizeCategory").text(tr.MerchandizeCategoryDesc);
                                   
                                    if ($("#rtRangeType").data('kendoDropDownList') != undefined) {
                                        //console.log(tr)
                                        //$("#rtRangeType").data('kendoDropDownList').value(tr.RangeTypeCode);
                                        if (tr.RangeTypeCode != null)
                                            $("#rtRangeType").data('kendoDropDownList').value(tr.RangeTypeCode);
                                        else
                                            $("#rtRangeType").data('kendoDropDownList').value("Select");
                                    }
                                    var dialog = $("#RangeTypeAPLwindowEdit").data("kendoWindow");

                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open().element.closest(".k-window").css({
                                        top: 167,

                                    });
                                    //dialog.open();
                                    dialog.center();
                                    dialog.title(APLName);
                                   
                                    Utility.UnLoading();
                                });                                

                                //dialog.title("APL Details - " + APLName);
                                return true;
                            }
                        }
                        
                    ],
                }
            ],
            dataSource: {
                data: filtteredAplMasterDataSource,
                schema: {
                    model: {
                        id: "ArticleID",
                        fields: {
                            ArticleNumber: { type: "string" },
                            ArticleDescription: { type: "string" },
                            UOM: { type: "string" },
                            ArticleType: { type: "string" },
                            Hierlevel3: { type: "string" },
                            MerchandizeCategoryDesc: { type: "string" },
                            MOGName: { type: "string" },
                            ModifiedDate: { type: "date" },
                            AllergensName: { type: "string" },
                            OtherAllergensName: { type: "string" },
                            RangeTypeCode: { type: "string" },
                            RangeTypeNameFormatted: { type: "string" }
                        }
                    }
                },
                pageSize: 100,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            excelExport: function onExcelExport(e) {
                debugger;
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid"
                        || col == "SiteDescription" || col == "CreatedBy" || col == "CreatedDate" || col == "ModifiedBy" || col == "ModifiedDate"
                        || col == "BrandName" || col == "StandardCostPerKg" || col == "ArticleCost" || col == "MOGStatus" || col == "SiteName"
                        || col == "SectorName" || col == "AllergensName" || col == "AllergensCode" || col == "NumberOfRows" || col == "OtherAllergensName"
                        || col == "OtherAllergensCode" || col == "IsMaxAPLCost") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
    }
    else {
        gridVariable.kendoGrid({
            excel: {
                fileName: "APLMaster.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "ArticleNumber", title: "Article Number", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ArticleDescription", title: "Article Description", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOM", title: "UOM", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "ArticleType", title: "Article Type", width: "40px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Hierlevel3", title: "Category", width: "50px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "MerchandizeCategoryDesc", title: "Merchandize Category", width: "80px", attributes: {

                //        style: "text-align: left; font-weight:normal"
                //    },
                //},
                {
                    field: "MOGName", title: "MOG Name", width: "80px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "AllergensName", title: "Top 8 Allergens", width: "80px", attributes: {

                //        style: "text-align: left; font-weight:normal"
                //    },
                //},
                //{
                //    field: "OtherAllergensName", title: "Other Allergens", width: "80px", attributes: {

                //        style: "text-align: left; font-weight:normal"
                //    },
                //},

                //{
                //    template: templateFunction,
                //    headerTemplate: "<span style='margin-right : 5px;'>Top 8 Allergens</span>",

                //    width: "110px", title: "Include",
                //    attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: left; vertical-align:middle"
                //    }
                //},

                //{
                //    template: templateFunctionOtherAllergen,
                //    headerTemplate: "<span style='margin-right : 5px;'>Other Allergens</span>",

                //    width: "70px", title: "Include",
                //    attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: left; vertical-align:middle"
                //    }
                //},
                //{
                //    field: "ModifiedDate", title: "Last Updated", width: "60px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    template: '# if (ModifiedDate == null) {#<span>-</span>#} else{#<span>#: kendo.toString(ModifiedDate, "dd-MMM-yy")#</span>#}#',
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    }
                //},

                {
                    field: "Edit", title: "Action", width: "30px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = $("#gridAPLMaster").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                APLName = tr.ArticleDescription + " (" + tr.ArticleNumber + ")";
                                var dialog = $("#APLwindowEdit").data("kendoWindow");

                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open().element.closest(".k-window").css({
                                    top: 167,
                                    //left: 558

                                });
                                //dialog.open();
                                dialog.center();
                                // 
                                $("#articleid").val(tr.ArticleID);
                                $("#articlenumber").text(tr.ArticleNumber);
                                $("#articledescription").text(tr.ArticleDescription);
                                $("#uom").text(tr.UOM);
                                $("#type").text(tr.ArticleType);
                                $("#category").text(tr.Hierlevel3);
                                $("#mcategory").text(tr.MerchandizeCategoryDesc)
                                if (tr.MOGCode) {
                                    $("#inputmog").data('kendoDropDownList').value(tr.MOGCode);
                                }
                                else {
                                    $("#inputmog").data('kendoDropDownList').value("Select");
                                }
                                $("#lastupdated").text(kendo.toString(tr.ModifiedDate, "dd-MMM-yy"));
                                //if (user.UserRoleId === 1) {
                                //    $("#inputmog").removeAttr('disabled');
                                //    $("#APLbtnSubmit").css('display', 'inline');
                                //    $("#APLbtnCancel").css('display', 'inline');
                                //} else {
                                //    $("#inputmog").attr('disabled', 'disabled');
                                //    $("#APLbtnSubmit").css('display', 'none');
                                //    $("#APLbtnCancel").css('display', 'none');
                                //}
                                var Allergendata = [];
                                var multiselect = $("#inputAllergen").data("kendoMultiSelect");
                                if (multiselect != null && multiselect.dataSource != null) {
                                    multiselect.dataSource.filter({});
                                    multiselect.value([""]);
                                    if (tr.AllergensCode != undefined && tr.AllergensCode != null) {
                                        tr.AllergensCode = tr.AllergensCode.replaceAll(" ", "");
                                        var array = tr.AllergensCode.split(',');
                                        if (tr.AllergensCode != undefined && tr.AllergensCode != null) {
                                            for (alergen of array) {
                                                Allergendata.push(alergen.toString());
                                            }
                                        }
                                        multiselect.value(Allergendata);
                                    }
                                }


                                var OtherAllergendata = [];
                                var multiselectother = $("#inputOtherAllergen").data("kendoMultiSelect");
                                if (multiselectother != null && multiselectother.dataSource != null) {

                                    multiselectother.dataSource.filter({});
                                    multiselectother.value([""]);
                                    if (tr.OtherAllergensCode != undefined && tr.OtherAllergensCode != null) {
                                        tr.OtherAllergensCode = tr.OtherAllergensCode.replaceAll(" ", "");
                                        var arrayother = tr.OtherAllergensCode.split(',');
                                        if (tr.OtherAllergensCode != undefined && tr.OtherAllergensCode != null) {
                                            for (alergen of arrayother) {
                                                OtherAllergendata.push(alergen.toString());
                                            }
                                        }
                                        multiselectother.value(OtherAllergendata);
                                    }
                                }


                                dialog.title("APL Details - " + APLName);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                data: filtteredAplMasterDataSource,
                schema: {
                    model: {
                        id: "ArticleID",
                        fields: {
                            ArticleNumber: { type: "string" },
                            ArticleDescription: { type: "string" },
                            UOM: { type: "string" },
                            ArticleType: { type: "string" },
                            Hierlevel3: { type: "string" },
                            MerchandizeCategoryDesc: { type: "string" },
                            MOGName: { type: "string" },
                            ModifiedDate: { type: "date" },
                            AllergensName: { type: "string" },
                            OtherAllergensName: { type: "string" },
                        }
                    }
                },
                pageSize: 100,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            excelExport: function onExcelExport(e) {
                debugger;
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid"
                        || col == "SiteDescription" || col == "CreatedBy" || col == "CreatedDate" || col == "ModifiedBy" || col == "ModifiedDate"
                        || col == "BrandName" || col == "StandardCostPerKg" || col == "ArticleCost" || col == "MOGStatus" || col == "SiteName"
                        || col == "SectorName" || col == "AllergensName" || col == "AllergensCode" || col == "NumberOfRows" || col == "OtherAllergensName"
                        || col == "OtherAllergensCode" || col == "IsMaxAPLCost") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
    }
    //$(".k-label")[0].innerHTML.replace("items", "records");
}

//Krish 11-10-2022
function populateRangeTypeDropdown() {

    $("#rtRangeType").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: rangeTypeData,
        index: 0,
    });
}
//Krish 07-10-2022
$("#rtbtnSubmit").on("click", function () {
    var rangetype = $("#rtRangeType").data("kendoDropDownList").value();
    if (rangetype == "Select") {
        toastr.error("Please select Range Type");
        return;
    }
    $("#rtbtnSubmit").attr('disabled', 'disabled');
    HttpClient.MakeSyncRequest(CookBookMasters.SaveRangeTypeAPLData, function (result) {
        if (result == false) {
            $('#rtbtnSubmit').removeAttr("disabled");
            //$("#error").css("display", "flex");
            toastr.error("Some error occured, please try again");
            // $("#error").find("p").text("Some error occured, please try again");
            $("#sitealias").focus();
        }
        else {
            $(".k-overlay").hide();
            //$("#error").css("display", "flex");
            var orderWindow = $("#RangeTypeAPLwindowEdit").data("kendoWindow");
            orderWindow.close();
            $('#rtbtnSubmit').removeAttr("disabled");
            //$("#success").css("display", "flex");
            //if (model.ID > 0) {
            toastr.success("Updated successfully");
            //}
            //else {
            //    toastr.success("New Range Type added successfully.");/
            //}

            // $("#success").find("p").text("Color configuration updated successfully");
            //populateAPLMasterGrid();
            HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataList, function (result) {

                if (result != null) {
                    aplMasterDataSource = result;
                    filtteredAplMasterDataSource = aplMasterDataSource;
                    populateAPLMasterGrid();
                }
            }, null
                , true);
            //$("#gridDish").data("kendoGrid").dataSource.data([]);
            //$("#gridDish").data("kendoGrid").dataSource.read();
            //$("#gridDish").data("kendoGrid").dataSource.sort({ field: "Code", dir: "asc" });
        }
        Utility.UnLoading();
    }, {
            articleNumber: $("#rtArticleNumber").text(),
        rangeTypeCode: rangetype

    }, true);

});
$("#rtbtnCancel").on("click", function () {
    var orderWindow = $("#RangeTypeAPLwindowEdit").data("kendoWindow");
    orderWindow.close();
    $("#rtRangeType").data('kendoDropDownList').value("Select");
});

function templateFunction(dataItem) {
    var cell = "";
    var itemHtml = "";
    if (dataItem.AllergensCode != null && dataItem.AllergensCode != "") {
        var allergensCode = dataItem.AllergensCode.split(',');
        var allergensName = dataItem.AllergensName.split(',');

         allergensCode.forEach(function (item, index) {
             itemHtml += "<div class='img-with-text' style='float: left;'>"
             itemHtml += "<img alt = '" + allergensName[index] +"' style = 'display: block;margin: 0 auto;height: 20px;width: 20px;border-radius: 10px;' src = './AllergenImages/"+item.trim()+".jpg'>"
             itemHtml += "<p>" + allergensName[index] + "</p></div >"
        });
    };
    cell += itemHtml;
    return cell;
};

function templateFunctionOtherAllergen(dataItem) {
    var cell = "";
    var itemHtml = "";
    if (dataItem.OtherAllergensCode != null && dataItem.OtherAllergensCode != "") {
        var allergensCode = dataItem.OtherAllergensCode.split(',');
        var allergensName = dataItem.OtherAllergensName.split(',');

        allergensCode.forEach(function (item, index) {
            itemHtml += "<div class='img-with-text' style='float: left;'>"
            itemHtml += "<img alt = '" + allergensName[index] + "' style = 'display: block;margin: 0 auto;height: 20px;width: 20px;border-radius: 10px;' src = './AllergenImages/" + item.trim() + ".jpg'>"
            itemHtml += "<p>" + allergensName[index] + "</p></div >"
        });
    };
    cell += itemHtml;
    return cell;
};

function populateBulkChangeControls() {

    Utility.Loading();
    var gridVariable = $("#gridBulkChange");
    gridVariable.html("");
    gridVariable.kendoGrid({
        selectable: "cell",
        sortable: false,
        filterable: false,
        groupable: false,
        //reorderable: true,
        //scrollable: true,
        columns: [
            {
                field: "", title: "", width: "50px"
            },
            {
                field: "", title: "", width: "100px"
            },
            {
                field: "", title: "", width: "30px"
            },
            {
                field: "", title: "", width: "40px"
            },
            {
                field: "", title: "", width: "50px"
            },
            {
                field: "", title: "", width: "80px"
            },
            {
                field: "MOG", title: "", width: "80px"
            },
            {
                field: "", title: "", width: "60px"
            },
            {
                field: "", title: "", width: "30px"
            }
        ],
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
        },
        change: function (e) {
        },
    });
}