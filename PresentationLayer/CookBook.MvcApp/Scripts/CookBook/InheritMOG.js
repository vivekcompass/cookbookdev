﻿//import { forEach } from "angular";

//const { checked } = require("modernizr");
var regionNonInheritedMasterDataSource = [];
var user;
var status = "";
var varname = "";
var datamodel;
var itemObject = {};
var ingredientObject = [];
var masterGroupList = [];
var masterQty = [];
var currentMasterQtyList = [];
var currentMasterList = [];
var masterItems = [];
var finalModel = [];
var finalModelExits = [];
var eid = 0;
var allMenuData = [];
var confingMenuData = [];
var unconfigMasterData = [];
var itemsids = [];
var tr = 0;
var multiarray = [];
var flagAlert = 0;
var flagCurated = 0;
var dataItems = [];
var dataItems1 = [];
var flagSectorIsActive = 0;
var filterDropdownData = [
    { text: "All", value: "All" },
    { text: "Sector", value: "Sector" },
    { text: "Region", value: "Region" },

];
var uomdata = [];
var finalModelDishCategory = [];
var user;
var grid;
var InheritedItem = [];
var regionDataSource = [];
var regionMasterDataSource = [];
var flagToggle = 1;
var UniqueItemCodes = [];
var checkedIds = {};
var checkedItemCodes = [];
var Pub_checkedItemCodes = [];
var isPublishBtnClick = false;
var topText = '';
var controlCall = 1;
var globalChecked = false;
var MOGName = "";

var isRegionInheritedCase = false;
var lastValid = 0;
var validNumber = new RegExp(/^\d*\.?\d*$/);
var uommodulemappingdata = [];

$(function () {
    $("#MOGwindowEdit").kendoWindow({
        modal: true,
        width: "350px",
        height: "300px",
        title: "MOG Details - " + MOGName,
        actions: ["Close"],
        visible: false,
        animation: false,
        resizable: false
    });
});

function validateNumber(elem) {

    if (validNumber.test(elem.value)) {
        lastValid = elem.value;
    } else {
        elem.value = lastValid;
    }
}
kendo.data.DataSource.prototype.dataFiltered = function () {
    // Gets the filter from the dataSource
    var filters = this.filter();

    // Gets the full set of data from the data source
    var allData = this.data();

    // Applies the filter to the data
    var query = new kendo.data.Query(allData);

    // Returns the filtered data
    return query.filter(filters).data;
}
function compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const bandA = a.RegionDesc.toUpperCase();
    const bandB = b.RegionDesc.toUpperCase();

    let comparison = 0;
    if (bandA > bandB) {
        comparison = 1;
    } else if (bandA < bandB) {
        comparison = -1;
    }
    return comparison;
}

function removeDuplicateObjectFromArray(array, key) {
    let check = {};
    let res = [];
    for (let i = 0; i < array.length; i++) {
        if (!check[array[i][key]]) {
            check[array[i][key]] = true;
            res.push(array[i]);
        }
    }
    return res;
}


function loadFormData() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
        user = result;
    }, null, false);
    HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
        var dataSource = data;
        debugger;
        uommodulemappingdata = [];
        uommodulemappingdata.push({ "value": "Select", "text": "Select" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].IsActive) {
                uommodulemappingdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].UOMName, "UOMModuleCode": dataSource[i].UOMModuleCode });
            }
        }
        uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001' || m.text == "Select");
        populateUOMDropdown();
        populateUOMDropdown_Bulk();
        GetUOMModuleMappingDataList
    }, null, false);
}

function InheritedRegionItem(flag) {
    HttpClient.MakeSyncRequest(CookBookMasters.GetInheritedMOGDataList, function (result) {
        if (result != null) {
            InheritedItem = result;
            modifiedDataSource();
            if (flag) {
                populateItemGrid();
                return;
            }
            populateItemGrid_IN();
        }
        else {

        }
    },  false);

}

function modifiedDataSource() {

    checkedItemCodes = [];
    checkedIds = {};

    HttpClient.MakeSyncRequest(CookBookMasters.GetInheritedMOGDataList, function (data) {

        regionNonInheritedMasterDataSource = data;
        regionDataSource = data.filter(m => m.IsInheritedMOG);

    }, null, true);
    

}

var itemcodes = "";

function saveProcedure(IsPublished) {
    if (globalChecked) {
        toastr.error("Have patience ,this process might takes few minutes");
    }
    Utility.Loading();
    setTimeout(function () {
        itemcodes = "";
        Pub_checkedItemCodes = checkedItemCodes;
        if (IsPublished) {
            $('#gridItem').data("kendoGrid").dataSource.dataFiltered().filter(function (item) {

                if ((checkedItemCodes.indexOf("1_" + item.MOGCode) != -1) || (checkedItemCodes.indexOf("0_" + item.MOGCode) != -1)) {
                    return item;
                }
                else if (item.MenuLevel == "Draft") {
                    Pub_checkedItemCodes.push("1_" + item.MOGCode);
                }
            });
            checkedItemCodes = Pub_checkedItemCodes;
            $("#btnPreview").attr('checked', false);;
        }

        itemcodes = checkedItemCodes.toString();
        if (itemcodes.length == 0) {
            alert("Please select at least one Item to continue");
            Utility.UnLoading();
            return;
        }

        var saveModel = {
            "MOGCode": itemcodes,
            "IsActive": IsPublished ? 1 : 0,
            "Status": IsPublished ? 3 : 2
        }

        HttpClient.MakeSyncRequest(CookBookMasters.SaveMOGNationalToSectorInheritance, function (result) {
            if (result == false) {

            }
            else {
                $(".k-overlay").hide();

                if (IsPublished) {
                    toastr.success("Changes Saved");

                    Utility.UnLoading();
                    // updateSiteCost(saveModel);
                    checkedItemCodes = [];
                    checkedIds = {};
                    InheritedMap();

                } else {
                    Toast.fire({
                        type: 'success',
                        title: 'Draft saved successfully',
                        timer: 5000
                    });
                    InheritedRegionItem(1);
                }
            }
        }, {
            model: saveModel
        });

        Utility.UnLoading();


    }, 2000);

}

function InheritedMap() {
    Utility.Loading();
    InheritedRegionItem(0);
    $("#actionRegion").show();
    $("#btnSaveChk").hide();
    $("#btnDrft").hide();
    $("#previewchk").hide();
    flagToggle = 0;
    toggleInherited();
    Utility.UnLoading();
}

function populateUOMDropdown() {

    $("#inputuom").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}


function back() {

    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $("#configMaster").show();
            $("#mapMaster").hide();
            $("#topHeading").text("Regional Menu Configuration");
        },
        function () {
        }
    );
}
function apply() {
     Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
        function () {
            $("#dishCategory").empty();
            var dropdownlist = $("#inputregion").data("kendoDropDownList");
            var regionID = dropdownlist.value();
            $("#configMaster").hide();
            $("#mapMaster").show();
            sameSource(ID, regionID);
            Utility.UnLoading();
        },
        function () {
            Utility.UnLoading();
        }
    );
}

$(document).ready(function () {
    topText = $("#location").text();
    $("#btnExport").click(function (e) {
        var grid = $("#gridItem").data("kendoGrid");
        grid.saveAsExcel();
    });

    $('#myInput').on('input', function (e) {
        if (globalChecked) {
            $('#header-chb').attr("checked", false).triggerHandler('change');
        }
        var grid = $('#gridItem').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "MenuLevel" || x.field == "MOGCode" || x.field == "Name" || x.field == "Alias" || x.field == "UOMName" ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        if (x.field == "Status") {
                            var pendingString = "No";
                            var savedString = "Saved";
                            var mappedString = "Yes";
                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "3";
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $('#myInputPreview').on('input', function (e) {
        var grid = $('#gridPreview').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "MOGCode" || x.field == "Name" || x.field == "Alias" || x.field == "MenuLevel") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
    $("#prevDiv").hide();
    loadFormData();
    InheritedMap();
    Utility.UnLoading();
});

function populateItemGrid() {

    var gridVariable = $("#gridItem");
    gridVariable.html("");
    grid = gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,
        columns: [

            {
                field: "MOGCode", title: "MOG Code", width: "140px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                }
            },
            {
                field: "Name", title: "MOG Name", attributes: {
                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                }
            },

            {
                field: "Alias", title: "MOG Alias", width: "160px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "155px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MenuLevel", title: "MOG Level", width: "135px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {

                template: "<label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px' ><input type='checkbox' class='checkbox'><span class='checkmarkgrid'></span></label>",
                headerTemplate: "<span style='margin-right : 5px;'>Include All</span><label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px' ><input type='checkbox' id='header-chb' class='checkbox1'><span class='checkmarkgrid'></span></label>",

                width: "135px", title: "Include",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center; vertical-align:middle"
                }
            },
        ],
        dataSource: {
            data: regionNonInheritedMasterDataSource,

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOGCode: { type: "string" },
                        Name: { type: "string" },
                        Alias: { type: "string" },
                        UOMName: { type: "string" },
                        MenuLevel: { type: "string" }
                    }

                }
            },
            pageSize: 150
        },
        columnResize: function (e) {
            //var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (globalChecked) {
                    $('#header-chb').attr("checked", "checked");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");
                    if (checkedIds[view[i].id] == false) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".checkbox")
                            .removeAttr("checked");
                        continue;

                    }
                    continue;
                }
                if (checkedIds[view[i].id]) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");
                    //.addClass("k-state-selected")
                }
                if (checkedIds[view[i].id] == false) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .removeAttr("checked");
                    continue;
                    //.addClass("k-state-selected")
                }

                if (view[i].MenuLevel == "Sector") {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");//.attr("disabled", "disabled");
                } else {
                    if (view[i].MenuLevel == "Draft") {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".checkbox")
                            .attr("checked", "checked");//.attr("disabled", "disabled");
                    }
                }
            }

        },

    });
    //bind click event to the checkbox
    grid = grid.data("kendoGrid")

    grid.table.on("click", ".checkbox", selectRow);
    $('#header-chb').change(function (ev) {
        var checked = ev.target.checked;
        globalChecked = checked;
        if (checked) {
            checkedItemCodes = $('#gridItem').data("kendoGrid").dataSource.dataFiltered().filter(item => item.MenuLevel == "Sector").map(item => "1_" + item.MOGCode);//Ram

        } else {

            checkedItemCodes = [];
            checkedIds = {};
            populateItemGrid();
            return;
        }
        $('.checkbox').each(function (idx, item) {
            if (checked) {

                if (!($('.checkbox')[idx].checked)) {
                    //  $('.checkbox').find(".checkbox").attr("checked", "checked");
                    $(item).click();
                }
            }
        });

    });

}
//bind click event to the checkbox
$("#showSelection").bind("click", function () {
    var checked = [];
    if (globalChecked)
        return;
    for (var i in checkedIds) {
        if (checkedIds[i]) {
            checked.push(i);
        }
    }


});

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        td = $(this).closest("td"),
        row = $(this).closest("tr"),
        grid = $("#gridItem").data("kendoGrid"),
        dataItem = grid.dataItem(row);
    // if (globalChecked)
    checkedIds[dataItem.id] = checked;
    if (checked) {
        var filteredAry = checkedItemCodes.filter(function (e) { return e != "0_" + dataItem.MOGCode && e != "1_" + dataItem.MOGCode });
        checkedItemCodes = filteredAry;
        if (dataItem.MenuLevel == "Sector")
            return;
        checkedItemCodes.push("1_" + dataItem.MOGCode);
    }
    else {
        var filteredAry = checkedItemCodes.filter(function (e) { return e != "1_" + dataItem.MOGCode && e != "0_" + dataItem.MOGCode });
        checkedItemCodes = filteredAry;
        if (dataItem.MenuLevel == "National")
            return;
        checkedItemCodes.push("0_" + dataItem.MOGCode);
        $('#header-chb').attr("checked", false);
    }


}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                .addClass("k-state-selected")
                .find(".checkbox")
                .attr("checked", "checked");
        }
    }
}

function toggleInherited() {

    if (!flagToggle) {
        isRegionInheritedCase = false;
        $("#toggleText").text("Add More");
        $("#btnSaveChk").hide();
        $("#btnDrft").hide();
        $("#previewchk").hide();
        $("#btnInherited").addClass("btn-info");
        $("#btnInherited").removeClass("btn-tool");
        populateItemGrid_IN()
        flagToggle = 1;
       }
    else {
        isRegionInheritedCase = true;
        $("#btnExport").hide();
        $("#myInput").show();
        $("#previewchk").show();
        $("#toggleText").text("Back");
        $("#btnInherited").removeClass("btn-info");
        $("#btnInherited").addClass("btn-tool");
        $("#btnSaveChk").show();
        $("#btnDrft").show();
        populateItemGrid();
        flagToggle = 0;
        $('#header-chb').attr("checked", false).triggerHandler('change');
        checkedItemCodes = [];
    }
}
function populateItemGrid_IN() {

    if (regionDataSource.length == 0) {
        $("#toggleText").text("Build Menu");
        $("#myInput").hide();
        $("#btnSaveChk").hide();
        $("#btnDrft").hide();
        $("#previewchk").hide();
        $("#btnExport").hide();
    }
    else {
        $("#myInput").show();
        $("#toggleText").text("Add More");
        $("#btnExport").show();

    }

    var gridVariable = $("#gridItem");
    gridVariable.html("");
    grid = gridVariable.kendoGrid({
        //selectable: "cell",
        noRecords: {
            template: "No Records Available"
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,
        columns: [

            {
                field: "MOGCode", title: "MOG Code", width: "95px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                }
            },
            {
                field: "Name", title: "MOG Name", attributes: {

                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                },
                },
            {
                field: "Alias", title: "MOG Alias", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MenuLevel", title: "MOG Level", width: "135px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "Edit", title: "Action", width: "195px",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center; vertical-align:middle"
                },
                command: [

                    {
                        title: 'Edit',
                        name: 'Edit',

                        click: function (e) {
                            isStatusSave = false;
                            populateUOMDropdown();
                            var gridObj = $("#gridItem").data("kendoGrid");
                            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            datamodel = tr;
                            MOGName = tr.Name;
                            MOGCode = tr.MOGCode;
                            MOGID = tr.ID;
                            var dialog = $("#MOGwindowEdit").data("kendoWindow");

                            $(".k-overlay").css("display", "block");
                            $(".k-overlay").css("opacity", "0.5");
                            dialog.open();
                            dialog.center();
                            console.log(tr)

                            $("#mogCodeDiv").css('display', 'block')

                            $("#mogcode").attr('disabled', 'disabled');
                            $("#mogid").val(tr.ID);
                            $("#inputmogname").val(tr.Name);
                            $("#inputmogalias").val(tr.Alias);
                            $("#mogcode").val(tr.MOGCode);
                            $("#inputuom").data('kendoDropDownList').value(tr.UOMCode);

                            dialog.title("MOG Details - " + MOGName);
                            return true;
                        }
                    }
                ]

            }
        ],
        dataSource: {
            data: regionDataSource,

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOGCode: { type: "string" },
                        Name: { type: "string" },
                        Alias: { type: "string" },
                        UOMName: { type: "string" },
                        UOMCode: { type: "string" },
                        MenuLevel: { type: "string" },
                    }

                }
            },
            pageSize: 150
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {

        },

        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var grid = $("#gridItem").data("kendoGrid");
            var data = grid.dataSource._data

            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "MOGCode" || col == "Name" || col == "UOMName" || col == "Alias") 
                {
                    return col;
                }
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }
    });
    grid = grid.data("kendoGrid")
}

function populateItemGridPreview() {

    var previewDataSource = $('#gridItem').data("kendoGrid").dataSource.dataFiltered().filter(function (item) {
        if ((checkedItemCodes.indexOf("1_" + item.MOGCode) != -1) || (checkedItemCodes.indexOf("0_" + item.MOGCode) != -1) || item.MenuLevel == "Draft") {
            return item;
        }

    });
    var gridVariable = $("#gridItem");
    gridVariable.html("");
    var grid1 = gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,
        // height: "430px",
        columns: [
            {
                field: "MOGCode", title: "MOG Code", width: "80px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                }
            },
            {
                field: "Name", title: "MOG Name", attributes: {

                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                }, },
            {
                field: "Alias", title: "MOG Alias", width: "160px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MenuLevel", title: "MOG Level", width: "135px",
                attributes: {
                    class: "mycustomstatus",
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
        ],
        dataSource: {
            data: previewDataSource,

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOGCode: { type: "string" },
                        Name: { type: "string" },
                        Alias: { type: "string" },
                        MenuLevel: { type: "string" }
                    }

                }
            },
            pageSize: 150
        },
        columnResize: function (e) {
            //var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (checkedIds[view[i].id] == false) {
                    var textValue = this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text() + " (To be Removed)";
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffe1e1");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text("To be Removed");

                } else {
                    var textValue = this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text() + " (To be Added)";
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text("To be Added");
                }
            }


        },
        change: function (e) {
        },
        //  filter: { field: "ItemCode", operator: "gt", value: "69999" }
    });
}

function preview(e) {

    if (e.checked)
        populateItemGridPreview();
    else
        populateItemGrid();

}

$("#btnSubmit").click(function () {
    saveMOGData();
});

function saveMOGData() {
    var model;
    if (datamodel != null) {
        model = datamodel;
        model.MOGCode = $("#mogcode").val();
        model.Name = $("#inputmogname").val();
        model.Alias = $("#inputmogalias").val();
        model.UOMCode = $("#inputuom").val();
        model.OldMOGName = MOGName;
        model.IsNationalMOGData = false;
        model.CreatedOn = kendo.parseDate(model.CreatedOn);
    }
    else {
        model = {
            "ID": $("#dishid").val(),
            "MOGCode": $("#mogcode").val(),
            "Name": $("#inputmogname").val(),
            "Alias": $("#inputmogalias").val(),
            "UOMCode": $("#inputuom").val(),
            "IsActive": true,
            "IsNationalMOGData": false,
            "OldMOGName": MOGName
        }
    }
    if (!sanitizeAndSend(model)) {
        return;
    }
    $("#btnSubmit").attr('disabled', 'disabled');
    HttpClient.MakeRequest(CookBookMasters.SaveMOGData, function (result) {
        if (result.xsssuccess !== undefined && !result.xsssuccess) {
            toastr.error(result.message);
            $('#btnSubmit').removeAttr("disabled");
            $("#inputmogname").focus();
        }
        else {
            if (result == false) {
                $('#btnSubmit').removeAttr("disabled");
                toastr.error("Some error occured, please try again");
                $("#inputmogname").focus();
            }
            else {
                $(".k-overlay").hide();
                var orderWindow = $("#MOGwindowEdit").data("kendoWindow");
                orderWindow.close();
                $('#btnSubmit').removeAttr("disabled");
                if (model.ID > 0) {


                    toastr.success("MOG updated successfully");
                }
                else {
                    toastr.success("New MOG created successfully");

                }
                InheritedRegionItem(0);
            }
        }
    }, {
        model: model
    }, true);
}




$("#btnCancel").on("click", function () {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $(".k-overlay").hide();
            var orderWindow = $("#MOGwindowEdit").data("kendoWindow");
            orderWindow.close();
        },
        function () {
        }
    );
});