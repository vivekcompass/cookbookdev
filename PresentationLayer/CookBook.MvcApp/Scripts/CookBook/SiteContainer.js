﻿var sitemasterdata = [];
var regionmasterdata = [];
var user;
var status = "";
var varname = "";
var datamodel;
var DishName = "";
var containerdata = [];
var mastercontainerdata = [];
var dietcategorydata = [];
var dishtypedata = [];
var uommodulemappingdata = [];
var planninguomdata = [];
var foodpanuomdata = [];
var servingtemperaturedata = [];
var recipedata = [];
var recipeCustomData = [];
var mappedData = [];
var mappedRecipeCode = [];
var DishCode;
var Dish_ID;
var multiarray = [];
var checkedRecipeCode = '';
var siteCode = '';
var saveModel = [];
var siteDishData = [];
var siteDishCategoryData = [];
var dataItems = [];
var dcatFinal = [];
var dshFinal = [];
var masterGroupList = [];
var contypedata = [];

function removeDuplicateObjectFromArray(array, key) {
    let check = {};
    let res = [];
    for (let i = 0; i < array.length; i++) {
        if (!check[array[i][key]]) {
            check[array[i][key]] = true;
            res.push(array[i]);
        }
    }
    return res;
}



HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {
    var dataSource = data;
    uomdata = [];
    uomdata.push({ "value": null, "text": "Select" });
    for (var i = 0; i < dataSource.length; i++) {
        uomdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].Name });
    }
}, null, false);

HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
    var dataSource = data;
    uommodulemappingdata = [];
    uommodulemappingdata.push({ "value": null, "text": "Select" });
    for (var i = 0; i < dataSource.length; i++) {
        if (dataSource[i].IsActive) {
            uommodulemappingdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].UOMName, "UOMModuleCode": dataSource[i].UOMModuleCode });
        }
    }
    planninguomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00004' || m.text == "Select");
    foodpanuomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00005' || m.text == "Select");


}, null, false);


HttpClient.MakeRequest(CookBookMasters.GetSiteDataListByUserId, function (data) {
    console.log("Site Master Data");
    console.log(data);
    sitemasterdata = data;
    sitemasterdata.forEach(function (item) {
        item.SiteName = item.SiteName + " - " + item.SiteCode;
        return item;
    })
    sitemasterdata = sitemasterdata.filter(item => item.IsActive != false);
    if (data.length > 1) {
        sitemasterdata.unshift({ "SiteCode": 0, "SiteName": "Select Site" });
        $("#btnGo").css("display", "inline-block");
    }
    else {
        $("#btnGo").css("display", "inline-none");
    }
    populateSiteMasterDropdown();
}, null, false);
HttpClient.MakeRequest(CookBookMasters.GetDishCategoryList, function (data) {

    var dataSource = data;
    dishcategorydata = [];

    for (var i = 0; i < dataSource.length; i++) {
        dishcategorydata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "code": dataSource[i].DishCategoryCode });
    }


}, null, false);


HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
    user = result;
    


}, null, false);
HttpClient.MakeRequest(CookBookMasters.GetContainerTypeDataList, function (data) {
    var dataSource = data;
    contypedata = [];
    contypedata.push({ "value": null, "text": "Select Planning & Requisition UOM", "IsActive": 0 });
    for (var i = 0; i < dataSource.length; i++) {
        //if (!dataSource[i].IsActive)
        //    continue;
        contypedata.push({ "value": dataSource[i].ContainerTypeCode, "text": dataSource[i].Name, "IsActive": dataSource[i].IsActive});
    }
}, null, false);

HttpClient.MakeRequest(CookBookMasters.GetContainerDataList, function (data) {

    var dataSource = data;
    containerdata = [];
    containerdata.push({ "value": null, "text": "Select Planning & Requisition UOM Detail", "IsActive": 0  });
    for (var i = 0; i < dataSource.length; i++) {
           //if (!dataSource[i].IsActive)
           //         continue;
        containerdata.push({ "value": dataSource[i].ContainerCode, "text": dataSource[i].Name, "ConatinerID": dataSource[i].ID, "TypeCode": dataSource[i].ContainerTypeCode, "IsActive": dataSource[i].IsActive });
    }




    mastercontainerdata = containerdata;
}, null, false);


function populateContainerTypeDropdown(uomid, uomvalue) {
   
    $(uomid).kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: planninguomdata,
        index: 0
    });
    var dropdownlist = $(uomid).data("kendoDropDownList");
    if (uomvalue != null)
        dropdownlist.value(uomvalue);
    dropdownlist.bind("change", ContainerType_select);

   
}

function ContainerType_select(e) {
   
    containerdata = mastercontainerdata;
    var ID = e.sender.dataItem(e.item).value;
    var cname =  e.sender.dataItem(e.item).text
    if (ID == 0) {
        return;
    }
    

    var idnum = e.sender.element.context.id.substring(5);
    var dishFlag = e.sender.element.context.id.search("small");
    var idContainer = e.sender.element.context.id.substring(2);
    if (cname == 'Food Pan') {
        $(".foodpanCapacityctfoo" + idnum).css('display', 'inline-block');
        $(".foodpanCapacityuomctfoo" + idnum).css('display', 'inline-flex');
    }
    else {
        $(".foodpanCapacityctfoo" + idnum).css('display', 'none');
        $(".foodpanCapacityuomctfoo" + idnum).css('display', 'none');
    }

    containerdata=containerdata.filter(function (item) {
        if (item.TypeCode == ID)
            return item;
    });
    containerdata.unshift({ "value": null, "text": "Select Planning & Requisition UOM" });
   
   // populateContainerDropdown("#" + idContainer);
    if (dishFlag > 0) {
        return
    }
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {
        var sm = subItem[i].id;
        // populateContainerDropdown("#food" + sm);

        $("#ctfood" + sm).data('kendoDropDownList').value(ID);
        if (cname == 'Food Pan') {
            $(".foodpanCapacityctfood" + sm).css('display', 'inline-block');
            $(".foodpanCapacityctuomfood" + sm).css('display', 'inline-block');
        }
        else {
            $(".foodpanCapacityctfood" + sm).css('display', 'none');
            $(".foodpanCapacityctuomfood" + sm).css('display', 'none');

        }
    }
}

function populateSiteMasterDropdown() {
    $("#ddlSiteMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "SiteName",
        dataValueField: "SiteCode",
        dataSource: sitemasterdata,
        index: 0,
    });
    $("#inputsite").css("display", "block");
    if (sitemasterdata.length == 1) {
        var dropdownlist = $("#ddlSiteMaster").data("kendoDropDownList");
        Utility.Loading();
        $("#btnSubmit").hide();
        $("#btnPublish").hide();
        $("#btnBack").hide();
        $("#mapMaster").hide();
        $("#dishCategory").empty();
        siteCode = dropdownlist.value();
        var siteName = dropdownlist.text();
        $("#location").text(" | " + siteName);
        $("#dishCategory").empty();
        GetSiteDishCategorydata(siteCode);
        //alert("hareRam");
        GetSiteDishdata(siteCode);

        CreateDishCategoryOutline();
        CreateDishTiles();
        if (siteDishCategoryData != null && siteDishCategoryData.length > 0) {
            $("#btnSubmit").show();
            $("#btnPublish").show();
            $("#btnBack").show();
            $("#mapMaster").show();
            $("#emptyMsg").hide();
        } 
        dropdownlist.wrapper.hide();
        Utility.UnLoading();

    }
    else {

        $("#btnGo").css("display", "inline-block");
    }
}

function GetSiteDishdata(siteCode) {

    HttpClient.MakeSyncRequest(CookBookMasters.GetDishDataListForContainer, function (result) {
        Utility.UnLoading();

        if (result != null) {
           
            siteDishData = result;

        }
        else {
            
        }
    },
        {
            siteCode: siteCode
        }
        , false);
}

function GetSiteDishCategorydata(siteCode) {

    HttpClient.MakeSyncRequest(CookBookMasters.GetDishCategoryDataListForContainer, function (result) {
        Utility.UnLoading();

        if (result != null) {
           
            siteDishCategoryData = result;

        }
        else {
           
        }
    },
        {
            siteCode: siteCode
        }
        , false);
    //alert("hareKrishna");
}

function populateUOMDropdown(uomid, uomvalue) {

    $(uomid).kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: foodpanuomdata,
        index: 0,
    });
    var dropdownlist = $(uomid).data("kendoDropDownList");
    if (uomvalue == null)
        dropdownlist.select(1);//value("Select");
    else
        dropdownlist.value(uomvalue);
  
    dropdownlist.bind("change", uomdropdownlist_select);
}
function uomdropdownlist_select(e) {

    var ID = e.sender.dataItem(e.item).value;
    if (ID == 0) {
        return;
    }

    var idnum = e.sender.element.context.id.substring(3);
    var dishFlag = e.sender.element.context.id.search("small");
    if (dishFlag > 0) {
        
       
        return
    }
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {

        var sm = subItem[i].id;

        $("#uom" + sm).data('kendoDropDownList').value(ID);

      

    }
    

}

function bindDataOnSave() {
    Utility.Loading();
    $("#btnSubmit").hide();
    $("#btnPublish").hide();
    $("#btnBack").hide();
    $("#mapMaster").hide();
    $("#dishCategory").empty();
    var dropdownlist = $("#ddlSiteMaster").data("kendoDropDownList");
    siteCode = dropdownlist.value();
    var siteName = dropdownlist.text();
    $("#location").text(" | " + siteName);
    $("#dishCategory").empty();
    GetSiteDishCategorydata(siteCode);
    //alert("hareRam");
    GetSiteDishdata(siteCode);

    CreateDishCategoryOutline();
    CreateDishTiles();
    if (siteDishCategoryData != null && siteDishCategoryData.length > 0) {
        $("#btnSubmit").show();
        $("#btnPublish").show();
        $("#btnBack").show();
        $("#mapMaster").show();
        $("#emptyMsg").hide();
    } else {

        $("#emptyMsg").show();
        $("#btnBack").show();

    }
    Utility.UnLoading();
}

//Ended
$(document).ready(function () {
    $("#btnSubmit").hide();
    $("#btnPublish").hide();
    $("#btnBack").hide();
    $("#btnGo").on("click", function () {
        Utility.Loading();
        bindDataOnSave();
        Utility.UnLoading();
    });

    $("#collapseIcon").click(function () {
        collapseAll();
    });

    $("#expandIcon").click(function () {
        expandAll();
    });
});
function expandAll() {
    var rows = $(".upper");
    $.each(rows, function () {
        var upperid = (this).id;
        var lowerid = (this).nextElementSibling.id;
        $("#" + upperid).find(".hideG").html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        $("#" + lowerid).show();
        $("#" + upperid).find(".k-widget").show();
        $("#" + upperid).find(".displayG").hide()
        $("#" + upperid).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" })
        $("#expandIcon").css({ "color": "#3f7990" });
        $("#collapseIcon").css({ "color": "#76b4b0" });
    });
}

function collapseAll() {
    var rows = $(".upper");
    rows.each(function () {
        var upperid = (this).id;
        var lowerid = (this).nextElementSibling.id;
        $("#" + upperid).find(".hideG").html(" Show <i class= 'fas fa-angle-down' ></i>");
        var count = $("#" + lowerid).find(".smallIngredient").length;
        $("#" + lowerid).hide();
        $("#" + upperid).find(".displayG").show().html(" " + count + " Dish(s)");
        $("#" + upperid).find(".k-widget:last-child").hide();
        $("#" + upperid).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
        $("#collapseIcon").css({ "color": "#3f7990" });
        $("#expandIcon").css({ "color": "#76b4b0" });
    });
}
 
function populateContainerDropdown(Containerid,value) {
    $(Containerid).kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: containerdata,
        index: 0

    });
    var dropdownlist = $(Containerid).data("kendoDropDownList");
    if (value == null)
        dropdownlist.select(0);//value("Select");
    else
        dropdownlist.value(value);

    dropdownlist.bind("change", Container_select);
}

function Container_select(e) {

    var ID = e.sender.dataItem(e.item).value;
    if (ID == 0) {
        return;
    }
    //if (!e.sender.dataItem()?.IsActive) {
    //    toastr.error("Selected Item is In-Active");
    //    return;
    //}
    var idnum = e.sender.element.context.id.substring(3);
    var dishFlag = e.sender.element.context.id.search("small");
    if (dishFlag > 0) {


        return
    }
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {

        var sm = subItem[i].id;

        $("#food" + sm).data('kendoDropDownList').value(ID);



    }


}
function closeBelow(dsh) {
    //alert(dsh);
    var lowerid = "subinputdish"+dsh
    var upperid ="dish"+dsh;
if ($("#" + lowerid).is(":visible")) {
    
            $("#" + upperid).find(".hideG").html(" Show <i class= 'fas fa-angle-down' ></i>");
            var count = $("#" + lowerid).find(".smallIngredient").length;
            $("#" + lowerid).hide();
            $("#" + upperid).find(".displayG").show().html(" " + count + " Dish(s)");
            $("#" + upperid).find(".k-widget:last-child").hide();
            $("#" + upperid).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
            
    } else {
    $("#" + upperid).find(".hideG").html(" Hide <i class= 'fas fa-angle-up' ></i> ");
    $("#" + lowerid).show();
    $("#" + upperid).find(".k-widget").show();
    $("#" + upperid).find(".displayG").hide()
    $("#" + upperid).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" })
    }
}
function CreateDishCategoryOutline() {

    var dataItems1 = siteDishCategoryData;
    dataItems = [];
        for (xi of dataItems1) {
            dishcategorydata.forEach(function (yi) {
                if (yi.code == xi.DishCategoryCode) {
                    dataItems.push({ "ID": xi.ID, "value": xi.DishCategoryCode, "text": yi.text, "stdqty": xi.Quantity, "ucode": xi.UOMCode, "ContainerCode": xi.ContainerCode, "ContainerTypeCode": xi.ContainerTypeCode })
                }
            });
        }

        for (item of dataItems) {
            var divid = "dish" + item.value;
            var dishesid = "inputdish" + item.value;
            var inputqty = "inputqty" + item.value;
            var subdishid = "subinputdish" + item.value;
            var uomid = "uom" + item.value;
            var Containerid = "foo" + item.value;
            var stdqty = item.stdqty;
            var ctfoo = "ctfoo" + item.value;

            // Previous Cat with Container and Container Type
            //$('#dishCategory').append('<div class="dish" ><div class="upper"  id=' + divid + '><div class="upperText" onclick="closeBelow(&quot;' + item.value +'&quot;)">'
            //    + item.text + '</div>'
            //    + '<span style="font-size:13px;">Planning & Requisition UOM </span><div style="margin-right:5px;" id=' + ctfoo +' ></div><span class="foodpanCapacity" style="font-size:13px;">Food Pan Dish Capacity </span><div class="foodpanCapacity" style="margin-right:5px;" id=' + Containerid + '></div> '
            //    + '<input id = "' + inputqty +
            //    '"  class="qty"  type=" " value='+stdqty+' min="0" onchange="updateQuantity(&quot;' + inputqty + '&quot;)" /> <div id = "' + uomid + '" class="colorG"></div ><span class="serv"></span> <span class="hideG" onclick="closeBelow(&quot;'+item.value+'&quot;)"> Hide <i class="fas fa-angle-up" ></i ></span > <span class="displayG"></span><span class="dllrgndish" style="float:right;margin-top:2px" id='
            //    + dishesid + '></span></div ><div  class="lower" id='
            //    + subdishid + ' > </div ></div> ');

            if (item.ContainerTypeCode == 'UOM-00004') {
                $('#dishCategory').append('<div class="dish" ><div class="upper"  id=' + divid + '><div class="upperText" onclick="closeBelow(&quot;' + item.value + '&quot;)">'
                    + item.text + '</div>'
                    + '<span style="font-size:13px;">Planning & Requisition UOM </span><div style="margin-right:5px;" id=' + ctfoo + ' ></div><span class="foodpanCapacity' + ctfoo + '" style="font-size:13px;">Food Pan Dish Capacity </span>'
                    + '<input id = "' + inputqty +
                    '" class="qty foodpanCapacity' + ctfoo + '"  type=" " value=' + stdqty + ' min="0" onchange="updateQuantity(&quot;' + inputqty + '&quot;)" /> <div id = "' + uomid + '" class="colorG foodpanCapacityuom' + ctfoo + '"></div ><span class="serv"></span> <span class="hideG" onclick="closeBelow(&quot;' + item.value + '&quot;)"> Hide <i class="fas fa-angle-up" ></i ></span > <span class="displayG"></span><span class="dllrgndish" style="float:right;margin-top:2px" id='
                    + dishesid + '></span></div ><div  class="lower class="foodpanCapacity' + ctfoo + '" id='
                    + subdishid + ' > </div ></div> ');
            }
            else {
                //$('#dishCategory').append('<div class="dish" ><div class="upper"  id=' + divid + '><div class="upperText" onclick="closeBelow(&quot;' + item.value + '&quot;)">'
                //    + item.text + '</div>'
                //    + '<span style="font-size:13px;">Planning & Requisition UOM </span><div style="margin-right:5px;" id=' + ctfoo + ' ></div></div> ');

                $('#dishCategory').append('<div class="dish" ><div class="upper"  id=' + divid + '><div class="upperText" onclick="closeBelow(&quot;' + item.value + '&quot;)">'
                    + item.text + '</div>'
                    + '<span style="font-size:13px;">Planning & Requisition UOM </span><div style="margin-right:5px;" id=' + ctfoo + ' ></div><span  class="foodpanCapacity' + ctfoo + '" style="font-size:13px;display:none;">Food Pan Dish Capacity </span>'
                    + '<input id = "' + inputqty +
                    '" style="display:none;" class="qty foodpanCapacity' + ctfoo + '"  type=" " value=' + stdqty + ' min="0" onchange="updateQuantity(&quot;' + inputqty + '&quot;)" /> <div style="display:none;" id = "'
                    + uomid + '" class="colorG foodpanCapacityuom' + ctfoo + '"></div ><span class="serv"></span> <span class="hideG" onclick="closeBelow(&quot;'
                    + item.value + '&quot;)"> Hide <i class="fas fa-angle-up" ></i ></span > <span class="displayG"></span><span class="dllrgndish" style="float:right;margin-top:2px" id='
                    + dishesid + '></span></div ><div class="lower class="foodpanCapacity' + ctfoo + '" id='
                    + subdishid + ' > </div ></div> ');
            }
            
          

            populateContainerTypeDropdown("#" + ctfoo, item.ContainerTypeCode);
            populateUOMDropdown('#' + uomid, item.ucode);

            if (item.ContainerTypeCode == 'UOM-00004') {
                $(".foodpanCapacityuom" + ctfoo + "").css("display", "inline-flex")
                $(".foodpanCapacityuom" + ctfoo + "").css("width", "35px !important")
            }
            else {
                $(".foodpanCapacityuom" + ctfoo + "").css("display", "none")
            }
           
           // populateContainerDropdown("#"+Containerid,item.ContainerCode)
        
        }
  
}
function CreateDishTiles() {
        // var dataSource = data;
    var finalModelExits = [];
    masterGroupList = [];
        var dataItems1 = siteDishData;
    for (xi of dataItems1) {
        dataItems.forEach(function (yi) {
                if (yi.text == xi.DishCategory) {
                    finalModelExits.push({
                        "ID": xi.ID, "DishCode": xi.DishCode, "DishCategoryCode": yi.value, "text": xi.DishName, "Quantity": xi.Capacity,
                        "ucode": xi.UOMCode, "ContainerCode": xi.ContainerCode, "ContainerTypeCode": xi.ContainerTypeCode, "CuisineName": xi.CuisineName
                    })
                }
            });
        }

       masterGroupList = finalModelExits;
        RecreateWholeExits();
    
}

function RecreateWholeExits() {
    for (item1 of masterGroupList) {
        
        var dshcat = item1.DishCode;//"DTC-00001";
        var dcat = item1.DishCategoryCode
        var text = item1.text;
        var uomcode = item1.ucode;
        var fpCode = item1.ContainerCode;
        var fpctCode = item1.ContainerTypeCode;
        var qty = item1.Quantity;
        if (qty == null)
            qty = 0;
        var sid = "small" + dshcat;
        var subid = "#subinputdish" + dcat;
        var fid = "foodsmall" + dshcat;
        var fctid = "ctfoodsmall" + dshcat;
        var uomid = "uomsmall" + dshcat;
       // var cuisinestring = item.CuisineName == null ? "" : '<span class="cuisine-heading">' + item.CuisineName + '</span>';

        //--- Previous dish container mapping screen with Container type

        //$(subid)
        //    .prepend(
        //        '<div class="smallIngredient" id=' + sid + ' title="' + text + '">' +
        //        '     <div class="supper">' + text + '</div>' +
        //        '     <div class="slower" id="' + fctid + '"></div>' +
        //        '     <div class="slower" style="margin-top:5px;" id="' + fid + '"></div>' +
        //        '     <div class="dishtile">' +
        //        '          <input id="inputsubqty' + sid + '" class="qtyS" type="" value="' + qty + '" min="0"  />' +
        //        '          <span id="' + uomid + '" class="colorGS"></span>' +
        //        '     </div>' +
        //        '</div>');
        if (item1.ContainerTypeCode == 'UOM-00004') {
            $(subid)
                .prepend(
                    '<div class="smallIngredient" id=' + sid + ' title="' + text + '">' +
                    '     <div class="supper">' + text + '</div>' +
                    '     <div class="slower"  id="' + fctid + '"></div>' +
                    '     <div class="dishtile foodpanCapacity' + fctid + '">' +
                    '          <input id="inputsubqty' + sid + '" class="qtyS" type="" value="' + qty + '" min="0"  />' +
                    '          <span id="' + uomid + '" class="colorGS"></span>' +
                    '     </div>' +
                    '</div>');
        }
        else {
            $(subid)
                .prepend(
                    '<div class="smallIngredient" id=' + sid + ' title="' + text + '">' +
                    '     <div class="supper">' + text + '</div>' +
                    '     <div class="slower"  id="' + fctid + '"></div>' +
                    '     <div style="display:none" class="dishtile foodpanCapacity' + fctid + '">' +
                    '          <input id="inputsubqty' + sid + '" class="qtyS" type="" value="' + qty + '" min="0"  />' +
                    '          <span id="' + uomid + '" class="colorGS"></span>' +
                    '     </div>' +
                    '</div>');
        }

        populateContainerTypeDropdown("#" + fctid, fpctCode);
        // populateContainerDropdown("#" + fid, fpCode);      
        populateUOMDropdown('#' + uomid, uomcode);
     
    }

   
}

function updateQuantity(iqty) {
    var qty = $("#" + iqty).val();

    var idnum = iqty.substring(8);
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {
        var sm = subItem[i].id;
        $("#inputsubqty" + sm).val(qty);
    }

}

function onSave(isPublish) {
    $("#btnSubmit").attr('disabled', 'disabled');
    $("#btnPublish").attr('disabled', 'disabled');
    $("#btnBack").attr('disabled', 'disabled');
    //cat save qunat uom foo
    dcatFinal = [];
    dshFinal = [];
    for (it of dataItems) {
        var dc = it.value;
       // console.log(dc);
        //var ContainerType = $("#foo" + dc).data('kendoDropDownList').value();
        var ContainerTypeCode = $("#ctfoo" + dc).data('kendoDropDownList').value();
       var model = {
            "ID": it.ID,
            "SiteCode": siteCode,
            "DishCategoryCode": dc,
           // "ContainerCode": ContainerType,
            "PlanningRequisitionUOMCode": ContainerTypeCode,
            "Quantity": $("#inputqty"+dc).val(),
            "UOMCode": $("#uom" + dc).data('kendoDropDownList').value(),
            "IsActive": true,
        }
        dcatFinal.push(model);
    }
  
    //dish change save qunt uom foo
    for (dish of masterGroupList) {
        var dshcode = dish.DishCode;
        console.log(dshcode);
        //var ContainerType = $("#foodsmall" + dshcode).data('kendoDropDownList').value();
        var ContainerTypeCode = $("#ctfoodsmall" + dshcode).data('kendoDropDownList').value()
       var modelDsh = {
           "ID":dish.ID,
           "SiteCode": siteCode,
           "DishCode": dish.DishCode,
           //"ContainerCode": ContainerType,
           "PlanningRequisitionUOMCode": ContainerTypeCode,
           "Capacity": $("#inputsubqtysmall" + dshcode).val(),
           "UOMCode": $("#uomsmall" + dshcode).data('kendoDropDownList').value(),
           "CapacityUOMCode": $("#uomsmall" + dshcode).data('kendoDropDownList').value(),
           "IsActive": true,
        }
        dshFinal.push(modelDsh);
    }
    //console.log(dcatFinal);
    //console.log(dshFinal);
    
    HttpClient.MakeRequest(CookBookMasters.SaveContainerCategoryDataList, function (result) {
        if (result == false) {
            $("#btnSubmit").removeAttr('disabled', 'disabled');
            $("#btnPublish").removeAttr('disabled', 'disabled');
            $("#btnBack").removeAttr('disabled', 'disabled');
            toastr.error("Some error occured, please try again.");
        }
        else {
           
           // toastr.success("Records have been updated successfully");

        }
    }, {
            model: dcatFinal

    }, true);
    HttpClient.MakeRequest(CookBookMasters.SaveSiteDishContainerDataList,
        function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again.");
            $("#btnSubmit").removeAttr('disabled', 'disabled');
            $("#btnPublish").removeAttr('disabled', 'disabled');
            $("#btnBack").removeAttr('disabled', 'disabled');
        }
        else {
            if (isPublish) {
                toastr.success("Dish-Container Mapping Published");
                setTimeout(rellocate, 500);
                
                function rellocate() {
                    window.location.href =  'Home/Index';
                }
            }
            else {
                    bindDataOnSave();
                    Toast.fire({
                        type: 'success',
                        title: 'Dish-Container Mapping Saved'
                    });
                $("#btnSubmit").removeAttr('disabled', 'disabled');
                $("#btnPublish").removeAttr('disabled', 'disabled');
                $("#btnBack").removeAttr('disabled', 'disabled');
            }
        }
    }, {
            model: dshFinal

    }, true);

}

function onPublish() {
    onSave(1);
}


