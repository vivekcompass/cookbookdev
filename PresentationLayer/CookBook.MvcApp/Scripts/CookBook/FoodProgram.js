﻿var user;
var conceptype2data;
var tileImage = null;
var item = null;
var daypartdata = [];
var fCode;
var daypartdataInactive = [];
function populateDayPartMultiSelect() {
    $("#inputdaypart").kendoMultiSelect({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "code",
        dataSource: daypartdata,
        index: 0,
        autoClose: false
    });
}

function GetDayPartMapping() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetFoodProgramDayPartMappingDataList, function (data) {

        siteDayPartResult = data;

    },
        { foodCode: fCode }, true);
    var multiarray = siteDayPartResult.map(item => item.DayPartCode);
    var multiselect = $("#inputdaypart").data("kendoMultiSelect");
    multiselect.value(multiarray);
    Utility.UnLoading();
}
function SaveDayPartMapping() {
    
    HttpClient.MakeSyncRequest(CookBookMasters.SaveFoodProgramDayPartMappingDataList, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again.");
            dayPartArray = [];
            Utility.UnLoading();
        }
        else {

            //if (user.SectorNumber != "20")
            //    toastr.success("Food Program Mealtime Mapping saved");
            //else
            //    toastr.success("Food Program Service Type Mapping saved");

            Utility.UnLoading();
        }
    }, {
        model: dayPartArray
    }, true);
}

$(function () {

    var status = "";
    var varname = "";
    var datamodel;
    var Name = "";
    var sitedata = [];
    var dkdata = [];

    HttpClient.MakeRequest(CookBookMasters.GetDayPartDataList, function (data) {
        var dataSource = data;
        daypartdata = [];

        for (var i = 0; i < dataSource.length; i++) {
            if (!dataSource[i].IsActive)
                daypartdataInactive.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "code": dataSource[i].DayPartCode, "IsActive": dataSource[i].IsActive });

            daypartdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "code": dataSource[i].DayPartCode, "IsActive": dataSource[i].IsActive });
        }
        populateDayPartMultiSelect();

    }, null, false);


    $('#myInput').on('input', function (e) {
        var grid = $('#gridFoodProgram').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "FoodProgramCode" || x.field == "Name" || x.field == "Description" || x.field == "ConceptType2Name") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        if (user.SectorNumber != "20") {
            $("#mtype").text("Mealtime");
        }
        $("#FDwindowEdit").kendoWindow({
            modal: true,
            width: "600px",
            //height: "350px",
            title: "Food Program Details  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });

        $("#btnExport").click(function (e) {
            var grid = $("#gridFoodProgram").data("kendoGrid");
            grid.saveAsExcel();
        });
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;

            populateFoodProgramGrid();
        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetConceptType2DataList, function (data) {

            var dataSource = data;
            conceptype2data = [];
            conceptype2data.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                conceptype2data.push({ "value": dataSource[i].ConceptType2Code, "text": dataSource[i].Name });
            }
            populateConceptType2Dropdown();
        }, null, true);
    });

    function populateConceptType2Dropdown() {
        $("#fptype").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: conceptype2data,
            index: 0,
        });
    }
    //Food Program Section Start

    function populateFoodProgramGrid() {

        Utility.Loading();
        var gridVariable = $("#gridFoodProgram");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [

                {
                    field: "", title: "Image", width: "35px", attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    template: "<div class='item-photo'" +
                        "style='background-image: url(./FoodProgramImages/#:data.TileImage#);'></div>", width: "35px"
                },
                {
                    field: "FoodProgramCode", title: "Code", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Name", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ConceptType2Name", title: "Type", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Description", title: "Brand Story", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive",
                //    title: "Status", width: "40px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal;"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {

                                var tr = $(e.target).closest("tr");    // get the current table row (tr)

                                item = this.dataItem(tr);          // get the date of this row

                                if (!item.IsActive) {
                                    return;
                                }
                                var model = item;
                                Name = item.Name;
                                FoodProgramCodeInitial = item.FoodProgramCode;
                                tileImage = item.TileImage;
                                if (item.TileImage !== null && item.TileImage !== "") {
                                    $("#removeImage").css("display", "block");
                                }
                                else {
                                    $("#removeImage").css("display", "none");
                                }
                                var dialog = $("#FDwindowEdit").data("kendoWindow");
                                fCode = item.FoodProgramCode;
                                GetDayPartMapping();
                                dialog.open().element.closest(".k-window").css({
                                    top: 167,
                                });
                                dialog.center();
                                $("#fpid").val(model.ID);
                                $("#fpcode").val(model.FoodProgramCode);
                                $("#fpname").val(model.Name);
                                $("#fptype").data('kendoDropDownList').value(model.ConceptType2Code);
                                $("#fpdesc").val(model.Description);
                                $("#fpactive").prop('checked', model.IsActive);
                                $("#file").val("");
                                $("#image").attr('src', './FoodProgramImages/' + model.TileImage);
                                $("#fpname").focus();
                                dialog.title("Details - " + Name);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetFoodProgramDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                                fCode = result.sort((a, b) => (a.FoodProgramCode > b.FoodProgramCode) ? 1 : -1)[result.length - 1].FoodProgramCode;

                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { type: "string" },
                            IsActive: { editable: false },
                            ConceptType2Name: { type: "string" },
                            Description: { type: "string" },
                            FoodProgramCode: { type: "string" }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
                
                $(".chkbox").on("change", function () {
                    var gridObj = $("#gridFoodProgram").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    var trEdit = $(this).closest("tr");
                    var th = this;
                    datamodel = tr;
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Food Program " + active + "?", " Food Program Update Confirmation", "Yes", "No", function () {
                        HttpClient.MakeRequest(CookBookMasters.SaveFoodProgram, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                toastr.success("Food Program configuration updated successfully");
                                if ($(th)[0].checked) {
                                    $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                } else {
                                    $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                }
                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {

                        $(th)[0].checked = !datamodel.IsActive;
                        if ($(th)[0].checked) {
                            $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                        } else {
                            $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                        }
                    });
                    return true;
                });

            },
            change: function (e) {
            },
        })

        //  var gridVariable = $("#gridFoodProgram").data("kendoGrid");
        //sort Grid's dataSource
        gridVariable.data("kendoGrid").dataSource.sort({ field: "FoodProgramCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }


    $("#fpcancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#FDwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#fpaddnew").on("click", function () {
        var model;
        var dialog = $("#FDwindowEdit").data("kendoWindow");

        dialog.open().element.closest(".k-window").css({
            top: 167,
           
        });
        dialog.center();

        datamodel = model;

        dialog.title("New Food Program Creation");
        fCode = null;
        $("#fptype").data('kendoDropDownList').value("Select");
        $('#fpsubmit').removeAttr("disabled");
        $("#fpid").val("");
        $("#fpcode").val("");
        $("#fpdesc").val("");
        $("#fpname").val("");
        $("#file").val("");
        $("#image").attr('src', '../FoodProgramImages/default.png');
        $("#fpname").removeClass("is-invalid")
        $("#fpname").focus();
        item = null;
        FoodProgramCodeInitial = null;
        var multiselect = $("#inputdaypart").data("kendoMultiSelect");
        multiselect.value([]);
        $("#removeImage").css("display", "none");
    });

    function fpvalidate() {
        var valid = true;

        if ($("#fpname").val() === "") {
            toastr.error("Please provide input");
            $("#fpname").addClass("is-invalid");
            valid = false;
            $("#fpname").focus();
        }
        else if (($.trim($("#fpname").val())).length > 30) {
            toastr.error("Please provide input not exceeding 30 charcters");
            $("#fpname").addClass("is-invalid");
            valid = false;
            $("#fpname").focus();
        }
        else if ($("#fptype").val() == "Select") {
            $("#fptype").addClass("is-invalid");
            valid = false;
            $("#fpname").removeClass("is-invalid");
            toastr.error("Please provide input for type");
            $("#fptype").focus();
        }
        return valid;
    }

    $('#btnUpload').click(function () {

        $('#txtFilePath').removeClass('validationError');
        $('#file').val('').trigger('click');
    });

    function validateImportFile(filename) {

        if (filename != '') {
            var fileExtension = ['png', 'jpg', 'jpeg'];
            if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == -1) {
                $("#file").val('')
                toastr.error("The uploader accepts only png or jpg files.");	
                return false;
            }
        }
        return true;
    };


    $('#file').change(function () {

        var that = this;
        if (validateImportFile(that.value)) {
            //$('#txtFilePath').val(that.value);
            //  $('#txtFilePath').val(that.files[0].name);
            if (that.files[0]) {
                var uploadimg = new FileReader();
                uploadimg.onload = function (displayimg) {
                    $("#image").attr('src', displayimg.target.result);
                    $("#removeImage").css("display", "block");
                }
                uploadimg.readAsDataURL(that.files[0]);
            }
        }
        else {
            $("#removeImage").css("display", "none");
            //ImportReport.refreshPage();
        }
    });

    function fpSubmit() {
        $("#fpsubmit").click(function () {
            
            Utility.Loading();
            setTimeout(function () {
            var editFlag = 0;
            if ($("#fpname").val() === "") {
                toastr.error("Please provide Food Program Name");
                $("#fpname").focus();
                Utility.UnLoading();
                return;
            }
            var fpcode = "";
            if (fpvalidate() === true) {
                $("#fpname").removeClass("is-invalid");
                var model = item;
                if (model == null) {
                    model = {
                        "ID": $("#fpid").val(),
                        "Name": $("#fpname").val(),
                        "IsActive": 1,
                        "FoodProgramCode": FoodProgramCodeInitial,
                        "ConceptType2Code": $("#fptype").val(),
                        "Description": $("#fpdesc").val(),
                        "TileImage": tileImage,
                    }
                    editFlag = 0;
                } else {
                    editFlag = 1;
                    model.Name = $("#fpname").val();
                    //  model.IsActive = $("#fpactive")[0].checked;
                    model.FoodProgramCode = FoodProgramCodeInitial;
                    model.ConceptType2Code = $("#fptype").val();
                    model.Description = $("#fpdesc").val();
                    model.TileImage = tileImage;
                }
                fCode = model.FoodProgramCode;
                var file = document.getElementById("file").files[0];
                if (file != undefined) {
                    var fileName = document.getElementById("file").value;
                    var idxDot = fileName.lastIndexOf(".") + 1;
                    var extn = fileName.substr(idxDot, fileName.length).toLowerCase();
                    //var extn = file.type.split("/")[1];
                }
                if (document.getElementById("file").files.length > 0 && model.TileImage != null) {
                    model.TileImage = model.Name.replace(/[^\w]/gi, '').substring(0, 11) + '.' + extn;
                } else if (document.getElementById("file").files.length > 0 && model.TileImage == null) {
                    model.TileImage = model.Name.replace(/[^\w]/gi, '').substring(0, 11) + '.' + extn;//replace(/\s/g, '')
                }
                if (document.getElementById("file").files.length == 0 && tileImage == null) {
                    model.TileImage = "default.png";
                }
                if (model.FoodProgramCode != null) {
                    fpcode = model.FoodProgramCode;

                }
                else {
                    fpcode = model.Name.replace(/[^\w]/gi, '').substring(0, 11);
                }
                if (!sanitizeAndSend(model)) {
                    return;
                }

                $("#fpsubmit").attr('disabled', 'disabled');

                if (document.getElementById("file").files.length > 0) {
                    var formData = new FormData();
                    model.TileImage = fpcode + '.' + extn;
                    formData.append('FoodProgramCode', fpcode);
                    formData.append('folderName', "FoodProgramImages");
                    formData.append("file", file);
                    debugger;
                    $.ajax({
                        type: "POST",
                        //url: './Item/UploadFile',
                        url: './Item/UploadFile',
                        data: formData,
                        contentType: false,
                        processData: false,
                        async: false,
                        success: function (response) {
                            if (response != null) {
                                alert(response);
                                if (!response.fileupload) {
                                      Utility.Page_Alert(response.message, "Success", "OK", function () {
                                    });
                                }
                            }
                        },
                        error: function (error) {
                            Utility.UnLoading();
                            Utility.Page_Alert("Problem while uploading the Food Program Image on Server. Please contact Administrator", "Success", "OK", function () {
                            });
                        }
                    });
                }
                HttpClient.MakeSyncRequest(CookBookMasters.SaveFoodProgram, function (result) {
                    if (result.xsssuccess !== undefined && !result.xsssuccess) {
                        toastr.error(result.message);
                        $('#fpsubmit').removeAttr("disabled");
                        $("#fpname").focus();
                        Utility.UnLoading();
                    }
                    else {
                        if (result == false) {
                            $('#fpsubmit').removeAttr("disabled");
                            $("#fpname").focus();
                            Utility.UnLoading();
                            return;
                        }
                        else {

                            if (result == "Duplicate") {
                                $('#fpsubmit').removeAttr("disabled");
                                toastr.error("Duplicate Name, Kindly change");
                                $("#fpname").focus();
                                Utility.UnLoading();
                                return;
                            } else {
                                $(".k-overlay").hide();
                                var orderWindow = $("#FDwindowEdit").data("kendoWindow");
                                orderWindow.close();
                                $('#fpsubmit').removeAttr("disabled");
                                if (model.FoodProgramCode != null)
                                    toastr.success("Food Program configuration updated successfully");
                                else
                                    toastr.success("New Food Program added successfully");

                                $("#gridFoodProgram").data("kendoGrid").dataSource.data([]);
                                $("#gridFoodProgram").data("kendoGrid").dataSource.read();
                                $("#gridFoodProgram").data("kendoGrid").dataSource.sort({ field: "FoodProgramCode", dir: "asc" });
                            }
                        }
                    }
                }, {
                    model: model
                }, true);

                dayPartArray = [];
                if (model.FoodProgramCode != null) {
                    fCode = model.FoodProgramCode;
                }
                else {
                    if (fCode == null) {
                        fCode = $("#gridFoodProgram").data("kendoGrid").dataSource._data.sort((a, b) => (a.FoodProgramCode > b.FoodProgramCode) ? 1 : -1)[$("#gridFoodProgram").data("kendoGrid").dataSource._data.length - 1].FoodProgramCode;
                    }
                }
                var multiselect = $("#inputdaypart").data("kendoMultiSelect");
                var dataItems = multiselect.dataItems();
                dayPartArray = [];
                for (itm of dataItems) {
                    var dayPartModel = {
                        "ID": 0,
                        "FoodProgramCode": fCode,
                        "DayPartCode": itm.code,
                        "IsActive": 1
                    }
                    dayPartArray.push(dayPartModel);
                }
                if (dayPartArray.length > 0) {
                    SaveDayPartMapping();
                } else if (editFlag) {
                    var dayPartModel = {
                        "ID": 0,
                        "FoodProgramCode": fCode,
                        "DayPartCode": null,
                        "IsActive": 1
                    }
                    dayPartArray.push(dayPartModel);
                    SaveDayPartMapping();
                    editFlag = 0;;
                }

                }
            },1000);
        });
    }


    $("#removeImage").click(function () {
        $('#image').removeAttr('src');
        $("#file").val('')
        tileImage = "";
        $("#removeImage").css("display", "none");
    });

    $("#fpsubmit").click(function () {
        
        Utility.Loading();
        setTimeout(function () {
            var editFlag = 0;
            var fileuploaded = true;
        if ($("#fpname").val() === "") {
            toastr.error("Please provide Food Program Name");
            $("#fpname").focus();
            Utility.UnLoading();
            return;
        }
        var fpcode = "";
        if (fpvalidate() === true) {
            $("#fpname").removeClass("is-invalid");
            var model = item;
            if (model == null) {
                model = {
                    "ID": $("#fpid").val(),
                    "Name": $("#fpname").val(),
                    "IsActive": 1,
                    "FoodProgramCode": FoodProgramCodeInitial,
                    "ConceptType2Code": $("#fptype").val(),
                    "Description": $("#fpdesc").val(),
                    "TileImage": tileImage,
                }
                editFlag = 0;
            } else {
                editFlag = 1;
                model.Name = $("#fpname").val();
                //  model.IsActive = $("#fpactive")[0].checked;
                model.FoodProgramCode = FoodProgramCodeInitial;
                model.ConceptType2Code = $("#fptype").val();
                model.Description = $("#fpdesc").val();
                model.TileImage = tileImage;
            }
            fCode = model.FoodProgramCode;
            var file = document.getElementById("file").files[0];
            if (file != undefined) {
                var fileName = document.getElementById("file").value;
                var idxDot = fileName.lastIndexOf(".") + 1;
                var extn = fileName.substr(idxDot, fileName.length).toLowerCase();
                //var extn = file.type.split("/")[1];
            }
            if (document.getElementById("file").files.length > 0 && model.TileImage != null) {
                model.TileImage = model.Name.replace(/[^\w]/gi, '').substring(0, 11) + '.' + extn;
            } else if (document.getElementById("file").files.length > 0 && model.TileImage == null) {
                model.TileImage = model.Name.replace(/[^\w]/gi, '').substring(0, 11) + '.' + extn;//replace(/\s/g, '')
            }
            if (document.getElementById("file").files.length == 0 && tileImage == null) {
                model.TileImage = "default.png";
            }
            if (model.FoodProgramCode != null) {
                fpcode = model.FoodProgramCode;

            }

            else
                fpcode = model.Name.replace(/[^\w]/gi, '').substring(0, 11);


            //if (!sanitizeAndSend(model)) {
            //    return;
            //}
            $("#fpsubmit").attr('disabled', 'disabled');

            if (document.getElementById("file").files.length > 0) {
                var formData = new FormData();
                model.TileImage = fpcode + '.' + extn;
                formData.append('FoodProgramCode', fpcode);
                formData.append('folderName', "FoodProgramImages");
                formData.append("file", file);
                $.ajax({
                    type: "POST",
                    //url: './Item/UploadFile',
                    url: './Item/UploadFile',
                    data: formData,
                    contentType: false,
                    processData: false,
                    async: false,
                    success: function (response) {
                        if (response != null) {
                            if (!response.fileupload) {
                                fileuploaded = false;
                                Utility.Page_Alert(response.message, "Success", "OK", function () {
                                });
                            }
                        }
                    },
                    error: function (error) {
                        fileuploaded = false;
                        Utility.UnLoading();
                        Utility.Page_Alert("Problem while uploading the Food Program Image on Server. Please contact Administrator", "Success", "OK", function () {
                        });
                    }
                });
            }
            HttpClient.MakeSyncRequest(CookBookMasters.SaveFoodProgram, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#fpsubmit').removeAttr("disabled");
                    $("#fpname").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#fpsubmit').removeAttr("disabled");
                        $("#fpname").focus();
                        Utility.UnLoading();
                        return;
                    }
                    else {

                        if (result == "Duplicate") {
                            $('#fpsubmit').removeAttr("disabled");
                            toastr.error("Duplicate Name, Kindly change");
                            $("#fpname").focus();
                            Utility.UnLoading();
                            return;
                        } else {
                            $(".k-overlay").hide();
                            var orderWindow = $("#FDwindowEdit").data("kendoWindow");
                            orderWindow.close();
                            $('#fpsubmit').removeAttr("disabled");
                            if (model.FoodProgramCode != null) {
                                if (fileuploaded) {
                                    toastr.success("Food Program configuration updated successfully");
                                }
                                else {
                                    toastr.success("File not uplaoded. Food Program configuration updated successfully");
                                }
                            }
                            else {
                                if (fileuploaded) {
                                    toastr.success("New Food Program added successfully");
                                }
                                else {
                                    toastr.success("File not uplaoded. New Food Program added successfully");
                                }
                            }

                            $("#gridFoodProgram").data("kendoGrid").dataSource.data([]);
                            $("#gridFoodProgram").data("kendoGrid").dataSource.read();
                            $("#gridFoodProgram").data("kendoGrid").dataSource.sort({ field: "FoodProgramCode", dir: "asc" });
                        }
                    }
                }
            }, {
                model: model
            }, true);

            dayPartArray = [];
            if (model.FoodProgramCode != null) {
                fCode = model.FoodProgramCode;
            }
            else {
                if (fCode == null) {
                    fCode = $("#gridFoodProgram").data("kendoGrid").dataSource._data.sort((a, b) => (a.FoodProgramCode > b.FoodProgramCode) ? 1 : -1)[$("#gridFoodProgram").data("kendoGrid").dataSource._data.length - 1].FoodProgramCode;
                }
           }
            var multiselect = $("#inputdaypart").data("kendoMultiSelect");
            var dataItems = multiselect.dataItems();
            dayPartArray = [];
            for (itm of dataItems) {
                var dayPartModel = {
                    "ID": 0,
                    "FoodProgramCode": fCode,
                    "DayPartCode": itm.code,
                    "IsActive": 1
                }
                dayPartArray.push(dayPartModel);
            }
            if (dayPartArray.length > 0) {
                SaveDayPartMapping();
            } else if (editFlag) {
                var dayPartModel = {
                    "ID": 0,
                    "FoodProgramCode": fCode,
                    "DayPartCode": null,
                    "IsActive": 1
                }
                dayPartArray.push(dayPartModel);
                SaveDayPartMapping();
                editFlag = 0;;
            }

            }
        }, 1000);
    });


});