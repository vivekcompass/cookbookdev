﻿var recipedata = [];
var recipeCustomData = [];
var mappedData = [];
var colordata = [];
var mappedRecipeCode = [];
var DishCode;
var Dish_ID;
var multiarray = [];
var checkedRecipeCode = '';
var saveModel = [];
var regionmasterdata = [];
var checkedIds = [];
var user;
var uomdata = [];
var dialog;
var mogdata = [];
var basedata = [{ "value": "Select", "text": "Select" }, { "value": "No", "text": "Final Recipe" },
    { "value": "Yes", "text": "Base Recipe" }];
var baserecipedata = [];
var mogWiseAPLMasterdataSource = [];
var isSubSectorApplicable = false;
var subsectorMasterData = [];
var subSectorCodeDD = "";
var dietcategorydata = [];

function removeDuplicateObjectFromArray(array, key) {
    let check = {};
    let res = [];
    for (let i = 0; i < array.length; i++) {
        if (!check[array[i][key]]) {
            check[array[i][key]] = true;
            res.push(array[i]);
        }
    }
    return res;
}

HttpClient.MakeRequest(CookBookMasters.GetSubSectorMasterList, function (data) {

    subsectorMasterData = data;

    if (data.length > 1) {
        isSubSectorApplicable = true;
        subsectorMasterData.unshift({ "SubSectorCode": '0', "Name": "Select Sub Sector" })
        $("#inputSubSector").css("display", "block");
        $(".googleonly").css("display", "inline-block");
        populateSubSectorMasterDropdown();
    }
    else {
        isSubSectorApplicable = false;
        $(".googleonly").css("display", "none");
    }

}, { isAllSubSector: false }, false);

HttpClient.MakeRequest(CookBookMasters.GetMOGDataList, function (data) {

    var dataSource = data;

    mogdata = [];
    mogdata.push({ "MOG_ID": "Select MOG", "MOGName": "Select MOG", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "MOG_Code": "" });
    for (var i = 0; i < dataSource.length; i++) {
        if (dataSource[i].CostPerUOM == null) {
            dataSource[i].CostPerUOM = 0;
        }
        if (dataSource[i].CostPerKG == null) {
            dataSource[i].CostPerKG = 0;
        }
        if (dataSource[i].TotalCost == null) {
            dataSource[i].TotalCost = 0;
        }
        mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode });
    }

}, null, true);
HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

    var dataSource = data;
    uomdata = [];
    uomdata.push({ "value": "Select", "text": "Select", "UOMCode": "" });
    for (var i = 0; i < dataSource.length; i++) {
        uomdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode });
    }
   // populateDishDropdown();
}, null, true);

HttpClient.MakeRequest(CookBookMasters.GetRecipeDataList, function (data) {

    var dataSource = data;

    baserecipedata = [];
    baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
    for (var i = 0; i < dataSource.length; i++) {
        if (dataSource[i].CostPerUOM == null) {
            dataSource[i].CostPerUOM = 0;
        }
        if (dataSource[i].CostPerKG == null) {
            dataSource[i].CostPerKG = 0;
        }
        if (dataSource[i].TotalCost == null) {
            dataSource[i].TotalCost = 0;
        }
        if (dataSource[i].Quantity == null) {
            dataSource[i].Quantity = 0;
        }
        baserecipedata.push({
            "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].BaseRecipeCode
        });
    }
  //  baserecipedatafiltered = baserecipedata;
}, { condition: "Base" }, true);


HttpClient.MakeRequest(CookBookMasters.GetRegionMasterListByUserId, function (data) {
    var dataSource = data;
    sitemasterdata = dataSource;
    regionmasterdata = sitemasterdata//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
    if (data.length > 1) {
        regionmasterdata.unshift({ "RegionID": 0, "Name": "Select Region" })
        $("#btnGo").css("display", "inline-block");
    }
    
    else {
        $("#btnGo").css("display", "inline-none");
    }
    populateRegionMasterDropdown();
    

}, null, false);

HttpClient.MakeRequest(CookBookMasters.GetDietCategoryDataList, function (data) {

    var dataSource = data;
    dietcategorydata = [];
    dietcategorydata.push({ "value": "Select Diet Category", "text": "Select" });
    for (var i = 0; i < dataSource.length; i++) {
        dietcategorydata.push({ "value": dataSource[i].DietCategoryCode, "text": dataSource[i].Name });
    }
    populateDietCategoryDropdownFilter();
}, null, false);


function populateSubSectorMasterDropdown() {
    $("#inputSubSector").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "SubSectorCode",
        dataSource: subsectorMasterData,
        index: 0,
    });
}

function populateRegionMasterDropdown() {
    $("#ddlRegionMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "RegionID",
        dataSource: regionmasterdata,
        index: 0,
    });
    if (regionmasterdata.length == 1) {
        $("#btnGo").click();

    }

}

function customData(DishCode) {
    mappedRecipeCode = [];
    HttpClient.MakeSyncRequest(CookBookMasters.GetRegionDishRecipeMappingDataList, function (data) {

        mappedData = data;
        for (var d of mappedData) {
            mappedRecipeCode.push(d.RecipeCode);
        }

    }, { RegionID: regionCode, DishCode: DishCode, SubSectorCode: subSectorCodeDD }, true);
    HttpClient.MakeSyncRequest(CookBookMasters.GetRegionRecipeDataList, function (data) {
        recipeCustomData = data;
        recipeCustomData = recipeCustomData.filter(function (item) {
            for (var d of mappedData) {
                if (d.RecipeCode == item.RecipeCode) {
                    item.IsDefault = d.IsDefault;
                    item.DishCode = d.DishCode;
                    item.IsActive = d.IsActive;
                    return item;
                }
            }
        })
    }, { RegionID: regionCode, condition: "All", subSectorCode: subSectorCodeDD }, true);

}


$(function () {
   

    var datamodel;

    var dishcategorydata = [];




    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }
    $('#myInputDish').on('input', function (e) {
        var grid = $('#gridDish').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "DishCode" || x.field == "Name" || x.field == "DishAlias" || x.field == "UOMName" || x.field == "DishCategoryName"
                    || x.field == "DishTypeName" || x.field == "DietCategoryName" || x.field == "SubSectorName" ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        //if (x.field == "Status") {
                        //    var pendingString = "pending";
                        //    var savedString = "saved";
                        //    var mappedString = "mapped";
                        //    if (pendingString.includes(e.target.value.toLowerCase())) {
                        //        targetValue = "1";
                        //    }
                        //    else if (savedString.includes(e.target.value.toLowerCase())) {
                        //        targetValue = "2";
                        //    }
                        //    else if (mappedString.includes(e.target.value.toLowerCase())) {
                        //        targetValue = "3";
                        //    }
                        //} || x.field == "Status"
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $(document).ready(function () {
        $("#dietcategorysearch").hide();
        $("#windowEditMOGWiseAPL").kendoWindow({
            modal: true,
            width: "900px",
            height: "300px",
            cssClass: 'aplPopup',
            top: '80px !important',
            title: "MOG APL Detials - " ,
            actions: ["Close"],
            visible: false,
            animation: false
        });

        $("#windowEdit1").kendoWindow({
            modal: true,
            width: "1020px",
            height: "550px",
            title: "Region Recipe Details Inline- ",
            actions: ["Close"],
            visible: false,
            draggable: false,
            resizable: false,
            animation: false
        });
         dialog = $("#windowEdit1").data("kendoWindow");
        $("#windowEdit1").kendoWindow({
            animation: false
        });
        HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            if (user.SectorNumber == "20") {
                $(".mfgHide").hide();
            }
            else {
                $(".mfgHide").show();
            }
        }, null, false);
        
        $("#btnGo").on("click", function () {
            var dropdownlist = $("#ddlRegionMaster").data("kendoDropDownList");
            regionCode = dropdownlist.value();
            var regionName = dropdownlist.text();
            $("#location").text(" | " + regionName);
            if (user.SectorNumber == 10 && isSubSectorApplicable) {
                subSectorCodeDD = $("#inputSubSector").val();
            }
            populateDishGrid(regionCode);
            $("#myInputDishSearch").show();
            $("#actionDishRegion").show();
            
        });
      
        });

    //Food Program Section Start

    function populateDishGrid(RegionID) {

        Utility.Loading();
        var gridVariable = $("#gridDish");
        gridVariable.html("");
        if (user.SectorNumber == 10 && isSubSectorApplicable) {
            gridVariable.kendoGrid({
                excel: {
                    fileName: "Dish.xlsx",
                    filterable: true,
                    allPages: true
                },
                //selectable: "cell",
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                groupable: false,
                pageable: false,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    {
                        field: "DishCode", title: "Code", width: "30px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "Name", title: "Dish Name", width: "90px", attributes: {
                            style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                        },
                        template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:Name#</span>',
                    },
                    {
                        field: "DishAlias", title: "Dish Alias", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "SubSectorName", title: "Sub Sector", width: "40px", attributes: {

                            style: "text-align: left; font-weight:normal; padding-left:35px"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                    },
                    {
                        field: "UOMName", title: "UOM", width: "30px", attributes: {

                            style: "text-align: left; font-weight:normal; padding-left:35px"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                    },
                    {
                        field: "ColorCode", title: "Color", width: "35px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                        template: '<div class="colortag" style="background-color:#: RGBCode #"></div>',
                    },
                    {
                        field: "DishCategoryName", title: "Dish Category", width: "50px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },

                    {
                        field: "DishTypeName", title: "Type", width: "40px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {

                            style: "text-align: center"
                        },
                    },
                    {
                        field: "ModifiedOn", title: "Last Updated", width: "40px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        template: '# if (ModifiedOn == null) {#<span>-</span>#} else{#<span>#: kendo.toString(ModifiedOn, "dd-MMM-yy")#</span>#}#',
                        headerAttributes: {
                            style: "text-align: center;"
                        }
                    },
                    {
                        field: "DietCategoryName", title: "", width: "1px", attributes: {

                            style: "text-align: center; font-weight:normal;"
                        },
                        headerAttributes: {
                            style: "text-align: center; width:1px; display:none;"
                        },
                        template: '<span class="itemname"></span>',
                    },
                    {
                        field: "Edit", title: "Action", width: "50px",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                        command: [
                            {
                                name: 'Map Recipe',
                                click: function (e) {
                                    Utility.Loading();
                                    setTimeout(function () {
                                        var gridObj = $("#gridDish").data("kendoGrid");
                                        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                        datamodel = tr;
                                        DishName = tr.Name;
                                        $("#dishid").val(tr.ID);
                                        $("#inputdishcode1").text(tr.DishCode);
                                        var cuisineText = " Cuisine: " + tr.CuisineName + " | ";
                                        $("#inputcuisinelabel").text(cuisineText);

                                        if (user.SectorNumber == "10") {
                                            var dsubsectorText = " Subsector: " + $("#inputSubSector").data("kendoDropDownList").text() + " | ";
                                            $("#dssector").text(dsubsectorText);
                                            $("#dssector").show();
                                        }
                                        else {
                                            $("#dssector").hide();
                                        }
                                        $("#inputcuisinelabel").text(cuisineText);
                                        if (tr.Name != tr.DishAlias && tr.DishAlias != "" && tr.DishAlias != null)
                                            $("#inputdishname1").text(tr.Name + "(" + tr.DishAlias + ")");
                                        else
                                            $("#inputdishname1").text(tr.Name);
                                        $("#inputdishtype1").text(tr.DishTypeName);
                                        $("#inputdishcategory1").text(tr.DishCategoryName);
                                        DishCode = tr.DishCode;
                                        Dish_ID = tr.ID;
                                        $("#ItemMaster").hide();
                                        $("#mapRecipe").show();
                                        if (tr.DietCategoryName == "Veg") {
                                            $("#itemDietCategoryStatus").removeClass("statusNonVeg statusEgg");
                                            $("#itemDietCategory").removeClass("squareNonVeg squareEgg");
                                            $("#itemDietCategoryStatus").addClass("statusVeg");
                                            $("#itemDietCategory").addClass("squareVeg")
                                        } else if (tr.DietCategoryName == "Non-Veg") {
                                            $("#itemDietCategoryStatus").removeClass("statusVeg statusEgg");
                                            $("#itemDietCategory").removeClass("squareVeg squareEgg");
                                            $("#itemDietCategory").addClass("squareNonVeg")
                                            $("#itemDietCategoryStatus").addClass("statusNonVeg")
                                        } else {
                                            $("#itemDietCategoryStatus").removeClass("statusNonVeg statusVeg");
                                            $("#itemDietCategory").removeClass("squareNonVeg squareVeg");
                                            $("#itemDietCategory").addClass("squareEgg")
                                            $("#itemDietCategoryStatus").addClass("statusEgg")
                                        }
                                        $("#iname").text(tr.Name + " (Dish Code: " + tr.DishCode + ")");

                                        var dropdownlist = $("#ddlRegionMaster").data("kendoDropDownList");
                                        //  var regionName = dropdownlist.text();
                                        $("#topHeading").text("Region Dish Configuration - Map Recipe");//.append("(" + regionName + ")");
                                        // $("#location").text(" | " + regionName);
                                        //call and Grid genration
                                        multiarray = [];
                                        populateRecipeGridCustom();
                                        Utility.UnLoading();
                                    }, 2000);
                                }
                            }
                        ],
                    }
                ],
                pageable: {
                    pageSize: 150
                },
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetRegionDishDataList, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, {
                                RegionID: RegionID, SubSectorCode: subSectorCodeDD

                            }, true);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                DishCode: { type: "string", editable: false },
                                Name: { type: "string", editable: false },
                                DishAlias: { type: "string", editable: false },
                                UOMName: { type: "string", editable: false },
                                SubSectorName: { type: "string", editable: false },
                                DishCategoryName: { type: "string", editable: false },
                                DishTypeName: { type: "string", editable: false },
                                DietCategoryName: { type: "string", editable: false },
                                IsActive: { type: "boolean", editable: false },

                                ModifiedOn: { type: "date" },
                                //Status: { type: "string", editable: false }
                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {
                    var items = e.sender.items();

                    items.each(function (e) {


                    });
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
        }
        else {
            gridVariable.kendoGrid({
                excel: {
                    fileName: "Dish.xlsx",
                    filterable: true,
                    allPages: true
                },
                //selectable: "cell",
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                groupable: false,
                pageable: false,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    {
                        field: "DishCode", title: "Dish Code", width: "30px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "Name", title: "Dish Name", width: "90px", attributes: {
                            style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                        },
                        template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:Name#</span>',
                    },
                    {
                        field: "DishAlias", title: "Dish Alias", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },

                    {
                        field: "UOMName", title: "UOM", width: "30px", attributes: {

                            style: "text-align: left; font-weight:normal; padding-left:35px"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                    },
                    {
                        field: "ColorCode", title: "Color", width: "35px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                        template: '<div class="colortag" style="background-color:#: RGBCode #"></div>',
                    },
                    {
                        field: "DishCategoryName", title: "Dish Category", width: "50px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },

                    {
                        field: "DishTypeName", title: "Type", width: "40px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {

                            style: "text-align: center"
                        },
                    },
                    {
                        field: "ModifiedOn", title: "Last Updated", width: "40px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        template: '# if (ModifiedOn == null) {#<span>-</span>#} else{#<span>#: kendo.toString(ModifiedOn, "dd-MMM-yy")#</span>#}#',
                        headerAttributes: {
                            style: "text-align: center;"
                        }
                    },
                    {
                        field: "DietCategoryName", title: "", width: "1px", attributes: {

                            style: "text-align: center; font-weight:normal;"
                        },
                        headerAttributes: {
                            style: "text-align: center; width:1px; display:none;"
                        },
                        template: '<span class="itemname"></span>',
                    },
                    {
                        field: "Edit", title: "Action", width: "30px",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                        command: [
                            {
                                name: 'Map Recipe',
                                click: function (e) {
                                    Utility.Loading();
                                    setTimeout(function () {
                                        var gridObj = $("#gridDish").data("kendoGrid");
                                        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                        datamodel = tr;
                                        DishName = tr.Name;
                                        $("#dishid").val(tr.ID);
                                        $("#inputdishcode1").text(tr.DishCode);
                                        var cuisineText = " Cuisine: " + tr.CuisineName + " | ";
                                        $("#inputcuisinelabel").text(cuisineText);

                                        if (user.SectorNumber == "10") {
                                            var dsubsectorText = " Subsector: " + $("#inputSubSector").data("kendoDropDownList").text() + " | ";
                                            $("#dssector").text(dsubsectorText);
                                            $("#dssector").show();
                                        }
                                        else {
                                            $("#dssector").hide();
                                        }
                                        $("#inputcuisinelabel").text(cuisineText);
                                        if (tr.Name != tr.DishAlias && tr.DishAlias != "" && tr.DishAlias != null)
                                            $("#inputdishname1").text(tr.Name + "(" + tr.DishAlias + ")");
                                        else
                                            $("#inputdishname1").text(tr.Name);
                                        $("#inputdishtype1").text(tr.DishTypeName);
                                        $("#inputdishcategory1").text(tr.DishCategoryName);
                                        DishCode = tr.DishCode;
                                        Dish_ID = tr.ID;
                                        $("#ItemMaster").hide();
                                        $("#mapRecipe").show();
                                        if (tr.DietCategoryName == "Veg") {
                                            $("#itemDietCategoryStatus").removeClass("statusNonVeg statusEgg");
                                            $("#itemDietCategory").removeClass("squareNonVeg squareEgg");
                                            $("#itemDietCategoryStatus").addClass("statusVeg");
                                            $("#itemDietCategory").addClass("squareVeg")
                                        } else if (tr.DietCategoryName == "Non-Veg") {
                                            $("#itemDietCategoryStatus").removeClass("statusVeg statusEgg");
                                            $("#itemDietCategory").removeClass("squareVeg squareEgg");
                                            $("#itemDietCategory").addClass("squareNonVeg")
                                            $("#itemDietCategoryStatus").addClass("statusNonVeg")
                                        } else {
                                            $("#itemDietCategoryStatus").removeClass("statusNonVeg statusVeg");
                                            $("#itemDietCategory").removeClass("squareNonVeg squareVeg");
                                            $("#itemDietCategory").addClass("squareEgg")
                                            $("#itemDietCategoryStatus").addClass("statusEgg")
                                        }
                                        $("#iname").text(tr.Name + " (Dish Code: " + tr.DishCode + ")");

                                        var dropdownlist = $("#ddlRegionMaster").data("kendoDropDownList");
                                        //  var regionName = dropdownlist.text();
                                        $("#topHeading").text("Region Dish Configuration - Map Recipe");//.append("(" + regionName + ")");
                                        // $("#location").text(" | " + regionName);
                                        //call and Grid genration
                                        multiarray = [];
                                        populateRecipeGridCustom();
                                        Utility.UnLoading();
                                    }, 2000);
                                }
                            }
                        ],
                    }
                ],
                pageable: {
                    pageSize: 150
                },
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetRegionDishDataList, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, {
                                RegionID: RegionID, SubSectorCode: subSectorCodeDD

                            }, true);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                DishCode: { type: "string", editable: false },
                                Name: { type: "string", editable: false },
                                DishAlias: { type: "string", editable: false },
                                UOMName: { type: "string", editable: false },
                                DishCategoryName: { type: "string", editable: false },
                                DishTypeName: { type: "string", editable: false },
                                DietCategoryName: { type: "string", editable: false },
                                IsActive: { type: "boolean", editable: false },
                                SubSectorName: { type: "string", editable: false },
                                ModifiedOn: { type: "date" },
                                //Status: { type: "string", editable: false }
                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {
                    var items = e.sender.items();

                    items.each(function (e) {


                    });
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
        }
       
    }

    
    $("#btnCancelDish").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });


    $("#btnSubmitDish").click(function () {
        ;
        if ($("#inputdishname").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputdishname").focus();
        }
        else {
            var localdishcategorydata = [];

            localdishcategorydata = dishcategorydata.filter(function (item) {

                return item.value == $("#inputdishcategory").val();
            });

            var dishcategoryid = localdishcategorydata[0].DishCategoryID;

            var model;
            if (datamodel != null) {
                model = datamodel;
                model.DishCode = $("#inputdishcode").val();
                model.Name = $("#inputdishname").val();
                model.DishAlias = $("#inputdishalias").val();
                model.DietCategoryCode = $("#inputdietcategory").val();
                model.DishCategoryCode = $("#inputdishcategory").val();
                model.DishCategory_ID = dishcategoryid;
                //model.DishSubCategoryCode= $("#inputdishsubcategory").val();
                model.UOMCode = $("#inputuom").val();
                model.ServingTemperatureCode = $("#inputservingtemperature").val();
                model.DishTypeCode = $("#inputdishtype").val();
                model.ColorCode = $(".colortagedit").val();
            }
            else {
                model = {
                    "ID": $("#dishid").val(),
                    "DishCode": $("#inputdishcode").val(),
                    "Name": $("#inputdishname").val(),
                    "DishAlias": $("#inputdishalias").val(),
                    "DietCategoryCode": $("#inputdietcategory").val(),
                    "DishCategoryCode": $("#inputdishcategory").val(),
                    "DishCategory_ID": dishcategoryid,
                    //"DishSubCategoryCode": $("#inputdishsubcategory").val(),
                    "UOMCode": $("#inputuom").val(),
                    "ServingTemperatureCode": $("#inputservingtemperature").val(),
                    "DishTypeCode": $("#inputdishtype").val(),
                    "IsActive": true,
                    "ColorCode": $(".colortagedit").val(),
                }
            }

            $("#btnSubmitDish").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveDishData, function (result) {

                if (result == false) {
                    $('#btnSubmitDish').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputdishname").focus();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#windowEditDish").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmitDish').removeAttr("disabled");
                    if (model.ID > 0)
                        toastr.success("Dish configuration updated successfully");
                    else
                        toastr.success("New Dish added successfully");
                    $("#gridDish").data("kendoGrid").dataSource.data([]);
                    $("#gridDish").data("kendoGrid").dataSource.read();
                    //});
                }
            }, {
                model: model

            } , true);
        }
    });
});

function back() {

    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $("#ItemMaster").show();
            $("#mapRecipe").hide();
            $("#topHeading").text("Regional Dish Configuration");
        },
        function () {
        }
    );
}
function back1() {

    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $(".k-window").hide();
            $(".k-overlay").hide();
        },
        function () {
        }
    );       
   
}





function populateRecipeGridCustom() {


   customData(DishCode);

    var gridVariable = $("#gridRecipeCustom");//Hare Krishna!!
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "Recipe.xlsx",
            filterable: true,
            allPages: true
        },
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        //pageable: {
        //    pageSize: 15
        //},
        columns: [
            {
                field: "RecipeCode", title: "Recipe Code", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Recipe Name", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.Name) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetailsCustom(this)'></i>`;
                    return html;
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "CostPerKG", title: "CostPerKG", format: Utility.Cost_Format,  width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "IsBase", title: "Base Recipe", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: '# if (IsBase == true) {#<div>Yes</div>#} else {#<div>No</div>#}#',
            },
            {

                template: templateIncludeFunction,
              
                width: "35px", title: "Include", field:"IsActive",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center; vertical-align:middle"
                }
            },

            {
                field: "IsDefault", title: "Default ", width: "80px", attributes: {

                    style: "text-align: center; font-weight:normal;display:list-item"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: templateFunction,
            }
        ],
        dataSource: {
            data: recipeCustomData,
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Name: { editable: false },
                        IsActive: { editable: false }
                    }
                }
            },
            pageSize: 150,
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
        
        },
        change: function (e) {
        },
        
    })
    function templateFunction(dataItem) {
        //var item = "<span>"
        //if (dataItem.IsDefault) {
        //    item += "<input type='radio' style='margin-top: 7px;' name='" + dataItem.DishCode + "' onclick='setDataItem(this);'   checked=checked />";
        //    console.log(dataItem);
        //    checkedRecipeCode = dataItem.RecipeCode;
        //} else {
        //    item += "<input  style='margin-top: 7px;' type='radio' onclick='setDataItem(this);' name='" + dataItem.DishCode + "'/>";
        //}
        //item += "</span>"

        //return item;
        var item = "<div class='icheck-success d-inline'>"
        if (dataItem.IsDefault) {
            item += "<input type='radio' style='margin-top: 7px;' name='" + dataItem.DishCode + "' id='" + dataItem.RecipeCode + "' onclick='setDataItem(this);'   checked=checked />";
            console.log(dataItem);
            checkedRecipeCode = dataItem.RecipeCode;
        } else {
            item += "<input  style='margin-top: 7px;' type='radio' onclick='setDataItem(this);'  id='" + dataItem.RecipeCode + "' name='" + dataItem.DishCode + "'/>";
        }
        item += "<label for='" + dataItem.RecipeCode + "' class='form-control-plaintext' style='display: inline!important'></label ></div>"
        return item;

    };

    function templateIncludeFunction(dataItem) {
        var item = "<label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px'>"
        if (dataItem.IsActive) {
            item += "<input type='checkbox' checked class='checkbox'>";
        } else {
            item += "<input type='checkbox' class='checkbox'>";
        }
        item += "<span class='checkmarkgrid'></span></label>"
        return item;
    };


    Utility.UnLoading();
    //bind click event to the checkbox
    var grid = gridVariable.data("kendoGrid");
    grid.table.on("click", ".checkbox", selectRow);
}
function selectRow() {
    var checked = this.checked,
        td = $(this).closest("td"),
        row = $(this).closest("tr"),
        grid = $("#gridRecipeCustom").data("kendoGrid"),
        dataItem = grid.dataItem(row);
    dataItem.IsActive = this.checked;
    grid.dataItem(row).IsActive = dataItem.IsActive;
    checkedIds[dataItem.id] = checked;
    console.log(dataItem);
}

function setDataItem(item) {
    var grid = $("#gridRecipeCustom").data("kendoGrid");
    var row = $(item).closest("tr");
    var dataItem = grid.dataItem(row);
    console.log(dataItem)
    dataItem.IsDefault = true;
    checkedRecipeCode = dataItem.RecipeCode;
    for (x of recipeCustomData) {
        if (x.RecipeCode == checkedRecipeCode) {
            x.IsDefault = true;
        }
        else {
            x.IsDefault = false;
        }
    }

    var gridObj = $("#gridRecipeCustom").data("kendoGrid");
    //var gridData = gridObj.dataSource._data;

    for (var i = 0; i < gridObj.dataSource._data.length;i++) {
        if (gridObj.dataSource._data[i].RecipeCode == checkedRecipeCode) {
            gridObj.dataSource._data[i].IsDefault = true;
        }
        else {
            gridObj.dataSource._data[i].IsDefault = false;
        }
    }
}

function saveDB() {

    //make model;
    //grid data
    var grid = $("#gridRecipeCustom").data("kendoGrid");
    var currentData = grid._data;
   // saveModel = [];
    //Validation check
   
    var validData = currentData.filter(item => item.IsActive && item.IsDefault);
    if (validData == null ||  validData.length == 0) {
        toastr.error("Please select at least one Default  Recipe.");
        return false;
    }
    for (item of currentData) {
        for (mainItem of mappedData) {
            if (item.RecipeCode == mainItem.RecipeCode) {
                mainItem.IsActive = item.IsActive;
                mainItem.IsDefault = item.IsDefault;
                mainItem.CreatedOn = kendo.parseDate(item.CreatedOn);
                mainItem.CreatedBy = item.CreatedBy;
                mainItem.ModifiedOn = Utility.CurrentDate();
                mainItem.ModifiedBy = user.UserId;
            }
        }
    }
    //return;
    //call and message display 
    if (mappedData.length > 0) {


        HttpClient.MakeRequest(CookBookMasters.SaveRegionDishRecipeMappingList, function (result) {
            if (result == false) {
                toastr.error("Some error occured, please try again.");
            }
            else {
                $(".k-overlay").hide();
                toastr.success("The selected Recipe(s) has been mapped to this Dish");
            }
        }, {
                model: mappedData

        });
    }
}

function showDetailsCustom(e) {
   
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridRecipeCustom").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    var gridObj = $("#gridRecipeCustom").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.Name == dataItem.Name)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.RecipeCode);
    populateMOGGrid1(tr.RecipeCode);

    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");
    $('#grandTotal1').html(tr.TotalCost);
    $('#grandCostPerKG1').html(tr.CostPerKG);
    $('#inputinstruction1').val(tr.Instructions);
  

    dialog.title("Recipe Details Inline - " + RecipeName);
    return true;

}

function populateDietCategoryDropdownFilter() {

    $("#dietcategorysearch").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: dietcategorydata,
        index: 0,
        select: function (e) {
            if (e.item.data().offsetIndex == 0) {
                //e.preventDefault();
                var filter = { logic: 'or', filters: [] };
                var grid = $('#gridDish').data('kendoGrid');
                grid.dataSource.filter(filter);
                return;
            }

            var grid = $('#gridDish').data('kendoGrid');
            var columns = grid.columns;

            var filter = { logic: 'or', filters: [] };
            columns.forEach(function (x) {
                if (x.field) {
                    if (x.field == "DietCategoryName") {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;
                        var targetValue = e.sender.dataItem(e.item).text;

                        if (type == 'string') {
                            if (x.field == "Status") {
                                var pendingString = "pending";
                                var savedString = "saved";
                                var mappedString = "mapped";
                                if (pendingString.includes(targetValue.toLowerCase())) {
                                    targetValue = "1";
                                }
                                else if (savedString.includes(targetValue.toLowerCase())) {
                                    targetValue = "2";
                                }
                                else if (mappedString.includes(targetValue.toLowerCase())) {
                                    targetValue = "3";
                                }
                            }
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: targetValue
                            })
                        }
                        else if (type == 'number') {

                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        } else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format(x.format, data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(targetValue) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                }
            });
            grid.dataSource.filter(filter);

        }

    });

    $("#dietcategorysearch").css("display", "none !important");
}



function backCreate() {
    $("#mapRecipe").show();
    $("#windowEdit0").hide();
  

}
function populateBaseRecipeGrid1(recipeCode) {

   
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe1");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" },
        ],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetails2(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange
                        });
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Region Qty", width: "35px",
                template: '# if (IsMajor == true) {#<div><input class="inputbrqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputbrqty"  name="Quantity" style="background-color:transparent!important; border:1px solid transparent!important" readonly oninput="calculateItemTotal(this)" /></div>#}#',
                // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorIngredientPerc", title: "Qty Change %", width: "50px", format: "{0:0.00}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= (SectorIngredientPerc-IngredientPerc).toFixed(2)# </div>#} #',
            },
            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "SectorTotalCost", title: "Sector Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "sectortotalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            }
        ],
       
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetRegionBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe1").css("display", "block");
                            $("#emptybr1").css("display", "none");
                        } else {
                            $("#gridBaseRecipe1").css("display", "none");
                            $("#emptybr1").css("display", "block");
                        }

                    },
                        {
                            regionID: $("#ddlRegionMaster").val(),
                            RecipeCode: recipeCode, subSectorCode: subSectorCodeDD
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });

    var toolbar = $("#gridBaseRecipe1").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");
   // cancelChangesConfirmation('#gridBaseRecipe1');
    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe1 .k-grid-content").addClass("gridInside");




}

function populateUOMDropdown1() {
    $("#inputuom1").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}


function populateMOGGrid1(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG1");
    gridVariable.html("");
    if (user.SectorNumber == '20') {
        gridVariable.kendoGrid({
            groupable: false,
            editable: false,
            columns: [
                {
                    field: "MOGName", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "DKgTotal", title: "DKG", width: "50px", format: "{0:0.000}", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Region Qty", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetRegionMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG1").css("display", "block");
                                $("#emptymog1").css("display", "none");
                            } else {
                                $("#gridMOG1").css("display", "none");
                                $("#emptymog1").css("display", "block");
                            }
                        },
                            {
                                regionID: $("#ddlRegionMaster").val(),
                                RecipeCode: recipeCode,
                                subSectorCode: subSectorCodeDD
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true } },
                            UOMName: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();

                items.each(function (e) {

                    var dataItem = grid.dataItem(this);

                    var ddt = $(this).find('.mogTemplate');

                    $(ddt).kendoDropDownList({
                        enable: false,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange1
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
    }
    else {
        gridVariable.kendoGrid({
            toolbar: [{ name: "cancel", text: "Reset" }],
            groupable: false,
            editable: false,
            columns: [
                {
                    field: "MOGName", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                   },
                   
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Region Qty", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "SectorIngredientPerc", title: "Qty Change % ", width: "50px", format: "{0:0.00}", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= ((Quantity-SectorQuantity)*100/SectorQuantity).toFixed(2)# </div>#} #',
                },


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "SectorTotalCost", title: "Sector  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "sectortotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetRegionMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG1").css("display", "block");
                                $("#emptymog1").css("display", "none");
                            } else {
                                $("#gridMOG1").css("display", "none");
                                $("#emptymog1").css("display", "block");
                            }
                        },
                            {
                                regionID: $("#ddlRegionMaster").val(),
                                RecipeCode: recipeCode,
                                subSectorCode: subSectorCodeDD
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true } },
                            UOMName: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();

                items.each(function (e) {

                    var dataItem = grid.dataItem(this);

                    var ddt = $(this).find('.mogTemplate');

                    $(ddt).kendoDropDownList({
                        enable: false,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange1
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
    }
    
}
function populateBaseDropdown1() {
    $("#inputbase1").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}
function onBRDDLChange(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    //baserecipedatafiltered = baserecipedata.filter(function (recipeObj) {
    //    return recipeObj.BaseRecipe_ID != e.sender.value();
    //});

    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    // $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};

function onMOGDDLChange1(e) {
    //if (checkItemAlreadyExist(e.sender.value(), 'gridMOG1', false)) {
    //    toastr.error("Base Recipe already exists.");
    //    var dropdownlist = $("#ddlMOG1").data("kendoDropDownList");
    //    dropdownlist.select(0);
    //    dropdownlist.trigger("change");
    //    return false;
    //}
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function getMOGWiseAPLMasterData(MOGCode) {
    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG_Region, function (result) {
        Utility.UnLoading();

        if (result != null) {
            mogWiseAPLMasterdataSource = result;
        }
        else {
        }
    }, { mogCode: MOGCode, Region_ID: regionCode }
        , true);
}

function populateAPLMasterGrid() {
    Utility.Loading();


    var gridVariable = $("#gridMOGWiseAPL");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "APLMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        scrollable: true,
        noRecords: {
            template: "No Records Available"
        },

        columns: [
            {
                field: "ArticleNumber", title: "Article Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ArticleDescription", title: "Article Name", width: "150px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOM", title: "UOM", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            },
            {
                field: "SiteName", title: "SiteName", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            }
            ,
            {
                field: "ArticleCost", title: "Standard Cost", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            },

            {
                field: "StandardCostPerKg", title: "Cost Per KG", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            }
        ],
        dataSource: {
            data: mogWiseAPLMasterdataSource,
            schema: {
                model: {
                    id: "ArticleID",
                    fields: {
                        ArticleNumber: { type: "string" },
                        ArticleDescription: { type: "string" },
                        UOM: { type: "string" },
                        ArticleType: { type: "string" },
                        Hierlevel3: { type: "string" },
                        MerchandizeCategoryDesc: { type: "string" },
                        MOGName: { type: "string" },
                        SiteCode: { type: "string" },
                        SiteName: { type: "string" }
                    }
                }
            },
            // pageSize: 100,
        },

        //maxHeight: 250,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {


        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
    //$(".k-label")[0].innerHTML.replace("items", "records");
}