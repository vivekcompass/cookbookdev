﻿var mogAPLImpactedData = [];

$("document").ready(function () {
   // populateMOGAPLDependencies();

    $("#btnDoLater").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditMOGAPLDependencies").data("kendoWindow");
        orderWindow.close();
    });

    $("#btnAgree").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditMOGAPLDependencies").data("kendoWindow");
        orderWindow.close();
    });
});

function getMOGAPLDependenciesData() {
    var mogId = 2;//$("#hiddenMogID").val();
    HttpClient.MakeSyncRequest(CookBookMasters.GetMOGAPLImpactedDataList, function (data) {
        mogAPLImpactedData = data;
    }, { mogID: mogId }, false);
}

function populateMOGAPLDependencies() {
    getMOGAPLDependenciesData();
    $('.mogName').text(MOGName);
    $("#totalImapactedAPLDishes").text(mogAPLImpactedData.TotalImapactedDishes);
    $("#totalImapactedAPLSites").text(mogAPLImpactedData.TotalImapactedSites);
    $("#totalImapactedAPLRegion").text(mogAPLImpactedData.TotalImapactedRegions);
    $("#previousMappedAPL").text(mogAPLImpactedData.PreviousMappedAPL);
    $("#currentMappedAPL").text(mogAPLImpactedData.CurrentMappedAPL);

    
    populateDishGrid();
    populateSiteGrid();
    populateRegionGrid();
    Utility.UnLoading();
}


function populateDishGrid() {
    Utility.Loading();
    var gridVariable = $("#dishAPLGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        noRecords: true,
        columns: [
            {
                field: "DishCode", title: "Dish Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishName", title: "Dish Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishType", title: "Dish Type", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "PreviousCost", title: "Previous Cost", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "CurrentCost", title: "Current Cost", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogAPLImpactedData.MOGAPLImpactedDishesData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        DishCode: { type: "string" },
                        DishName: { type: "string" },
                        DishType: { type: "string" },
                        PreviousCost: { type: "string" },
                        CurrentCost: { type: "string" }
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateSiteGrid() {
    Utility.Loading();
    var gridVariable = $("#siteAPLGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        noRecords: true,
        columns: [
            {
                field: "SiteName", title: "Site", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishCode", title: "Dish Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishName", title: "Dish Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishType", title: "Dish Type", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "PreviousCost", title: "Previous Cost", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "CurrentCost", title: "Current Cost", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogAPLImpactedData.MOGAPLImpactedDishesRegionData,
            schema: {
                model: {
                    fields: {
                        fields: {
                            DishCode: { type: "string" },
                            DishName: { type: "string" },
                            DishType: { type: "string" },
                            PreviousCost: { type: "string" },
                            CurrentCost: { type: "string" },
                            Site: { type: "string" },
                        }
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateRegionGrid() {
    Utility.Loading();
    var gridVariable = $("#regionAPLGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        noRecords: true,
        columns: [
            {
                field: "RegionName", title: "Region", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishCode", title: "Dish Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishName", title: "Dish Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishType", title: "Dish Type", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "PreviousCost", title: "Previous Cost", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "CurrentCost", title: "Current Cost", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogAPLImpactedData.MOGAPLImpactedDishesSiteData,
            schema: {
                model: {
                    fields: {
                        DishCode: { type: "string" },
                        DishName: { type: "string" },
                        DishType: { type: "string" },
                        PreviousCost: { type: "string" },
                        CurrentCost: { type: "string" },
                        Region: { type: "string" }
                    }
                }
            },
            pageSize: 100,
        },
    })
}

