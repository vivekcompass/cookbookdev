﻿//import { Util } from "../bootstrap/js/bootstrap.bundle";

var mogWiseAPLMasterdataSource = [];
var checkedIds = {};
var checkedAPLCodes = [];
var user;
var status = "";
var varname = "";
var datamodel;
var ProductName = "";
var BATCHNUMBER = "";
var NewProductName = "";
var ProductCode = "";
var ProductID = 0;
var uomdata = [];
var aplMasterdataSource = [];
var sectorMasterdataSource = [];
var isShowPreview = false;
var CheckedTrueAPLCodes = [];
var mogWiseAPLArray = [];
var mogImpactedData = [];
var isStatusSave = false;
var mogStatus = false;
var isRIEnabled = false;
var isMOGAPLMappingSave = false;
var aplFilterDataSource = [];
var uommodulemappingdata = [];
var expirycategorymappingdata = [];

$(function () {

    $("#ExpiryCategoryMappingwindowEdit").kendoWindow({
        modal: true,
        width: "475px",
        height: "150px",
        title: "Product Details - " + ProductName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#ExpiryCategoryPrintingwindowEdit").kendoWindow({
        modal: true,
        width: "475px",
        height: "165px",
        title: "Product Details - " + ProductName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#datepickerscandate").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date(),
        change: onchangeScanDate
    });

    function onchangeScanDate(e) {
        debugger;
        var expiryDays = 0;
        var expiryShelfLife = $("#pshelflife").val();
        var expiryShelfLifeUOM = $("#pshelflifeuom").val();
        if (expiryShelfLife != null && expiryShelfLifeUOM != null) {
            if (expiryShelfLifeUOM == "Days") {
                expiryDate = expiryShelfLife
            }
            else {

            }
        }
        var expiryDate = kendo.date.addDays(expiryDays, expiryShelfLife);
        $("#pexpiryDate").text(expiryDate);
    }


    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $('#myInput').on('input', function (e) {
        var grid = $('#gridExpiryCategoryMapping').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ProductCode" || x.field == "ProductName" || x.field == "ProductType" || x.field == "ProductExpiryName" || x.field == "ShelfLife" || x.field == "ShelfLifeUOMName") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        if (x.field == "Status") {
                            var pendingString = "pending";
                            var savedString = "saved";
                            var mappedString = "mapped";
                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "3";
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        Utility.Loading();
        setTimeout(function () {
            onLoad();
            Utility.UnLoading();
        },2000);
    });

    function onLoad() {
        $("#btnExport").click(function (e) {
            var grid = $("#gridExpiryCategoryMapping").data("kendoGrid");
            grid.saveAsExcel();
        });

        HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNew").css("display", "none");
            //}
            populateExpiryProductGrid();
        }, null, true);

        HttpClient.MakeRequest(CookBookMasters.GetSectorDataList, function (data) {
            var dataSource = data;
            sectorMasterdataSource = [];
            for (var i = 0; i < dataSource.length; i++) {
                sectorMasterdataSource.push({ "value": dataSource[i].ID, "text": dataSource[i].SectorName, "SectorNumber": dataSource[i].SectorNumber });
            }
            populateSectorMasterDropdown();

        }, null, true);

        //HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
        //    var dataSource = data;
        //    uommodulemappingdata = [];
        //    uommodulemappingdata.push({ "value": "Select", "text": "Select" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        uommodulemappingdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].UOMName, "UOMModuleCode": dataSource[i].UOMModuleCode });
        //    }
        //    uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001' || m.text == "Select");
        //    populateUOMDropdown();
        //    populateUOMDropdown_Bulk();

        //}, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetExpiryCategoryDataList, function (data) {
            var dataSource = data;
            expirycategorymappingdata = [];
            expirycategorymappingdata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].IsActive) {
                    expirycategorymappingdata.push({
                        "value": dataSource[i].ExpiryCategoryCode, "text": dataSource[i].Name,
                        "ShelfLife": dataSource[i].ShelfLife, "ShelfLifeName": dataSource[i].ShelfLifeName
                    });
                }
            }
            populateExpiryCategoryDropdown();
        }, null, false);
        

    }


    function populateUOMDropdown() {
        $("#inputuom").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: uomdata,
            index: 0,
        });
    }

    function populateExpiryCategoryDropdown() {
        $("#expiryCategory").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: expirycategorymappingdata,
            index: 0,
            change: onExpiryCategoryChange
        });
    }

    function onExpiryCategoryChange(e) {
        var dataItem = e.sender.dataItem();
        $("#shelflife").text(dataItem.ShelfLife);
       // $("#shelflifeuom").text(dataItem.ShelfLifeName);
        $("#shelflife").text(dataItem.ShelfLife + " " + dataItem.ShelfLifeName);
        $(".shelflifediv").show();
    }

    function populateExpiryProductGrid() {
        var gridVariable = $("#gridExpiryCategoryMapping");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "ExpiryCategoryMapping.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,

            pageable: {
                pageSize: 15,
                messages: {
                    display: "{0}-{1} of {2} records"
                }
            },
            //reorderable: true,
            scrollable: true,
            columns: [
                {
                    field: "ProductCode", title: "Product Code", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: left;"
                    }
                },
                {
                    field: "ProductName", title: "Product Name", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "ProductType", title: "Product Type", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ProductExpiryName", title: "Expiry Category", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "ShelfLife", title: "Shelf Life", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "ShelfLifeUOMName", title: "Shelf Life UOM", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Edit", title: "Action", width: "70px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: left"
                    },
                    command: [
                        {
                            name: 'Map Expiry Category',
                            click: function (e) {
                                isStatusSave = false;
                                $("#expiryCategory").data('kendoDropDownList').value("Select");
                                var gridObj = $("#gridExpiryCategoryMapping").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                ProductName = tr.ProductName;
                                ProductCode = tr.ProductCode;
                                ProductID = tr.ID;
                                var dialog = $("#ExpiryCategoryMappingwindowEdit").data("kendoWindow");
                               
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open().element.closest(".k-window").css({
                                    top: 167
                                });
                                dialog.center();

                                $("#productcodelabel").text(tr.ProductCode);
                                $("#inputproductnamelabel").text(tr.ProductName);
                                $("#inputproducttypelabel").text(tr.ProductType);
                               
                                if (tr.ProductExpiryCode != null && tr.ProductExpiryCode != "") {
                                    $(".shelflifediv").show();
                                    $("#expiryCategory").data('kendoDropDownList').value(tr.ProductExpiryCode);
                                    $("#shelflife").text(tr.ShelfLife + " " + tr.ShelfLifeUOMName);
                                   // $("#shelflifeuom").text(tr.ShelfLifeUOMName);
                                   
                                }
                                else {
                                    $(".shelflifediv").hide();
                                }
                                
                                dialog.title("Product Details - " + ProductName);
                                return true;
                            }
                        },
                        //{
                        //    name: 'Print Label',
                        //    click: function (e) {
                        //        isStatusSave = false;
                        //        var gridObj = $("#gridExpiryCategoryMapping").data("kendoGrid");
                        //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                        //        datamodel = tr;
                        //        ProductName = tr.ProductName;
                        //        ProductCode = tr.ProductCode;
                        //        ProductID = tr.ID;
                        //        var dialog = $("#ExpiryCategoryPrintingwindowEdit").data("kendoWindow");

                        //        $(".k-overlay").css("display", "block");
                        //        $(".k-overlay").css("opacity", "0.5");
                        //        dialog.open().element.closest(".k-window").css({
                        //            top: 167
                        //        });
                        //        dialog.center();

                        //        $("#pproductcodelabel").text(tr.ProductCode);
                        //        $("#pinputproductnamelabel").text(tr.ProductName);
                        //        $("#pinputproducttypelabel").text(tr.ProductType);

                        //        if (tr.ProductExpiryCode != null && tr.ProductExpiryCode != "") {
                        //            $("#pexpiryCategory").text(tr.ProductExpiryName);

                        //            $("#pshelflife").val(tr.ShelfLife);
                        //            $("#pshelflifeuom").val(tr.ShelfLifeUOMName);

                        //            var expiryDays = 0;
                        //            if (tr.ShelfLife != null && tr.ShelfLifeUOMName != null) {
                        //                if (expiryShelfLifeUOM == "Days") {
                        //                    expiryDate = tr.ShelfLife
                        //                }
                        //                else {

                        //                }
                        //            }
                        //            var expiryDate = kendo.date.addDays(expiryDays, "Days");
                        //            $("#pexpiryDate").text(expiryDate);

                        //        }

                        //        dialog.title("Product Details - " + ProductName);
                        //        return true;
                        //    }
                        //}
                       
                    ],
                },


            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetExpiryProductDataList, function (result) {
                            Utility.UnLoading();
                            if (result != null) {

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                             , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ProductCode: { type: "string" },
                            ProductName: { type: "string" },
                            ProductType: { type: "string" },
                            ProductExpiryCode: { type: "string" },
                            ProductExpiryName: { type: "string" },
                            ShelfLifeUOMCode: { type: "string" },
                            ShelfLife: { type: "int" },
                            ShelfLifeUOMName: { type: "string" }
                        }
                    }
                },
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {

                var items = e.sender.items();
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.ProductExpiryCode == "" || model.ProductExpiryCode == null) {
                        $(this).find(".k-grid-PrintLabel").addClass("k-state-disabled");
                    }
                });


                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }
                    else {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    }

                }
                var items = e.sender.items();
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "ProductCode" || col == "ProductName" || col == "ProductType" || col == "ProductExpiryName" || col == "ShelfLife" || col == "ShelfLifeUOMName") {
                        return col;
                    }
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
    //    if ($(".k-label")[0]!= null)
           //$(".k-label")[0].innerHTML.replace("items", "records");
    }

   

    $("#NatiMogbtnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#ExpiryCategoryMappingwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#PrintCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#ExpiryCategoryPrintingwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });


    $("#PrintPreviewSubmit").on("click", function () {
      
    });

    $("#PrintSubmit").on("click", function () {

    });

    $("#NatiMogbtnSubmit").click(function () {
        if ($("#expiryCategory").val() === "") {
            toastr.error("Please select Expiry Category");
            $("#expiryCategory").focus();
            return;
        }
        else {
            var model = {
                "ProductCode": $("#productcodelabel").text(),
                "ProductType": $("#inputproducttypelabel").text(),
                "ProductExpiryCode": $("#expiryCategory").val()
            }
            $("#NatiMogbtnSubmit").attr('disabled', 'disabled');
            if (!sanitizeAndSend(model)) {
                return;
            }
            HttpClient.MakeRequest(CookBookMasters.SaveExpiryProductData, function (result) {
                if (result == false) {
                    $('#secredpsubmit').removeAttr("disabled");
                    toastr.success("There was some error, the record cannot be saved");

                }
                else {
                    if (result == false) {
                        $('#NatiMogbtnSubmit').removeAttr("disabled");
                        toastr.success("There was some error, the record cannot be saved");
                    }
                    else {
                        $(".k-overlay").hide();
                        $('#NatiMogbtnSubmit').removeAttr("disabled");

                        toastr.success("Expiry Category mapped successfully");
                        $(".k-overlay").hide();
                        var orderWindow = $("#ExpiryCategoryMappingwindowEdit").data("kendoWindow");
                        orderWindow.close();
                        $("#gridExpiryCategoryMapping").data("kendoGrid").dataSource.data([]);
                        $("#gridExpiryCategoryMapping").data("kendoGrid").dataSource.read();
                        $("#gridExpiryCategoryMapping").data("kendoGrid").dataSource.sort({ field: "ExpiryCategoryCode", dir: "asc" });
                    }
                }
            }, {
                model: model
            }, true);
        }
    });

    function populateSectorMasterDropdown() {

        //var sectorFilterDataSource = sectorMasterdataSource.filter(function (item) {
        //    if (mogWiseAPLArray.indexOf(parseInt(item.ArticleID)) == -1) {
        //        return item;
        //    }
        //});

        $("#inputSector").kendoMultiSelect({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "text",
            dataSource: sectorMasterdataSource,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
        });
        var multiselect = $("#inputSector").data("kendoMultiSelect");

        //clear filter
        //multiselect.dataSource.filter({});

        //set value
        // multiselect.value(mogWiseAPLArray);

    }

    $("#showSelection").bind("click", function () {
        var checked = [];
        for (var i in checkedIds) {
            if (checkedIds[i]) {
                checked.push(i);
            }
        }
    });

});

$("document").ready(function () {
   // showMOGAPLDependencies()
    $(".k-window").hide();
    $(".k-overlay").hide();
    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();

    });
});
