﻿//import { Debugger } from "../../Content/css/bootstrap-colorpicker/js/bootstrap-colorpicker";

var user;
var status = "";
var varname = "";
var datamodel;
var RecipeName = "";
var RecipeID = "";
var RecipeCode = "";
var uomdata = [];
var moguomdata = [];
var uommodulemappingdata = [];
var recipecategoryuomdata = [];
var basedata = [];
var basedataList = [];
var dishdata = [];
var baserecipedata = [];
var baserecipedatafiltered = [];
var mogdata = [];
var mogdataList = [];
var mogWiseAPLMasterdataSource = [];
var MOGCode = "MOG-00156";
var MOGName = "Test"
var IsMajor = 0;
var isBaseRecipe = 0;
var MajorPercentData = 5;
var x = 'x';
var isBRConfigurationChange = true;
var isBRNameChange = false;
var isPublishBtnClick = false;
var isStatusSave = false;
var applicationSettingData = [];
var resetID, gridIdin, vresetId;
var copyData = [];
var copyData0 = [];
var mogtotal = 0;
var BaseRecipetotal = 0;
var ToTIngredients = 0;
var RecipeWisenutrientMasterdataSource = [];
var checkedNutrientCodes = [];
var nutrientFilterDataSource = [];
var nutrientMasterdataSource = [];
var RecipeWiseNutrientArray = [];
var isShowElmentoryRecipe = false;
var isMainRecipe = false;
var allergenmasterdata = [];
var currentGridId;
var modelHistoryData = [];
var modelHistoryData0 = [];
var isRegionAPL = false;;
var regionAPLSiteCode = "";
//$(function () {
//    kendo.data.DataSource.prototype.dataFiltered = function () {
//        // Gets the filter from the dataSource
//        var filters = this.filter();

//        // Gets the full set of data from the data source
//        var allData = this.data();

//        // Applies the filter to the data
//        var query = new kendo.data.Query(allData);

//        // Returns the filtered data
//        return query.filter(filters).data;
//    }



//});

function saveReceipeOnPublishBtn() {
    //Model Creation
    //insertion or updation in  two tables
    //Base recipie
    Utility.Loading();
    if (!validateDataBeforeSave("gridBaseRecipe", 'gridMOG')) {
        toastr.error("Invalid Data!");
        Utility.UnLoading();
        return false;
    }

    var rName = $("#inputrecipename").val();
    var rAlias = $("#inputrecipealiasname").val();
    if (rName === rAlias) {
        Utility.UnLoading();
        toastr.error("Recipe Name and Recipe Alias should be different");
        return;
    }

    if (user.SectorNumber == "20" || user.SectorNumber == "10") {

        if ($("#inputrecipecum").val() == null || $("#inputrecipecum").val() == "" || $("#inputrecipecum").val() == undefined || $("#inputrecipecum").val() == "Select") {
            toastr.error("Please select Recipe UOM Category");
            Utility.UnLoading();
            return false;
        }
    }

    if ($("#inputuom").val() == null || $("#inputuom").val() == "" || $("#inputuom").val() == undefined) {
        toastr.error("Please select UOM");
        Utility.UnLoading();
        return false;
    }

    if ($("#inputquantity").val() == null || $("#inputquantity").val() == "" || $("#inputquantity").val() == undefined || $("#inputquantity").val() == 0 || $("#inputquantity").val() == "Select") {
        toastr.error("Please enter Recipe Quantity");
        Utility.UnLoading();
        return false;
    }


    var brgrid = $("#gridBaseRecipe").data("kendoGrid").dataSource;
    var data = brgrid._data;
    var baseRecipes = [];

    for (var i = 0; i < brgrid._data.length; i++) {
        var iMajor = false;
        if (data[i].IngredientPerc >= MajorPercentData) {
            iMajor = true;
        }
        var obj = {
            "RecipeCode": $("#inputrecipecode").val(),
            "Recipe_ID": $("#recipeid").val(),
            "BaseRecipe_ID": data[i].BaseRecipe_ID,
            "BaseRecipeCode": data[i].BaseRecipeCode,
            "UOMCode": data[i].UOMCode,
            "UOM_ID": data[i].UOM_ID,
            "CostPerUOM": data[i].CostPerUOM,
            "CostPerKG": data[i].CostPerKG,
            "TotalCost": data[i].TotalCost,
            "Quantity": data[i].Quantity,
            "IngredientPerc": data[i].IngredientPerc,
            "IsMajor": iMajor,
            "IsActive": 1
        }
        baseRecipes.push(obj);
    }
    //MOG
    var mogGrid = $("#gridMOG").data("kendoGrid").dataSource;
    data = mogGrid._data;
    var mogs = [];
    for (var i = 0; i < mogGrid._data.length; i++) {
        var iMajor = false;
        if (data[i].IngredientPerc >= 5) {
            iMajor = true;
        }
        var obj = {
            "RecipeCode": $("#inputrecipecode").val(),
            "Recipe_ID": $("#recipeid").val(),
            "MOG_ID": data[i].MOG_ID,
            "MOGCode": data[i].MOGCode,
            "UOMCode": data[i].UOMCode,
            "UOM_ID": data[i].UOM_ID,
            "CostPerUOM": data[i].CostPerUOM,
            "CostPerKG": data[i].CostPerKG,
            "TotalCost": data[i].TotalCost,
            "Quantity": data[i].Quantity,
            "IngredientPerc": data[i].IngredientPerc,
            "IsMajor": iMajor,
            "IsActive": 1
        }
        mogs.push(obj);
    }

    //Top Level Actual Recipie
    var base;
    if ($("#inputbase").val() == "Yes")
        base = true
    else if ($("#inputbase").val() == "No")
        base = false
    var objUOM = uomdata.filter(function (uomObj) {
        return uomObj.value == $("#inputuom").val();
    });

    var model = {
        "RecipeCode": $("#inputrecipecode").val(),
        "ID": $("#recipeid").val(),
        "Name": $("#inputrecipename").val(),
        "RecipeAlias": $("#inputrecipealiasname").val(),
        "OldRecipeName": RecipeName,
        "UOM_ID": $("#inputuom").val(),
        "UOMCode": objUOM[0].UOMCode,
        "Quantity": $("#inputquantity").val(),
        "IsBase": base,
        "CostPerKG": $("#grandCostPerKG").text().substring(1).replace(",", ""),
        "CostPerUOM": $("#grandCostPerKG").text().substring(1).replace(",", ""),
        "TotalCost": $('#grandTotal').text().substring(1).replace(",", ""),
        "Quantity": $('#inputquantity').val(),
        "Instructions": $("#inputinstruction").val(),
        "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum").val() : "",
        "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit").val() : "",
        "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom").val() : "",
        "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit").val() : "",
        "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom").val() : "",
        "TotalIngredientWeight": $("#ToTIngredients").text(),
        "IsActive": 1,
        "IsRecipeNameChange": isBRNameChange,
        "IsRecipeStatusChange": isStatusSave,
        "Status": isPublishBtnClick ? 3 : 2

    }

    if ($("#inputrecipename").val() === "") {
        $("#error").css("display", "flex");
        $("#error").find("p").text("Please provide valid input(s)");
        $("#inputrecipename").focus();
        Utility.UnLoading();
    }
    else {
        $("#error").css("display", "none");
        var base;
        if ($("#inputbase").val() == "Yes")
            base = true
        else if ($("#inputbase").val() == "No")
            base = false
        console.log("model");
        console.log(model);
        $("#btnPublish").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.SaveRecipeData,
            function (result) {
                if (result == false) {
                    $('#btnPublish').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputrecipename").focus();
                    Utility.UnLoading();
                }
                else {
                    $(".k-overlay").hide();
                    $('#btnPublish').removeAttr("disabled");
                    toastr.success("Recipe published successfully");
                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();
                    datamodel = model;
                    if (recipeImpactedData.IsMOGImpacted) {
                        sendRecipeEmail();
                    }
                    $("#mainContent").show();

                    $("#windowEdit").hide();
                }
            }, {
            model: model,
            baseRecipes: baseRecipes,
            mogs: mogs

        }, true);
    }
    console.log(model);
}

function sendRecipeEmail() {
    Utility.Loading();
    HttpClient.MakeRequest(CookBookMasters.SendRecipeEmail, function (result) {
        if (result == false) {
            // toastr.error("Some error occured, please try again");
        }
        else {
        }
    }, {
        model: datamodel
    }, true);
}

function SaveRecipeElmentoryStatus() {
    datamodel.IsRecipeStatusChange = true;
    datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
    HttpClient.MakeSyncRequest(CookBookMasters.SaveRecipeData, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again");
        }
        else {
            toastr.success("Recipe status updated successfully");
            $("#gridRecipe").data("kendoGrid").dataSource.data([]);
            $("#gridRecipe").data("kendoGrid").dataSource.read();
            if (recipeImpactedData.IsMOGImpacted) {
                //   sendRecipeEmail();
            }
        }
    }, {
        model: datamodel
    }, true);
}

function SaveRecipeStatus() {
    datamodel.IsRecipeStatusChange = true;
    datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
    HttpClient.MakeSyncRequest(CookBookMasters.SaveRecipeData, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again");
        }
        else {
            toastr.success("Recipe status updated successfully");
            $("#gridRecipe").data("kendoGrid").dataSource.data([]);
            $("#gridRecipe").data("kendoGrid").dataSource.read();
            if (recipeImpactedData.IsMOGImpacted) {
                sendRecipeEmail();
            }
        }
    }, {
        model: datamodel
    }, true);
}

function showRecipeImpactedMessage() {
    if (isStatusSave) {
        $("#recipeNameChangeMsgDiv").css('display', 'none');
        $("#recipeConfigChangeMsgDiv").css('display', 'none');
        $("#recipeInactiveMsgDiv").css('display', 'block');
    }
    else if (isBRNameChange) {
        $("#recipeNameChangeMsgDiv").css('display', 'block');
        $("#recipeConfigChangeMsgDiv").css('display', 'none');
        $("#recipeInactiveMsgDiv").css('display', 'none');
    }
    else {
        $("#recipeNameChangeMsgDiv").css('display', 'none');
        $("#recipeInactiveMsgDiv").css('display', 'none');
        $("#recipeConfigChangeMsgDiv").css('display', 'block');
    }
}

function showRecipeDependenciesPopUp() {
    var dialog = $("#windowEditRecipeDependencies").data("kendoWindow");

    dialog.title("Base Recipe - " + RecipeName + " Dependencies");
    dialog.open();
    dialog.center();
    populateRecipeDependencies();
}

$("#inputrecipealiasname").focusout(function () {
    var rName = $("#inputrecipename").val();
    var rAlias = $("#inputrecipealiasname").val();
    if (rName === rAlias) {
        Utility.UnLoading();
        toastr.error("Recipe Name and Recipe Alias should be different");
        return;
    }
});

function saveRecipeOnBtnSubmit() {
    //Model Creation
    //insertion or updation in  two tables
    //Base recipie
    if (!validateDataBeforeSave("gridBaseRecipe", 'gridMOG')) {
        toastr.error("Invalid Data!");
        Utility.UnLoading();
        return false;
    }

    var rName = $("#inputrecipename").val();
    var rAlias = $("#inputrecipealiasname").val();
    if (rName === rAlias) {
        Utility.UnLoading();
        toastr.error("Recipe Name and Recipe Alias should be different");
        return;
    }
    if ($("#inputquantity").val() == null || $("#inputquantity").val() == "" || $("#inputquantity").val() == undefined || $("#inputquantity").val() == 0) {
        toastr.error("Please enter Recipe Quantity");
        Utility.UnLoading();
        return false;
    }

    if (user.SectorNumber == "20" || user.SectorNumber == "10") {

        if ($("#inputrecipecum").val() == null || $("#inputrecipecum").val() == "" || $("#inputrecipecum").val() == undefined || $("#inputrecipecum").val() == "Select") {
            toastr.error("Please select Recipe UOM Category");
            Utility.UnLoading();
            return false;
        }
    }

    if ($("#inputuom").val() == null || $("#inputuom").val() == "" || $("#inputuom").val() == undefined || $("#inputuom").val() == "Select") {
        toastr.error("Please select UOM");
        Utility.UnLoading();
        return false;
    }

    var brgrid = $("#gridBaseRecipe").data("kendoGrid").dataSource;
    var data = brgrid._data;
    var baseRecipes = [];

    for (var i = 0; i < brgrid._data.length; i++) {
        var iMajor = false;
        if (data[i].IngredientPerc >= MajorPercentData) {
            iMajor = true;
        }
        var obj = {
            "RecipeCode": $("#inputrecipecode").val(),
            "Recipe_ID": $("#recipeid").val(),
            "BaseRecipe_ID": data[i].BaseRecipe_ID,
            "BaseRecipeCode": data[i].BaseRecipeCode,
            "UOMCode": data[i].UOMCode,
            "UOM_ID": data[i].UOM_ID,
            "CostPerUOM": data[i].CostPerUOM,
            "CostPerKG": data[i].CostPerKG,
            "TotalCost": data[i].TotalCost,
            "Quantity": data[i].Quantity,
            "IngredientPerc": data[i].IngredientPerc,
            "IsMajor": iMajor,
            "IsActive": 1,
            "CreatedOn": data[i].BaseRecipe_ID == 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].CreatedOn),
            "CreatedBy": data[i].BaseRecipe_ID == 0 ? user.UserId : data[i].CreatedBy,
            "ModifiedOn": data[i].BaseRecipe_ID > 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].ModifiedOn),
            "ModifiedBy": data[i].BaseRecipe_ID > 0 ? user.UserId : data[i].ModifiedBy,

        }
        baseRecipes.push(obj);
    }
    //MOG
    var mogGrid = $("#gridMOG").data("kendoGrid").dataSource;
    data = mogGrid._data;
    var mogs = [];
    for (var i = 0; i < mogGrid._data.length; i++) {
        var iMajor = false;
        if (data[i].IngredientPerc >= MajorPercentData) {
            iMajor = true;
        }
        var obj = {
            "RecipeCode": $("#inputrecipecode").val(),
            "Recipe_ID": $("#recipeid").val(),
            "MOG_ID": data[i].MOG_ID,
            "MOGCode": data[i].MOGCode,
            "UOMCode": data[i].UOMCode,
            "UOM_ID": data[i].UOM_ID,
            "CostPerUOM": data[i].CostPerUOM,
            "CostPerKG": data[i].CostPerKG,
            "TotalCost": data[i].TotalCost,
            "Quantity": data[i].Quantity,
            "IngredientPerc": data[i].IngredientPerc,
            "IsMajor": iMajor,
            "IsActive": 1,
            "CreatedOn": data[i].MOG_ID == 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].CreatedOn),
            "CreatedBy": data[i].MOG_ID == 0 ? user.UserId : data[i].CreatedBy,
            "ModifiedOn": data[i].MOG_ID > 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].ModifiedOn),
            "ModifiedBy": data[i].MOG_ID > 0 ? user.UserId : data[i].ModifiedBy
        }
        mogs.push(obj);
    }

    //Top Level Actual Recipie
    var base;
    if ($("#inputbase").val() == "Yes")
        base = true
    else if ($("#inputbase").val() == "No")
        base = false
    var objUOM = uomdata.filter(function (uomObj) {
        return uomObj.value == $("#inputuom").val();
    });

    var model = {
        "RecipeCode": $("#inputrecipecode").val(),
        "ID": $("#copyRecipe0").val(),
        "Name": $("#inputrecipename").val(),
        "RecipeAlias": $("#inputrecipealiasname").val(),
        "OldRecipeName": RecipeName,
        "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum").val() : "",
        "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit").val() : "",
        "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom").val() : "",
        "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit").val() : "",
        "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom").val() : "",
        "TotalIngredientWeight": $("#ToTIngredients").text(),
        "UOM_ID": $("#inputuom").val(),
        "UOMCode": objUOM[0].UOMCode,
        "Quantity": $("#inputquantity").val(),
        "IsBase": base,
        "CostPerKG": $("#grandCostPerKG").text().substring(1).replace(",", ""),
        "CostPerUOM": $("#grandCostPerKG").text().substring(1).replace(",", ""),
        "TotalCost": $('#grandTotal').text().substring(1).replace(",", ""),
        "Quantity": $('#inputquantity').val(),
        "Instructions": $("#inputinstruction").val(),
        "IsActive": 1,
        "IsRecipeNameChange": isBRNameChange,
        "IsRecipeStatusChange": isStatusSave,
        "Status": isPublishBtnClick ? 3 : 2,
        //"CreatedOn": kendo.parseDate(datamodel.CreatedOn),
        //"CreatedBy": datamodel.CreatedBy
        "ModifiedOn": (datamodel != undefined && datamodel != null && datamodel.CreatedOn != undefined) ? kendo.parseDate(datamodel.CreatedOn) : Utility.CurrentDate(),
        "ModifiedBy": (datamodel != undefined && datamodel != null && datamodel.CreatedBy != undefined) ? datamodel.CreatedBy : user.UserId,
        "Site_ID": $("#NewSimSiteByRegion").val(),
        "Region_ID": $("#NewSimRegion").val(),
        "SiteCode": $("#hdnSiteCode").val(),
        "DishID": $("#hdnDishId").val(),
        "DishCode": $("#hdnDishCode").val(),
        "History": modelHistoryData
    }

    if ($("#inputrecipename").val() === "") {
        toastr.error("Please provide Recipe Name");
        $("#inputrecipename").focus();
        return;
    }
    else {
        var base;
        if ($("#inputbase").val() == "Yes")
            base = true
        else if ($("#inputbase").val() == "No")
            base = false


        $("#btnSubmitRecipe").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.SaveRecipeData, function (result) {
            //alert("Result" + result);
            if (result == false) {
                $('#btnSubmitRecipe').removeAttr("disabled");
                toastr.error("Some error occured, please try again");
                $("#inputrecipename").focus();
            }
            else if (result == "Duplicate") {
                $('#btnSubmitRecipe').removeAttr("disabled");
                toastr.error("Recipe Name already exits , kindly check");
                $("#inputrecipename").focus();
            }
            else {
                $(".k-overlay").hide();
                $('#btnSubmitRecipe').removeAttr("disabled");


                Toast.fire({
                    type: 'success',
                    title: 'Recipe data saved'
                });
                //Utility.UnLoading();
                //$("#gridRecipe").data("kendoGrid").dataSource.data([]);
                //$("#gridRecipe").data("kendoGrid").dataSource.read();
                datamodel = model;
                //if (recipeImpactedData.IsMOGImpacted) {
                //    sendRecipeEmail();
                //}
                $("#divSimConfigMain").show();
                $("#divBoard").show();
                $("#windowEdit").hide();

                //inputTxtGrm.id = "txtGrmroot_" + itemidvalue + "_" + selecteditem[it].dpcode + "_" + itemDATA.DishCategories[dsc].Dishes[dis].CostPerKG + "_" + i + "_" + itemDATA.DishCategories[dsc].Dishes[dis].DishID;
                //txtGrmroot_79_DPC-00001_20.34_1_119
                //"txtGrmroot_" + i + "_" + itemidvalue + "_" + selecteditem[it].dpcode + "_" + itemDATA.DishCategories[dsc].Dishes[dis].DishID
                //txtTotalGrm1 txtGrmroot_1_79_DPC-00001_119
                //"txtPax_" + i + "_" + itemidvalue + "_" + selecteditem[it].dpcode + "_" + itemDATA.DishCategories[dsc].Dishes[dis].DishID;
                var noOfDays = $("#NewSimNoOfDays").val();

                setTimeout(function () {
                    for (var i = 1; i <= noOfDays; i++) {
                        var setcostid = "txtGrmroot_" + $("#hdnItemId").val() + "_" + $("#hdnDayPartCode").val() + "_" + $("#grandCostPerKG").text().substring(1).replace(",", "") + "_" + i + "_" + $("#hdnDishId").val();
                        var getbyClass = ".txtGrmroot_" + i + "_" + $("#hdnItemId").val() + "_" + $("#hdnDayPartCode").val() + "_" + $("#hdnDishId").val();
                        $(getbyClass).attr("id", setcostid);
                        $(getbyClass).trigger("input");
                        var getTxtById = "txtPax_" + i + "_" + $("#hdnItemId").val() + "_" + $("#hdnDayPartCode").val() + "_" + $("#hdnDishId").val();
                        var inputTxt = document.getElementById(getTxtById);
                        inputTxt.setAttribute("data-cost", $("#grandCostPerKG").text().substring(1).replace(",", ""));
                        if (inputTxt.value.length > 0) {
                            onInputGrm(setcostid, $("#hdnItemId").val(), $("#hdnDayPartCode").val(), $("#grandCostPerKG").text().substring(1).replace(",", ""), i, $("#hdnDishId").val());
                        }
                    }
                    Utility.UnLoading();
                }, 0);
                

                ////Edit Grid button fire
                //HttpClient.MakeRequest(CookBookMasters.GetSimulationBoardData, function (result) {
                //    console.log("result");
                //    console.log(result);

                //    $("#SimulationID").val(result.ID);
                //    $("#SimulationCode").val(result.SimCode);
                //    $("#hdnStatus").val(result.Status);

                //    $("#NewSimNoOfDays").val(result.NoOfDays);
                //    $("#txtSimulationName").val(result.SimulationName);
                //    var version = "";
                //    if (result.Version != null && result.Version != "null")
                //        version = result.Version;

                //    $("#divSimConfig input").attr("disabled", "disabled");
                //    $("#divSimConfig").addClass("divDisabled");

                //    //dropdownlist.enable(true);
                //    viewOnly = 0;

                //    $("#spanHeadertxt").html("Update Simulation " + result.SimulationName + " " + version);

                //    //get sitedish data if not generated
                //    //if(sitedish)

                //    populateExistingBoard(result);


                //    var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
                //    dropdownlist.enable(false);
                //    dropdownlist = $("#selectFromSite").data("kendoDropDownList");
                //    dropdownlist.enable(false);

                //    //Utility.UnLoading();
                //}, { simId: $("#SimulationID").val() }, false);
            }
        }, {
            rmodel: null,
            model: model,
            baseRecipes: baseRecipes,
            mogs: mogs,
            simCode: $("#SimulationCode").val()

        }, true);
    }
}
function populateRecipeCopy0DropDown() {
    //allergenmasterdata
    HttpClient.MakeSyncRequest(CookBookMasters.GetAllergenDataList, function (result) {
        Utility.UnLoading();

        if (result != null) {
            allergenmasterdata = result;;

        }
        else {

        }
    }, null, false);

    $("#copyRecipe0").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "ID",
        //dataValueField: "Recipe_ID",
        dataSource: copyData0,
        index: 0,
    });
    var dropdownlist = $("#copyRecipe0").data("kendoDropDownList");
    dropdownlist.bind("change", dropdownlist_selectMain0);
    if (copyData0.length > 1) {
        console.log("copyData0");
        console.log(copyData0)
        $("#btnSubmitRecipe").show();        
        var sel = copyData0.filter(a => a.IsDefault == true)[0];
        console.log("sel")
        console.log(sel)
        if (sel != undefined) {
            console.log(sel)
            if (viewOnly == 1) {
                $(".rcHide").show();
                $("#inputuom").removeAttr('disabled');
                $("#inputrecipecum").removeAttr('disabled');
                var dropdownlist = $("#copyRecipe0").data("kendoDropDownList");
                if (dropdownlist != undefined)
                    dropdownlist.enable(true);
            }

            dropdownlist.value(sel.ID);
            populateRecipeDetails(sel);

            
        }
        else {
            //console.log(copyData0)
            //Changed due to issue raised on 25th March in excel file line #51
            dropdownlist.value(copyData0[0].ID);
            $("#gridMOG").hide();
            $("#emptymog").show();
            //TODO uncomment commented line 6th April
            //populateRecipeDetails(copyData0[0]);  
            populateRecipeDetails2();

            if (viewOnly == 1) {
                $(".rcHide").hide();
                $("#inputuom").attr('disabled', 'disabled');
                $("#inputrecipecum").attr('disabled', 'disabled');
                var dropdownlist = $("#copyRecipe0").data("kendoDropDownList");
                if (dropdownlist != undefined)
                    dropdownlist.enable(false);
            }
            else
                toastr.warning("No recipe connected");
            Utility.UnLoading();
        }
        //dropdownlist.trigger("change");

    }
    else {
        $("#btnSubmitRecipe").hide();
        Utility.UnLoading();
    }
}
function populateRecipeDetails(tr) {
    //var gridObj = $("#gridRecipe").data("kendoGrid");
    //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
    //if (tr.Status == 1)
    //    return;
    Utility.Loading();
    setTimeout(function () {
        console.log("tr")
        console.log(tr)
        datamodel = tr;
        RecipeName = tr.Name;
        populateUOMDropdown();
        recipeCategoryHTMLPopulateOnEditRecipe("", tr);
        $("#mainContent").hide();
        $("#windowEdit").show();


        populateBaseDropdown();
        Recipe_ID = tr.Recipe_ID;
        populateMOGGrid(tr.Recipe_ID, tr.RecipeCode);
        //resetID = tr.Recipe_ID;
        populateBaseRecipeGrid(tr.Recipe_ID, tr.RecipeCode);

        $("#recipeAllergens").html(showAllergens(tr.AllergensName));

        $("#recipeid").val(tr.ID);
        $("#inputrecipecode").val(tr.RecipeCode);
        $("#inputrecipename").val(tr.Name);
        $("#inputrecipealiasname").val(tr.RecipeAlias);
        $("#inputuom").data('kendoDropDownList').value(tr.UOM_ID);
        $("#inputquantity").val(tr.Quantity);
        if (tr.IsBase == true)
            $("#inputbase").data('kendoDropDownList').value("Yes");
        else
            $("#inputbase").data('kendoDropDownList').value("No");

        $('#grandTotal').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
        $('#grandCostPerKG').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
        $('#regiongrandTotal').html(Utility.AddCurrencySymbol(tr.RegionTotalCost, 2));
        oldcost = tr.TotalCost;// tr.RegionTotalCost;
        var editor = $("#inputinstruction").data("kendoEditor");
        editor.value(decodeHTMLEntities(tr.Instructions));
        var change = 0; //(tr.TotalCost - oldcost).toFixed(2);
        var changeperc = 0; //(((change * 100) / oldcost)).toFixed(2);
        if (user.SectorNumber == "20") {
            $('#inputrecipename').removeAttr("disabled");
            $('#inputquantity').removeAttr("disabled");
            $('#inputrecipealiasname').removeAttr("disabled");
            $("#inputbase").data("kendoDropDownList").enable(true);
            $(".changegrandtotal").css("display", "none");
            $("#percentageChangeDiv").css("display", "none");


        }
        else {
            $('#inputquantity').attr('disabled', 'disabled');
            $('#inputrecipename').attr('disabled', 'disabled');
            $('#inputrecipealiasname').attr('disabled', 'disabled');
            $("#inputbase").data("kendoDropDownList").enable(false);
            $(".changegrandTotal").css("display", "block");
            $("#percentageChangeDiv").css("display", "block");
            $("#changegrandTotal").html(Utility.AddCurrencySymbol(change, 2));

            $("#totalchangepercent").html(Utility.AddCurrencySymbol(changeperc, 2));
            if (isNaN(oldcost)) {
                $("#changegrandTotal").hide();
                $("#totalchangepercent").hide();
            } else {
                $("#changegrandTotal").show();
                $("#totalchangepercent").show();
            }
            if (change == 0) {
                $("#changegrandTotal").html("No Change");
            }

        }

        if (viewOnly == 1) {
            $('#inputquantity').attr('disabled', 'disabled');
            
            $('#inputrecipealiasname').attr('disabled', 'disabled');
        }

        var dropdownlist = $("#inputsite").data("kendoDropDownList");
        $("#topHeading").text("Food Cost Simulator");//.append("(" + siteName + ")");
        hideGrid('gridBaseRecipe', 'emptyBr', 'baseRecipePlus', 'baseRecipeMinus');
        hideGrid('gridMOG', 'emptymog', 'mogPlus', 'mogMinus');

        //8th April
        CalculateGrandTotal("");
        Utility.UnLoading();
    }, 100);

}

function populateRecipeDetails2() {
    //var gridObj = $("#gridRecipe").data("kendoGrid");
    //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
    //if (tr.Status == 1)
    //    return;
    Utility.Loading();
    setTimeout(function () {
        
        populateUOMDropdown();
        recipeCategoryHTMLPopulateOnEditRecipe("", []);
        $("#mainContent").hide();
        $("#windowEdit").show();
        var dropdownlist = $("#inputrecipecum").data("kendoDropDownList");        
        dropdownlist.select(0);
        populateBaseDropdown();
        //Recipe_ID = tr.Recipe_ID;
        populateMOGGrid("", "");
        //$("#gridMOG").css("display", "none");
        //$("#emptymog").css("display", "block");
        
        populateBaseRecipeGrid(0,"");

        $("#recipeAllergens").html(showAllergens(""));

        //$("#recipeid").val(tr.ID);
        //$("#inputrecipecode").val(tr.RecipeCode);
        //$("#inputrecipename").val(tr.Name);
        //$("#inputrecipealiasname").val(tr.RecipeAlias);
        //$("#inputuom").data('kendoDropDownList').value(tr.UOM_ID);
       // $("#inputquantity").val(tr.Quantity);
        //if (tr.IsBase == true)
        //    $("#inputbase").data('kendoDropDownList').value("Yes");
        //else
        //    $("#inputbase").data('kendoDropDownList').value("No");

        //$('#grandTotal').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
        //$('#grandCostPerKG').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
        //$('#regiongrandTotal').html(Utility.AddCurrencySymbol(tr.RegionTotalCost, 2));
       // oldcost = tr.TotalCost;// tr.RegionTotalCost;
        var editor = $("#inputinstruction").data("kendoEditor");
        //editor.value(decodeHTMLEntities(tr.Instructions));
        var change = 0; //(tr.TotalCost - oldcost).toFixed(2);
        var changeperc = 0; //(((change * 100) / oldcost)).toFixed(2);
        if (user.SectorNumber == "20") {
            $('#inputrecipename').removeAttr("disabled");
            $('#inputquantity').removeAttr("disabled");
            $('#inputrecipealiasname').removeAttr("disabled");
            $("#inputbase").data("kendoDropDownList").enable(true);
            $(".changegrandtotal").css("display", "none");
            $("#percentageChangeDiv").css("display", "none");


        }
        else {
            $('#inputquantity').attr('disabled', 'disabled');
            $('#inputrecipename').attr('disabled', 'disabled');
            $('#inputrecipealiasname').attr('disabled', 'disabled');
            $("#inputbase").data("kendoDropDownList").enable(false);
            $(".changegrandTotal").css("display", "block");
            $("#percentageChangeDiv").css("display", "block");
            $("#changegrandTotal").html(Utility.AddCurrencySymbol(change, 2));

            $("#totalchangepercent").html(Utility.AddCurrencySymbol(changeperc, 2));
            if (isNaN(oldcost)) {
                $("#changegrandTotal").hide();
                $("#totalchangepercent").hide();
            } else {
                $("#changegrandTotal").show();
                $("#totalchangepercent").show();
            }
            if (change == 0) {
                $("#changegrandTotal").html("No Change");
            }

        }

        if (viewOnly == 1) {
            $('#inputquantity').attr('disabled', 'disabled');

            $('#inputrecipealiasname').attr('disabled', 'disabled');
        }

        var dropdownlist = $("#inputsite").data("kendoDropDownList");
        $("#topHeading").text("Food Cost Simulator");//.append("(" + siteName + ")");
        hideGrid('gridBaseRecipe', 'emptyBr', 'baseRecipePlus', 'baseRecipeMinus');
        //hideGrid('gridMOG', 'emptymog', 'mogPlus', 'mogMinus');
        //hideGrid('gridMOG', 'mogPlus', 'mogMinus');

        Utility.UnLoading();
    }, 100);

}

function showAllergens(allergens) {
    var allergenHtml = '<label class="form-control-label">Allergens</label>';
    if (allergens != undefined && allergens != "") {
        var allergenArray = allergens.split(',');
        if (allergenArray != null) {
            allergenArray.forEach(function (item, index) {
                if (item != null && item != "" && item != ",") {
                    var allergenCode = allergenmasterdata.filter(m => m.Name == item);
                    if (allergenCode != null && allergenCode != "") {
                        allergenHtml += '<div class="img-with-text" style="float: left;height: 30px;"><img alt="Tree Nuts" style="display: block;margin: 0 auto;height: 15px;width: 15px;border-radius: 10px;" src="./AllergenImages/' + allergenCode[0].AllergenCode + '.jpg">';
                        allergenHtml += '<p style="font-size: 11px;">' + item + '</p></div></div>';
                    }
                }
            });
        }
    }
    else {
        allergenHtml += '<label class="form-control-label" style="color: #676767!important;font - size: 14px!important;">None</label>';
    }
    return allergenHtml;
}

function dropdownlist_selectMain0(eid) {
    var filteredcopyData0 = copyData0.filter(ds => ds.ID == $("#copyRecipe0").val());
    console.log("copyData0")
    console.log(copyData0)
    populateRecipeDetails(filteredcopyData0[0]);
    //console.log(eid.sender.dataItem(eid.item));
    //var ID = eid.sender.dataItem(eid.item).ID;
    ////var ItemCode = eid.sender.dataItem(eid.item).ItemCode;
    //if (ID == 0) {
    //    return;
    //}
    ////  alert(ID)
    //populateBaseRecipeGrid(ID);
    //populateMOGGrid(ID);
}
function populateRecipeCopyDropDown() {
    console.log("copyData");
    console.log(copyData);
    $("#copyRecipe").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "ID",
        dataSource: copyData,
        index: 0,
    });
    var dropdownlist = $("#copyRecipe").data("kendoDropDownList");
    dropdownlist.bind("change", dropdownlist_selectMain);
}
function dropdownlist_selectMain(eid) {
    console.log(eid.sender.dataItem(eid.item));
    var ID = eid.sender.dataItem(eid.item).Recipe_ID;
    var code = eid.sender.dataItem(eid.item).RecipeCode;
    //var ItemCode = eid.sender.dataItem(eid.item).ItemCode;
    if (ID == 0) {
        return;
    }
    //  alert(ID)
    Utility.Loading();
    setTimeout(function () {
        populateBaseRecipeGridCopy(ID);
        populateMOGGridCopy(ID, code);
        vresetId = ID;
        var editor = $("#inputinstruction0").data("kendoEditor");
        editor.value(eid.sender.dataItem(eid.item).Instructions);
    }, 50);
}
function populateRecipeGrid() {

    Utility.Loading();
    var gridVariable = $("#gridRecipe");//Hare Krishna!!
    gridVariable.html("");
    if (user.SectorNumber == "80") {
        gridVariable.kendoGrid({
            excel: {
                fileName: "Recipe.xlsx",
                filterable: true,
                allPages: true
            },
            //selectable: "cell",
            sortable: true,
            noRecords: {
                template: "No Records Available"
            },
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: true,
            //reorderable: true,
            scrollable: true,
            columns: [
                {
                    field: "RecipeCode", title: "Code", width: "25px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Recipe Name", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "RecipeAlias", title: "Recipe Alias", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "20px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "Quantity", title: "Qty", width: "20px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "RecipeType", title: "Type", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    template: '# if (IsBase == true) {#<div>Base Recipe</div>#} else {#<div>Final Recipe</div>#}#',
                },
                {
                    field: "Impact", title: "Impacts", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    template: '# if (IsBase == true) { #<div>#:Impact# Recipe(s)</div>#} else {#<div>#:Impact# Dish</div>#}#',
                },
                //{
                //    field: "IsElementory",
                //    title: "Elementory", width: "25px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" id="chkbox-#:ID#" class="chkboxElementory" #= IsElementory ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "IsActive",
                    title: "Active", width: "25px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    template: '<label class= "switch"><input type="checkbox" id="chkbox-#:ID#" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                },
                {
                    field: "PublishedDateAsString", title: "Last Updated", width: "35px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    // template: '# if (PublishedDate == null) {#<span>-</span>#} else{#<span>#: kendo.toString(PublishedDate, "dd-MMM-yy")#</span>#}#',
                    headerAttributes: {
                        style: "text-align: center;"
                    }

                },
                {
                    field: "Status", title: "Status", width: "30px", attributes: {
                        class: "mycustomstatus",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Published#}#',
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },
                {
                    field: "Edit", title: "Configure", width: "30px",
                    attributes: {
                        style: "text-align: left; font-weight:normal;cursor: pointer;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            template: function (dataItem) {
                                var html = `  <div title="Configure" onClick="recipeGridEdit(this)"  style='color:#8f9090; margin-left:20px' class='fa fa-cog EditRecipe'></div>`;
                                return html;
                            },

                        },

                        //{
                        //    name: 'Map Nutrients',
                        //    template: function (dataItem) {
                        //        var html = `  <div title="Map Nutrients" onClick="mapNutrients(this)"  style='color:#8f9090; margin-left:20px' class='fa fa-nutrition MapNutrients'></div>`;
                        //        return html;
                        //    }
                        //}
                    ],
                }
            ],
            dataSource: {
                sort: { field: "IsBase", dir: "asc" },
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        //copyRecipe
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteRecipeDataList, function (result) {
                            Utility.UnLoading();
                            if (result != null) {
                                copyData = result;
                                if (isShowElmentoryRecipe) {
                                    result = result.filter(m => m.IsElementory);
                                }
                                options.success(result);

                                copyData.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
                                populateRecipeCopyDropDown();

                            }
                            else {
                                options.success("");
                            }
                        },
                            {
                                condition: "All",
                                SiteCode: $("#hdnSiteCode").val()
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            RecipeCode: { type: "string", editable: false },
                            Name: { type: "string", editable: false },
                            RecipeAlias: { type: "string", editable: false },
                            UOMName: { type: "string", editable: false },
                            IsBase: { type: "boolean", editable: false },
                            Quantity: { type: "int", editable: false },
                            IsActive: { type: "boolean", editable: false },

                            Status: { type: "string", editable: false },
                            RecipeType: { type: "string", editable: false },
                            PublishedDate: { type: "date" },
                            PublishedDateAsString: { type: "string", editable: false },
                            IsElementoryString: { type: "string", editable: false },
                            IsElementory: { type: "boolean", editable: false },

                        }
                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            // height: 500,
            dataBound: function (e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }
                    else {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    }

                }

                var items = e.sender.items();

                var items = e.sender.items();
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsElementory) {
                        $(this).find(".MapNutrients").addClass("k-state-disabled");
                    }
                });

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".EditRecipe").addClass("k-state-disabled");
                    }
                });



                //items.each(function (e) {

                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //    }
                //});


                $(".chkbox").on("change", function (e) {
                    // 
                    isStatusSave = true;
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridRecipe").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    RecipeName = tr.Name;
                    RecipeCode = tr.RecipeCode;
                    Recipe_ID = tr.ID;
                    $("#hiddenRecipeID").val(tr.ID);
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }
                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Recipe " + active + "?", "Recipe Update Confirmation", "Yes", "No", function () {
                        //  SaveRecipeStatus();
                        getRecipeDependenciesData();
                        if (recipeImpactedData.IsMOGImpacted) {
                            $(".recipeDepStatus").text(datamodel.IsActive ? "active" : "inactive");
                            showRecipeImpactedMessage();
                            showRecipeDependenciesPopUp();
                        }
                        else {
                            SaveRecipeStatus();
                        }
                    }, function () {
                        $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsActive);
                    });
                    return true;
                });


                $(".chkboxElementory").on("change", function (e) {
                    // 
                    // isStatusSave = true;
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridRecipe").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    RecipeName = tr.Name;
                    RecipeCode = tr.RecipeCode;
                    Recipe_ID = tr.ID;
                    $("#hiddenRecipeID").val(tr.ID);
                    datamodel.IsElementory = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsElementory) {
                        active = "Elementory";
                    } else {
                        active = "Non Elementory";
                    }
                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Recipe " + active + "?", "Recipe Update Confirmation", "Yes", "No", function () {
                        SaveRecipeElmentoryStatus();
                    }, function () {
                        $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsElementory);
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });

                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }
        })
    }
    else {
        gridVariable.kendoGrid({
            excel: {
                fileName: "Recipe.xlsx",
                filterable: true,
                allPages: true
            },
            //selectable: "cell",
            sortable: true,
            noRecords: {
                template: "No Records Available"
            },
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: true,
            //reorderable: true,
            scrollable: true,
            columns: [
                {
                    field: "RecipeCode", title: "Code", width: "25px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Recipe Name", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "RecipeAlias", title: "Recipe Alias", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "20px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "Quantity", title: "Qty", width: "20px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "RecipeType", title: "Type", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    template: '# if (IsBase == true) {#<div>Base Recipe</div>#} else {#<div>Final Recipe</div>#}#',
                },
                {
                    field: "Impact", title: "Impacts", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    template: '# if (IsBase == true) { #<div>#:Impact# Recipe(s)</div>#} else {#<div>#:Impact# Dish</div>#}#',
                },

                {
                    field: "IsActive",
                    title: "Active", width: "25px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    template: '<label class= "switch"><input type="checkbox" id="chkbox-#:ID#" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                },
                {
                    field: "PublishedDateAsString", title: "Last Updated", width: "35px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    // template: '# if (PublishedDate == null) {#<span>-</span>#} else{#<span>#: kendo.toString(PublishedDate, "dd-MMM-yy")#</span>#}#',
                    headerAttributes: {
                        style: "text-align: center;"
                    }

                },
                {
                    field: "Status", title: "Status", width: "30px", attributes: {
                        class: "mycustomstatus",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Published#}#',
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },
                {
                    field: "Edit", title: "Configure", width: "30px",
                    attributes: {
                        style: "text-align: left; font-weight:normal;cursor: pointer;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            template: function (dataItem) {
                                var html = `  <div title="Configure" onClick="recipeGridEdit(this)"  style='color:#8f9090; margin-left:20px' class='fa fa-cog EditRecipe'></div>`;
                                return html;
                            },

                        },
                    ],
                }
            ],
            dataSource: {
                sort: { field: "IsBase", dir: "asc" },
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (result) {
                            Utility.UnLoading();
                            if (result != null) {
                                copyData = result;
                                if (isShowElmentoryRecipe) {
                                    result = result.filter(m => m.IsElementory);
                                }
                                options.success(result);

                                copyData.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
                                populateRecipeCopyDropDown();

                            }
                            else {
                                options.success("");
                            }
                        },
                            {
                                condition: "All",
                                DishCode: "0"
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            RecipeCode: { type: "string", editable: false },
                            Name: { type: "string", editable: false },
                            RecipeAlias: { type: "string", editable: false },
                            UOMName: { type: "string", editable: false },
                            IsBase: { type: "boolean", editable: false },
                            Quantity: { type: "int", editable: false },
                            IsActive: { type: "boolean", editable: false },

                            Status: { type: "string", editable: false },
                            RecipeType: { type: "string", editable: false },
                            PublishedDate: { type: "date" },
                            PublishedDateAsString: { type: "string", editable: false },
                            IsElementoryString: { type: "string", editable: false },
                            IsElementory: { type: "boolean", editable: false },

                        }
                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            // height: 500,
            dataBound: function (e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }
                    else {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    }

                }

                var items = e.sender.items();

                var items = e.sender.items();
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsElementory) {
                        $(this).find(".MapNutrients").addClass("k-state-disabled");
                    }
                });

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".EditRecipe").addClass("k-state-disabled");
                    }
                });



                //items.each(function (e) {

                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //    }
                //});


                $(".chkbox").on("change", function (e) {
                    // 
                    isStatusSave = true;
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridRecipe").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    RecipeName = tr.Name;
                    RecipeCode = tr.RecipeCode;
                    Recipe_ID = tr.ID;
                    $("#hiddenRecipeID").val(tr.ID);
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }
                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Recipe " + active + "?", "Recipe Update Confirmation", "Yes", "No", function () {
                        //  SaveRecipeStatus();
                        getRecipeDependenciesData();
                        if (recipeImpactedData.IsMOGImpacted) {
                            $(".recipeDepStatus").text(datamodel.IsActive ? "active" : "inactive");
                            showRecipeImpactedMessage();
                            showRecipeDependenciesPopUp();
                        }
                        else {
                            SaveRecipeStatus();
                        }
                    }, function () {
                        $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsActive);
                    });
                    return true;
                });


                $(".chkboxElementory").on("change", function (e) {
                    // 
                    // isStatusSave = true;
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridRecipe").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    RecipeName = tr.Name;
                    RecipeCode = tr.RecipeCode;
                    Recipe_ID = tr.ID;
                    $("#hiddenRecipeID").val(tr.ID);
                    datamodel.IsElementory = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsElementory) {
                        active = "Elementory";
                    } else {
                        active = "Non Elementory";
                    }
                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Recipe " + active + "?", "Recipe Update Confirmation", "Yes", "No", function () {
                        SaveRecipeElmentoryStatus();
                    }, function () {
                        $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsElementory);
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });

                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }
        })
    }

    //$(".k-label")[0].innerHTML.replace("items", "records");
}

function elementoryRecipe(e) {
    isShowElmentoryRecipe = e.checked;
    populateRecipeGrid();
}

function mapNutrients(e) {
    Utility.Loading();
    setTimeout(function () {
        $("#inputNutrientClear").empty();
        $("#inputNutrientClear").append('<div style="display: none; text - align: left; " id="inputNutrient" ></div>');
        checkedNutrientCodes = [];
        checkedIds = [];
        CheckedTrueAPLCodes = [];
        RecipeWisenutrientMasterdataSource = [];
        isShowPreview = false;
        $("#btnShowChecked").prop('checked', false);
        $("#btnPreview").prop('checked', false);
        var gridObj = $("#gridRecipe").data("kendoGrid");
        var tr = gridObj.dataItem($(e).closest("tr"));

        datamodel = tr;
        RecipeName = tr.Name;
        RecipeCode = tr.RecipeCode;
        RecipeID = tr.ID;
        $("#nRecipecodelabel").text(tr.RecipeCode);
        $("#ninputRecipenamelabel").text(tr.Name);
        $("#ninputuomnamelabel").text(tr.UOMName);
        $("#ninputRecipename").val(tr.Name);
        $("#ninputRecipename").attr("disabled", "disabled");
        $("#ninputRecipealiaslabel").val(tr.Alias);
        $("#nRecipecode").val(tr.RecipeCode);
        $("#Recipequantity").val(tr.NutrientRecipeQuantity);
        var dialog = $("#windowEditRecipeWiseNutrient").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();
        $("#Recipeid").val(tr.ID);
        dialog.title(RecipeName + ": ");
        $("#btnGo").css("display", "inline-block");
        RecipeCode = tr.RecipeCode;

        if (tr.NutritionData != null && tr.NutritionData != "") {
            RecipeWisenutrientMasterdataSource = JSON.parse(tr.NutritionData);
            var nData = JSON.parse(tr.NutritionData);
            for (dataItem of nutrientMasterdataSource) {
                var notExists = nData.filter(m => m.ElementCode == dataItem.NutritionElementCode);
                if (notExists == null || notExists.length == 0) {
                    var nutmodel = {
                        "TypeCode": dataItem.NutritionElementTypeCode,
                        "TypeName": dataItem.NutritionElementTypeName,
                        "ElementCode": dataItem.NutritionElementCode,
                        "ElementName": dataItem.Name,
                        "Quantity": 0,
                        "UOMCode": dataItem.NutritionElementUOMCode,
                        "UOMName": dataItem.NutritionElementUOMName,
                        "DisplayOrder": dataItem.DisplayOrder,
                        "IsDisplay": dataItem.IsDisplay
                    }
                    RecipeWisenutrientMasterdataSource.push(nutmodel);
                }
            }

        }
        else {
            for (dataItem of nutrientMasterdataSource) {
                var nutmodel = {
                    "TypeCode": dataItem.NutritionElementTypeCode,
                    "TypeName": dataItem.NutritionElementTypeName,
                    "ElementCode": dataItem.NutritionElementCode,
                    "ElementName": dataItem.Name,
                    "Quantity": 0,
                    "UOMCode": dataItem.NutritionElementUOMCode,
                    "UOMName": dataItem.NutritionElementUOMName,
                    "DisplayOrder": dataItem.DisplayOrder,
                    "IsDisplay": dataItem.IsDisplay
                }
                RecipeWisenutrientMasterdataSource.push(nutmodel);
                checkedNutrientCodes.push("1_" + dataItem.NutritionElementCode);
            }
        }
        populateNutrientMasterGrid();

        Utility.UnLoading();
    }, 2000);
}

function populateNutrientMasterGrid() {
    Utility.Loading();


    var gridVariable = $("#gridRecipeWiseNutrients");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "RecipeMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: true,
        groupable: false,
        //reorderable: true,
        //scrollable: true,
        columns: [
            {
                field: "ElementCode", title: "Nutrient Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ElementName", title: "Nutrient", width: "150px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "TypeName", title: "Nutrient Type", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "Nutrient UOM", width: "40px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Nutrient Quantity", width: "60px",
                template: '<input type="number" class="inputbrqty"  value="Quantity" name="ElementQuantity" oninput="savePriceChange(this,true)"/>',
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            }
        ],
        dataSource: {
            data: RecipeWisenutrientMasterdataSource.sort((a, b) => a.DisplayOrder - b.DisplayOrder),
            schema: {
                model: {
                    fields: {
                        ElementCode: { type: "string" },
                        ElementName: { type: "string" },
                        UOMName: { type: "string" },
                        TypeName: { type: "string" },
                        Quantity: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
        height: 415,
        minHeight: 200,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();
            items.each(function (e) {
                var dataItem = grid.dataItem(this);
                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
}


var lastValid = 0;
var validNumber = new RegExp(/^\d*\.?\d*$/);
function validateNumber(elem) {

    if (validNumber.test(elem.value)) {
        lastValid = elem.value;
    } else {
        elem.value = lastValid;
    }
}

function savePriceChange(e, isSavePrice) {
    validateNumber(e);
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridRecipeWiseNutrients").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var ElementCode = dataItem.ElementCode;
    if (dataItem.Quantity == $(e).val()) {
        return;
    }
    dataItem.Quantity = $(e).val();
    dataItem.set('Quantity', $(e).val());
    //RecipeWisenutrientMasterdataSource = RecipeWisenutrientMasterdataSource.forEach(function (item, index) {
    //    if (item.ElementCode == ElementCode) {
    //        item.Quantity = $(e).val();
    //    };
    //});
}

function saveRecipeNutrientMapping() {
    var RecipeQuantity = $("#Recipequantity").text();
    if (RecipeQuantity == null || RecipeQuantity == "" || RecipeQuantity == undefined || RecipeQuantity == "0") {
        toastr.error("Please enter Recipe Quantity for Nutrients First.");
        Utility.UnLoading();
        return;
    }
    var grid = $("#gridRecipeWiseNutrients").data("kendoGrid");
    var nutritionData = JSON.stringify(grid._data);

    var model;
    if (datamodel != null) {
        model = datamodel;
        model.RecipeCode = RecipeCode;
        model.NutrientRecipeQuantity = RecipeQuantity;
        model.NutritionData = nutritionData;
        model.CreatedOn = kendo.parseDate(model.CreatedOn);
        model.IsRecipeNutrientMappingData = true;
    }

    //if (filterData == null || filterData.length == 0) {
    //    toastr.error("Please update or map Nutrients data First.");
    //    return;
    //}
    //else {

    $("#NatiRecipebtnSubmit").attr('disabled', 'disabled');
    HttpClient.MakeSyncRequest(CookBookMasters.SaveRecipeNutrientMappingData, function (result) {
        if (result == false) {
            $('#NatiRecipebtnSubmit').removeAttr("disabled");
            toastr.error("Some error occured, please try again");
            $("#inputRecipe").focus();
            Utility.UnLoading();
        }
        else {
            $(".k-overlay").hide();
            var orderWindow = $("#windowEditRecipeWiseNutrient").data("kendoWindow");
            orderWindow.close();
            $('#NatiRecipebtnSubmit').removeAttr("disabled");
            toastr.success("Recipe Nutrient  mapping saved successfully.");
            Utility.UnLoading();
        }
    }, {
        model: datamodel

    }, true);
    // }
}

function populateNutrientsMasterDropdown() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetNutritionMasterDataList, function (data) {
        nutrientMasterdataSource = data.filter(m => m.IsActive);
        //nutrientMasterdataSource.unshift({ "NutritionElementCode": "Select", "Name": "Select Nutrient" })

    }, null, true);

    nutrientFilterDataSource = nutrientMasterdataSource.filter(function (item) {
        if (!item.RecipeCode)
            return item;
    });


    $("#inputNutrient").kendoMultiSelect({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "NutritionElementCode",
        dataSource: nutrientFilterDataSource,
        placeholder: " ",
        index: 0,
        autoBind: true,
        autoClose: false,
    });
    var multiselect = $("#inputNutrient").data("kendoMultiSelect");

    //clear filter
    multiselect.dataSource.filter({});

    //set value
    // multiselect.value(RecipeWiseNutrientArray);

}

function addNutrient() {
    var dropdownlist = $("#inputNutrient").data("kendoMultiSelect");

    var dataItems = dropdownlist.dataItems();
    for (dataItem of dataItems) {
        var nutmodel = {
            "TypeCode": dataItem.NutritionElementTypeCode,
            "TypeName": dataItem.NutritionElementTypeName,
            "ElementCode": dataItem.NutritionElementCode,
            "ElementName": dataItem.Name,
            "Quantity": 0,
            "UOMCode": dataItem.NutritionElementUOMCode,
            "UOMName": dataItem.NutritionElementUOMName,
            "DisplayOrder": dataItem.DisplayOrder,
            "IsDisplay": dataItem.IsDisplay
        }
        RecipeWisenutrientMasterdataSource.unshift(nutmodel);
        checkedNutrientCodes.push("1_" + dataItem.NutritionElementCode);
        var grid = $("#gridRecipeWiseNutrients").data("kendoGrid");
        grid.dataSource.insert(0, nutmodel);
    }
    filterMultiSelectDropdown();
}

function filterMultiSelectDropdown() {
    var grid = $("#gridRecipeWiseNutrients").data("kendoGrid");
    var multiselect = $("#inputNutrient").data("kendoMultiSelect");

    var modifiedDataSource = nutrientMasterdataSource.filter(function (array_el) {
        return grid.dataSource._data.filter(function (anotherOne_el) {
            return anotherOne_el.ElementCode == array_el.NutritionElementCode;
        }).length == 0
    });

    multiselect.value([]);
    multiselect.setDataSource(modifiedDataSource);
}



function checkRecipeNutrientChange() {
    var isChanged = true;

    return isChanged;
}

function saveAll() {
    var RecipeQuantity = $("#Recipequantity").text();
    if (RecipeQuantity == null || RecipeQuantity == "" || RecipeQuantity == undefined || RecipeQuantity == "0") {
        toastr.error("Please enter Recipe Quantity for each Nutrients First.");
        Utility.UnLoading();
        return;
    }
    if (checkRecipeNutrientChange()) {
        $("#confirmSave_No").css('display', 'block');
        $("#confirmSave_Ok").addClass('white-btn');
        $("#confirmSave_Ok").addClass('white-btn');
        $("#confirmSave_Ok").removeClass('colored-btn');
        $("#confirmSave_Box").css('z-index', '99999');
        Utility.UnLoading();
        Utility.Page_Alert_Save("Proceeding will perform change Recipe Nutrients mapping. Are you sure to proceed?", "Recipe-Nutrients Mapping Change Confirmation", "Yes", "No",
            function () {
                saveRecipeNutrientMapping();
            },
            function () {

            }
        );
    }

    Utility.UnLoading();
}

function onMOGDDLChange(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG', false)) {
        toastr.error("Mog is already selected");
        var dropdownlist = $("#ddlMOG").data("kendoDropDownList");
        dropdownlist.select("");
        //dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    console.log("Data Item ")
    console.log(dataItem)

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);

    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    // var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.CostPerUOM = UnitCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    console.log(dataItem)
    //  dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('');
};

function onMOGUOMDDLChange(e) {
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    console.log("Data Item ")
    console.log(dataItem)

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("UOM_ID", e.sender.dataItem().UOM_ID);
    dataItem.set("UOMName", data);
    dataItem.set("UOMCode", e.sender.value());

    dataItem.UOMName = data;
    dataItem.UOM_ID = e.sender.dataItem().UOM_ID;
    dataItem.UOMCode = e.sender.value();
}

function onMOGUOMDDLChange0(e) {
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    console.log("Data Item ")
    console.log(dataItem)

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("UOM_ID", e.sender.dataItem().UOM_ID);
    dataItem.set("UOMName", data);
    dataItem.set("UOMCode", e.sender.value());

    dataItem.UOMName = data;
    dataItem.UOM_ID = e.sender.dataItem().UOM_ID;
    dataItem.UOMCode = e.sender.value();
}

function populateBaseRecipeGrid(recipeID) {
    // alert(recipeID);
    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe");
    gridVariable.html("");

    if (user.SectorNumber == '20') {
        gridVariable.kendoGrid({
            toolbar: [{ name: "clearall", text: "Clear All" },
            { name: "cancel", text: "Reset" }, {
                name: "create", text: "Add"
            }
            ],
            groupable: false,
            editable: true,
            columns: [
                {
                    field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        //var html = `  <i title="Click to Change Base Recipe" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>`
                        //    + '<span class="brspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        //    + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails(this)'></i>`;
                        var html = '<span class="brspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>';
                            
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "BaseRecipeName",
                                dataValueField: "BaseRecipe_ID",
                                autoBind: true,
                                dataSource: refreshBaseRecipeDropDownData(options.model.BaseRecipeName),
                                value: options.field,
                                change: onBRDDLChange
                            });
                    }
                },

                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "Quantity", title: "Quantity", width: "35px",
                    template: function (dataItem) {
                        var html = '<input  type="number" class="inputbrqty" style="text - align: center;" name="Quantity" oninput="calculateItemTotal(this,null)"/>';
                        return html;
                    },

                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridBaseRecipe").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (selectedRow.IsMajor) {
                                    Utility.Page_Alert_Save("This is a major Ingredient. Do you still want to remove it from the Recipe??", "Delete Confirmation", "Yes", "No",
                                        function () {
                                            gridObj.dataSource._data.remove(selectedRow);
                                            CalculateGrandTotal('');
                                            return true;
                                        },
                                        function () {
                                        }
                                    );
                                }
                                else {
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal('');
                                    return true;
                                }
                            }
                        },
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteBaseRecipesByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                BaseRecipetotal = 0.0;
                                $.each(obj, function (key, value) {

                                    BaseRecipetotal = parseFloat(BaseRecipetotal) + parseFloat(value.Quantity);
                                });
                                $("#totBaseRecpQty").text(BaseRecipetotal.toFixed(2));
                                options.success(result);
                                if (result.length > 0) {
                                    $("#gridBaseRecipe").css("display", "block");
                                    $("#emptybr").css("display", "none");
                                } else {
                                    $("#gridBaseRecipe").css("display", "none");
                                    $("#emptybr").css("display", "block");
                                }

                            }
                            else {
                                options.success("");
                            }


                        },
                            {
                                //recipeCode: recipeCode,
                                SiteCode: $("#NewSimSiteByRegion").val(),
                                recipeID: recipeID
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false },
                            BaseRecipe_ID: { type: "number", validation: { required: true } },
                            BaseRecipeName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },

            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },

            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.brTemplate');
                    $(ddt).kendoDropDownList({
                        index: 0,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "BaseRecipeName",
                        dataValueField: "BaseRecipe_ID",
                        change: onBRDDLChange,
                        //  autoBind: true,
                    });
                    //  ddt.find(".k-input").val(dataItem.BaseRecipeName);
                    $(this).find('.inputbrqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
    }
    else {
        gridVariable.kendoGrid({
            toolbar: [{ name: "clearall", text: "Clear All" },
            { name: "cancel", text: "Reset" }, {
                name: "create", text: "Add"
            }
            ],
            groupable: false,
            editable: true,
            columns: [
                {
                    field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        //var html = `  <i title="Click to Change Base Recipe" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>`
                        //    + '<span class="brspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        //    + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails(this)'></i>`;
                        var html = '<span class="brspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "BaseRecipeName",
                                dataValueField: "BaseRecipe_ID",
                                autoBind: true,
                                dataSource: refreshBaseRecipeDropDownData(options.model.BaseRecipeName),
                                value: options.field,
                                change: onBRDDLChange
                            });
                    }
                },

                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "Quantity", title: "Quantity", width: "35px",
                    template: function (dataItem) {
                        var html = '<input  type="number" class="inputbrqty" style="text - align: center;" name="Quantity" oninput="calculateItemTotal(this,null)"/>';
                        return html;
                    },

                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "major",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
                },
                {
                    field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "minor",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridBaseRecipe").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (selectedRow.IsMajor) {
                                    Utility.Page_Alert_Save("This is a major Ingredient. Do you still want to remove it from the Recipe?", "Delete Confirmation", "Yes", "No",
                                        function () {
                                            gridObj.dataSource._data.remove(selectedRow);
                                            CalculateGrandTotal('');
                                            return true;
                                        },
                                        function () {
                                        }
                                    );
                                }
                                else {
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal('');
                                    return true;
                                }
                            }
                        },
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                BaseRecipetotal = 0.0;
                                $.each(obj, function (key, value) {

                                    BaseRecipetotal = parseFloat(BaseRecipetotal) + parseFloat(value.Quantity);
                                });
                                $("#totBaseRecpQty").text(Utility.MathRound(BaseRecipetotal, 2));

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridBaseRecipe").css("display", "block");
                                $("#emptybr").css("display", "none");
                            } else {
                                $("#gridBaseRecipe").css("display", "none");
                                $("#emptybr").css("display", "block");
                            }

                        },
                            {
                                recipeID: recipeID
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false },
                            BaseRecipe_ID: { type: "number", validation: { required: true } },
                            BaseRecipeName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },

            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },

            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.brTemplate');
                    $(ddt).kendoDropDownList({
                        index: 0,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "BaseRecipeName",
                        dataValueField: "BaseRecipe_ID",
                        change: onBRDDLChange,
                        //  autoBind: true,
                    });
                    //  ddt.find(".k-input").val(dataItem.BaseRecipeName);
                    $(this).find('.inputbrqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
    }



    //  cancelChangesConfirmation('gridBaseRecipe');
    var toolbar = $("#gridBaseRecipe").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.find(".k-grid-clearall").addClass("gridButton");

    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe .k-grid-content").addClass("gridInside");
    toolbar.find(".k-grid-clearall").addClass("gridButton");
    $('#gridBaseRecipe a.k-grid-clearall').click(function (e) {
         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                $("#gridBaseRecipe").data('kendoGrid').dataSource.data([]);
                gridIdin = 'gridBaseRecipe';
                // showHideGrid('gridBaseRecipe', 'emptybr', 'bup_gridBaseRecipe');
                hideGrid('emptybr', 'gridBaseRecipe');
            },
            function () {
            }
        );

    });
    $("#gridBaseRecipe .k-grid-cancel-changes").click(function (e) {
         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                // var grid = $("#" + gridIdin + "").data("kendoGrid");

                if (gridIdin == 'gridBaseRecipe') {
                    populateBaseRecipeGrid(resetID);
                    return;
                } else if (gridIdin == 'gridMOG') {
                    populateMOGGrid(resetID)
                    return;
                }
                // grid.cancelChanges();
            },
            function () {
            }
        );
        //alert('base');
    });

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
        Utility.UnLoading();
    });
}

function refreshBaseRecipeDropDownData(x) {
    // alert("call");
    if (basedataList != null && basedataList.length > 0) {
        var baseGridData = $("#gridBaseRecipe").data('kendoGrid').dataSource._data;
        if (baseGridData != null && baseGridData.length > 0) {
            baseGridData = baseGridData.filter(m => m.BaseRecipeName != "" && m.BaseRecipeName != x);
            baserecipedata = basedataList;
            var basedataListtemp = basedataList;
            basedataListtemp = basedataListtemp.filter(m => m.BaseRecipe_Code != "");
            basedataListtemp.forEach(function (item, index) {
                for (var i = 0; i < baseGridData.length; i++) {
                    if (baseGridData[i].BaseRecipeCode !== "" && item.BaseRecipe_Code === baseGridData[i].BaseRecipeCode) {
                        baserecipedata.splice(index, 1);
                        baserecipedata = baserecipedata.filter(r => r.BaseRecipe_Code !== item.BaseRecipe_Code && r.BaseRecipe_Code != RecipeCode);
                        break;
                    }
                }
                return true;
            });
        }
    }
    baserecipedata = baserecipedata.filter(r => r.BaseRecipe_Code != RecipeCode);
    return baserecipedata;
};

function populateMOGGrid(recipeID, recipeCode) {
    if (user.SectorNumber == "20") {

        Utility.Loading();
        var gridVariable = $("#gridMOG");
        gridVariable.html("");
        if (viewOnly != 1) {
            gridVariable.kendoGrid({
                //toolbar: [{ name: "cancel", text: "Reset" }],
                toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
                groupable: false,
                editable: true,
                //  navigatable: true,
                columns: [
                    {
                        field: "MOG_ID", title: "MOG", width: "120px",
                        attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        template: function (dataItem) {
                            var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                            return html;
                        },
                        editor: function (container, options) {
                            $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    enable: true,
                                    filter: "contains",
                                    dataTextField: "MOGName",
                                    dataValueField: "MOG_ID",
                                    autoBind: true,
                                    dataSource: mogdata,
                                    value: options.field,
                                    change: onMOGDDLChange,
                                });
                            // container.find(".k-input")[0].text = options.model.MOGName;
                        }
                    },
                    {
                        field: "AllergensName", title: "Allergens", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "AllergensName",
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    {
                        field: "UOMName", title: "UOM", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "uomname",
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    attributes: {
                    //        class: "si",
                    //        style: "text-align: center; font-weight:normal"
                    //    }
                    //},

                    {
                        field: "Quantity", title: "Quantity", width: "35px", editable: false,
                        headerAttributes: {
                            style: "text-align: center;width:35px;"
                        },

                        template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,null)"/>',
                        attributes: {

                            style: "text-align: center; font-weight:normal;width:35px;"
                        },
                    },
                    //{

                    //    field: "Quantity", title: "Site Qty", width: "50px",
                    //    template: '# if (IsMajor == true) {#<div><input type="number"  class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" /></div>#} else {#<div><input class="inputmogqty" type="number"   name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',


                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    attributes: {
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //},

                    //{
                    //    field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    attributes: {
                    //        class: "change",
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                    //},


                    {
                        field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                        headerAttributes: {

                            style: "text-align: right"
                        },
                        attributes: {
                            class: "uomcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    //    headerAttributes: {

                    //        style: "text-align: right"
                    //    },
                    //    attributes: {
                    //        class: "regiontotalcost",
                    //        style: "text-align: right; font-weight:normal"
                    //    },
                    //},
                    {
                        field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                        headerAttributes: {

                            style: "text-align: right"
                        },
                        attributes: {
                            class: "totalcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },

                    {
                        title: "Action",
                        width: "60px",
                        headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            style: "text-align: center; font-weight:normal;"
                        },
                        command: [
                            {
                                name: 'Delete',
                                text: "Delete",
                                //iconClass: "fa fa-trash-alt",
                                click: function (e) {
                                    if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                        return false;
                                    }
                                    var gridObj = $("#gridMOG").data("kendoGrid");
                                    var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal('');

                                    //var modelHistory = {
                                    //    "MOGCode": MOGCode,
                                    //    "PreviousArticleNumber": MOGSelectedArticleNumber,
                                    //    "PreviousCost": MOGSelectedCost,
                                    //    "CurrentArticleNumber": artNumber,
                                    //    "CurrentCost": curCost,
                                    //    "UpdatedDate": Utility.CurrentDate(),
                                    //    "UpdatedBy": user.UserId,
                                    //    "SimulationCode": $("#SimulationCode").val(),
                                    //    "RegionID": $("#NewSimRegion").val(),
                                    //    "SiteID": $("#NewSimSiteByRegion").val(),
                                    //    "RecipeCode": $("#copyRecipe0").val(),
                                    //    "DishCode": $("#hdnDishCode").val(),
                                    //    "ItemId": $("#hdnItemId").val(),
                                    //    "DayPartCode": $("#hdnDayPartCode").val()
                                    //};
                                    //HttpClient.MakeRequest(CookBookMasters.SaveSimAPLHistory, function (result) {

                                    //}, { data: modelHistory }, false)
                                    if (modelHistoryData.length > 0) {
                                        var filtermodelHistoryData = modelHistoryData.filter(ds => ds.MOGCode == selectedRow.MOGCode);
                                        modelHistoryData.forEach(function (item, index) {
                                            if (item.MOGCode == selectedRow.MOGCode) {
                                                modelHistoryData.splice(index, 1);
                                            }
                                        });

                                        console.log("modelHistoryData");
                                        console.log(modelHistoryData)
                                    }
                                    return true;
                                }
                            },
                            {
                                name: 'Change APL',
                                click: function (e) {
                                    isMainRecipe = true;
                                    var gridObj = $("#gridMOG").data("kendoGrid");
                                    currentPopuatedMOGGrid = "";
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    console.log("tr")
                                    console.log(tr)
                                    MOGName = tr.MOGName;
                                    MOGSelectedArticleNumber = tr.ArticleNumber;
                                    //MOGSelectedCost = tr.TotalCost * tr.Quantity;
                                    MOGSelectedCost = tr.CostPerKG;
                                    isRegionAPL = false;

                                    var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                    $("#windowEditMOGWiseAPL").kendoWindow({
                                        animation: false
                                    });
                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open();
                                    dialog.center();
                                    dialog.title("MOG APL Details - " + MOGName);
                                    MOGCode = tr.MOGCode;
                                    mogWiseAPLMasterdataSource = [];
                                    getMOGWiseAPLMasterData(MOGCode);
                                    currentGridId = "#gridMOG";
                                    //alert(currentGridId);
                                    populateAPLMasterGrid("Change", "#gridMOG");
                                    Utility.UnLoading();
                                    return true;
                                }
                            }
                        ],
                    }
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";
                            HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    var obj = result;
                                    mogtotal = 0.0;
                                    $.each(obj, function (key, value) {

                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                    });

                                    $("#totmog").text(mogtotal.toFixed(2));
                                    var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                    $("#ToTIngredients").text(tot);
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                                                                
                                if (result.length > 0) {
                                    $("#gridMOG").css("display", "block");
                                    $("#emptymog").css("display", "none");
                                } else {
                                    $("#gridMOG").css("display", "none");
                                    $("#emptymog").css("display", "block");
                                }
                            },
                                {

                                    SiteCode: $("#hdnSiteCode").val(),
                                    recipeID: 0,
                                    recipeCode: recipeCode,
                                    simCode: $("#SimulationCode").val()
                                }
                                , true);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                MOG_ID: { type: "number", validation: { required: true } },
                                MOGName: { type: "string", validation: { required: true } },
                                Quantity: { type: "number", validation: { required: true, min: 1 } },
                                RegionQuantity: { type: "number", editable: false },
                                RegionIngredientPerc: { type: "number", editable: false },
                                UOMName: { editable: false },
                                CostPerUOM: { editable: false },
                                CostPerKG: { editable: false },
                                TotalCost: { editable: false },
                                RegionTotalCost: { editable: false },
                                ArticleNumber: { editable: false }
                            }
                        }
                    }
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var grid = e.sender;
                    var items = e.sender.items();
                    items.each(function (e) {
                        var dataItem = grid.dataItem(this);
                        var ddt = $(this).find('.mogTemplate');
                        $(ddt).kendoDropDownList({
                            enable: false,
                            value: dataItem.value,
                            dataSource: baserecipedata,
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            change: onMOGDDLChange
                        });
                        //ddt.find(".k-input").val(dataItem.MOGName);

                        $(this).find('.inputmogqty').val(dataItem.Quantity);
                    });
                },
                change: function (e) {
                },
            });
            var dropdownlist = $("#copyRecipe0").data("kendoDropDownList");
            if (dropdownlist != undefined)
                dropdownlist.enable(true);
            dropdownlist = $("#inputrecipecum").data("kendoDropDownList");
            if (dropdownlist != undefined)
                dropdownlist.enable(true);
        }
        else {  
            gridVariable.kendoGrid({
                //toolbar: [{ name: "cancel", text: "Reset" }],
                toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
                groupable: false,
                //editable: true,
                editable: false,
                //  navigatable: true,
                columns: [
                    {
                        field: "MOG_ID", title: "MOG", width: "120px",
                        attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        template: function (dataItem) {
                            var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                            return html;
                        },
                        editor: function (container, options) {
                            $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    enable: true,
                                    filter: "contains",
                                    dataTextField: "MOGName",
                                    dataValueField: "MOG_ID",
                                    autoBind: true,
                                    dataSource: mogdata,
                                    value: options.field,
                                    change: onMOGDDLChange,
                                });
                            // container.find(".k-input")[0].text = options.model.MOGName;
                        }
                    },
                    {
                        field: "AllergensName", title: "Allergens", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "AllergensName",
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    {
                        field: "UOMName", title: "UOM", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "uomname",
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    attributes: {
                    //        class: "si",
                    //        style: "text-align: center; font-weight:normal"
                    //    }
                    //},

                    {
                        field: "Quantity", title: "Quantity", width: "35px", editable: false,
                        headerAttributes: {
                            style: "text-align: center;width:35px;"
                        },

                        //template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,null)"/>',
                        attributes: {

                            style: "text-align: center; font-weight:normal;width:35px;"
                        },
                    },
                    //{

                    //    field: "Quantity", title: "Site Qty", width: "50px",
                    //    template: '# if (IsMajor == true) {#<div><input type="number"  class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" /></div>#} else {#<div><input class="inputmogqty" type="number"   name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',


                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    attributes: {
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //},

                    //{
                    //    field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    attributes: {
                    //        class: "change",
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                    //},


                    {
                        field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                        headerAttributes: {

                            style: "text-align: right"
                        },
                        attributes: {
                            class: "uomcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    //    headerAttributes: {

                    //        style: "text-align: right"
                    //    },
                    //    attributes: {
                    //        class: "regiontotalcost",
                    //        style: "text-align: right; font-weight:normal"
                    //    },
                    //},
                    {
                        field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                        headerAttributes: {

                            style: "text-align: right"
                        },
                        attributes: {
                            class: "totalcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },

                    {
                        title: "Action",
                        width: "60px",
                        headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            style: "text-align: center; font-weight:normal;"
                        },
                        command: [
                            //{
                            //    name: 'Delete',
                            //    text: "Delete",
                            //    //iconClass: "fa fa-trash-alt",
                            //    click: function (e) {
                            //        if ($(e.currentTarget).hasClass("k-state-disabled")) {
                            //            return false;
                            //        }
                            //        var gridObj = $("#gridMOG").data("kendoGrid");
                            //        var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            //        gridObj.dataSource._data.remove(selectedRow);
                            //        CalculateGrandTotal('');

                            //        //var modelHistory = {
                            //        //    "MOGCode": MOGCode,
                            //        //    "PreviousArticleNumber": MOGSelectedArticleNumber,
                            //        //    "PreviousCost": MOGSelectedCost,
                            //        //    "CurrentArticleNumber": artNumber,
                            //        //    "CurrentCost": curCost,
                            //        //    "UpdatedDate": Utility.CurrentDate(),
                            //        //    "UpdatedBy": user.UserId,
                            //        //    "SimulationCode": $("#SimulationCode").val(),
                            //        //    "RegionID": $("#NewSimRegion").val(),
                            //        //    "SiteID": $("#NewSimSiteByRegion").val(),
                            //        //    "RecipeCode": $("#copyRecipe0").val(),
                            //        //    "DishCode": $("#hdnDishCode").val(),
                            //        //    "ItemId": $("#hdnItemId").val(),
                            //        //    "DayPartCode": $("#hdnDayPartCode").val()
                            //        //};
                            //        //HttpClient.MakeRequest(CookBookMasters.SaveSimAPLHistory, function (result) {

                            //        //}, { data: modelHistory }, false)
                            //        if (modelHistoryData.length > 0) {
                            //            var filtermodelHistoryData = modelHistoryData.filter(ds => ds.MOGCode == selectedRow.MOGCode);
                            //            modelHistoryData.forEach(function (item, index) {
                            //                if (item.MOGCode == selectedRow.MOGCode) {
                            //                    modelHistoryData.splice(index, 1);
                            //                }
                            //            });

                            //            console.log("modelHistoryData");
                            //            console.log(modelHistoryData)
                            //        }
                            //        return true;
                            //    }
                            //},
                            {
                                name: 'View APL',
                                click: function (e) {

                                    var gridObj = $("#gridMOG").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    console.log("MOG APL");
                                    console.log(tr);
                                    MOGName = tr.MOGName;

                                    var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                    $("#windowEditMOGWiseAPL").kendoWindow({
                                        animation: false
                                    });
                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open();
                                    dialog.center();
                                    dialog.title("MOG APL Details - " + MOGName);
                                    MOGCode = tr.MOGCode;
                                    mogWiseAPLMasterdataSource = [];
                                    getMOGWiseAPLMasterData();
                                    currentGridId = "#gridMOG";
                                    populateAPLMasterGrid("View", "#gridMOG");
                                    return true;
                                }
                            }
                            //{
                            //    name: 'Change APL',
                            //    click: function (e) {
                            //        isMainRecipe = true;
                            //        var gridObj = $("#gridMOG").data("kendoGrid");
                            //        currentPopuatedMOGGrid = "";
                            //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            //        datamodel = tr;
                            //        console.log("tr")
                            //        console.log(tr)
                            //        MOGName = tr.MOGName;
                            //        MOGSelectedArticleNumber = tr.ArticleNumber;
                            //        //MOGSelectedCost = tr.TotalCost * tr.Quantity;
                            //        MOGSelectedCost = tr.CostPerKG;
                            //        isRegionAPL = false;

                            //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                            //        $("#windowEditMOGWiseAPL").kendoWindow({
                            //            animation: false
                            //        });
                            //        $(".k-overlay").css("display", "block");
                            //        $(".k-overlay").css("opacity", "0.5");
                            //        dialog.open();
                            //        dialog.center();
                            //        dialog.title("MOG APL Details - " + MOGName);
                            //        MOGCode = tr.MOGCode;
                            //        mogWiseAPLMasterdataSource = [];
                            //        getMOGWiseAPLMasterData(MOGCode);
                            //        currentGridId = "#gridMOG";
                            //        //alert(currentGridId);
                            //        populateAPLMasterGrid("Change", "#gridMOG");
                            //        Utility.UnLoading();
                            //        return true;
                            //    }
                            //}
                        ],
                    }
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";
                            HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    var obj = result;
                                    mogtotal = 0.0;
                                    $.each(obj, function (key, value) {

                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                    });

                                    $("#totmog").text(mogtotal.toFixed(2));
                                    var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                    $("#ToTIngredients").text(tot);
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }

                                if (result.length > 0) {
                                    $("#gridMOG").css("display", "block");
                                    $("#emptymog").css("display", "none");
                                } else {
                                    $("#gridMOG").css("display", "none");
                                    $("#emptymog").css("display", "block");
                                }
                            },
                                {

                                    SiteCode: $("#hdnSiteCode").val(),
                                    recipeID: 0,
                                    recipeCode: recipeCode,
                                    simCode: $("#SimulationCode").val()
                                }
                                , true);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                MOG_ID: { type: "number", validation: { required: true } },
                                MOGName: { type: "string", validation: { required: true } },
                                Quantity: { type: "number", validation: { required: true, min: 1 } },
                                RegionQuantity: { type: "number", editable: false },
                                RegionIngredientPerc: { type: "number", editable: false },
                                UOMName: { editable: false },
                                CostPerUOM: { editable: false },
                                CostPerKG: { editable: false },
                                TotalCost: { editable: false },
                                RegionTotalCost: { editable: false },
                                ArticleNumber: { editable: false }
                            }
                        }
                    }
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var grid = e.sender;
                    var items = e.sender.items();
                    items.each(function (e) {
                        var dataItem = grid.dataItem(this);
                        var ddt = $(this).find('.mogTemplate');
                        $(ddt).kendoDropDownList({
                            enable: false,
                            value: dataItem.value,
                            dataSource: baserecipedata,
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            change: onMOGDDLChange
                        });
                        //ddt.find(".k-input").val(dataItem.MOGName);

                        $(this).find('.inputmogqty').val(dataItem.Quantity);
                    });
                },
                change: function (e) {
                },
            });
            var dropdownlist = $("#copyRecipe0").data("kendoDropDownList");
            if (dropdownlist != undefined)
                dropdownlist.enable(false);
            dropdownlist = $("#inputrecipecum").data("kendoDropDownList");
            if (dropdownlist != undefined)
                dropdownlist.enable(false);
        }
        var toolbar = $("#gridMOG").find(".k-grid-toolbar");
        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
       
        $("#gridMOG .k-grid-content").addClass("gridInside");
        toolbar.find(".k-grid-clearall").addClass("gridButton");
        $('#gridMOG a.k-grid-clearall').click(function (e) {
             Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    modelHistoryData = [];

                    $("#gridMOG").data('kendoGrid').dataSource.data([]);
                    showHideGrid('gridMOG', 'emptymog', 'mup_gridBaseRecipe');
                    gridIdin = 'gridMOG';
                },
                function () {
                }
            );

        });
        $("#gridMOG .k-grid-cancel-changes").unbind("mousedown");
        $(".k-grid-cancel-changes").mousedown(function (e) {
             Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    var grid = $("#gridMOG").data("kendoGrid");
                    grid.cancelChanges();
                    modelHistoryData = [];
                    //alert('down');
                },
                function () {
                }
            );

        });
        if (viewOnly == 1) {
            $("#gridMOG .gridToolbarShift").hide();
            //$("#windowEdit input[type=text]").attr("disabled", true);
            $("#inputrecipealiasname").attr('disabled', 'disabled');
            $("#inputquantity").attr('disabled', 'disabled');
            $($('#inputinstruction').data().kendoEditor.body).attr('contenteditable', false);
        }
        else {
            $("#gridMOG .gridToolbarShift").show();
            //$("#windowEdit input[type=text]").removeAttr("disabled");
            $("#inputrecipealiasname").removeAttr('disabled');
            $("#inputquantity").removeAttr('disabled');
            $($('#inputinstruction').data().kendoEditor.body).attr('contenteditable', true);
        }
        //var gridObj = $("#gridMOG").data("kendoGrid");
        //$("#gridMOG").find(".k-grid-toolbar").on("click", ".k-grid-cancel-changes", function (e) {
        //    e.preventDefault();
        //     Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
        //        function () {
        //            // var grid = $("#" + gridIdin + "").data("kendoGrid");
        //            //alert(gridIdin);
        //            //if (gridIdin == 'gridBaseRecipe') {
        //            //    populateBaseRecipeGrid(resetID);
        //            //    return;
        //            //} else if (gridIdin == 'gridMOG') {
        //            //populateMOGGrid(resetID)
        //            //return;
        //            //}
        //            // grid.cancelChanges();
        //        },
        //        function () {
        //        }
        //    );
        //});

        //$("#gridMOG .k-grid-cancel-changes").click(function (e) {
        //    e.preventDefault();
        //});
    }
    else {
        Utility.Loading();
        var gridVariable = $("#gridMOG");
        gridVariable.html("");
        if (viewOnly != 1) {
            gridVariable.kendoGrid({
                toolbar: [{ name: "cancel", text: "Reset" }],
                groupable: false,
                editable: false,
                navigatable: true,
                columns: [
                    {
                        field: "MOG_ID", title: "MOG", width: "120px",
                        attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        template: function (dataItem) {
                            var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                            return html;
                        },
                        editor: function (container, options) {
                            $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    enable: false,
                                    filter: "contains",
                                    dataTextField: "MOGName",
                                    dataValueField: "MOG_ID",
                                    autoBind: true,
                                    dataSource: mogdata,
                                    value: options.field,
                                    change: onMOGDDLChange,
                                });
                            // container.find(".k-input")[0].text = options.model.MOGName;
                        }
                    },
                    {
                        field: "AllergensName", title: "Allergens", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "AllergensName",
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    {
                        field: "UOMName", title: "UOM", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "uomname",
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    {
                        field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "si",
                            style: "text-align: center; font-weight:normal"
                        }
                    },
                    {
                        field: "Quantity", title: "Site Qty", width: "50px",
                        template: '# if (IsMajor == true) {#<div><input type="number"  class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputmogqty" type="number"   name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',


                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },
                    },

                    {
                        field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "change",
                            style: "text-align: center; font-weight:normal"
                        },
                        template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                    },


                    {
                        field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                        headerAttributes: {

                            style: "text-align: right"
                        },
                        attributes: {
                            class: "uomcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                        headerAttributes: {

                            style: "text-align: right"
                        },
                        attributes: {
                            class: "regiontotalcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                        headerAttributes: {

                            style: "text-align: right"
                        },
                        attributes: {
                            class: "totalcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },

                    {
                        title: "Action",
                        width: "60px",
                        headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            style: "text-align: center; font-weight:normal;"
                        },
                        command: [
                            {
                                name: 'Change APL',
                                click: function (e) {
                                    isMainRecipe = true;
                                    var gridObj = $("#gridMOG").data("kendoGrid");
                                    currentPopuatedMOGGrid = "";
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;

                                    isRegionAPL = false;

                                    MOGName = tr.MOGName;
                                    MOGSelectedArticleNumber = tr.ArticleNumber;
                                    var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                    $("#windowEditMOGWiseAPL").kendoWindow({
                                        animation: false
                                    });
                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open();
                                    dialog.center();
                                    dialog.title("MOG APL Details - " + MOGName);
                                    MOGCode = tr.MOGCode;
                                    mogWiseAPLMasterdataSource = [];
                                    getMOGWiseAPLMasterData(MOGCode);
                                    currentGridId = "#gridMOG";
                                    //alert(currentGridId);
                                    populateAPLMasterGrid("Change", "#gridMOG");
                                    Utility.UnLoading();
                                    return true;
                                }
                            }
                        ],
                    }
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";
                            HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    var obj = result;
                                    mogtotal = 0.0;
                                    $.each(obj, function (key, value) {

                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                    });

                                    $("#totmog").text(mogtotal.toFixed(2));
                                    var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                    $("#ToTIngredients").text(tot);
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }

                                if (result.length > 0) {
                                    $("#gridMOG").css("display", "block");
                                    $("#emptymog").css("display", "none");
                                } else {
                                    $("#gridMOG").css("display", "none");
                                    $("#emptymog").css("display", "block");
                                }
                            },
                                {

                                    SiteCode: $("#hdnSiteCode").val(),
                                    recipeID: 0,
                                    recipeCode: recipeCode,
                                    simCode: $("#SimulationCode").val()
                                }
                                , true);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                MOG_ID: { type: "number", validation: { required: true } },
                                MOGName: { type: "string", validation: { required: true } },
                                Quantity: { type: "number", validation: { required: true, min: 1 } },
                                UOMName: { editable: false },
                                CostPerUOM: { editable: false },
                                CostPerKG: { editable: false },
                                TotalCost: { editable: false }
                            }
                        }
                    }
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var grid = e.sender;
                    var items = e.sender.items();
                    items.each(function (e) {
                        var dataItem = grid.dataItem(this);
                        var ddt = $(this).find('.mogTemplate');
                        $(ddt).kendoDropDownList({
                            enable: false,
                            value: dataItem.value,
                            dataSource: baserecipedata,
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            change: onMOGDDLChange
                        });
                        $(this).find('.inputmogqty').val(dataItem.Quantity);
                    });
                },
                change: function (e) {
                },
            });
            var dropdownlist = $("#copyRecipe0").data("kendoDropDownList");
            if (dropdownlist != undefined)
                dropdownlist.enable(true);
            dropdownlist = $("#inputrecipecum").data("kendoDropDownList");
            if (dropdownlist != undefined)
                dropdownlist.enable(false);
        }
        else {
            gridVariable.kendoGrid({
                toolbar: [{ name: "cancel", text: "Reset" }],
                groupable: false,
                editable: false,
                navigatable: true,
                columns: [
                    {
                        field: "MOG_ID", title: "MOG", width: "120px",
                        attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        template: function (dataItem) {
                            var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                            return html;
                        },
                        editor: function (container, options) {
                            $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    enable: false,
                                    filter: "contains",
                                    dataTextField: "MOGName",
                                    dataValueField: "MOG_ID",
                                    autoBind: true,
                                    dataSource: mogdata,
                                    value: options.field,
                                    change: onMOGDDLChange,
                                });
                            // container.find(".k-input")[0].text = options.model.MOGName;
                        }
                    },
                    {
                        field: "AllergensName", title: "Allergens", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "AllergensName",
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    {
                        field: "UOMName", title: "UOM", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "uomname",
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    {
                        field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "si",
                            style: "text-align: center; font-weight:normal"
                        }
                    },
                    {
                        field: "Quantity", title: "Site Qty", width: "50px",
                        template: '# if (IsMajor == true) {#<div><input type="number"  class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputmogqty" type="number"   name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',


                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },
                    },

                    {
                        field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            class: "change",
                            style: "text-align: center; font-weight:normal"
                        },
                        template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                    },


                    {
                        field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                        headerAttributes: {

                            style: "text-align: right"
                        },
                        attributes: {
                            class: "uomcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                        headerAttributes: {

                            style: "text-align: right"
                        },
                        attributes: {
                            class: "regiontotalcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                        headerAttributes: {

                            style: "text-align: right"
                        },
                        attributes: {
                            class: "totalcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },

                    {
                        title: "Action",
                        width: "60px",
                        headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            style: "text-align: center; font-weight:normal;"
                        },
                        command: [
                            //{
                            //    name: 'Change APL',
                            //    click: function (e) {
                            //        isMainRecipe = true;
                            //        var gridObj = $("#gridMOG").data("kendoGrid");
                            //        currentPopuatedMOGGrid = "";
                            //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            //        datamodel = tr;

                            //        isRegionAPL = false;

                            //        MOGName = tr.MOGName;
                            //        MOGSelectedArticleNumber = tr.ArticleNumber;
                            //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                            //        $("#windowEditMOGWiseAPL").kendoWindow({
                            //            animation: false
                            //        });
                            //        $(".k-overlay").css("display", "block");
                            //        $(".k-overlay").css("opacity", "0.5");
                            //        dialog.open();
                            //        dialog.center();
                            //        dialog.title("MOG APL Details - " + MOGName);
                            //        MOGCode = tr.MOGCode;
                            //        mogWiseAPLMasterdataSource = [];
                            //        getMOGWiseAPLMasterData(MOGCode);
                            //        currentGridId = "#gridMOG";
                            //        //alert(currentGridId);
                            //        populateAPLMasterGrid("Change", "#gridMOG");
                            //        Utility.UnLoading();
                            //        return true;
                            //    }
                            //}
                            {
                                name: 'View APL',
                                click: function (e) {

                                    var gridObj = $("#gridMOG").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    console.log("MOG APL");
                                    console.log(tr);
                                    MOGName = tr.MOGName;

                                    var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                    $("#windowEditMOGWiseAPL").kendoWindow({
                                        animation: false
                                    });
                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open();
                                    dialog.center();
                                    dialog.title("MOG APL Details - " + MOGName);
                                    MOGCode = tr.MOGCode;
                                    mogWiseAPLMasterdataSource = [];
                                    getMOGWiseAPLMasterData();
                                    populateAPLMasterGrid();
                                    return true;
                                }
                            }
                        ],
                    }
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";
                            HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    var obj = result;
                                    mogtotal = 0.0;
                                    $.each(obj, function (key, value) {

                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                    });

                                    $("#totmog").text(mogtotal.toFixed(2));
                                    var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                    $("#ToTIngredients").text(tot);
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }

                                if (result.length > 0) {
                                    $("#gridMOG").css("display", "block");
                                    $("#emptymog").css("display", "none");
                                } else {
                                    $("#gridMOG").css("display", "none");
                                    $("#emptymog").css("display", "block");
                                }
                            },
                                {

                                    SiteCode: $("#hdnSiteCode").val(),
                                    recipeID: 0,
                                    recipeCode: recipeCode,
                                    simCode: $("#SimulationCode").val()
                                }
                                , true);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                MOG_ID: { type: "number", validation: { required: true } },
                                MOGName: { type: "string", validation: { required: true } },
                                Quantity: { type: "number", validation: { required: true, min: 1 } },
                                UOMName: { editable: false },
                                CostPerUOM: { editable: false },
                                CostPerKG: { editable: false },
                                TotalCost: { editable: false }
                            }
                        }
                    }
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var grid = e.sender;
                    var items = e.sender.items();
                    items.each(function (e) {
                        var dataItem = grid.dataItem(this);
                        var ddt = $(this).find('.mogTemplate');
                        $(ddt).kendoDropDownList({
                            enable: false,
                            value: dataItem.value,
                            dataSource: baserecipedata,
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            change: onMOGDDLChange
                        });
                        $(this).find('.inputmogqty').val(dataItem.Quantity);
                    });
                },
                change: function (e) {
                },
            });
            var dropdownlist = $("#copyRecipe0").data("kendoDropDownList");
            if (dropdownlist != undefined)
                dropdownlist.enable(false);
            dropdownlist = $("#inputrecipecum").data("kendoDropDownList");
            if (dropdownlist != undefined)
                dropdownlist.enable(false);
        }
        var toolbar = $("#gridMOG").find(".k-grid-toolbar");

        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        cancelChangesConfirmation(document.getElementById("gridMOG"));
        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG .k-grid-content").addClass("gridInside");
        if (viewOnly == 1) {
            $("#gridMOG .gridToolbarShift").hide();
            //$("#windowEdit input[type=text]").attr("disabled", true);
            $("#inputrecipealiasname").attr('disabled', 'disabled');
            $("#inputquantity").attr('disabled', 'disabled');
            $($('#inputinstruction').data().kendoEditor.body).attr('contenteditable', false);
        }
        else {
            $("#gridMOG .gridToolbarShift").show();
            //$("#windowEdit input[type=text]").removeAttr("disabled");
            $("#inputrecipealiasname").removeAttr('disabled');
            $("#inputquantity").removeAttr('disabled');
            $($('#inputinstruction').data().kendoEditor.body).attr('contenteditable', true);;
        }
    }

    if (recipeCode == "") {
        $("#gridMOG").hide();
        $("#emptymog").show();
    }
}


function templateFunction(dataItem) {
    var aplGrid = $("#gridMOGWiseAPL").data("kendoGrid");

    var maxStandardCost = 0;
    if (aplGrid != null && aplGrid._data != null && aplGrid._data[0] != undefined) {
        maxStandardCost = aplGrid._data[0].StandardCostPerKg;
        //console.log("MAX" + maxStandardCost)
    }

    var cell = "";
    var item = "";

    item += "<label>"
    if (MOGSelectedArticleNumber == null || MOGSelectedArticleNumber == "") {
        if (dataItem.IsMaxAPLCost) {
            if ($("#labelUnitAPLList").text() == "Region APL List")
                item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "_" + dataItem.SiteCode + "' onchange='radioButtonChange()' checked=checked />";
            else
                item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "' onchange='radioButtonChange()' checked=checked />";
        } else {
            if ($("#labelUnitAPLList").text() == "Region APL List")
                item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "_" + dataItem.SiteCode + "' onchange='radioButtonChange()' class=''/>";
            else
                item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "' onchange='radioButtonChange()' class=''/>";
        }
    }
    else {
        if (dataItem.ArticleNumber === MOGSelectedArticleNumber) {
            if ($("#labelUnitAPLList").text() == "Region APL List")
                item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "_" + dataItem.SiteCode + "' onchange='radioButtonChange()' checked=checked />";
            else
                item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "' onchange='radioButtonChange()' checked=checked />";
        } else {
            if ($("#labelUnitAPLList").text() == "Region APL List")
                item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "_" + dataItem.SiteCode + "' onchange='radioButtonChange()' class=''/>";
            else
                item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "' onchange='radioButtonChange()' class=''/>";
        }
    }
    //item += categories[i].text;
    item += "</label>"
    item += "</br>";

    cell += item;
    return cell;
};

function radioButtonChange(gridId) {
    var selectedVal = "";
    //if (currentGridId.length == 0)
    gridId = currentGridId;

    //console.log(gridId);
    var level = gridId.match(/\d+/) == null ? "" : gridId.match(/\d+/)[0];
    var selected = $("input[type='radio'][name='radioLineSelector']:checked");
    var recipeAllergens = "";
    if (selected.length > 0) {
        selectedVal = selected.val();
        var artNumber = "";
        var curCost = "";
        var prevCost = "";
        var siteCode = "";
        var moggrid = $(gridId + currentPopuatedMOGGrid + "").data("kendoGrid").dataSource;
        var aplGrid = $("#gridMOGWiseAPL").data("kendoGrid");
        var allergen = [];
        if ($("#labelUnitAPLList").text() == "Region APL List") {
            artNumber = selectedVal.split('_')[0];
            siteCode = selectedVal.split('_')[1];
            allergen = aplGrid._data.filter(m => m.ArticleNumber == selectedVal.split('_')[0] && m.SiteCode == selectedVal.split('_')[1]);
            curCost = aplGrid._data.filter(m => m.ArticleNumber == selectedVal.split('_')[0] && m.SiteCode == selectedVal.split('_')[1])[0].StandardCostPerKg;

        }
        else {
            artNumber = selectedVal;
            siteCode = $("#hdnSiteCode").val();
            allergen = aplGrid._data.filter(m => m.ArticleNumber == selectedVal);
            curCost = aplGrid._data.filter(m => m.ArticleNumber == selectedVal)[0].StandardCostPerKg;
        }
        var commaString = ",";

        moggrid._data.forEach(function (item, index) {
            //console.log("aplGrid");
            //console.log(aplGrid);
            //console.log("allergen");
            //console.log(allergen);
            if (item.MOGCode == MOGCode) {
                prevCost = item.CostPerUOM;
                item.ArticleNumber = artNumber;
                item.AllergensName = allergen[0].AllergensName;
                item.CostPerUOM = allergen[0].StandardCostPerKg;
                var qty = item.Quantity;
                var recipeQty = $("#inputquantity").val();
                var perc = (qty / recipeQty) * 100;
                item.Quantity = qty;
                item.IngredientPerc = perc.toFixed(2);
                item.TotalCost = qty * item.CostPerUOM;
                item.TotalCost = Utility.MathRound(item.TotalCost, 2);
                //curCost = item.TotalCost;

                //siteCode = aplGrid._data.filter(z=>z.MOGCode==MOGCode)[0].SiteCode;
                //if (level == 0)
                /*CalculateGrandTotal(level);*/
                //else
                //CalculateGrandTotal("");
            }

            commaString = index == 0 ? "" : ",";
            recipeAllergens = item.AllergensName == "None" ? recipeAllergens : recipeAllergens + commaString + item.AllergensName;
        });

        $(gridId + currentPopuatedMOGGrid + "").data('kendoGrid').refresh();
        CalculateGrandTotal(level);
        //console.log("siteCode radio")
        //console.log(siteCode)
        if (level == "") {
            //console.log("level radio")
            //console.log(level)
            var modelHistory = {
                "MOGCode": MOGCode,
                "PreviousArticleNumber": MOGSelectedArticleNumber,
                "PreviousCost": MOGSelectedCost,
                //"PreviousCost": prevCost,
                "CurrentArticleNumber": artNumber,
                "CurrentCost": curCost,
                "UpdatedDate": Utility.CurrentDate(),
                "UpdatedBy": user.UserId,
                "SimulationCode": $("#SimulationCode").val(),
                "RegionID": $("#NewSimRegion").val(),
                "SiteID": $("#NewSimSiteByRegion").val(),
                "RecipeCode": $("#copyRecipe0").val(),
                "DishCode": $("#hdnDishCode").val(),
                "ItemID": $("#hdnItemId").val(),
                "DayPartCode": $("#hdnDayPartCode").val(),
                "SiteCode": $("#hdnSiteCode").val(),
                "SelectedMOGSiteCode": siteCode
            };
            //console.log("MOGSelectedCost")
            //console.log(MOGSelectedCost)
            //if (modelHistoryData.length > 0) {
            var filtermodelHistoryData = modelHistoryData.filter(ds => ds.MOGCode == MOGCode);
            if (filtermodelHistoryData != undefined && filtermodelHistoryData.length > 0) {

                modelHistoryData.forEach(function (item, index) {
                    if (item.MOGCode == MOGCode && item.SimulationCode == $("#SimulationCode").val() && item.RecipeCode == $("#copyRecipe0").val()) {

                        item.PreviousArticleNumber = MOGSelectedArticleNumber;
                        item.PreviousCost = MOGSelectedCost;
                        item.CurrentArticleNumber = artNumber;
                        item.CurrentCost = curCost;
                        item.UpdatedDate = Utility.CurrentDate();
                        item.UpdatedBy = user.UserId;
                        item.SelectedMOGSiteCode = siteCode;
                        //"SimulationCode": $("#SimulationCode").val();
                        //"RegionID": $("#NewSimRegion").val();
                        //"SiteID": $("#NewSimSiteByRegion").val();
                        //"RecipeCode": $("#copyRecipe0").val();
                        //"DishCode": $("#hdnDishCode").val();
                        //"ItemId": $("#hdnItemId").val();
                        //"DayPartCode": $("#hdnDayPartCode").val();
                    }
                });
            }
            else {
                modelHistoryData.push(modelHistory);
            }
            //}

            console.log("modelHistoryData");
            console.log(modelHistoryData)
        }
        //TODO
        //8th March
        //HttpClient.MakeRequest(CookBookMasters.SaveSimAPLHistory, function (result) {

        //}, { data: modelHistory},false)
        //console.log("recipeAllergens")
        //console.log(recipeAllergens)
        //bindRecipeAllergensName(recipeAllergens);
    }
    else {
        toastr.error("Please select at least one APL.");
        return false;
    }
}
function bindRecipeAllergensName(recipeAllergens) {
    var list = recipeAllergens.split(',');
    var arr = [];
    $.each(list, function (i, val) {
        console.log(val);
        if (val !== undefined && val.trim() !== "" && arr.includes(val.trim())) {
            console.log("exist" + val.trim())
        }
        else if (val != "null") {
            arr.push(val.trim());
            console.log("notexist" + val.trim())
        }
    });
    var allergenString = arr.toString();
    //if (allergenString.match(/,.*,/)) { // Check if there are 2 commas
    //    allergenString = allergenString.replace(',', ''); // Remove the first one
    //}
    $("#recipeAllergens" + currentPopuatedMOGGrid + "").html(showAllergens(allergenString));

}
//function populateMOGGrid(recipeID) {

//    Utility.Loading();
//    var gridVariable = $("#gridMOG");
//    gridVariable.html("");
//    if (user.SectorNumber == '20') {
//        gridVariable.kendoGrid({
//            toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
//            groupable: false,
//            editable: true,
//            //navigatable: true,
//            columns: [
//                {
//                    field: "MOG_ID", title: "MOG", width: "120px",
//                    attributes: {
//                        style: "text-align: left; font-weight:normal"
//                    },
//                    template: function (dataItem) {
//                        var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
//                            '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
//                        return html;
//                    },
//                    editor: function (container, options) {
//                        $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
//                            .appendTo(container)
//                            .kendoDropDownList({
//                                index: 0,
//                                filter: "contains",
//                                dataTextField: "MOGName",
//                                dataValueField: "MOG_ID",
//                                autoBind: true,
//                                dataSource: refreshMOGDropDownData(options.model.MOGName),
//                                value: options.field,
//                                change: onMOGDDLChange,
//                            });

//                    }
//                },
//                {
//                    field: "UOMName", title: "UOM", width: "50px",
//                    headerAttributes: {
//                        style: "text-align: center;"
//                    },
//                    attributes: {

//                        class: 'uomname',
//                        style: "text-align: center; font-weight:normal"
//                    },
//                },
//                //{
//                //    field: "UOMCode", title: "UOM", width: "120px",
//                //    attributes: {
//                //        style: "text-align: left; font-weight:normal",
//                //        class: ''
//                //    },
//                //    template: function (dataItem) {
//                //        var html = `  <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
//                //            '<span class="uomname" style="margin-left:8px">' + kendo.htmlEncode(dataItem.UOMName) + '</span>';
//                //        return html;
//                //    },
//                //    editor: function (container, options) {
//                //        $('<input class="mogTemplate" id="ddlMOGUOM" text="' + options.model.UOMName + '" data-text-field="text" data-value-field="UOMCode" data-bind="value:' + options.field + '"/>')
//                //            .appendTo(container)
//                //            .kendoDropDownList({
//                //                index: 0,
//                //                filter: "contains",
//                //                dataTextField: "text",
//                //                dataValueField: "UOMCode",
//                //                autoBind: true,
//                //                dataSource: moguomdata,
//                //               // dataSource: refreshMOGUOMDropDownData(options.model.UOMName),
//                //                value: options.field,
//                //                change: onMOGUOMDDLChange,
//                //            });

//                //    }
//                //},

//                {
//                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
//                    headerAttributes: {
//                        style: "text-align: center;width:35px;"
//                    },

//                    template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,null)"/>',
//                    attributes: {

//                        style: "text-align: center; font-weight:normal;width:35px;"
//                    },
//                },
//                {
//                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },
//                    attributes: {
//                        class: "uomcost",

//                        style: "text-align: right; font-weight:normal"
//                    },
//                },
//                {
//                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },
//                    attributes: {
//                        class: "totalcost",
//                        style: "text-align: right; font-weight:normal"
//                    },
//                },
//                {
//                    title: "Delete",
//                    width: "60px",
//                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
//                    headerAttributes: {
//                        style: "text-align: center;"
//                    },
//                    attributes: {
//                        style: "text-align: center; font-weight:normal;"
//                    },
//                    command: [
//                        {
//                            name: 'Delete',
//                            text: "Delete",
//                            //iconClass: "fa fa-trash-alt",
//                            click: function (e) {
//                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
//                                    return false;
//                                }

//                                var gridObj = $("#gridMOG").data("kendoGrid");
//                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
//                                if (selectedRow.IsMajor) {
//                                    Utility.Page_Alert_Save("This is a major Ingredient. Do you still want to remove it from the Recipe?", "Delete Confirmation", "Yes", "No",
//                                        function () {
//                                            gridObj.dataSource._data.remove(selectedRow);
//                                            CalculateGrandTotal('');
//                                            return true;
//                                        },
//                                        function () {
//                                        }
//                                    );
//                                }
//                                else {
//                                    gridObj.dataSource._data.remove(selectedRow);
//                                    CalculateGrandTotal('');
//                                    return true;
//                                }

//                            }
//                        },
//                        {
//                            name: 'View APL',
//                            click: function (e) {

//                                var gridObj = $("#gridMOG").data("kendoGrid");
//                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
//                                datamodel = tr;
//                                console.log("MOG APL");
//                                console.log(tr);
//                                MOGName = tr.MOGName;

//                                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
//                                $("#windowEditMOGWiseAPL").kendoWindow({
//                                    animation: false
//                                });
//                                $(".k-overlay").css("display", "block");
//                                $(".k-overlay").css("opacity", "0.5");
//                                dialog.open();
//                                dialog.center();
//                                dialog.title("MOG APL Details - " + MOGName);
//                                MOGCode = tr.MOGCode;
//                                mogWiseAPLMasterdataSource = [];
//                                getMOGWiseAPLMasterData();
//                                populateAPLMasterGrid();
//                                return true;
//                            }
//                        }
//                    ],
//                }
//            ],
//            dataSource: {
//                transport: {
//                    read: function (options) {

//                        var varCodes = "";

//                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
//                            Utility.UnLoading();

//                            if (result != null) {
//                                var obj = result;
//                                mogtotal = 0.0;
//                                $.each(obj, function (key, value) {
//                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
//                                });

//                                $("#totmog").text(mogtotal.toFixed(2));
//                                //$("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()));
//                                var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
//                                $("#ToTIngredients").text(Utility.MathRound(tot, 2));
//                                options.success(result);
//                            }
//                            else {
//                                options.success("");
//                            }

//                            if (result.length > 0) {
//                                $("#gridMOG").css("display", "block");
//                                $("#emptymog").css("display", "none");
//                            } else {
//                                $("#gridMOG").css("display", "none");
//                                $("#emptymog").css("display", "block");
//                            }
//                        },
//                            {
//                                recipeID: recipeID
//                            }
//                            , true);
//                    }
//                },
//                schema: {
//                    model: {
//                        id: "MOG_ID",
//                        fields: {
//                            MOG_ID: { type: "number", validation: { required: true } },
//                            MOGName: { type: "string", validation: { required: true } },
//                            Quantity: { editable: false },
//                            UOMName: { editable: false },
//                            Quantity: { editable: false },
//                            IngredientPerc: { editable: false },
//                            CostPerUOM: { editable: false },
//                            CostPerKG: { editable: false },
//                            TotalCost: { editable: false }
//                        }
//                    }
//                }
//            },
//            columnResize: function (e) {
//                var grid = gridVariable.data("kendoGrid");
//                e.preventDefault();
//            },
//            dataBound: function (e) {

//                var grid = e.sender;
//                var items = e.sender.items();
//                items.each(function (e) {
//                    var dataItem = grid.dataItem(this);
//                    var ddt = $(this).find('.mogTemplate');
//                    $(ddt).kendoDropDownList({
//                        index: 0,
//                        value: dataItem.value,
//                        dataSource: baserecipedata,
//                        dataTextField: "MOGName",
//                        dataValueField: "MOG_ID",
//                        change: onMOGDDLChange
//                    });
//                    //ddt.find(".k-input").val(dataItem.MOGName);

//                    $(this).find('.inputmogqty').val(dataItem.Quantity);
//                });


//            },
//            change: function (e) {
//                if (e.action == "itemchange") {
//                    e.items[0].dirtyFields = e.items[0].dirtyFields || {};
//                    e.items[0].dirtyFields[e.field] = true;
//                }
//            },
//        });
//    }
//    else {
//        gridVariable.kendoGrid({
//            toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
//            groupable: false,
//            editable: true,
//            //navigatable: true,
//            columns: [
//                {
//                    field: "MOG_ID", title: "MOG", width: "120px",
//                    attributes: {
//                        style: "text-align: left; font-weight:normal"
//                    },
//                    template: function (dataItem) {
//                        var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
//                            '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
//                        return html;
//                    },
//                    editor: function (container, options) {
//                        $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
//                            .appendTo(container)
//                            .kendoDropDownList({
//                                index: 0,
//                                filter: "contains",
//                                dataTextField: "MOGName",
//                                dataValueField: "MOG_ID",
//                                autoBind: true,
//                                dataSource: refreshMOGDropDownData(options.model.MOGName),
//                                value: options.field,
//                                change: onMOGDDLChange,
//                            });

//                    }
//                },
//                {
//                    field: "UOMName", title: "UOM", width: "50px",
//                    headerAttributes: {
//                        style: "text-align: center;"
//                    },
//                    attributes: {

//                        class: 'uomname',
//                        style: "text-align: center; font-weight:normal"
//                    },
//                },

//                {
//                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
//                    headerAttributes: {
//                        style: "text-align: center;width:35px;"
//                    },

//                    template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,null)"/>',
//                    attributes: {

//                        style: "text-align: center; font-weight:normal;width:35px;"
//                    },
//                },
//                {
//                    field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
//                    headerAttributes: {
//                        style: "text-align: center;"
//                    },
//                    attributes: {
//                        class: "major",
//                        style: "text-align: center; font-weight:normal"
//                    },
//                    template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
//                },
//                {
//                    field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
//                    headerAttributes: {
//                        style: "text-align: center;"
//                    },
//                    attributes: {
//                        class: "minor",
//                        style: "text-align: center; font-weight:normal"
//                    },
//                    template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
//                },
//                {
//                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },
//                    attributes: {
//                        class: "uomcost",

//                        style: "text-align: right; font-weight:normal"
//                    },
//                },
//                {
//                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },
//                    attributes: {
//                        class: "totalcost",
//                        style: "text-align: right; font-weight:normal"
//                    },
//                },
//                {
//                    title: "Delete",
//                    width: "60px",
//                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
//                    headerAttributes: {
//                        style: "text-align: center;"
//                    },
//                    attributes: {
//                        style: "text-align: center; font-weight:normal;"
//                    },
//                    command: [
//                        {
//                            name: 'Delete',
//                            text: "Delete",
//                            //iconClass: "fa fa-trash-alt",
//                            click: function (e) {
//                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
//                                    return false;
//                                }
//                                var gridObj = $("#gridMOG").data("kendoGrid");
//                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
//                                if (selectedRow.IsMajor) {
//                                    Utility.Page_Alert_Save("This is a major Ingredient. Do you still want to remove it from the Recipe?", "Delete Confirmation", "Yes", "No",
//                                        function () {
//                                            gridObj.dataSource._data.remove(selectedRow);
//                                            CalculateGrandTotal('');
//                                            return true;
//                                        },
//                                        function () {
//                                        }
//                                    );
//                                }
//                                else {
//                                    gridObj.dataSource._data.remove(selectedRow);
//                                    CalculateGrandTotal('');
//                                    return true;
//                                }
//                            }
//                        },
//                        {
//                            name: 'View APL',
//                            click: function (e) {

//                                var gridObj = $("#gridMOG").data("kendoGrid");
//                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
//                                datamodel = tr;
//                                console.log("MOG APL");
//                                console.log(tr);
//                                MOGName = tr.MOGName;

//                                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
//                                $("#windowEditMOGWiseAPL").kendoWindow({
//                                    animation: false
//                                });
//                                $(".k-overlay").css("display", "block");
//                                $(".k-overlay").css("opacity", "0.5");
//                                dialog.open();
//                                dialog.center();
//                                dialog.title("MOG APL Details - " + MOGName);
//                                MOGCode = tr.MOGCode;
//                                mogWiseAPLMasterdataSource = [];
//                                getMOGWiseAPLMasterData();
//                                populateAPLMasterGrid();
//                                return true;
//                            }
//                        }
//                    ],
//                }
//            ],
//            dataSource: {
//                transport: {
//                    read: function (options) {

//                        var varCodes = "";

//                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
//                            Utility.UnLoading();

//                            if (result != null) {
//                                var obj = result;
//                                mogtotal = 0.0;
//                                $.each(obj, function (key, value) {
//                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
//                                });

//                                $("#totmog").text(mogtotal.toFixed(2));
//                                //$("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()));
//                                var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
//                                $("#ToTIngredients").text(Utility.MathRound(tot, 2));
//                                options.success(result);
//                            }
//                            else {
//                                options.success("");
//                            }

//                            if (result.length > 0) {
//                                $("#gridMOG").css("display", "block");
//                                $("#emptymog").css("display", "none");
//                            } else {
//                                $("#gridMOG").css("display", "none");
//                                $("#emptymog").css("display", "block");
//                            }
//                        },
//                            {
//                                recipeID: recipeID
//                            }
//                            , true);
//                    }
//                },
//                schema: {
//                    model: {
//                        id: "MOG_ID",
//                        fields: {
//                            MOG_ID: { type: "number", validation: { required: true } },
//                            MOGName: { type: "string", validation: { required: true } },
//                            Quantity: { editable: false },
//                            UOMName: { editable: false },
//                            Quantity: { editable: false },
//                            IngredientPerc: { editable: false },
//                            CostPerUOM: { editable: false },
//                            CostPerKG: { editable: false },
//                            TotalCost: { editable: false }
//                        }
//                    }
//                }
//            },
//            columnResize: function (e) {
//                var grid = gridVariable.data("kendoGrid");
//                e.preventDefault();
//            },
//            dataBound: function (e) {

//                var grid = e.sender;
//                var items = e.sender.items();
//                items.each(function (e) {
//                    var dataItem = grid.dataItem(this);
//                    var ddt = $(this).find('.mogTemplate');
//                    $(ddt).kendoDropDownList({
//                        index: 0,
//                        value: dataItem.value,
//                        dataSource: baserecipedata,
//                        dataTextField: "MOGName",
//                        dataValueField: "MOG_ID",
//                        change: onMOGDDLChange
//                    });
//                    //ddt.find(".k-input").val(dataItem.MOGName);

//                    $(this).find('.inputmogqty').val(dataItem.Quantity);
//                });


//            },
//            change: function (e) {
//                if (e.action == "itemchange") {
//                    e.items[0].dirtyFields = e.items[0].dirtyFields || {};
//                    e.items[0].dirtyFields[e.field] = true;
//                }
//            },
//        });
//    }

//    // cancelChangesConfirmation('gridMOG');
//    var toolbar = $("#gridMOG").find(".k-grid-toolbar");
//    toolbar.find(".k-add").remove();
//    toolbar.find(".k-cancel").remove();
//    toolbar.addClass("gridToolbarShift");
//    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
//    toolbar.find(".k-grid-add").addClass("gridButton")
//    $("#gridMOG .k-grid-content").addClass("gridInside");
//    toolbar.find(".k-grid-clearall").addClass("gridButton");
//    $('#gridMOG a.k-grid-clearall').click(function (e) {
//         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
//            function () {
//                $("#gridMOG").data('kendoGrid').dataSource.data([]);
//                showHideGrid('gridMOG', 'emptymog', 'mup_gridBaseRecipe');
//                gridIdin = 'gridMOG';
//            },
//            function () {
//            }
//        );

//    });

//    $(".k-grid-cancel-changes").click(function (e) {
//         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
//            function () {
//                // var grid = $("#" + gridIdin + "").data("kendoGrid");
//                if (gridIdin == 'gridBaseRecipe') {
//                    populateBaseRecipeGrid(resetID);
//                    return;
//                } else if (gridIdin == 'gridMOG') {
//                    populateMOGGrid(resetID)
//                    return;
//                }
//                // grid.cancelChanges();
//            },
//            function () {
//            }
//        );

//    });
//}

function refreshMOGDropDownData(x) {

    if (mogdataList != null && mogdataList.length > 0) {
        var mogGridData = $("#gridMOG").data('kendoGrid').dataSource._data;

        if (mogGridData != null && mogGridData.length > 0) {
            mogGridData = mogGridData.filter(m => m.MOGName != "" && m.MOGName != x);
            mogdata = mogdataList;
            var mogdataListtemp = mogdataList;
            mogdataListtemp = mogdataListtemp.filter(m => m.MOG_Code != "");
            mogdataListtemp.forEach(function (item, index) {
                for (var i = 0; i < mogGridData.length; i++) {
                    if (mogGridData[i].MOGCode !== "" && item.MOG_Code === mogGridData[i].MOGCode) {
                        // mogdata.splice(index, 1);
                        mogdata = mogdata.filter(mog => mog.MOG_Code !== item.MOG_Code);
                        break;
                    }
                }
                return true;
            });
        }
    }
    return mogdata.filter(m => m.IsActive == true);
};

function refreshMOGUOMDropDownData(x) {
    return moguomdata.filter(m => m.text != x);
};


function populateDishDropdown() {
    $("#inputdish").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: dishdata,
        index: 0,
    });
}

function populateUOMDropdown_Bulk() {
    $("#uom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}

function populateUOMDropdown() {

    $("#inputuom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}

function populateMOGUOMDropdown() {

    $("#inputmoguom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: moguomdata,
        index: 0,
    });
}

function populateBaseDropdown_Bulk() {
    $("#base").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateBaseDropdown() {
    $("#inputbase").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}

function onEditRecipeGrid(tr, isSavedData) {

    datamodel = tr;
    RecipeName = tr.Name;
    populateUOMDropdown();
    recipeCategoryHTMLPopulateOnEditRecipe("", tr);
    isBaseRecipe = tr.IsBase;
    RecipeCode = tr.RecipeCode;
    if (isSavedData) {
        $("#windowEdit0").hide();
    }
    else {
        $("#mainContent").hide();
    }
    $("#windowEdit").show();


    populateBaseDropdown();
    resetID = tr.ID;
    populateBaseRecipeGrid(tr.ID);
    populateMOGGrid(tr.ID, tr.RecipeCode);
    $("#hiddenRecipeID").val(tr.ID);
    $("#recipeid").val(tr.ID);

    $("#inputrecipecode").val(tr.RecipeCode);
    $("#inputrecipename").val(tr.Name);
    $("#inputrecipealiasname").val(tr.RecipeAlias);
    // $("#inputrecipecum").data("kendoDropDownList").value(tr.RecipeUOMCategory);
    $("#inputuom").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity").val(tr.Quantity);
    if (user.SectorNumber == "20" || user.SectorNumber == "10") {
        $('#inputquantity').removeAttr("disabled");
        // $("#inputbase").data('kendoDropDownList').value("No");
    }
    else {
        var inputwtperunituom = $("#inputuom").data("kendoDropDownList");
        inputwtperunituom.enable(false);
        $('#inputquantity').attr('disabled', 'disabled');
    }
    if (tr.IsBase == true)
        $("#inputbase").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase").data('kendoDropDownList').value("No");
    $('#grandTotal').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    var editor = $("#inputinstruction").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
}

function recipeCategoryHTMLPopulateOnEditRecipe(level, tr) {
    if (user.SectorNumber == "20" || user.SectorNumber == "10") {
        populateRecipeCategoryUOMDropdown(level);
        populateWtPerUnitUOMDropdown(level);
        populateGravyWtPerUnitUOMDropdown(level);
        if (tr.RecipeUOMCategory)
            $("#inputrecipecum" + level + "").data('kendoDropDownList').value(tr.RecipeUOMCategory);
        var recipeCategory = tr.RecipeUOMCategory;
        if (tr.RecipeUOMCategory == undefined || tr.RecipeUOMCategory == null || tr.RecipeUOMCategory == "") {
            recipeCategory = "UOM-00001";
            $("#inputrecipecum" + level + "").data('kendoDropDownList').value("UOM-00001");
        }
        OnRecipeUOMCategoryChange(recipeCategory, level);
        if (tr.WeightPerUnit)
            $("#inputwtperunit" + level + "").val(tr.WeightPerUnit);
        if (tr.WeightPerUnitGravy)
            $("#inputgravywtperunit" + level + "").val(tr.WeightPerUnitGravy);
        if (tr.WeightPerUnitUOM)
            $("#inputwtperunituom" + level + "").data("kendoDropDownList").value(tr.WeightPerUnitUOM);
        if (tr.WeightPerUnitUOMGravy)
            $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value(tr.WeightPerUnitUOMGravy);
    }
}

function recipeGridEdit(e) {
    Utility.Loading();
    setTimeout(function () {
        HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {

            var dataSource = data;

            baserecipedata = [];
            baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].CostPerUOM == null) {
                    dataSource[i].CostPerUOM = 0;
                }
                if (dataSource[i].CostPerKG == null) {
                    dataSource[i].CostPerKG = 0;
                }
                if (dataSource[i].TotalCost == null) {
                    dataSource[i].TotalCost = 0;
                }
                if (dataSource[i].Quantity == null) {
                    dataSource[i].Quantity = 0;
                }
                if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });
                else
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });

            }
            baserecipedatafiltered = baserecipedata;
            basedataList = baserecipedata;
        }, { condition: "Base", DishCode: "0" }, true);

        var gridObj = $("#gridRecipe").data("kendoGrid");
        var tr = gridObj.dataItem($(e).closest("tr")); // 'e' is the HTML element <a>
        if (!tr.IsActive) {
            return;
        }
        Utility.Loading();
        setTimeout(function () {
            $("#success").css("display", "none");
            $("#error").css("display", "none");

            // var tr = gridObj.dataItem($(e).closest("tr")); // 'e' is the HTML element <a>
            onEditRecipeGrid(tr, false);
            return true;
            Utility.UnLoading();
        }, 2000);
    }, 2000);
}

function onSaveRecipeSetData() {
    var gridObj = $("#gridRecipe").data("kendoGrid");
    if (gridObj.dataSource != undefined && gridObj.dataSource._total > 0) {
        var tr = gridObj.dataSource._data[parseInt(gridObj.dataSource._total) - 1];
        onEditRecipeGrid(tr, true);
    }
    return true;
}
//Hare Ram
function onBRDDLChange(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    //baserecipedatafiltered = baserecipedata.filter(function (recipeObj) {
    //    return recipeObj.BaseRecipe_ID != e.sender.value();
    //});

    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    // $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onBRDDLChange0(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe0', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe0").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(0);

};

function onBRDDLChangex(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipex', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipex").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipex").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('x');

};


function onBRDDLChange1(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe1', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe1").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(1);;
};
function onBRDDLChange2(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe2', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe2").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(2);
};

function onBRDDLChange3(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe3', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe3").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(3);
};

function onBRDDLChange4(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe4', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe4").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(4);
};

function onBRDDLChange5(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe5', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe5").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('');
};


function populateBaseRecipeGridCopy(recipeID) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe0");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [
            //    { name: "cancel", text: "Reset" }, {
            //    name: "create", text: "Add"
            //},
            //{
            //    name: "new", text: "Create New"
            //},

        ],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>';
                        //+ `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails1(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input tabindex="0" id="ddlBaseRecipe0" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange0,
                            //open: function (e) { e.preventDefault();}
                        });
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,0)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    {
                        name: 'Delete',
                        text: "Delete",

                        click: function (e) {
                            if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                return false;
                            }
                            var gridObj = $("#gridBaseRecipe0").data("kendoGrid");
                            var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            gridObj.dataSource._data.remove(selectedRow);
                            CalculateGrandTotal(0);
                            return true;
                        }
                    }
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe0").css("display", "block");
                            $("#emptybr0").css("display", "none");
                            CalculateGrandTotal(0);
                        } else {
                            $("#gridBaseRecipe0").css("display", "none");
                            $("#emptybr0").css("display", "block");
                        }

                    },
                        {
                            //recipeCode: recipeCode,
                            //SiteCode: $("#NewSimSiteByRegion").val(),
                            //17-05-2022 Base Recipe Issue
                            SiteCode: $("#hdnSiteCode").val(),
                            recipeID: recipeID
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe0');
    var toolbar = $("#gridBaseRecipe1").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe1 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");
        $("#windowEditx").kendoWindow({
            animation: false
        });
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}
function populateBaseRecipeGrid0() {

    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe0");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [
            { name: "clearall", text: "Clear All" },
            { name: "cancel", text: "Reset" }, {
                name: "create", text: "Add"
            }
        ],
        groupable: false,
        editable: true,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails1(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input tabindex="0" id="ddlBaseRecipe0" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange0,
                            //open: function (e) { e.preventDefault();}
                        });
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,0)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    {
                        name: 'Delete',
                        text: "Delete",

                        click: function (e) {
                            if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                return false;
                            }
                            var gridObj = $("#gridBaseRecipe0").data("kendoGrid");
                            var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            gridObj.dataSource._data.remove(selectedRow);
                            CalculateGrandTotal(0);
                            return true;
                        }
                    }
                ],
            }
        ],
        dataSource: {

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false }, IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },

                        TotalCost: { editable: false }
                    }
                }
            }
        },

        columnResize: function (e) {

            e.preventDefault();
        },

        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
                $(this).next().focus();
            });



        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe0');
    var toolbar = $("#gridBaseRecipe0").find(".k-grid-toolbar");

    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe0 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
    toolbar.find(".k-grid-clearall").addClass("gridButton");
    $('#gridBaseRecipe0 a.k-grid-clearall').click(function (e) {
         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                $("#gridBaseRecipe0").data('kendoGrid').dataSource.data([]);
                showHideGrid('gridBaseRecipe0', 'emptybr0', 'bup_gridBaseRecipe0');
            },
            function () {
            }
        );

    });
}
function populateBaseRecipeGridx() {

    Utility.Loading();
    var gridVariable = $("#gridBaseRecipex");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, {
            name: "create", text: "Add"
        },

        ],
        groupable: false,
        editable: true,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetailsx(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input tabindex="0" id="ddlBaseRecipex" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChangex,
                            //open: function (e) { e.preventDefault();}
                        });
                }
            },

            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                template: '<input type="number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,' + 'x' + ')"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    {
                        name: 'Delete',
                        text: "Delete",

                        click: function (e) {
                            if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                return false;
                            }
                            var gridObj = $("#gridBaseRecipex").data("kendoGrid");
                            var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            gridObj.dataSource._data.remove(selectedRow);
                            CalculateGrandTotal('x');
                            return true;
                        }
                    }
                ],
            }
        ],
        dataSource: {

            schema: {
                model: {
                    id: "ID",

                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }

                }
            }
        },

        columnResize: function (e) {

            e.preventDefault();
        },

        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChangex
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
                $(this).next().focus();
            });

            $('.k-grid-add').unbind("click");

            $('.k-grid-add').bind("click", function (e) {
                // var di = grid.dataItem(e);
                console.log(e);
                console.log(items);
                console.log("Handle the add button click")
            });

        },
        change: function (e) {
        },
    });

    cancelChangesConfirmation('gridBaseRecipex');
    var toolbar = $("#gridBaseRecipex").find(".k-grid-toolbar");

    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipex .k-grid-content").addClass("gridInside");
}

function onMOGDDLChange0(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG0', false)) {
        toastr.error("MOG is already selected.");
        var dropdownlist = $("#ddlMOG0").data("kendoDropDownList");
        dropdownlist.select("");
        //  dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(0);

};
function onMOGDDLChangex(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOGx', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOGx").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOGx").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('x');
};
function populateUOMDropdown0() {
    $("#inputuom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 1
    });
}

function populateWtPerUnitUOMDropdown0() {
    $("#inputwtperunituom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}

function populateGravyWtPerUnitUOMDropdown0() {
    $("#inputgravywtperunituom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}

function populateRecipeCategoryUOMDropdown0() {
    $("#inputrecipecum0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: recipecategoryuomdata,
        index: 1,
        change: function (e) {
            OnRecipeUOMCategoryChange(e.sender.dataItem().UOMCode, "0")

        }
    });
}

function populateWtPerUnitUOMDropdown(level) {
    $("#inputwtperunituom" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}

function populateGravyWtPerUnitUOMDropdown(level) {
    $("#inputgravywtperunituom" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}

function populateRecipeCategoryUOMDropdown(level) {
    $("#inputrecipecum" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: recipecategoryuomdata,
        index: 1,
        change: function (e) {
            OnRecipeUOMCategoryChange(e.sender.dataItem().UOMCode, level)

        }
    });
}

function OnRecipeUOMCategoryChange(recipeCategory, level) {
    $("#inputwtperunituom" + level + "").closest(".k-widget").hide();
    $("#inputgravywtperunituom" + level + "").closest(".k-widget").hide();
    $("#inputuom" + level + "").closest(".k-widget").hide();
    $("#linputwtperunituom" + level + "").text("-");
    $("#linputgravywtperunituom" + level + "").text("-");
    $("#linputuom" + level + "").text("-");

    $("#inputwtperunit" + level + "").val("");

    if ($("#inputuom" + level + "").data("kendoDropDownList") != undefined) {
        $("#inputuom" + level + "").data("kendoDropDownList").value("Select");
    }
    $("#inputgravywtperunit" + level + "").val("");
    $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("Select");
    $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("Select");
    var inputwtperunituom0 = $("#inputwtperunituom" + level + "").data("kendoDropDownList");
    inputwtperunituom0.enable(false);
    var inputgravywtperunituom0 = $("#inputgravywtperunituom" + level + "").data("kendoDropDownList");
    inputgravywtperunituom0.enable(false);

    if (recipeCategory == "UOM-00005" || recipeCategory == "UOM-00006") {
        var rUOM = recipeCategory == "UOM-00005" ? "1" : "2";
        var rUOMText = uomdata.find(m => m.UOM_ID == rUOM).UOMName;
        $("#inputwtperunit" + level + "").prop("disabled", true);
        $("#inputgravywtperunit" + level + "").prop("disabled", true);
        $("#inputuom" + level + "").data("kendoDropDownList").value(rUOM);
        $("#linputuom" + level + "").text(rUOMText);
        var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputwtperunituom" + level + "").text("-");
        $("#linputgravywtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00007") {
        $("#inputwtperunit" + level + "").prop("disabled", false);
        $("#inputgravywtperunit" + level + "").prop("disabled", true);
        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
        var rUOMText = uomdata.find(m => m.UOM_ID == 3).UOMName;
        $("#linputuom" + level + "").text(rUOMText);
        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").UOMName;
        $("#linputwtperunituom" + level + "").text(rUText);
        var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputgravywtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00009") {
        $("#inputwtperunit" + level + "").prop("disabled", true);
        $("#inputgravywtperunit" + level + "").prop("disabled", false);
        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
        var rUOMText = uomdata.find(m => m.UOM_ID == 3).UOMName;
        $("#linputuom" + level + "").text(rUOMText);
        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").UOMName;
        $("#linputgravywtperunituom" + level + "").text(rUText);
        var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputwtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00008") {
        $("#inputwtperunit" + level + "").prop("disabled", false);
        $("#inputgravywtperunit" + level + "").prop("disabled", false);
        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").UOMName;
        $("#linputgravywtperunituom" + level + "").text(rUText);
        $("#linputwtperunituom" + level + "").text(rUText);
        var rUOMText = uomdata.find(m => m.UOM_ID == 3).UOMName;
        $("#linputuom" + level + "").text(rUOMText);
    }
};

function populateUOMDropdownx() {
    $("#inputuomx").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 1,
    });
}
function populateBaseDropdown0() {
    $("#inputbase0").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateBaseDropdownx() {
    $("#inputbasex").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}


function calculateItemTotal0(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObjforBR = $("#gridBaseRecipe0.totalcost");
    var gtotalObjforMOG = $("#gridBaseRecipeMOG0.totalcost");
    var len = $('#gridBaseRecipe0.totalcost').length;
    var len1 = $('#gridBaseRecipeMOG0.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObjforBR[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    for (i = 0; i < len1; i++) {
        if (gtotalObjforMOG[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal0').html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#inputquantity0').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG0').html(Utility.AddCurrencySymbol(costPerKG, 2));
}
function mogtotalQTYDelete(level) {
    console.log("level")
    console.log(level)
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var gTotal = 0.0;
    var len1 = grid._data.length;
    for (i = 0; i < len1; i++) {
        var totalCostMOG = grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {

        $("#totmog").text(Utility.MathRound(gTotal, 2));
        //parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))).toFixed(2);
        //parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())));
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
        $("#ToTIngredients").text(Utility.MathRound(tot, 2));
    }

}

function BaseRecipetotalQTYDelete(level) {

    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");

    var gTotal = 0.0;
    var len1 = grid._data.length;

    for (i = 0; i < len1; i++) {
        var totalCostMOG = grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {

        $("#totBaseRecpQty").text(Utility.MathRound(gTotal, 2));
        // parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))).toFixed(2);
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))
        $("#ToTIngredients").text(tot);
    }


}
function CalculateGrandTotal(level) {
    var gtotalObjforBR = $("#gridBaseRecipe" + level + " .totalcost");
    var gtotalObjforMOG = $("#gridMOG" + level + " .totalcost");
    var len = gtotalObjforBR.length;
    var len1 = gtotalObjforMOG.length;
    var gTotal = 0.0;

    for (i = 0; i < len; i++) {
        var totalCostBR = gtotalObjforBR[i].innerText;
        totalCostBR = totalCostBR.substring(1).replace(',', '');
        if (totalCostBR == "") {
            continue;
        }
        gTotal += Number(totalCostBR);
        //console.log("gTotal1")
        //console.log(gTotal)
    }
    for (i = 0; i < len1; i++) {
        var totalCostMOG = gtotalObjforMOG[i].innerText;
        totalCostMOG = totalCostMOG.substring(1).replace(',', '');
        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
        //console.log("gTotal2")
        //console.log(gTotal)
    }

    if (!isNaN(gTotal)) {
        $("#grandTotal" + level).html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#inputquantity' + level).val();
    var costPerKG = gTotal / Number(qty);
    var newres = Utility.Currency + twodecimalWithoutRound(costPerKG);
    var t = newres;
    newres = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 3)) : t;
    $('#grandCostPerKG' + level).html(newres);
    //$('#grandCostPerKG' + level).html(Utility.AddCurrencySymbol(costPerKG, 2));
    mogtotalQTYDelete(level);
    BaseRecipetotalQTYDelete(level);
}

function calculateItemTotalx(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipex").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotalx').html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#inputquantityx').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKGx').html(Utility.AddCurrencySymbol(costPerKG, 2));

}


function calculateItemTotalMOG0(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal0').html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#inputquantity0').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG0').html(Utility.AddCurrencySymbol(costPerKG, 2));
}

function calculateItemTotalMOGx(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOGx").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotalx').html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#inputquantityx').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKGx').html(Utility.AddCurrencySymbol(costPerKG, 2));
}



function populateBaseRecipeGrid5(recipeID) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe5");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [
            //{ name: "cancel", text: "Reset" }, {
            //     name: "create", text: "Add"
            //  },
            {
                name: "new", text: "Create New"
            },

        ],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails6(this)'></i>`;
                    return html;
                },
                //editor: function (container, options) {
                //    $('<input tabindex="0" id="ddlBaseRecipe5" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 1,
                //            filter: "contains",
                //            dataTextField: "BaseRecipeName",
                //            dataValueField: "BaseRecipe_ID",
                //            autoBind: true,
                //            dataSource: baserecipedata,
                //            value: options.field,
                //            change: onBRDDLChange5,
                //            //open: function (e) { e.preventDefault();}
                //        });
                //}
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                //    template: '<input type="number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,5)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;"
            //    },
            //    command: [
            //        {
            //            name: 'Delete',
            //            text: "Delete",

            //            click: function (e) {
            //                if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //                    return false;
            //                }
            //                var gridObj = $("#gridBaseRecipe5").data("kendoGrid");
            //                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                gridObj.dataSource._data.remove(selectedRow);
            //                CalculateGrandTotal(5);
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe5").css("display", "block");
                            $("#emptybr5").css("display", "none");
                        } else {
                            $("#gridBaseRecipe5").css("display", "none");
                            $("#emptybr5").css("display", "block");
                        }

                    },
                        {
                            recipeID: recipeID
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange5
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe5');
    var toolbar = $("#gridBaseRecipe5").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe5 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}

function populateBaseRecipeGrid4(recipeID) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe4");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" }, {
        //    name: "create", text: "Add"
        //},
        //{
        //    name: "new", text: "Create New"
        //},

        //],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails5(this)'></i>`;
                    return html;
                },
                //editor: function (container, options) {
                //    $('<input tabindex="0" id="ddlBaseRecipe4" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 1,
                //            filter: "contains",
                //            dataTextField: "BaseRecipeName",
                //            dataValueField: "BaseRecipe_ID",
                //            autoBind: true,
                //            dataSource: baserecipedata,
                //            value: options.field,
                //            change: onBRDDLChange4,
                //            //open: function (e) { e.preventDefault();}
                //        });
                //    }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                //       template: '<input type="number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,4)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;"
            //    },
            //    command: [
            //        {
            //            name: 'Delete',
            //            text: "Delete",

            //            click: function (e) {
            //                if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //                    return false;
            //                }
            //                var gridObj = $("#gridBaseRecipe4").data("kendoGrid");
            //                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                gridObj.dataSource._data.remove(selectedRow);
            //                CalculateGrandTotal(4);
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe4").css("display", "block");
                            $("#emptybr4").css("display", "none");
                        } else {
                            $("#gridBaseRecipe4").css("display", "none");
                            $("#emptybr4").css("display", "block");
                        }

                    },
                        {
                            recipeID: recipeID
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false }, IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange4
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe4');
    var toolbar = $("#gridBaseRecipe4").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe4 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}

function populateBaseRecipeGrid3(recipeID) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe3");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" }, {
        //    name: "create", text: "Add"
        //},
        //{
        //    name: "new", text: "Create New"
        //},

        //],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails4(this)'></i>`;
                    return html;
                },
                //editor: function (container, options) {
                //    $('<input tabindex="0" id="ddlBaseRecipe3" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 1,
                //            filter: "contains",
                //            dataTextField: "BaseRecipeName",
                //            dataValueField: "BaseRecipe_ID",
                //            autoBind: true,
                //            dataSource: baserecipedata,
                //            value: options.field,
                //            change: onBRDDLChange3,
                //            //open: function (e) { e.preventDefault();}
                //        });
                //}
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                //   template: '<input type="number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,3)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },


            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;"
            //    },
            //    command: [
            //        {
            //            name: 'Delete',
            //            text: "Delete",

            //            click: function (e) {
            //                if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //                    return false;
            //                }
            //                var gridObj = $("#gridBaseRecipe3").data("kendoGrid");
            //                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                gridObj.dataSource._data.remove(selectedRow);
            //                CalculateGrandTotal(3);
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe3").css("display", "block");
                            $("#emptybr3").css("display", "none");
                        } else {
                            $("#gridBaseRecipe3").css("display", "none");
                            $("#emptybr3").css("display", "block");
                        }

                    },
                        {
                            recipeID: recipeID
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false }, IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange2
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe3');
    var toolbar = $("#gridBaseRecipe3").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe3 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}

function populateBaseRecipeGrid2(recipeID) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe2");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" },
        ////    {
        ////    name: "create", text: "Add"
        ////},
        //{
        //    name: "new", text: "Create New"
        //},

        //],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails3(this)'></i>`;
                    return html;
                },
                //editor: function (container, options) {
                //    $('<input tabindex="0" id="ddlBaseRecipe2" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 1,
                //            filter: "contains",
                //            dataTextField: "BaseRecipeName",
                //            dataValueField: "BaseRecipe_ID",
                //            autoBind: true,
                //            dataSource: baserecipedata,
                //            value: options.field,
                //            change: onBRDDLChange2,
                //            //open: function (e) { e.preventDefault();}
                //        });
                //    }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                // template: '<input type="Number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,2)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;display:none;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;display:none"
            //    },
            //    command: [
            //        {
            //            name: 'Delete',
            //            text: "Delete",

            //            click: function (e) {
            //                if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //                    return false;
            //                }
            //                var gridObj = $("#gridBaseRecipe2").data("kendoGrid");
            //                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                gridObj.dataSource._data.remove(selectedRow);
            //                CalculateGrandTotal(2);
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe2").css("display", "block");
                            $("#emptybr2").css("display", "none");
                        } else {
                            $("#gridBaseRecipe2").css("display", "none");
                            $("#emptybr2").css("display", "block");
                        }

                    },
                        {
                            recipeID: recipeID
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false }, IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange2
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe2');
    var toolbar = $("#gridBaseRecipe2").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe2 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}

function populateBaseRecipeGrid1(recipeID) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe1");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [
            //    { name: "cancel", text: "Reset" }, {
            //    name: "create", text: "Add"
            //},
            //{
            //    name: "new", text: "Create New"
            //},

        ],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails2(this)'></i>`;
                    return html;
                },
                //editor: function (container, options) {
                //    $('<input tabindex="0" id="ddlBaseRecipe1" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 1,
                //            filter: "contains",
                //            dataTextField: "BaseRecipeName",
                //            dataValueField: "BaseRecipe_ID",
                //            autoBind: true,
                //            dataSource: baserecipedata,
                //            value: options.field,
                //            change: onBRDDLChange1,
                //            //open: function (e) { e.preventDefault();}
                //        });
                // }
            },

            {
                field: "UOMName", title: "UOM", width: "40px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "30px",
                //  template: '<input type="number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,1)" readonly/>',

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "IngredientPerc", title: "Major %", width: "40px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "40px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;"
            //    },
            //    command: [
            //        {
            //            name: 'Delete',
            //            text: "Delete",

            //            click: function (e) {
            //                if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //                    return false;
            //                }
            //                var gridObj = $("#gridBaseRecipe1").data("kendoGrid");
            //                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                gridObj.dataSource._data.remove(selectedRow);
            //                CalculateGrandTotal(1);
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe1").css("display", "block");
                            $("#emptybr1").css("display", "none");
                        } else {
                            $("#gridBaseRecipe1").css("display", "none");
                            $("#emptybr1").css("display", "block");
                        }

                    },
                        {
                            recipeID: recipeID
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe1');
    var toolbar = $("#gridBaseRecipe1").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe1 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");
        $("#windowEditx").kendoWindow({
            animation: false
        });
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}


function onMOGDDLChange5(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG5', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG5").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(5);
};
function onMOGDDLChange4(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG4', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG4").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(4)
};
function onMOGDDLChange3(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG3', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG3").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(3)
};
function onMOGDDLChange2(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG2', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG2").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(2)
};
function onMOGDDLChange1(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG1', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG1").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal(1);
};
function populateMOGGrid0(recipeID, recipeCode) {


    if (user.SectorNumber == "20") {

        Utility.Loading();
        var gridVariable = $("#gridMOG0");
        gridVariable.html("");
        gridVariable.kendoGrid({
            //toolbar: [{ name: "cancel", text: "Reset" }],
            toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,
            //  navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: true,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: mogdata,
                                value: options.field,
                                change: onMOGDDLChange0,
                            });
                        // container.find(".k-input")[0].text = options.model.MOGName;
                    }
                },
                {
                    field: "AllergensName", title: "Allergens", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "AllergensName",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                //{
                //    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "si",
                //        style: "text-align: center; font-weight:normal"
                //    }
                //},

                {
                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },

                    template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,0)"/>',
                    attributes: {

                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                //{

                //    field: "Quantity", title: "Site Qty", width: "50px",
                //    template: '# if (IsMajor == true) {#<div><input type="number"  class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" /></div>#} else {#<div><input class="inputmogqty" type="number"   name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',


                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //},

                //{
                //    field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "change",
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                //},


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                //{
                //    field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                //    headerAttributes: {

                //        style: "text-align: right"
                //    },
                //    attributes: {
                //        class: "regiontotalcost",
                //        style: "text-align: right; font-weight:normal"
                //    },
                //},
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },

                {
                    title: "Action",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridMOG0").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                gridObj.dataSource._data.remove(selectedRow);
                                //CalculateGrandTotal('');
                                CalculateGrandTotal('0');
                                return true;
                            }
                        },
                        {
                            name: 'Change APL',
                            click: function (e) {

                                isMainRecipe = true;
                                var gridObj = $("#gridMOG0").data("kendoGrid");
                                currentPopuatedMOGGrid = "";
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;

                                isRegionAPL = false;

                                MOGName = tr.MOGName;
                                MOGSelectedArticleNumber = tr.ArticleNumber;
                                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                $("#windowEditMOGWiseAPL").kendoWindow({
                                    animation: false
                                });
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open();
                                dialog.center();
                                dialog.title("MOG APL Details - " + MOGName);
                                MOGCode = tr.MOGCode;
                                mogWiseAPLMasterdataSource = [];
                                getMOGWiseAPLMasterData(MOGCode);
                                currentGridId = "#gridMOG0";
                                populateAPLMasterGrid("Change", "#gridMOG0");
                                Utility.UnLoading();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                $.each(obj, function (key, value) {

                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                });

                                $("#totmog").text(mogtotal.toFixed(2));
                                var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                $("#ToTIngredients").text(tot);
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG0").css("display", "block");
                                $("#emptymog0").css("display", "none");
                            } else {
                                $("#gridMOG0").css("display", "none");
                                $("#emptymog0").css("display", "block");
                            }
                        },
                            {

                                SiteCode: $("#hdnSiteCode").val(),
                                recipeID: 0,
                                recipeCode: recipeCode,
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            RegionQuantity: { type: "number", editable: false },
                            RegionIngredientPerc: { type: "number", editable: false },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false },
                            RegionTotalCost: { editable: false },
                            ArticleNumber: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
        var toolbar = $("#gridMOG0").find(".k-grid-toolbar");
        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG0 .k-grid-content").addClass("gridInside");
        toolbar.find(".k-grid-clearall").addClass("gridButton");
        $('#gridMOG0 a.k-grid-clearall').click(function (e) {
             Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    $("#gridMOG0").data('kendoGrid').dataSource.data([]);
                    showHideGrid('gridMOG0', 'emptymog0', 'mup_gridBaseRecipe0');
                    gridIdin = 'gridMOG0';
                },
                function () {
                }
            );

        });
        $("#gridMOG0 .k-grid-cancel-changes").unbind("mousedown");
        $(".k-grid-cancel-changes").mousedown(function (e) {
             Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    var grid = $("#gridMOG0").data("kendoGrid");
                    grid.cancelChanges();
                    //alert('down');
                },
                function () {
                }
            );

        });
        //var gridObj = $("#gridMOG").data("kendoGrid");
        //$("#gridMOG").find(".k-grid-toolbar").on("click", ".k-grid-cancel-changes", function (e) {
        //    e.preventDefault();
        //     Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
        //        function () {
        //            // var grid = $("#" + gridIdin + "").data("kendoGrid");
        //            //alert(gridIdin);
        //            //if (gridIdin == 'gridBaseRecipe') {
        //            //    populateBaseRecipeGrid(resetID);
        //            //    return;
        //            //} else if (gridIdin == 'gridMOG') {
        //            //populateMOGGrid(resetID)
        //            //return;
        //            //}
        //            // grid.cancelChanges();
        //        },
        //        function () {
        //        }
        //    );
        //});

        //$("#gridMOG .k-grid-cancel-changes").click(function (e) {
        //    e.preventDefault();  


        //});
    }
    else {
        Utility.Loading();
        var gridVariable = $("#gridMOG0");
        gridVariable.html("");
        gridVariable.kendoGrid({
            toolbar: [{ name: "cancel", text: "Reset" }],
            groupable: false,
            editable: false,
            navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: false,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: mogdata,
                                value: options.field,
                                change: onMOGDDLChange0,
                            });
                        // container.find(".k-input")[0].text = options.model.MOGName;
                    }
                },
                {
                    field: "AllergensName", title: "Allergens", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "AllergensName",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Site Qty", width: "50px",
                    template: '# if (IsMajor == true) {#<div><input type="number"  class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputmogqty" type="number"   name="Quantity" oninput="calculateItemTotalMOG(this,0)"/></div>#}#',


                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                },


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "regiontotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },

                {
                    title: "Action",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Change APL',
                            click: function (e) {
                                isMainRecipe = true;
                                var gridObj = $("#gridMOG0").data("kendoGrid");
                                currentPopuatedMOGGrid = "";
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                isRegionAPL = false;

                                MOGName = tr.MOGName;
                                MOGSelectedArticleNumber = tr.ArticleNumber;
                                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                $("#windowEditMOGWiseAPL").kendoWindow({
                                    animation: false
                                });
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open();
                                dialog.center();
                                dialog.title("MOG APL Details - " + MOGName);
                                MOGCode = tr.MOGCode;
                                mogWiseAPLMasterdataSource = [];
                                getMOGWiseAPLMasterData(MOGCode);
                                currentGridId = "#gridMOG0";
                                populateAPLMasterGrid("Change", "#gridMOG0");
                                Utility.UnLoading();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                $.each(obj, function (key, value) {

                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                });

                                $("#totmog").text(mogtotal.toFixed(2));
                                var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                $("#ToTIngredients").text(tot);
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG0").css("display", "block");
                                $("#emptymog0").css("display", "none");
                            } else {
                                $("#gridMOG0").css("display", "none");
                                $("#emptymog0").css("display", "block");
                            }
                        },
                            {

                                SiteCode: $("#hdnSiteCode").val(),
                                recipeID: 0,
                                recipeCode: recipeCode,
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange0
                    });
                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
        var toolbar = $("#gridMOG0").find(".k-grid-toolbar");

        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        cancelChangesConfirmation(document.getElementById("gridMOG0"));
        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG0 .k-grid-content").addClass("gridInside");
    }



}
//function populateMOGGrid0() {

//    Utility.Loading();
//    var gridVariable = $("#gridMOG0");
//    gridVariable.html("");
//    /*if (user.SectorNumber == "20") {*/
//        //gridVariable.kendoGrid({
//        //    toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
//        //    groupable: false,
//        //    editable: true,

//        //    columns: [
//        //        {
//        //            field: "MOG_ID", title: "MOG", width: "120px",
//        //            attributes: {
//        //                style: "text-align: left; font-weight:normal"
//        //            },
//        //            template: function (dataItem) {
//        //                var html = ` <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
//        //                    '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
//        //                return html;
//        //            },
//        //            editor: function (container, options) {

//        //                $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
//        //                    .appendTo(container)
//        //                    .kendoDropDownList({
//        //                        index: 0,
//        //                        filter: "contains",
//        //                        dataTextField: "MOGName",
//        //                        dataValueField: "MOG_ID",
//        //                        autoBind: true,
//        //                        dataSource: mogdata,
//        //                        value: options.field,
//        //                        change: onMOGDDLChange0,
//        //                    });

//        //            }
//        //        },
//        //        {
//        //            field: "UOMName", title: "UOM", width: "50px",
//        //            headerAttributes: {
//        //                style: "text-align: center;"
//        //            },
//        //            attributes: {

//        //                class: 'uomname',
//        //                style: "text-align: center; font-weight:normal"
//        //            },
//        //        },
//        //        //{
//        //        //    field: "UOMCode", title: "UOM", width: "120px",
//        //        //    attributes: {
//        //        //        style: "text-align: left; font-weight:normal",
//        //        //        class: ''
//        //        //    },
//        //        //    template: function (dataItem) {
//        //        //        var html = `  <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
//        //        //            '<span class="uomname" style="margin-left:8px">' + kendo.htmlEncode(dataItem.UOMName) + '</span>';
//        //        //        return html;
//        //        //    },
//        //        //    editor: function (container, options) {
//        //        //        $('<input class="mogTemplate" id="ddlMOGUOM0" text="' + options.model.UOMName + '" data-text-field="text" data-value-field="UOMCode" data-bind="value:' + options.field + '"/>')
//        //        //            .appendTo(container)
//        //        //            .kendoDropDownList({
//        //        //                index: 0,
//        //        //                filter: "contains",
//        //        //                dataTextField: "text",
//        //        //                dataValueField: "UOMCode",
//        //        //                autoBind: true,
//        //        //                dataSource: moguomdata,
//        //        //                // dataSource: refreshMOGUOMDropDownData(options.model.UOMName),
//        //        //                value: options.field,
//        //        //                change: onMOGUOMDDLChange0,
//        //        //            });

//        //        //    }
//        //        //},

//        //        {
//        //            field: "Quantity", title: "Quantity", width: "35px",
//        //            headerAttributes: {
//        //                style: "text-align: right;"
//        //            },

//        //            template: '<input type="number" min="0" name="Quantity" class="inputmogqty"   oninput="calculateItemTotalMOG(this,0)"/>',
//        //            attributes: {

//        //                style: "text-align: right; font-weight:normal"
//        //            },
//        //        },

//        //        {
//        //            field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
//        //            headerAttributes: {
//        //                style: "text-align: right;"
//        //            },
//        //            attributes: {
//        //                class: "uomcost",

//        //                style: "text-align: right; font-weight:normal"
//        //            },
//        //        },
//        //        {
//        //            field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
//        //            headerAttributes: {
//        //                style: "text-align: right;"
//        //            },
//        //            attributes: {
//        //                class: "totalcost",
//        //                style: "text-align: right; font-weight:normal"
//        //            },
//        //        },
//        //        {
//        //            title: "Delete",
//        //            width: "60px",
//        //            headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
//        //            headerAttributes: {
//        //                style: "text-align: center;"
//        //            },
//        //            attributes: {
//        //                style: "text-align: center; font-weight:normal;"
//        //            },
//        //            command: [
//        //                {
//        //                    name: 'Delete',
//        //                    text: "Delete",

//        //                    click: function (e) {
//        //                        if ($(e.currentTarget).hasClass("k-state-disabled")) {
//        //                            return false;
//        //                        }
//        //                        var gridObj = $("#gridMOG0").data("kendoGrid");
//        //                        var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
//        //                        gridObj.dataSource._data.remove(selectedRow);
//        //                        CalculateGrandTotal(0);

//        //                        return true;
//        //                    }
//        //                },
//        //                {
//        //                    name: 'View APL',
//        //                    click: function (e) {
//        //                        isMainRecipe = false;
//        //                        var gridObj = $("#gridMOG0").data("kendoGrid");
//        //                        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
//        //                        datamodel = tr;
//        //                        MOGName = tr.MOGName;
//        //                        if (MOGName == "") {
//        //                            return false;
//        //                        }
//        //                        else {
//        //                            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
//        //                            //$("#windowEditMOGWiseAPL").kendoWindow({
//        //                            //    animation: false
//        //                            //});
//        //                            $(".k-overlay").css("display", "block");
//        //                            $(".k-overlay").css("opacity", "0.5");
//        //                            dialog.open();
//        //                            dialog.center();

//        //                            dialog.title("MOG APL Details - " + MOGName);
//        //                            MOGCode = tr.MOGCode;
//        //                            mogWiseAPLMasterdataSource = [];

//        //                            getMOGWiseAPLMasterData("View");
//        //                            populateAPLMasterGrid("View");
//        //                            return true;
//        //                        }
//        //                    }
//        //                }
//        //            ],
//        //        }
//        //    ],
//        //    dataSource: {
//        //        schema: {
//        //            model: {
//        //                id: "ID",
//        //                fields: {
//        //                    MOG_ID: { type: "number", validation: { required: true } },
//        //                    MOGName: { type: "string", validation: { required: true } },
//        //                    Quantity: { editable: false },
//        //                    UOMName: { editable: false },
//        //                    IngredientPerc: { editable: false },
//        //                    CostPerUOM: { editable: false },
//        //                    CostPerKG: { editable: false },
//        //                    TotalCost: { editable: false }
//        //                }
//        //            }
//        //        }
//        //    },
//        //    columnResize: function (e) {
//        //        var grid = gridVariable.data("kendoGrid");
//        //        e.preventDefault();
//        //    },
//        //    dataBound: function (e) {
//        //        var grid = e.sender;
//        //        var items = e.sender.items();

//        //        items.each(function (e) {

//        //            var dataItem = grid.dataItem(this);

//        //            var ddt = $(this).find('.mogTemplate');

//        //            $(ddt).kendoDropDownList({
//        //                index: 0,
//        //                filter: "contains",
//        //                value: dataItem.value,
//        //                dataSource: mogdata,
//        //                dataTextField: "MOGName",
//        //                dataValueField: "MOG_ID",
//        //                change: onMOGDDLChange0
//        //            });
//        //            //ddt.find(".k-input").val(dataItem.MOGName);

//        //            $(this).find('.inputmogqty').val(dataItem.Quantity);
//        //        });
//        //    },
//        //    change: function (e) {
//        //    },
//        //});
//    /*}*/
//    //else {
//    //    gridVariable.kendoGrid({
//    //        toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
//    //        groupable: false,
//    //        editable: true,

//    //        columns: [
//    //            {
//    //                field: "MOG_ID", title: "MOG", width: "120px",
//    //                attributes: {
//    //                    style: "text-align: left; font-weight:normal"
//    //                },
//    //                template: function (dataItem) {
//    //                    var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
//    //                        '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
//    //                    return html;
//    //                },
//    //                editor: function (container, options) {

//    //                    $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
//    //                        .appendTo(container)
//    //                        .kendoDropDownList({
//    //                            index: 0,
//    //                            filter: "contains",
//    //                            dataTextField: "MOGName",
//    //                            dataValueField: "MOG_ID",
//    //                            autoBind: true,
//    //                            dataSource: mogdata,
//    //                            value: options.field,
//    //                            change: onMOGDDLChange0,
//    //                        });

//    //                }
//    //            },

//    //            {
//    //                field: "UOMName", title: "UOM", width: "50px",
//    //                headerAttributes: {
//    //                    style: "text-align: center;"
//    //                },
//    //                attributes: {

//    //                    class: 'uomname',
//    //                    style: "text-align: center; font-weight:normal"
//    //                },
//    //            },
//    //            {
//    //                field: "Quantity", title: "Quantity", width: "35px",
//    //                headerAttributes: {
//    //                    style: "text-align: right;"
//    //                },

//    //                template: '<input type="number" min="0" name="Quantity" class="inputmogqty"   oninput="calculateItemTotalMOG(this,0)"/>',
//    //                attributes: {

//    //                    style: "text-align: right; font-weight:normal"
//    //                },
//    //            },
//    //            {
//    //                field: "IngredientPerc", title: "Major %", width: "50px", format: "{0:0.000}", editable: false,
//    //                headerAttributes: {
//    //                    style: "text-align: right;"
//    //                },
//    //                attributes: {
//    //                    class: "major",
//    //                    style: "text-align: right; font-weight:normal"
//    //                },
//    //                template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
//    //            },
//    //            {
//    //                field: "IngredientPerc", title: "Minor %", width: "50px", format: "{0:0.000}", editable: false,
//    //                headerAttributes: {
//    //                    style: "text-align: right;"
//    //                },
//    //                attributes: {
//    //                    class: "minor",
//    //                    style: "text-align: right; font-weight:normal"
//    //                },
//    //                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
//    //            },
//    //            {
//    //                field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
//    //                headerAttributes: {
//    //                    style: "text-align: right;"
//    //                },
//    //                attributes: {
//    //                    class: "uomcost",

//    //                    style: "text-align: right; font-weight:normal"
//    //                },
//    //            },
//    //            {
//    //                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
//    //                headerAttributes: {
//    //                    style: "text-align: right;"
//    //                },
//    //                attributes: {
//    //                    class: "totalcost",
//    //                    style: "text-align: right; font-weight:normal"
//    //                },
//    //            },
//    //            {
//    //                title: "Delete",
//    //                width: "60px",
//    //                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
//    //                headerAttributes: {
//    //                    style: "text-align: center;"
//    //                },
//    //                attributes: {
//    //                    style: "text-align: center; font-weight:normal;"
//    //                },
//    //                command: [
//    //                    {
//    //                        name: 'Delete',
//    //                        text: "Delete",

//    //                        click: function (e) {
//    //                            if ($(e.currentTarget).hasClass("k-state-disabled")) {
//    //                                return false;
//    //                            }
//    //                            var gridObj = $("#gridMOG0").data("kendoGrid");
//    //                            var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
//    //                            gridObj.dataSource._data.remove(selectedRow);
//    //                            CalculateGrandTotal(0);
//    //                            return true;
//    //                        }
//    //                    },
//    //                    {
//    //                        name: 'View APL',
//    //                        click: function (e) {
//    //                            isMainRecipe = false;
//    //                            var gridObj = $("#gridMOG0").data("kendoGrid");
//    //                            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
//    //                            datamodel = tr;
//    //                            MOGName = tr.MOGName;
//    //                            if (MOGName == "") {
//    //                                return false;
//    //                            }
//    //                            else {
//    //                                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
//    //                                //$("#windowEditMOGWiseAPL").kendoWindow({
//    //                                //    animation: false
//    //                                //});
//    //                                $(".k-overlay").css("display", "block");
//    //                                $(".k-overlay").css("opacity", "0.5");
//    //                                dialog.open();
//    //                                dialog.center();

//    //                                dialog.title("MOG APL Details - " + MOGName);
//    //                                MOGCode = tr.MOGCode;
//    //                                mogWiseAPLMasterdataSource = [];
//    //                                getMOGWiseAPLMasterData("View");
//    //                                populateAPLMasterGrid("View");
//    //                                return true;
//    //                            }
//    //                        }
//    //                    }
//    //                ],
//    //            }
//    //        ],
//    //        dataSource: {
//    //            schema: {
//    //                model: {
//    //                    id: "ID",
//    //                    fields: {
//    //                        MOG_ID: { type: "number", validation: { required: true } },
//    //                        MOGName: { type: "string", validation: { required: true } },
//    //                        Quantity: { editable: false },
//    //                        UOMName: { editable: false },
//    //                        IngredientPerc: { editable: false },
//    //                        CostPerUOM: { editable: false },
//    //                        CostPerKG: { editable: false },
//    //                        TotalCost: { editable: false }
//    //                    }
//    //                }
//    //            }
//    //        },
//    //        columnResize: function (e) {
//    //            var grid = gridVariable.data("kendoGrid");
//    //            e.preventDefault();
//    //        },
//    //        dataBound: function (e) {
//    //            var grid = e.sender;
//    //            var items = e.sender.items();

//    //            items.each(function (e) {

//    //                var dataItem = grid.dataItem(this);

//    //                var ddt = $(this).find('.mogTemplate');

//    //                $(ddt).kendoDropDownList({
//    //                    index: 0,
//    //                    filter: "contains",
//    //                    value: dataItem.value,
//    //                    dataSource: mogdata,
//    //                    dataTextField: "MOGName",
//    //                    dataValueField: "MOG_ID",
//    //                    change: onMOGDDLChange0
//    //                });
//    //                //ddt.find(".k-input").val(dataItem.MOGName);

//    //                $(this).find('.inputmogqty').val(dataItem.Quantity);
//    //            });
//    //        },
//    //        change: function (e) {
//    //        },
//    //    });
//    //}

//    cancelChangesConfirmation('gridMOG0');
//    var toolbar = $("#gridMOG0").find(".k-grid-toolbar");


//    //toolbar.find(".k-add").remove();
//    //toolbar.find(".k-cancel").remove();

//    toolbar.addClass("gridToolbarShift");
//    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
//    toolbar.find(".k-grid-add").addClass("gridButton")
//    $("#gridMOG0 .k-grid-content").addClass("gridInside");
//    toolbar.find(".k-grid-clearall").addClass("gridButton");
//    $('#gridMOG0 a.k-grid-clearall').click(function (e) {
//         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
//            function () {
//                $("#gridMOG0").data('kendoGrid').dataSource.data([]);
//                showHideGrid('gridMOG0', 'emptymog0', 'mup_gridBaseRecipe0');
//            },
//            function () {
//            }
//        );

//    });
//}

//function populateMOGGridx() {

//    Utility.Loading();
//    var gridVariable = $("#gridMOGx");
//    gridVariable.html("");
//    gridVariable.kendoGrid({
//        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
//        groupable: false,
//        editable: true,

//        columns: [
//            {
//                field: "MOG_ID", title: "MOG", width: "120px",
//                attributes: {
//                    style: "text-align: left; font-weight:normal"
//                },
//                template: function (dataItem) {
//                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
//                    return html;
//                },
//                editor: function (container, options) {

//                    $('<input class="mogTemplate" id="ddlMOGx" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
//                        .appendTo(container)
//                        .kendoDropDownList({
//                            index: 0,
//                            filter: "contains",
//                            dataTextField: "MOGName",
//                            dataValueField: "MOG_ID",
//                            autoBind: true,
//                            dataSource: mogdata,
//                            value: options.field,
//                            change: onMOGDDLChangex,
//                        });

//                }
//            },
//            {
//                field: "UOMName", title: "UOM", width: "50px",
//                headerAttributes: {
//                    style: "text-align: center;"
//                },
//                attributes: {

//                    class: 'uomname',
//                    style: "text-align: center; font-weight:normal"
//                },
//            },
//            {
//                field: "Quantity", title: "Quantity", width: "35px",
//                headerAttributes: {
//                    style: "text-align: right;"
//                },
//                //editor: function (container, options) {
//                //    $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" oninput="calculateItemTotalMOG(this,x)"/>')
//                //        .appendTo(container)
//                //        .kendoNumericTextBox({
//                //            spinners: false,
//                //            decimals: 4
//                //        });
//                //},
//                template: '<input type="number" class="inputmogqty"  name="Quantity" oninput="calculateItemTotalMOG(this,' + 'x' + ')"/>',
//                attributes: {

//                    style: "text-align: right; font-weight:normal"
//                },
//            },
//            {
//                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
//                headerAttributes: {
//                    style: "text-align: center;"
//                },
//                attributes: {
//                    class: "major",
//                    style: "text-align: center; font-weight:normal"
//                },
//                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
//            },
//            {
//                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
//                headerAttributes: {
//                    style: "text-align: center;"
//                },
//                attributes: {
//                    class: "minor",
//                    style: "text-align: center; font-weight:normal"
//                },
//                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
//            },

//            {
//                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
//                headerAttributes: {
//                    style: "text-align: right;"
//                },
//                attributes: {
//                    class: "uomcost",

//                    style: "text-align: right; font-weight:normal"
//                },
//            },
//            {
//                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
//                headerAttributes: {
//                    style: "text-align: right;"
//                },
//                attributes: {
//                    class: "totalcost",
//                    style: "text-align: right; font-weight:normal"
//                },
//            },
//            {
//                title: "Delete",
//                width: "60px",
//                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
//                headerAttributes: {
//                    style: "text-align: center;"
//                },
//                attributes: {
//                    style: "text-align: center; font-weight:normal;"
//                },
//                command: [
//                    {
//                        name: 'Delete',
//                        text: "Delete",

//                        click: function (e) {
//                            if ($(e.currentTarget).hasClass("k-state-disabled")) {
//                                return false;
//                            }
//                            var gridObj = $("#gridMOGx").data("kendoGrid");
//                            var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
//                            gridObj.dataSource._data.remove(selectedRow);
//                            CalculateGrandTotal('x');
//                            return true;
//                        }
//                    },
//                    {
//                        name: 'View APL',
//                        click: function (e) {
//                            var gridObj = $("#gridMOGx").data("kendoGrid");
//                            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
//                            datamodel = tr;
//                            MOGName = tr.MOGName;
//                            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
//                            $("#windowEditMOGWiseAPL").kendoWindow({
//                                animation: false
//                            });
//                            $(".k-overlay").css("display", "block");
//                            $(".k-overlay").css("opacity", "0.5");
//                            dialog.open();
//                            dialog.center();
//                            dialog.title("MOG APL Details - " + MOGName);
//                            MOGCode = tr.MOGCode;
//                            mogWiseAPLMasterdataSource = [];
//                            getMOGWiseAPLMasterData();
//                            populateAPLMasterGrid("View");
//                            return true;
//                        }
//                    }
//                ],
//            }
//        ],
//        dataSource: {
//            schema: {
//                model: {
//                    id: "ID",
//                    fields: {
//                        MOG_ID: { type: "number", validation: { required: true } },
//                        MOGName: { type: "string", validation: { required: true } },
//                        Quantity: { editable: false },
//                        UOMName: { editable: false },
//                        IngredientPerc: { editable: false },
//                        CostPerUOM: { editable: false },
//                        CostPerKG: { editable: false },
//                        TotalCost: { editable: false }
//                    }
//                }
//            }
//        },
//        columnResize: function (e) {
//            var grid = gridVariable.data("kendoGrid");
//            e.preventDefault();
//        },
//        dataBound: function (e) {
//            var grid = e.sender;
//            var items = e.sender.items();

//            items.each(function (e) {

//                var dataItem = grid.dataItem(this);

//                var ddt = $(this).find('.mogTemplate');

//                $(ddt).kendoDropDownList({
//                    index: 0,
//                    filter: "contains",
//                    value: dataItem.value,
//                    dataSource: mogdata,
//                    dataTextField: "MOGName",
//                    dataValueField: "MOG_ID",
//                    change: onMOGDDLChangex
//                });
//                //ddt.find(".k-input").val(dataItem.MOGName);

//                $(this).find('.inputmogqty').val(dataItem.Quantity);
//            });
//        },
//        change: function (e) {
//        },
//    });
//    cancelChangesConfirmation('gridMOGx');
//    var toolbar = $("#gridMOGx").find(".k-grid-toolbar");


//    toolbar.find(".k-add").remove();
//    toolbar.find(".k-cancel").remove();

//    toolbar.addClass("gridToolbarShift");
//    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
//    toolbar.find(".k-grid-add").addClass("gridButton")
//    $("#gridMOGx .k-grid-content").addClass("gridInside");
//}


function populateUOMDropdown1() {
    $("#inputuom1").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateBaseDropdown1() {
    $("#inputbase1").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateUOMDropdown2() {
    $("#inputuom2").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateBaseDropdown2() {
    $("#inputbase2").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateUOMDropdown3() {
    $("#inputuom3").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateBaseDropdown3() {
    $("#inputbase3").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateUOMDropdown4() {
    $("#inputuom4").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateBaseDropdown4() {
    $("#inputbase4").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateUOMDropdown5() {
    $("#inputuom5").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateBaseDropdown5() {
    $("#inputbase5").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function populateMOGGridCopy(recipeID, recipeCode) {


    if (user.SectorNumber == "20") {

        Utility.Loading();
        var gridVariable = $("#gridMOG0");
        gridVariable.html("");
        gridVariable.kendoGrid({
            //toolbar: [{ name: "cancel", text: "Reset" }],
            toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,
            //  navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: true,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: mogdata,
                                value: options.field,
                                change: onMOGDDLChange0,
                            });
                        // container.find(".k-input")[0].text = options.model.MOGName;
                    }
                },
                {
                    field: "AllergensName", title: "Allergens", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "AllergensName",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                //{
                //    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "si",
                //        style: "text-align: center; font-weight:normal"
                //    }
                //},

                {
                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },

                    template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,0)"/>',
                    attributes: {

                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                //{

                //    field: "Quantity", title: "Site Qty", width: "50px",
                //    template: '# if (IsMajor == true) {#<div><input type="number"  class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" /></div>#} else {#<div><input class="inputmogqty" type="number"   name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',


                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //},

                //{
                //    field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "change",
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                //},


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                //{
                //    field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                //    headerAttributes: {

                //        style: "text-align: right"
                //    },
                //    attributes: {
                //        class: "regiontotalcost",
                //        style: "text-align: right; font-weight:normal"
                //    },
                //},
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },

                {
                    title: "Action",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridMOG0").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                gridObj.dataSource._data.remove(selectedRow);
                                //CalculateGrandTotal('');
                                CalculateGrandTotal('0');
                                //if (modelHistoryData0.length > 0) {
                                //    var filtermodelHistoryData = modelHistoryData0.filter(ds => ds.MOGCode == selectedRow.MOGCode);
                                //    modelHistoryData0.forEach(function (item, index) {
                                //        if (item.MOGCode == selectedRow.MOGCode) {
                                //            modelHistoryData0.splice(index, 1);
                                //        }
                                //    });

                                //    console.log("modelHistoryData0");
                                //    console.log(modelHistoryData0)
                                //}
                                return true;
                            }
                        },
                        {
                            name: 'Change APL',
                            click: function (e) {
                                isMainRecipe = true;
                                var gridObj = $("#gridMOG0").data("kendoGrid");
                                currentPopuatedMOGGrid = "";
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                isRegionAPL = false;

                                MOGName = tr.MOGName;
                                MOGSelectedArticleNumber = tr.ArticleNumber;
                                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                $("#windowEditMOGWiseAPL").kendoWindow({
                                    animation: false
                                });
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open();
                                dialog.center();
                                dialog.title("MOG APL Details - " + MOGName);
                                MOGCode = tr.MOGCode;
                                mogWiseAPLMasterdataSource = [];
                                getMOGWiseAPLMasterData(MOGCode);
                                currentGridId = "#gridMOG0";
                                //alert(currentGridId);
                                populateAPLMasterGrid("Change", "#gridMOG0");
                                Utility.UnLoading();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                $.each(obj, function (key, value) {

                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                });

                                $("#totmog").text(mogtotal.toFixed(2));
                                var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                $("#ToTIngredients").text(tot);
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG0").css("display", "block");
                                $("#emptymog0").css("display", "none");
                                CalculateGrandTotal(0);
                            } else {
                                $("#gridMOG0").css("display", "none");
                                $("#emptymog0").css("display", "block");
                            }
                        },
                            {

                                SiteCode: $("#hdnSiteCode").val(),
                                recipeID: 0,
                                recipeCode: recipeCode,
                                simCode: $("#SimulationCode").val()
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            RegionQuantity: { type: "number", editable: false },
                            RegionIngredientPerc: { type: "number", editable: false },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false },
                            RegionTotalCost: { editable: false },
                            ArticleNumber: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
        var toolbar = $("#gridMOG0").find(".k-grid-toolbar");
        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG0 .k-grid-content").addClass("gridInside");
        toolbar.find(".k-grid-clearall").addClass("gridButton");
        $('#gridMOG0 a.k-grid-clearall').click(function (e) {
             Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    //modelHistoryData0 = [];
                    $("#gridMOG0").data('kendoGrid').dataSource.data([]);
                    showHideGrid('gridMOG0', 'emptymog0', 'mup_gridBaseRecipe0');
                    gridIdin = 'gridMOG0';
                },
                function () {
                }
            );

        });
        $("#gridMOG0 .k-grid-cancel-changes").unbind("mousedown");
        $(".k-grid-cancel-changes").mousedown(function (e) {
             Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    //modelHistoryData0 = [];
                    var grid = $("#gridMOG0").data("kendoGrid");
                    grid.cancelChanges();
                    //alert('down');
                },
                function () {
                }
            );

        });
        //var gridObj = $("#gridMOG").data("kendoGrid");
        //$("#gridMOG").find(".k-grid-toolbar").on("click", ".k-grid-cancel-changes", function (e) {
        //    e.preventDefault();
        //     Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
        //        function () {
        //            // var grid = $("#" + gridIdin + "").data("kendoGrid");
        //            //alert(gridIdin);
        //            //if (gridIdin == 'gridBaseRecipe') {
        //            //    populateBaseRecipeGrid(resetID);
        //            //    return;
        //            //} else if (gridIdin == 'gridMOG') {
        //            //populateMOGGrid(resetID)
        //            //return;
        //            //}
        //            // grid.cancelChanges();
        //        },
        //        function () {
        //        }
        //    );
        //});

        //$("#gridMOG .k-grid-cancel-changes").click(function (e) {
        //    e.preventDefault();  


        //});
    }
    else {
        Utility.Loading();
        var gridVariable = $("#gridMOG0");
        gridVariable.html("");
        gridVariable.kendoGrid({
            toolbar: [{ name: "cancel", text: "Reset" }],
            groupable: false,
            editable: false,
            navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: false,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: mogdata,
                                value: options.field,
                                change: onMOGDDLChange0,
                            });
                        // container.find(".k-input")[0].text = options.model.MOGName;
                    }
                },
                {
                    field: "AllergensName", title: "Allergens", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "AllergensName",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Site Qty", width: "50px",
                    template: '# if (IsMajor == true) {#<div><input type="number"  class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputmogqty" type="number"   name="Quantity" oninput="calculateItemTotalMOG(this,0)"/></div>#}#',


                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                },


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "regiontotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },

                {
                    title: "Action",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Change APL',
                            click: function (e) {
                                isMainRecipe = true;
                                var gridObj = $("#gridMOG0").data("kendoGrid");
                                currentPopuatedMOGGrid = "";
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                isRegionAPL = false;

                                MOGName = tr.MOGName;
                                MOGSelectedArticleNumber = tr.ArticleNumber;
                                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                $("#windowEditMOGWiseAPL").kendoWindow({
                                    animation: false
                                });
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open();
                                dialog.center();
                                dialog.title("MOG APL Details - " + MOGName);
                                MOGCode = tr.MOGCode;
                                mogWiseAPLMasterdataSource = [];
                                getMOGWiseAPLMasterData(MOGCode);
                                currentGridId = "#gridMOG0";
                                //alert(currentGridId);
                                populateAPLMasterGrid("Change", "#gridMOG0");
                                Utility.UnLoading();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                $.each(obj, function (key, value) {

                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                });

                                $("#totmog").text(mogtotal.toFixed(2));
                                var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                $("#ToTIngredients").text(tot);
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG0").css("display", "block");
                                $("#emptymog0").css("display", "none");
                            } else {
                                $("#gridMOG0").css("display", "none");
                                $("#emptymog0").css("display", "block");
                            }
                        },
                            {

                                SiteCode: $("#hdnSiteCode").val(),
                                recipeID: 0,
                                recipeCode: recipeCode,
                                simCode: $("#SimulationCode").val()
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange0
                    });
                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
        var toolbar = $("#gridMOG0").find(".k-grid-toolbar");

        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        cancelChangesConfirmation(document.getElementById("gridMOG0"));
        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG0 .k-grid-content").addClass("gridInside");
    }



}
//function populateMOGGridCopy(recipeID, recipeCode) {

//    Utility.Loading();
//    var gridVariable = $("#gridMOG0");
//    gridVariable.html("");
//    if (user.SectorNumber == "20") {
//        gridVariable.kendoGrid({
//            toolbar: [//{ name: "cancel", text: "Reset" },
//                { name: "create", text: "Add" }
//            ],
//            groupable: false,
//            editable: true,

//            columns: [
//                {
//                    field: "MOG_ID", title: "MOG", width: "120px",
//                    attributes: {
//                        style: "text-align: left; font-weight:normal"
//                    },
//                    template: function (dataItem) {
//                        var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
//                            '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
//                        return html;
//                    },
//                    editor: function (container, options) {

//                        $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
//                            .appendTo(container)
//                            .kendoDropDownList({
//                                index: 0,
//                                filter: "contains",
//                                dataTextField: "MOGName",
//                                dataValueField: "MOG_ID",
//                                autoBind: true,
//                                dataSource: mogdata,
//                                value: options.field,
//                                change: onMOGDDLChange0,
//                            });

//                    }
//                },
//                {
//                    field: "UOMName", title: "UOM", width: "50px",
//                    headerAttributes: {
//                        style: "text-align: center;"
//                    },
//                    attributes: {

//                        class: 'uomname',
//                        style: "text-align: center; font-weight:normal"
//                    },
//                },
//                //{
//                //    field: "UOMCode", title: "UOM", width: "120px",
//                //    attributes: {
//                //        style: "text-align: left; font-weight:normal",
//                //        class: ''
//                //    },
//                //    template: function (dataItem) {
//                //        var html = `  <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
//                //            '<span class="uomname" style="margin-left:8px">' + kendo.htmlEncode(dataItem.UOMName) + '</span>';
//                //        return html;
//                //    },
//                //    editor: function (container, options) {
//                //        $('<input class="mogTemplate" id="ddlMOGUOM0" text="' + options.model.UOMName + '" data-text-field="text" data-value-field="UOMCode" data-bind="value:' + options.field + '"/>')
//                //            .appendTo(container)
//                //            .kendoDropDownList({
//                //                index: 0,
//                //                filter: "contains",
//                //                dataTextField: "text",
//                //                dataValueField: "UOMCode",
//                //                autoBind: true,
//                //                dataSource: moguomdata,
//                //                // dataSource: refreshMOGUOMDropDownData(options.model.UOMName),
//                //                value: options.field,
//                //                change: onMOGUOMDDLChange0,
//                //            });

//                //    }
//                //},
//                {
//                    field: "Quantity", title: "Quantity", width: "35px",
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },

//                    template: '<input type="number" min="0" name="Quantity" class="inputmogqty"   oninput="calculateItemTotalMOG(this,0)"/>',
//                    attributes: {

//                        style: "text-align: right; font-weight:normal"
//                    },
//                },
//                {
//                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },
//                    attributes: {
//                        class: "uomcost",

//                        style: "text-align: right; font-weight:normal"
//                    },
//                },
//                {
//                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },
//                    attributes: {
//                        class: "totalcost",
//                        style: "text-align: right; font-weight:normal"
//                    },
//                },
//                {
//                    title: "Delete",
//                    width: "60px",
//                    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
//                    headerAttributes: {
//                        style: "text-align: center;"
//                    },
//                    attributes: {
//                        style: "text-align: center; font-weight:normal;"
//                    },
//                    command: [
//                        {
//                            name: 'Delete',
//                            text: "Delete",

//                            click: function (e) {
//                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
//                                    return false;
//                                }
//                                var gridObj = $("#gridMOG0").data("kendoGrid");
//                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
//                                gridObj.dataSource._data.remove(selectedRow);
//                                CalculateGrandTotal(0);
//                                return true;
//                            }
//                        },
//                        {
//                            name: 'View APL',
//                            click: function (e) {
//                                var gridObj = $("#gridMOG0").data("kendoGrid");
//                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
//                                datamodel = tr;
//                                MOGName = tr.MOGName;
//                                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
//                                $("#windowEditMOGWiseAPL").kendoWindow({
//                                    animation: false
//                                });
//                                $(".k-overlay").css("display", "block");
//                                $(".k-overlay").css("opacity", "0.5");
//                                dialog.open();
//                                dialog.center();
//                                dialog.title("MOG APL Details - " + MOGName);
//                                MOGCode = tr.MOGCode;
//                                mogWiseAPLMasterdataSource = [];
//                                getMOGWiseAPLMasterData("View");
//                                populateAPLMasterGrid("View");
//                                return true;
//                            }
//                        }
//                    ],
//                }
//            ],
//            dataSource: {
//                transport: {
//                    read: function (options) {

//                        var varCodes = "";

//                        //HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
//                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
//                            Utility.UnLoading();

//                            if (result != null) {

//                                options.success(result);
//                            }
//                            else {
//                                options.success("");
//                            }

//                            if (result.length > 0) {
//                                $("#gridMOG0").css("display", "block");
//                                $("#emptymog0").css("display", "none");
//                                CalculateGrandTotal(0);
//                            } else {
//                                $("#gridMOG0").css("display", "none");
//                                $("#emptymog0").css("display", "block");
//                            }
//                        },
//                            {
//                                //recipeID: recipeID
//                                SiteCode: $("#hdnSiteCode").val(),
//                                recipeID: 0,
//                                recipeCode: recipeCode,
//                            }
//                            , true);
//                    }
//                },
//                schema: {
//                    model: {
//                        id: "ID",
//                        fields: {
//                            MOG_ID: { type: "number", validation: { required: true } },
//                            MOGName: { type: "string", validation: { required: true } },
//                            Quantity: { editable: false },
//                            UOMName: { editable: false },
//                            IngredientPerc: { editable: false },
//                            CostPerUOM: { editable: false },
//                            CostPerKG: { editable: false },
//                            TotalCost: { editable: false }
//                        }
//                    }
//                }
//            },
//            columnResize: function (e) {
//                var grid = gridVariable.data("kendoGrid");
//                e.preventDefault();
//            },
//            dataBound: function (e) {
//                var grid = e.sender;
//                var items = e.sender.items();

//                items.each(function (e) {

//                    var dataItem = grid.dataItem(this);

//                    var ddt = $(this).find('.mogTemplate');

//                    $(ddt).kendoDropDownList({
//                        index: 0,
//                        filter: "contains",
//                        value: dataItem.value,
//                        dataSource: baserecipedata,
//                        dataTextField: "MOGName",
//                        dataValueField: "MOG_ID",
//                        change: onMOGDDLChange0
//                    });
//                    //ddt.find(".k-input").val(dataItem.MOGName);

//                    $(this).find('.inputmogqty').val(dataItem.Quantity);
//                });
//            },
//            change: function (e) {
//            },
//        });
//    }
//    else {
//        gridVariable.kendoGrid({
//            toolbar: [//{ name: "cancel", text: "Reset" },
//                { name: "create", text: "Add" }
//            ],
//            groupable: false,
//            editable: true,

//            columns: [
//                {
//                    field: "MOG_ID", title: "MOG", width: "120px",
//                    attributes: {
//                        style: "text-align: left; font-weight:normal"
//                    },
//                    template: function (dataItem) {
//                        var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
//                            '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
//                        return html;
//                    },
//                    editor: function (container, options) {

//                        $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
//                            .appendTo(container)
//                            .kendoDropDownList({
//                                index: 0,
//                                filter: "contains",
//                                dataTextField: "MOGName",
//                                dataValueField: "MOG_ID",
//                                autoBind: true,
//                                dataSource: mogdata,
//                                value: options.field,
//                                change: onMOGDDLChange0,
//                            });

//                    }
//                },
//                {
//                    field: "UOMName", title: "UOM", width: "50px",
//                    headerAttributes: {
//                        style: "text-align: center;"
//                    },
//                    attributes: {

//                        class: 'uomname',
//                        style: "text-align: center; font-weight:normal"
//                    },
//                },
//                {
//                    field: "Quantity", title: "Quantity", width: "35px",
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },

//                    template: '<input type="number" min="0" name="Quantity" class="inputmogqty"   oninput="calculateItemTotalMOG(this,0)"/>',
//                    attributes: {

//                        style: "text-align: right; font-weight:normal"
//                    },
//                },
//                {
//                    field: "IngredientPerc", title: "Major %", width: "50px", format: "{0:0.000}", editable: false,
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },
//                    attributes: {
//                        class: "major",
//                        style: "text-align: right; font-weight:normal"
//                    },
//                    template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
//                },
//                {
//                    field: "IngredientPerc", title: "Minor %", width: "50px", format: "{0:0.000}", editable: false,
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },
//                    attributes: {
//                        class: "minor",
//                        style: "text-align: right; font-weight:normal"
//                    },
//                    template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
//                },
//                {
//                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },
//                    attributes: {
//                        class: "uomcost",

//                        style: "text-align: right; font-weight:normal"
//                    },
//                },
//                {
//                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
//                    headerAttributes: {
//                        style: "text-align: right;"
//                    },
//                    attributes: {
//                        class: "totalcost",
//                        style: "text-align: right; font-weight:normal"
//                    },
//                },
//                {
//                    title: "Delete",
//                    width: "60px",
//                    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
//                    headerAttributes: {
//                        style: "text-align: center;"
//                    },
//                    attributes: {
//                        style: "text-align: center; font-weight:normal;"
//                    },
//                    command: [
//                        {
//                            name: 'Delete',
//                            text: "Delete",

//                            click: function (e) {
//                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
//                                    return false;
//                                }
//                                var gridObj = $("#gridMOG0").data("kendoGrid");
//                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
//                                gridObj.dataSource._data.remove(selectedRow);
//                                CalculateGrandTotal(0);
//                                return true;
//                            }
//                        },
//                        {
//                            name: 'View APL',
//                            click: function (e) {
//                                var gridObj = $("#gridMOG0").data("kendoGrid");
//                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
//                                datamodel = tr;
//                                MOGName = tr.MOGName;
//                                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
//                                $("#windowEditMOGWiseAPL").kendoWindow({
//                                    animation: false
//                                });
//                                $(".k-overlay").css("display", "block");
//                                $(".k-overlay").css("opacity", "0.5");
//                                dialog.open();
//                                dialog.center();
//                                dialog.title("MOG APL Details - " + MOGName);
//                                MOGCode = tr.MOGCode;
//                                mogWiseAPLMasterdataSource = [];
//                                getMOGWiseAPLMasterData("View");
//                                populateAPLMasterGrid("View");
//                                return true;
//                            }
//                        }
//                    ],
//                }
//            ],
//            dataSource: {
//                transport: {
//                    read: function (options) {

//                        var varCodes = "";

//                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
//                            Utility.UnLoading();

//                            if (result != null) {

//                                options.success(result);
//                            }
//                            else {
//                                options.success("");
//                            }

//                            if (result.length > 0) {
//                                $("#gridMOG0").css("display", "block");
//                                $("#emptymog0").css("display", "none");
//                                CalculateGrandTotal(0);
//                            } else {
//                                $("#gridMOG0").css("display", "none");
//                                $("#emptymog0").css("display", "block");
//                            }
//                        },
//                            {
//                                recipeID: recipeID
//                            }
//                            , true);
//                    }
//                },
//                schema: {
//                    model: {
//                        id: "ID",
//                        fields: {
//                            MOG_ID: { type: "number", validation: { required: true } },
//                            MOGName: { type: "string", validation: { required: true } },
//                            Quantity: { editable: false },
//                            UOMName: { editable: false },
//                            IngredientPerc: { editable: false },
//                            CostPerUOM: { editable: false },
//                            CostPerKG: { editable: false },
//                            TotalCost: { editable: false }
//                        }
//                    }
//                }
//            },
//            columnResize: function (e) {
//                var grid = gridVariable.data("kendoGrid");
//                e.preventDefault();
//            },
//            dataBound: function (e) {
//                var grid = e.sender;
//                var items = e.sender.items();

//                items.each(function (e) {

//                    var dataItem = grid.dataItem(this);

//                    var ddt = $(this).find('.mogTemplate');

//                    $(ddt).kendoDropDownList({
//                        index: 0,
//                        filter: "contains",
//                        value: dataItem.value,
//                        dataSource: baserecipedata,
//                        dataTextField: "MOGName",
//                        dataValueField: "MOG_ID",
//                        change: onMOGDDLChange0
//                    });
//                    //ddt.find(".k-input").val(dataItem.MOGName);

//                    $(this).find('.inputmogqty').val(dataItem.Quantity);
//                });
//            },
//            change: function (e) {
//            },
//        });
//    }
//    cancelChangesConfirmation('gridMOG0');
//    var toolbar = $("#gridMOG0").find(".k-grid-toolbar");
//    toolbar.find(".k-add").remove();
//    toolbar.find(".k-cancel").remove();

//    toolbar.addClass("gridToolbarShift");
//    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
//    toolbar.find(".k-grid-add").addClass("gridButton")
//    $("#gridMOG0 .k-grid-content").addClass("gridInside");
//}


function showDetails(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");

    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    });

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    $("#hiddenRecipeID").val(tr.Recipe_ID);
    var dialog = $("#windowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.ID);
    populateMOGGrid1(tr.ID);

    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputrecipealiasname1").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("1", tr);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");
    $('#grandTotal1').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG1').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));

    var editor = $("#inputinstruction1").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename1").removeAttr('disabled');
    //    $("#inputuom1").removeAttr('disabled');
    //    $("#inputquantity1").removeAttr('disabled');
    //    $("#inputbase1").removeAttr('disabled');
    //   // $("#btnSubmit1").css('display', '');
    //    $("#btnCancel1").css('display', '');
    //} else {
    //    $("#inputrecipename1").attr('disabled', 'disabled');
    //    $("#inputuom1").attr('disabled', 'disabled');
    //    $("#inputquantity1").attr('disabled', 'disabled');
    //    $("#inputbase1").attr('disabled', 'disabled');
    //   //$("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details  - " + RecipeName);
    return true;

}


function showDetailsx(e) {
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipex").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#windowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.ID);
    populateMOGGrid1(tr.ID);

    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");

    $('#grandTotal1').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG1').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    var editor = $("#inputinstruction1").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename1").removeAttr('disabled');
    //    $("#inputuom1").removeAttr('disabled');
    //    $("#inputquantity1").removeAttr('disabled');
    //    $("#inputbase1").removeAttr('disabled');
    //  //  $("#btnSubmit1").css('display', '');
    //    $("#btnCancel1").css('display', '');
    //} else {
    //    $("#inputrecipename1").attr('disabled', 'disabled');
    //    $("#inputuom1").attr('disabled', 'disabled');
    //    $("#inputquantity1").attr('disabled', 'disabled');
    //    $("#inputbase1").attr('disabled', 'disabled');
    // //   $("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details  - " + RecipeName);
    return true;

}


function showDetails1(e) {



    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#windowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.ID);
    populateMOGGrid1(tr.ID);

    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputrecipealiasname1").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("1", tr);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");

    $('#grandTotal1').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG1').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));

    var editor = $("#inputinstruction1").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));

    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename1").removeAttr('disabled');
    //    $("#inputuom1").removeAttr('disabled');
    //    $("#inputquantity1").removeAttr('disabled');
    //    $("#inputbase1").removeAttr('disabled');
    //  //  $("#btnSubmit1").css('display', '');
    //    $("#btnCancel1").css('display', '');
    //} else {
    //    $("#inputrecipename1").attr('disabled', 'disabled');
    //    $("#inputuom1").attr('disabled', 'disabled');
    //    $("#inputquantity1").attr('disabled', 'disabled');
    //    $("#inputbase1").attr('disabled', 'disabled');
    //    $("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details  - " + RecipeName);
    return true;

}


function showDetails2(e) {
    $("#windowEdit1").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe1").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    // $("#windowEdit1").parent('.k-widget').css("display", "none");
    var dialog = $("#windowEdit2").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown2();
    populateBaseDropdown2();
    populateBaseRecipeGrid2(tr.ID);
    populateMOGGrid2(tr.ID);

    $("#recipeid2").val(tr.ID);
    $("#inputrecipecode2").val(tr.RecipeCode);
    $("#inputrecipename2").val(tr.Name);
    $("#inputrecipealiasname2").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("2", tr);
    $("#inputuom2").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity2").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase2").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase2").data('kendoDropDownList').value("No");
    $('#grandTotal2').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG2').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    var editor = $("#inputinstruction2").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename2").removeAttr('disabled');
    //    $("#inputuom2").removeAttr('disabled');
    //    $("#inputquantity2").removeAttr('disabled');
    //    $("#inputbase2").removeAttr('disabled');
    //  //  $("#btnSubmit2").css('display', '');
    //    $("#btnCancel2").css('display', '');
    //} else {
    //    $("#inputrecipename2").attr('disabled', 'disabled');
    //    $("#inputuom2").attr('disabled', 'disabled');
    //    $("#inputquantity2").attr('disabled', 'disabled');
    //    $("#inputbase2").attr('disabled', 'disabled');
    //    $("#btnSubmit2").css('display', 'none');
    //    $("#btnCancel2").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}

function showDetails3(e) {
    $("#windowEdit2").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe2").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error3").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#windowEdit3").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown3();
    populateBaseDropdown3();
    populateBaseRecipeGrid3(tr.ID);
    populateMOGGrid3(tr.ID);

    $("#recipeid3").val(tr.ID);
    $("#inputrecipecode3").val(tr.RecipeCode);
    $("#inputrecipename3").val(tr.Name);
    $("#inputrecipealiasname3").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("3", tr);
    $("#inputuom3").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity3").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase3").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase3").data('kendoDropDownList').value("No");
    $('#grandTotal3').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG3').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    var editor = $("#inputinstruction3").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename3").removeAttr('disabled');
    //    $("#inputuom3").removeAttr('disabled');
    //    $("#inputquantity3").removeAttr('disabled');
    //    $("#inputbase3").removeAttr('disabled');
    //   // $("#btnSubmit3").css('display', '');
    //    $("#btnCancel3").css('display', '');
    //} else {
    //    $("#inputrecipename3").attr('disabled', 'disabled');
    //    $("#inputuom3").attr('disabled', 'disabled');
    //    $("#inputquantity3").attr('disabled', 'disabled');
    //    $("#inputbase3").attr('disabled', 'disabled');
    //    $("#btnSubmit3").css('display', 'none');
    //    $("#btnCancel3").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}


function showDetails4(e) {
    $("#windowEdit3").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe3").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error4").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#windowEdit4").data("kendoWindow");

    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown4();
    populateBaseDropdown4();
    populateBaseRecipeGrid4(tr.ID);
    populateMOGGrid4(tr.ID);

    $("#recipeid4").val(tr.ID);
    $("#inputrecipecode4").val(tr.RecipeCode);
    $("#inputrecipename4").val(tr.Name);
    $("#inputrecipealiasname4").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("4", tr);
    $("#inputuom4").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity4").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase4").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase4").data('kendoDropDownList').value("No");
    $('#grandTotal4').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG4').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));

    var editor = $("#inputinstruction4").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename4").removeAttr('disabled');
    //    $("#inputuom4").removeAttr('disabled');
    //    $("#inputquantity4").removeAttr('disabled');
    //    $("#inputbase4").removeAttr('disabled');
    //  //  $("#btnSubmit4").css('display', '');
    //    $("#btnCancel4").css('display', '');
    //} else {
    //    $("#inputrecipename4").attr('disabled', 'disabled');
    //    $("#inputuom4").attr('disabled', 'disabled');
    //    $("#inputquantity4").attr('disabled', 'disabled');
    //    $("#inputbase4").attr('disabled', 'disabled');
    //    $("#btnSubmit4").css('display', 'none');
    //    $("#btnCancel4").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}

function showDetails5(e) {
    $("#windowEdit4").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe4").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error4").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#windowEdit5").data("kendoWindow");

    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown5();
    populateBaseDropdown5();
    populateBaseRecipeGrid5(tr.ID);
    populateMOGGrid5(tr.ID);

    $("#recipeid5").val(tr.ID);
    $("#inputrecipecode5").val(tr.RecipeCode);
    $("#inputrecipename5").val(tr.Name);
    $("#inputrecipealiasname5").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("5", tr);
    $("#inputuom5").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity5").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase5").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase5").data('kendoDropDownList').value("No");
    $('#grandTotal5').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG5').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    var editor = $("#inputinstruction5").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename5").removeAttr('disabled');
    //    $("#inputuom5").removeAttr('disabled');
    //    $("#inputquantity5").removeAttr('disabled');
    //    $("#inputbase5").removeAttr('disabled');
    //    //$("#btnSubmit5").css('display', '');
    //    $("#btnCancel5").css('display', '');
    //} else {
    //    $("#inputrecipename5").attr('disabled', 'disabled');
    //    $("#inputuom5").attr('disabled', 'disabled');
    //    $("#inputquantity5").attr('disabled', 'disabled');
    //    $("#inputbase5").attr('disabled', 'disabled');
    //    $("#btnSubmit5").css('display', 'none');
    //    $("#btnCancel5").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}

function showDetails6(e) {
    alert("Close Previous then Proceed!!!");
}


function BaseRecipetotalQTY(e, level) {
    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var gTotal = 0.0;
    var len1 = grid._data.length;

    for (i = 0; i < len1; i++) {
        var totalCostMOG = grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {

        $("#totBaseRecpQty").text(Utility.MathRound(gTotal, 2));
        // parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))).toFixed(2);
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
        $("#ToTIngredients").text(tot);
    }


}
function calculateItemTotal(e, level) {


    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");
    if (level == null || typeof level == 'undefined')
        level = '';
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    var recipeQty = $("#inputquantity" + level).val();
    var perc = (qty / recipeQty) * 100;
    dataItem.IngredientPerc = Utility.MathRound(perc, 2);
    //  if (level == '' || level == '0') {
    if (user.SectorNumber !== "20") {
        if (perc >= MajorPercentData) {
            dataItem.IsMajor = 1;
            $(element).closest('tr').find('.major')[0].innerHTML = dataItem.IngredientPerc;
            $(element).closest('tr').find('.minor')[0].innerHTML = "-";
        } else {
            dataItem.IsMajor = 0;
            $(element).closest('tr').find('.major')[0].innerHTML = "-"
            $(element).closest('tr').find('.minor')[0].innerHTML = dataItem.IngredientPerc;
        }
    }
    // }
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    CalculateGrandTotal(level);
    BaseRecipetotalQTY(e, level);
}

function mogtotalQTY(e, level) {
    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var gTotal = 0.0;

    var len1 = grid._data.length;



    for (i = 0; i < len1; i++) {
        var totalCostMOG = grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {

        $("#totmog").text(Utility.MathRound(gTotal, 2));
        //parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))).toFixed(2);
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
        $("#ToTIngredients").text(tot);
    }

}

function calculateItemTotalMOG(e, level) {
    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();


    var recipeQty = $("#inputquantity" + level).val();
    var perc = (qty / recipeQty) * 100;
    dataItem.Quantity = qty;
    dataItem.IngredientPerc = Utility.MathRound(perc, 2);
    //  if (level == '' || level == '0') {
    if (user.SectorNumber !== "20") {
        if (perc >= MajorPercentData) {
            dataItem.IsMajor = 1;
            $(element).closest('tr').find('.major')[0].innerHTML = dataItem.IngredientPerc;
            $(element).closest('tr').find('.minor')[0].innerHTML = "-";
        } else {
            dataItem.IsMajor = 0;
            $(element).closest('tr').find('.major')[0].innerHTML = "-"
            $(element).closest('tr').find('.minor')[0].innerHTML = dataItem.IngredientPerc;
            //if (dataItem.IsMajor) {
            //    if (perc <= MajorPercentData) {
            //        Utility.Page_Alert_Save("Ingredient weight will change from Major to Minor. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            //            function () {
            //                dataItem.IsMajor = 0;
            //                $(element).closest('tr').find('.major')[0].innerHTML = "-"
            //                $(element).closest('tr').find('.minor')[0].innerHTML = dataItem.IngredientPerc;
            //            },
            //            function () {
            //            }
            //        );
            //    }
            //}

        }
    }
    // }
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    CalculateGrandTotal(level);
    mogtotalQTY(e, level);
}

function changeMajorToMinorConfirmation() {
    Utility.Page_Alert_Save("Ingredient weight will change from Major to Minor. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
        function () {
        },
        function () {
        }
    );
}


function showHideGrid(gridId, emptyGrid, uid) {

    var chk = $("#" + gridId).data().kendoGrid.dataSource.data();
    if (chk != undefined) {
        var gridLength = $("#" + gridId).data().kendoGrid.dataSource.data().length;

        if (gridLength == 0) {
            $("#" + emptyGrid).toggle();
            $("#" + gridId).hide();
        }
        else {
            $("#" + emptyGrid).hide();
            $("#" + gridId).toggle();
        }

        if (!($("#" + gridId).is(":visible") || $("#" + emptyGrid).is(":visible"))) {
            $("#" + uid).addClass("bottomCurve");

        } else {
            $("#" + uid).removeClass("bottomCurve");

        }
    }
}

function hideGrid(gridId, emptyGrid) {
    $("#" + emptyGrid).hide();
    $("#" + gridId).show();
}

function instructionTextEditor(divid) {

    $("#" + divid).kendoEditor({
        stylesheets: [
            // "../content/shared/styles/editor.css",
        ],
        tools: [
            "bold",
            "italic",
            "underline",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "insertUnorderedList",
            "tableWizard",
            "createTable",
            "addRowAbove",
            "addRowBelow",
            "addColumnLeft",
            "addColumnRight",
            "deleteRow",
            "deleteColumn",
            "mergeCellsHorizontally",
            "mergeCellsVertically",
            "splitCellHorizontally",
            "splitCellVertically",
            "createLink",
            "unlink",

            {
                name: "fontName",
                items: [
                    { text: "Andale Mono", value: "Andale Mono" },
                    { text: "Arial", value: "Arial" },
                    { text: "Arial Black", value: "Arial Black" },
                    { text: "Book Antiqua", value: "Book Antiqua" },
                    { text: "Comic Sans MS", value: "Comic Sans MS" },
                    { text: "Courier New", value: "Courier New" },
                    { text: "Georgia", value: "Georgia" },
                    { text: "Helvetica", value: "Helvetica" },
                    //{ text: "Impact", value: "Impact" },
                    { text: "Symbol", value: "Symbol" },
                    { text: "Tahoma", value: "Tahoma" },
                    { text: "Terminal", value: "Terminal" },
                    { text: "Times New Roman", value: "Times New Roman" },
                    { text: "Trebuchet MS", value: "Trebuchet MS" },
                    { text: "Verdana", value: "Verdana" },
                ]
            },
            "fontSize",
            "foreColor",
            "backColor",
        ]
    });

}


function decodeHTMLEntities(text) {
    if (text == null || text == '' || text == 'undefined')
        return "";
    var entities = [
        ['amp', '&'],
        ['apos', '\''],
        ['#x27', '\''],
        ['#x2F', '/'],
        ['#39', '\''],
        ['#47', '/'],
        ['lt', '<'],
        ['gt', '>'],
        ['nbsp', ' '],
        ['quot', '"']
    ];

    for (var i = 0, max = entities.length; i < max; ++i)
        text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1]);

    return text;
}
$("document").ready(function () {
    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();

    });

});
function getMOGWiseAPLMasterData(purpose) {
    console.log("purpose")
    console.log(purpose)
    //if (purpose == "View") {
    if (viewOnly==1) {
        $("#btnUnitAPLSelector").hide();
        $("#labelUnitAPLList").hide();
        $("#labelUnitAPLList2").show();
        $("#labelUnitAPLList2").text("Unit APL List");
        $("#inputSearchAPL").attr("style", "width:61%;");
        $("#labelUnitAPLList").attr("style", "float:right;");
       
    }
    else {
        $("#btnUnitAPLSelector").show();
        $("#labelUnitAPLList").show();
        $("#labelUnitAPLList").text("Unit APL List");
        $("#btnUnitAPLSelector").text("Region APL");
        $("#labelUnitAPLList2").hide();
        $("#inputSearchAPL").removeAttr("style");
        $("#labelUnitAPLList").attr("style", "float:none;margin-top:7px;");        
    }
    //if (purpose == "View") {
    //    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG, function (result) {
    //        Utility.UnLoading();

    //        if (result != null) {
    //            mogWiseAPLMasterdataSource = result;
    //            console.log(result);
    //        }
    //        else {
    //        }
    //    }, { mogCode: MOGCode }
    //        , true);
    //}
    //else{
    var chkStatus = $("#hdnStatus").val();
    if (chkStatus == "Versioned") {
        HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG_Site_FCS, function (result) {
            Utility.UnLoading();

            if (result != null) {
                mogWiseAPLMasterdataSource = result;
            }
            else {
            }
        }, {
            mogCode: MOGCode, SiteCode: $("#hdnSiteCode").val(), simuCode: $("#SimulationCode").val()
        }
            , true);
    } else {
        HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG_Site, function (result) {
            Utility.UnLoading();

            if (result != null) {
                mogWiseAPLMasterdataSource = result;
            }
            else {
            }
        }, {
            mogCode: MOGCode, SiteCode: $("#hdnSiteCode").val()
        }
            , true);
    }
    //}
}
function getMOGWiseAPLMasterData_RegionSite(btnEle) {
    var btnClicked = $(btnEle).text();

    if (btnClicked == "Region APL") {
        isRegionAPL = true;
        HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG_Region, function (result) {
            Utility.UnLoading();
            console.log("mogWiseAPLMasterdataSource")
            console.log(result)
            $(btnEle).text("Unit APL");
            $("#labelUnitAPLList").text("Region APL List");
            if (result != null) {
                mogWiseAPLMasterdataSource = result;
                //console.log(result);
                populateAPLMasterGrid("Change", "");
            }
            else {
            }
        }, {
            mogCode: MOGCode, Region_ID: $("#NewSimRegion").val()
        }
            , true);
    }
    else {
        isRegionAPL = false;
        HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG_Site, function (result) {
            Utility.UnLoading();

            $(btnEle).text("Region APL");
            $("#labelUnitAPLList").text("Unit APL List");
            if (result != null) {
                mogWiseAPLMasterdataSource = result;
                populateAPLMasterGrid("Change", "");
            }
            else {
            }
        }, {
            mogCode: MOGCode, SiteCode: $("#hdnSiteCode").val()
        }
            , true);
    }
}
function populateAPLMasterGrid(purpose, gridId) {
    Utility.Loading();
    //alert(currentGridId);
    //currentGridId = gridId;
    var gridVariable = $("#gridMOGWiseAPL");
    gridVariable.html("");

    if (!isRegionAPL) {
        if (isMainRecipe && purpose != "View") {
            $("#btnUnitAPLSelector").show();
            gridVariable.kendoGrid({
                excel: {
                    fileName: "APLMaster.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                pageable: {
                    numeric: false,
                    previousNext: false,
                    messages: {
                        display: "Total: {2} records"
                    }
                },
                groupable: false,
                scrollable: true,
                noRecords: {
                    template: "No Records Available"
                },

                columns: [
                    {
                        field: "ArticleNumber", title: "Article Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "ArticleDescription", title: "Article Name", width: "120px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "UOM", title: "UOM", width: "30px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

                    //        style: "text-align: left; font-weight:normal;text-align:center;"
                    //    },
                    //},
                    //{
                    //    field: "SiteName", title: "SiteName", width: "50px", attributes: {

                    //        style: "text-align: left; font-weight:normal;text-align:center;"
                    //    },
                    //},
                    {
                        field: "AllergensName", title: "Allergens", width: "50px", attributes: {

                            style: "text-align: left; font-weight:normal;text-align:center;"
                        },
                    }
                    ,
                    {
                        field: "ArticleCost", title: "APL Cost", width: "50px", format: Utility.Cost_Format, attributes: {

                            style: "text-align: left; font-weight:normal;text-align:center;"
                        },
                    },

                    {
                        field: "StandardCostPerKg", title: "STD CpKg", width: "50px", format: Utility.Cost_Format, attributes: {

                            style: "text-align: left; font-weight:normal;text-align:center;"
                        },
                    },
                    {
                        template: templateFunction,
                        headerTemplate: "<span style='margin-right : 5px;'>Change APL</span>",

                        width: "70px", title: "Include",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center; vertical-align:middle"
                        }
                    }
                ],
                dataSource: {

                    data: mogWiseAPLMasterdataSource,
                    schema: {
                        model: {
                            id: "ArticleID",
                            fields: {
                                ArticleNumber: { type: "string" },
                                ArticleDescription: { type: "string" },
                                UOM: { type: "string" },
                                ArticleType: { type: "string" },
                                Hierlevel3: { type: "string" },
                                MerchandizeCategoryDesc: { type: "string" },
                                MOGName: { type: "string" },
                                AllergensName: { type: "string" },
                                StandardCostPerKg: { type: "string" },
                                ArticleCost: { type: "string" },
                                // StandardCostPerKg: { type: "decimal" },
                                // SiteCode: { type: "string" },
                                //SiteName: { type: "string" }
                            }
                        }
                    },
                    sort: { field: "StandardCostPerKg", dir: "desc" },
                    // pageSize: 100,
                },

                //maxHeight: 250,
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {

                    }

                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            });
        }
        else if (purpose == "View") {
            $("#btnUnitAPLSelector").hide();
            gridVariable.kendoGrid({
                excel: {
                    fileName: "APLMaster.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                pageable: {
                    numeric: false,
                    previousNext: false,
                    messages: {
                        display: "Total: {2} records"
                    }
                },
                groupable: false,
                scrollable: true,
                noRecords: {
                    template: "No Records Available"
                },

                columns: [
                    {
                        field: "ArticleNumber", title: "Article Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "ArticleDescription", title: "Article Name", width: "120px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "UOM", title: "UOM", width: "30px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

                    //        style: "text-align: left; font-weight:normal;text-align:center;"
                    //    },
                    //},
                    //{
                    //    field: "SiteName", title: "SiteName", width: "50px", attributes: {

                    //        style: "text-align: left; font-weight:normal;text-align:center;"
                    //    },
                    //},
                    {
                        field: "AllergensName", title: "Allergens", width: "50px", attributes: {

                            style: "text-align: left; font-weight:normal;"
                        },
                    }
                    ,
                    {
                        field: "ArticleCost", title: "APL Cost", width: "50px", format: Utility.Cost_Format, attributes: {

                            style: "text-align: left; font-weight:normal;"
                        },
                    },

                    {
                        field: "StandardCostPerKg", title: "STD CpKg", width: "50px", format: Utility.Cost_Format, attributes: {

                            style: "text-align: left; font-weight:normal;"
                        },
                    },
                    //{
                    //    template: templateFunction,
                    //    headerTemplate: "<span style='margin-right : 5px;'>Change APL</span>",

                    //    width: "70px", title: "Include",
                    //    attributes: {
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center; vertical-align:middle"
                    //    }
                    //}
                ],
                dataSource: {

                    data: mogWiseAPLMasterdataSource,
                    schema: {
                        model: {
                            id: "ArticleID",
                            fields: {
                                ArticleNumber: { type: "string" },
                                ArticleDescription: { type: "string" },
                                UOM: { type: "string" },
                                ArticleType: { type: "string" },
                                Hierlevel3: { type: "string" },
                                MerchandizeCategoryDesc: { type: "string" },
                                MOGName: { type: "string" },
                                AllergensName: { type: "string" },
                                StandardCostPerKg: { type: "string" },
                                ArticleCost: { type: "string"},
                                // SiteCode: { type: "string" },
                                //SiteName: { type: "string" }
                            }
                        }
                    },
                    sort: { field: "StandardCostPerKg", dir: "desc" },
                    // pageSize: 100,
                },

                //maxHeight: 250,
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {

                    }

                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            });
        }
    }
    else {
        if (isMainRecipe && purpose != "View") {
            $("#btnUnitAPLSelector").show();
            gridVariable.kendoGrid({
                excel: {
                    fileName: "APLMaster.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                pageable: {
                    numeric: false,
                    previousNext: false,
                    messages: {
                        display: "Total: {2} records"
                    }
                },
                groupable: false,
                scrollable: true,
                noRecords: {
                    template: "No Records Available"
                },

                columns: [
                    {
                        field: "ArticleNumber", title: "Article Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "ArticleDescription", title: "Article Name", width: "120px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "UOM", title: "UOM", width: "30px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    {
                        field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

                            style: "text-align: left; font-weight:normal;text-align:center;"
                        },
                    },
                    {
                        field: "SiteName", title: "SiteName", width: "50px", attributes: {

                            style: "text-align: left; font-weight:normal;text-align:center;"
                        },
                    },
                    {
                        field: "AllergensName", title: "Allergens", width: "50px", attributes: {

                            style: "text-align: left; font-weight:normal;text-align:center;"
                        },
                    }
                    ,
                    {
                        field: "ArticleCost", title: "APL Cost", width: "50px", format: Utility.Cost_Format, attributes: {

                            style: "text-align: left; font-weight:normal;text-align:center;"
                        },
                    },

                    {
                        field: "StandardCostPerKg", title: "STD CpKg", width: "50px", format: Utility.Cost_Format, attributes: {

                            style: "text-align: left; font-weight:normal;text-align:center;"
                        },
                    },
                    {
                        template: templateFunction,
                        headerTemplate: "<span style='margin-right : 5px;'>Change APL</span>",

                        width: "70px", title: "Include",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center; vertical-align:middle"
                        }
                    }
                ],
                dataSource: {

                    data: mogWiseAPLMasterdataSource,
                    schema: {
                        model: {
                            id: "ArticleID",
                            fields: {
                                ArticleNumber: { type: "string" },
                                ArticleDescription: { type: "string" },
                                UOM: { type: "string" },
                                ArticleType: { type: "string" },
                                Hierlevel3: { type: "string" },
                                MerchandizeCategoryDesc: { type: "string" },
                                MOGName: { type: "string" },
                                AllergensName: { type: "string" },
                                // StandardCostPerKg: { type: "decimal" },
                                StandardCostPerKg: { type: "string" },
                                ArticleCost: { type: "string" },
                                SiteCode: { type: "string" },
                                SiteName: { type: "string" }
                            }
                        }
                    },
                    sort: { field: "StandardCostPerKg", dir: "desc" },
                    // pageSize: 100,
                },

                //maxHeight: 250,
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {

                    }

                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            });

        }
        else if (purpose == "View") {
            $("#btnUnitAPLSelector").hide();
            gridVariable.kendoGrid({
                excel: {
                    fileName: "APLMaster.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                pageable: {
                    numeric: false,
                    previousNext: false,
                    messages: {
                        display: "Total: {2} records"
                    }
                },
                groupable: false,
                scrollable: true,
                noRecords: {
                    template: "No Records Available"
                },

                columns: [
                    {
                        field: "ArticleNumber", title: "Article Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "ArticleDescription", title: "Article Name", width: "120px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "UOM", title: "UOM", width: "30px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

                    //        style: "text-align: left; font-weight:normal;text-align:center;"
                    //    },
                    //},
                    //{
                    //    field: "SiteName", title: "SiteName", width: "50px", attributes: {

                    //        style: "text-align: left; font-weight:normal;text-align:center;"
                    //    },
                    //},
                    {
                        field: "AllergensName", title: "Allergens", width: "50px", attributes: {

                            style: "text-align: left; font-weight:normal;text-align:center;"
                        },
                    }
                    ,
                    {
                        field: "ArticleCost", title: "APL Cost", width: "50px", format: Utility.Cost_Format, attributes: {

                            style: "text-align: left; font-weight:normal;text-align:center;"
                        },
                    },

                    {
                        field: "StandardCostPerKg", title: "STD CpKg", width: "50px", format: Utility.Cost_Format, attributes: {

                            style: "text-align: left; font-weight:normal;text-align:center;"
                        },
                    },
                    //{
                    //    template: templateFunction,
                    //    headerTemplate: "<span style='margin-right : 5px;'>Change APL</span>",

                    //    width: "70px", title: "Include",
                    //    attributes: {
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center; vertical-align:middle"
                    //    }
                    //}
                ],
                dataSource: {

                    data: mogWiseAPLMasterdataSource,
                    schema: {
                        model: {
                            id: "ArticleID",
                            fields: {
                                ArticleNumber: { type: "string" },
                                ArticleDescription: { type: "string" },
                                UOM: { type: "string" },
                                ArticleType: { type: "string" },
                                Hierlevel3: { type: "string" },
                                MerchandizeCategoryDesc: { type: "string" },
                                MOGName: { type: "string" },
                                AllergensName: { type: "string" },
                                StandardCostPerKg: { type: "string" },
                                ArticleCost: { type: "string" },
                                // StandardCostPerKg: { type: "decimal" },
                                // SiteCode: { type: "string" },
                                //SiteName: { type: "string" }
                            }
                        }
                    },
                    sort: { field: "StandardCostPerKg", dir: "desc" },
                    // pageSize: 100,
                },

                //maxHeight: 250,
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {

                    }

                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            });
        }
    }
    //else {

    //gridVariable.html("");
    //gridVariable.kendoGrid({
    //    excel: {
    //        fileName: "APLMaster.xlsx",
    //        filterable: true,
    //        allPages: true
    //    },
    //    sortable: true,
    //    filterable: {
    //        extra: true,
    //        operators: {
    //            string: {
    //                contains: "Contains",
    //                startswith: "Starts with",
    //                eq: "Is equal to",
    //                neq: "Is not equal to",
    //                doesnotcontain: "Does not contain",
    //                endswith: "Ends with"
    //            }
    //        }
    //    },
    //    // pageable: true,
    //    pageable: {
    //        numeric: false,
    //        previousNext: false,
    //        messages: {
    //            display: "Total: {2} records"
    //        }
    //    },
    //    groupable: false,
    //    //reorderable: true,
    //    scrollable: true,
    //    columns: [
    //        {
    //            field: "ArticleNumber", title: "Article Code", width: "50px", attributes: {
    //                style: "text-align: left; font-weight:normal"
    //            }
    //        },
    //        {
    //            field: "ArticleDescription", title: "Article Name", width: "150px", attributes: {
    //                style: "text-align: left; font-weight:normal"
    //            }
    //        },
    //        {
    //            field: "UOM", title: "UOM", width: "30px", attributes: {
    //                style: "text-align: left; font-weight:normal"
    //            },
    //        },
    //        {
    //            field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

    //                style: "text-align: left; font-weight:normal;text-align:center;"
    //            },
    //        },
    //        {
    //            field: "SiteName", title: "SiteName", width: "50px", attributes: {

    //                style: "text-align: left; font-weight:normal;text-align:center;"
    //            },
    //        }
    //        ,
    //        {
    //            field: "ArticleCost", title: "Standard Cost", width: "50px", format: Utility.Cost_Format,
    //            attributes: {

    //                style: "text-align: left; font-weight:normal;text-align:center;"
    //            },
    //        },
    //        {
    //            field: "StandardCostPerKg", title: "Cost Per KG", width: "50px", format: Utility.Cost_Format,
    //            attributes: {

    //                style: "text-align: left; font-weight:normal;text-align:center;"
    //            },
    //        }

    //    ],
    //    dataSource: {
    //        data: mogWiseAPLMasterdataSource,
    //        schema: {
    //            model: {
    //                id: "ArticleID",
    //                fields: {
    //                    ArticleNumber: { type: "string" },
    //                    ArticleDescription: { type: "string" },
    //                    UOM: { type: "string" },
    //                    ArticleCost: { type: "string" },
    //                    StandardCostPerKg: { type: "string" },
    //                    MerchandizeCategoryDesc: { type: "string" },
    //                    MOGName: { type: "string" },
    //                    StandardCostPerKg: { editable: false },
    //                    ArticleCost: { editable: false },
    //                    SiteCode: { type: "string" },
    //                    SiteName: { type: "string" }

    //                }
    //            }
    //        },
    //        // pageSize: 5,
    //    },
    //    // height: 404,
    //    noRecords: {
    //        template: "No Records Available"
    //    },

    //    columnResize: function (e) {
    //        var grid = gridVariable.data("kendoGrid");
    //        e.preventDefault();
    //    },
    //    dataBound: function (e) {


    //    },
    //    excelExport: function onExcelExport(e) {
    //        var sheet = e.workbook.sheets[0];
    //        var data = e.data;
    //        var cols = Object.keys(data[0])
    //        var columns = cols.filter(function (col) {
    //            if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
    //            else
    //                return col;
    //        });
    //        var columns1 = columns.map(function (col) {
    //            return {
    //                value: col,
    //                autoWidth: true,
    //                background: "#7a7a7a",
    //                color: "#fff"
    //            };
    //        });
    //        console.log(columns1);
    //        var rows = [{ cells: columns1, type: "header" }];

    //        for (var i = 0; i < data.length; i++) {
    //            var rowCells = [];
    //            for (var j = 0; j < columns.length; j++) {
    //                var cellValue = data[i][columns[j]];
    //                rowCells.push({ value: cellValue });
    //            }
    //            rows.push({ cells: rowCells, type: "data" });
    //        }
    //        sheet.rows = rows;
    //    }

    //})
    //$(".k-label")[0].innerHTML.replace("items", "records");

    //gridVariable.kendoGrid({
    //    excel: {
    //        fileName: "APLMaster.xlsx",
    //        filterable: true,
    //        allPages: true
    //    },
    //    sortable: true,
    //    filterable: {
    //        extra: true,
    //        operators: {
    //            string: {
    //                contains: "Contains",
    //                startswith: "Starts with",
    //                eq: "Is equal to",
    //                neq: "Is not equal to",
    //                doesnotcontain: "Does not contain",
    //                endswith: "Ends with"
    //            }
    //        }
    //    },
    //    pageable: false,
    //    groupable: false,
    //    scrollable: true,
    //    noRecords: {
    //        template: "No Records Available"
    //    },

    //    columns: [
    //        {
    //            field: "ArticleNumber", title: "Article Code", width: "40px", attributes: {
    //                style: "text-align: left; font-weight:normal"
    //            }
    //        },
    //        {
    //            field: "ArticleDescription", title: "Article Name", width: "120px", attributes: {
    //                style: "text-align: left; font-weight:normal"
    //            }
    //        },
    //        {
    //            field: "UOM", title: "UOM", width: "30px", attributes: {
    //                style: "text-align: left; font-weight:normal"
    //            },
    //        },
    //        //{
    //        //    field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

    //        //        style: "text-align: left; font-weight:normal;text-align:center;"
    //        //    },
    //        //},
    //        //{
    //        //    field: "SiteName", title: "SiteName", width: "50px", attributes: {

    //        //        style: "text-align: left; font-weight:normal;text-align:center;"
    //        //    },
    //        //},
    //        {
    //            field: "AllergensName", title: "Allergens", width: "50px", attributes: {

    //                style: "text-align: left; font-weight:normal;text-align:center;"
    //            },
    //        }
    //        ,
    //        {
    //            field: "ArticleCost", title: "APL Cost", width: "50px", attributes: {

    //                style: "text-align: left; font-weight:normal;text-align:center;"
    //            },
    //        },

    //        {
    //            field: "StandardCostPerKg", title: "STD CpKg", width: "50px", attributes: {

    //                style: "text-align: left; font-weight:normal;text-align:center;"
    //            },
    //        },
    //    ],
    //    dataSource: {

    //        data: mogWiseAPLMasterdataSource,
    //        schema: {
    //            model: {
    //                id: "ArticleID",
    //                fields: {
    //                    ArticleNumber: { type: "string" },
    //                    ArticleDescription: { type: "string" },
    //                    UOM: { type: "string" },
    //                    ArticleType: { type: "string" },
    //                    Hierlevel3: { type: "string" },
    //                    MerchandizeCategoryDesc: { type: "string" },
    //                    MOGName: { type: "string" },
    //                    AllergensName: { type: "string" },
    //                    // StandardCostPerKg: { type: "decimal" },
    //                    // SiteCode: { type: "string" },
    //                    //SiteName: { type: "string" }
    //                }
    //            }
    //        },
    //        sort: { field: "StandardCostPerKg", dir: "desc" },
    //        // pageSize: 100,
    //    },

    //    //maxHeight: 250,
    //    columnResize: function (e) {
    //        var grid = gridVariable.data("kendoGrid");
    //        e.preventDefault();
    //    },
    //    dataBound: function (e) {
    //        var view = this.dataSource.view();
    //        for (var i = 0; i < view.length; i++) {

    //        }

    //    },
    //    excelExport: function onExcelExport(e) {
    //        var sheet = e.workbook.sheets[0];
    //        var data = e.data;
    //        var cols = Object.keys(data[0])
    //        var columns = cols.filter(function (col) {
    //            if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
    //            else
    //                return col;
    //        });
    //        var columns1 = columns.map(function (col) {
    //            return {
    //                value: col,
    //                autoWidth: true,
    //                background: "#7a7a7a",
    //                color: "#fff"
    //            };
    //        });
    //        console.log(columns1);
    //        var rows = [{ cells: columns1, type: "header" }];

    //        for (var i = 0; i < data.length; i++) {
    //            var rowCells = [];
    //            for (var j = 0; j < columns.length; j++) {
    //                var cellValue = data[i][columns[j]];
    //                rowCells.push({ value: cellValue });
    //            }
    //            rows.push({ cells: rowCells, type: "data" });
    //        }
    //        sheet.rows = rows;
    //    }

    //});
    //}


    //$(".k-label")[0].innerHTML.replace("items", "records");
}
//function populateAPLMasterGrid() {
//    Utility.Loading();


//    var gridVariable = $("#gridMOGWiseAPL");
//    gridVariable.html("");
//    gridVariable.kendoGrid({
//        excel: {
//            fileName: "APLMaster.xlsx",
//            filterable: true,
//            allPages: true
//        },
//        sortable: true,
//        filterable: {
//            extra: true,
//            operators: {
//                string: {
//                    contains: "Contains",
//                    startswith: "Starts with",
//                    eq: "Is equal to",
//                    neq: "Is not equal to",
//                    doesnotcontain: "Does not contain",
//                    endswith: "Ends with"
//                }
//            }
//        },
//       // pageable: true,
//        pageable: {
//            numeric: false,
//            previousNext: false,
//            messages: {
//                display: "Total: {2} records"
//            }
//        },
//        groupable: false,
//        //reorderable: true,
//        scrollable: true,
//        columns: [
//            {
//                field: "ArticleNumber", title: "Article Code", width: "50px", attributes: {
//                    style: "text-align: left; font-weight:normal"
//                }
//            },
//            {
//                field: "ArticleDescription", title: "Article Name", width: "150px", attributes: {
//                    style: "text-align: left; font-weight:normal"
//                }
//            },
//            {
//                field: "UOM", title: "UOM", width: "30px", attributes: {
//                    style: "text-align: left; font-weight:normal"
//                },
//            },
//            {
//                field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

//                    style: "text-align: left; font-weight:normal;text-align:center;"
//                },
//            },
//            {
//                field: "SiteName", title: "SiteName", width: "50px", attributes: {

//                    style: "text-align: left; font-weight:normal;text-align:center;"
//                },
//            }
//            ,
//            {
//                field: "ArticleCost", title: "Standard Cost", width: "50px", format: Utility.Cost_Format,
//                attributes: {

//                    style: "text-align: left; font-weight:normal;text-align:center;"
//                },
//            },
//            {
//                field: "StandardCostPerKg", title: "Cost Per KG", width: "50px", format: Utility.Cost_Format,
//                attributes: {

//                    style: "text-align: left; font-weight:normal;text-align:center;"
//                },
//            }

//        ],
//        dataSource: {
//            data: mogWiseAPLMasterdataSource,
//            schema: {
//                model: {
//                    id: "ArticleID",
//                    fields: {
//                        ArticleNumber: { type: "string" },
//                        ArticleDescription: { type: "string" },
//                        UOM: { type: "string" },
//                        ArticleCost: { type: "string" },
//                        StandardCostPerKg: { type: "string" },
//                        MerchandizeCategoryDesc: { type: "string" },
//                        MOGName: { type: "string" },
//                        StandardCostPerKg: { editable: false },
//                        ArticleCost: { editable: false },
//                        SiteCode: { type: "string" },
//                        SiteName: { type: "string" }

//                    }
//                }
//            },
//            // pageSize: 5,
//        },
//        // height: 404,
//        noRecords: {
//            template: "No Records Available"
//        },

//        columnResize: function (e) {
//            var grid = gridVariable.data("kendoGrid");
//            e.preventDefault();
//        },
//        dataBound: function (e) {


//        },
//        excelExport: function onExcelExport(e) {
//            var sheet = e.workbook.sheets[0];
//            var data = e.data;
//            var cols = Object.keys(data[0])
//            var columns = cols.filter(function (col) {
//                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
//                else
//                    return col;
//            });
//            var columns1 = columns.map(function (col) {
//                return {
//                    value: col,
//                    autoWidth: true,
//                    background: "#7a7a7a",
//                    color: "#fff"
//                };
//            });
//            console.log(columns1);
//            var rows = [{ cells: columns1, type: "header" }];

//            for (var i = 0; i < data.length; i++) {
//                var rowCells = [];
//                for (var j = 0; j < columns.length; j++) {
//                    var cellValue = data[i][columns[j]];
//                    rowCells.push({ value: cellValue });
//                }
//                rows.push({ cells: rowCells, type: "data" });
//            }
//            sheet.rows = rows;
//        }

//    })
//    $(".k-label")[0].innerHTML.replace("items", "records");
//}


function checkItemAlreadyExist(selectedId, gridID, isBR) {
    var dataExists = false;
    var gridObj = $("#" + gridID).data("kendoGrid");
    var data = gridObj.dataSource.data();
    var existingData = [];
    if (isBR) { existingData = data.filter(m => m.BaseRecipe_ID == selectedId); }
    else {
        existingData = data.filter(m => m.MOG_ID == selectedId);
        //if (!existingData[0]?.IsActive)
        //    return true;
    }
    if (existingData.length == 2) {
        dataExists = true;
    }
    return dataExists;
}

function validateDataBeforeSave(brGridID, mogGridId) {
    var isValid = true;
    var brGridObj = $("#" + brGridID + "").data("kendoGrid");
    var data = brGridObj.dataSource.data();
    for (var i = 0; i < data.length; i++) {
        if (data[i].BaseRecipe_ID === '' || data[i].BaseRecipe_ID === null || data[i].Quantity === '' || data[i].Quantity === 0 || data[i].Quantity === "0") {
            isValid = false;
        }
    }
    var mogGridObj = $("#" + mogGridId + "").data("kendoGrid");
    var mogdata = mogGridObj.dataSource.data();
    for (var i = 0; i < mogdata.length; i++) {
        if (mogdata[i].MOG_ID === '' || mogdata[i].MOG_ID === null || mogdata[i].UOMName === '' || mogdata[i].UOMName === 'Select' || mogdata[i].UOMName === null ||
            mogdata[i].Quantity === '' || mogdata[i].Quantity === 0 || mogdata[i].Quantity === "0") {
            isValid = false;
        }
    }
    return isValid;
}

function hideShowRecipeWindow(hideWIndow, showWindow) {
    $("#" + hideWIndow + "").hide();
    $("#" + showWindow + "").show();

    var dialog = $("#windowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
}

function checkIsBRConfigurationChange() {
    var gridArray = ["gridMOG", "gridBaseRecipe"];
    for (var i = 0, len = gridArray.length; i < len; i++) {
        isBRConfigurationChange = doesDataSourceHaveChanges(gridArray[i]);
        if (isBRConfigurationChange) {
            break;
        }
    };
}

function checkIsBRNameChange() {
    var brOldName = RecipeName;
    var brNewName = $("#inputrecipename").val();
    if (brOldName != brNewName) {
        isBRNameChange = true;
    }
    else {
        isBRNameChange = false;
    }
}

function doesDataSourceHaveChanges(gridID) {
    var dirty = false;
    var gridObj = $("#" + gridID + "").data("kendoGrid");
    if (gridObj != undefined) {
        dirty = gridObj.dataSource.hasChanges();
        if (!dirty) {
            var previousData = gridObj.dataSource._pristineData;
            var changedData = gridObj.dataSource._data;
            if (previousData.length > 0 && changedData.length > 0) {
                for (var i = 0, len = previousData.length; i < len; i++) {
                    if (previousData[i].Quantity != changedData[i].Quantity) {
                        dirty = true;
                        break;
                    }
                }

            }
        }
    }
    return dirty;
}

function capitalizeFirstLetter(string) {
    
    if (string != null && string != "") {
        return string[0].toUpperCase() + string.slice(1).toLowerCase();
    }
}

function titleCase(string) {
    if (string.length > 0)
        return string.split(" ").map(x => capitalizeFirstLetter(x)).join(" ");
    else return "";
}
function cancelChangesConfirmation(gridId) {
    $(".k-grid-cancel-changes").unbind("mousedown");
    $(".k-grid-cancel-changes").mousedown(function (e) {
         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                var grid = $(gridId).data("kendoGrid");
                grid.cancelChanges();
                //alert('down');
            },
            function () {
            }
        );

    });
}
//function cancelChangesConfirmation(gridId) {
//    //  return;
//    $(".k-grid-cancel-changes").unbind("mousedown");
//    $(".k-grid-cancel-changes").mousedown(function (e) {
//         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
//            function () {
//                //  var grid = $("#" + gridIdin + "").data("kendoGrid");
//                //  if (gridIdin == 'gridBaseRecipe') {
//                //      populateBaseRecipeGrid(resetID);
//                //      return;
//                //  } else if (gridIdin == 'gridMOG') {
//                //      populateMOGGrid(resetID)
//                //      return;
//                //}
//                //  grid.cancelChanges();
//            },
//            function () {
//            }
//        );

//    });
//}
function twodecimalWithoutRound(num) {
    
    var with2Decimals = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
   return with2Decimals
}