﻿//import { Util } from "../bootstrap/js/bootstrap.bundle";

var sectorWiseAPLMasterdataSource = [];
var checkedIds = {};
var checkedAPLCodes = [];
var user;
var status = "";
var varname = "";
var datamodel;
var sectorName = "";
var BATCHNUMBER = "";
var NewsectorName = "";
var sectorCode = "";
var sectorID = 0;
var uomdata = [];
var aplMasterdataSource = [];
var sectorMasterdataSource = [];
var isShowPreview = false;
var CheckedTrueAPLCodes = [];
var sectorWiseAPLArray = [];
var sectorImpactedData = [];
var isStatusSave = false;
var sectorStatus = false;
var isRIEnabled = false;
var issectorAPLMappingSave = false;
var aplFilterDataSource = [];

$(function () {

    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        Utility.Loading();
        setTimeout(function () {
            populatesectorGrid();
            Utility.UnLoading();
        }, 2000);
    });

    
    $('#myInput').on('input', function (e) {
        var grid = $('#gridSectorList').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "SectorNumber" || x.field == "SectorName" || x.field == "IsActive" || x.field == "SectorCode") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        if (x.field == "Status") {
                            var pendingString = "pending";
                            var savedString = "saved";
                            var mappedString = "mapped";
                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "3";
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
    $("#btnExport").click(function (e) {
        var grid = $("#gridSectorList").data("kendoGrid");
        grid.saveAsExcel();
    });

        function populatesectorGrid() {

            debugger;
            var gridVariable = $("#gridSectorList");
            gridVariable.html("");
            gridVariable.kendoGrid({
                excel: {
                    fileName: "SectorMaster.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                groupable: false,

                //pageable: true,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    {
                        field: "SectorNumber", title: "Sector Number", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: left;"
                        }
                    },
                    {
                        field: "SectorName", title: "Sector Name", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },

                    },
                    {
                        field: "IsActive", title: "IsActive", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },

                    {
                        field: "SectorCode", title: "Sector Code", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        //template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Mapped#}#',
                    },



                    {
                        field: "Edit", title: "Action", width: "35px",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                        command: [
                            {
                                name: 'Edit',
                                click: function (e) {

                                    var dialog = $("#SectorindowEdit1").data("kendoWindow");
                                    var tr = $(e.target).closest("tr");
                                    var gridObj = $("#gridSectorList").data("kendoGrid");
                                    tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    // get the date of this row
                                    
                                   // return false; 
                                    $("#sectorid").val(tr.id);

                                    var model = {
                                        "id": tr.id,
                                        "SectorNumber": tr.SectorNumber,
                                        "SectorName": tr.SectorName,
                                        "SectorCode": tr.SectorCode,

                                    };
                                    dialog.open();
                                    dialog.center();

                                    datamodel = model;


                                    $("#txt_sectornumberEdit").val(model.SectorNumber);
                                    $("#txt_sectornameEdit").val(model.SectorName);
                                    $("#txt_sectorcodeEdit").val(model.SectorCode);
                                    return true;
                                }
                            }
                            ,
                            
                        ],
                    },


                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetManageSectorList, function (result) {
                                Utility.UnLoading();
                                if (result != null) {

                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , false);
                        }
                    },
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                SectorNumber: { type: "string" },
                                SectorName: { type: "string" },
                                IsActive: { type: "string" },
                                SectorCode: { type: "string" },
                                
                            }
                        }
                    },
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {

                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if (view[i].Status == 1) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#ffe1e1");

                        }
                        else if (view[i].Status == 2) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#ffffbf");
                        }
                        else {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#d9ffb3");
                        }

                    }
                    var items = e.sender.items();

                    //items.each(function (e) {

                    //    if (user.UserRoleId == 1) {
                    //        $(this).find('.k-grid-Edit').text("Edit");
                    //        $(this).find('.chkbox').removeAttr('disabled');
                    //    } else {
                    //        $(this).find('.k-grid-Edit').text("View");
                    //        $(this).find('.chkbox').attr('disabled', 'disabled');
                    //    }
                    //});

                    
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
            //    if ($(".k-label")[0]!= null)
            //$(".k-label")[0].innerHTML.replace("items", "records");
        }
        
    $("#SectorindowEdit").kendoWindow({
        modal: true,
        width: "462px",
        height: "155px",
        title: "Sector Details Add ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#SectorindowEdit1").kendoWindow({
        modal: true,
        width: "462px",
        height: "155px",
        title: "Sector Details Update ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#SectorcancelEdit").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#SectorindowEdit1").data("kendoWindow");
        orderWindow.close();
    });

       
    $("#AddNew").on("click", function () {
        var model;
        var dialog = $("#SectorindowEdit").data("kendoWindow");
        //dialog.open();
        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });
        dialog.center();
        datamodel = model;
        dialog.title("New Sector Details");
    });

    $("#SectorSubmit").click(function () {
        
        if ($("#txt_sectornumber").val() === "") {
            toastr.error("Please Provide Sector Number");
            $("#txt_sectornumber").focus();
            return false;
        }
        else if ($("#txt_sectorname").val() === "") {
            toastr.error("Please Provide Sector Name");
            $("#txt_sectorname").focus();
            return false;
        }
        else if ($("#txt_sectorcode").val() === "") {
            toastr.error("Please Provide Sector Code");
            $("#txt_sectorcode").focus();
            return false;
        }


        var model = datamodel;

        model = {

            "SectorNumber": $("#txt_sectornumber").val(),
            "SectorName": $("#txt_sectorname").val(),
            "SectorCode": $("#txt_sectorcode").val()
        }

        $("#SectorSubmit").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.SaveManageSectorInfo, function (result) {

            if (result.Data == 'Success') {
                $('#SectorSubmit').removeAttr("disabled");
                toastr.success("Record added successfully");
                $(".k-window").hide();
                $(".k-overlay").hide();

                var orderWindow = $("#SectorindowEdit").data("kendoWindow");
                orderWindow.close();
                $("#gridSectorList").data("kendoGrid").dataSource.data([]);
                $("#gridSectorList").data("kendoGrid").dataSource.read();
                Utility.UnLoading();
                $("#txt_Levelname").val('');
                $("#txt_Leveldesc").val('');
                return;
            }
            else {
                toastr.error("Already Exits Sector Code");
                return;
            }
        }, {
            model: model

        }, true);

        Utility.UnLoading();
        //populateCafeGrid();
    });

    $("#SectorSubmitEdit").click(function () {

        if ($("#txt_sectornumberEdit").val() === "") {
            toastr.error("Please Provide Sector Number");
            $("#txt_sectornumberEdit").focus();
            return false;
        }
        else if ($("#txt_sectornameEdit").val() === "") {
            toastr.error("Please Provide Sector Name");
            $("#txt_sectornameEdit").focus();
            return false;
        }
        else if ($("#txt_sectorcodeEdit").val() === "") {
            toastr.error("Please Provide Sector Code");
            $("#txt_sectorcodeEdit").focus();
            return false;
        }


        var model = datamodel;

        model = {
            "id": $("#sectorid").val(),
            "SectorNumber": $("#txt_sectornumberEdit").val(),
            "SectorName": $("#txt_sectornameEdit").val(),
            "SectorCode": $("#txt_sectorcodeEdit").val()
        }

        $("#SectorSubmitEdit").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.UpdateManageSectorInfo, function (result) {

            if (result.Data == 'Success') {
                $('#SectorSubmitEdit').removeAttr("disabled");
                toastr.success("New Sector record updates successfully");
                $(".k-window").hide();
                $(".k-overlay").hide();


                //return true;

                var orderWindow = $("#SectorindowEdit1").data("kendoWindow");
                orderWindow.close();
                $("#gridSectorList").data("kendoGrid").dataSource.data([]);
                $("#gridSectorList").data("kendoGrid").dataSource.read();
                Utility.UnLoading();
                $("#txt_sectornumberEdit").val('');
                $("#txt_sectornameEdit").val('');
                $("#txt_sectorcodeEdit").val('');
                return;
            }
            else {

                $('#SectorSubmitEdit').removeAttr("disabled");
                toastr.error("Already Exits Sector Code");

                return;
            }
        }, {
            model: model

        }, true);

        Utility.UnLoading();
        //populateCafeGrid();
    });

});

$("document").ready(function () {
    // showsectorAPLDependencies()
    $(".k-window").hide();
    $(".k-overlay").hide();
    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();

    });
});
