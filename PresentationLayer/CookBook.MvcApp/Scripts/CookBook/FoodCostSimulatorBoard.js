﻿//var newdishcatwithdishData = [];
var daypartData = [];
var selecteditemData = []
var noOfDaysData;
var boardData = [];

//var simData = [];
function performSimulationBoardFill(noOfDays, region, daypart, selecteditem) {
    Utility.Loading();
    newsitedishcats = [];
    //Display board
    $("#divBoard").show();
    daypartData = [];
    daypartData = daypart;

    var dataVal = [];
    //This context will always have original items.    
    localStorage.setItem("contextitem", JSON.stringify(selecteditem));

    selecteditemData = [];
    //This data will have current grid data.
    selecteditemData = selecteditem;

    noOfDaysData = noOfDays;
    //Create a HTML Table element.
    var table = document.createElement("TABLE");
    //table.border = "1";
    table.className = "maingrid";
    table.id = "table1";
    //Create a HTML Table element.
    //var tableBody = document.createElement("TABLE");
    //tableBody.border = "1";

    //table will have only header row
    //tableBody will have all the body

    //Add the header row. table
    var row = table.insertRow(-1);

    var hdrtable = document.createElement("TABLE");
    hdrtable.id = "header-fixed";
    //var hdrrow = hdrtable.insertRow(-1);
    for (var i = 0; i <= noOfDays; i++) {
        var headerCell = document.createElement("TH");
        headerCell.className = "mg_top_header";
        if (i == 0) {
            headerCell.innerHTML = "Items & Dishes";
            headerCell.style.width = "240px";
        }
        else {
            headerCell.innerHTML = "Day " + i;
            //headerCell.style.width = "200px";
        }
        row.appendChild(headerCell);
    }
    //row.appendChild(hdrtable);

    //Add the daypart row.
    for (var dp = 0; dp < daypart.length; dp++) {
        var row = table.insertRow(-1);

        for (var i = 0; i <= noOfDays; i++) {
            if (i == 0) {
                var headerCell = document.createElement("td");
                headerCell.className = "mg_top_header_dp_title_text"
                headerCell.style.width = "240px";
                var divEle = document.createElement("div");
                //divEle.style.width = "167px";
                divEle.style.width = "240px";
                divEle.innerHTML = daypart[dp].Name;
                headerCell.appendChild(divEle);
                row.appendChild(headerCell);
            }
            else {
                var headerCell = document.createElement("td");
                headerCell.className = "mg_top_header_dp_title"
                var tableHead = document.createElement("TABLE");
                //tableHead.style.width = "200px";
                tableHead.style.minHeight = "35px";
                var rowth = tableHead.insertRow(-1);
                var headerCellth = document.createElement("TH");
                //headerCellth.style.width = "60px";
                headerCellth.style.width = "30%";
                headerCellth.style.textAlign = "center";
                headerCellth.style.border = "1px solid #fff";
                //headerCellth.style.borderLeft = "1px solid #fff";
                headerCellth.innerHTML = "Pax";
                rowth.appendChild(headerCellth);
                headerCellth = document.createElement("TH");
                headerCellth.innerHTML = "Qty";
                /*headerCellth.style.width = "60px";*/
                headerCellth.style.width = "30%";
                headerCellth.style.textAlign = "center";
                headerCellth.style.paddingLeft = "4px";
                headerCellth.style.textAlign = "center";
                headerCellth.style.border = "#fff solid 1px";
                rowth.appendChild(headerCellth);
                headerCellth = document.createElement("TH");
                headerCellth.innerHTML = "Cost";
                headerCellth.style.textAlign = "center";
                headerCellth.style.border = "#fff solid 1px";
                rowth.appendChild(headerCellth);

                headerCell.appendChild(tableHead);
                row.appendChild(headerCell);

            }
        }
        ////Go through items

        for (var it = 0; it < selecteditem.length; it++) {
            var row2 = table.insertRow(-1);
            row2.className = "row-striped";
            row2.classList.add("expand_" + selecteditem[it].value);

            //console.log(daypart[dp].DayPartCode)
            //console.log(selecteditem[it].DayPartCode)
            // console.log("selecteditem"+selecteditem)
            var itemDATA = selecteditem[it];
            //console.log("itemDATA" + itemDATA.DishCategories);
            //console.log("itemDATAVALUE" + itemDATA.value);
            console.log("go through items")
            console.log(daypart[dp].DayPartCode)
            console.log(selecteditem[it].dpcode)
            //Below loop will only run for the current day part that primary for loop is currently on
            if (daypart[dp].DayPartCode == selecteditem[it].dpcode) {
                //console.log('indi')
                for (var i = 0; i <= noOfDays; i++) {
                    if (i == 0) {
                        var headerCell = document.createElement("td");
                        headerCell.className = "mg_top_header_item_title";
                        headerCell.style.width = "240px";
                        var div = document.createElement("div");
                        var sourcePath = selecteditem[it].text;
                        var lastIndex = sourcePath.lastIndexOf("-");
                        var requiredPath = sourcePath.slice(0, lastIndex);
                        div.innerHTML = requiredPath;
                        //div.id = "span_" + daypart[dp].DayPartCode;
                        div.id = "span_" + selecteditem[it].value;
                        div.style.cursor = "pointer";
                        var spanup = document.createElement("i");
                        spanup.className = "k-icon k-i-arrow-60-up ac";
                        var spandown = document.createElement("i");
                        spandown.style.display = "none";
                        spandown.className = "k-icon k-i-arrow-60-down";
                        div.addEventListener('click', function () {
                            //result.textContent = this.value;
                            var dpcode = this.id.split('_')[2];
                            var itid = this.id.split('_')[1];
                            onClickExpand(this, dpcode, itid);
                        });

                        div.appendChild(spanup);
                        div.appendChild(spandown);
                        headerCell.appendChild(div);
                        //headerCell.innerHTML = selecteditem[it].text +"  <span class='k-icon k-i-arrow-60-up' ></span>";
                        row2.appendChild(headerCell);
                        row2.classList.add("simBoardItemHeaderRow");

                    }
                    else {
                        var headerCell = document.createElement("TD");
                        //headerCell.style.padding = "0";
                        headerCell.style.paddingLeft = "2px";
                        var inputTxt = document.createElement("input");
                        inputTxt.type = "text";
                        var itemidvalue = selecteditem[it].value.split('_')[0];
                        inputTxt.id = "txtPxroot_" + it + "_" + itemidvalue + "_" + i + "_" + selecteditem[it].dpcode;
                        inputTxt.className = "txtPxroot_" + itemidvalue + "_" + i + "_" + selecteditem[it].dpcode;
                        inputTxt.style.width = "50px";
                        //inputTxt.oninput = "onInputPax(" + inputTxt + "," + selecteditem[it].value + ")";
                        inputTxt.addEventListener('input', function () {
                            //result.textContent = this.value;
                            var itemid = this.id.split('_')[2];
                            var dpcode = this.id.split('_')[4];
                            var colindex = this.id.split('_')[3];
                            onInputPax(this, itemid, dpcode, colindex);
                        });
                        var tableHead = document.createElement("TABLE");
                        //tableHead.style.width = "100%";
                        //tableHead.style.width = "180px";
                        //tableHead.style.width = "200px";
                        var rowth = tableHead.insertRow(-1);
                        var headerCellth = document.createElement("TD");
                        //headerCellth.style.width = "60px";
                        headerCellth.style.width = "30%";
                        headerCellth.style.textAlign = "center";
                        headerCellth.className = "mg_top_header_item_text";
                        headerCellth.appendChild(inputTxt);
                        rowth.appendChild(headerCellth);
                        headerCellth = document.createElement("TD");
                        /*headerCellth.style.width = "60px";*/
                        headerCellth.style.width = "30%";
                        headerCellth.style.textAlign = "center";
                        //headerCellth.style.paddingLeft = "0px";
                        headerCellth.innerHTML = "";
                        headerCellth.style.textAlign = "center";
                        rowth.appendChild(headerCellth);
                        headerCellth = document.createElement("TD");
                        headerCellth.innerHTML = "";
                        headerCellth.style.textAlign = "center";
                        rowth.appendChild(headerCellth);

                        headerCell.appendChild(tableHead);
                        row2.appendChild(headerCell);
                    }
                }

                //var divdishCategoryMain = document.createElement("table");
                //divdishCategoryMain.className = "expand_dc_" + daypart[dp].DayPartCode;

                //console.log("selecteditem[it].text" + selecteditem[it].text)
                var itemidvalue = selecteditem[it].value.split('_')[0];

                //fix for slide toggle
                var rowAddDcST = table.insertRow(-1);
                headerCellthST = document.createElement("TD");
                headerCellthST.style.padding = "0";
                headerCellthST.setAttribute("colspan", noOfDays + 1);
                //headerCellthST.innerHTML = "-"               

                var tableHeadST = document.createElement("TABLE");
                tableHeadST.style.width = "100%";
                tableHeadST.id = "tableForST_" + selecteditem[it].dpcode + "_" + itemidvalue;




                ////Add Dish Category
                //var rowAddDc = table.insertRow(-1);
                var rowAddDc = tableHeadST.insertRow(-1);

                rowAddDc.id = "rowAddDc_" + selecteditem[it].dpcode + "_" + itemidvalue;
                //var rowAddDc = divdishCategoryMain.insertRow(-1);
                rowAddDc.className = "row-striped";
                //rowAddDc.classList.add("expand_dc_" + daypart[dp].DayPartCode);
                rowAddDc.classList.add("expand_dc_" + selecteditem[it].value);
                for (var i = 0; i <= noOfDays; i++) {
                    if (i == 0) {
                        var headerCell = document.createElement("td");
                        headerCell.className = "mg_top_header_dc_title";
                        //var link = "Add Dish Category <a style='cursor:pointer;text-decoration:none;' class='k-icon k-i-plus' onclick='getDishCategoryByItemID(" + selecteditem[it].value + ")'></a>";
                        //headerCell.innerHTML = link;
                        var divAddDc = document.createElement("div");
                        divAddDc.innerHTML = "Add Dish Category";
                        //divAddDc.style.width = "150px";
                        //divAddDc.style.width = "182px";
                        divAddDc.style.width = "222px";

                        divAddDc.style.cursor = "pointer";
                        //add 11th
                        divAddDc.style.paddingLeft = "17px";
                        divAddDc.id = "divAddDc_" + selecteditem[it].dpcode + "_" + itemidvalue;
                        var spanAddDc = document.createElement("i");
                        spanAddDc.className = "fas fa-plus";
                        spanAddDc.id = "spanAddDc_" + selecteditem[it].dpcode + "_" + itemidvalue;
                        //spanAddDc.style.marginLeft = "23px";
                        //spanAddDc.style.marginLeft = "71px";
                        spanAddDc.style.marginLeft = "110px";
                        spanAddDc.style.color = "lightseagreen";

                        spanAddDc.addEventListener('click', function () {
                            //result.textContent = this.value;
                            var dpcode = this.id.split('_')[1];
                            var itemid = this.id.split('_')[2];
                            //onClickExpandDishes(this, dpcode, dccode);
                            addDishCategory(dpcode, itemid);
                        });
                        divAddDc.appendChild(spanAddDc);
                        headerCell.appendChild(divAddDc);
                        //headerCell.style.paddingLeft = "25px";
                        //headerCell.style.width = "160px";
                        headerCell.style.width = "240px";
                        rowAddDc.appendChild(headerCell);
                    }
                    else {
                        if (i == 1) {
                            var headerCell = document.createElement("TD");
                            headerCell.className = "mg_top_header_dc_title";
                            headerCell.setAttribute("colspan", noOfDays);
                            var tableHead = document.createElement("TABLE");
                            tableHead.style.width = ($(".card-header").width() - 240) + "px!important";
                            tableHead.id = "tableAddDC_" + selecteditem[it].dpcode + "_" + itemidvalue;
                            tableHead.className = "cardHeaderInheritance";
                            //var rowth = tableHead.insertRow(-1);
                            //var headerCellth = document.createElement("TD");
                            //headerCellth.style.width = "60px";
                            //headerCellth.innerHTML = "";
                            //rowth.appendChild(headerCellth);
                            //headerCellth = document.createElement("TD");
                            //headerCellth.innerHTML = ""
                            //rowth.appendChild(headerCellth);
                            //headerCellth = document.createElement("TD");
                            //headerCellth.innerHTML = ""
                            //rowth.appendChild(headerCellth);

                            headerCell.appendChild(tableHead);
                            rowAddDc.appendChild(headerCell);
                        }
                    }
                }
                //divdishCategoryMain.appendChild(rowAddDc);
                //var rowNewTab = table.insertRow(-1);     
                //rowNewTab.appendChild(divdishCategoryMain);
                //Level 3 get dishcategories
                //Utility.Loading();
                if (daypart[dp].DayPartCode == selecteditem[it].dpcode) {
                    for (var dsc = 0; dsc < itemDATA.DishCategories.length; dsc++) {
                        newsitedishcats.push(itemDATA.DishCategories[dsc]);
                        //var haveDishes = itemDATA.DishCategories[dsc].Dishes.length;
                        ////Fix for issue : Only dish category row was generated, even if it was deleted
                        //if (haveDishes > 0) {

                        //boardData.push(["ItemDetails", [["ItemName", itemDATA.text], ["DayPartCode", itemDATA.dpcode], ["ItemID", itemDATA.value]], ["DishCategories", dataSource[dc]]]);
                        //var row3 = table.insertRow(-1);
                        var row3 = tableHeadST.insertRow(-1);
                        //var row3 = divdishCategoryMain.insertRow(-1);
                        row3.id = "row_AddDc_" + daypart[dp].DayPartCode + "_" + itemDATA.DishCategories[dsc].DishCategoryCode + "_" + itemidvalue;
                        row3.className = "row-striped";
                        //row3.classList.add("expand_dc_" + daypart[dp].DayPartCode);
                        row3.classList.add("expand_dc_" + selecteditem[it].value);
                        row3.classList.add("row_dc_" + daypart[dp].DayPartCode + "_" + itemDATA.DishCategories[dsc].DishCategoryCode + "_" + itemidvalue);
                        //itemdata.push({ "value": dataSource[i].ItemID, "text": dataSource[i].ItemName, "code": dataSource[i].ItemCode, "dpcode": dataSource[i].DayPartCode });
                        for (var i = 0; i <= noOfDays; i++) {
                            if (i == 0) {
                                var headerCell = document.createElement("td");
                                headerCell.className = "mg_top_header_dc_body";
                                //headerCell.innerHTML = itemDATA.DishCategories[dsc].DishCategoryName;
                                var div = document.createElement("div");
                                //11th
                                div.style.paddingLeft = "17px";
                                //div.innerHTML = itemDATA.DishCategories[dsc].DishCategoryName;
                                div.id = "span_dc_" + daypart[dp].DayPartCode + "_" + itemDATA.DishCategories[dsc].DishCategoryCode;
                                var spanText = document.createElement("div");
                                //spanText.style.width = "114px";
                                //spanText.style.width = "163px";
                                spanText.style.width = "203px";

                                spanText.style.display = "inline-block";

                                spanText.innerHTML = itemDATA.DishCategories[dsc].DishCategoryName;

                                var spanAddDc = document.createElement("i");
                                spanAddDc.className = "fas fa-plus";
                                spanAddDc.style.paddingLeft = "5px";
                                spanAddDc.style.color = "lightseagreen";
                                spanAddDc.style.cursor = "pointer";
                                //var itemidvalue = selecteditem[it].value.split('_')[0];
                                spanAddDc.id = "spanAddDc_" + selecteditem[it].dpcode + "_" + itemidvalue + "_" + itemDATA.DishCategories[dsc].DishCategoryCode;
                                //spanAddDc.style.marginLeft = "23px";
                                spanAddDc.addEventListener('click', function () {
                                    //result.textContent = this.value;
                                    var dpcode = this.id.split('_')[1];
                                    var itemid = this.id.split('_')[2];
                                    var dccode = this.id.split('_')[3];
                                    //onClickExpandDishes(this, dpcode, dccode);
                                    addDish(dpcode, itemid, dccode);
                                });
                                spanText.appendChild(spanAddDc);

                                div.appendChild(spanText);
                                //div.style.cursor = "pointer";
                                //var spanup = document.createElement("span");
                                //spanup.className = "k-icon k-i-arrow-60-up ac";
                                //var spandown = document.createElement("span");
                                //spandown.style.display = "none";
                                //spandown.className = "k-icon k-i-arrow-60-down";
                                //div.addEventListener('click', function () {
                                //    //result.textContent = this.value;
                                //    var dccode = this.id.split('_')[3];
                                //    var dpcode = this.id.split('_')[2];
                                //    onClickExpandDishes(this,dpcode, dccode);
                                //});
                                //div.appendChild(spanup);
                                //div.appendChild(spandown);


                                var spanCloseBtn = document.createElement("span");
                                spanCloseBtn.className = "fas fa-times";
                                spanCloseBtn.id = "span_delDC_" + daypart[dp].DayPartCode + "_" + itemDATA.DishCategories[dsc].DishCategoryCode + "_" + itemidvalue;
                                //spanCloseBtn.style.marginLeft = "98px";
                                spanCloseBtn.addEventListener('click', function () {

                                    var dccode = this.id.split('_')[3];
                                    var dpcode = this.id.split('_')[2];
                                    var itid = this.id.split('_')[4];
                                    //deleteDishCategory(table, dpcode, dccode, itid);
                                    deleteDishCategory(tableHeadST, dpcode, dccode, itid);

                                });
                                div.appendChild(spanCloseBtn);
                                headerCell.appendChild(div);
                                //headerCell.style.width = "160px";      
                                headerCell.style.width = "240px";
                                //headerCell.style.paddingLeft = "25px";
                                row3.appendChild(headerCell);
                            }
                            else if (i == 1) {
                                var headerCell = document.createElement("TD");
                                headerCell.className = "mg_top_header_ds_title";
                                headerCell.setAttribute("colspan", noOfDays);
                                var tableHead = document.createElement("TABLE");
                                tableHead.style.width = ($(".card-header").width() - 240) + "px!important";
                                tableHead.id = "tableAddDS_" + selecteditem[it].dpcode + "_" + itemidvalue + "_" + itemDATA.DishCategories[dsc].DishCategoryCode;
                                tableHead.className = "cardHeaderInheritance";
                                //var rowth = tableHead.insertRow(-1);
                                //var headerCellth = document.createElement("TD");
                                //headerCellth.style.width = "60px";
                                //headerCellth.innerHTML = "";
                                //rowth.appendChild(headerCellth);
                                //headerCellth = document.createElement("TD");
                                //headerCellth.innerHTML = ""
                                //rowth.appendChild(headerCellth);
                                //headerCellth = document.createElement("TD");
                                //headerCellth.innerHTML = ""
                                //rowth.appendChild(headerCellth);

                                headerCell.appendChild(tableHead);
                                row3.appendChild(headerCell);
                            }
                            //else {
                            //    var headerCell = document.createElement("TD");
                            //    headerCell.className = "mg_top_header_dc_body";
                            //    var tableHead = document.createElement("TABLE");
                            //    //tableHead.style.width = "200px";
                            //    var rowth = tableHead.insertRow(-1);
                            //    var headerCellth = document.createElement("TD");
                            //    //headerCellth.style.width = "60px";
                            //    headerCellth.style.width = "30%";
                            //    headerCellth.style.textAlign = "center";
                            //    headerCellth.innerHTML = "";
                            //    rowth.appendChild(headerCellth);
                            //    headerCellth = document.createElement("TD");
                            ////headerCellth.style.width = "60px";
                            //    headerCellth.style.width = "30%";
                            //    headerCellth.style.textAlign = "center";
                            //    headerCellth.innerHTML = ""
                            //    rowth.appendChild(headerCellth);
                            //    headerCellth = document.createElement("TD");
                            //    headerCellth.innerHTML = ""
                            //    rowth.appendChild(headerCellth);

                            //    headerCell.appendChild(tableHead);
                            //    row3.appendChild(headerCell);
                            //}
                        }
                        //divdishCategoryMain.appendChild(row3);
                        //Level 4 dishes

                        for (var dis = 0; dis < itemDATA.DishCategories[dsc].Dishes.length; dis++) {
                            //console.log(itemDATA.DishCategories[dsc].Dishes[dis].DishName)
                            //console.log(itemDATA.DishCategories[dsc].Dishes[dis].DishID)
                            //var row4 = table.insertRow();
                            var row4 = tableHeadST.insertRow();
                            //var row4 = divdishCategoryMain.insertRow(-1);
                            row4.className = "row-striped";
                            //row4.classList.add("expand_dc_" + daypart[dp].DayPartCode);
                            row4.classList.add("expand_dc_" + selecteditem[it].value);
                            row4.classList.add("expand_dc_" + daypart[dp].DayPartCode + "_" + itemDATA.DishCategories[dsc].DishCategoryCode);
                            row4.classList.add("row_ds_" + daypart[dp].DayPartCode + "_" + itemDATA.DishCategories[dsc].DishCategoryCode + "_" + itemDATA.DishCategories[dsc].Dishes[dis].DishID + "_" + selecteditem[it].value.split('_')[0]);
                            row4.classList.add("dishes_forslidingpurpose");
                            for (var i = 0; i <= noOfDays; i++) {
                                if (i == 0) {
                                    var headerCell = document.createElement("td");
                                    headerCell.className = "mg_top_header_ds_body";
                                    //headerCell.innerHTML = itemDATA.DishCategories[dsc].Dishes[dis].DishName;

                                    //headerCell.style.width = "160px";      
                                    headerCell.style.width = "240px";
                                    //headerCell.style.paddingLeft = "50px";

                                    var divCb = document.createElement("div");
                                    //divCb.innerHTML = itemDATA.DishCategories[dsc].Dishes[dis].DishName;
                                    var spanText = document.createElement("span");
                                    //spanText.style.width = "72px";
                                    //spanText.style.width = "163px";
                                    spanText.style.width = "160px";

                                    spanText.style.display = "inline-block";
                                    spanText.innerHTML = itemDATA.DishCategories[dsc].Dishes[dis].DishName;

                                    //Recipe button
                                    var spanRecipeBtn = document.createElement("span");
                                    //spanRecipeBtn.className = "fa fa-solid fa-link";
                                    spanRecipeBtn.className = "fas fa-utensils";
                                    spanRecipeBtn.style.color = "deepskyblue";
                                    spanRecipeBtn.id = "span_recipeDC_" + daypart[dp].DayPartCode + "_" + itemDATA.DishCategories[dsc].DishCategoryCode + "_" + itemDATA.DishCategories[dsc].Dishes[dis].DishID + "_" + selecteditem[it].value.split('_')[0] + "_" + itemDATA.DishCategories[dsc].Dishes[dis].DishCode;
                                    //spanCloseBtn.style.marginLeft = "68px";
                                    spanRecipeBtn.addEventListener('click', function () {

                                        var dccode = this.id.split('_')[3];
                                        var dpcode = this.id.split('_')[2];
                                        var did = this.id.split('_')[4];
                                        var itemid = this.id.split('_')[5];
                                        var dishcode = this.id.split('_')[6];
                                        openRecipe(itemid, did, dccode, dishcode, dpcode);
                                    });

                                    //11th
                                    divCb.style.paddingLeft = "42px";
                                    divCb.appendChild(spanRecipeBtn);
                                    divCb.appendChild(spanText);

                                    //Delete dish button
                                    var spanCloseBtn = document.createElement("span");
                                    spanCloseBtn.className = "fas fa-times";
                                    spanCloseBtn.id = "span_delDC_" + daypart[dp].DayPartCode + "_" + itemDATA.DishCategories[dsc].DishCategoryCode + "_" + itemDATA.DishCategories[dsc].Dishes[dis].DishID + "_" + selecteditem[it].value.split('_')[0];
                                    //spanCloseBtn.style.marginLeft = "68px";
                                    spanCloseBtn.addEventListener('click', function () {

                                        var dccode = this.id.split('_')[3];
                                        var dpcode = this.id.split('_')[2];
                                        var did = this.id.split('_')[4];
                                        var itid = this.id.split('_')[5];
                                        deleteDish(row4, dpcode, dccode, did, itid);
                                    });
                                    divCb.appendChild(spanCloseBtn);
                                    headerCell.appendChild(divCb);
                                    row4.appendChild(headerCell);
                                }
                                else {
                                    var dataO = itemDATA.DishCategories[dsc].Dishes[dis].dataO;

                                    if (dataO != undefined)
                                        dataVal = dataO;

                                    //console.log("dataO")
                                    //console.log(dataO)

                                    var headerCell = document.createElement("TD");
                                    headerCell.style.paddingRight = "0";
                                    headerCell.style.paddingLeft = "3px";
                                    var inputTxt = document.createElement("input");
                                    inputTxt.className = "txtPax_" + itemidvalue + "_" + selecteditem[it].dpcode + "_" + i;
                                    inputTxt.classList.add("txtTotalPax" + i);
                                    inputTxt.type = "text";
                                    //inputTxt.id = "txtDsxroot_" + dis + "_" +  itemDATA.DishCategories[dsc].Dishes[dis].DishID;
                                    inputTxt.id = "txtPax_" + i + "_" + itemidvalue + "_" + selecteditem[it].dpcode + "_" + itemDATA.DishCategories[dsc].Dishes[dis].DishID;
                                    inputTxt.setAttribute("data-cost", itemDATA.DishCategories[dsc].Dishes[dis].CostPerKG);
                                    inputTxt.style.width = "50px";
                                    inputTxt.style.marginLeft = "2px";
                                    inputTxt.addEventListener('input', function () {
                                        //result.textContent = this.value;
                                        var itemid = this.id.split('_')[2];
                                        var dpcode = this.id.split('_')[3];
                                        var cost = this.getAttribute("data-cost");
                                        var colIndex = this.id.split('_')[1];
                                        var dishid = this.id.split('_')[4];
                                        var txtGrmInp = "txtGrmroot_" + itemid + "_" + dpcode + "_" + cost + "_" + colIndex + "_" + dishid;
                                        onInputGrm(txtGrmInp, itemid, dpcode, cost, colIndex, dishid);

                                        //calculateTotals();
                                    });
                                    if (dataO != undefined)
                                        inputTxt.value = dataO.filter(a => a.index == i)[0].paxTxt;

                                    var inputTxtGrm = document.createElement("input");
                                    inputTxtGrm.type = "text";
                                    inputTxtGrm.id = "txtGrmroot_" + itemidvalue + "_" + selecteditem[it].dpcode + "_" + itemDATA.DishCategories[dsc].Dishes[dis].CostPerKG + "_" + i + "_" + itemDATA.DishCategories[dsc].Dishes[dis].DishID;
                                    inputTxtGrm.style.width = "50px";
                                    inputTxtGrm.style.marginLeft = "8px";
                                    inputTxtGrm.className = "txtTotalGrm" + i;
                                    inputTxtGrm.classList.add("txtGrmroot_" + i + "_" + itemidvalue + "_" + selecteditem[it].dpcode + "_" + itemDATA.DishCategories[dsc].Dishes[dis].DishID);
                                    inputTxtGrm.classList.add("txtGrmroot_" + i + "_" + itemidvalue + "_" + selecteditem[it].dpcode);
                                    inputTxtGrm.classList.add("txtGrmroots");
                                    //inputTxtGrm.addEventListener('keypress', function () {
                                    //    validateFloatKeyPress(this, event);
                                    //});                                    
                                    inputTxtGrm.addEventListener('input', function () {
                                        //result.textContent = this.value;      
                                        validateU2D(this);
                                        var itemid = this.id.split('_')[1];
                                        var dpcode = this.id.split('_')[2];
                                        var cost = this.id.split('_')[3];
                                        var colIndex = this.id.split('_')[4];
                                        var dishid = this.id.split('_')[5];
                                        var txtGrmInp = "txtGrmroot_" + itemid + "_" + dpcode + "_" + cost + "_" + colIndex + "_" + dishid;
                                        onInputGrm(txtGrmInp, itemid, dpcode, cost, colIndex, dishid);
                                    });
                                    if (dataO != undefined) {
                                        var txt = dataO.filter(a => a.index == i)[0].grmTxt
                                        if (txt != "null")
                                            inputTxtGrm.value = Utility.MathRound(txt, 2);
                                        //inputTxtGrm.value = txt;
                                    }

                                    var tableHead = document.createElement("TABLE");
                                    //tableHead.style.width = "200px";
                                    //tableHead.style.width = "175px";
                                    var rowth = tableHead.insertRow(-1);
                                    var headerCellth = document.createElement("TD");
                                    //headerCellth.style.width = "60px";
                                    headerCellth.style.width = "30%";
                                    headerCellth.style.textAlign = "center";
                                    headerCellth.className = "mg_top_header_item_text";
                                    headerCellth.appendChild(inputTxt);
                                    rowth.appendChild(headerCellth);
                                    headerCellth = document.createElement("TD");
                                    //headerCellth.style.width = "50px";
                                    //headerCellth.style.width = "60px";
                                    headerCellth.style.width = "30%";
                                    headerCellth.style.textAlign = "center";
                                    headerCellth.className = "mg_top_header_item_text"
                                    headerCellth.appendChild(inputTxtGrm);
                                    rowth.appendChild(headerCellth);
                                    headerCellth = document.createElement("TD");
                                    headerCellth.style.width = "50px";
                                    //headerCellth.style.width = "30%";
                                    headerCellth.style.textAlign = "center";
                                    //7th April
                                    headerCellth.style.paddingBottom = "0";
                                    var lblCost = document.createElement("label");
                                    lblCost.id = "lblCost_" + i + "_" + itemidvalue + "_" + selecteditem[it].dpcode + "_" + itemDATA.DishCategories[dsc].Dishes[dis].DishID;
                                    lblCost.innerHTML = "-";
                                    lblCost.className = "lblTotalCost" + i;
                                    lblCost.style.width = "50px";
                                    //lblCost.style.marginLeft = "17px";
                                    lblCost.classList.add("lblCostForItem_" + itemidvalue + "_" + selecteditem[it].dpcode);
                                    if (dataO != undefined) {
                                        var txt = dataO.filter(a => a.index == i)[0].costTxt
                                        if (txt != "null")
                                            lblCost.innerHTML = txt;


                                    }
                                    headerCellth.appendChild(lblCost);
                                    rowth.appendChild(headerCellth);

                                    headerCell.appendChild(tableHead);
                                    row4.appendChild(headerCell);
                                }
                            }
                            //divdishCategoryMain.appendChild(row3);
                        }
                        //}
                    }
                }



                headerCellthST.appendChild(tableHeadST)
                rowAddDcST.appendChild(headerCellthST);

            }
            //HttpClient.MakeRequest(CookBookMasters.GetSimulationRegionItemDishCategories, function (data) {
            //    //Utility.UnLoading();

            //    var dataSource = data;    
            //    console.log(dataSource);
            //    for (var dc = 0; dc < dataSource.length; dc++) {

            //        boardData.push(["ItemDetails",[["ItemName",itemDATA.text],["DayPartCode", itemDATA.dpcode],["ItemID", itemDATA.value]],["DishCategories", dataSource[dc]]]);
            //        var row3 = table.insertRow(-1);
            //        //itemdata.push({ "value": dataSource[i].ItemID, "text": dataSource[i].ItemName, "code": dataSource[i].ItemCode, "dpcode": dataSource[i].DayPartCode });
            //        for (var i = 0; i <= noOfDays; i++) {
            //            if (i == 0) {
            //                var headerCell = document.createElement("td");
            //                headerCell.innerHTML = dataSource[dc].DishCategoryName;
            //                row3.appendChild(headerCell);
            //            }
            //            else {
            //                var headerCell = document.createElement("TD");                            
            //                var tableHead = document.createElement("TABLE");
            //                tableHead.style.width = "150px";
            //                var rowth = tableHead.insertRow(-1);
            //                var headerCellth = document.createElement("TD");
            //                headerCellth.style.width = "60px";
            //                headerCellth.innerHTML="";
            //                rowth.appendChild(headerCellth);
            //                headerCellth = document.createElement("TD");
            //                headerCellth.innerHTML = ""
            //                rowth.appendChild(headerCellth);
            //                headerCellth = document.createElement("TD");
            //                headerCellth.innerHTML = ""
            //                rowth.appendChild(headerCellth);

            //                headerCell.appendChild(tableHead);
            //                row3.appendChild(headerCell);
            //            }
            //        }
            //        //Level 4 dishes

            //        for (var dis = 0; dis < dataSource[dc].Dishes.length; dis++) {
            //            console.log(dataSource[dc].Dishes[dis].DishName)
            //            console.log(dataSource[dc].Dishes[dis].DishID)
            //            var row4 = table.insertRow();
            //            for (var i = 0; i <= noOfDays; i++) {
            //                if (i == 0) {
            //                    var headerCell = document.createElement("td");
            //                    headerCell.innerHTML = dataSource[dc].Dishes[dis].DishName;
            //                    row4.appendChild(headerCell);
            //                }
            //                else {
            //                    var headerCell = document.createElement("TD");
            //                    var inputTxt = document.createElement("input");
            //                    inputTxt.type = "text";
            //                    inputTxt.id = "txtDsx_root_"+dis+"_"+ + dataSource[dc].Dishes[dis].DishID;
            //                    inputTxt.style.width = "50px";
            //                    var tableHead = document.createElement("TABLE");
            //                    tableHead.style.width = "150px";
            //                    var rowth = tableHead.insertRow(-1);
            //                    var headerCellth = document.createElement("TD");
            //                    headerCellth.style.width = "60px";
            //                    headerCellth.appendChild(inputTxt);
            //                    rowth.appendChild(headerCellth);
            //                    headerCellth = document.createElement("TD");
            //                    headerCellth.innerHTML = "-"
            //                    rowth.appendChild(headerCellth);
            //                    headerCellth = document.createElement("TD");
            //                    headerCellth.innerHTML = "-"
            //                    rowth.appendChild(headerCellth);

            //                    headerCell.appendChild(tableHead);
            //                    row4.appendChild(headerCell);
            //                }
            //            }
            //        }

            //    }
            //    Utility.UnLoading();
            //    console.log(boardData);
            //}, { itemId: selecteditem[it].value }, false);
        }

        ////auto slide to hide
        //onClickExpand(div, daypart[dp].DayPartCode);
    }

    //Day Wise Total
    var rowDwt = table.insertRow(-1);
    rowDwt.className = "row-striped";
    for (var i = 0; i <= noOfDays; i++) {
        var headerCell = document.createElement("td");
        headerCell.className = "mg_bottom_header";
        if (i == 0) {
            headerCell.innerHTML = "Day Wise Total";
            rowDwt.appendChild(headerCell);
        }
        else {
            //for (var it = 0; it < selecteditem.length; it++) {
            //    for (var dp = 0; dp < daypart.length; dp++) {
            //        var txtpax = ".txtPax_" + selecteditem[it].value + "_" + daypart[dp].dpcode + "_" + i;

            //    }
            //}
            //headerCell.innerHTML = "Day " + i;
            var headerCell = document.createElement("TD");
            headerCell.className = "mg_bot_header_dc_title";
            headerCell.style.border = "1px solid rgb(255, 255, 255)!important";
            var tableHead = document.createElement("TABLE");
            //April 7th
            tableHead.style.width = "160px";
            var rowth = tableHead.insertRow(-1);
            var headerCellth = document.createElement("TD");
            headerCellth.style.paddingLeft = "0";
            headerCellth.style.width = "30%";
            headerCellth.style.border = "1px solid rgb(255, 255, 255)!important";
            var lblPaxTotal = document.createElement("label");
            lblPaxTotal.id = "lblPaxTotal" + i;
            headerCellth.appendChild(lblPaxTotal);
            rowth.appendChild(headerCellth);
            headerCellth = document.createElement("TD");
            headerCellth.style.border = "1px solid rgb(255, 255, 255)!important";
            headerCellth.style.width = "30%";
            var lblPaxTotal = document.createElement("label");
            lblPaxTotal.id = "lblGmTotal" + i;
            headerCellth.appendChild(lblPaxTotal);
            rowth.appendChild(headerCellth);
            headerCellth = document.createElement("TD");
            headerCellth.style.textAlign = "center";
            headerCellth.style.border = "1px solid rgb(255, 255, 255)";
            var lblPaxTotal = document.createElement("label");
            lblPaxTotal.id = "lblCostTotal" + i;
            headerCellth.appendChild(lblPaxTotal);
            rowth.appendChild(headerCellth);

            headerCell.appendChild(tableHead);
            //rowAddDc.appendChild(headerCell);
            rowDwt.appendChild(headerCell);
        }
        /*rowDwt.appendChild(headerCell);*/
    }
    //Items Average Cost

    for (var i = 0; i < selecteditem.length; i++) {
        var rowIac = table.insertRow(-1);
        rowIac.className = "row-striped";
        var itdata = selecteditem[i];
        var itemidvalue = selecteditem[i].value.split('_')[0];
        //console.log( selecteditem[i].text)
        var text = itdata.text;
        var headerCell = document.createElement("td");
        headerCell.className = "mg_bottom_iac";
        //if (i == 0)
        //headerCell.innerHTML = text.split('-')[0]+"Wise Total";
        headerCell.innerHTML = text + " Wise Total";
        //else
        //    headerCell.innerHTML = "Day " + i;
        rowIac.appendChild(headerCell);
        var headerCell = document.createElement("td");
        headerCell.className = "mg_bottom_iac_total";
        var lblavgItemTotal = document.createElement("label");
        lblavgItemTotal.id = "lblavgItemTotal" + itemidvalue;
        lblavgItemTotal.className = "lblavgItemTotal";
        lblavgItemTotal.classList.add("lblavgItemTotal_" + itdata.dpcode + "_" + itemidvalue);
        lblavgItemTotal.innerHTML = "";
        headerCell.appendChild(lblavgItemTotal);
        rowIac.appendChild(headerCell);
        for (var j = 1; j < noOfDays; j++) {
            var headerCell = document.createElement("TD");

            var tableHead = document.createElement("TABLE");
            tableHead.style.width = "150px";
            var rowth = tableHead.insertRow(-1);
            var headerCellth = document.createElement("TD");
            headerCellth.style.width = "52px";
            headerCellth.innerHTML = "";
            rowth.appendChild(headerCellth);
            headerCellth = document.createElement("TD");
            headerCellth.innerHTML = "";
            rowth.appendChild(headerCellth);
            headerCellth = document.createElement("TD");
            headerCellth.innerHTML = "";
            rowth.appendChild(headerCellth);

            headerCell.appendChild(tableHead);
            rowIac.appendChild(headerCell);
        }
    }
    //Total Average Cost
    var rowTotal = table.insertRow(-1);
    rowTotal.className = "row-striped";
    //for (var i = 0; i <= noOfDays; i++) {
    var headerCell = document.createElement("td");
    headerCell.className = "mg_bottom_tac";
    //if (i == 0)
    headerCell.innerHTML = "Total Average Cost";
    rowTotal.appendChild(headerCell);
    headerCell = document.createElement("td");
    //else
    var lblavgTotal = document.createElement("label");
    lblavgTotal.id = "lblavgTotal";
    lblavgTotal.className = "lblavgTotal";

    lblavgTotal.innerHTML = "";
    headerCell.appendChild(lblavgTotal);
    //headerCell.innerHTML = "Total";
    rowTotal.appendChild(headerCell);
    for (var j = 1; j < noOfDays; j++) {
        var headerCell = document.createElement("TD");

        var tableHead = document.createElement("TABLE");
        tableHead.style.width = "150px";
        var rowth = tableHead.insertRow(-1);
        var headerCellth = document.createElement("TD");
        headerCellth.style.width = "52px";
        headerCellth.innerHTML = "";
        rowth.appendChild(headerCellth);
        headerCellth = document.createElement("TD");
        headerCellth.innerHTML = "";
        rowth.appendChild(headerCellth);
        headerCellth = document.createElement("TD");
        headerCellth.innerHTML = "";
        rowth.appendChild(headerCellth);

        headerCell.appendChild(tableHead);
        rowTotal.appendChild(headerCell);
    }
    //}
    ////Add the data rows.
    //for (var i = 1; i < customers.length; i++) {
    //    row = table.insertRow(-1);
    //    for (var j = 0; j < columnCount; j++) {
    //        var cell = row.insertCell(-1);
    //        cell.innerHTML = customers[i][j];
    //    }
    //}

    ////Add the data rows.
    //for (var i = 1; i < customers.length; i++) {
    //    row = table.insertRow(-1);
    //    for (var j = 0; j < columnCount; j++) {
    //        var cell = row.insertCell(-1);
    //        cell.innerHTML = customers[i][j];
    //    }
    //}

    var dvTable = document.getElementById("gridSimulationBoard");
    dvTable.innerHTML = "";
    //dvTable.appendChild(hdrtable);
    dvTable.appendChild(table);
    //dvTable.appendChild(hdrtable);
    //dvTable.appendChild(tableBody);

    //For fixed header
    //var tableOffset = $("#table-1").offset().top;
    //var $header = $("#table-1 > thead").clone();
    //var $fixedHeader = $("#header-fixed").append($header);

    //$(window).bind("scroll", function () {
    //    var offset = $(this).scrollTop();

    //    if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
    //        $fixedHeader.show();
    //    }
    //    else if (offset < tableOffset) {
    //        $fixedHeader.hide();
    //    }
    //});

    if (dataVal.length > 0)
        calculateTotals();

    if (viewOnly == 1) {
        $("#divBoard input").attr("disabled", "disabled");
        //$("#divBoard input").attr("style", "background-color: -internal-light-dark(rgba(239, 239, 239, 0.3), rgba(59, 59, 59, 0.3)); color: -internal-light-dark(rgb(84, 84, 84), rgb(170, 170, 170));");
        $(".fa-plus").hide();
        $(".fa-times").hide();
        //$("#divSimConfigMain").hide();
    }
    else {
        //$("#divBoard input").attr("style", "background-color: rgb(255, 255, 102)!important;");
        $("#divBoard input").attr("style", "background-color: lightgoldenrodyellow!important;");
        $("#divSimConfigMain").show();
    }
    Utility.UnLoading();
    $("#topHeading").text("Food Cost Simulator");
    //Dish Category Data with site items
    console.log("newsitedishcats");
    console.log(newsitedishcats);

    if ($("#SimulationID").val().length > 0) {
        if ($("#btnExpandConfig").text() == "Collapse")
            expandConfig($("#btnExpandConfig"));
    }

    var getWidth = ($("#card-header-forcalling").width() - 250);
    //alert(getWidth);
    //getWidth = getWidth < 0 ? getWidth + (getWidth * 2) : getWidth;
    //alert(getWidth);
    $(".cardHeaderInheritance").attr("style", "width:" + getWidth + "px !important")
    //for (var i = 0; i < dishcatwithdishData.length; i++) {
    //    dishcatwithdishData[i].Dishes=
    //} 
    //HttpClient.MakeSyncRequest(CookBookMasters.GetDishList, function (data) {
    //    var dataSource = data;
    //    dishdata = [];        

    //    dishdata.push({ "value": 0, "text": "Select", "price": 0 });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        dishdata.push({
    //            "DishID": dataSource[i].ID, "DishName": dataSource[i].Name, "DishCode": dataSource[i].DishCode,
    //            "DishCategoryID": dataSource[i].DishCategory_ID, "DishCategoryName": dataSource[i].DishCategoryName,
    //            "DishCategoryCode": dataSource[i].DishCategoryCode,
    //            "CostPerKG": dataSource[i].CostPerKG, "tooltip": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
    //            "DietCategoryCode": dataSource[i].DietCategoryCode,
    //            "CuisineName": dataSource[i].CuisineName, "RecipeMapStatus": dataSource[i].RecipeMapStatus
    //        });
    //    }        
    //    dishcatwithdishData = [];
    //    for (var i = 0; i < sitedishcatlist.length; i++) {
    //        var filteredDishes = dishdata.filter(ds => ds.DishCategoryID == sitedishcatlist[i].ID);
    //        dishcatwithdishData.push({
    //            "DishCategoryName": sitedishcatlist[i].Name,
    //            "DishCategoryID": sitedishcatlist[i].ID,
    //            "Dishes": filteredDishes,
    //            "DishCategoryCode": sitedishcatlist[i].DishCategoryCode
    //        });
    //        //dcsData.push({ "DishCategoryName": filteredDc[j].DishCategoryName, "DishCategoryID": filteredDc[j].DishCategoryID, "Dishes": filteredDc[j].Dishes, "DishCategoryCode": filteredDc[j].DishCategoryCode })
    //    }      

    //}, { siteCode: SiteCode,itemCode:itemc }, true);
}
function calculateTotalAvg() {
    var sum = 0;
    $(".lblavgItemTotal").each(function () {
        if (this.innerHTML.length > 0)
            sum = sum + parseFloat(this.innerHTML);
    });
    //sum = parseFloat(sum / noOfDaysData).toFixedNoRounding(2);
    $("#lblavgTotal").text(Utility.MathRound(sum, 2));
    $("#lblavgTotal2").text(Utility.Currency + Utility.MathRound(sum, 2));
}
function calculateItemAvg() {
    var selectedItem = JSON.parse(localStorage.getItem("contextitem"));
    setTimeout(function () {
        for (var i = 0; i < selectedItem.length; i++) {
            var itemidvalue = selectedItem[i].value.split('_')[0];
            var itemcostclass = ".lblCostForItem_" + itemidvalue + "_" + selectedItem[i].dpcode;
            var sum = 0;
            console.log(itemcostclass);
            $(itemcostclass).each(function () {
                console.log("lblCostForItem_" + this.innerHTML)
                //console.log("txtTotalPax" + this.value);
                if (this.innerHTML != "-" && this.innerHTML.length > 0)
                    sum = sum + parseFloat(this.innerHTML);
            });
            var avgItemId = ".lblavgItemTotal_" + selectedItem[i].dpcode + "_" + itemidvalue;
            sum = parseFloat(sum / noOfDaysData).toFixedNoRounding(2);
            $(avgItemId).text(sum);
        }

        calculateTotalAvg();
    }, 0);

}

function calculateTotals() {
    //console.log("paxTotal"+paxTotal())
    var avgitem = 0;
    for (var i = 1; i <= noOfDaysData; i++) {
        var lblPaxTotal = "#lblPaxTotal" + i;
        $(lblPaxTotal).text(paxTotal(".txtTotalPax" + i));
        var lblGmTotal = "#lblGmTotal" + i;
        $(lblGmTotal).text(grmTotal(".txtTotalGrm" + i));
        var lblCostTotal = "#lblCostTotal" + i;
        var ct = costTotal(".lblTotalCost" + i);
        $(lblCostTotal).text(ct);
        if (ct.length > 0)
            avgitem = avgitem + parseFloat(ct);
    }
    console.log("avgitem")
    console.log(avgitem)
    calculateItemAvg();
}

//function calculateAvgItemTotal() {
//    for (var i = 0; i < daypartData; i++) {

//    }
//}


function getDishCategoryByItemID(itemId, itemName) {
    console.log(itemId)
    console.log(itemName)
    var dialog = $("#divDishCategory").data("kendoWindow");
    $(winId).prev().find(".k-window-title").text(' Dish Categories for' + itemName);
    /*dialog.data("kendoWindow").title('Editing: Dish Categories for' + itemName)*/
    dialog.open();
    dialog.center();
}

function onInputPax(txtEle, itid, dcode, colindex) {
    var val = $(txtEle).val();
    var classes = ".txtPax_" + itid + "_" + dcode + "_" + colindex;
    $(classes).val(val);

    var nod = $("#NewSimNoOfDays").val();
    for (var i = 1; i <= nod; i++) {
        var classesMain = ".txtPxroot_" + itid + "_" + i + "_" + dcode;
        $(classesMain).val(val);

        var classes = ".txtPax_" + itid + "_" + dcode + "_" + i;
        //var classes = ".txtPax_" + itid + "_" + dcode ;
        $(classes).val(val);
        $(classes).each(function () {
            var inpGrm = $(this).closest('td').next('td').find('input');
            var lblCost = $(this).closest('td').next('td').next('td').find('label');

            var cost = this.getAttribute("data-cost");

            if (inpGrm.val().length > 0) {
                var costGrm = (parseFloat(inpGrm.val()) * parseFloat(cost));
                if (val.length > 0)
                    lblCost.text((parseInt(val) * parseFloat(costGrm)).toFixedNoRounding(2));
                else
                    lblCost.text("0");
            }
            else
                lblCost.text("0");
        });
        //calculateTotals();
    }
    //txtPax_79_DPC-00001_1 txtTotalPax1
    //txtPax_79_DPC-00001_1 txtTotalPax1
    //txtPax_79_DPC-00001_2 txtTotalPax2
    //txtPax_79_DPC-00001_2 txtTotalPax2
    //id=txtPax_1_79_DPC-00001_119
    //id=txtPax_1_79_DPC-00001_169    
    //class=txtTotalGrm1 txtGrmroot_1_79_DPC-00001_119 txtGrmroot_1_79_DPC-00001 txtGrmroots
    //class=txtTotalGrm1 txtGrmroot_1_79_DPC-00001_169 txtGrmroot_1_79_DPC-00001 txtGrmroots
    console.log("hits calculateTotals inp pax");
    calculateTotals();
}

function onInputGrm(eleId, itemid, dpcode, cost, colIndex, dishid) {
    //var val = $(eleId).val(); //0.1
    var val = document.getElementById(eleId).value;
    console.log("colIndex" + colIndex);
    console.log(val)

    var pax = "#txtPax_" + colIndex + "_" + itemid + "_" + dpcode + "_" + dishid;
    var person = $(pax).val(); //10
    var costCal = parseFloat(cost) * parseFloat(val); //cost is 500 per kg
    var total = costCal * parseInt(person);
    if (isNaN(total))
        total = 0;
    var totallbl = "#lblCost_" + colIndex + "_" + itemid + "_" + dpcode + "_" + dishid;
    $(totallbl).text((total).toFixedNoRounding(2));

    console.log("hits calculateTotals inp grm");
    calculateTotals();
}

function paxTotal(className) {
    var sum = 0;
    $(className).each(function () {
        //console.log("txtTotalPax" + this.value);
        if (this.value.length > 0)
            sum += parseInt(this.value);
    });
    return sum;
}
function grmTotal(className) {
    var sum = 0;
    $(className).each(function () {
        if (this.value.length > 0)
            sum += parseFloat(this.value);
    });
    return sum.toFixedNoRounding(2);
}
function costTotal(className) {
    var sum = 0;
    $(className).each(function () {
        //console.log()
        if (this.innerHTML != "-" && this.innerHTML.length > 0)
            sum += parseFloat(this.innerHTML);
    });
    return sum.toFixedNoRounding(2);
}

function onClickExpandDishes(divEle, dpcode, dccode) {
    var childClass = ".expand_dc_" + dpcode + "_" + dccode;
    //alert($(spanEle).hasClass("k-i-arrow-60-up"));
    $(childClass).slideToggle(100, function () {
        //execute this after slideToggle is done
        //change text of header based on visibility of content div        
    });
    var spanEleUp = $(divEle).children("span").eq(0);
    var spanEleDown = $(divEle).children("span").eq(1);
    if ($(spanEleUp).hasClass("ac")) {
        $(spanEleUp).removeClass("ac");
        $(spanEleDown).addClass("ac");
        $(spanEleUp).hide();
        $(spanEleDown).show();
    }
    else if ($(spanEleDown).hasClass("ac")) {
        $(spanEleDown).removeClass("ac");
        $(spanEleUp).addClass("ac");
        $(spanEleDown).hide();
        $(spanEleUp).show();
    }
}
function onClickExpand(divEle, dpcode, itemid) {
    //var childClass = ".expand_dc_" + itemid + "_" + dpcode;
    var childClass = "#tableForST_" + dpcode + "_" + itemid;

    console.log(childClass)
    $(childClass).slideToggle(0, function () {

    });
    var spanEleUp = $(divEle).children("i").eq(0);
    var spanEleDown = $(divEle).children("i").eq(1);
    if ($(spanEleUp).hasClass("ac")) {
        $(spanEleUp).removeClass("ac");
        $(spanEleDown).addClass("ac");
        $(spanEleUp).hide();
        $(spanEleDown).show();
    }
    else if ($(spanEleDown).hasClass("ac")) {
        $(spanEleDown).removeClass("ac");
        $(spanEleUp).addClass("ac");
        $(spanEleDown).hide();
        $(spanEleUp).show();
    }


}
var removedDishCats = [];
var removedData = [];
var removedDishContext = [];
//var removedDishCategoryContext = [];
function deleteDishCategory(tableEle, dpcode, dccode, itid) {
    /*var refreshItemData = selecteditemData;*/
    var contextData = JSON.parse(localStorage.getItem("contextitem"));
    var refreshItemData = contextData;
    Utility.Loading();
    setTimeout(function () {
        for (var i = 0; i < refreshItemData.length; i++) {
            if (refreshItemData[i].dpcode == dpcode) {
                for (var j = 0; j < refreshItemData[i].DishCategories.length; j++) {
                    if (refreshItemData[i].DishCategories[j].DishCategoryCode == dccode) {

                        removedData.push(refreshItemData[i].DishCategories[j]);

                        //if (removedDishCats.length == 0) {
                        //    removedDishCats.push({ "itemid": itid, "dpcode": dpcode, "dishcategories": refreshItemData[i].DishCategories[j] });
                        //}
                        //else {
                        //    removedDishCats.filter(ds => ds.itemid == itid && ds.dpcode == dpcode).push(refreshItemData[i].DishCategories[j]);
                        //}
                        contextData[i].DishCategories[j].Dishes.forEach(function (item, index) {
                            removedDishContext.push(item)
                        });
                        contextData[i].DishCategories.splice(j, 1);

                    }
                }
            }
        }

        var dcrows = ".expand_dc_" + dpcode + "_" + dccode;
        var maindcrow = ".row_dc_" + dpcode + "_" + dccode + "_" + itid;
        $(dcrows).each(function () {
            $(this).remove();
        });
        $(maindcrow).remove();

        if (removedDishCats.length == 0)
            removedDishCats.push({
                "itemid": itid, "dpcode": dpcode, "dishcategories": []
            });
        var chkRem = removedDishCats.filter(ds => ds.itemid == itid && ds.dpcode == dpcode);
        if (chkRem.length > 0) {
            //for (var i = 0; i < chkRem[0].dishcategories.length; i++) {
            //    var chkds = removedData.filter(ds => ds.DishCategoryCode == chkRem[0].dishcategories[i].DishCategoryCode);
            //    if (chkds.length == 0)
            //        chkRem[0].dishcategories.push(chkds[0]);
            //}
            chkRem[0].dishcategories = removedData;
        }
        else
            removedDishCats.push({
                "itemid": itid, "dpcode": dpcode, "dishcategories": removedData
            });


        console.log("removedDishContext");
        console.log(removedDishContext);
        //Rebind dish cat multiselect
        //var contextitem = [];
        //contextitem = selecteditemData;
        //var filtered = contextData.filter(item => item.value == itid + "_" + dpcode && item.dpcode == dpcode)[0];
        //var filteredDc = filtered.DishCategories;
        //var newdishcatwithdishData = [];
        //for (var i = 0; i < dishcatwithdishData.length; i++) {
        //    var chkFd = filteredDc.filter(a => a.DishCategoryCode == dishcatwithdishData[i].DishCategoryCode)[0];

        //    if (chkFd == undefined)
        //        newdishcatwithdishData.push(dishcatwithdishData[i]);
        //    //if (chkFd != undefined && chkFd.DishCategoryCode == dccode)
        //    //    newdishcatwithdishData.push(dishcatwithdishData[i]);
        //}

        //var dishctMultiId = "#inputDishCTxt_" + itid + "_" + dpcode;
        //var multiselect = $(dishctMultiId).data("kendoMultiSelect");
        //var ds = new kendo.data.DataSource({ data: newdishcatwithdishData });
        //multiselect.setDataSource(ds);   

        //console.log(contextitem);

        localStorage.setItem("contextitem", JSON.stringify(contextData));
        //7th April
        calculateTotals();
        Utility.UnLoading();
    }, 50);
}

function deleteDish(tableEle, dpcode, dccode, did, itemid) {
    //var refreshItemData = selecteditemData;
    var contextData = JSON.parse(localStorage.getItem("contextitem"));
    var refreshItemData = contextData;
    console.log("contextData")
    console.log(contextData)
    //var getdc = refreshItemData.filter(rd => rd.dpcode == dpcode)[0].DishCategories.filter(rd => rd.DishCategoryCode==dccode)[0].Dishes;
    for (var i = 0; i < refreshItemData.length; i++) {

        if (refreshItemData[i].dpcode == dpcode && refreshItemData[i].value.split('_')[0] == itemid) {

            for (var j = 0; j < refreshItemData[i].DishCategories.length; j++) {

                for (var k = 0; k < refreshItemData[i].DishCategories[j].Dishes.length; k++) {

                    if (refreshItemData[i].DishCategories[j].Dishes[k].DishID == did) {

                        removedDishContext.push(refreshItemData[i].DishCategories[j].Dishes[k])

                        contextData[i].DishCategories[j].Dishes.splice(k, 1);
                    }
                }
            }
        }
    }
    //for (var i = 0; i < getdc.length; i++) {
    //    if (getdc[i].DishID == did)

    //}
    var dcrows = ".row_ds_" + dpcode + "_" + dccode + "_" + did + "_" + itemid;

    $(dcrows).remove();

    console.log("deleted dish")
    console.log(removedDishContext)

    localStorage.setItem("contextitem", JSON.stringify(contextData));

    calculateTotals();
}
function addDish(dpcode, itemid, dccode) {
    var row = ".row_dc_" + dpcode + "_" + dccode + "_" + itemid;
    //var row = "#tableAddDS_" + dpcode + "_" + itemid + "_" + dccode  ;

    var spanAddDc = "#spanAddDc_" + dpcode + "_" + itemid + "_" + dccode;
    var contextitem = [];
    //contextitem =JSON.parse(localStorage.getItem("contextitem"));
    //console.log(contextitem)
    contextitem = selecteditemData;
    if ($(spanAddDc).hasClass("fa-plus")) {

        Utility.Loading();
        HttpClient.MakeSyncRequest(CookBookMasters.GetDishList, function (data) {

            var dataSource = data;
            dishdata = [];
            //for (var i = 0; i < dataSource.length; i++) {
            //    sitedishdata.forEach(function (yi) {
            //        if (yi.DishCode == dataSource[i].DishCode) {
            //            //dataSource[i].CostPerKG = yi.CostPerKG;
            //        }
            //    });
            //}
            //console.log(SiteCode)
            //console.log("chkdishcateg")
            //console.log(sitedishdata);

            dishdata.push({ "value": 0, "text": "Select", "price": 0 });
            for (var i = 0; i < dataSource.length; i++) {
                dishdata.push({
                    "DishID": dataSource[i].ID, "DishName": dataSource[i].Name, "DishCode": dataSource[i].DishCode,
                    "DishCategoryID": dataSource[i].DishCategory_ID, "DishCategoryName": dataSource[i].DishCategoryName,
                    "DishCategoryCode": dataSource[i].DishCategoryCode,
                    "CostPerKG": dataSource[i].CostPerKG, "tooltip": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
                    "DietCategoryCode": dataSource[i].DietCategoryCode,
                    "CuisineName": dataSource[i].CuisineName, "RecipeMapStatus": dataSource[i].RecipeMapStatus, "ServedPortion": dataSource[i].ServedPortion
                });
            }
            console.log("chkdishcateg")
            console.log(dishdata);
            console.log(data);

            dishcatwithdishData = [];
            for (var i = 0; i < sitedishcatlist.length; i++) {
                var filteredDishes = dishdata.filter(ds => ds.DishCategoryID == sitedishcatlist[i].ID);
                dishcatwithdishData.push({
                    "DishCategoryName": sitedishcatlist[i].Name,
                    "DishCategoryID": sitedishcatlist[i].ID,
                    "Dishes": filteredDishes,
                    "DishCategoryCode": sitedishcatlist[i].DishCategoryCode
                });
                //dcsData.push({ "DishCategoryName": filteredDc[j].DishCategoryName, "DishCategoryID": filteredDc[j].DishCategoryID, "Dishes": filteredDc[j].Dishes, "DishCategoryCode": filteredDc[j].DishCategoryCode })
            }
            console.log("dishcatwithdishData");
            console.log(dishcatwithdishData)

            //Old Code copied here from outside server call
            //var filtered = contextitem.filter(item => item.value == itemid+"_"+dpcode && item.dpcode == dpcode)[0];
            //var filteredDs = filtered.DishCategories.filter(dc => dc.DishCategoryCode == dccode)[0];
            var filteredDs = dishcatwithdishData.filter(dc => dc.DishCategoryCode == dccode);
            console.log("filteredDs")
            console.log(filteredDs)
            var newRow = document.createElement("tr");
            newRow.id = "newRow_" + dpcode + "_" + itemid + "_" + dccode;
            //newRow.className = "expand_dc_" + dpcode;
            newRow.className = "expand_dc_" + itemid + "_" + dpcode;
            newRow.classList.add("row_dc_" + dpcode + "_" + dccode + "_" + itemid);
            var dcsData = [];
            var newdishData = [];
            for (var i = 0; i < filteredDs[0].Dishes.length; i++) {
                newdishData.push(filteredDs[0].Dishes[i]);
            }
            //console.log("contextitem")
            //console.log(contextitem)
            var filcon = contextitem.filter(a => a.value == itemid + "_" + dpcode && a.dpcode == dpcode)[0].DishCategories.filter(a => a.DishCategoryCode == dccode);
            if (filcon.length > 0) {
                for (var i = 0; i < filcon[0].Dishes.length; i++) {
                    for (var j = 0; j < newdishData.length; j++) {
                        if (newdishData[j].DishCode == filcon[0].Dishes[i].DishCode) {
                            newdishData.splice(j, 1);
                        }
                    }
                    //newdishData = newdishData.filter(a => a.Dishcode != filcon[0].Dishes[i].DishCode);
                    //console.log(filcon[0].Dishes[i].DishCode)            
                }
            }
            //console.log("newdishData2")
            //console.log(newdishData)

            for (var i = 0; i <= noOfDaysData; i++) {
                if (i == 0) {
                    //text
                    var headerCell = document.createElement("TD");
                    var inputDishCTxt = document.createElement("div");
                    //inputDishCTxt.type = "text";
                    inputDishCTxt.className = "form-control";
                    inputDishCTxt.id = "inputDishCTxt_" + itemid + "_" + dpcode + "_" + dccode;
                    inputDishCTxt.style.width = "100%!important";
                    inputDishCTxt.classList.add("multitSelectdishcatdish");
                    headerCell.appendChild(inputDishCTxt);
                    newRow.appendChild(headerCell);

                    var filteredDc = filteredDs[0].Dishes;

                    for (var j = 0; j < filteredDc.length; j++) {
                        //var dataO={ "index": j+1, "paxTxt": "", "grmTxt": "", "costTxt": "" };
                        dcsData.push({ "DishID": filteredDc[j].DishID, "DishName": filteredDc[j].DishName, "DishCode": filteredDc[j].DishCode, "CostPerKG": filteredDc[j].CostPerKG, "ServedPortion": filteredDc[j].ServedPortion });
                        //newdishData = newdishData.filter(ds => ds.DishCode != filteredDc[j].DishCode);
                    }

                }
                else {
                    //var headerCell = document.createElement("TD");
                    //var tableHead = document.createElement("TABLE");
                    //tableHead.style.width = "240px";
                    //var rowth = tableHead.insertRow(-1);
                    //var headerCellth = document.createElement("TD");
                    //headerCellth.style.width = "52px";
                    //headerCellth.innerHTML = "";
                    //rowth.appendChild(headerCellth);
                    //headerCellth = document.createElement("TD");
                    //headerCellth.innerHTML = "";
                    //rowth.appendChild(headerCellth);
                    //headerCellth = document.createElement("TD");
                    //headerCellth.innerHTML = "";
                    //rowth.appendChild(headerCellth);
                    //headerCell.appendChild(tableHead);
                    //newRow.appendChild(headerCell);

                }
            }

            //console.log(newRow);
            //console.log(row);
            //$(newRow).insertAfter($(row));
            var getTable = document.getElementById("tableAddDS_" + dpcode + "_" + itemid + "_" + dccode);
            getTable.appendChild(newRow);

            $(spanAddDc).removeClass("fa-plus");
            $(spanAddDc).addClass("fa-times");
            for (var i = 0; i < noOfDaysData; i++) {
                if (i == 0) {
                    console.log("newdishData")
                    console.log(newdishData)
                    var dishctMultiId = "#inputDishCTxt_" + itemid + "_" + dpcode + "_" + dccode;
                    var dcsMulti = $(dishctMultiId).kendoMultiSelect({
                        filter: "contains",
                        dataTextField: "DishName",
                        dataValueField: "DishID",
                        dataSource: newdishData,
                        placeholder: " ",
                        index: 0,
                        autoBind: true,
                        autoClose: false,
                        open: function (e) { onclickDs(itemid, dpcode, dccode, newdishData); },
                        change: function (e) { onDishSelected(e, dpcode, itemid, dccode); }
                    }).data("kendoMultiSelect");
                }
            }

            Utility.UnLoading();
        }, { siteCode: $("#hdnSiteCode").val(), itemId: itemid }, true);

    }
    else if ($(spanAddDc).hasClass("fa-times")) {
        var rowId = "#newRow_" + dpcode + "_" + itemid + "_" + dccode;
        //var rowId = "#tableAddDS_" + dpcode + "_" + itemid + "_" + dccode;
        $(rowId).remove();
        //$(rowId).html("");
        $(spanAddDc).removeClass("fa-times");
        $(spanAddDc).addClass("fa-plus");
    }
}
function onclickDs(itid, dpcode, dccode) {
    //removedDishCats
    var contextData = JSON.parse(localStorage.getItem("contextitem"));
    if (contextData != undefined && contextData.length > 0) {
        var newdishData = [];
        var excludeDishes = [];
        //console.log("contextitem")
        //console.log(contextitem)
        var filcon = contextData.filter(a => a.value == itid + "_" + dpcode && a.dpcode == dpcode)[0].DishCategories.filter(a => a.DishCategoryCode == dccode);
        for (var i = 0; i < filcon[0].Dishes.length; i++) {
            console.log(filcon[0].Dishes[i].DishCode)
            excludeDishes.push(filcon[0].Dishes[i].DishCode)
            //for (var j = 0; j < dishcatwithdishData.length; j++) {
            //    if (dishcatwithdishData[j].DishCode != filcon[0].Dishes[i].DishCode) {
            //        var chk = newdishData.filter(a => a.DishCode == dishcatwithdishData[j].DishCode)
            //        if(chk.length==0)
            //        newdishData.push(filcon[0].Dishes[i]);
            //    }
            //}
            //newdishData = newdishData.filter(a => a.Dishcode != filcon[0].Dishes[i].DishCode);
            //console.log(filcon[0].Dishes[i].DishCode)            
        }
        var filteredDs = dishcatwithdishData.filter(dc => dc.DishCategoryCode == dccode);
        for (var i = 0; i < filteredDs[0].Dishes.length; i++) {
            var chkExck = excludeDishes.filter(ds => ds == filteredDs[0].Dishes[i].DishCode);
            if (chkExck.length == 0)
                newdishData.push(filteredDs[0].Dishes[i]);
        }


        var dishctMultiId = "#inputDishCTxt_" + itid + "_" + dpcode + "_" + dccode;
        var multiselect = $(dishctMultiId).data("kendoMultiSelect");
        var ds = new kendo.data.DataSource({ data: newdishData });
        multiselect.setDataSource(ds);
    }
}
function onDishSelected(e, dpcode, itemid, dccode) {
    //var dcsMulti ="#inputDishCTxt_"
    var dishctMultiId = "#inputDishCTxt_" + itemid + "_" + dpcode + "_" + dccode;
    var selecteditems = $(dishctMultiId).data("kendoMultiSelect").dataItems();
    //selecteditems = selecteditems.filter(a => a.value == itemid + "_" + dpcode);
    console.log("selecteditems filtered");
    console.log(selecteditems);

    var contextitem = [];
    contextitem = JSON.parse(localStorage.getItem("contextitem"));
    //contextitem = selecteditemData;
    for (var si = 0; si < selecteditems.length; si++) {
        //var currentdcRowId = ".row_dc_" + dpcode + "_" + selecteditems[si].DishCategoryCode;
        var filtered = contextitem.filter(item => item.value == itemid + "_" + dpcode && item.dpcode == dpcode)[0].DishCategories;
        var dcsFiltered = filtered.filter(dc => dc.DishCategoryCode == dccode)[0].Dishes;
        var dcFiltered = dcsFiltered.filter(ds => ds.DishID == selecteditems[si].DishID);

        console.log("contextitem")
        console.log(contextitem)
        console.log("selecteditemData")
        console.log(selecteditemData)
        if (dcFiltered.length == 0) {
            //var contextDc = selecteditemData.filter(item => item.value == itemid+"_"+dpcode && item.dpcode == dpcode)[0].DishCategories;            
            //var getdishes = contextDc.filter(ds => ds.DishCategoryCode == dccode)[0].Dishes;
            var getdishes = dishcatwithdishData.filter(ds => ds.DishCategoryCode == dccode)[0].Dishes;
            var dcContext = getdishes.filter(dc => dc.DishID == selecteditems[si].DishID);

            var dataO = [];
            //contextitem.filter(item => item.value == itemid + "_" + dpcode && item.dpcode == dpcode)[0].DishCategories.filter(ds => ds.DishCategoryCode == dccode)[0].Dishes.push(dcContext[0]);
            //console.log("sid")
            //console.log(contextitem);
            //localStorage.setItem("contextitem", JSON.stringify(contextitem));

            //Loop throgh dishes and add all over
            //var cRowId = "#rowAddDc_" + dpcode + "_" + itemid;
            var dishid = selecteditems[si].DishID;
            var dishname = selecteditems[si].DishName;
            var row3 = document.createElement("tr");
            //var row3 = divdishCategoryMain.insertRow(-1);
            //row3.className = "row-striped";
            //row3.classList.add("expand_dc_" + dpcode);
            //row3.classList.add("row_dc_" + dpcode + "_" + dccode+"_"+dishid);
            row3.className = "row-striped";
            row3.classList.add("expand_dc_" + itemid + "_" + dpcode);
            row3.classList.add("expand_dc_" + dpcode + "_" + dccode);
            row3.classList.add("row_ds_" + dpcode + "_" + dccode + "_" + selecteditems[si].DishID + "_" + itemid);
            row3.classList.add("dishes_forslidingpurpose");
            //itemdata.push({ "value": dataSource[i].ItemID, "text": dataSource[i].ItemName, "code": dataSource[i].ItemCode, "dpcode": dataSource[i].DayPartCode });
            for (var i = 0; i <= noOfDaysData; i++) {
                if (i == 0) {

                    var headerCell = document.createElement("td");
                    headerCell.className = "mg_top_header_ds_body";
                    //headerCell.innerHTML = itemDATA.DishCategories[dsc].Dishes[dis].DishName;
                    //headerCell.style.paddingLeft = "50px";
                    headerCell.style.width = "240px";

                    var divCb = document.createElement("div");
                    //divCb.innerHTML = itemDATA.DishCategories[dsc].Dishes[dis].DishName;
                    var spanText = document.createElement("span");
                    //spanText.style.width = "72px";
                    //spanText.style.width = "121px";
                    //spanText.style.width = "163px";
                    spanText.style.width = "160px";

                    spanText.style.display = "inline-block";
                    spanText.innerHTML = selecteditems[si].DishName;

                    //Recipe button
                    var spanRecipeBtn = document.createElement("span");
                    //spanRecipeBtn.className = "fa fa-solid fa-link";
                    spanRecipeBtn.className = "fas fa-utensils";
                    spanRecipeBtn.style.color = "deepskyblue";
                    spanRecipeBtn.id = "span_recipeDC_" + dpcode + "_" + dccode + "_" + selecteditems[si].DishID + "_" + itemid + "_" + selecteditems[si].DishCode;
                    //spanCloseBtn.style.marginLeft = "68px";
                    spanRecipeBtn.addEventListener('click', function () {

                        var dccode = this.id.split('_')[3];
                        var dpcode = this.id.split('_')[2];
                        var did = this.id.split('_')[4];
                        var itemid1 = this.id.split('_')[5];
                        var dishcode = this.id.split('_')[6];
                        openRecipe(itemid1, did, dccode, dishcode, dpcode);
                        //openRecipe(itemid1, did, dccode);
                    });
                    divCb.style.paddingLeft = "42px";
                    divCb.appendChild(spanRecipeBtn);

                    divCb.appendChild(spanText);

                    var spanCloseBtn = document.createElement("span");
                    spanCloseBtn.className = "fas fa-times";
                    spanCloseBtn.id = "span_delDC_" + dpcode + "_" + dccode + "_" + selecteditems[si].DishID + "_" + itemid;
                    //spanCloseBtn.style.marginLeft = "68px";
                    spanCloseBtn.addEventListener('click', function () {

                        var dccode = this.id.split('_')[3];
                        var dpcode = this.id.split('_')[2];
                        var did = this.id.split('_')[4];
                        var itid = this.id.split('_')[5];
                        deleteDish(row3, dpcode, dccode, did, itid);
                    });
                    divCb.appendChild(spanCloseBtn);
                    headerCell.appendChild(divCb);
                    row3.appendChild(headerCell);

                }
                else {
                    dataO.push({ "index": i, "paxTxt": "", "grmTxt": "", "costTxt": "" });;

                    var headerCell = document.createElement("TD");
                    headerCell.style.paddingRight = "0";
                    headerCell.style.paddingLeft = "3px";
                    var inputTxt = document.createElement("input");
                    inputTxt.className = "txtPax_" + itemid + "_" + dpcode + "_" + i;
                    inputTxt.classList.add("txtTotalPax" + i);
                    inputTxt.classList.add("addedDishes");
                    inputTxt.type = "text";
                    //inputTxt.id = "txtDsxroot_" + dis + "_" +  itemDATA.DishCategories[dsc].Dishes[dis].DishID;
                    inputTxt.id = "txtPax_" + i + "_" + itemid + "_" + dpcode + "_" + selecteditems[si].DishID;
                    inputTxt.setAttribute("data-cost", selecteditems[si].CostPerKG);
                    inputTxt.style.width = "50px";
                    inputTxt.style.marginLeft = "2px";
                    //pre populate root pax
                    var rootPax = ".txtPxroot_" + itemid + "_" + i + "_" + dpcode;
                    inputTxt.value = $(rootPax).val();

                    //headerCellth.style.width = "60px";
                    inputTxt.addEventListener('input', function () {
                        //result.textContent = this.value;
                        var itemid = this.id.split('_')[2];
                        var dpcode = this.id.split('_')[3];
                        var cost = this.getAttribute("data-cost");
                        var colIndex = this.id.split('_')[1];
                        var dishid = this.id.split('_')[4];
                        var txtGrmInp = "txtGrmroot_" + itemid + "_" + dpcode + "_" + cost + "_" + colIndex + "_" + dishid;
                        onInputGrm(txtGrmInp, itemid, dpcode, cost, colIndex, dishid);

                        //calculateTotals();
                    });

                    //inputTxt.style.width = "50px";
                    //inputTxt.addEventListener('input', function () {
                    //    //result.textContent = this.value;
                    //    calculateTotals();
                    //});
                    var inputTxtGrm = document.createElement("input");
                    inputTxtGrm.type = "text";
                    inputTxtGrm.id = "txtGrmroot_" + itemid + "_" + dpcode + "_" + selecteditems[si].CostPerKG + "_" + i + "_" + selecteditems[si].DishID;
                    inputTxtGrm.style.width = "50px";
                    inputTxtGrm.style.marginLeft = "8px";
                    inputTxtGrm.className = "txtTotalGrm" + i;
                    inputTxtGrm.classList.add("txtGrmroot_" + i + "_" + itemid + "_" + dpcode + "_" + selecteditems[si].DishID);
                    inputTxtGrm.classList.add("txtGrmroot_" + i + "_" + itemid + "_" + dpcode);
                    inputTxtGrm.classList.add("txtGrmroots");
                    inputTxtGrm.classList.add("addedDishes");
                    //inputTxtGrm.value = selecteditems[si].ServedPortion;
                    if (selecteditems[si].ServedPortion != undefined)
                        inputTxtGrm.value = Utility.MathRound(selecteditems[si].ServedPortion, 2);
                    inputTxtGrm.addEventListener('input', function () {
                        //result.textContent = this.value;
                        validateU2D(this);
                        var itemid = this.id.split('_')[1];
                        var dpcode = this.id.split('_')[2];
                        var cost = this.id.split('_')[3];
                        var colIndex = this.id.split('_')[4];
                        var dishid = this.id.split('_')[5];
                        var txtGrmInp = "txtGrmroot_" + itemid + "_" + dpcode + "_" + cost + "_" + colIndex + "_" + dishid;
                        onInputGrm(txtGrmInp, itemid, dpcode, cost, colIndex, dishid);
                    });

                    var tableHead = document.createElement("TABLE");
                    //April 7th
                    //tableHead.style.width = "240px";
                    var rowth = tableHead.insertRow(-1);
                    var headerCellth = document.createElement("TD");
                    headerCellth.style.width = "30%";
                    headerCellth.style.textAlign = "center";
                    headerCellth.className = "mg_top_header_item_text";
                    headerCellth.appendChild(inputTxt);
                    rowth.appendChild(headerCellth);
                    headerCellth = document.createElement("TD");
                    //headerCellth.style.width = "50px";
                    headerCellth.style.width = "30%";
                    headerCellth.style.textAlign = "center";
                    headerCellth.className = "mg_top_header_item_text"
                    headerCellth.appendChild(inputTxtGrm);
                    rowth.appendChild(headerCellth);
                    headerCellth = document.createElement("TD");
                    headerCellth.style.width = "50px";
                    //7th April
                    headerCellth.style.paddingBottom = "0";
                    var lblCost = document.createElement("label");
                    lblCost.id = "lblCost_" + i + "_" + itemid + "_" + dpcode + "_" + selecteditems[si].DishID;
                    lblCost.innerHTML = "";
                    //lblCost.style.marginLeft = "17px";
                    lblCost.style.width = "50px";
                    lblCost.className = "lblTotalCost" + i;
                    lblCost.classList.add("lblCostForItem_" + itemid + "_" + dpcode);
                    headerCellth.style.textAlign = "center";
                    headerCellth.appendChild(lblCost);
                    rowth.appendChild(headerCellth);

                    headerCell.appendChild(tableHead);
                    row3.appendChild(headerCell);

                }
            }

            //moved here from top on 22nd
            dcContext[0].dataO = dataO;
            contextitem.filter(item => item.value == itemid + "_" + dpcode && item.dpcode == dpcode)[0].DishCategories.filter(ds => ds.DishCategoryCode == dccode)[0].Dishes.push(dcContext[0]);
            console.log("contextitem after dataO when add dish")
            console.log(contextitem);
            localStorage.setItem("contextitem", JSON.stringify(contextitem));

            //$(row3).insertAfter($(cRowId));
            //row_dc_DPC-00002_DCC-00003
            var lastClass = ".row_dc_" + dpcode + "_" + dccode + "_" + itemid;
            //var check = 0;
            //$(lastClass).each(function () { check++; });
            //if (check == 0)
            //    lastClass = ".row_dc_" + dpcode + "_" + dccode + "_" + itemid;
            //$(row3).insertAfter($(lastClass).last());
            lastClass = "#row_AddDc_" + dpcode + "_" + dccode + "_" + itemid;
            $(row3).insertAfter($(lastClass));
            //insertAfter(row3, $(cRowId));
            //divdishCategoryMain.appendChild(row3);

            //7th April
            for (var i = 1; i <= noOfDaysData; i++) {
                var inputTxtGrm = "txtGrmroot_" + itemid + "_" + dpcode + "_" + selecteditems[si].CostPerKG + "_" + i + "_" + selecteditems[si].DishID;
                onInputGrm(inputTxtGrm, itemid, dpcode, selecteditems[si].CostPerKG, i, selecteditems[si].DishID);
            }
        }
    }
}
function addDishCategory(dpcode, itemid) {
    var tempSelectedItemData = selecteditemData;
    var row = "#rowAddDc_" + dpcode + "_" + itemid;
    var spanAddDc = "#spanAddDc_" + dpcode + "_" + itemid;
    var contextitem = [];
    //contextitem =JSON.parse(localStorage.getItem("contextitem"));
    //console.log(contextitem)
    contextitem = selecteditemData;
    if ($(spanAddDc).hasClass("fa-plus")) {
        var filtered = contextitem.filter(item => item.value == itemid + "_" + dpcode && item.dpcode == dpcode)[0];
        console.log("filtered")
        console.log(filtered)
        var newRow = document.createElement("tr");
        newRow.id = "newRow_" + dpcode + "_" + itemid;
        newRow.className = "expand_dc_" + itemid + "_" + dpcode;
        var dcsData = [];
        console.log("dishdata")
        console.log(dishdata);

        Utility.Loading();
        //Reload dishcatwithdishData
        HttpClient.MakeSyncRequest(CookBookMasters.GetDishList, function (data) {

            var dataSource = data;
            dishdata = [];
            //for (var i = 0; i < dataSource.length; i++) {
            //    sitedishdata.forEach(function (yi) {
            //        if (yi.DishCode == dataSource[i].DishCode) {
            //            //dataSource[i].CostPerKG = yi.CostPerKG;
            //        }
            //    });
            //}
            //console.log(SiteCode)
            //console.log("chkdishcateg")
            //console.log(sitedishdata);

            dishdata.push({ "value": 0, "text": "Select", "price": 0 });
            for (var i = 0; i < dataSource.length; i++) {
                dishdata.push({
                    "DishID": dataSource[i].ID, "DishName": dataSource[i].Name, "DishCode": dataSource[i].DishCode,
                    "DishCategoryID": dataSource[i].DishCategory_ID, "DishCategoryName": dataSource[i].DishCategoryName,
                    "DishCategoryCode": dataSource[i].DishCategoryCode,
                    "CostPerKG": dataSource[i].CostPerKG, "tooltip": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
                    "DietCategoryCode": dataSource[i].DietCategoryCode,
                    "CuisineName": dataSource[i].CuisineName, "RecipeMapStatus": dataSource[i].RecipeMapStatus, "ServedPortion": dataSource[i].ServedPortion
                });
            }
            //for (var i = 0; i < sitedishdata.length; i++) {
            //    dishdata.push({
            //        "DishID": sitedishdata[i].ID, "DishName": sitedishdata[i].Name, "DishCode": sitedishdata[i].DishCode,
            //        "DishCategoryID": sitedishdata[i].DishCategory_ID, "DishCategoryName": sitedishdata[i].DishCategoryName,
            //        "DishCategoryCode": sitedishdata[i].DishCategoryCode,
            //        "CostPerKG": sitedishdata[i].CostPerKG, "tooltip": sitedishdata[i].Name, "UOMCode": sitedishdata[i].UOMCode,
            //        "DietCategoryCode": sitedishdata[i].DietCategoryCode,
            //        "CuisineName": sitedishdata[i].CuisineName, "RecipeMapStatus": sitedishdata[i].RecipeMapStatus
            //    });
            //}
            console.log("chkdishcateg")
            console.log(dishdata);
            console.log(data);

            dishcatwithdishData = [];
            for (var i = 0; i < sitedishcatlist.length; i++) {
                var filteredDishes = dishdata.filter(ds => ds.DishCategoryID == sitedishcatlist[i].ID);
                dishcatwithdishData.push({
                    "DishCategoryName": sitedishcatlist[i].Name,
                    "DishCategoryID": sitedishcatlist[i].ID,
                    "Dishes": filteredDishes,
                    "DishCategoryCode": sitedishcatlist[i].DishCategoryCode
                });
                //dcsData.push({ "DishCategoryName": filteredDc[j].DishCategoryName, "DishCategoryID": filteredDc[j].DishCategoryID, "Dishes": filteredDc[j].Dishes, "DishCategoryCode": filteredDc[j].DishCategoryCode })
            }
            console.log("dishcatwithdishData");
            console.log(dishcatwithdishData)
            //console.log("dishdata")
            //console.log(dishdata)


            //Old Code copied here from outside server call
            console.log("dishcatwithdishData")
            console.log(dishcatwithdishData);
            newdishcatwithdishData = [];
            for (var i = 0; i < dishcatwithdishData.length; i++) {
                newdishcatwithdishData.push(dishcatwithdishData[i]);
            }

            for (var i = 0; i <= noOfDaysData; i++) {
                if (i == 0) {
                    //text
                    var headerCell = document.createElement("TD");
                    var inputDishCTxt = document.createElement("div");
                    //inputDishCTxt.type = "text";
                    inputDishCTxt.className = "form-control";
                    inputDishCTxt.id = "inputDishCTxt_" + itemid + "_" + dpcode;
                    inputDishCTxt.style.width = "100%!important";
                    inputDishCTxt.classList.add("multitSelectdishcatdish");
                    headerCell.appendChild(inputDishCTxt);
                    newRow.appendChild(headerCell);

                    var filteredDc = filtered.DishCategories;
                    //var filteredDc = dishdata;

                    // var dishCats = [];
                    //for (var ij = 0; ij < filteredDc.length; ij++) {
                    //    var chk = dcsData.filter(a => a.DishCategoryID == filteredDc[ij].DishCategoryID);
                    //    if (chk.length == 0) {
                    //        var dishes = filteredDc.filter(a => a.DishCategoryID == filteredDc[ij].DishCategoryID);
                    //        var formatDish = [];
                    //        for (var id = 0; id < dishes.length; id++) {
                    //               formatDish.push({
                    //                   "DishID": dishes[id].value,
                    //                   "DishName": dishes[id].text,
                    //                   "CostPerKG": dishes[id].price
                    //            });
                    //        }
                    //        //dishes.forEach(function (ele) {
                    //        //    formatDish.push({
                    //        //        "DishID": ele.value,
                    //        //        "DishName": ele.text,
                    //        //        "CostPerKG": ele.price
                    //        //    });
                    //        //});
                    //        if (filteredDc[ij].DishCategoryName != undefined) {

                    //            dcsData.push({
                    //                "DishCategoryName": filteredDc[ij].DishCategoryName, "DishCategoryID": filteredDc[ij].DishCategoryID, "Dishes": formatDish, "DishCategoryCode": filteredDc[ij].DishCategoryCode
                    //            });
                    //        }
                    //    }
                    //}

                    for (var j = 0; j < filteredDc.length; j++) {

                        dcsData.push({ "DishCategoryName": filteredDc[j].DishCategoryName, "DishCategoryID": filteredDc[j].DishCategoryID, "Dishes": filteredDc[j].Dishes, "DishCategoryCode": filteredDc[j].DishCategoryCode });
                        //deduct selected dcs from all dcs
                        newdishcatwithdishData = newdishcatwithdishData.filter(ds => ds.DishCategoryCode != filteredDc[j].DishCategoryCode);
                    }

                }
                else {
                    //var headerCell = document.createElement("TD");
                    //var tableHead = document.createElement("TABLE");
                    //tableHead.style.width = "100%";
                    //var rowth = tableHead.insertRow(-1);
                    //var headerCellth = document.createElement("TD");
                    //headerCellth.style.width = "52px";
                    //headerCellth.innerHTML = "";
                    //rowth.appendChild(headerCellth);
                    //headerCellth = document.createElement("TD");
                    //headerCellth.innerHTML = "";
                    //rowth.appendChild(headerCellth);
                    //headerCellth = document.createElement("TD");
                    //headerCellth.innerHTML = "";
                    //rowth.appendChild(headerCellth);
                    //headerCell.appendChild(tableHead);
                    //newRow.appendChild(headerCell);
                }
            }

            //$(newRow).insertAfter($(row));
            //New way
            //"tableAddDC_" + selecteditem[it].dpcode + "_" + itemidvalue
            //var getTable = document.getElementById("tableAddDC_" + dpcode + "_" + itemid);
            var getTable = document.getElementById("tableAddDC_" + dpcode + "_" + itemid);
            getTable.appendChild(newRow);

            $(spanAddDc).removeClass("fa-plus");
            $(spanAddDc).addClass("fa-times");

            console.log("newdishcatwithdishData")
            console.log(newdishcatwithdishData);
            for (var i = 0; i < noOfDaysData; i++) {
                if (i == 0) {
                    console.log("dcsData")
                    console.log(dcsData)
                    var dishctMultiId = "#inputDishCTxt_" + itemid + "_" + dpcode;
                    var dcsMulti = $(dishctMultiId).kendoMultiSelect({
                        filter: "contains",
                        dataTextField: "DishCategoryName",
                        dataValueField: "DishCategoryID",
                        //maxSelectedItems :1,
                        //dataSource: dcsData,
                        dataSource: newdishcatwithdishData,
                        placeholder: " ",
                        index: 0,
                        autoBind: true,
                        autoClose: false,
                        open: function (e) { onclickDc(itemid, dpcode); },
                        change: function (e) { onDcsSelected(e, dpcode, itemid); }
                    }).data("kendoMultiSelect");
                }
            }


            Utility.UnLoading();
        }, { siteCode: $("#hdnSiteCode").val() }, true);


    }
    else if ($(spanAddDc).hasClass("fa-times")) {
        var rowId = "#newRow_" + dpcode + "_" + itemid;
        $(rowId).remove();
        $(spanAddDc).removeClass("fa-times");
        $(spanAddDc).addClass("fa-plus");
    }
}
function onclickDc(itid, dpcode) {
    //removedDishCats
    var contextData = JSON.parse(localStorage.getItem("contextitem"));
    if (contextData != undefined && contextData.length > 0) {
        var filtered = contextData.filter(item => item.value == itid + "_" + dpcode && item.dpcode == dpcode)[0];
        var filteredDc = filtered.DishCategories;
        var newdishcatwithdishData = [];
        //var filteredDs = dishcatwithdishData.filter(dc => dc.DishCategoryCode == dccode);

        for (var i = 0; i < dishcatwithdishData.length; i++) {
            var chkFd = filteredDc.filter(a => a.DishCategoryCode == dishcatwithdishData[i].DishCategoryCode)[0];

            if (chkFd == undefined) {
                //var chkrdc = removedDishCats.filter(ds => ds.itemid == itid && ds.dpcode == dpcode);
                //if (chkrdc.lenght > 0) {
                //newdishcatwithdishData.push(dishcatwithdishData[i]);
                var chk = newsitedishcats.filter(ds => ds.DishCategoryCode == dishcatwithdishData[i].DishCategoryCode && ds.DayPartCode == dpcode && ds.ItemID == itid)
                if (chk.length > 0) {
                    //var filteredData = dishcatwithdishData[i].Dishes.filter(function (item) {
                    //    return (
                    //        chk[0].Dishes.filter(x=>x.DishCode==item.DishCode).length>0
                    //    );
                    //});
                    //dishcatwithdishData[i].Dishes.filter(a=>a.)
                    dishcatwithdishData[i].Dishes = chk[0].Dishes;
                    //console.log("chk");
                    //console.log(chk);
                    newdishcatwithdishData.push(dishcatwithdishData[i]);
                } else
                    newdishcatwithdishData.push(dishcatwithdishData[i]);
                //}
            }
            //if (chkFd != undefined && chkFd.DishCategoryCode == dccode)
            //    newdishcatwithdishData.push(dishcatwithdishData[i]);
        }

        var dishctMultiId = "#inputDishCTxt_" + itid + "_" + dpcode;
        var multiselect = $(dishctMultiId).data("kendoMultiSelect");
        var ds = new kendo.data.DataSource({ data: newdishcatwithdishData });
        multiselect.setDataSource(ds);
    }
}
function onDcsSelected(inputDishCTxt, dpcode, itemid) {
    //var dcsMulti ="#inputDishCTxt_"
    var dishctMultiId = "#inputDishCTxt_" + itemid + "_" + dpcode;
    var selecteditems = $(dishctMultiId).data("kendoMultiSelect").dataItems();
    //selecteditems = selecteditems.filter(a => a.value == itemid + "_" + dpcode);
    console.log("selecteditems after item filter");
    console.log(selecteditems);

    var contextitem = [];
    contextitem = JSON.parse(localStorage.getItem("contextitem"));
    //contextitem = selecteditemData;
    for (var si = 0; si < selecteditems.length; si++) {
        var currentdcRowId = ".row_dc_" + dpcode + "_" + selecteditems[si].DishCategoryCode;
        var filtered = contextitem.filter(item => item.value == itemid + "_" + dpcode && item.dpcode == dpcode)[0].DishCategories;
        var dcFiltered = filtered.filter(dc => dc.DishCategoryCode == selecteditems[si].DishCategoryCode);
        console.log("contextitem")
        console.log(contextitem)
        console.log("selecteditemData")
        console.log(selecteditemData)
        if (dcFiltered.length == 0) {
            //var contextDc = selecteditemData.filter(item => item.value == itemid+"_"+dpcode && item.dpcode == dpcode)[0].DishCategories;
            //var contextDc = dishcatwithdishData.filter(item => item.value == itemid + "_" + dpcode && item.dpcode == dpcode);
            var dcContext = dishcatwithdishData.filter(dc => dc.DishCategoryCode == selecteditems[si].DishCategoryCode);

            //contextitem.filter(item => item.value == itemid + "_" + dpcode && item.dpcode == dpcode)[0].DishCategories.push(dcContext[0]);
            //console.log("sid")
            //console.log(contextitem);
            //localStorage.setItem("contextitem", JSON.stringify(contextitem));

            //Loop throgh dish categories and add all over
            var cRowId = "#rowAddDc_" + dpcode + "_" + itemid;
            var dccode = selecteditems[si].DishCategoryCode;
            var dcname = selecteditems[si].DishCategoryName;
            var row3 = document.createElement("tr");
            //var row3 = divdishCategoryMain.insertRow(-1);
            row3.className = "row-striped";
            row3.id = "row_AddDc_" + dpcode + "_" + dccode + "_" + itemid;
            row3.classList.add("expand_dc_" + itemid + "_" + dpcode);
            row3.classList.add("row_dc_" + dpcode + "_" + dccode + "_" + itemid);
            //itemdata.push({ "value": dataSource[i].ItemID, "text": dataSource[i].ItemName, "code": dataSource[i].ItemCode, "dpcode": dataSource[i].DayPartCode });
            for (var i = 0; i <= noOfDaysData; i++) {
                if (i == 0) {
                    var headerCell = document.createElement("td");
                    headerCell.className = "mg_top_header_dc_body";
                    //headerCell.innerHTML = itemDATA.DishCategories[dsc].DishCategoryName;
                    var div = document.createElement("div");
                    //div.innerHTML = itemDATA.DishCategories[dsc].DishCategoryName;
                    div.id = "span_dc_" + dpcode + "_" + dccode;
                    var spanText = document.createElement("div");
                    //spanText.style.width = "114px";
                    spanText.style.width = "203px";
                    spanText.style.display = "inline-block";

                    spanText.innerHTML = dcname;

                    var spanAddDc = document.createElement("i");
                    spanAddDc.className = "fas fa-plus";
                    spanAddDc.style.paddingLeft = "5px";
                    spanAddDc.style.color = "lightseagreen";
                    spanAddDc.style.cursor = "pointer";
                    spanAddDc.id = "spanAddDc_" + dpcode + "_" + itemid + "_" + dccode;
                    //spanAddDc.style.marginLeft = "23px";
                    spanAddDc.addEventListener('click', function () {
                        //result.textContent = this.value;
                        var dpcode = this.id.split('_')[1];
                        var itemid = this.id.split('_')[2];
                        var dccode = this.id.split('_')[3];
                        //onClickExpandDishes(this, dpcode, dccode);
                        addDish(dpcode, itemid, dccode);
                    });
                    spanText.appendChild(spanAddDc);

                    div.appendChild(spanText);

                    var spanCloseBtn = document.createElement("span");
                    spanCloseBtn.className = "fas fa-times";
                    spanCloseBtn.id = "span_delDC_" + dpcode + "_" + dccode + "_" + itemid;
                    //spanCloseBtn.style.marginLeft = "98px";
                    spanCloseBtn.addEventListener('click', function () {

                        var dccode = this.id.split('_')[3];
                        var dpcode = this.id.split('_')[2];
                        var itid = this.id.split('_')[4];
                        deleteDishCategory(null, dpcode, dccode, itid);
                    });
                    div.appendChild(spanCloseBtn);
                    headerCell.appendChild(div);
                    headerCell.style.paddingLeft = "25px";
                    row3.appendChild(headerCell);
                }
                else if (i == 1) {
                    var headerCell = document.createElement("TD");
                    headerCell.className = "mg_top_header_ds_title";
                    headerCell.setAttribute("colspan", noOfDaysData);
                    var tableHead = document.createElement("TABLE");
                    console.log("$(#card-header-forcalling).width()")
                    console.log($("#card-header-forcalling").width())
                    tableHead.style.width = parseInt(parseInt($("#card-header-forcalling").width()) - 240) + "px!important";
                    tableHead.id = "tableAddDS_" + dpcode + "_" + itemid + "_" + dccode;
                    tableHead.className = "cardHeaderInheritance";
                    //var rowth = tableHead.insertRow(-1);
                    //var headerCellth = document.createElement("TD");
                    //headerCellth.style.width = "60px";
                    //headerCellth.innerHTML = "";
                    //rowth.appendChild(headerCellth);
                    //headerCellth = document.createElement("TD");
                    //headerCellth.innerHTML = ""
                    //rowth.appendChild(headerCellth);
                    //headerCellth = document.createElement("TD");
                    //headerCellth.innerHTML = ""
                    //rowth.appendChild(headerCellth);

                    headerCell.appendChild(tableHead);
                    row3.appendChild(headerCell);
                }
                //else {
                //    var headerCell = document.createElement("TD");
                //    headerCell.className = "mg_top_header_dc_body";
                //    var tableHead = document.createElement("TABLE");
                //    tableHead.style.width = "240px";
                //    var rowth = tableHead.insertRow(-1);
                //    var headerCellth = document.createElement("TD");
                //    headerCellth.style.width = "60px";
                //    headerCellth.innerHTML = "";
                //    rowth.appendChild(headerCellth);
                //    headerCellth = document.createElement("TD");
                //    headerCellth.innerHTML = ""
                //    rowth.appendChild(headerCellth);
                //    headerCellth = document.createElement("TD");
                //    headerCellth.innerHTML = ""
                //    rowth.appendChild(headerCellth);

                //    headerCell.appendChild(tableHead);
                //    row3.appendChild(headerCell);
                //}
            }
            //$(row3).insertAfter($(cRowId));
            //newRow_DPC-00001_74
            //var lastClass = "#newRow_" + dpcode + "_" + itemid;
            //rowAddDc_DPC-00001_74
            var lastClass = "#rowAddDc_" + dpcode + "_" + itemid;
            $(row3).insertAfter($(lastClass));

            //8th April           
            var gettableHead = document.getElementById("tableAddDS_" + dpcode + "_" + itemid + "_" + dccode);
            var newWidth = parseInt(parseInt($("#card-header-forcalling").width()) - 250) + "px!important";
            $(gettableHead).attr("style", "width:" + newWidth);

            //insertAfter(row3, $(cRowId));
            //divdishCategoryMain.appendChild(row3);
            //Level 4 dishes

            for (var dis = 0; dis < selecteditems[si].Dishes.length; dis++) {
                //console.log(selecteditems[si].Dishes[dis].DishName)
                //console.log(selecteditems[si].Dishes[dis].DishID)
                var dataO = [];
                var row4 = document.createElement("tr");
                //var row4 = divdishCategoryMain.insertRow(-1);
                row4.className = "row-striped";
                row4.classList.add("expand_dc_" + itemid + "_" + dpcode);
                row4.classList.add("expand_dc_" + dpcode + "_" + dccode);
                row4.classList.add("row_ds_" + dpcode + "_" + dccode + "_" + selecteditems[si].Dishes[dis].DishID + "_" + itemid);
                row4.classList.add("dishes_forslidingpurpose");
                for (var i = 0; i <= noOfDaysData; i++) {
                    if (i == 0) {
                        var headerCell = document.createElement("td");
                        headerCell.className = "mg_top_header_ds_body";
                        //headerCell.innerHTML = itemDATA.DishCategories[dsc].Dishes[dis].DishName;
                        //headerCell.style.paddingLeft = "50px";
                        headerCell.style.width = "240px";

                        var divCb = document.createElement("div");
                        //divCb.innerHTML = itemDATA.DishCategories[dsc].Dishes[dis].DishName;
                        var spanText = document.createElement("span");
                        //spanText.style.width = "72px";
                        //spanText.style.width = "121px";
                        //spanText.style.width = "163px";
                        spanText.style.width = "160px";

                        spanText.style.display = "inline-block";
                        spanText.innerHTML = selecteditems[si].Dishes[dis].DishName;

                        //Recipe button
                        var spanRecipeBtn = document.createElement("span");
                        //spanRecipeBtn.className = "fa fa-solid fa-link";
                        spanRecipeBtn.className = "fas fa-utensils";
                        spanRecipeBtn.style.color = "deepskyblue";
                        spanRecipeBtn.id = "span_recipeDC_" + dpcode + "_" + selecteditems[si].DishCategoryCod + "_" + selecteditems[si].Dishes[dis].DishID + "_" + itemid + "_" + selecteditems[si].Dishes[dis].DishCode;
                        //spanCloseBtn.style.marginLeft = "68px";
                        spanRecipeBtn.addEventListener('click', function () {

                            var dccode = this.id.split('_')[3];
                            var dpcode = this.id.split('_')[2];
                            var did = this.id.split('_')[4];
                            var itemid1 = this.id.split('_')[5];
                            var dishcode = this.id.split('_')[6];
                            openRecipe(itemid1, did, dccode, dishcode, dpcode);
                            //openRecipe(itemid1, did, dccode);
                        });
                        divCb.style.paddingLeft = "42px";
                        divCb.appendChild(spanRecipeBtn);
                        divCb.appendChild(spanText);

                        var spanCloseBtn = document.createElement("span");
                        spanCloseBtn.className = "fas fa-times";
                        spanCloseBtn.id = "span_delDC_" + dpcode + "_" + selecteditems[si].DishCategoryCode + "_" + selecteditems[si].Dishes[dis].DishID + "_" + itemid;
                        //spanCloseBtn.style.marginLeft = "68px";
                        spanCloseBtn.addEventListener('click', function () {

                            var dccode = this.id.split('_')[3];
                            var dpcode = this.id.split('_')[2];
                            var did = this.id.split('_')[4];
                            var itid = this.id.split('_')[5];
                            deleteDish(row4, dpcode, dccode, did, itid);
                        });
                        divCb.appendChild(spanCloseBtn);
                        headerCell.appendChild(divCb);
                        row4.appendChild(headerCell);

                    }
                    else {
                        dataO.push({ "index": i, "paxTxt": "", "grmTxt": "", "costTxt": "" });;
                        var headerCell = document.createElement("TD");
                        headerCell.style.paddingRight = "0";
                        headerCell.style.paddingLeft = "3px";
                        var inputTxt = document.createElement("input");
                        inputTxt.className = "txtPax_" + itemid + "_" + dpcode + "_" + i;
                        inputTxt.classList.add("txtTotalPax" + i);
                        inputTxt.classList.add("addedDishes");

                        inputTxt.type = "text";
                        //inputTxt.id = "txtDsxroot_" + dis + "_" +  itemDATA.DishCategories[dsc].Dishes[dis].DishID;
                        inputTxt.id = "txtPax_" + i + "_" + itemid + "_" + dpcode + "_" + selecteditems[si].Dishes[dis].DishID;
                        inputTxt.setAttribute("data-cost", selecteditems[si].Dishes[dis].CostPerKG);
                        inputTxt.style.width = "50px";
                        inputTxt.style.marginLeft = "2px";
                        //headerCellth.style.width = "60px";
                        //pre populate root pax
                        var rootPax = ".txtPxroot_" + itemid + "_" + i + "_" + dpcode;
                        inputTxt.value = $(rootPax).val();

                        inputTxt.addEventListener('input', function () {
                            //result.textContent = this.value;
                            var itemid = this.id.split('_')[2];
                            var dpcode = this.id.split('_')[3];
                            var cost = this.getAttribute("data-cost");
                            var colIndex = this.id.split('_')[1];
                            var dishid = this.id.split('_')[4];
                            var txtGrmInp = "txtGrmroot_" + itemid + "_" + dpcode + "_" + cost + "_" + colIndex + "_" + dishid;
                            onInputGrm(txtGrmInp, itemid, dpcode, cost, colIndex, dishid);

                            //calculateTotals();
                        });
                        //inputTxt.style.width = "50px";
                        //inputTxt.addEventListener('input', function () {
                        //    //result.textContent = this.value;
                        //    calculateTotals();
                        //});
                        var inputTxtGrm = document.createElement("input");
                        inputTxtGrm.type = "text";
                        inputTxtGrm.id = "txtGrmroot_" + itemid + "_" + dpcode + "_" + selecteditems[si].Dishes[dis].CostPerKG + "_" + i + "_" + selecteditems[si].Dishes[dis].DishID;
                        inputTxtGrm.style.width = "50px";
                        inputTxtGrm.style.marginLeft = "8px";
                        inputTxtGrm.className = "txtTotalGrm" + i;
                        inputTxtGrm.classList.add("txtGrmroot_" + i + "_" + itemid + "_" + dpcode + "_" + selecteditems[si].Dishes[dis].DishID);
                        inputTxtGrm.classList.add("txtGrmroot_" + i + "_" + itemid + "_" + dpcode);
                        inputTxtGrm.classList.add("addedDishes");
                        inputTxtGrm.classList.add("txtGrmroots");
                        //inputTxtGrm.value = selecteditems[si].Dishes[dis].ServedPortion;
                        if (selecteditems[si].Dishes[dis].ServedPortion != undefined)
                            inputTxtGrm.value = Utility.MathRound(selecteditems[si].Dishes[dis].ServedPortion, 2);
                        inputTxtGrm.addEventListener('input', function () {
                            //result.textContent = this.value;
                            validateU2D(this);
                            var itemid = this.id.split('_')[1];
                            var dpcode = this.id.split('_')[2];
                            var cost = this.id.split('_')[3];
                            var colIndex = this.id.split('_')[4];
                            var dishid = this.id.split('_')[5];
                            var txtGrmInp = "txtGrmroot_" + itemid + "_" + dpcode + "_" + cost + "_" + colIndex + "_" + dishid;
                            onInputGrm(txtGrmInp, itemid, dpcode, cost, colIndex, dishid);
                        });
                        var tableHead = document.createElement("TABLE");
                        //April 7th
                        //tableHead.style.width = "240px";
                        var rowth = tableHead.insertRow(-1);
                        var headerCellth = document.createElement("TD");
                        headerCellth.style.width = "30%";
                        headerCellth.style.textAlign = "center";
                        headerCellth.className = "mg_top_header_item_text";
                        headerCellth.appendChild(inputTxt);
                        rowth.appendChild(headerCellth);
                        headerCellth = document.createElement("TD");
                        //headerCellth.style.width = "50px";
                        headerCellth.style.width = "30%";
                        headerCellth.className = "mg_top_header_item_text"
                        headerCellth.style.textAlign = "center";
                        headerCellth.appendChild(inputTxtGrm);
                        rowth.appendChild(headerCellth);
                        headerCellth = document.createElement("TD");
                        headerCellth.style.width = "50px";
                        //7th April
                        headerCellth.style.paddingBottom = "0";
                        var lblCost = document.createElement("label");
                        lblCost.id = "lblCost_" + i + "_" + itemid + "_" + dpcode + "_" + selecteditems[si].Dishes[dis].DishID;
                        lblCost.innerHTML = "";
                        lblCost.className = "lblTotalCost" + i;
                        lblCost.style.width = "50px";
                        //lblCost.style.marginLeft = "17px";
                        lblCost.classList.add("lblCostForItem_" + itemid + "_" + dpcode);
                        headerCellth.style.textAlign = "center";
                        headerCellth.appendChild(lblCost);
                        rowth.appendChild(headerCellth);

                        headerCell.appendChild(tableHead);
                        row4.appendChild(headerCell);
                    }
                }
                //if(dis==0)
                var lastClass = ".row_dc_" + dpcode + "_" + dccode + "_" + itemid;
                $(row4).insertAfter($(lastClass).last());
                //insertAfter(row4, row3);
                // else
                selecteditems[si].Dishes[dis].dataO = dataO;
                //divdishCategoryMain.appendChild(row3);
            }
            dcContext[0].Dishes = selecteditems[si].Dishes;
            contextitem.filter(item => item.value == itemid + "_" + dpcode && item.dpcode == dpcode)[0].DishCategories.push(dcContext[0]);
            console.log("contextitem after applying dataO")
            console.log(contextitem);
            localStorage.setItem("contextitem", JSON.stringify(contextitem));

            //7th April
            for (var dis = 0; dis < selecteditems[si].Dishes.length; dis++) {
                for (var i = 1; i <= noOfDaysData; i++) {
                    var inputTxtGrm = "txtGrmroot_" + itemid + "_" + dpcode + "_" + selecteditems[si].Dishes[dis].CostPerKG + "_" + i + "_" + selecteditems[si].Dishes[dis].DishID;
                    onInputGrm(inputTxtGrm, itemid, dpcode, selecteditems[si].Dishes[dis].CostPerKG, i, selecteditems[si].Dishes[dis].DishID);
                }
            }
        }
    }
}
var mappedRecipeCode = [];
var recipeCustomData = [];
var mappedData = [];
function openRecipe(itemid, dishid, dishcategorycode, dishcode, dpcode) {

    var simId = $("#SimulationID").val();
    if (simId > 0) {
        Utility.Loading();
        setTimeout(function () {
            $("#divSimConfigMain").hide();
            $("#divBoard").hide();
            $("#windowEdit").show();
            $("#hdnDishId").val(dishid);
            $("#hdnItemId").val(itemid);
            $("#hdnDishCatCode").val(dishcategorycode);
            $("#hdnDishCode").val(dishcode);
            $("#hdnDayPartCode").val(dpcode);

            //reset mog grid
            $("#gridMOG").html("");
            $("#gridMOG").hide();
            $("#emptymog").show();
            $("#inputrecipealiasname").val("");
            $("#inputquantity").val("10");
            var recipeum = $("#inputrecipecum").data("kendoDropDownList");
            if (recipeum != undefined)
                $("#inputrecipecum").data("kendoDropDownList").value("Select");
            //$("#copyRecipe").data("kendoDropDownList").value("Select");
            //20th April
            $("#totalchangepercent").hide();
            $("#grandCostPerKG").html(Utility.AddCurrencySymbol(0, 2));
            $("#grandTotal").html(Utility.AddCurrencySymbol(0, 2));
            $("#ToTIngredients").html("");
            $("#totmogspan").hide();
            $("#totmog").text("");

            //Top nav
            $("#divSimTopNav").hide();
            //var getsiteid = $("#NewSimSiteByRegion").val();
            var siteCode = $("#hdnSiteCode").val();
            var DishCode = dishcode;
            mappedRecipeCode = [];
            mappedData = [];

            modelHistoryData = [];
            //HttpClient.MakeSyncRequest(CookBookMasters.GetSiteDishRecipeMappingDataList, function (data) {

            //    mappedData = data;
            //    for (var d of mappedData) {
            //        mappedRecipeCode.push(d.RecipeCode);
            //    }

            //}, { SiteCode: siteCode, DishCode: DishCode }, true);
            //HttpClient.MakeSyncRequest(CookBookMasters.GetSiteRecipeDataList, function (data) {
            //    recipeCustomData = [];
            //    copyData = [];
            //    recipeCustomData = data;
            //    copyData = data;

            //    if (mappedData.length > 0) {
            //        recipeCustomData = recipeCustomData.filter(function (item) {
            //            for (var d of mappedData) {
            //                if (d.RecipeCode == item.RecipeCode) {
            //                    item.IsDefault = d.IsDefault;
            //                    item.DishCode = d.DishCode;
            //                    item.IsActive = d.IsActive;
            //                    return item;
            //                }
            //            }
            //        })
            //    }
            //}, { SiteCode: siteCode, condition: "All" }, true);
            copyData0 = [];

            HttpClient.MakeSyncRequest(CookBookMasters.UpdateAndFetchSiteDishRecipeData, function (data) {
                mappedData = data;
                for (var d of mappedData) {
                    mappedRecipeCode.push(d.RecipeCode);
                }
                //console.log(mappedData)
            }, {
                simCode: $("#SimulationCode").val(), SiteCode: siteCode, DishCode: DishCode
            }, true);
            HttpClient.MakeSyncRequest(CookBookMasters.UpdateAndFetchSiteRecipeData, function (data) {
                recipeCustomData = [];
                copyData = [];
                recipeCustomData = data;
                copyData = data;

                if (mappedData.length > 0) {
                    recipeCustomData.forEach(function (item, index) {
                        var chkmd = mappedData.filter(a => a.RecipeCode == item.RecipeCode)[0];
                        if (chkmd != undefined) {
                            item.IsDefault = chkmd.IsDefault;
                        }
                    });
                }
                copyData0 = recipeCustomData;

            }, {
                simCode: $("#SimulationCode").val(), SiteCode: siteCode, DishCode: DishCode
            }, true);

            console.log("recipeCustomData")
            console.log(recipeCustomData)

            //if (recipeCustomData.length > 0) {
            //copyData0 = [];

            //if (isShowElmentoryRecipe) {
            //    result = result.filter(m => m.IsElementory);
            //}
            ////options.success(result);
            //copyRecipe
            if (copyData0.length > 0) {
                var chk = copyData0.filter(a => a.Name == "Select Recipe");
                if (chk.length == 0)
                    copyData0.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
            }
            else {

                copyData0.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
            }
            var chk1 = copyData.filter(a => a.Name == "Select Recipe");
            if (chk1.length == 0)
                copyData.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });

            console.log(copyData0)
            populateRecipeCopy0DropDown();


            if (viewOnly == 1) {
                $("#btnSubmitRecipe").hide();
                $("#AddNewRecipe").hide();

                $("#totmogspan").hide();
                $("#totmog").hide();

                $("#addmog").hide();

                //$("#windowEdit input[type=text]").attr("disabled", true);
                //var dropdownlist = $("#inputrecipecum").data("kendoDropDownList");
                //dropdownlist.enable(false);
                // dropdownlist = $("#copyRecipe0").data("kendoDropDownList");
                //dropdownlist.enable(false);
                //$($('#inputinstruction').data().kendoEditor.body).attr('contenteditable', false)

            }
            else {
                $("#btnSubmitRecipe").show();
                $("#AddNewRecipe").show();

                $("#totmogspan").show();
                $("#totmog").show();
                $("#addmog").show();

                //$("#windowEdit input[type=text]").removeAttr("disabled");
                //var dropdownlist = $("#inputrecipecum").data("kendoDropDownList");
                //dropdownlist.enable(true);
                //dropdownlist = $("#copyRecipe0").data("kendoDropDownList");
                //dropdownlist.enable(true);

                //$($('#inputinstruction').data().kendoEditor.body).attr('contenteditable', true)
            }
        }, 100);
    }
    else {
        toastr.warning("Please save the selected simulation.");
    }
    /* Utility.UnLoading();*/
    //}
    //else {

    //}
    //HttpClient.MakeSyncRequest(CookBookMasters.GetSiteDishRecipeList, function (result) {
    //    if (result != null && result.length > 0) {
    //        copyData0 = [];
    //        copyData0 = result;
    //        if (isShowElmentoryRecipe) {
    //            result = result.filter(m => m.IsElementory);
    //        }
    //        //options.success(result);
    //        //copyRecipe
    //        copyData0.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
    //        populateRecipeCopy0DropDown();

    //        Utility.UnLoading();
    //    }
    //    else {
    //        //options.success("");

    //        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteRecipeDataList, function (result) {
    //            copyData0 = [];
    //            copyData0 = result;
    //            if (isShowElmentoryRecipe) {
    //                result = result.filter(m => m.IsElementory);
    //            }
    //            //options.success(result);
    //            //copyRecipe
    //            copyData0.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
    //            populateRecipeCopy0DropDown();
    //            Utility.UnLoading();
    //        },
    //            {
    //                SiteCode: code,
    //                condition: "All"
    //            }
    //            , true);
    //    }

    //},
    //    {
    //        SiteCode: code,
    //        dishCode: dishcode,
    //        condition: "All"
    //    }
    //    , true);
}
//function calcTwoDecimalWithoutRound(theform) {
//    var num = theform.original.value, rounded = theform.rounded
//    var with2Decimals = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
//    rounded.value = with2Decimals
//}
function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
Number.prototype.toFixedNoRounding = function (n) {
    const reg = new RegExp("^-?\\d+(?:\\.\\d{0," + n + "})?", "g")
    const a = this.toString().match(reg)[0];
    const dot = a.indexOf(".");
    if (dot === -1) { // integer, insert decimal dot and pad up zeros
        return a + "." + "0".repeat(n);
    }
    const b = n - (a.length - dot) + 1;
    return b > 0 ? (a + "0".repeat(b)) : a;
}