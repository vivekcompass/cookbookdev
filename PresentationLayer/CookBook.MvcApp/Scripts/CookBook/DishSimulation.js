﻿var dishMasterData = [];
var recipeCustomData = [];
var mappedData = [];
var multiarray = [];
var sitemasterdata = [];
var regionmasterdata = [];
var basedataList = [];
var baserecipedata = [];
function populateRegionMasterDropdown() {
    $("#ddRegion").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "RegionID",
        dataSource: regionmasterdata,
        index: 0,
    });

}

function populateSiteMasterDropdown() {
    $("#ddSite").kendoDropDownList({
        filter: "contains",
        dataTextField: "SiteName",
        dataValueField: "SiteCode",
        dataSource: sitemasterdata,
        index: 0,
    });
}

function populateDishDropdown() {
    $("#ddDish").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "DishCode",
        dataSource: dishMasterData,
        index: 0,
    });
}


function populateRecipeDropdown() {
    $("#ddRecipe").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "RecipeCode",
        dataSource: recipeCustomData,
        index: 0,
    });

    var dropdownlist = $("#ddRecipe").data("kendoDropDownList");
    dropdownlist.bind("change", recipedropdownlist_select);
    Utility.UnLoading();
}
function recipedropdownlist_select(e) {

    //var RecipeCode = e.sender.element.context.value;
    var dropDownData = $("#ddRecipe").data("kendoDropDownList");
    var tr = dropDownData.dataItem();
    populateBaseRecipeGrid(tr.ID);
    populateMOGGrid(tr.ID)
   // alert(tr.ID)
}
HttpClient.MakeRequest(CookBookMasters.GetDishDataList, function (data) {

    dishMasterData = data;
    dishMasterData.unshift({ "DishCode": 0, "Name": "Select Dish" })
    populateDishDropdown();
    
}, null, false);

HttpClient.MakeRequest(CookBookMasters.GetSiteMasterList, function (data) {
  
    sitemasterdata = data;
    sitemasterdata.unshift({ "SiteCode": 0, "SiteName": "Select" });
    populateSiteMasterDropdown();
}, null, false);

HttpClient.MakeRequest(CookBookMasters.GetRegionMasterList, function (data) {
    regionmasterdata = data;
    regionmasterdata.unshift({ "RegionID": 0, "Name": "Select Region" })
    populateRegionMasterDropdown();
}, null, false);

function customData(DishCode) {
    mappedRecipeCode = [];
    HttpClient.MakeSyncRequest(CookBookMasters.GetDishRecipeMappingDataList, function (data) {

        mappedData = data;
        for (var d of mappedData) {
            mappedRecipeCode.push(d.RecipeCode);
        }

    }, { DishCode: DishCode }, false);

    HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeMappingDataList, function (data) {
        recipeCustomData = data;
        
    }, { DishCode: DishCode }, true);
    recipeCustomData.unshift({ "RecipeCode": 0, "Name": "Select Recipe" })

    populateRecipeDropdown();
}

function PopulateRecipe(){
    $('#DishMaster').css("display", "block");
    var dropDownData = $("#ddDish").data("kendoDropDownList");
    var tr = dropDownData.dataItem();

    $("#itemCode").text(tr.ItemCode);
    $("#inputdishname1").text(tr.ItemName);
    $("#inputdishcode1").text(tr.DishCode);
    if (tr.Name != tr.DishAlias)
        $("#inputdishname1").text(tr.Name + "(" + tr.DishAlias + ")");
    else
        $("#inputdishname1").text(tr.Name);
    $("#inputdishtype1").text(tr.DishTypeName);
    $("#inputdishcategory1").text(tr.DishCategoryName);
    $("#itemName").attr('title', tr.ItemName);
    if (tr.DietCategory_ID == 3) {
        $("#itemDietCategoryStatus").removeClass("statusNonVeg statusEgg");
        $("#itemDietCategory").removeClass("squareNonVeg squareEgg");
        $("#itemDietCategoryStatus").addClass("statusVeg");
        $("#itemDietCategory").addClass("squareVeg")
    } else if (tr.DietCategory_ID == 2) {
        $("#itemDietCategoryStatus").removeClass("statusVeg statusEgg");
        $("#itemDietCategory").removeClass("squareVeg squareEgg");
        $("#itemDietCategory").addClass("squareNonVeg")
        $("#itemDietCategoryStatus").addClass("statusNonVeg")
    } else {
        $("#itemDietCategoryStatus").removeClass("statusNonVeg statusVeg");
        $("#itemDietCategory").removeClass("squareNonVeg squareVeg");
        $("#itemDietCategory").addClass("squareEgg")
        $("#itemDietCategoryStatus").addClass("statusEgg")
    }
    customData(tr.DishCode);
}

function showHideGrid(gridId, emptyGrid, uid) {
    var gridLength = $("#" + gridId).data().kendoGrid.dataSource.data().length;

    if (gridLength == 0) {
        $("#" + emptyGrid).toggle();
        $("#" + gridId).hide();
    }
    else {
        $("#" + emptyGrid).hide();
        $("#" + gridId).toggle();
    }

    if (!($("#" + gridId).is(":visible") || $("#" + emptyGrid).is(":visible"))) {
        $("#" + uid).addClass("bottomCurve");

    } else {
        $("#" + uid).removeClass("bottomCurve");

    }

}

function populateBaseRecipeGrid(recipeID) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [
         {
            name: "create", text: "Add More"
        },
            //    {
            //    name: "new", text: "Create New"
            //},

        ],
        groupable: false,
        editable: true,
        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = `  <i title="Click to Change Base Recipe" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>`
                        + '<span class="brspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: refreshBaseRecipeDropDownData(),
                            value: options.field,
                            change: onBRDDLChange
                        });
                }
            },

            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                template: function (dataItem) {
                    var html = '<input  type="number" class="inputbrqty" style="text - align: center;" name="Quantity" oninput="calculateItemTotal(this,null)"/>';
                    return html;
                },

                headerAttributes: {
                    style: "text-align: center;width:35px;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;width:35px;"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
            },
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    {
                        name: 'Delete',
                        text: "Delete",
                        //iconClass: "fa fa-trash-alt",
                        click: function (e) {
                            if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                return false;
                            }
                            var gridObj = $("#gridBaseRecipe").data("kendoGrid");
                            var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            gridObj.dataSource._data.remove(selectedRow);
                            CalculateGrandTotal('');
                            return true;
                        }
                    },
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe").css("display", "block");
                            $("#emptybr").css("display", "none");
                        } else {
                            $("#gridBaseRecipe").css("display", "none");
                            $("#emptybr").css("display", "block");
                        }

                    },
                        {
                            recipeID: recipeID
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },

        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },

        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();
            items.each(function (e) {
                var dataItem = grid.dataItem(this);
                var ddt = $(this).find('.brTemplate');
                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange,
                    //  autoBind: true,
                });
                //  ddt.find(".k-input").val(dataItem.BaseRecipeName);
                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe');
    var toolbar = $("#gridBaseRecipe").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.find(".k-grid-clearall").addClass("gridButton");

    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe .k-grid-content").addClass("gridInside");
    toolbar.find(".k-grid-clearall").addClass("gridButton");
    $('#gridBaseRecipe a.k-grid-clearall').click(function (e) {
         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                $("#gridBaseRecipe").data('kendoGrid').dataSource.data([]);
                // showHideGrid('gridBaseRecipe', 'emptybr', 'bup_gridBaseRecipe');
                hideGrid('emptybr', 'gridBaseRecipe');
            },
            function () {
            }
        );

    });
    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
        Utility.UnLoading();
    });
}

function refreshBaseRecipeDropDownData() {
    if (basedataList != null && basedataList.length > 0) {
        var baseGridData = $("#gridBaseRecipe").data('kendoGrid').dataSource._data;
        if (baseGridData != null && baseGridData.length > 0) {
            baseGridData = baseGridData.filter(m => m.BaseRecipeName != "");
            baserecipedata = basedataList;
            var basedataListtemp = basedataList;
            basedataListtemp = basedataListtemp.filter(m => m.BaseRecipe_Code != "");
            basedataListtemp.forEach(function (item, index) {
                for (var i = 0; i < baseGridData.length; i++) {
                    if (baseGridData[i].BaseRecipeCode !== "" && item.BaseRecipe_Code === baseGridData[i].BaseRecipeCode) {
                        baserecipedata.splice(index, 1);
                        baserecipedata = baserecipedata.filter(r => r.BaseRecipe_Code !== item.BaseRecipe_Code && r.BaseRecipe_Code != RecipeCode);
                        break;
                    }
                }
                return true;
            });
        }
    }
    baserecipedata = baserecipedata.filter(r => r.BaseRecipe_Code != RecipeCode);
    return baserecipedata;
};

function populateMOGGrid(recipeID) {

    Utility.Loading();
    var gridVariable = $("#gridMOG");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "create", text: "Add More" }],
        groupable: false,
        editable: true,
        //navigatable: true,
        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                        '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: refreshMOGDropDownData(),
                            value: options.field,
                            change: onMOGDDLChange,
                        });

                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Quantity", width: "35px", editable: false,
                headerAttributes: {
                    style: "text-align: center;width:35px;"
                },

                template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,null)"/>',
                attributes: {

                    style: "text-align: center; font-weight:normal;width:35px;"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
            },
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    {
                        name: 'Delete',
                        text: "Delete",
                        //iconClass: "fa fa-trash-alt",
                        click: function (e) {
                            if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                return false;
                            }
                            var gridObj = $("#gridMOG").data("kendoGrid");
                            var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            gridObj.dataSource._data.remove(selectedRow);
                            CalculateGrandTotal('');
                            return true;
                        }
                    },
                    {
                        name: 'View APL',
                        click: function (e) {
                            var gridObj = $("#gridMOG").data("kendoGrid");
                            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            datamodel = tr;
                            console.log("MOG APL");
                            console.log(tr);
                            MOGName = tr.MOGName;
                            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                            $("#windowEditMOGWiseAPL").kendoWindow({
                                animation: false
                            });
                            $(".k-overlay").css("display", "block");
                            $(".k-overlay").css("opacity", "0.5");
                            dialog.open();
                            dialog.center();
                            dialog.title("MOG APL Details - " + MOGName);
                            MOGCode = tr.MOGCode;
                            mogWiseAPLMasterdataSource = [];
                            getMOGWiseAPLMasterData();
                            populateAPLMasterGrid();
                            return true;
                        }
                    }
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG").css("display", "block");
                            $("#emptymog").css("display", "none");
                        } else {
                            $("#gridMOG").css("display", "none");
                            $("#emptymog").css("display", "block");
                        }
                    },
                        {
                            recipeID: recipeID
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "MOG_ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        Quantity: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();
            items.each(function (e) {
                var dataItem = grid.dataItem(this);
                var ddt = $(this).find('.mogTemplate');
                $(ddt).kendoDropDownList({
                    index: 0,
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });


        },
        change: function (e) {
            if (e.action == "itemchange") {
                e.items[0].dirtyFields = e.items[0].dirtyFields || {};
                e.items[0].dirtyFields[e.field] = true;
            }
        },
    });

    cancelChangesConfirmation('gridMOG');
    var toolbar = $("#gridMOG").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG .k-grid-content").addClass("gridInside");
    toolbar.find(".k-grid-clearall").addClass("gridButton");
    $('#gridMOG a.k-grid-clearall').click(function (e) {
         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                $("#gridMOG").data('kendoGrid').dataSource.data([]);
                showHideGrid('gridMOG', 'emptymog', 'mup_gridBaseRecipe');
            },
            function () {
            }
        );

    });
    //$('#gridMOG a.k-grid-add').click(function (e) {
    //    refreshMOGDropDownData();
    //});

}

function refreshMOGDropDownData() {
    if (mogdataList != null && mogdataList.length > 0) {
        var mogGridData = $("#gridMOG").data('kendoGrid').dataSource._data;
        if (mogGridData != null && mogGridData.length > 0) {
            mogGridData = mogGridData.filter(m => m.MOGName != "");
            mogdata = mogdataList;
            var mogdataListtemp = mogdataList;
            mogdataListtemp = mogdataListtemp.filter(m => m.MOG_Code != "");
            mogdataListtemp.forEach(function (item, index) {
                for (var i = 0; i < mogGridData.length; i++) {
                    if (mogGridData[i].MOGCode !== "" && item.MOG_Code === mogGridData[i].MOGCode) {
                        // mogdata.splice(index, 1);
                        mogdata = mogdata.filter(mog => mog.MOG_Code !== item.MOG_Code);
                        break;
                    }
                }
                return true;
            });
        }
    }
    return mogdata;
};
HttpClient.MakeRequest(CookBookMasters.GetRecipeDataList, function (data) {

    var dataSource = data;

    baserecipedata = [];
    baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
    for (var i = 0; i < dataSource.length; i++) {
        if (dataSource[i].CostPerUOM == null) {
            dataSource[i].CostPerUOM = 0;
        }
        if (dataSource[i].CostPerKG == null) {
            dataSource[i].CostPerKG = 0;
        }
        if (dataSource[i].TotalCost == null) {
            dataSource[i].TotalCost = 0;
        }
        if (dataSource[i].Quantity == null) {
            dataSource[i].Quantity = 0;
        }
        baserecipedata.push({
            "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
        });
    }
    baserecipedatafiltered = baserecipedata;
    basedataList = baserecipedata;
}, { condition: "Base" }, true);


function cancelChangesConfirmation(gridId) {
    $(".k-grid-cancel-changes").unbind("mousedown");
    $(".k-grid-cancel-changes").mousedown(function (e) {
         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                var grid = $("#" + gridId + "").data("kendoGrid");
                grid.cancelChanges();
            },
            function () {
            }
        );

    });
}

function onBRDDLChange(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    //baserecipedatafiltered = baserecipedata.filter(function (recipeObj) {
    //    return recipeObj.BaseRecipe_ID != e.sender.value();
    //});

    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    // $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};

function onMOGDDLChange(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    console.log("Data Item ")
    console.log(dataItem)

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);

    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    // var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.CostPerUOM = UnitCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    console.log(dataItem)
    //  dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('');
};

function getMOGWiseAPLMasterData() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG, function (result) {
        Utility.UnLoading();

        if (result != null) {
            mogWiseAPLMasterdataSource = result;
            console.log(result);
        }
        else {
        }
    }, { mogCode: MOGCode }
        , true);
}