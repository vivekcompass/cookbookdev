﻿var user;
var conceptype2data;
var tileImage = null;
var item = null;
var DayPartCodeIntial = null;
var datamodel;
var revenuetypedata = [];
var daypartdata = [];
function populateRevenueTypedrpodown() {
    $("#dprevenuetype").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "RevenueTypeCode",
        dataSource: revenuetypedata,
        index: 0,

        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In Active");
                var dropdownlist = $("#dprevenuetype").data("kendoDropDownList");
                dropdownlist.select("");
            }

        }
    });
}

$(function () {
    HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
        user = result;
        if (user.SectorNumber == "20") {
            $("#topHeading").text("Service Type Master");
            $("#ct").text("Service Type Details");
        }
        else {
            $("#topHeading").text("Mealtime Master");
            $("#ct").text("Mealtime Details");
        }
    }, null, false);
    $('#myInput').on('input', function (e) {
        var grid = $('#gridDayPart').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "DayPartCode" || x.field == "Name" || x.field == "SequenceNumber" ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#MealwindowEdit").kendoWindow({
            modal: true,
            width: "270px",
        
            title: "Day Part Details  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });


        HttpClient.MakeSyncRequest(CookBookMasters.GetRevenueTypeDataList, function (result) {
            dataSource = result;
            revenuetypedata.push({ "value": "Select", "text": "Select", "RevenueTypeCode": null, "IsActive": 1 });
            for (var i = 0; i < dataSource.length; i++) {
                revenuetypedata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "RevenueTypeCode": dataSource[i].RevenueTypeCode, "IsActive": dataSource[i].IsActive });
            }
            populateRevenueTypedrpodown();
            populateDayPartGrid();
        }, null, false);


    });



    function dpvalidate() {
        var valid = true;

        if ($("#dpname").val() === "") {
            toastr.error("Please provide input");
            $("#dpname").addClass("is-invalid");
            valid = false;
            $("#dpname").focus();
        }
        if (($.trim($("#dpname").val())).length > 30) {
            toastr.error("Day Part accepts upto 30 charactre");

            $("#dpname").addClass("is-invalid");
            valid = false;
            $("#dpname").focus();
        }
        return valid;
    }
    //DayPart Section Start

    function populateDayPartGrid() {

        Utility.Loading();
        var gridVariable = $("#gridDayPart");
        gridVariable.html("");
      
        if (user.SectorNumber != "20") {
            gridVariable.kendoGrid({
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                groupable: false,
                columns: [
                    {
                        field: "DayPartCode", title: "Code", width: "170px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "Name", title: "Mealtime", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "RevenueName", title: "Revenue Type", width: "250px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "SequenceNumber", title: "Sequence Number", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    //{
                    //    field: "IsActive", title: "Active", width: "110px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                    //},
                    {
                        field: "Edit", title: "Action", width: "100px",
                        attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        command: [
                            {
                                name: 'Edit',
                                click: function (e) {
                                    var gridObj = gridVariable.data("kendoGrid");
                                    //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    var tr = $(e.target).closest("tr");    // get the current table row (tr)

                                    var item = this.dataItem(tr);
                                    if (!item.IsActive) {
                                        return;
                                    }// get the date of this row
                                    var model = {
                                        "ID": item.ID,
                                        "Name": item.Name,
                                        "IsActive": item.IsActive,
                                        "DayPartCode": item.DayPartCode,
                                        "SequenceNumber": item.SequenceNumber,
                                        "RevenueTypeCode": null,
                                        "CreatedBy": item.CreatedBy,
                                        "CreatedOn": kendo.parseDate(item.CreatedOn)

                                    };
                                    DayPartCodeIntial = item.DayPartCode;
                                    var dialog = $("#MealwindowEdit").data("kendoWindow");
                                    if (user.SectorNumber == "20") {
                                        dialog.title("Service Type Details - " + model.Name);
                                        model.RevenueTypeCode = item.RevenueTypeCode;
                                        $("#dprevenuetype").data('kendoDropDownList').value(model.RevenueTypeCode);
                                        $(".mealtimeNameLabel").text("Service Type");
                                        // dialog.height = "250px";
                                    }
                                    else {
                                        dialog.title("Mealtime Details - " + model.Name);
                                        $(".mealtimeNameLabel").text("Mealtime");
                                        // var dialog = $("#MealwindowEdit").data("kendoWindow");
                                        //  dialog.height = "220px";
                                        $("#inputSequenceNumber").val(model.SequenceNumber);

                                    }


                                    dialog.open();
                                    dialog.center();

                                    datamodel = model;

                                    $("#dpid").val(model.ID);
                                    $("#dpcode").val(model.DayPartCode);
                                    $("#dpname").val(model.Name);
                                    $("#dpname").focus();


                                    return true;
                                }
                            }
                        ],
                    }
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeRequest(CookBookMasters.GetDayPartDataList, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    //Result modifiy
                                    daypartdata = result;
                                    if (user.SectorNumber == "20") {
                                        result.forEach(function (item) {
                                            if (item.RevenueTypeCode == null) {
                                                item.RevenueName = null;
                                                return item;
                                            }
                                            for (i of revenuetypedata) {
                                                if (item.RevenueTypeCode == i.RevenueTypeCode) {
                                                    item.RevenueName = i.text;
                                                    return item;
                                                }

                                            }
                                            item.RevenueName = null;
                                            return item;
                                        })
                                    }

                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , true);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                Name: { type: "string" },
                                DayPartCode: { type: "string" },
                                SequenceNumber: { type: "string" },
                            }
                        }
                    }
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var items = e.sender.items();
                    var grid = this;
                    grid.tbody.find("tr[role='row']").each(function () {
                        var model = grid.dataItem(this);

                        if (!model.IsActive) {
                            $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                        }
                    });
                   
                    $(".chkbox").on("change", function () {
                        var gridObj = $("#gridDayPart").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        var trEdit = $(this).closest("tr");
                        var th = this;
                        datamodel = tr;
                        datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);

                        datamodel.IsActive = $(this)[0].checked;
                        var active = "";
                        var msg = "";
                        if (datamodel.IsActive) {
                            active = "Active";
                        } else {
                            active = "Inactive";
                        }
                        if (user.SectorNumber == "20") {
                            msg = " Service Type ";

                        }
                        else {
                            msg = " Mealtime ";
                        }

                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b>" + msg + " " + active + "?", " " + msg + " Update Confirmation", "Yes", "No", function () {
                            HttpClient.MakeRequest(CookBookMasters.SaveDayPart, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again");
                                }
                                else {
                                    toastr.success(msg + " configuration updated successfully");
                                    $(th)[0].checked = datamodel.IsActive;
                                    if ($(th)[0].checked) {
                                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                    } else {
                                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                    }
                                }
                            }, {
                                model: datamodel
                            }, false);
                        }, function () {
                            datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);

                            $(th)[0].checked = !datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        });
                        return true;
                    });

                },
                change: function (e) {
                },
            })
            //gridVariable.data("kendoGrid").dataSource.sort({ field: "SequenceNumber", dir: "asc" });
            gridVariable.data("kendoGrid").hideColumn("RevenueName");
            $("#hiderow").hide();
            $("#hiderowmfg").show();
        } else {
            gridVariable.kendoGrid({
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                groupable: false,
                columns: [
                    {
                        field: "DayPartCode", title: "Code", width: "170px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "Name", title: "Service Type", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "RevenueName", title: "Revenue Type", width: "250px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    //{
                    //    field: "SequenceNumber", title: "Sequence Number", attributes: {
                    //        style: "text-align: left; font-weight:normal"
                    //    }
                    //},
                    //{
                    //    field: "IsActive", title: "Active", width: "110px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                    //},
                    {
                        field: "Edit", title: "Action", width: "100px",
                        attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        command: [
                            {
                                name: 'Edit',
                                click: function (e) {
                                    var gridObj = gridVariable.data("kendoGrid");
                                    //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    var tr = $(e.target).closest("tr");    // get the current table row (tr)

                                    var item = this.dataItem(tr);
                                    if (!item.IsActive) {
                                        return;
                                    }// get the date of this row
                                    var model = {
                                        "ID": item.ID,
                                        "Name": item.Name,
                                        "IsActive": item.IsActive,
                                        "DayPartCode": item.DayPartCode,
                                        "RevenueTypeCode": null,
                                        "CreatedBy": item.CreatedBy,
                                        "CreatedOn": kendo.parseDate(item.CreatedOn)

                                    };
                                    DayPartCodeIntial = item.DayPartCode;
                                    var dialog = $("#MealwindowEdit").data("kendoWindow");
                                    if (user.SectorNumber == "20") {
                                        dialog.title("Service Type Details - " + model.Name);
                                        model.RevenueTypeCode = item.RevenueTypeCode;
                                        $("#dprevenuetype").data('kendoDropDownList').value(model.RevenueTypeCode);
                                        $(".mealtimeNameLabel").text("Service Type");
                                        // dialog.height = "250px";
                                    }
                                    else {
                                        dialog.title("Mealtime Details - " + model.Name);
                                        $(".mealtimeNameLabel").text("Mealtime");
                                        // var dialog = $("#MealwindowEdit").data("kendoWindow");
                                        //  dialog.height = "220px";
                                    }


                                    dialog.open();
                                    dialog.center();

                                    datamodel = model;

                                    $("#dpid").val(model.ID);
                                    $("#dpcode").val(model.DayPartCode);
                                    $("#dpname").val(model.Name);
                                    $("#dpname").focus();


                                    return true;
                                }
                            }
                        ],
                    }
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeRequest(CookBookMasters.GetDayPartDataList, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    //Result modifiy
                                    daypartdata = result;
                                    if (user.SectorNumber == "20") {
                                        result.forEach(function (item) {
                                            if (item.RevenueTypeCode == null) {
                                                item.RevenueName = null;
                                                return item;
                                            }
                                            for (i of revenuetypedata) {
                                                if (item.RevenueTypeCode == i.RevenueTypeCode) {
                                                    item.RevenueName = i.text;
                                                    return item;
                                                }

                                            }
                                            item.RevenueName = null;
                                            return item;
                                        })
                                    }

                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , true);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                Name: { type: "string" },
                                DayPartCode: { type: "string" },
                                SequenceNumber: { type: "string" },
                            }
                        }
                    }
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var items = e.sender.items();
                    var grid = this;
                    grid.tbody.find("tr[role='row']").each(function () {
                        var model = grid.dataItem(this);

                        if (!model.IsActive) {
                            $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                        }
                    });
                    
                    $(".chkbox").on("change", function () {
                        var gridObj = $("#gridDayPart").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        var trEdit = $(this).closest("tr");
                        var th = this;
                        datamodel = tr;
                        datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);

                        datamodel.IsActive = $(this)[0].checked;
                        var active = "";
                        var msg = "";
                        if (datamodel.IsActive) {
                            active = "Active";
                        } else {
                            active = "Inactive";
                        }
                        if (user.SectorNumber == "20") {
                            msg = " Service Type ";

                        }
                        else {
                            msg = " Mealtime ";
                        }

                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b>" + msg + " " + active + "?", " " + msg + " Update Confirmation", "Yes", "No", function () {
                            HttpClient.MakeRequest(CookBookMasters.SaveDayPart, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again");
                                }
                                else {
                                    toastr.success(msg + " configuration updated successfully");
                                    $(th)[0].checked = datamodel.IsActive;
                                    if ($(th)[0].checked) {
                                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                    } else {
                                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                    }
                                }
                            }, {
                                model: datamodel
                            }, false);
                        }, function () {
                            datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);

                            $(th)[0].checked = !datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        });
                        return true;
                    });

                },
                change: function (e) {
                },
            })
           // gridVariable.data("kendoGrid").dataSource.sort({ field: "SequenceNumber", dir: "asc" });
            $("#hiderow").show();
            $("#hiderowmfg").hide();
        }
    }


    $("#dpcancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#MealwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#dpaddnew").on("click", function () {
        var model;
        var dialog = $("#MealwindowEdit").data("kendoWindow");

        dialog.open();
        dialog.center();

        datamodel = model;
        if (user.SectorNumber == "20") {
            dialog.title("New Service Type Creation");
            $(".mealtimeNameLabel").text("Service Type");
            $("#hiderowmfg").hide();
        }
          
        else {
            dialog.title("New Mealtime Creation");
            $(".mealtimeNameLabel").text("Mealtime");
            $("#inputSequenceNumber").val("");
            $("#hiderowmfg").show();
        }

        $("#dpname").removeClass("is-invalid");
        $('#dpsubmit').removeAttr("disabled");
        $("#dpid").val("");
        $("#dpname").val("");
        $("#dpcode").val("");
      

        
        $("#dpname").focus();
        if (user.SectorNumber == "20") {
            $("#dprevenuetype").data('kendoDropDownList').value(null)
        } else {
            $("#dprevenuetype").hide();
        }
        DayPartCodeIntial = null;
    });

    $("#dpsubmit").click(function () {
        var msg = "";
        if (user.SectorNumber == "20") {
            msg = "Service Type";
        }
        else {
            msg = "Mealtime";
        }
        
        if ($("#dpname").val() === "" || $("#dpname").val() == null) {
            toastr.error("Please provide "+msg+ " Name");
            $("#dpname").focus();
            return;
        }
        if (user.SectorNumber == "20") {
            if ($("#dprevenuetype").val() == "" || $("#dprevenuetype").val() == null) {
                toastr.error("Please provide " + msg + " Revenue Type");
                $("#dprevenuetype").focus();
                return;
            }
        }
        else {
            if ($("#inputSequenceNumber").val() === "" || $("#inputSequenceNumber").val() == null) {
                toastr.error("Please provide " + msg + " Sequence Number");
                $("#inputSequenceNumber").focus();
                return;
            }
            var dorderdp = $("#inputSequenceNumber").val().toLowerCase().trim();
            if (DayPartCodeIntial == null) { //Duplicate Check
                for (item of daypartdata) {
                    if (item.SequenceNumber != null && item.SequenceNumber.toLowerCase().trim() == dorderdp) {
                        toastr.error("Kindly check Duplicate " + msg + " Sequence Number");
                        $("#inputSequenceNumber").focus();
                        return;
                    }
                }
            } else {
                for (item of daypartdata) {
                    if (item.SequenceNumber != null && item.SequenceNumber.toLowerCase().trim() == dorderdp && item.DayPartCode != DayPartCodeIntial) {
                        toastr.error("Kindly check Duplicate  " + msg + " Sequence Number is entered on editing");
                        $("#inputSequenceNumber").focus();
                        return;
                    }
                }
            }
        }
        
        var namedp = $("#dpname").val().toLowerCase().trim();
        if (DayPartCodeIntial == null) { //Duplicate Check
            for (item of daypartdata) {
                if (item.Name.toLowerCase().trim() == namedp) {
                    toastr.error("Kindly check Duplicate " + msg + " Name");
                    $("#dpname").focus();
                    return;
                }
            }
        } else {
            for (item of daypartdata) {
                if (item.Name.toLowerCase().trim() == namedp && item.DayPartCode != DayPartCodeIntial) {
                    toastr.error("Kindly check Duplicate  " + msg + " Name is entered on editing");
                    $("#dpname").focus();
                    return;
                }
            }
        }

       
        if (dpvalidate() === true) {
            $("#dpname").removeClass("is-invalid");

            var model = {
                "ID": $("#dpid").val(),
                "Name": $("#dpname").val(),
                "DayPartCode": DayPartCodeIntial,
                "RevenueTypeCode": null,
                "SequenceNumber": $("#inputSequenceNumber").val()
            }
            if (DayPartCodeIntial == null) {
                model.IsActive = 1;
            } else {
                model.IsActive = 1;
                    model.CreatedBy = datamodel.CreatedBy;
                    model.CreatedOn = datamodel.CreatedOn;
               
            }
            if (user.SectorNumber == "20") {
                model.RevenueTypeCode = $("#dprevenuetype").data('kendoDropDownList').value()
            }
            else {
                model.SequenceNumber = dorderdp;
            }
            if (!sanitizeAndSend(model)) {
                return;
            }

            $("#dpsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveDayPart, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#dpsubmit').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#dpsubmit').removeAttr("disabled");
                        toastr.success("There was some error, the record cannot be saved");

                    }
                    else {
                        $(".k-overlay").hide();
                        $('#dpsubmit').removeAttr("disabled");

                        if (model.ID > 0)
                            toastr.success(msg + " configuration updated successfully");
                        else
                            toastr.success("New " + msg + " added successfully");
                        $(".k-overlay").hide();
                        var orderWindow = $("#MealwindowEdit").data("kendoWindow");
                        orderWindow.close();
                        $("#gridDayPart").data("kendoGrid").dataSource.data([]);
                        $("#gridDayPart").data("kendoGrid").dataSource.read();
                        // $("#gridDayPart").data("kendoGrid").dataSource.sort({ field: "SequenceNumber", dir: "asc" });

                    }
                }
            }, {
                model: model

            }, true);
        }
    });



});