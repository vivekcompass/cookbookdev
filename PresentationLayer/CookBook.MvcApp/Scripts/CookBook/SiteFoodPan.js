﻿
var sitemasterdata = [];
var regionmasterdata = [];
var user;
var status = "";
var varname = "";
var datamodel;
var DishName = "";
var foodPanTypedata = [];
var dietcategorydata = [];
var dishtypedata = [];
var uomdata = [];
var servingtemperaturedata = [];
var recipedata = [];
var recipeCustomData = [];
var mappedData = [];
var mappedRecipeCode = [];
var DishCode;
var Dish_ID;
var multiarray = [];
var checkedRecipeCode = '';
var siteCode = '';
var saveModel = [];
var siteDishData = [];
var siteDishCategoryData = [];
var dataItems = [];
var dcatFinal = [];
var dshFinal = [];
var masterGroupList = [];
function removeDuplicateObjectFromArray(array, key) {
    let check = {};
    let res = [];
    for (let i = 0; i < array.length; i++) {
        if (!check[array[i][key]]) {
            check[array[i][key]] = true;
            res.push(array[i]);
        }
    }
    return res;
}



HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {
    var dataSource = data;
    uomdata = [];
    uomdata.push({ "value": null, "text": "Select" });
    for (var i = 0; i < dataSource.length; i++) {
        uomdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].Name });
    }
}, null, false);
HttpClient.MakeRequest(CookBookMasters.GetSiteMasterList, function (data) {
    console.log("Site Master Data");
    console.log(data);
    var dataSource = data;
    sitemasterdata = dataSource;
    sitemasterdata.unshift({ "SiteCode": 0, "SiteName": "Select Site" });
    populateSiteMasterDropdown();
}, null, false);
HttpClient.MakeRequest(CookBookMasters.GetDishCategoryList, function (data) {

    var dataSource = data;
    dishcategorydata = [];

    for (var i = 0; i < dataSource.length; i++) {
        dishcategorydata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "code": dataSource[i].DishCategoryCode });
    }


}, null, false);


HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
    user = result;
    //if (user.UserRoleId === 1) {
    //    $("#InitiateBulkChanges").css("display", "inline");
    //    $("#AddNew").css("display", "inline");
    //}
    //if (user.UserRoleId === 2) {
    //    $("#InitiateBulkChanges").css("display", "none");
    //    $("#AddNew").css("display", "none");
    //}


}, null, false);

HttpClient.MakeRequest(CookBookMasters.GetFoodPanDataList, function (data) {

    var dataSource = data;
    foodPanTypedata = [];
    foodPanTypedata.push({ "value": null, "text": "Select Food Pan Type" });
    for (var i = 0; i < dataSource.length; i++) {
        foodPanTypedata.push({ "value": dataSource[i].FoodPanCode, "text": dataSource[i].Name, "FoodPanTypeID": dataSource[i].FoodPanCode });
    }
   
  
}, null, false);



function populateSiteMasterDropdown() {
    $("#ddlSiteMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "SiteName",
        dataValueField: "SiteCode",
        dataSource: sitemasterdata,
        index: 0,
    });
    $("#inputsite").css("display", "block")
    $("#btnGo").css("display", "inline-block")
}

function GetSiteDishdata(siteCode) {

    HttpClient.MakeSyncRequest(CookBookMasters.GetDishDataListForFood, function (result) {
        Utility.UnLoading();

        if (result != null) {
           
            siteDishData = result;

        }
        else {
            
        }
    },
        {
            siteCode: siteCode
        }
        , false);
}

function GetSiteDishCategorydata(siteCode) {

    HttpClient.MakeSyncRequest(CookBookMasters.GetDishCategoryDataListForFood, function (result) {
        Utility.UnLoading();

        if (result != null) {
           
            siteDishCategoryData = result;

        }
        else {
           
        }
    },
        {
            siteCode: siteCode
        }
        , false);
}

function populateUOMDropdown(uomid, uomvalue) {

    $(uomid).kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
    var dropdownlist = $(uomid).data("kendoDropDownList");
    if (uomvalue == null)
        dropdownlist.select(1);//value("Select");
    else
        dropdownlist.value(uomvalue);
  
    dropdownlist.bind("change", uomdropdownlist_select);
}
function uomdropdownlist_select(e) {

    var ID = e.sender.dataItem(e.item).value;
    if (ID == 0) {
        return;
    }

    var idnum = e.sender.element.context.id.substring(3);
    var dishFlag = e.sender.element.context.id.search("small");
    if (dishFlag > 0) {
        
       
        return
    }
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {

        var sm = subItem[i].id;

        $("#uom" + sm).data('kendoDropDownList').value(ID);

      

    }
    

}

//Ended
$(document).ready(function () {

    $("#btnGo").on("click", function () {
        $("#dishCategory").empty();
        var dropdownlist = $("#ddlSiteMaster").data("kendoDropDownList");
        siteCode = dropdownlist.value();
        $("#mapMaster").show();
        $("#dishCategory").empty();
        GetSiteDishCategorydata(siteCode);
        GetSiteDishdata(siteCode);
     
        CreateDishCategoryOutline();
        CreateDishTiles();
        $("#btnSubmit").show();
        $("#btnPublish").show();
        $("#btnBack").show();
    });

    $("#collapseIcon").click(function () {
        collapseAll();
    });

    $("#expandIcon").click(function () {
        expandAll();
    });
});
function expandAll() {
    var rows = $(".upper");
    $.each(rows, function () {
        var upperid = (this).id;
        var lowerid = (this).nextElementSibling.id;
        $("#" + upperid).find(".hideG").html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        $("#" + lowerid).show();
        $("#" + upperid).find(".k-widget").show();
        $("#" + upperid).find(".displayG").hide()
        $("#" + upperid).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" })
        $("#expandIcon").css({ "color": "#3f7990" });
        $("#collapseIcon").css({ "color": "#76b4b0" });
    });
}

function collapseAll() {
    var rows = $(".upper");
    rows.each(function () {
        var upperid = (this).id;
        var lowerid = (this).nextElementSibling.id;
        $("#" + upperid).find(".hideG").html(" Show <i class= 'fas fa-angle-down' ></i>");
        var count = $("#" + lowerid).find(".smallIngredient").length;
        $("#" + lowerid).hide();
        $("#" + upperid).find(".displayG").show().html(" " + count + " Dish(s) Added");
        $("#" + upperid).find(".k-widget:last-child").hide();
        $("#" + upperid).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
        $("#collapseIcon").css({ "color": "#3f7990" });
        $("#expandIcon").css({ "color": "#76b4b0" });
    });
}
 
function populateFoodPanTypeDropdown(foodpanid,value) {
    $(foodpanid).kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: foodPanTypedata,
        index: 0,
    });
    var dropdownlist = $(foodpanid).data("kendoDropDownList");
    if (value == null)
        dropdownlist.select(0);//value("Select");
    else
        dropdownlist.value(value);

    dropdownlist.bind("change", foodPanType_select);
}

function foodPanType_select(e) {

    var ID = e.sender.dataItem(e.item).value;
    if (ID == 0) {
        return;
    }

    var idnum = e.sender.element.context.id.substring(3);
    var dishFlag = e.sender.element.context.id.search("small");
    if (dishFlag > 0) {


        return
    }
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {

        var sm = subItem[i].id;

        $("#food" + sm).data('kendoDropDownList').value(ID);



    }


}
function closeBelow(dsh) {
    //alert(dsh);
    var lowerid = "subinputdish"+dsh
    var upperid ="dish"+dsh;
if ($("#" + lowerid).is(":visible")) {
    
            $("#" + upperid).find(".hideG").html(" Show <i class= 'fas fa-angle-down' ></i>");
            var count = $("#" + lowerid).find(".smallIngredient").length;
            $("#" + lowerid).hide();
            $("#" + upperid).find(".displayG").show().html(" " + count + " Dish(s) Added");
            $("#" + upperid).find(".k-widget:last-child").hide();
            $("#" + upperid).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
            
    } else {
    $("#" + upperid).find(".hideG").html(" Hide <i class= 'fas fa-angle-up' ></i> ");
    $("#" + lowerid).show();
    $("#" + upperid).find(".k-widget").show();
    $("#" + upperid).find(".displayG").hide()
    $("#" + upperid).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" })
    }
}
function CreateDishCategoryOutline() {

    var dataItems1 = siteDishCategoryData;
    dataItems = [];
        for (xi of dataItems1) {
            dishcategorydata.forEach(function (yi) {
                if (yi.code == xi.DishCategoryCode) {
                    dataItems.push({ "ID": xi.ID, "value": xi.DishCategoryCode, "text": yi.text, "stdqty": xi.Quantity, "ucode": xi.UOMCode, "foodPanCode": xi.FoodPanCode })
                }
            });
        }

        for (item of dataItems) {
            var divid = "dish" + item.value;
            var dishesid = "inputdish" + item.value;
            var inputqty = "inputqty" + item.value;
            var subdishid = "subinputdish" + item.value;
            var uomid = "uom" + item.value;
            var foodpanid = "foo" + item.value;
            var stdqty = item.stdqty;
            $('#dishCategory').append('<div class="dish" ><div class="upper"  id=' + divid + '><div class="upperText">'
                + item.text + '</div>'
                + '<div style="margin-right:10px;" id=' + foodpanid + '></div>'
                + '<input id = "' + inputqty +
                '"  class="qty"  type=" " value='+stdqty+' min="0" onchange="updateQuantity(&quot;' + inputqty + '&quot;)" /> <div id = "' + uomid + '" class="colorG"></div ><span class="serv"></span> <span class="hideG" onclick="closeBelow(&quot;'+item.value+'&quot;)"> Hide <i class="fas fa-angle-up" ></i ></span > <span class="displayG"></span><span class="dllrgndish" style="float:right;margin-top:2px" id='
                + dishesid + '></span></div ><div  class="lower" id='
                + subdishid + ' > </div ></div> ');

       
            populateUOMDropdown('#' + uomid, item.ucode);
            populateFoodPanTypeDropdown("#"+foodpanid,item.foodPancode)
        
        }
  
}
function CreateDishTiles() {
        // var dataSource = data;
    var finalModelExits = [];
    masterGroupList = [];
        var dataItems1 = siteDishData;
    for (xi of dataItems1) {
        dataItems.forEach(function (yi) {
                if (yi.text == xi.DishCategory) {
                    finalModelExits.push({"ID":xi.ID, "DishCode": xi.DishCode,"DishCategoryCode": yi.value, "text": xi.DishName, "Quantity": xi.Capacity, "ucode": xi.UOMCode, "foodPanCode": xi.FoodPanCode })
                }
            });
        }

       masterGroupList = finalModelExits;
        RecreateWholeExits();
    
}

function RecreateWholeExits() {
    for (item1 of masterGroupList) {
        
        var dshcat = item1.DishCode;//"DTC-00001";
        var dcat = item1.DishCategoryCode
        var text = item1.text;
        var uomcode = item1.ucode;
        var fpCode = item1.foodPanCode;
        var qty = item1.Quantity;
        if (qty == null)
            qty = 0;
        var sid = "small" + dshcat;
        var subid = "#subinputdish" + dcat;
        var fid = "foodsmall" + dshcat;
        var uomid = "uomsmall" + dshcat;
        
            $(subid)
                .prepend(
                    '<div class="smallIngredient" id=' + sid + ' title="' + text + '">' +
                    '     <div class="supper">' + text + '</div>' +
                    '     <div class="slower" id="' + fid + '"></div>'+
                    '     <div class="dishtile">' +
                    '          <input id="inputsubqty' + sid + '" class="qtyS" type="" value="' + qty + '" min="0"  />' +
                    '          <span id="' + uomid + '" class="colorGS"></span>' +
                    '     </div>' +
                    '</div>');
        

        populateFoodPanTypeDropdown("#" + fid, fpCode)
      
        populateUOMDropdown('#' + uomid, uomcode);
        
    }

   
}

function updateQuantity(iqty) {
    var qty = $("#" + iqty).val();

    var idnum = iqty.substring(8);
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {
        var sm = subItem[i].id;
        $("#inputsubqty" + sm).val(qty);
    }

}

function onSave() {
    //cat save qunat uom foo
    dcatFinal = [];
     dshFinal = [];
    for (it of dataItems) {
        var dc = it.value;
        console.log(dc);
        var foodPanType = $("#foo" + dc).data('kendoDropDownList').value();

       var model = {
            "ID": it.ID,
            "SiteCode": siteCode,
            "DishCategoryCode": dc,
            "FoodPanCode": foodPanType,
            "Quantity": $("#inputqty"+dc).val(),
            "UOMCode": $("#uom" + dc).data('kendoDropDownList').value(),
            "IsActive": true,
        }
        dcatFinal.push(model);
    }
  
    //dish change save qunt uom foo
    for (dish of masterGroupList) {
        var dshcode = dish.DishCode;
        console.log(dshcode);
        var foodPanType = $("#foodsmall" + dshcode).data('kendoDropDownList').value()
       var modelDsh = {
            "ID":dish.ID,
            "SiteCode": siteCode,
            "DishCode": dish.DishCode,
           "FoodPanCode": foodPanType,
           "Capacity": $("#inputsubqtysmall" + dshcode).val(),
           "UOMCode": $("#uomsmall" + dshcode).data('kendoDropDownList').value(),
           "CapacityUOMCode": $("#uomsmall" + dshcode).data('kendoDropDownList').value(),
            "IsActive": true,
        }
        dshFinal.push(modelDsh);
    }
    HttpClient.MakeRequest(CookBookMasters.SaveFoodPanCategoryDataList, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again.");
        }
        else {
           
           // toastr.success("Records have been updated successfully");

        }
    }, {
            model: dcatFinal

    }, false);
    HttpClient.MakeRequest(CookBookMasters.SaveSiteDishFoodPanDataList, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again.");
        }
        else {
           
            toastr.success("Records have been updated successfully");
           
        }
    }, {
            model: dshFinal

    }, false);

}

function onPublish() {
    onSave();
}