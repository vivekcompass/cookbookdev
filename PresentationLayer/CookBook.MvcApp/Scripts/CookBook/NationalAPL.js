﻿var datamodel;
var aplMasterDataSource = [];
var filtteredAplMasterDataSource = [];
var totalrecord = "";
var pageindex = 1;
$(function () {
    var user;
    var status = "";
    var varname = "";
    var ArticleName = "";
    var mogdata = [];
    var dkdata = [];
    var isMappedAPL = false;

    $("#APLwindowEdit").kendoWindow({
        modal: true,
        width: "720px",
        height: "240px",
        title: "APL Details - " + ArticleName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#UploadExcelFileAPL").kendoWindow({
        modal: true,
        width: "575px",
        height: "105px",
        title: "MOG EXCEL",
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#GETALLMogViewALLHistoryBATCHWISEAPL").kendoWindow({
        modal: true,
        width: "975px",
        height: "105px",
        title: "MOG EXCEL",
        actions: ["Close"],
        visible: false,
        animation: false

    });
    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        Utility.Loading();
        setTimeout(function () {
            onLoad();
            Utility.UnLoading();
        }, 2000);
    });
    function onLoad() {
        $('#BatchmyInputAPL').on('input', function (e) {
            var grid = $('#gridMogViewALLHistoryAPL').data('kendoGrid');
            var columns = grid.columns;

            var filter = { logic: 'or', filters: [] };
            columns.forEach(function (x) {
                if (x.field) {
                    if (x.field == "BATCHNUMBER" || x.field == "SectorName" || x.field == "FLAG" || x.field == "CreationTime" || x.field == "TOTALRECORDS"
                        || x.field == "FAILED" || x.field == "CreationTime" || x.field == "UPLOADBY") {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;

                        if (type == 'string') {
                            var targetValue = e.target.value;
                            filter.filters.push({
                                field: x.field,
                                operator: 'contains',
                                value: targetValue
                            })
                        }
                        else if (type == 'number') {

                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        } else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format(x.format, data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                }
            });
            grid.dataSource.filter(filter);
        });
    }
    $("#APLMOGbtnbackAPL").click(function () {
        //alert("call");
        $("#SiteMaster").show();
        $("#mogviewhistorymainAPL").hide();
    });
    $("#BtnviewuploadhistoryAPL").click(function (e) {

        populateCafeGrid();
        populateCafeGridDetails();

        $("#SiteMaster").hide();
        $("#mogviewhistorymainAPL").show();
        $("#gridMogViewALLHistoryAPL").hide();



        $("#gridMogViewCustomHistoryAPL").show();
        $("#gridMogViewCustomHistoryDetailsAPL").show();
        $("#ALLBATCHVIEWAPL").show();
        $("#batchsearchAPL").hide();
        $("#GETALLMogViewALLHistoryBATCHWISEAPL").hide();

    });
    //$("#BtnExportTempAPL").click(function (e) {
    //    window.location.href = 'APLMaster/DownloadExcTemplate';
    //});

    $("#btnExport").click(function (e) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: CookBookMasters.DownloadExcTemplateAPL,
            beforeSend: function () {
                Utility.Loading();
            },
            success: function (response) {
                var blob = new Blob([response], { type: 'application/ms-excel' });
                var downloadUrl = URL.createObjectURL(blob);
                var a = document.createElement("a");
                a.href = downloadUrl;
                a.download = "NationalAPlMaster.xlsx";
                document.body.appendChild(a);
                a.click();
            },
            complete: function () {
                Utility.UnLoading();
            }
        });
        return false;
    });
    //$("#btnExport").click(function (e) {



    $("#ExcelUploadAPL").on("click", function () {

        if ($("#fileAPL").val() != "") {
            $("#fileAPL").val('');
        }
        var dialogs = $("#UploadExcelFileAPL").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialogs.open().element.closest(".k-window").css({
            top: 167,
        });
        dialogs.center();

        dialogs.title("Upload Excel");
    })
    $("#UploadMOGExcelAPL").click(function (e) {

        if ($("#fileAPL").val() != "") {
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
            /*Checks whether the file is a valid excel file*/
            if (!regex.test($("#fileAPL").val().toLowerCase())) {
                toastr.error("Please upload a valid Excel file!");
                return false;
            }
            else {
                UploadSelectedExcelsheet();

            }
        }
        else {
            toastr.error("Please upload a Excel file!");
            return false;
        }
    });
    $("#ALLBATCHVIEWAPL").click(function (e) {
        ViewALLHistoryBatch();
        $("#gridMogViewALLHistoryAPL").show();
        $("#gridMogViewCustomHistoryAPL").hide();
        $("#gridMogViewCustomHistoryDetailsAPL").hide();

        $("#ALLBATCHVIEWAPL").hide();
        $("#batchsearchAPL").show();
        $("#GETALLMogViewALLHistoryBATCHWISEAPL").show();
    });
    function populateCafeGrid() {

        Utility.Loading();
        var gridVariable = $("#gridMogViewCustomHistoryAPL").height(180);
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "MogAPLHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            height: "180px",
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "MOGCode", title: "MOG Code", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ArticalNumber", title: "Article Number", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "BATCHSTATUS", title: "Error Message", width: "100px", attributes: {
                        "class": "FLAGCELL",
                        style: "text-align: left; font-weight:normal"
                    },
                }

            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGAPLHISTORYAPL, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            MOGCode: { type: "string" },
                            ArticalNumber: { type: "string" },
                            BATCHSTATUS: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.BATCHSTATUS == "Uploaded") {

                        $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //alert("call");
                    }
                    else {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolorred");

                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function populateCafeGridDetails() {

        Utility.Loading();
        var gridVariable = $("#gridMogViewCustomHistoryDetailsAPL");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "MogAPLHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FLAG", title: "Batch Status", width: "80px", attributes: {
                        "class": "FLAGCELL",
                        style: "text-align: left; font-weight:normal"
                    }, template: '<div class="colortag"></div>',
                },
                {
                    field: "TOTALRECORDS", title: "Total Records", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "FAILED", title: "Failed", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },


            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGAPLHISTORYDetailsAPL, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            FLAG: { type: "string" },
                            TOTALRECORDS: { type: "string" },
                            FAILED: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    //if (model.FLAG != "Uploaded") {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    //    //alert("call");
                    //}
                    //else {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                    //}
                    if (model.FLAG == "1") {

                        $(this).find(".colortag").css("background-color", "green");

                    }
                    else if (model.FLAG == "2") {
                        $(this).find(".colortag").css("background-color", "orange");

                    }
                    else {

                        $(this).find(".colortag").css("background-color", "red");
                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }


    function UploadSelectedExcelsheet() {

        var data = new FormData();
        var i = 0;
        var fl = $("#fileAPL").get(0).files[0];

        if (fl != undefined) {

            data.append("file", fl);

        }
        //HttpClient.MakeSyncRequest(CookBookMasters.UploadExcelsheet, function () { },
        //    {
        //        data: data
        //    } ,
        // null, true);
        Utility.Loading();
        $.ajax({
            type: "POST",
            url: CookBookMasters.UploadExcelsheet,
            contentType: false,
            processData: false,
            async: false,
            data: data,
            success: function (result) {
                if (result == 'MOG Number is null') {
                    toastr.error("Can not be MOG Number is Null");
                    $(".k-window").hide();
                    $(".k-overlay").hide();


                    //return true;
                    Utility.UnLoading();
                }
                else if (result == 'Success') {
                    toastr.success("Upload successfully");
                    $(".k-window").hide();
                    $(".k-overlay").hide();

                    // return true;
                    Utility.UnLoading();
                }
                else if (result == 'MOG Number does not exits in APL Master') {
                    toastr.error("MOG Number does not exits in APL Master");
                    $(".k-window").hide();
                    $(".k-overlay").hide();

                    // return true;
                    Utility.UnLoading();
                }
                else {
                    toastr.error("Something want Wrong.");
                    $(".k-window").hide();
                    $(".k-overlay").hide();

                    // return true;
                    Utility.UnLoading();
                }
                result = "";
                return false;
            },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3 + " " + p4;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).Message;
                alert(err);
                Utility.UnLoading();
                return false;
            }
        });
    }
    $(document).ready(function () {
        var content = "";
        $("#gridMogViewALLHistoryAPL").kendoTooltip({
            filter: "td:nth-child(2), th:nth-child(2)",
            position: "center",


            content: function (e) {

                if (e.target.is("th")) {

                    return e.target.text();
                }

                var dataItem = $("#gridMogViewALLHistoryAPL").data("kendoGrid").dataItem(e.target.closest("tr"));
                console.log(dataItem);
                if (dataItem.FLAG == "1") {
                    content = "Success";
                }
                else if (dataItem.FLAG == "2") {
                    content = "Partially Success";
                }
                else {
                    content = "Failed";
                }
                return content;
            }
        }).data("kendoTooltip");
        $("#gridMogViewCustomHistoryDetailsAPL").kendoTooltip({
            filter: "td:nth-child(2), th:nth-child(2)",
            position: "center",


            content: function (e) {

                if (e.target.is("th")) {

                    return e.target.text();
                }

                var dataItem = $("#gridMogViewCustomHistoryDetailsAPL").data("kendoGrid").dataItem(e.target.closest("tr"));
                console.log(dataItem);
                if (dataItem.FLAG == "1") {
                    content = "Success";
                }
                else if (dataItem.FLAG == "2") {
                    content = "Partially Success";
                }
                else {
                    content = "Failed";
                }
                return content;
            }
        }).data("kendoTooltip");
    });


    function ViewALLHistoryBatch() {

        Utility.Loading();
        var gridVariable = $("#gridMogViewALLHistoryAPL");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "MogAPLHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FLAG", title: "Batch Status", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal",

                    }, template: '<div class="colortag"></div>',
                },
                {
                    field: "TOTALRECORDS", title: "Total Records", width: "80px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "CreationTime", title: "Batch Upload Time", width: "80px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "FAILED", title: "Failed", width: "80px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "UPLOADBY", title: "Upload By", width: "80px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },


                {
                    field: "View", title: "View Details", width: "50px",

                    attributes: {
                        style: "text-align: center; font-weight:normal;color:red!important",
                        "class": "ViewBatchcell"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'View',
                            click: function (e) {
                                debugger;
                                var MDgridObj = $("#gridMogViewALLHistoryAPL").data("kendoGrid");
                                var tr = MDgridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                BATCHNUMBER = tr.BATCHNUMBER;
                                console.log(e.currentTarget);

                                var trEdit = $(this).closest("tr");
                                $(trEdit).find(".k-grid-View").addClass("k-state-disabled");


                                var dialogs = $("#GETALLMogViewALLHistoryBATCHWISEAPL").data("kendoWindow");
                                populateGetallBatchwishDetails(BATCHNUMBER);
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialogs.open().element.closest(".k-window").css({
                                    left: 305,
                                    top: 215
                                });
                                // dialogs.center();


                                dialogs.title("Batch Details");

                                return false;

                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetALLHISTORYDetailsAPL, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            FLAG: { type: "string" },
                            TOTALRECORDS: { type: "string" },
                            FAILED: { type: "string" },
                            CreationTime: { type: "string" },
                            UPLOADBY: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);
                    $(this).find(".k-grid-View").addClass("ViewBatch");
                    //if (model.FLAG != "Uploaded") {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    //    //alert("call");
                    //}
                    //else {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                    //}
                    if (model.FLAG == "1") {

                        $(this).find(".colortag").css("background-color", "green");


                    }
                    else if (model.FLAG == "2") {
                        $(this).find(".colortag").css("background-color", "orange");

                    }
                    else {

                        $(this).find(".colortag").css("background-color", "red");
                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function populateGetallBatchwishDetails(BATCHNUMBER) {

        Utility.Loading();
        var gridVariable = $("#GETALLMogViewALLHistoryBATCHWISEAPL").height(180);
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "MogAPLHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            scrollable: true,
            // height: "20px",
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "MOGCode", title: "MOG Code", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ArticalNumber", title: "Article Number", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "FLAG", title: "Record Status", width: "100px", attributes: {
                        "class": "FLAGCELL",
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "CreationTime", title: "Upload Time", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                }



            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetshowGetALLHISTORYBATCHWISEAPL, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, {
                            BATCHNUMBER: BATCHNUMBER
                        }
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            MOGCode: { type: "string" },
                            ArticalNumber: { type: "string" },
                            FLAG: { type: "string" },
                            CreationTime: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.FLAG == "Success") {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //alert("call");
                    }
                    else if (model.FLAG == "Uploaded" || model.FLAG == "Uploaded") {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //alert("call");
                    }
                    else {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }
    //$(document).ready(function () {
    //    $("#myInput").keyup(function () {

    //        var value = $("#myInput").val();
    //        grid = $("#gridAPLMaster").data("kendoGrid");

    //        if (value) {
    //            grid.dataSource.filter({ field: "SectorName", operator: "contains", value: value });
    //        } else {
    //            grid.dataSource.filter({});
    //        }
    //    });
    //});
    //$(document).ready(function () {
    //    //change event
    //    $("#myInput").keyup(function () {
    //        var val = $('#myInput').val();
    //        $("#gridAPLMaster").data("kendoGrid").dataSource.filter({
    //            logic: "or",
    //            filters: [
    //                {
    //                    field: "SectorName",
    //                    operator: "contains",
    //                    value: val
    //                },


    //            ]
    //        });


    //    });
    //});

    $('#myInput').on('input', function (e) {
        
        var grid = $('#gridAPLMaster').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "SectorName" || x.field == "SiteName" || x.field == "SiteCode" || x.field == "ArticleNumber" || x.field == "ArticleDescription" || x.field == "UOM" || x.field == "ArticleType"
                    || x.field == "Hierlevel3" || x.field == "MerchandizeCategoryDesc" || x.field == "MOGName" || x.field == "ArticleCost" || x.field == "StandardCostPerKg") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    var user;
    $(document).ready(function () {



        $(".k-window").hide();
        $(".k-overlay").hide();
        //$("#btnExport").click(function (e) {
        //    setTimeout(Utility.Loading(), 1000)
        //    var grid = $("#gridAPLMaster").data("kendoGrid");
        //    setTimeout(function () {
        //        Utility.UnLoading();
        //        grid.saveAsExcel();
        //    }, 2000)
        //    //Utility.UnLoading();
        //    //grid.saveAsExcel();
        //});
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            if (user.UserRoleId === 1) {
                $("#InitiateBulkChanges").css("display", "inline");
                $("#AddNew").css("display", "inline");
            }
            if (user.UserRoleId === 2) {
                $("#InitiateBulkChanges").css("display", "none");
                $("#AddNew").css("display", "none");
            }

        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetMOGDataList, function (data) {

            var dataSource = data;
            mogdata = [];
            mogdata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                mogdata.push({ "value": dataSource[i].MOGCode, "text": dataSource[i].Name });
            }
            $("#inputmog").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: mogdata,
                index: 0,
            });

            $("#mog").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: mogdata,
                index: 0,
            });


            $("#inputmog").data("kendoDropDownList").select(function (dataItem) {
                return dataItem.value === dataSource[0].value;
            });
            $("#inputmog").data("kendoDropDownList").select(0);
        }, null, false);

        HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataList, function (result) {

            if (result != null) {
                aplMasterDataSource = result.aplList;
                filtteredAplMasterDataSource = aplMasterDataSource;
                totalrecord = result.aplList[0].NumberOfRows;

                populateAPLMasterGrid();
            }
        }, null
            , true);

        $("#gridBulkChange").css("display", "none");
        populateBulkChangeControls();
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        changeControls.css("background-color", "#fff");
        changeControls.css("border", "none");

        //$("#gridBulkChange").children(".k-grid-header").css("border", "none");
        //$("#InitiateBulkChanges").css("display", "inline");
        //$("#CancelBulkChanges").css("display", "none");
        //$("#SaveBulkChanges").css("display", "none");
        //$("#bulkerror").css("display", "none");
        //$("#success").css("display", "none");
        changeControls.eq(6).text("");
        changeControls.eq(6).append("<div id='mog' style='width:100%; margin-left:3px; font-size:10.5px!important'></div>");
        //$(".k-label")[0].innerHTML.replace("items", "records");
    });

    $("#InitiateBulkChanges").on("click", function () {
        $("#gridBulkChange").css("display", "block");
        $("#gridBulkChange").children(".k-grid-header").css("border", "none");
        $("#InitiateBulkChanges").css("display", "none");
        $("#CancelBulkChanges").css("display", "inline");
        $("#SaveBulkChanges").css("display", "inline");
    });

    $("#CancelBulkChanges").on("click", function () {
        $("#InitiateBulkChanges").css("display", "inline");
        $("#CancelBulkChanges").css("display", "none");
        $("#SaveBulkChanges").css("display", "none");
        $("#gridBulkChange").css("display", "none");
        populateAPLMasterGrid();
    });

    $("#SaveBulkChanges").on("click", function () {

        var neVal = $(this).val();
        var dataFiltered = $("#gridAPLMaster").data("kendoGrid").dataSource.dataFiltered();


        var model = dataFiltered;
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        var validChange = false;
        var mog = "";

        var mogspan = $("#mog");//  changeControls.eq(4).children("span").children("span").children(".k-input");

        if (mogspan.val() != "Select") {
            validChange = true;
            mog = mogspan.val();
        }

        if (validChange == false) {
            toastr.error("Please change at least one attribute for Bulk Update");
        } else {
            Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                function () {
                    $.each(model, function () {
                        this.CreatedDate = kendo.parseDate(this.CreatedDate)
                        if (mog.length > 0)
                            this.MOGCode = mog;
                    });
                    debugger;
                    HttpClient.MakeSyncRequest(CookBookMasters.SaveAPLMasterDataList, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again.");
                        }
                        else {
                            $(".k-overlay").hide();
                            toastr.success("MOG configuration updated successfully");
                            $("#gridAPLMaster").data("kendoGrid").dataSource.data([]);
                            $("#gridAPLMaster").data("kendoGrid").dataSource.read();
                            $("#InitiateBulkChanges").css("display", "inline");
                            $("#CancelBulkChanges").css("display", "none");
                            $("#SaveBulkChanges").css("display", "none");
                            $("#gridBulkChange").css("display", "none");
                            // window.location.href = "/APLMaster/Index";
                            Utility.Loading();
                        }
                    },
                        {
                            model: model
                        }, true);
                    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataList, function (result) {
                        if (result != null) {
                            aplMasterDataSource = result;
                            filtteredAplMasterDataSource = aplMasterDataSource;
                            populateAPLMasterGrid();
                            Utility.UnLoading();
                        }
                    }, null
                        , true);
                    //populateAPLMasterGrid();
                    Utility.UnLoading();
                },
                function () {
                    maptype.focus();
                }
            );
        }

    });
    //Food Program Section Start

    $("#APLbtnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#APLwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#APLbtnSubmit").click(function () {

        if ($("#inputmog").val() === "Select") {
            toastr.error("Please select the MOG to proceed");
            $("#inputmog").focus();
        }
        else {
            $("#error").css("display", "none");
            var model;
            if (datamodel != null) {
                model = datamodel;
                model.MOGCode = $("#inputmog").val();
                model.CreatedDate = kendo.parseDate(datamodel.CreatedDate);
            }

            $("#APLbtnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeSyncRequest(CookBookMasters.SaveAPLMasterData, function (result) {
                if (result == false) {
                    $('#APLbtnSubmit').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputmog").focus();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#APLwindowEdit").data("kendoWindow");
                    orderWindow.close();
                    $('#APLbtnSubmit').removeAttr("disabled");
                    toastr.success("APL updated successfully");

                    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataList, function (result) {
                        if (result != null) {
                            aplMasterDataSource = result;
                            filtteredAplMasterDataSource = aplMasterDataSource;
                            populateAPLMasterGrid();
                        }
                    }, null
                        , true);
                    Utility.UnLoading();
                }
            }, {
                model: model

            }, true);
        }
    });

});

function mappedApl(e) {
    isMappedAPL = e.checked;
    if (aplMasterDataSource != null) {
        if (e.checked) {
            filtteredAplMasterDataSource = aplMasterDataSource.filter(m => m.MOGCode == null);
        }
        else {
            filtteredAplMasterDataSource = aplMasterDataSource;
        }
        populateAPLMasterGrid();

    }
}







function populateAPLMasterGrid() {

    var gridVariable = $("#gridAPLMaster");
    var dataSource = gridVariable.dataSource;
    //gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "APLMaster.xlsx",
            filterable: true,
            allPages: true
        },
        //change: () => { this.gridVariable. },
        sortable: true,
        filterable: true,
        //filterable: {
        //    extra: true,
        //    operators: {
        //        string: {
        //            contains: "Contains",
        //            startswith: "Starts with",
        //            eq: "Is equal to",
        //            neq: "Is not equal to",
        //            doesnotcontain: "Does not contain",
        //            endswith: "Ends with"
        //        }
        //    }
        //},
        height: 550,

        groupable: true,


        pageable: true,
        sortable: true,
        serverFiltering: true,
        columns: [
            {
                field: "SectorName", title: "Sector Name", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ArticleNumber", title: "Article Number", width: "35px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ArticleDescription", title: "Article Description", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOM", title: "UOM", width: "20px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "SiteName", title: "SiteName", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "SiteCode", title: "SiteCode", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ArticleCost", title: "Standard Cost", width: "30px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "StandardCostPerKg", title: "Cost Per Kg", width: "30px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },


            {
                field: "Hierlevel3", title: "Category", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },

        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";
                    //var dataRequest  = {
                    //    "Page": "12",
                    //    "PageSize": "20",
                    //}

                    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataList, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result.aplList);

                        }
                        else {
                            options.success("");
                        }
                    },

                        {
                            PageSize: 100,
                            Page: $("#gridAPLMaster").data("kendoGrid").dataSource.page(),
                            filter: null
                        }
                        , false);
                }
            },
            schema: {
                total: function (data) {
                    return totalrecord;
                },
                //data: function (data) {
                //    return data.aplList;
                //},

                model: {
                    id: "ArticleID",

                    fields: {
                        SectorName: { type: "string" },
                        ArticleNumber: { type: "string" },
                        ArticleDescription: { type: "string" },
                        UOM: { type: "string" },
                        ArticleType: { type: "string" },
                        Hierlevel3: { type: "string" },
                        MerchandizeCategoryDesc: { type: "string" },
                        MOGName: { type: "string" },
                        ModifiedDate: { type: "date" },
                        SiteCode: { type: "string" },
                        SiteName: { type: "string" },
                        ArticleCost: { type: "string" },
                        StandardCostPerKg: { type: "string" },
                    }
                }
            },
            pageSize: 100,
            serverPaging: true,
            serverSorting: true,
            //serverFiltering: true,
            serverGrouping: true,
            serverAggregates: true,
            page: pageindex,


        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }
    });

    //var grid = $("#gridAPLMaster").data('kendoGrid');
    //var pager = grid.pager;
    //pager.bind('page', function (e) {
    //    console.log(e.index);
    //    grid.dataSource.page(e.index);
    //});
    //$(".k-label")[0].innerHTML.replace("items", "records");
}

function populateBulkChangeControls() {

    Utility.Loading();
    var gridVariable = $("#gridBulkChange");
    gridVariable.html("");
    gridVariable.kendoGrid({
        selectable: "cell",
        sortable: false,
        filterable: false,
        groupable: false,
        //reorderable: true,
        //scrollable: true,
        columns: [
            {
                field: "", title: "", width: "50px"
            },
            {
                field: "", title: "", width: "100px"
            },
            {
                field: "", title: "", width: "30px"
            },
            {
                field: "", title: "", width: "40px"
            },
            {
                field: "", title: "", width: "50px"
            },
            {
                field: "", title: "", width: "80px"
            },
            {
                field: "MOG", title: "", width: "80px"
            },
            {
                field: "", title: "", width: "60px"
            },
            {
                field: "", title: "", width: "30px"
            }
        ],
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
        },
        change: function (e) {
        },
    });
}