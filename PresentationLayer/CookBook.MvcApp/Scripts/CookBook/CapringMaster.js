﻿var Sectormasterdata = [];
var levelmasterdata = [];
var functionmasterdata = [];
var permissionmasterdata = [];


var SectormasterdataEdit = [];
var levelmasterdataEdit = [];
var functionmasterdataEdit = [];
var permissionmasterdataEdit = [];

var Sectorcode="";
var levelcode = "";
var functioncode = "";
var permisioncode = "";
var datamodel;
$(document).ready(function () {
    populateCapringGrid();
});

HttpClient.MakeRequest(CookBookMasters.GetSectorMasterData, function (data) {
    var dataSource = data;
    Sectormasterdata = dataSource;
    //removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
    if (data.length > 1) {
        Sectormasterdata.unshift({ "SectorCode": 0, "SectorName": "Select Sector" })
        $("#btnGo").css("display", "inline-block");
    }

    else {
        $("#btnGo").css("display", "inline-none");
    }
    populateSectorMasterDropdown();
    populateSectorMasterDropdownEdit();


}, null, false);


HttpClient.MakeRequest(CookBookMasters.GetLevelMasterData, function (data) {
    var dataSource = data;
    levelmasterdata = dataSource;
    //regionmasterdata = sitemasterdata//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
    if (data.length > 1) {
        levelmasterdata.unshift({ "Code": 0, "Name": "Select Level" })
        $("#btnGo").css("display", "inline-block");
    }

    else {
        $("#btnGo").css("display", "inline-none");
    }
    populateLevelMasterDropdown();
    populateLevelMasterDropdownEdit();


}, null, false);



HttpClient.MakeRequest(CookBookMasters.GetFunctionMasterData, function (data) {
    var dataSource = data;
    functionmasterdata = dataSource;
   // regionmasterdata = sitemasterdata//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
    if (data.length > 1) {
        functionmasterdata.unshift({ "Code": 0, "Name": "Select Function" })
        $("#btnGo").css("display", "inline-block");
    }

    else {
        $("#btnGo").css("display", "inline-none");
    }
    populateFunctionMasterDropdown();
    populateFunctionMasterDropdownEdit();


}, null, false);

HttpClient.MakeRequest(CookBookMasters.GetPermissionMasterData, function (data) {
    var dataSource = data;
    permissionmasterdata = dataSource;
    //regionmasterdata = sitemasterdata//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
    if (data.length > 1) {
        permissionmasterdata.unshift({ "typ": "Select Permission", "Code": "Select Permission" })
        $("#btnGo").css("display", "inline-block");
    }

    else {
        $("#btnGo").css("display", "inline-none");
    }
    populatePermissionMasterDropdown();
    populatePermissionMasterDropdownEdit();


}, null, false);

function populateSectorMasterDropdown() {
    $("#ddlSectorMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "SectorName",
        dataValueField: "SectorCode",
        dataSource: Sectormasterdata,
        change: onChangeSector,  
        index: 0,
    });
    if (Sectormasterdata.length == 1) {
        $("#btnGo").click();

    }

}

function populateLevelMasterDropdown() {
    $("#ddlLevelMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "Code",
        dataSource: levelmasterdata,
        change: onChangeLevel,  
        index: 0,
    });
    if (levelmasterdata.length == 1) {
        $("#btnGo").click();

    }

}


function populateFunctionMasterDropdown() {
    $("#ddlFunctionMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "Code",
        dataSource: functionmasterdata,
        change: onChangeFunction,  
        index: 0,
    });
    if (functionmasterdata.length == 1) {
        $("#btnGo").click();

    }

}

function populatePermissionMasterDropdown() {
    $("#ddlPermissionMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "typ",
        dataValueField: "Code",
        dataSource: permissionmasterdata,
        change: onChangePermission,  
        index: 0,
    });
    if (permissionmasterdata.length == 1) {
        $("#btnGo").click();

    }

}



function populateSectorMasterDropdownEdit() {
    $("#EditddlSectorMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "SectorName",
        dataValueField: "SectorCode",
        dataSource: Sectormasterdata,
        change: onChangeSectorEdit,
        index: 0,
    });
    if (Sectormasterdata.length == 1) {
        $("#btnGo").click();

    }

}

function populateLevelMasterDropdownEdit() {
    $("#EditddlLevelMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "Code",
        dataSource: levelmasterdata,
        change: onChangeLevelEdit,
        index: 0,
    });
    if (levelmasterdata.length == 1) {
        $("#btnGo").click();

    }

}


function populateFunctionMasterDropdownEdit() {
    $("#EditddlFunctionMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "Code",
        dataSource: functionmasterdata,
        change: onChangeFunctionEdit,
        index: 0,
    });
    if (regionmasterdata.length == 1) {
        $("#btnGo").click();

    }

}

function populatePermissionMasterDropdownEdit() {
    $("#EditddlPermissionMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "typ",
        dataValueField: "Code",
        dataSource: permissionmasterdata,
        change: onChangePermissionEdit,
        index: 0,
    });
    if (permissionmasterdata.length == 1) {
        $("#btnGo").click();

    }

}


function onChangeSectorEdit() {

    var dropdownlist = $("#EditddlSectorMaster").data("kendoDropDownList");
    Sectorcode = dropdownlist.value();

    $("#txt_CapringCodeEdit").val(Sectorcode + levelcode + functioncode + permisioncode);


};
function onChangeLevelEdit() {
    var dropdownlist = $("#EditddlLevelMaster").data("kendoDropDownList");
    levelcode = dropdownlist.value();

    $("#txt_CapringCodeEdit").val(Sectorcode + levelcode + functioncode + permisioncode);
};

function onChangeFunctionEdit() {
    var dropdownlist = $("#EditddlFunctionMaster").data("kendoDropDownList");
    functioncode = dropdownlist.value();

    $("#txt_CapringCodeEdit").val(Sectorcode + levelcode + functioncode + permisioncode);
};

function onChangePermissionEdit() {
    var dropdownlist = $("#EditddlPermissionMaster").data("kendoDropDownList");
    permisioncode = dropdownlist.value();

    $("#txt_CapringCodeEdit").val(Sectorcode + levelcode + functioncode + permisioncode);
};  






    function onChangeSector() {
       
   var dropdownlist = $("#ddlSectorMaster").data("kendoDropDownList");
    Sectorcode = dropdownlist.value();
   
    $("#txt_CapringCode").val(Sectorcode + levelcode + functioncode + permisioncode);    
    
    
};  
function onChangeLevel() {
    var dropdownlist = $("#ddlLevelMaster").data("kendoDropDownList");
    levelcode = dropdownlist.value();
    
    $("#txt_CapringCode").val(Sectorcode + levelcode + functioncode + permisioncode);
};  

function onChangeFunction() {
    var dropdownlist = $("#ddlFunctionMaster").data("kendoDropDownList");
    functioncode = dropdownlist.value();

    $("#txt_CapringCode").val(Sectorcode + levelcode + functioncode + permisioncode);
};  

function onChangePermission() {
    var dropdownlist = $("#ddlPermissionMaster").data("kendoDropDownList");
    permisioncode = dropdownlist.value();

    $("#txt_CapringCode").val(Sectorcode + levelcode + functioncode + permisioncode);
};  

    $("#btnGo").on("click", function () {

        debugger;
        if ($("#ddlSectorMaster").val() == 0) {
            toastr.error("Please Select Sector");
            $("#ddlSectorMaster").focus();
            return false;
        }
        else if ($("#ddlLevelMaster").val() == 0) {
            toastr.error("Please Select Level");
            $("#ddlLevelMaster").focus();
            return false;
        }
        else if ($("#ddlFunctionMaster").val() == 0) {
            toastr.error("Please Select Function");
            $("#ddlFunctionMaster").focus();
            return false;
        }
        else if ($("#ddlPermissionMaster").val() == "Select Permission") {
            toastr.error("Please Select Permission");
            $("#ddlPermissionMaster").focus();
            return false;
        }
        else if ($("#txt_CapringDesc").val() == "") {
            toastr.error("Please input type description");
            $("#ddlPermissionMaster").focus();
            return false;
        }
        
        var model = datamodel;

        model = {

            "CAPRINGCode": $("#txt_CapringCode").val(),
            "Description": $("#txt_CapringDesc").val(),
            "SectorCode": $("#ddlSectorMaster").val(),
            "LevelCode": $("#ddlLevelMaster").val(),
            "FunctionCode": $("#ddlFunctionMaster").val(),
            "PermissionCode": $("#ddlPermissionMaster").val(),

        }

        
        HttpClient.MakeSyncRequest(CookBookMasters.SaveCapringInfo, function (result) {

            if (result.Data == 'Success') {                
                toastr.success("New CAPRING record added successfully");
                $(".k-window").hide();
                $(".k-overlay").hide();
                $("#gridCapringMaster").data("kendoGrid").dataSource.data([]);
                $("#gridCapringMaster").data("kendoGrid").dataSource.read();
                return;
            }
            else {
                toastr.error("CAPRING code already Exits.");
                return false;
            }
        }, {
            model: model

        }, true);

        Utility.UnLoading();
      
    });

$("#btnGoEdit").on("click", function () {

    
    if ($("#EditddlSectorMaster").val() == 0) {
        toastr.error("Please Select Sector");
        $("#EditddlSectorMaster").focus();
        return false;
    }
    else if ($("#EditddlLevelMaster").val() == 0) {
        toastr.error("Please Select Level");
        $("#EditddlLevelMaster").focus();
        return false;
    }
    else if ($("#EditddlFunctionMaster").val() == 0) {
        toastr.error("Please Select Function");
        $("#EditddlFunctionMaster").focus();
        return false;
    }
    else if ($("#EditddlPermissionMaster").val() == "Select Permission") {
        toastr.error("Please Select Permission");
        $("#EditddlPermissionMaster").focus();
        return false;
    }
    else if ($("#txt_CapringDescEdit").val() == "") {
        toastr.error("Please input type description");
        $("#txt_CapringDescEdit").focus();
        return false;
    }

    var model = datamodel;

    model = {
        "ID": $("#CapringID").val(),
        "CAPRINGCode": $("#txt_CapringCodeEdit").val(),
        "Description": $("#txt_CapringDescEdit").val(),
        "SectorCode": $("#EditddlSectorMaster").val(),
        "LevelCode": $("#EditddlLevelMaster").val(),
        "FunctionCode": $("#EditddlFunctionMaster").val(),
        "PermissionCode": $("#EditddlPermissionMaster").val(),

    }

    if ($("#CapringDescription").val() == $.trim(model.Description)) {
        HttpClient.MakeSyncRequest(CookBookMasters.UpdateCapringInfo, function (result) {

            if (result.Data == 'Success') {
                toastr.success("CAPRING record update successfully");
                $(".k-window").hide();
                $(".k-overlay").hide();
                $("#gridCapringMaster").data("kendoGrid").dataSource.data([]);
                $("#gridCapringMaster").data("kendoGrid").dataSource.read();
                return;
            }
            else {
                toastr.error("CAPRING code already Exits.");
                return false;
            }
        }, {
            model: model

        }, true);

        Utility.UnLoading();
    }
    else {
        HttpClient.MakeSyncRequest(CookBookMasters.UpdateCapringDescriptionInfo, function (result) {

            if (result.Data == 'Success') {
                toastr.success("CAPRING record update successfully");
                $(".k-window").hide();
                $(".k-overlay").hide();
                $("#gridCapringMaster").data("kendoGrid").dataSource.data([]);
                $("#gridCapringMaster").data("kendoGrid").dataSource.read();
                return;
            }
            else {
                toastr.error("CAPRING code already Exits.");
                return false;
            }
        }, {
            model: model

        }, true);

        Utility.UnLoading();
    }

    

});
    $("#btnExport").click(function (e) {
        var grid = $("#gridCapringMaster").data("kendoGrid");
        grid.saveAsExcel();
    });
    function populateCapringGrid() {

        
        
        var gridVariable = $("#gridCapringMaster");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "CAPRINGMaster.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,

            //pageable: true,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "CAPRINGCODE", title: "CAPRING Code", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: left;"
                    }
                },
               
                {
                    field: "Description", title: "Description", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "SectorName", title: "Sector Name", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Level Name", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FunctionName", title: "Function Name", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "typ", title: "Permission Name", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                

                //{
                //    field: "IsActive", title: "Active", width: "30px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},


                {
                    field: "Edit", title: "Action", width: "35px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {

                                var dialog = $("#Editmaindiv").data("kendoWindow");
                                var tr = $(e.target).closest("tr");
                                var gridObj = $("#gridCapringMaster").data("kendoGrid");
                                tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                // get the date of this row
                                $("#CapringID").val(tr.ID);
                                $("#CapringDescription").val(tr.Description);
                                

                                var model = {
                                    "ID": tr.ID,
                                    "CAPRINGCODE": tr.CAPRINGCODE,
                                    "Description": tr.Description,
                                    "SectorCode": tr.SectorCode,
                                    "LevelCode": tr.LevelCode,
                                    "FunctionCode": tr.FunctionCode,
                                    "PermissionCode": tr.PermissionCode,
                                    "SCODE": tr.SCODE,

                                };
                                dialog.open();
                                dialog.center();

                                datamodel = model;
                                
                                $("#EditddlSectorMaster").data('kendoDropDownList').value(model.SCODE);
                                $("#EditddlLevelMaster").data('kendoDropDownList').value(model.LevelCode);
                                $("#EditddlFunctionMaster").data('kendoDropDownList').value(model.FunctionCode);
                                $("#EditddlPermissionMaster").data('kendoDropDownList').value(model.PermissionCode);
                                
                                $("#txt_CapringCodeEdit").val(model.CAPRINGCODE);
                                $("#txt_CapringDescEdit").val(model.Description);

                                return true;
                            }
                        }
                        ,

                    ],
                },


            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetCapringMasterList, function (result) {
                            Utility.UnLoading();
                            if (result != null) {

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { type: "string" },
                            CAPRINGCODE: { type: "string" },                           
                            Description: { type: "string" },
                            IsActive: { type: "bool" },
                            SectorCode: { type: "string" },
                            LevelCode: { type: "string" },
                            FunctionCode: { type: "string" },
                            PermissionCode: { type: "string" },
                            SectorName: { type: "string" },
                            Name: { type: "string" },
                            typ: { type: "string" },
                            FunctionName: { type: "string" },
                            CreatedOn: { type: "date" },
                            ModifiedOn: { type: "date" },
                        }
                    }
                },
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {


                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='gridcell']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
                var items = e.sender.items();
                $(".chkbox").on("change", function () {

                    var gridObj = $("#gridCapringMaster").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    var trEdit = $(this).closest("tr");
                    var th = this;
                    datamodel = tr;

                    if ($(this)[0].checked == false) {
                        datamodel.IsActive = 0;
                    }
                    else {
                        datamodel.IsActive = 1;
                    }

                    var active = "";
                    if (datamodel.IsActive) {
                        active = 1;
                    } else {
                        active = 0;
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Sector " + active + "?", " CAPRING Update Confirmation", "Yes", "No", function () {
                        HttpClient.MakeRequest(CookBookMasters.UpdateCapringInactiveActive, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                toastr.success("CAPRING updated successfully");
                                $(th)[0].checked = datamodel.IsActive;
                                if ($(th)[0].checked) {
                                    $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                } else {
                                    $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                }
                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {

                        $(th)[0].checked = !datamodel.IsActive;
                        if ($(th)[0].checked) {
                            $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                        } else {
                            $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                        }
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //    if ($(".k-label")[0]!= null)
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }


$("#maindiv").kendoWindow({
    modal: true,
    width: "775px",
    height: "135px",
    title: "CAPRING Details Add ",
    actions: ["Close"],
    visible: false,
    animation: false
});
$("#Editmaindiv").kendoWindow({
    modal: true,
    width: "775px",
    height: "135px",
    title: "Edit CAPRING Details ",
    actions: ["Close"],
    visible: false,
    animation: false
});
$("#Capringcancel").on("click", function () {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $(".k-overlay").hide();
            var orderWindow = $("#maindiv").data("kendoWindow");
            orderWindow.close();
        },
        function () {
        }
    );
});
$("#CapringcancelEdit").on("click", function () {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $(".k-overlay").hide();
            var orderWindow = $("#Editmaindiv").data("kendoWindow");
            orderWindow.close();
        },
        function () {
        }
    );
});
$("#CapringAddNew").on("click", function () {
    //
    // var jq = jQuery.noConflict();
    // alert("calick");
    var model;

    var dialog = $("#maindiv").data("kendoWindow");

    //dialog.open();
    dialog.open().element.closest(".k-window").css({
        top: 167,
        left: 380

    });
    // dialog.center();

    datamodel = model;

    dialog.title("New CAPRING Details");

    DishCategoryCodeIntial = null;

});

$('#myInput').on('input', function (e) {
    var grid = $('#gridCapringMaster').data('kendoGrid');
    var columns = grid.columns;

    var filter = { logic: 'or', filters: [] };
    columns.forEach(function (x) {
        if (x.field) {
            if (x.field == "CAPRINGCODE" || x.field == "Description" || x.field == "SectorName" || x.field == "Name" || x.field == "typ" || x.field == "FunctionName") {
                var type = grid.dataSource.options.schema.model.fields[x.field].type;
                if (type == 'string') {
                    var targetValue = e.target.value;
                    if (x.field == "Status") {
                        var pendingString = "pending";
                        var savedString = "saved";
                        var mappedString = "mapped";
                        if (pendingString.includes(e.target.value.toLowerCase())) {
                            targetValue = "1";
                        }
                        else if (savedString.includes(e.target.value.toLowerCase())) {
                            targetValue = "2";
                        }
                        else if (mappedString.includes(e.target.value.toLowerCase())) {
                            targetValue = "3";
                        }
                    }
                    filter.filters.push({
                        field: x.field,
                        operator: 'contains',
                        value: targetValue
                    })
                }
                else if (type == 'number') {
                    if (isNumeric(e.target.value)) {
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: e.target.value
                        });
                    }
                } else if (type == 'date') {
                    var data = grid.dataSource.data();
                    for (var i = 0; i < data.length; i++) {
                        var dateStr = kendo.format(x.format, data[i][x.field]);
                        if (dateStr.startsWith(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: data[i][x.field]
                            })
                        }
                    }
                } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                    var bool = getBoolean(e.target.value);
                    filter.filters.push({
                        field: x.field,
                        operator: 'eq',
                        value: bool
                    });
                }
            }
        }
    });
    grid.dataSource.filter(filter);
});