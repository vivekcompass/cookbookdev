﻿var user;
var status = "";
var varname = "";
var datamodel;
var ItemName = "";
var foodprogramdata = [];
var menuitemdata = [];
var dishcategorydata = [];
var dietcategorydata = [];
var dietcategorydataOut = [];
var visualcategorydata = [];
var servewaredata = [];
var itemtype1data = [];
var itemtype2data = [];
var conceptype1data = [];
var conceptype2data = [];
var conceptype3data = [];
var conceptype4data = [];
var conceptype5data = [];
var uomdata = [];
var itemObject = {};
var ingredientObject = [];
var dishdata = [];
var masterGroupList = [];
var masterQty = [];
var currentMasterQtyList = [];
var currentMasterList = [];
var masterItems = [];
var finalModel = [];
var finalModelExits = [];
var finalModelExitsDishCategory = [];
var flagCurated = 0;
var finalModelDishCategory = [];
var dataItemsMulti = [];
var alertFlag = 0;
var selItemDietCategory = 0;
var isPublishedClicked = false;
var masterCategoryGroup = [];
var dishcategorydataInactive = [];
var isSubSectorApplicable = false;
var lastValid = 0;
var validNumber = new RegExp(/^\d*\.?\d*$/);
function validateNumber(elem) {

    if (validNumber.test(elem.value)) {
        lastValid = elem.value;
    } else {
        elem.value = lastValid;
    }
}
$(function () {

    $("#MenuCwindowEdit").kendoWindow({
        modal: true,
        width: "620px",
        title: "Item Details - " + ItemName,
        actions: ["Close"],
        animation: false,
        visible: false,
        resizable: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $('#myInput').on('input',
        function (e) {
            var grid = $('#gridItem').data('kendoGrid');
            var columns = grid.columns;

            var filter = { logic: 'or', filters: [] };
            columns.forEach(function (x) {
                if (x.field) {
                    if (x.field == "PublishedDateAsString" || x.field == "SectorName"|| x.field == "ConceptType2Name" || x.field == "ApplicableTo" || x.field == "ItemCode" || x.field == "ItemName" || x.field == "VisualCategoryName" || x.field == "FoodProgramName"
                        || x.field == "DietCategoryName" || x.field == "HSNCode" || x.field == "CGST" || x.field == "SGST" || x.field == "CESS" || x.field == "Status" || x.field == "SubSectorName") {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;

                        if (type == 'string') {
                            var targetValue = e.target.value;
                            if (x.field == "Status") {
                                var pendingString = "pending";
                                var savedString = "saved";
                                var mappedString = "mapped";
                                if (pendingString.includes(e.target.value.toLowerCase())) {
                                    targetValue = "1";
                                }
                                else if (savedString.includes(e.target.value.toLowerCase())) {
                                    targetValue = "2";
                                }
                                else if (mappedString.includes(e.target.value.toLowerCase())) {
                                    targetValue = "3";
                                }
                            }
                            filter.filters.push({
                                field: x.field,
                                operator: 'contains',
                                value: targetValue
                            })
                        }
                        else if (type == 'number') {

                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        } else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format('dd-MMM-yy', data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                }
            });
            grid.dataSource.filter(filter);
        }
    );
});

$(document).ready(function () {
    $(".k-window").hide();
    $(".k-overlay").hide();
        Utility.Loading();
        HttpClient.MakeSyncRequest(CookBookMasters.GetDietCategoryDataList, function (data) {
            var dataSource = data;
            dietcategorydata = [];
            dietcategorydataOut = [];
            dietcategorydataOut.push({ "value": "Select", "text": "Select Diet Category" });
            dietcategorydata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                dietcategorydata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "DietCategoryCode": dataSource[i].DietCategoryCode });
                dietcategorydataOut.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "DietCategoryCode": dataSource[i].DietCategoryCode });

            }
            populateDietCategoryDropdown();
            populateDietCategoryDropdown_Bulk();
        }, null, false);

        $("#btnExport").click(function (e) {
            var grid = $("#gridItem").data("kendoGrid");
            grid.saveAsExcel();
        });
       
        HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            populateItemGrid();
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNew").css("display", "inline");
            //    $("#btnSubmitClose").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNew").css("display", "none");
            //    $("#btnSubmitClose").css("display", "none");
            //}
            
            
        }, null, false);
        
        HttpClient.MakeRequest(CookBookMasters.GetSubSectorMasterList, function (data) {

            if (data.length > 1) {
                isSubSectorApplicable = true;
                data.unshift({ "SubSectorCode": 'Select', "Name": "Select Sub Sector" })
                $("#inputsubsector").kendoDropDownList({
                    filter: "contains",
                    dataTextField: "Name",
                    dataValueField: "SubSectorCode",
                    dataSource: data,
                    index: 0,
                });
            }
            else {
                isSubSectorApplicable = false;
            }
        }, { isAllSubSector: true }, false);
       
        
        HttpClient.MakeRequest(CookBookMasters.GetFoodProgramDataList, function (data) {
            var dataSource = data;
            foodprogramdata = [];
            foodprogramdata.push({ "value": "Select", "text": "Select", "FoodProgramCode": null, "IsActive": 1});
            for (var i = 0; i < dataSource.length; i++) {
                foodprogramdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "FoodProgramCode": dataSource[i].FoodProgramCode, "IsActive": dataSource[i].IsActive });
            }
            populateFoodProgramDropdown();
           // populateFoodProgramDropdown_Bulk();
        }, null, false);

        HttpClient.MakeSyncRequest(CookBookMasters.GetUOMDataList, function (data) {
            var dataSource = data;
            uomdata = [];
            uomdata.push({ "value": "UOM", "text": "UOM" });
            for (var i = 0; i < dataSource.length; i++) {
                uomdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].Name });
            }
          
        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetDishCategoryList, function (data) {
            var dataSource = data;
            dishcategorydata = [];

            for (var i = 0; i < dataSource.length; i++) {
                if (!dataSource[i].IsActive)
                    dishcategorydataInactive.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "code": dataSource[i].DishCategoryCode, "IsActive": dataSource[i].IsActive });

                dishcategorydata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "code": dataSource[i].DishCategoryCode,"IsActive":dataSource[i].IsActive });
            }
            populateDishCategoryMultiSelect();

        }, null, false);

      

      
        //$("#gridBulkChange").css("display", "none");
        //populateBulkChangeControls();
        //var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        //changeControls.css("background-color", "#fff");
        //changeControls.css("border", "none");
        //changeControls.eq(1).text("");
        //changeControls.eq(1).append("<div id='dietcategory' style='width:100%; font-size:12px!important'></div>");
        //changeControls.eq(2).text("");
        //changeControls.eq(2).append("<div id='foodprogram' style='width:100%; font-size:12px!important'></div>");
        //changeControls.eq(3).text("");
        //changeControls.eq(3).append("<div id='visualcategory' style='width:100%; font-size:12px!important'></div>");

        $("#InitiateBulkChanges").on("click", function () {
            $("#gridBulkChange").css("display", "block");
            $("#gridBulkChange").children(".k-grid-header").css("border", "none");
            $("#InitiateBulkChanges").css("display", "none");
            $("#CancelBulkChanges").css("display", "inline");
            $("#SaveBulkChanges").css("display", "inline");
        });

        //function populateFoodProgramDropdown_Bulk() {
        //    $("#foodprogram").kendoDropDownList({
        //        filter: "contains",
        //        dataTextField: "text",
        //        dataValueField: "value",
        //        dataSource: foodprogramdata,
        //        index: 0,
        //    });
        //}

        function populateDietCategoryDropdown_Bulk() {
            //$("#dietcategory").kendoDropDownList({
            //    filter: "contains",
            //    dataTextField: "text",
            //    dataValueField: "value",
            //    dataSource: dietcategorydata,
            //    index: 0,
            //});
            $("#dietcategorysearch").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: dietcategorydataOut,
                index: 0,
                select: function (e) {
                    if (e.item.data().offsetIndex == 0) {
                        //e.preventDefault();
                        var filter = { logic: 'or', filters: [] };
                        var grid = $('#gridItem').data('kendoGrid');
                        grid.dataSource.filter(filter);
                        return;
                    }

                    var grid = $('#gridItem').data('kendoGrid');
                    var columns = grid.columns;

                    var filter = { logic: 'or', filters: [] };
                    columns.forEach(function (x) {
                        if (x.field) {
                            if ( x.field == "DietCategoryName" ) {
                                var type = grid.dataSource.options.schema.model.fields[x.field].type;
                                var targetValue = e.sender.dataItem(e.item).text;

                                if (type == 'string') {
                                    if (x.field == "Status") {
                                        var pendingString = "pending";
                                        var savedString = "saved";
                                        var mappedString = "mapped";
                                        if (pendingString.includes(targetValue.toLowerCase())) {
                                            targetValue = "1";
                                        }
                                        else if (savedString.includes(targetValue.toLowerCase())) {
                                            targetValue = "2";
                                        }
                                        else if (mappedString.includes(targetValue.toLowerCase())) {
                                            targetValue = "3";
                                        }
                                    }
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: targetValue
                                    })
                                }
                                else if (type == 'number') {

                                    if (isNumeric(e.target.value)) {
                                        filter.filters.push({
                                            field: x.field,
                                            operator: 'eq',
                                            value: e.target.value
                                        });
                                    }
                                } else if (type == 'date') {
                                    var data = grid.dataSource.data();
                                    for (var i = 0; i < data.length; i++) {
                                        var dateStr = kendo.format(x.format, data[i][x.field]);
                                        if (dateStr.startsWith(e.target.value)) {
                                            filter.filters.push({
                                                field: x.field,
                                                operator: 'eq',
                                                value: data[i][x.field]
                                            })
                                        }
                                    }
                                } else if (type == 'boolean' && getBoolean(targetValue) !== null) {
                                    var bool = getBoolean(e.target.value);
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: bool
                                    });
                                }
                            }
                        }
                    }); grid.dataSource.filter(filter);
                   
                }
           
            });
        }

        //function populateVisualCategoryDropdown_Bulk() {
        //    $("#visualcategory").kendoDropDownList({
        //        filter: "contains",
        //        dataTextField: "text",
        //        dataValueField: "value",
        //        dataSource: visualcategorydata,
        //        index: 0,
        //    });
        //}

        function populateFoodProgramDropdown() {
            $("#inputfoodprogram").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: foodprogramdata,
                index: 0,
                
                change: function (e) {
                    if (!e.sender.dataItem()?.IsActive) {
                       toastr.error("Selected Item is In Active");
                        var dropdownlist = $("#inputfoodprogram").data("kendoDropDownList");
                        dropdownlist.select("");
                    }

                }
            });
        }


        function populateMenuItemDropdown() {
            $("#inputmenuitem").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: menuitemdata,
                index: 0,
                //select: function (e) {
                //    if (e.item.data().offsetIndex == 0) {
                //        e.preventDefault();
                //    }
                //}
            });
            var dropdownlist = $("#inputmenuitem").data("kendoDropDownList");
            dropdownlist.bind("change", dropdownlist_selectMain);
            //dropdownlist.bind("select", dropdownlist_selectMain);

        }
        // 

        function dropdownlist_selectMain(eid) {
            // var item = e.item;
            //var mid = e.sender.element[0].id;
            var ID = eid.sender.dataItem(eid.item).value;
            var ItemCode = eid.sender.dataItem(eid.item).ItemCode;
            if (ID == 0) {
                return;
            }
            // alert(mid);
            //console.log(mid);
            console.log("ID" + ID);
            console.log("ItemCode" + ItemCode);
            //hare krishna
            $("#dishCategory").empty();
            currentMasterList = [];
            masterGroupList = [];
            masterQty = [];
            currentMasterQtyList = [];
            CreateDishCategoryOutline(ItemCode,ID);
        }

        function populateDishCategoryMultiSelect() {
            $("#inputdishcategory").kendoMultiSelect({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: dishcategorydata,
                index: 0,
                autoClose: false
            });
            var multiselect = $("#inputdishcategory").data("kendoMultiSelect");
            multiselect.bind("change", multiselect_select);
        }

        function multiselect_select(e) {
          
            var multiselect = $("#inputdishcategory").data("kendoMultiSelect");
            var dataItems = multiselect.dataItems();

        // for (var i = 0; i < dishcategorydataInactive.length; i++) {
            if (dataItems[dataItems.length - 1].IsActive != undefined && dataItems[dataItems.length - 1].IsActive == false) {
                    toastr.error("Selected Dish Category is Inactive");
                    dataItems.pop();
                    var multiarray = dataItems.map(item => item.value);
                    // var multiselect = $("#inputdishcategory").data("kendoMultiSelect");

                    //clear filter
                    multiselect.dataSource.filter({});

                    //set value
                    multiselect.value(multiarray);
                   // e.preventdefault();
                  //  return;
                }
            // }2 more lines
           
           
            var count = dataItems.length;;
            $("#dischCount").text(count);
        }



        $("#SaveBulkChanges").on("click", function () {

            var neVal = $(this).val();
            var dataFiltered = $("#gridItem").data("kendoGrid").dataSource.dataFiltered();


            var model = dataFiltered;
            var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
            var validChange = false;
            var foodprogram = "";
            var dietcategory = "";
            var visualcategory = "";
            var foodprogramcode = "";
            var dietcategorycode = "";
            var visualcategorycode = "";

            var foodprogramspan = $("#foodprogram");//  changeControls.eq(4).children("span").children("span").children(".k-input");
            var dietcategoryspan = $("#dietcategory");
            var visualcategoryspan = $("#visualcategory");

            if (foodprogramspan.val() != "Select") {
                validChange = true;
                foodprogram = foodprogramspan.val();
                if (foodprogram != null && foodprogram != "") {
                    foodprogramcode = $("#foodprogram").data("kendoDropDownList").dataItem().FoodProgramCode;
                }
            }
            if (dietcategoryspan.val() != "Select") {
                validChange = true;
                dietcategory = dietcategoryspan.val();
                if (dietcategory != null && dietcategory != "") {
                    dietcategorycode = $("#dietcategory").data("kendoDropDownList").dataItem().DietCategoryCode;
                }
            }
            if (visualcategoryspan.val() != "Select") {
                validChange = true;
                visualcategory = visualcategoryspan.val();
                if (visualcategory != null && visualcategory != "") {
                    visualcategorycode = $("#visualcategory").data("kendoDropDownList").dataItem().VisualCategoryCode;
                }
            }

            if (validChange == false) {
                $("#bulkerror").css("display", "block");
                $("#bulkerror").find("p").text("Please change at least one attribute for Bulk Update");
            } else {
                $("#bulkerror").css("display", "none");
                Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                    function () {
                        $.each(model, function () {
                            this.ModifiedOn = Utility.CurrentDate();
                            this.CreatedOn = kendo.parseDate(this.CreatedOn);
                            this.ModifiedBy = user.UserId;
                            if (foodprogram != "") {
                                this.FoodProgram_ID = foodprogram;
                                this.FoodProgramCode = foodprogramcode;
                            }
                           
                            if (dietcategory != "") {
                                this.DietCategory_ID = dietcategory;
                                this.DietCategoryCode = dietcategorycode;
                            }
                            if (visualcategory != "") {
                                this.VisualCategory_ID = visualcategory;
                                this.VisualCategoryCode = visualcategorycode;
                            }
                        });

                        HttpClient.MakeRequest(CookBookMasters.SaveItemDataList, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again.");
                            }
                            else {
                                $(".k-overlay").hide();
                                toastr.success("Records have been updated successfully");
                                $("#gridItem").data("kendoGrid").dataSource.data([]);
                                $("#gridItem").data("kendoGrid").dataSource.read();
                                $("#InitiateBulkChanges").css("display", "inline");
                                $("#CancelBulkChanges").css("display", "none");
                                $("#SaveBulkChanges").css("display", "none");
                                $("#gridBulkChange").css("display", "none");
                            }
                        }, {
                            model: model

                        }, true);

                        //populateSiteGrid();
                    },
                    function () {
                        foodprogramspan.focus();
                    }
                );
            }
        });

    function CreateDishCategoryOutline(itemcode, id,sectorcode) {
        
        HttpClient.MakeSyncRequest(CookBookMasters.GetNationalItemDishCategoryMappingDataList, function (data) {
                finalModelExitsDishCategory = data;
                var dataItems = [];
                var multiarray = [];
                dataItems1 = finalModelExitsDishCategory.filter(function (x) {

                    if (multiarray.indexOf(parseInt(x.DishCategory_ID)) == -1) {
                        multiarray.push(x.DishCategory_ID);
                        return x;
                    }

                });

                for (xi of dataItems1) {
                    dishcategorydata.forEach(function (yi) {
                        if (yi.code == xi.DishCategoryCode) {
                            dataItems.push({ "value": xi.DishCategory_ID, "text": yi.text, "stdqty": xi.StandardPortion, "ucode": xi.UOMCode, "wtuom": xi.WeightPerUOM })
                        }
                    });
                }

                for (item of dataItems) {
                    var divid = "dish" + item.value;
                    var dishesid = "inputdish" + item.value;
                    var inputqty = "inputqty" + item.value;
                    var subdishid = "subinputdish" + item.value;
                    var uomid = "uom" + item.value;
                    var wtspanid = "wtspan" + uomid;
                    var wtqty = item.wtuom;//radha
                    if (wtqty == null)
                        wtqty = 0;
                    var uomcode = item.ucode;
                    var wtuom = "wtuom" + uomid;
                    $('#dishCategory')
                        .append(
                            '<div class="dish"  >' +
                            '     <div class="upper"  id=' + divid + '>' +
                            '          <div class= "upperText"   onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')">' + item.text + '</div>' +
                            '          <input id="' + inputqty + '"  class="qty"  type="Number" oninput="validateNumber(this);" value="0" min="0" onchange="updateQuantity(' + inputqty + ')" />' +
                            '          <div id = "' + uomid + '" class="colorG"></div >' +
                            '<span class="serv">Per Serving</span>' +
                            '          <span id="' + wtspanid + '">' +
                            '                <input id="' + wtuom + '" class="qtyT" type="Number" oninput="validateNumber(this);" value="' + wtqty + '" min="0" onchange="updateWtPerUOMQuantity(' + wtuom +')"/>' +
                            '                <span class="wtlabel">wt. /pc (kg)</span>' +
                            '          </span>'+
                            
                            '          <span class="hideG" onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')"> Hide <i class="fas fa-angle-up" ></i ></span>' +
                            '          <span class="displayG"></span>' +
                            '          <span class="ddldish" id=' + dishesid + '></span><span class="dishlabel">Choose Dish</span>' +
                            '     </div>' +
                            '     <div class= "lower" id = ' + subdishid + '></div>' +
                            '</div >');

                    populateUOMDropdown('#' + uomid, item.ucode);
                    populateDishDataDropdown('#' + dishesid, item.text, "#" + subdishid);
                   
                    currentMasterList.push({ "dropdownid": dishesid, "id": inputqty, "uomid": uomid, "uomcode": item.ucode });

                    $("#" + inputqty).val(item.stdqty);
                    currentMasterQtyList.push({ "id": inputqty, "qty": item.stdqty, "uomid": uomid, "uomcode": item.ucode });
                    if (uomcode != "UOM-00003") {
                        $("#" + wtspanid).hide();
                    }
                    if (uomcode == "UOM-00003") {

                        $("#wtuom" + uomid).val(wtqty);
                    }
                }
                var multiselect = $("#inputdishcategory").data("kendoMultiSelect");

                //clear filter
                multiselect.dataSource.filter({});

                //set value
                multiselect.value(multiarray);

                dataItemsMulti = multiselect.dataItems();
                masterQty = currentMasterQtyList;

                if (finalModelExitsDishCategory.length > 0) { CreateDishTiles(id); }

        }, { itemCode: itemcode, sectorcode:sectorcode }, false);
            Utility.UnLoading();
        }

    function CreateDishTiles(itemID) {
       
            HttpClient.MakeSyncRequest(CookBookMasters.GetItemDishMappingDataList, function (data) {
                ingredientObject = [];
                // var dataSource = data;
                var dataItems = [];
                finalModelExits = data;
                console.log("Final Model");
                console.log(finalModelExits);
                masterGroupList = [];
                //   currentMasterQtyList = [];

                //finalModelExist = ItemDisCategoryDish Mapping
                //build dataset
                for (itemRec of finalModelExits) {
                    var localdishdata = [];

                    localdishdata = dishdata.filter(function (item) {

                        return item.value == itemRec.Dish_ID;
                    });

                    var text = localdishdata[0].text;   //Dish Name
                    var tooltip = localdishdata[0].alias;
                    var dcat = localdishdata[0].DietCategoryCode;
                    var idr = localdishdata[0].DishCategoryID;
                    var smid = localdishdata[0].value;   //Dish ID
                    var ingPrice = '-';//localdishdata[0].price; // Dishlevel Cost to be compued basis portion given
                    var ingPrice = localdishdata[0].price;
                    var wtPerUOM = itemRec.WeightPerUOM;
                    var dropdownid = "inputdish" + idr;   //Dish dropdown at DC level
                    var subid = "#subinputdish" + idr;    //Container for all Dishes to keep the Dish tiles for a category
                    var sid = "small" + smid;             // Dish Tile ID
                    //  var inputqty = "inputqty" + idr;      // Dish Category Qty ID
                    var ingID = smid;                     // DishID
                    var qty = itemRec.StandardPortion;    //StandardPortion
                    var subqty = itemRec.ContractPortion; //ContractPortion
                    var costPerKg = localdishdata[0].price;
                    var cuisine = localdishdata[0].CuisineName;
                    if (qty == null) {
                        qty = 0;
                    }
                    ingredientObject.push({
                        "ID": ingID,
                        "Name": text,
                        "Price": 0
                    })
                    //pura scelaton of Dish Category Section + Dish Tile Section ka information
                    masterGroupList.push({
                        "dropdownid": dropdownid,
                        "subid": subid,
                        "sid": sid,
                        "ingPrice": ingPrice,
                        "ingID": ingID,
                        "text": text,
                        "smallinput": "inputsubqty" + sid,
                        "ContractPortion": subqty,
                        "StandardPortion": itemRec.StandardPortion,
                        "UOMCode": itemRec.UOMCode,    //Dish's UOM Code
                        "DietCategoryCode": dcat,
                        "WeigthPerUOM": wtPerUOM,
                        "CostPerKg": costPerKg,
                        "tooltip": tooltip,
                        "CuisineName": cuisine

                    });
                }

                RecreateWholeExits();
                multiselect_select();

            }, { itemID: itemID }, false);

        }

        function populateItemGrid() {
            Utility.Loading();
            if (user.SectorNumber == 10) {
                var gridVariable = $("#gridItem");
                gridVariable.html("");
                var grid = gridVariable.kendoGrid({
                    //selectable: "cell",
                    sortable: true,
                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                startswith: "Starts with",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                doesnotcontain: "Does not contain",
                                endswith: "Ends with"
                            }
                        }
                    },
                    groupable: false,
                    pageable: false,
                    //reorderable: true,
                    //scrollable: true,
                    columns: [

                        {
                            field: "ItemCode", title: "Item Code", width: "30px", attributes: {
                                style: "text-align: center; font-weight:normal"
                            },
                            headerAttributes: {
                                style: "text-align: center"
                            }
                        },
                        {
                            field: "ItemName", title: "Item Name", width: "110px", attributes: {

                                style: "text-align: left; font-weight:normal;float: left; display: flex;margin-top: 3px; "
                            },
                            template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
                        },
                        //{
                        //    field: "SubSectorName", title: "Sub Sector", width: "60px", attributes: {
                        //        style: "text-align: left; font-weight:normal"
                        //    },
                        //},
                        {
                            field: "PublishedDateAsString", title: "Last Updated", width: "50px", attributes: {

                                style: "text-align: center; font-weight:normal"
                            },
                            //  template: '# if (PublishedDate == null) {#<span>-</span>#} else{#<span>#: kendo.toString(PublishedDate, "dd-MMM-yy")#</span>#}#',
                            headerAttributes: {
                                style: "text-align: center;"
                            }
                        },
                        {
                            field: "Edit", title: "Edit", width: "25px",
                            attributes: {
                                style: "text-align: center; font-weight:normal;cursor: pointer;"
                            },
                            headerAttributes: {
                                style: "text-align: center; vertical-align:middle"
                            },
                            command: [
                                {
                                    name: 'Edit',
                                    template: function (dataItem) {

                                        var html = `<a href="#" onClick="itemGridEdit(this)" class="fa fa-pen" title="Click to Edit Item Details" style='color:#8f9090; margin-left:8px'></ a>`;
                                        return html;
                                    },
                                    width: "35px",

                                }
                            ]

                        },
                        {
                            field: "MapDishes", title: "Item-Dish", width: "35px",
                            attributes: {
                                style: "text-align: center; font-weight:normal"
                            },
                            headerAttributes: {
                                style: "text-align: center; vertical-align:middle"
                            },
                            command: [
                                {
                                    title: 'Map',
                                    name: 'Map',
                                    //template: "<div class='fas fa-link'></div>",
                                    click: function (e) {
                                        if (dishdata.length < 1) {
                                            populateDishData();
                                        }

                                        var gridObj = $("#gridItem").data("kendoGrid");
                                        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                        if (tr.ItemCode > 80000) { return; }
                                        Utility.Loading();
                                        setTimeout(function () {
                                            ingredientObject = [];
                                            masterGroupList = [];
                                            finalModelExitsDishCategory = [];



                                            itemObject.ItemName = tr.ItemName;
                                            itemObject.ID = tr.ID;
                                            itemObject.Code = tr.ItemCode;
                                            itemObject.ItemPrice = 100;
                                            itemObject.DietCategorID = tr.DietCategory_ID;
                                            selItemDietCategory = tr.DietCategoryCode;
                                            itemObject.ImagePath = 'ItemImages/' + '/' + tr.ImageName;
                                            $("#itemCode").text(tr.ItemCode);
                                            $("#itemName").text(tr.ItemName).focus();
                                            $("#fpname").text(tr.FoodProgramName);
                                            $("#fptype").text(tr.Foo);
                                            $("#vcat").text(tr.VisualCategoryName);
                                            $("#iname").text(tr.ItemName + " (Item Code: " + tr.ItemCode + ")");
                                            if (tr.ConceptType2Code == 'CT2-00001' || tr.ConceptType2Code == 'CT2-00003') {
                                                $("#fptype").text("Curated");
                                                flagCurated = 1;
                                            } else {
                                                $("#fptype").text("Non-Curated");
                                                flagCurated = 0;
                                            }
                                            $("#fptype").hide();
                                            $("#fpname").hide();
                                            $("#pipeSpan").hide();
                                            $("#itemName").attr('title', tr.ItemName);
                                            if (tr.DietCategory_ID == 3) {
                                                $("#itemDietCategoryStatus").removeClass("statusNonVeg statusEgg");
                                                $("#itemDietCategory").removeClass("squareNonVeg squareEgg");
                                                $("#itemDietCategoryStatus").addClass("statusVeg");
                                                $("#itemDietCategory").addClass("squareVeg")
                                            } else if (tr.DietCategory_ID == 2) {
                                                $("#itemDietCategoryStatus").removeClass("statusVeg statusEgg");
                                                $("#itemDietCategory").removeClass("squareVeg squareEgg");
                                                $("#itemDietCategory").addClass("squareNonVeg")
                                                $("#itemDietCategoryStatus").addClass("statusNonVeg")
                                            } else {
                                                $("#itemDietCategoryStatus").removeClass("statusNonVeg statusVeg");
                                                $("#itemDietCategory").removeClass("squareNonVeg squareVeg");
                                                $("#itemDietCategory").addClass("squareEgg")
                                                $("#itemDietCategoryStatus").addClass("statusEgg")
                                            }
                                            $("#itemPrice").text(itemObject.ItemPrice);
                                            $("#ItemMaster").hide();
                                            $("#ConfigMaster").show();
                                            $("#imageMenuItem").attr('src', itemObject.ImagePath);
                                            $("#topHeading").text("Sector Menu Item - Dish Mapping");

                                            //hare krishna
                                            $("#dishCategory").empty();
                                            //var multiarray = [];
                                            masterQty = [];
                                            currentMasterQtyList = [];
                                            currentMasterList = [];
                                            masterGroupList = [];
                                            var dropdownlist = $("#inputmenuitem").data("kendoDropDownList");
                                            dropdownlist.select(0);
                                            CreateDishCategoryOutline(tr.ItemCode, tr.ID);

                                            //if (user.UserRoleId === 1) {
                                            //    $("#btnSubmitRecipe").css('display', 'inline');
                                            //    // $("#btnRecipeCancel").css('display', 'inline');
                                            //    $("#btnSubmitClose").css("display", "inline");
                                            //} else {
                                            //    $("#btnSubmitRecipe").css('display', 'none');
                                            //    //  $("#btnRecipeCancel").css('display', 'none');
                                            //    $("#btnSubmitClose").css("display", "none");
                                            //}
                                            Utility.UnLoading();
                                        }, 2000)
                                    }
                                }
                            ]

                        },
                        {
                            field: "Status", title: "Status", width: "35px", attributes: {
                                class: "mycustomstatus",
                                style: "text-align: center; font-weight:normal"
                            },
                            template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Mapped#}#',
                            headerAttributes: {

                                style: "text-align: center"
                            },
                        },
                        {
                            field: "DietCategoryName", title: "", width: "1px", attributes: {

                                style: "text-align: center; font-weight:normal;"
                            },
                            headerAttributes: {
                                style: "text-align: center; width:1px; display:none;"
                            },
                            template: '<span class="itemname"></span>',
                        }
                    ],
                    dataSource: {
                        transport: {
                            read: function (options) {

                                var varCodes = "";

                                HttpClient.MakeSyncRequest(CookBookMasters.GetItemDataList, function (result) {
                                    Utility.UnLoading();

                                    if (result != null) {
                                        options.success(result);


                                        var dataSource = result;
                                        menuitemdata = [];
                                        menuitemdata.push({ "value": 0, "text": "Select" });
                                        for (var i = 0; i < dataSource.length; i++) {
                                            menuitemdata.push({
                                                "value": dataSource[i].ID, "text": dataSource[i].ItemName, "ItemCode": dataSource[i].ItemCode
                                            });
                                        }
                                        populateMenuItemDropdown();
                                    }
                                    else {
                                        options.success("");
                                    }
                                }, null
                                    //{
                                    //filter: mdl
                                    //}
                                    , true);
                            }
                        },
                        schema: {
                            parse: function (data) {
                                var events = [];
                                for (var i = 0; i < data.length; i++) {
                                    var event = data[i];
                                    event.PublishedDate = kendo.toString(event.PublishedDate, 'dd-MMM-yy');
                                    events.push(event);
                                }
                                return events;
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ItemCode: { type: "string" },
                                    ItemName: { type: "string" },
                                    DietCategoryName: { type: "string" },
                                    VisualCategoryName: { type: "string" },
                                    FoodProgramName: { type: "string" },
                                    HSNCode: { type: "string" },
                                    CGST: { type: "string" },
                                    SGST: { type: "string" },
                                    CESS: { type: "string" },
                                    Status: { type: "string" },
                                    ApplicableTo: { type: "string" },
                                    ConceptType2Name: { type: "string" },
                                    PublishedDate: { type: "date" },
                                    CreatedOn: { type: "date" },
                                    PublishedDateAsString: { type: "string" },
                                    SubSectorName: { type: "string" },
                                }

                            }
                        },
                        pageSize: 150,
                    },
                    columnResize: function (e) {
                        var grid = gridVariable.data("kendoGrid");
                        e.preventDefault();
                    },
                    //  height: 490,
                    noRecords: {
                        template: "No Records Available"
                    },
                    dataBound: function (e) {
                        var grid = this;

                        grid.tbody.find("tr[role='row']").each(function () {
                            var model = grid.dataItem(this);

                            if (model.ItemCode > 80000) {
                                $(this).find(".k-grid-Map").addClass("k-state-disabled");
                            }
                        });

                        var view = this.dataSource.view();
                        for (var i = 0; i < view.length; i++) {
                            if (view[i].Status == 1) {
                                this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                    .find(".mycustomstatus").css("background-color", "#ffe1e1");

                            }
                            else if (view[i].Status == 2) {
                                this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                    .find(".mycustomstatus").css("background-color", "#ffffbf");
                            }
                            else {
                                this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                    .find(".mycustomstatus").css("background-color", "#d9ffb3");
                            }

                        }
                        var items = e.sender.items();

                        //items.each(function (e) {

                        //    if (user.UserRoleId == 1) {
                        //        $(this).find('.k-grid-Edit').text("Edit");
                        //    } else {
                        //        $(this).find('.k-grid-Edit').text("View");
                        //    }
                        //});
                    },
                    change: function (e) {
                    },
                    excelExport: function onExcelExport(e) {
                        var sheet = e.workbook.sheets[0];
                        var grid = $("#gridItem").data("kendoGrid");
                        var data = grid.dataSource._data
                        var cols = Object.keys(data[0])
                        var columns = cols.filter(function (col) {
                            if (col == "HSNCode" || col == "SGST" || col == "CGST" || col == "CESS" || col == "VisualCategoryName" || col == "DietCategoryName" || col == "FoodProgramName" || col == "ApplicableTo" || col == "ItemName" || col == "ItemCode" || col == "SubSectorName")
                                return col;
                        });
                        var columns1 = columns.map(function (col) {
                            return {
                                value: col,
                                autoWidth: true,
                                background: "#7a7a7a",
                                color: "#fff"
                            };
                        });

                        var rows = [{ cells: columns1, type: "header" }];

                        for (var i = 0; i < data.length; i++) {
                            var rowCells = [];
                            for (var j = 0; j < columns.length; j++) {

                                var cellValue = data[i][columns[j]];
                                rowCells.push({ value: cellValue });
                            }
                            rows.push({ cells: rowCells, type: "data" });
                        }
                        sheet.rows = rows;
                    }
                    //})
                });
                //grid = grid.data("kendoGrid")
                //if (user.SectorNumber != "10") {
                //    grid.hideColumn("SubSectorName");
                //}
                return;
            }
            else if (user.SectorNumber == 20) {
                var gridVariable = $("#gridItem");
                gridVariable.html("");
                var grid = gridVariable.kendoGrid({
                    //selectable: "cell",
                    sortable: true,
                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                startswith: "Starts with",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                doesnotcontain: "Does not contain",
                                endswith: "Ends with"
                            }
                        }
                    },
                    groupable: false,
                    pageable: false,
                    //reorderable: true,
                    //scrollable: true,
                    columns: [

                        {
                            field: "ItemCode", title: "Item Code", width: "30px", attributes: {
                                style: "text-align: center; font-weight:normal"
                            },
                            headerAttributes: {
                                style: "text-align: center"
                            }
                        },
                        {
                            field: "ItemName", title: "Item Name", width: "110px", attributes: {

                                style: "text-align: left; font-weight:normal;float: left; display: flex;margin-top: 3px; "
                            },
                            template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
                        },
                        {
                            field: "SubSectorName", title: "Sub Sector", width: "60px", attributes: {
                                style: "text-align: left; font-weight:normal"
                            },
                        },
                        {
                            field: "PublishedDateAsString", title: "Last Updated", width: "50px", attributes: {

                                style: "text-align: center; font-weight:normal"
                            },
                            //  template: '# if (PublishedDate == null) {#<span>-</span>#} else{#<span>#: kendo.toString(PublishedDate, "dd-MMM-yy")#</span>#}#',
                            headerAttributes: {
                                style: "text-align: center;"
                            }
                        },
                        {
                            field: "Edit", title: "Edit", width: "25px",
                            attributes: {
                                style: "text-align: center; font-weight:normal;cursor: pointer;"
                            },
                            headerAttributes: {
                                style: "text-align: center; vertical-align:middle"
                            },
                            command: [
                                {
                                    name: 'Edit',
                                    template: function (dataItem) {

                                        var html = `<a href="#" onClick="itemGridEdit(this)" class="fa fa-pen" title="Click to Edit Item Details" style='color:#8f9090; margin-left:8px'></ a>`;
                                        return html;
                                    },
                                    width: "35px",

                                }
                            ]

                        },
                        {
                            field: "MapDishes", title: "Item-Dish", width: "35px",
                            attributes: {
                                style: "text-align: center; font-weight:normal"
                            },
                            headerAttributes: {
                                style: "text-align: center; vertical-align:middle"
                            },
                            command: [
                                {
                                    title: 'Map',
                                    name: 'Map',
                                    //template: "<div class='fas fa-link'></div>",
                                    click: function (e) {
                                        if (dishdata.length < 1) {
                                            populateDishData();
                                        }

                                        var gridObj = $("#gridItem").data("kendoGrid");
                                        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                        if (tr.ItemCode > 80000) { return; }
                                        Utility.Loading();
                                        setTimeout(function () {
                                            ingredientObject = [];
                                            masterGroupList = [];
                                            finalModelExitsDishCategory = [];



                                            itemObject.ItemName = tr.ItemName;
                                            itemObject.ID = tr.ID;
                                            itemObject.Code = tr.ItemCode;
                                            itemObject.ItemPrice = 100;
                                            itemObject.DietCategorID = tr.DietCategory_ID;
                                            selItemDietCategory = tr.DietCategoryCode;
                                            itemObject.ImagePath = 'ItemImages/' + '/' + tr.ImageName;
                                            $("#itemCode").text(tr.ItemCode);
                                            $("#itemName").text(tr.ItemName).focus();
                                            $("#fpname").text(tr.FoodProgramName);
                                            $("#fptype").text(tr.Foo);
                                            $("#vcat").text(tr.VisualCategoryName);
                                            $("#iname").text(tr.ItemName + " (Item Code: " + tr.ItemCode + ")");
                                            if (tr.ConceptType2Code == 'CT2-00001' || tr.ConceptType2Code == 'CT2-00003') {
                                                $("#fptype").text("Curated");
                                                flagCurated = 1;
                                            } else {
                                                $("#fptype").text("Non-Curated");
                                                flagCurated = 0;
                                            }
                                            $("#fptype").hide();
                                            $("#fpname").hide();
                                            $("#pipeSpan").hide();
                                            $("#itemName").attr('title', tr.ItemName);
                                            if (tr.DietCategory_ID == 3) {
                                                $("#itemDietCategoryStatus").removeClass("statusNonVeg statusEgg");
                                                $("#itemDietCategory").removeClass("squareNonVeg squareEgg");
                                                $("#itemDietCategoryStatus").addClass("statusVeg");
                                                $("#itemDietCategory").addClass("squareVeg")
                                            } else if (tr.DietCategory_ID == 2) {
                                                $("#itemDietCategoryStatus").removeClass("statusVeg statusEgg");
                                                $("#itemDietCategory").removeClass("squareVeg squareEgg");
                                                $("#itemDietCategory").addClass("squareNonVeg")
                                                $("#itemDietCategoryStatus").addClass("statusNonVeg")
                                            } else {
                                                $("#itemDietCategoryStatus").removeClass("statusNonVeg statusVeg");
                                                $("#itemDietCategory").removeClass("squareNonVeg squareVeg");
                                                $("#itemDietCategory").addClass("squareEgg")
                                                $("#itemDietCategoryStatus").addClass("statusEgg")
                                            }
                                            $("#itemPrice").text(itemObject.ItemPrice);
                                            $("#ItemMaster").hide();
                                            $("#ConfigMaster").show();
                                            $("#imageMenuItem").attr('src', itemObject.ImagePath);
                                            $("#topHeading").text("Sector Menu Item - Dish Mapping");

                                            //hare krishna
                                            $("#dishCategory").empty();
                                            //var multiarray = [];
                                            masterQty = [];
                                            currentMasterQtyList = [];
                                            currentMasterList = [];
                                            masterGroupList = [];
                                            var dropdownlist = $("#inputmenuitem").data("kendoDropDownList");
                                            dropdownlist.select(0);
                                            CreateDishCategoryOutline(tr.ItemCode, tr.ID);

                                            //if (user.UserRoleId === 1) {
                                            //    $("#btnSubmitRecipe").css('display', 'inline');
                                            //    // $("#btnRecipeCancel").css('display', 'inline');
                                            //    $("#btnSubmitClose").css("display", "inline");
                                            //} else {
                                            //    $("#btnSubmitRecipe").css('display', 'none');
                                            //    //  $("#btnRecipeCancel").css('display', 'none');
                                            //    $("#btnSubmitClose").css("display", "none");
                                            //}
                                            Utility.UnLoading();
                                        }, 2000)
                                    }
                                }
                            ]

                        },
                        {
                            field: "Status", title: "Status", width: "35px", attributes: {
                                class: "mycustomstatus",
                                style: "text-align: center; font-weight:normal"
                            },
                            template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Mapped#}#',
                            headerAttributes: {

                                style: "text-align: center"
                            },
                        },
                        {
                            field: "DietCategoryName", title: "", width: "1px", attributes: {

                                style: "text-align: center; font-weight:normal;"
                            },
                            headerAttributes: {
                                style: "text-align: center; width:1px; display:none;"
                            },
                            template: '<span class="itemname"></span>',
                        }
                    ],
                    dataSource: {
                        transport: {
                            read: function (options) {

                                var varCodes = "";

                                HttpClient.MakeSyncRequest(CookBookMasters.GetItemDataList, function (result) {
                                    Utility.UnLoading();

                                    if (result != null) {
                                        options.success(result);


                                        var dataSource = result;
                                        menuitemdata = [];
                                        menuitemdata.push({ "value": 0, "text": "Select" });
                                        for (var i = 0; i < dataSource.length; i++) {
                                            menuitemdata.push({
                                                "value": dataSource[i].ID, "text": dataSource[i].ItemName, "ItemCode": dataSource[i].ItemCode
                                            });
                                        }
                                        populateMenuItemDropdown();
                                    }
                                    else {
                                        options.success("");
                                    }
                                }, null
                                    //{
                                    //filter: mdl
                                    //}
                                    , true);
                            }
                        },
                        schema: {
                            parse: function (data) {
                                var events = [];
                                for (var i = 0; i < data.length; i++) {
                                    var event = data[i];
                                    event.PublishedDate = kendo.toString(event.PublishedDate, 'dd-MMM-yy');
                                    events.push(event);
                                }
                                return events;
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ItemCode: { type: "string" },
                                    ItemName: { type: "string" },
                                    DietCategoryName: { type: "string" },
                                    VisualCategoryName: { type: "string" },
                                    FoodProgramName: { type: "string" },
                                    HSNCode: { type: "string" },
                                    CGST: { type: "string" },
                                    SGST: { type: "string" },
                                    CESS: { type: "string" },
                                    Status: { type: "string" },
                                    ApplicableTo: { type: "string" },
                                    ConceptType2Name: { type: "string" },
                                    PublishedDate: { type: "date" },
                                    CreatedOn: { type: "date" },
                                    PublishedDateAsString: { type: "string" },
                                    SubSectorName: { type: "string" },
                                }

                            }
                        },
                        pageSize: 150,
                    },
                    columnResize: function (e) {
                        var grid = gridVariable.data("kendoGrid");
                        e.preventDefault();
                    },
                    //  height: 490,
                    noRecords: {
                        template: "No Records Available"
                    },
                    dataBound: function (e) {
                        var grid = this;

                        grid.tbody.find("tr[role='row']").each(function () {
                            var model = grid.dataItem(this);

                            if (model.ItemCode > 80000) {
                                $(this).find(".k-grid-Map").addClass("k-state-disabled");
                            }
                        });

                        var view = this.dataSource.view();
                        for (var i = 0; i < view.length; i++) {
                            if (view[i].Status == 1) {
                                this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                    .find(".mycustomstatus").css("background-color", "#ffe1e1");

                            }
                            else if (view[i].Status == 2) {
                                this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                    .find(".mycustomstatus").css("background-color", "#ffffbf");
                            }
                            else {
                                this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                    .find(".mycustomstatus").css("background-color", "#d9ffb3");
                            }

                        }
                        var items = e.sender.items();

                        //items.each(function (e) {

                        //    if (user.UserRoleId == 1) {
                        //        $(this).find('.k-grid-Edit').text("Edit");
                        //    } else {
                        //        $(this).find('.k-grid-Edit').text("View");
                        //    }
                        //});
                    },
                    change: function (e) {
                    },
                    excelExport: function onExcelExport(e) {
                        var sheet = e.workbook.sheets[0];
                        var grid = $("#gridItem").data("kendoGrid");
                        var data = grid.dataSource._data
                        var cols = Object.keys(data[0])
                        var columns = cols.filter(function (col) {
                            if (col == "HSNCode" || col == "SGST" || col == "CGST" || col == "CESS" || col == "VisualCategoryName" || col == "DietCategoryName" || col == "FoodProgramName" || col == "ApplicableTo" || col == "ItemName" || col == "ItemCode" || col == "SubSectorName")
                                return col;
                        });
                        var columns1 = columns.map(function (col) {
                            return {
                                value: col,
                                autoWidth: true,
                                background: "#7a7a7a",
                                color: "#fff"
                            };
                        });

                        var rows = [{ cells: columns1, type: "header" }];

                        for (var i = 0; i < data.length; i++) {
                            var rowCells = [];
                            for (var j = 0; j < columns.length; j++) {

                                var cellValue = data[i][columns[j]];
                                rowCells.push({ value: cellValue });
                            }
                            rows.push({ cells: rowCells, type: "data" });
                        }
                        sheet.rows = rows;
                    }
                    //})
                });
            }
            else {
                var gridVariable = $("#gridItem");
                gridVariable.html("");
                gridVariable.kendoGrid({
                    sortable: true,
                    filterable: {
                        extra: true,
                        operators: {
                            string: {
                                contains: "Contains",
                                startswith: "Starts with",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                doesnotcontain: "Does not contain",
                                endswith: "Ends with"
                            }
                        }
                    },
                    groupable: false,
                    pageable: true,
                    columns: [
                        {
                            field: "SectorName", title: "Sector Name", width: "40px", attributes: {

                                style: "text-align: left; font-weight:normal"
                            },

                        },
                        {
                            field: "ItemCode", title: "Item Code", width: "30px", attributes: {
                                style: "text-align: center; font-weight:normal"
                            },
                            headerAttributes: {
                                style: "text-align: center"
                            }
                        },
                        {
                            field: "ItemName", title: "Item Name", width: "70px", attributes: {

                                style: "text-align: left; font-weight:normal;float: left; display: flex;margin-top: 3px; "
                            },
                            template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
                        },
                        
                        
                        {
                            field: "FoodProgramName", title: "Food Program", width: "60px", attributes: {
                                style: "text-align: left; font-weight:normal"
                            },
                        },
                        {
                            field: "ConceptType2Name", title: "Type", width: "60px", attributes: {
                                style: "text-align: left; font-weight:normal"
                            },
                        },
                        {
                            field: "ApplicableTo", title: "Usability", width: "35px", attributes: {
                                style: "text-align: center; font-weight:normal"
                            },
                        },
                        
                        
                        {
                            field: "MapDishes", title: "Item-Dish", width: "35px",
                            attributes: {
                                style: "text-align: center; font-weight:normal"
                            },
                            headerAttributes: {
                                style: "text-align: center; vertical-align:middle"
                            },
                            command: [
                                {
                                    title: 'View',
                                    name: 'View',
                                    //template: "<div class='fas fa-link'></div>",
                                    click: function (e) {
                                        var gridObj = $("#gridItem").data("kendoGrid");
                                        if (dishdata.length < 1) {
                                            populateDishData();
                                        }
                                        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                       
                                        if (tr.ItemCode > 80000) { return; }
                                        Utility.Loading();
                                        setTimeout(function () {
                                            ingredientObject = [];
                                            masterGroupList = [];
                                            finalModelExitsDishCategory = [];
                                            itemObject.ItemName = tr.ItemName;
                                            itemObject.ID = tr.ID;
                                            itemObject.Code = tr.ItemCode;
                                            itemObject.ItemPrice = 100;
                                            itemObject.DietCategorID = tr.DietCategory_ID;
                                            selItemDietCategory = tr.DietCategoryCode;
                                            itemObject.ImagePath = 'ItemImages/' + '/' + tr.ImageName;
                                            $("#itemCode").text(tr.ItemCode);
                                            $("#itemName").text(tr.ItemName).focus();
                                            $("#fpname").text(tr.FoodProgramName);
                                            $("#fptype").text(tr.Foo);
                                            $("#vcat").text(tr.VisualCategoryName);
                                            $("#iname").text(tr.ItemName + " (Item Code: " + tr.ItemCode + ")");
                                            if (tr.ConceptType2Code == 'CT2-00001' || tr.ConceptType2Code == 'CT2-00003') {
                                                $("#fptype").text("Curated");
                                                flagCurated = 1;
                                            } else {
                                                $("#fptype").text("Non-Curated");
                                                flagCurated = 0;
                                            }
                                            $("#itemName").attr('title', tr.ItemName);
                                            if (tr.DietCategory_ID == 3) {
                                                $("#itemDietCategoryStatus").removeClass("statusNonVeg statusEgg");
                                                $("#itemDietCategory").removeClass("squareNonVeg squareEgg");
                                                $("#itemDietCategoryStatus").addClass("statusVeg");
                                                $("#itemDietCategory").addClass("squareVeg")
                                            } else if (tr.DietCategory_ID == 2) {
                                                $("#itemDietCategoryStatus").removeClass("statusVeg statusEgg");
                                                $("#itemDietCategory").removeClass("squareVeg squareEgg");
                                                $("#itemDietCategory").addClass("squareNonVeg")
                                                $("#itemDietCategoryStatus").addClass("statusNonVeg")
                                            } else {
                                                $("#itemDietCategoryStatus").removeClass("statusNonVeg statusVeg");
                                                $("#itemDietCategory").removeClass("squareNonVeg squareVeg");
                                                $("#itemDietCategory").addClass("squareEgg")
                                                $("#itemDietCategoryStatus").addClass("statusEgg")
                                            }
                                            $("#itemPrice").text(itemObject.ItemPrice);
                                            $("#ItemMaster").hide();
                                            $("#ConfigMaster").show();
                                            $("#imageMenuItem").attr('src', itemObject.ImagePath);
                                            $("#topHeading").text("Sector Menu Item - Dish Mapping");

                                            //hare krishna
                                            $("#dishCategory").empty();
                                            //var multiarray = [];
                                            masterQty = [];
                                            currentMasterQtyList = [];
                                            currentMasterList = [];
                                            masterGroupList = [];
                                            var dropdownlist = $("#inputmenuitem").data("kendoDropDownList");
                                            dropdownlist.select(0);
                                            CreateDishCategoryOutline(tr.ItemCode, tr.ID, tr.SectorName);

                                            //if (user.UserRoleId === 1) {
                                            //    $("#btnSubmitRecipe").css('display', 'inline');
                                            //    // $("#btnRecipeCancel").css('display', 'inline');
                                            //    $("#btnSubmitClose").css("display", "inline");
                                            //} else {
                                            //    $("#btnSubmitRecipe").css('display', 'none');
                                            //    //  $("#btnRecipeCancel").css('display', 'none');
                                            //    $("#btnSubmitClose").css("display", "none");
                                            //}
                                            Utility.UnLoading();
                                        }, 2000)
                                    }
                                }
                            ]

                        },
                        {
                            field: "Status", title: "Status", width: "35px", attributes: {
                                class: "mycustomstatus",
                                style: "text-align: center; font-weight:normal"
                            },
                            template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Mapped#}#',
                            headerAttributes: {

                                style: "text-align: center"
                            },
                        },
                        {
                            field: "DietCategoryName", title: "", width: "1px", attributes: {

                                style: "text-align: center; font-weight:normal;"
                            },
                            headerAttributes: {
                                style: "text-align: center; width:1px; display:none;"
                            },
                            template: '<span class="itemname"></span>',
                        }
                    ],
                    dataSource: {
                        transport: {
                            read: function (options) {

                                var varCodes = "";

                                HttpClient.MakeSyncRequest(CookBookMasters.GetItemDataList, function (result) {
                                    Utility.UnLoading();

                                    if (result != null) {
                                        options.success(result);


                                        var dataSource = result;
                                        menuitemdata = [];
                                        menuitemdata.push({ "value": 0, "text": "Select" });
                                        for (var i = 0; i < dataSource.length; i++) {
                                            menuitemdata.push({
                                                "value": dataSource[i].ID, "text": dataSource[i].ItemName, "ItemCode": dataSource[i].ItemCode
                                            });
                                        }
                                        populateMenuItemDropdown();
                                    }
                                    else {
                                        options.success("");
                                    }
                                }, null
                                    //{
                                    //filter: mdl
                                    //}
                                    , true);
                            }
                        },
                        schema: {
                            parse: function (data) {
                                var events = [];
                                for (var i = 0; i < data.length; i++) {
                                    var event = data[i];
                                    event.PublishedDate = kendo.toString(event.PublishedDate, 'dd-MMM-yy');
                                    events.push(event);
                                }
                                return events;
                            },
                            model: {
                                id: "ID",
                                fields: {
                                    ItemCode: { type: "string" },
                                    ItemName: { type: "string" },
                                    DietCategoryName: { type: "string" },
                                    VisualCategoryName: { type: "string" },
                                    FoodProgramName: { type: "string" },
                                    HSNCode: { type: "string" },
                                    CGST: { type: "string" },
                                    SGST: { type: "string" },
                                    CESS: { type: "string" },
                                    Status: { type: "string" },
                                    ApplicableTo: { type: "string" },
                                    ConceptType2Name: { type: "string" },
                                    PublishedDate: { type: "date" },
                                    CreatedOn: { type: "date" },
                                    PublishedDateAsString: { type: "string" },
                                    SectorName: { type: "string" },
                                }

                            }
                        },
                        pageSize: 150,
                    },
                    columnResize: function (e) {
                        var grid = gridVariable.data("kendoGrid");
                        e.preventDefault();
                    },
                    //  height: 490,
                    noRecords: {
                        template: "No Records Available"
                    },
                    dataBound: function (e) {
                        var grid = this;

                        grid.tbody.find("tr[role='row']").each(function () {
                            var model = grid.dataItem(this);

                            if (model.ItemCode > 80000) {
                                $(this).find(".k-grid-Map").addClass("k-state-disabled");

                            }
                        });

                        var view = this.dataSource.view();
                        for (var i = 0; i < view.length; i++) {
                            if (view[i].Status == 1) {
                                this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                    .find(".mycustomstatus").css("background-color", "#ffe1e1");

                            }
                            else if (view[i].Status == 2) {
                                this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                    .find(".mycustomstatus").css("background-color", "#ffffbf");
                            }
                            else {
                                this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                    .find(".mycustomstatus").css("background-color", "#d9ffb3");
                            }

                        }
                        var items = e.sender.items();

                        //items.each(function (e) {

                        //    if (user.UserRoleId == 1) {
                        //        $(this).find('.k-grid-Edit').text("Edit");
                        //    } else {
                        //        $(this).find('.k-grid-Edit').text("View");
                        //    }
                        //});
                    },
                    change: function (e) {
                    },
                    excelExport: function onExcelExport(e) {
                        var sheet = e.workbook.sheets[0];
                        var grid = $("#gridItem").data("kendoGrid");
                        var data = grid.dataSource._data
                        var cols = Object.keys(data[0])
                        var columns = cols.filter(function (col) {
                            if (col == "SectorName" || col == "HSNCode" || col == "SGST" || col == "CGST" || col == "CESS" || col == "VisualCategoryName" || col == "DietCategoryName" || col == "FoodProgramName" || col == "ApplicableTo" || col == "ItemName" || col == "ItemCode" || col == "Status")
                                return col;
                        });
                        var columns1 = columns.map(function (col) {
                            return {
                                value: col,
                                autoWidth: true,
                                background: "#7a7a7a",
                                color: "#fff"
                            };
                        });

                        var rows = [{ cells: columns1, type: "header" }];

                        for (var i = 0; i < data.length; i++) {
                            var rowCells = [];
                            for (var j = 0; j < columns.length; j++) {

                                var cellValue = data[i][columns[j]];
                                rowCells.push({ value: cellValue });
                            }
                            rows.push({ cells: rowCells, type: "data" });
                        }
                        sheet.rows = rows;
                    }
                    //})
                });
            }


          //  populateDishData();

            //$(".k-label")[0].innerHTML.replace("items", "records");
        }


        function populateDishData() {
            Utility.Loading();
            HttpClient.MakeSyncRequest(CookBookMasters.GetDishList, function (data) {
                var dataSource = data;
                dishdata = [];
                dishdata.push({ "value": 0, "text": "Select Dish(s)", "price": 0.0 });
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i].DishCode == null || dataSource[i].DishCode.trim()=="")
                        continue;
                    dishdata.push({
                        "value": dataSource[i].ID, "alias": dataSource[i].DishAlias, "text": dataSource[i].Name, "DishCode": dataSource[i].DishCode,
                        "DishCategoryID": dataSource[i].DishCategory_ID, "DishCategoryName": dataSource[i].DishCategoryName,
                        "price": dataSource[i].CostPerKG, "tooltip": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
                        "DietCategoryCode": dataSource[i].DietCategoryCode, "CuisineName": dataSource[i].CuisineName
                    });
                }
                Utility.UnLoading();
            }, null, false);
        }
    
        $("#AddNew").on("click", function () {
            var model;
            if (visualcategorydata.length<1)
                AddEdit();
            datamodel = null;
            var dialog = $("#MenuCwindowEdit").data("kendoWindow");
            //if (user.SectorNumber == "30")
            //    dialog.height = "538px";
            //else
            //    dialog.height = "200px";
            //$("#MenuCwindowEdit").kendoWindow({
            //    height: "200px"
            //});
            dialog.open().element.closest(".k-window").css({
                top: 167,
                left: 558

            });
            dialog.center();

            if (user.SectorNumber == "30") {
                $(".coreonly").show();
                $(".googleonly").hide();
                $("#itemid").val("");
                $("#itemid").text("");
                $("#inputitemcode").val("");
                $("#inputitemname").val("").focus();
                $("#inputfoodprogram").data('kendoDropDownList').value("Select");
                $("#inputdietcategory").data('kendoDropDownList').value("Select");
                $("#inputvisualcategory").data('kendoDropDownList').value("Select");
                $("#inputhsncode").val("");
                $("#inputcgst").val("");
                $("#inputsgst").val("");
                $("#inputugst").val("");
                $("#inputcess").val("");
                $("#inputapplicableTo").val("");
                $("#inputserveware").data('kendoDropDownList').value("Select");
                $("#inputitemtype1").data('kendoDropDownList').value("Select");
                $("#inputitemtype2").data('kendoDropDownList').value("Select");
                $("#inputcontype1").data('kendoDropDownList').value("Select");
                $("#inputcontype2").data('kendoDropDownList').value("Select");
                $("#inputcontype3").data('kendoDropDownList').value("Select");
                $("#inputcontype4").data('kendoDropDownList').value("Select");
                $("#inputcontype5").data('kendoDropDownList').value("Select");
                $("#file").val("");
                //$("#image").attr('src', '../ItemImages/default.png');
                $("#image").attr('src', './ItemImages/default.png');
                $("#inputitemcode").removeAttr("disabled");
            }
            else if (user.SectorNumber == "10"){
                $(".coreonly").hide();
                $("#inputitemcode").attr("disabled", "disabled");
                $("#itemid").text("");
                $("#inputitemcode").val("");
                $("#inputitemname").val("").focus();
                $("#inputdietcategory").data('kendoDropDownList').value("Select");
                if (isSubSectorApplicable) {
                    $("#inputsubsector").data('kendoDropDownList').value("Select");
                    $(".googleonly").show();
                }
                else {
                    $(".googleonly").hide();
                }
            }
            else {
                $(".coreonly").hide();
                $(".googleonly").hide();
                $("#inputitemcode").attr("disabled", "disabled");
                $("#itemid").text("");
                $("#inputitemcode").val("");
                $("#inputitemname").val("").focus();
                $("#inputdietcategory").data('kendoDropDownList').value("Select");
            }
            dialog.title("New Item Creation");
        })

        $("#btnCancel").on("click", function () {
            // $(".k-overlay").hide(); 
            Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
                function () {
                    $(".k-overlay").hide();
                    var orderWindow = $("#MenuCwindowEdit").data("kendoWindow");
                    orderWindow.close();
                },
                function () {
                }
            );
            $("#topHeading").text("Sector Menu Configuration");
        });

    $("#btnSubmit").click(function () {
        console.log('save item');
            var formData = new FormData();
            var file = document.getElementById("file").files[0];
            if (file != undefined) {
                formData.append('itemCode', $("#inputitemcode").val());
                formData.append('folderName', "ItemImages");
                formData.append("file", file);
                var flagImage = 0;
                $.ajax({
                    type: "POST",
                    url: './Item/UploadFile',
                    data: formData,
                    contentType: false,
                    processData: false,
                    async: false,
                    success: function (response) {
                        flagImage = 1;
                    },
                    error: function (error) {
                        Utility.UnLoading();
                        toastr.error("Problem while uploading the Item Image on Server. Please contact Administrator");
                    }
                });
            }

            if ($("#inputitemname").val() === "") {
                toastr.error("Please input Item Name");
                $("#inputitemname").focus();
                return;
            }

            if ($("#inputdietcategory").val() === "Select") {
                toastr.error("Please select Diet Category");
                $("#inputdietcategory").focus();
                return;
            }
            if (isSubSectorApplicable && user.SectorNumber == "10" && $("#inputsubsector").val() === "Select") {
                toastr.error("Please select Sub Sector");
                $("#inputsubsector").focus();
                return;
            }
            else {
                var model;
                if (datamodel != null) {
                    var model = datamodel;
                    model.IsActive = datamodel.IsActive;
                    model.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
                    model.CreatedBy = datamodel.CreatedBy;
                    model.ItemCode = $("#inputitemcode").val();
                    model.ItemName = $("#inputitemname").val();
                    model.FoodProgram_ID = $("#inputfoodprogram").val();
                    model.VisualCategory_ID = $("#inputvisualcategory").val();
                    model.DietCategory_ID = $("#inputdietcategory").val();
                    model.SubSectorCode = $("#inputsubsector").val();
                    model.HSNCode = $("#inputhsncode").val();
                    model.CGST = $("#inputcgst").val();
                    model.SGST = $("#inputsgst").val();
                    model.UGST = $("#inputugst").val();
                    model.CESS = $("#inputcess").val();
                    model.Serveware_ID = $("#inputserveware").val();
                    model.ItemType1_ID = $("#inputitemtype1").val();
                    model.ItemType2_ID = $("#inputitemtype2").val();
                    model.ConceptType1_ID = $("#inputcontype1").val();
                    model.ConceptType2_ID = $("#inputcontype2").val();
                    model.ConceptType3_ID = $("#inputcontype3").val();
                    model.ConceptType4_ID = $("#inputcontype4").val();
                    model.ConceptType5_ID = $("#inputcontype5").val();
                    model.ApplicableTo= $("#inputapplicableTo").val();
                    if (flagImage) {
                        model.ImageName = model.ItemCode + ".png";
                        flagImage = 0;
                    }
                }
                else {
                    var iCode = $("#inputitemcode").val().trim();
                    if (user.SectorNumber == "30") {
                        if (iCode == null || iCode == "") {
                            toastr.error("Kindly provide Itemcode");
                            return;
                        }
                        if (menuitemdata.filter(item => item.ItemCode == iCode).length > 0) {
                            toastr.error("Itemcode already exits");
                            return;
                        }

                    }
                    
                    if($("#input"))
                    model = {
                        "ItemCode": $("#inputitemcode").val(),
                        "ItemName":$("#inputitemname").val(),
                        "FoodProgram_ID": $("#inputfoodprogram").val(),
                        "DietCategory_ID": $("#inputdietcategory").val(),
                        "SubSectorCode": $("#inputsubsector").val(),
                        "VisualCategory_ID": $("#inputvisualcategory").val(),
                        "HSNCode": $("#inputhsncode").val(),
                        "CGST": $("#inputcgst").val(),
                        "SGST": $("#inputsgst").val(),
                        "UGST": $("#inputugst").val(),
                        "CESS": $("#inputcess").val(),
                        "Serveware_ID": $("#inputserveware").val(),
                        "ItemType1_ID": $("#inputitemtype1").val(),
                        "ItemType2_ID": $("#inputitemtype2").val(),
                        "ConceptType1_ID": $("#inputcontype1").val(),
                        "ConceptType2_ID": $("#inputcontype2").val(),
                        "ConceptType3_ID": $("#inputcontype3").val(),
                        "ConceptType4_ID": $("#inputcontype4").val(),
                        "ConceptType5_ID": $("#inputcontype5").val(),
                        "ApplicableTo": $("#inputapplicableTo").val(),
                    }
                }

                if (user.SectorNumber != "30") {
                    model.FoodProgram_ID = 1;
                }
                if (model.ItemType1_ID != null && model.ItemType1_ID != "") {
                    model.ItemType1Code = $("#inputitemtype1").data("kendoDropDownList").dataItem().ItemType1Code;
                }
                if (model.ItemType2_ID != null && model.ItemType2_ID != "") {
                    model.ItemType2Code = $("#inputitemtype2").data("kendoDropDownList").dataItem().ItemType2Code;
                }
                if (model.Serveware_ID != null && model.Serveware_ID != "") {
                    model.ServewareCode = $("#inputserveware").data("kendoDropDownList").dataItem().ServewareCode;
                }
                if (model.ConceptType1_ID != null && model.ConceptType1_ID != "") {
                    model.ConceptType1Code = $("#inputcontype1").data("kendoDropDownList").dataItem().ConceptType1Code;
                }
                if (model.ConceptType2_ID != null && model.ConceptType2_ID != "") {
                    model.ConceptType2Code = $("#inputcontype2").data("kendoDropDownList").dataItem().ConceptType2Code;
                }
                if (model.ConceptType3_ID != null && model.ConceptType3_ID != "") {
                    model.ConceptType3Code = $("#inputcontype3").data("kendoDropDownList").dataItem().ConceptType3Code;
                }
                if (model.ConceptType4_ID != null && model.ConceptType4_ID != "") {
                    model.ConceptType4Code = $("#inputcontype4").data("kendoDropDownList").dataItem().ConceptType4Code;
                }
                if (model.ConceptType5_ID != null && model.ConceptType5_ID != "") {
                    model.ConceptType5Code = $("#inputcontype5").data("kendoDropDownList").dataItem().ConceptType5Code;
                }
                model.DietCategory_ID = $("#inputdietcategory").val();
                if (model.DietCategory_ID != null && model.DietCategory_ID != "") {
                    model.DietCategoryCode = $("#inputdietcategory").data("kendoDropDownList").dataItem().DietCategoryCode;
                }
                if (model.FoodProgram_ID != null && model.FoodProgram_ID != "") {
                    model.FoodProgramCode = $("#inputfoodprogram").data("kendoDropDownList").dataItem().FoodProgramCode;
                }
                if (model.VisualCategory_ID != null && model.VisualCategory_ID != "") {
                    model.VisualCategoryCode = $("#inputvisualcategory").data("kendoDropDownList").dataItem().VisualCategoryCode;
                }

                if (user.SectorNumber != "30") {
                    model.FoodProgram_ID = 1;
                    model.FoodProgramCode = "FPC-00001";
                } else {
                    if (model.FoodProgram_ID != null && model.FoodProgram_ID != "") {
                        model.FoodProgramCode = $("#inputfoodprogram").data("kendoDropDownList").dataItem().FoodProgramCode;
                    }
                }

                $("#btnSubmit").attr('disabled', 'disabled');
                HttpClient.MakeRequest(CookBookMasters.SaveItemData, function (result) {
                    console.log(result);
                    if (result == false) {
                        $('#btnSubmit').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        $("#inputitemname").focus();
                    } else if (result=="Duplicate") {
                        $('#btnSubmit').removeAttr("disabled");
                        $("#inputitemname").focus();
                        toastr.error("Duplicate Item Name, Please Correct it.");

                    }
                    else {
                        //$(".k-overlay").hide();
                        var orderWindow = $("#MenuCwindowEdit").data("kendoWindow");
                        orderWindow.close();
                        $('#btnSubmit').removeAttr("disabled");
                        var msg = "";
                        if (model.ID > 0) {
                            msg = "Item updated successfully";
                            //toastr.success(msg);

                        } else {
                            msg = "Item created successfully";
                            //toastr.success(msg);
                        }

                        $("#gridItem").data("kendoGrid").dataSource.data([]);
                        $("#gridItem").data("kendoGrid").dataSource.read();

                        toastr.success(msg);
                       
                        $("#topHeading").text("Sector Menu Configuration");
                        //toastr.success(msg);
                    }
                }, {
                    model: model

                }, true);
            }
        });

        $('#btnUpload').click(function () {

            $('#txtFilePath').removeClass('validationError');
            $('#file').val('').trigger('click');
        });

        function validateImportFile(filename) {

            if (filename != '') {
                var fileExtension = ['png', 'jpg'];
                if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == -1) {
                    Utility.Page_Alert(ExtensionAllowedMessage);
                    return false;
                }
            }
            return true;
        };

        $('#file').change(function () {

            var that = this;
            if (validateImportFile(that.value)) {
                //$('#txtFilePath').val(that.value);
                $('#txtFilePath').val(that.files[0].name);
                if (that.files[0]) {
                    var uploadimg = new FileReader();
                    uploadimg.onload = function (displayimg) {
                        $("#image").attr('src', displayimg.target.result);
                    }
                    uploadimg.readAsDataURL(that.files[0]);
                }
            }
            else {
                //ImportReport.refreshPage();
            }
        });

        $("#btnSubmitClose").click(function () {
            isPublishedClicked = true;
            RecreateQuantity();
            SaveModel();
           

            if (flagCurated && alertFlag) {
                return;
            }
            if (finalModelDishCategory.length < 1) {
                toastr.error("Kindly add at least one dish categories ");
                return;
            }

            if (finalModelDishCategory.length > 0) {
                HttpClient.MakeRequest(CookBookMasters.SaveItemDishCategoryMappingDataList, function (result) {
                    if (result == false) {
                        toastr.error("Some error occured, please try again.");
                    }
                    else {
                        if (finalModel.length == 0) {
                            finalModel.push({
                                "ID": 0,
                                "Item_ID": itemObject.ID,
                                "ItemCode": itemObject.Code,
                                "CreatedBy": user.UserId,
                                "CreatedOn": Utility.CurrentDate(),
                                "Status": isPublishedClicked ? 3 : 2
                            });
                        }
                            HttpClient.MakeRequest(CookBookMasters.SaveItemDishMappingDataList, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again.");
                                }
                            }, {
                                model: finalModel

                            },true);
                        
                        $(".k-overlay").hide();
                        $("#ItemMaster").show();
                        $("#ConfigMaster").hide();
                        $("#topHeading").text("Sector Menu Configuration");
                        toastr.success("Menu Item-Dish mapping complete.");
                       // populateItemGrid();
                        $("#gridItem").data("kendoGrid").dataSource.data([]);
                        $("#gridItem").data("kendoGrid").dataSource.read();
                    }
                }, {
                    model: finalModelDishCategory
                },true);
            }
            else {
                toastr.error("Dish Category(s) cannot be left blank for Curated Items. Please add the appropriate Dishes");
                return;
            }
        });

        $("#btnSubmitRecipe").click(function () {
            isPublishedClicked = false;
            console.log("ItemObject");
            console.log(itemObject);
            RecreateQuantity();
            SaveModel();
            // return;
            console.log(currentMasterQtyList);
            if (flagCurated && alertFlag) {
                return;
            }
            if (finalModelDishCategory.length <1) {
                toastr.error("Kindly add at least one dish categories ");
                return;
            }
            if (finalModel.length == 0) {
                finalModel.push({
                    "ID": 0,
                    "Item_ID": itemObject.ID,
                    "ItemCode": itemObject.Code,
                    "CreatedBy": user.UserId,
                    "CreatedOn": Utility.CurrentDate(),
                    "Status": isPublishedClicked ? 3 : 2
                })

            }
            HttpClient.MakeRequest(CookBookMasters.SaveItemDishMappingDataList, function (result) {
                    if (result == false) {
                        toastr.error("Some error occured, please try again.");
                    }
                    else {
                        $(".k-overlay").hide();

                        Toast.fire({
                            type: 'success',
                            title: 'Item Configuration has been saved successfully'
                        });
                        populateItemGrid();
                        //toastr.success("Item Configuration has been saved successfully");
                    }
                }, {
                    model: finalModel

                },true);
            
            if (finalModelDishCategory.length > 0) {
                HttpClient.MakeRequest(CookBookMasters.SaveItemDishCategoryMappingDataList, function (result) {
                    if (result == false) {
                        toastr.error("Some error occured, please try again.");
                    }
                    else {
                        if (!finalModel.length) {
                            $(".k-overlay").hide();

                            Toast.fire({
                                type: 'success',
                                title: 'Item Configuration has been saved successfully'
                            });
                        }
                    }
                }, {
                    model: finalModelDishCategory
                },true);
            }
            else {
                //  alert("Kindly Add dish");
                toastr.error("Please select the Dish(s) under each category");
                return;
            }
        });
        $("#btnRecipeBack").click(function () {
            $("#ItemMaster").show();
            $("#ConfigMaster").hide();
            $("#topHeading").text("Sector Menu Configuration");
        });

        $("#btnRecipeCancel").click(function () {
            $("#ItemMaster").show();
            $("#ConfigMaster").hide();
            $("#topHeading").text("Sector Menu Configuration");
        });



        $("#collapseIcon").click(function () {
            collapseAll();
        });

        $("#expandIcon").click(function () {
            expandAll();
        });
    });

   
//});
function populateDishDataDropdown(dishid, dishcategory, subdishid) {
    //Filter for data based on category
    var localdishdata = [];

    if (selItemDietCategory == "DTC-00003") {
        localdishdata = dishdata.filter(item => item.DishCategoryName == dishcategory && item.DietCategoryCode == selItemDietCategory);
    }
    else if (selItemDietCategory == "DTC-00001") {
        localdishdata = dishdata.filter(item => item.DishCategoryName == dishcategory && (item.DietCategoryCode == selItemDietCategory || item.DietCategoryCode == "DTC-00003"));
    }
    else {
        localdishdata = dishdata.filter(item => item.DishCategoryName == dishcategory);
    }
    localdishdata.unshift({
        "value": 0, "text": "Select Dish(s)", "DishCategoryName": "Select", "price": 0
    });
    $(dishid).kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: localdishdata,
        autoClose: false,
        preventCloseCnt: 0,
        index: 0,
        close: function (e) {
            if (this.preventCloseCnt > 0) {//now the next time something tries to close it will work ( unless it's a change )
                e.preventDefault();
                this.preventCloseCnt--;
            } else {
                var dropdownlist = $(dishid).data("kendoDropDownList");
                dropdownlist.select(0);

            }
        }
    });
    var dropdownlist = $(dishid).data("kendoDropDownList");
    dropdownlist.bind("change", dropdownlist_select);
    dropdownlist.bind("open", dropdownlist_open);

}

function dropdownlist_open(e) {
    if (e.sender.dataSource._pristineTotal == 1) {
        toastr.error("No Dish exists for this dish category.");
    }
}

function dropdownlist_select(e) {
    this.preventCloseCnt = 1;
    if (e.sender.dataItem(e.item).value == 0) {
        return;
    }
    var item = e.sender.dataItem(e.item)
    var mid = e.sender.element[0].id;
    var subid = "#sub" + mid;
    var dropdownid = e.sender.element[0].id;
    var text = item.text;
    var tooltip = item.alias;
    var ingID = item.value;
    var costPerKg = item.price;
    var ingPrice = item.price;
    var len = ingredientObject.length;
    var cuisinename = item.CuisineName;
    console.log("cu" + item.CuisineName);
    var flag = 1;
    var subqty = $("#inputqty" + dropdownid.substring(9)).val();
    var uomcode = $("#uom" + dropdownid.substring(9)).val();
    var wtqty = 0;
    if (uomcode == 'UOM-00003') {
        wtqty = $("#wtuomuom" + dropdownid.substring(9)).val();
    }
    var dtcat = item.DietCategoryCode;
    for (var i = 0; i < len; i++) {
        if (ingredientObject[i].ID == ingID) {
            flag = 0;
            break;
        }
    }
    if (flag == 1) {
        ingredientObject.push({
            "ID": ingID,
            "Name": text,
            "Price": ingPrice,
            "CostPerKg": costPerKg
        })
    }
    else {
        return;
    }
   
    var sid = "small" + ingID;
    var subInput = 'inputsubqty' + sid;
    var uomid = 'dishuom' + sid;
    var wtspanid = 'wtspan' + uomid;
    var costpsid = 'costpsid' + sid;
    var costpkid = 'costpkid' + sid;
    var subInputUOM = "wtuom" + uomid;
    var cuisinestring = cuisinename == null ? "" : '<span class="cuisine-heading">' + cuisinename + '</span>';
    $(subid)
        .prepend(
            '<div class="smallIngredient" id=' + sid + '>' +
            '     <div class="supper"  title="' + tooltip + '">' + text + '</div>' +
            '     <div class="slower">' + cuisinestring +'<span  id="' + costpsid + '">Cost / Serving:  ' + Utility.AddCurrencySymbol(ingPrice, 2) + '</span></div><input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
            '     <div class="dishtile">' +
            '          <input id="inputsubqty' + sid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + subqty + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
            '          <span id="' + uomid + '" class="colorGS"></span>' +
            '          <span id="' + wtspanid + '">' +
            '                <input id="wtuom' + uomid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtqty + '" min="0" onchange="updateSubUOMInput(' + subInputUOM + ',' + sid + ')" />' +
            '                <span class="wtlabel">wt. /pc (kg)</span>' +
            '          </span>' +
            '          <span style="float:right;margin-top:-3px" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
            '                 <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
            '          </span >' +
            '     </div>' +
            '</div>');
    if (uomcode != "UOM-00003") {
        $("#" + wtspanid).hide();
    }

    if (dtcat == "DTC-00001") {
        $('#' + sid).addClass("yellowborder");
    } else if (dtcat == "DTC-00002") {
        $('#' + sid).addClass("redborder");
    } else if (dtcat == "DTC-00003") {
        $('#' + sid).addClass("greenborder");
    }
    populateUOMDropdown('#' + uomid, uomcode);

    //List containg
    masterGroupList.push({
        "dropdownid": dropdownid,
        "subid": subid,
        "sid": sid,
        "ingPrice": ingPrice,
        "ingID": ingID,
        "text": text,
        "smallinput": "inputsubqty" + sid,
        "ContractPortion": subqty,
        "UOMCode": uomcode,
        "DietCategoryCode": dtcat,
        "wtqty": wtqty
    });
    $("#" + e.sender.element.context.id).data("kendoDropDownList").open();
    calculateCostPerServing(sid);
}

function populateUOMDropdown(uomid, uomvalue) {
    console.log(uomvalue);
    $(uomid).kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
    var dropdownlist = $(uomid).data("kendoDropDownList");
    if (dropdownlist !== undefined) {
        if (uomvalue == -1)
            dropdownlist.select(1);//value("Select");
        else
            dropdownlist.value(uomvalue);

        dropdownlist.bind("change", uomdropdownlist_select);
    }
   
}

function uomdropdownlist_select(e) {

    var ID = e.sender.dataItem(e.item).value;
    if (ID == 0) {
        return;
    }


    if (ID == "UOM-00003")
        $("#wtspan" + e.sender.element.context.id).show();
    else
        $("#wtspan" + e.sender.element.context.id).hide();
    console.log(e.sender.element.context.id);
    var idnum = e.sender.element.context.id.substring(3);
    var dishFlag = e.sender.element.context.id.search("small");
    var x = e.sender.element.context.id;
    var i = 0;
    if (dishFlag > 0) {
       
   
        var smallId = x.substring(7);
        calculateCostPerServing(smallId);
        for (item of masterGroupList) {
            if (item.sid == smallId) {
                item.UOMCode = ID;//$("#" + x).val();
              //  item.WeigthPerUOM = $("#wtspan" + e.sender.element.context.id).val();
                masterGroupList[i].UOMCode = ID;
             //   masterGroupList[i].WeigthPerUOM = $("#wtspan" + e.sender.element.context.id).val();
            }

            i++;
        }
        return
    }
   
    for (item of finalModelExitsDishCategory) 
    {
        
        if (idnum == item.DishCategory_ID) {
            finalModelExitsDishCategory[i].UOMCode = ID;
                item.UOMCode = ID;
        }
        i++;
    }

    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {

        var sm = subItem[i].id;

        $("#dishuom" + sm).data('kendoDropDownList').value(ID);

        if (ID == "UOM-00003")
            $("#wtspandishuom" + sm).show();
        else
            $("#wtspandishuom" + sm).hide();
        calculateCostPerServing(sm);
    }
    i = 0;
    for (item of masterGroupList) {
      
        if (item.dropdownid == "inputdish" + idnum) {
           // alert("hare Krishna")
            item.UOMCode = ID;//$("#" + x).val();
            item.WeigthPerUOM = $("#wtuom" + e.sender.element.context.id).val();
           // masterGroupList[i].UOMCode = ID;
           // masterGroupList[i].WeigthPerUOM = $("#wtspan" + e.sender.element.context.id).val();
        }
        i++;
    }
    
}


function itemGridEdit(e) {
    var gridObj = $("#gridItem").data("kendoGrid");
   // var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
    var tr = gridObj.dataItem($(e).closest("tr")); // 'e' is the HTML element <a>
    datamodel = tr;
   
    var dialog = $("#MenuCwindowEdit").data("kendoWindow");

    if (user.SectorNumber == "30") {
        dialog.height = "538px";
    }
    if (user.SectorNumber == "10") {
        dialog.height = "538px";
    }
    else {
        dialog.height = "200px";
    }
    //$("#MenuCwindowEdit").kendoWindow({
    //    animation: false
    //});
    //$(".k-overlay").css("display", "block");
    //$(".k-overlay").css("opacity", "0.5");
    dialog.open().element.closest(".k-window").css({
        top: 83.5,
        left: 558

    });
    dialog.center();
    
    if (user.SectorNumber == "30") {
        if (visualcategorydata.length < 1)
            AddEdit();
        $(".coreonly").show();
        $(".googleonly").hide();
        $("#itemid").text(tr.ItemID);
        $("#inputitemcode").val(tr.ItemCode);
        $("#inputitemname").val(tr.ItemName).focus();
        $("#inputfoodprogram").data('kendoDropDownList').value(tr.FoodProgram_ID);
        $("#inputdietcategory").data('kendoDropDownList').value(tr.DietCategory_ID);
        if (tr.VisualCategory_ID)
        $("#inputvisualcategory").data('kendoDropDownList').value(tr.VisualCategory_ID);
        $("#inputhsncode").val(tr.HSNCode);
        $("#inputcgst").val(tr.CGST);
        $("#inputsgst").val(tr.SGST);
        $("#inputugst").val(tr.UGST);
        $("#inputcess").val(tr.CESS);
        if (tr.Serveware_ID)
            $("#inputserveware").data('kendoDropDownList').value(tr.Serveware_ID);
        if (tr.ItemType1_ID)
            $("#inputitemtype1").data('kendoDropDownList').value(tr.ItemType1_ID);
        if (tr.ItemType2_ID)
            $("#inputitemtype2").data('kendoDropDownList').value(tr.ItemType2_ID);
        if (tr.ConceptType1_ID)
        $("#inputcontype1").data('kendoDropDownList').value(tr.ConceptType1_ID);
        if (tr.ConceptType2_ID)
        $("#inputcontype2").data('kendoDropDownList').value(tr.ConceptType2_ID);
        if (tr.ConceptType3_ID)
        $("#inputcontype3").data('kendoDropDownList').value(tr.ConceptType3_ID);
        if (tr.ConceptType4_ID)
        $("#inputcontype4").data('kendoDropDownList').value(tr.ConceptType4_ID);
        if (tr.ConceptType5_ID)
        $("#inputcontype5").data('kendoDropDownList').value(tr.ConceptType5_ID);
        $("#file").val("");
        $("#inputapplicableTo").val(tr.ApplicableTo);
        //$("#image").attr('src', '../ItemImages/' + tr.ImageName);
        $("#image").attr('src', './ItemImages/' + tr.ImageName);
        $("#inputitemcode").removeAttr("disabled");
    }
    else if (user.SectorNumber == "10") {
        $(".coreonly").hide();
        if (isSubSectorApplicable) {
            $(".googleonly").show();
            $("#inputsubsector").data('kendoDropDownList').value("Select");
            if (tr.SubSectorCode)
                $("#inputsubsector").data('kendoDropDownList').value(tr.SubSectorCode);
        }
        else {
            $(".googleonly").hide();
        }
        $("#inputitemcode").attr("disabled", "disabled");
        $("#itemid").text(tr.ItemID);
        $("#inputitemcode").val(tr.ItemCode);
        $("#inputitemname").val(tr.ItemName).focus();
        $("#inputdietcategory").data('kendoDropDownList').value(tr.DietCategory_ID);
    }
    else {
        $(".coreonly").hide();
        $(".googleonly").hide();
        $("#inputitemcode").attr("disabled", "disabled");
        $("#itemid").text(tr.ItemID);
        $("#inputitemcode").val(tr.ItemCode);
        $("#inputitemname").val(tr.ItemName).focus();
        $("#inputdietcategory").data('kendoDropDownList').value(tr.DietCategory_ID);
    }
    
    dialog.title("Item Details - " + tr.ItemName);
    return true;
}


function RecreateQuantity() {
    for (m1 of masterQty) {
        for (m2 of currentMasterQtyList) {
            if (m1.id == m2.id) {
                m2.qty = m1.qty;

                $("#" + m2.id).val(m1.qty);
                // $("#" + m2.uomid).data("kendoDropDownList").value(m1.uomcode);
                m2.uomid = m1.uomid;
                m2.uomcode = m1.uomcode;
            }
        }
    }
}

function SaveModel() {
    finalModel = [];
    var ContractPortion = 0;
    var wtPerUOM = 0;
    var UOMCode = "";
    var uniqueArray = [...new Set(currentMasterQtyList)]
    alertFlag = 0;
    var dishcategory_id_coll = [];

    for (m1 of uniqueArray) {
        var idnumeric = m1.id.substr(8);
        dishcategory_id_coll.push(idnumeric);
    }

    var uniqueDishCategoryID = [...new Set(dishcategory_id_coll)]

    for (m1 of uniqueDishCategoryID) {
        var idnumeric = m1;
        var StandardPortion = $("#inputqty" + idnumeric).val();

        //loop array
        var len = $("#subinputdish" + idnumeric).find(".smallIngredient").length;
        if (len == 0) {
            alertFlag = 1;
        }
        var coll = $("#subinputdish" + idnumeric).find(".smallIngredient");
        var idAlready = 0;
        for (i = 0; i < len; i++) {
            dishid = coll[i].id.substr(5);

            var localdishdata = [];

            localdishdata = dishdata.filter(function (item) {
                return item.value == dishid;
            });

            var dishcode = localdishdata[0].DishCode;
            //alert(dishcode);
            if (dishcode == null) {
                toastr.error("Dish Code is null or blank");
                continue;
            }
            ContractPortion = $("#inputsubqtysmall" + dishid).val();
            UOMCode = $("#dishuomsmall" + dishid).data("kendoDropDownList").value();
            if (UOMCode == "UOM-00003") {
                wtPerUOM = $("#wtuomdishuomsmall" + dishid).val();
            }
            for (item of finalModelExits) {
                idAlready = 0;
                if (dishid == item.Dish_ID) {
                    idAlready = item.ID;
                    break;
                }

            }

            finalModel.push({
                "ID": idAlready,
                "Item_ID": itemObject.ID,
                "ItemCode": itemObject.Code,
                "Dish_ID": dishid,
                "DishCode": dishcode,
                "StandardPortion": StandardPortion,
                "ContractPortion": ContractPortion,
                "IsActive": 1,
                "CreatedBy": user.UserId,
                "CreatedOn": Utility.CurrentDate(),
                "UOMCode": UOMCode,
                "WeightPerUOM": wtPerUOM,
                "Status": isPublishedClicked ? 3 : 2
            })
        }

    }
    console.log("Final model");
    console.log(finalModel);

    if (flagCurated && alertFlag) {
    
        toastr.error("This is a Curated Item. Map the appropriate Dishes before Publishing or saving");
        return;
       
    }

    finalModelDishCategory = [];
    idAlready = 0;
    if (dataItemsMulti.length == 0) {
        return;
    }

    for (item of dataItemsMulti) {
        var StandardPortion = $("#inputqty" + item.value).val();
        var WeightPerUOM =0;
        var DCUOMCode = $("#uom" + item.value).data("kendoDropDownList").value();
        if (DCUOMCode=='UOM-00003')
            WeightPerUOM = $("#wtuomuom" + item.value).val();
        
        idAlready = 0;
        for (itemin of finalModelExitsDishCategory) {

            if (item.code == itemin.DishCategoryCode) {
                idAlready = itemin.ID;
                console.log("idalready");
                console.log(idAlready);
                break;
            }

        }
        finalModelDishCategory.push({
            "ID": idAlready,
            "Item_ID": itemObject.ID,
            "ItemCode": itemObject.Code,
            "DishCategory_ID": item.value,
            "DishCategoryCode": item.code,
            "StandardPortion": StandardPortion,
            "IsActive": 1,
            "CreatedBy": user.UserId,
            "CreatedOn": Utility.CurrentDate(),
            "UOMCode": DCUOMCode,
            "WeightPerUOM": WeightPerUOM
        })
    }

}

function RecreateWholeExits() {

    for (item1 of masterGroupList) {
        var wtqty = 0;
        var dtcat = item1.DietCategoryCode;//"DTC-00001";
        var subid = item1.subid;
        var sid = item1.sid;
        var ingPrice = item1.ingPrice;
        var ingID = item1.ingID;
        var text = item1.text;
        var uomcode = item1.UOMCode;
        var tooltip = item1.tooltip;
        var subqty = item1.ContractPortion;
        var subInput = "inputsubqty" + sid;
        var uomid = 'dishuom' + item1.sid;
        var wtspanid = 'wtspan' + uomid;
        var wtPerUOM = item1.WeigthPerUOM;
        var costpsid = 'costpsid' + sid;
        var costpkid = 'costpkid' + sid;
        var costPerKg = item1.CostPerKg;
        var subInputUOM = "wtuom" + uomid;
        var cuisinename = item1.CuisineName;
        var cuisinestring = cuisinename == null ? "" : '<span class="cuisine-heading">' + cuisinename + '</span>';
        $(subid)
            .prepend(
                '<div class="smallIngredient" id=' + sid + '>' +
                '     <div class="supper" title="'+tooltip+'" >' + text + '</div>' +
                '     <div class="slower">' + cuisinestring +'<span  id="' + costpsid + '">Cost / Serving:  ' + Utility.AddCurrencySymbol(ingPrice, 2) + '</span></div><input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
                '     <div class="dishtile">' +
                '          <input type="number" id="inputsubqty' + sid + '" class="qtyS" type="Number"  oninput="validateNumber(this);" value="' + subqty + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
                '          <span id="' + uomid + '" class="colorGS"></span>' +
                '          <span id="' + wtspanid + '">' +
                '                <input id="wtuom' + uomid + '" class="qtyS"  type="number"  oninput="validateNumber(this);" value="' + wtqty + '" min="0" onchange="updateSubUOMInput(' + subInputUOM + ',' + sid + ')"/>' +
                '                <span class="wtlabel">wt. /pc (kg)</span>' +
                '          </span>' +
                '          <span style="float:right;margin-top:-3px" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
                '                 <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
                '          </span >' +
                '     </div>' +
                '</div>');

        if (uomcode != "UOM-00003") {
            $("#" + wtspanid).hide();
        }
        if (uomcode == "UOM-00003") {

            $("#wtuom" + uomid).val(wtPerUOM);
        }

        if (dtcat == "DTC-00001") {
            $('#' + sid).addClass("yellowborder");
        } else if (dtcat == "DTC-00002") {
            $('#' + sid).addClass("redborder");
        } else if (dtcat == "DTC-00003") {
            $('#' + sid).addClass("greenborder");
        }
        populateUOMDropdown('#' + uomid, uomcode);
        $('#' + uomid).val(uomcode);
        calculateCostPerServing(sid);
    }

    RecreateQuantity();
}



function expandAll() {
    var rows = $(".upper");
    $.each(rows, function () {
        var upperid = (this).id;
        var lowerid = (this).nextElementSibling.id;
        $("#" + upperid).find(".hideG").html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        $("#" + lowerid).show();
        $("#" + upperid).find(".k-widget").show();
        $("#" + upperid).find(".displayG").hide()
        $("#" + upperid).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" })
       // $("#expandIcon").css({ "text-decoration-line": "underline", "color": "#3f7990" });
        $("#collapseIcon").css({ "text-decoration-line": "none", "color": "#76b4b0" });
        $("#expandIcon").css({ "color": "#3f7990" });
        $("#collapseIcon").css({ "color": "#76b4b0" });
        $(".dishlabel").show();
        $(".ddldish").show();
    });
}

function collapseAll() {
    var rows = $(".upper");
    rows.each(function () {
        var upperid = (this).id;
        var lowerid = (this).nextElementSibling.id;
        $("#" + upperid).find(".hideG").html(" Show <i class= 'fas fa-angle-down' ></i>");
        var count = $("#" + lowerid).find(".smallIngredient").length;
        $("#" + lowerid).hide();
        $("#" + upperid).find(".displayG").show().html(" " + count + " Dish(s) Added");
        //   $("#" + upperid).find(".k-widget").hide();
        $(".dishlabel").hide();
        $("#" + upperid).find(".k-widget:last-child").hide();
        $("#" + upperid).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
       // $("#collapseIcon").css({ "text-decoration-line": "underline", "color": "#3f7990" });
        $("#collapseIcon").css({ "color": "#3f7990" });
        $("#expandIcon").css({ "color": "#76b4b0" });
        $("#expandIcon").css({ "text-decoration-line": "none", "color": "#76b4b0" });
        $(".ddldish").hide();
    });
}



function RecreateWhole() {
    for (item1 of masterGroupList) {
        for (item2 of currentMasterList) {
            if (item1.dropdownid == item2.dropdownid) {
                var dtcat = item1.DietCategoryCode;
                var wtqty = item1.WeigthPerUOM;
                var subid = item1.subid;
                var sid = item1.sid;
                var ingPrice = item1.ingPrice;
                var ingID = item1.ingID;
                var tooltip = item1.tooltip;
                var uomcode = item1.UOMCode;
                var text = item1.text;
                var ps = item1.ContractPortion;
                var subInput = "inputsubqty" + sid;
                var uomid = "dishuom" + item1.sid;
                var wtspanid = 'wtspan' + uomid;
                var costpsid = 'costpsid' + sid;
                var costpkid = 'costpkid' + sid;
                var costPerKg = item1.CostPerKg;
                var subInputUOM = "wtuom" + uomid;
                var cuisinename = item1.CuisineName;
                var cuisinestring = cuisinename == null ? "" : '<span class="cuisine-heading">' + cuisinename + '</span>';
                $(subid)
                    .prepend(
                        '<div class="smallIngredient" id=' + sid + '>' +
                        '     <div class="supper" title="'+tooltip+'">' + text + '</div>' +
                        '<div class="slower">' + cuisinestring +'<span  id="' + costpsid + '">Cost / Serving:  ' + Utility.AddCurrencySymbol(ingPrice, 2) + '</span></div><input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
                        '     <div class="dishtile">' +
                        '          <input id="inputsubqty' + sid + '" class="qtyS" type="Number"  oninput="validateNumber(this);" value="' + ps + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid+')" />' +
                        '          <span id="' + uomid + '" class="colorGS"></span>' +
                        '          <span id="' + wtspanid + '">' +
                        '                <input id="wtuom' + uomid + '" class="qtyS" type="Number"  oninput="validateNumber(this);" value="' + wtqty + '" min="0" onchange="updateSubUOMInput(' + subInputUOM + ',' + sid + ')" />' +
                        '                <span class="wtlabel">wt. /pc (kg)</span>' +
                        '          </span>' +
                        '          <span style="float:right;margin-top:-3px" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
                        '                 <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
                        '          </span >' +
                        '     </div>' +
                        '</div>');
                if (uomcode != "UOM-00003") {
                    $("#" + wtspanid).hide();
                }

                if (dtcat == "DTC-00001") {
                    $('#' + sid).addClass("yellowborder");
                } else if (dtcat == "DTC-00002") {
                    $('#' + sid).addClass("redborder");
                } else if (dtcat == "DTC-00003") {
                    $('#' + sid).addClass("greenborder");
                }

                $("#inputsubqty" + sid).val(ps);
                populateUOMDropdown('#' + uomid, uomcode);
                $('#' + uomid).val(uomcode);
                calculateCostPerServing(sid);
            }
        }
    }

    RecreateQuantity();
}
function smallRemove(sid, ingID) {
    $("#" + sid.id).remove();
    masterGroupList = masterGroupList.filter(function (item) {
        return item.ID !== ingID

    });
    ingredientObject = ingredientObject.filter(function (item) {
        return item.ID !== ingID

    });
}

function closeBelow(belowid, sameid, dishesid) {
    
    $("#" + belowid.id).toggle();
    if ($("#" + belowid.id).is(":visible")) {
        $("#" + sameid.id).find(".hideG").html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        $("#" + sameid.id).find(".ddldish").show();
        $("#" + sameid.id).find(".displayG").hide()
        $("#" + sameid.id).find(".dishlabel").show();
        $("#" + sameid.id).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" });
    } else {
        $("#" + sameid.id).find(".hideG").html(" Show <i class= 'fas fa-angle-down' ></i>");
        var count = $("#" + belowid.id).find(".smallIngredient").length;
        $("#" + sameid.id).find(".displayG").show().html(" " + count + " Dish(s) Added");
        //$("#" + sameid.id).find(".k-widget").hide();
        $("#" + sameid.id).find(".ddldish").hide();
        $("#" + sameid.id).find(".dishlabel").hide();
        $("#" + sameid.id).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
    }
}

function ToggleMultiSelect() {
    var multiselect = $("#inputdishcategory").data("kendoMultiSelect");
    currentMasterList = [];
    var dataItems = multiselect.dataItems();
    dataItemsMulti = dataItems;
    $("#dishCategory").empty();
    currentMasterQtyList = [];
    //update data items based on finalModelExitsDishCategory
   

    for (item of dataItems) {
       // if (finalModelExitsDishCategory.length > 0) {
            var flag = 0;
            for (itemX of finalModelExitsDishCategory) {

                if (item.code == itemX.DishCategoryCode) {
                    console.log("itemX" + itemX.UOMCode);
                    item.UOMCode = itemX.UOMCode;
                    item.StandardPortion = itemX.StandardPortion;
                    item.WeightPerUOM = itemX.WeightPerUOM;
                    flag = 1;
                    break;
                }

            }
            if (flag == 0) {
                item.UOMCode = -1;
                item.StandardPortion = 0;
                item.WeightPerUOM = 0;
                item.DishCategoryCode = item.code;
                item.DishCategory_ID = item.value;
                item.DishCategoryCode = item.code;
                finalModelExitsDishCategory.push(item);

            }
       // }

        var divid = "dish" + item.value;
        var dishesid = "inputdish" + item.value;
        var inputqty = "inputqty" + item.value;
        var subdishid = "subinputdish" + item.value;
        var uomid = "uom" + item.value;
        var wtspanid = "wtspan" + uomid;
        var ucode = item.UOMCode;
        console.log(ucode);
        var wtuom = "wtuom" + uomid;
        var wtqty = item.WeightPerUOM;
        var stdqty = item.StandardPortion;
        $('#dishCategory')
            .prepend(
                '<div class= "dish"  >'
                + '    <div class= "upper" id = ' + divid + ' >'
                + '         <div class= "upperText" onclick = "closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')" > ' + item.text + '</div>'
                + '         <input id = "' + inputqty + '"  class="qty"  type="Number" oninput="validateNumber(this);" value="0" min="0" onchange="updateQuantity(' + inputqty + ')" />'
                + '         <div id = "' + uomid + '" class="colorG"></div>' 

                + '            <span class="serv">Per Serving</span>'
                 +                    '          <span id="' + wtspanid + '">' +
                '                <input id="' + wtuom + '" class="qtyT" type="Number" oninput="validateNumber(this);" value="' + wtqty + '" min="0" onchange="updateWtPerUOMQuantity(' + wtuom + ')"/>' +
                '                <span class="wtlabel">wt. /pc (kg)</span>' +
                '          </span>' 
                + '         <span class= "hideG" onclick = "closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')"> Hide <i class= "fas fa-angle-up" ></i ></span >'
                + '         <span class= "displayG" ></span >'
                + '         <span class= "ddldish" id = ' + dishesid + '></span><span class="dishlabel">Choose Dish</span>'
                + '    </div>'
                + '    <div class= "lower" id = ' + subdishid + ' > </div > '
                + '</div>');

        populateDishDataDropdown('#' + dishesid, item.text, "#" + subdishid);
        populateUOMDropdown('#' + uomid, ucode);
        $("#" + inputqty).val(stdqty);
        if (ucode != "UOM-00003") {
            $("#" + wtspanid).hide();
        }
        currentMasterList.push({ "dropdownid": dishesid, "id": inputqty.id, "uomid": uomid, "uomcode": ucode });
        currentMasterQtyList.push({ "id": inputqty, "qty": stdqty });

    }
    RecreateWhole();
}

function updateQuantity(iqty) {
    var qty = $("#" + iqty.id).val();
    var flag = 1;
    for (sqty of masterQty) {
        if (sqty.id == iqty.id) {
            sqty.qty = qty;
            //console.log(sqty);
            flag = 0;
        }
    }
    if (flag) {
        masterQty.push({
            "id": iqty.id,
            "qty": qty
        });
    }
    var idnum = iqty.id.substring(8);
    for (item of finalModelExitsDishCategory) 
        {
            if (idnum == item.DishCategory_ID) {
                item.StandardPortion = qty;
            }
        }

    var idstobeupdated = [];
 
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {
        var sm = subItem[i].id;
        $("#inputsubqty" + sm).val(qty);
        var ins = "inputsubqty" + sm
        calculateCostPerServing(sm);
        idstobeupdated.push(ins);
    }
    //Master to be updated
    for (x of idstobeupdated) {
        var subinID = x;

        for (item of masterGroupList) {
            if (item.smallinput == subinID) {
                item.ContractPortion = $("#" + subinID).val();
                // calculateCostPerServing(sm);
            }
        }
    }
}


function updateWtPerUOMQuantity(iqty) {
    var qty = $("#" + iqty.id).val();
    var idstobeupdated = [];
    var idnum = iqty.id.substring(8);
    console.log(idnum);
    for (item of finalModelExitsDishCategory) 
        {
            if (idnum == item.DishCategory_ID) {
                item.WeightPerUOM = qty;
            }
        }

    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {
        var sm = subItem[i].id;
        $("#wtuomdishuom" + sm).val(qty);
        var ins = "wtuomdishuom" + sm
        calculateCostPerServing(sm);
        idstobeupdated.push(ins);
    }
    //Master to be updated
 

    for (item of masterGroupList) {
        if (item.dropdownid == "inputdish"+idnum) {
            item.WeigthPerUOM = qty;
               
            }
        }
  
}


function updateSubInput(inID, sid) {
    //Master to be updated //Ram
    var subinID = inID.id;
    for (item of masterGroupList) {
        if (item.smallinput == subinID) {
            item.ContractPortion = $("#" + subinID).val();
            calculateCostPerServing(sid.id);
        }
    }
}

function updateSubUOMInput(inID, sid) {
    //Master to be updated
    var subinID = inID.id;
    for (item of masterGroupList) {
        if (item.smallinput.substring(16) == subinID.substring(17)) {
            item.WeigthPerUOM = $("#" + subinID).val();
            calculateCostPerServing(sid.id);
        }
    }
}

function calculateCostPerServing(sid) {
    var costPerKg = $("#costpkid" + sid).val();
    var qty = $("#inputsubqty" + sid).val();
    var uom = $("#dishuom" + sid).val();
    var uomQty = $("#wtuom" + sid).val();
    if (uom == 'UOM-00001') {
        $("#costpsid" + sid).text("Cost / Serving: " + Utility.AddCurrencySymbol(costPerKg * qty,2));
    }
    else if (uom == 'UOM-00003') {
        var uomQty = $("#wtuomdishuom" + sid).val();
        $("#costpsid" + sid).text("Cost / Serving: " + Utility.AddCurrencySymbol(costPerKg * qty * uomQty,2));
    }
}

function AddEdit() {
  
    HttpClient.MakeSyncRequest(CookBookMasters.GetVisualCategoryDataList, function (data) {

        var dataSource = data;
        visualcategorydata = [];
        visualcategorydata.push({ "value": null, "text": "Select" ,"IsActive":false});
        for (var i = 0; i < dataSource.length; i++) {
            visualcategorydata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "VisualCategoryCode": dataSource[i].VisualCategoryCode, "IsActive": dataSource[i].IsActive });
        }
        populateVisualCategoryDropdown();
        //populateVisualCategoryDropdown_Bulk();
    }, null, false);

    HttpClient.MakeSyncRequest(CookBookMasters.GetServewareDataList, function (data) {

        var dataSource = data;
        servewaredata = [];
        servewaredata.push({ "value": null, "text": "Select", "IsActive": false});
        for (var i = 0; i < dataSource.length; i++) {
            servewaredata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "ServewareCode": dataSource[i].ServewareCode, "IsActive": dataSource[i].IsActive});
        }
        populateServewareDropdown();
    }, null, false);

    HttpClient.MakeSyncRequest(CookBookMasters.GetItemType1DataList, function (data) {

        var dataSource = data;
        itemtype1data = [];
        itemtype1data.push({ "value": null, "text": "Select", "IsActive": false});
        for (var i = 0; i < dataSource.length; i++) {
            itemtype1data.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "ItemType1Code": dataSource[i].ItemType1Code, "IsActive": dataSource[i].IsActive });
        }
        populateItemType1Dropdown();
    }, null, false);

    HttpClient.MakeSyncRequest(CookBookMasters.GetItemType2DataList, function (data) {

        var dataSource = data;
        itemtype2data = [];
        itemtype2data.push({ "value": null, "text": "Select", "IsActive": false});
        for (var i = 0; i < dataSource.length; i++) {
            itemtype2data.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "ItemType2Code": dataSource[i].ItemType2Code, "IsActive": dataSource[i].IsActive });
        }
        populateItemType2Dropdown();
    }, null, false);

    HttpClient.MakeSyncRequest(CookBookMasters.GetConceptType1DataList, function (data) {

        var dataSource = data;
        conceptype1data = [];
        conceptype1data.push({ "value": null, "text": "Select","IsActive":false });
        for (var i = 0; i < dataSource.length; i++) {
            conceptype1data.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "ConceptType1Code": dataSource[i].ConceptType1Code,"IsActive":dataSource[i].IsActive });
        }
        populateConceptType1Dropdown();
    }, null, false);

    HttpClient.MakeSyncRequest(CookBookMasters.GetConceptType2DataList, function (data) {

        var dataSource = data;
        conceptype2data = [];
        conceptype2data.push({ "value": null, "text": "Select", "IsActive": false});
        for (var i = 0; i < dataSource.length; i++) {
            conceptype2data.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "ConceptType2Code": dataSource[i].ConceptType2Code, "IsActive": dataSource[i].IsActive  });
        }
        populateConceptType2Dropdown();
    }, null, false);

    HttpClient.MakeSyncRequest(CookBookMasters.GetConceptType3DataList, function (data) {

        var dataSource = data;
        conceptype3data = [];
        conceptype3data.push({ "value": null, "text": "Select", "IsActive": false});
        for (var i = 0; i < dataSource.length; i++) {
            conceptype3data.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "ConceptType3Code": dataSource[i].ConceptType3Code, "IsActive": dataSource[i].IsActive  });
        }
        populateConceptType3Dropdown();
    }, null, false);

    HttpClient.MakeSyncRequest(CookBookMasters.GetConceptType4DataList, function (data) {

        var dataSource = data;
        conceptype4data = [];
        conceptype4data.push({ "value": null, "text": "Select", "IsActive": false});
        for (var i = 0; i < dataSource.length; i++) {
            conceptype4data.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "ConceptType4Code": dataSource[i].ConceptType4Code, "IsActive": dataSource[i].IsActive  });
        }
        populateConceptType4Dropdown();
    }, null, false);

    HttpClient.MakeSyncRequest(CookBookMasters.GetConceptType5DataList, function (data) {

        var dataSource = data;
        conceptype5data = [];
        conceptype5data.push({ "value": null, "text": "Select", "IsActive": false });
        for (var i = 0; i < dataSource.length; i++) {
            conceptype5data.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "ConceptType5Code": dataSource[i].ConceptType5Code, "IsActive": dataSource[i].IsActive });
        }
        populateConceptType5Dropdown();
    }, null, false);
    Utility.UnLoading();
}
function populateBulkChangeControls() {
    Utility.Loading();
    var gridVariable = $("#gridBulkChange");
    gridVariable.html("");
    gridVariable.kendoGrid({
        selectable: "cell",
        sortable: true,
        filterable: true,
        columns: [
            {
                field: "", title: "", width: "40px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                }
            },
            {
                field: "", title: "", width: "120px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
           
            {
                field: "FoodProgramName", title: "", width: "60px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "", title: "", width: "30px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "", title: "", width: "50px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            }
        ],
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
        },
        change: function (e) {
        },
    });
}


function populateDietCategoryDropdown() {
    $("#inputdietcategory").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: dietcategorydata,
        index: 0,
    });
}

function populateVisualCategoryDropdown() {
    $("#inputvisualcategory").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: visualcategorydata,
        index: 0,
        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In-Active");
                var dropdownlist = $("#inputvisualcategory").data("kendoDropDownList");
                dropdownlist.select("");
            }

        }
    });
}

function populateServewareDropdown() {
    $("#inputserveware").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: servewaredata,
        index: 0,
        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In-Active");
                var dropdownlist = $("#inputserveware").data("kendoDropDownList");
                dropdownlist.select("");
            }

        }
    });
}

function populateItemType1Dropdown() {
    $("#inputitemtype1").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: itemtype1data,
        index: 0,
        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In-Active");
                var dropdownlist = $("#inputitemtype1").data("kendoDropDownList");
                dropdownlist.select("");
            }

        }
    });
}

function populateItemType2Dropdown() {
    $("#inputitemtype2").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: itemtype2data,
        index: 0,
        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In-Active");
                var dropdownlist = $("#inputitemtype2").data("kendoDropDownList");
                dropdownlist.select("");
            }

        }
    });
}
function populateConceptType1Dropdown() {
    $("#inputcontype1").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: conceptype1data,
        index: 0,
        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In Active");
                var dropdownlist = $("#inputcontype1").data("kendoDropDownList");
                dropdownlist.select("");
            }

        }
    });
}
function populateConceptType2Dropdown() {
    $("#inputcontype2").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: conceptype2data,
        index: 0,
        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In Active");
                var dropdownlist = $("#inputcontype2").data("kendoDropDownList");
                dropdownlist.select("");
            }

        }
    });
}
function populateConceptType3Dropdown() {
    $("#inputcontype3").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: conceptype3data,
        index: 0,
        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In Active");
                var dropdownlist = $("#inputcontype3").data("kendoDropDownList");
                dropdownlist.select("");
            }

        }
    });
}
function populateConceptType4Dropdown() {
    $("#inputcontype4").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: conceptype4data,
        index: 0,
        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In Active");
                var dropdownlist = $("#inputcontype4").data("kendoDropDownList");
                dropdownlist.select("");
            }
        }

    });
}
function populateConceptType5Dropdown() {
    $("#inputcontype5").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: conceptype5data,
        index: 0,
        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In Active");
                var dropdownlist = $("#inputcontype5").data("kendoDropDownList");
                dropdownlist.select("");
            }
        }

    });
}
