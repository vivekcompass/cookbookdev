﻿var user;
var conceptype2data;
var tileImage = null;
var item = null;
var DishCategoryCodeIntial = null;
var datamodel;
var daypartdata = [];
var dishcategorysiblingdata = [];
var dishcategorydata = [];
var siblingMultiDDDdata = [];

$(function () {

    $('#myInput').on('input', function (e) {
        var grid = $('#gridDishCategory').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "DishCategoryCode" || x.field == "Name"  ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#DishwindowEdit").kendoWindow({
            modal: true,
            width: "250px",
            height: "167px",
            title: "Dish Category Details  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });


        HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            
            populateDishCategoryGrid();
        }, null, false);

        if (user.SectorNumber == "80") {
            //HttpClient.MakeRequest(CookBookMasters.GetDayPartDataList, function (data) {
            //    var dataSource = data;
            //    daypartdata = [];
            //    daypartdata.push({ "value": "Select", "text": "Select" });
            //    for (var i = 0; i < dataSource.length; i++) {
            //        if (dataSource[i].IsActive) {
            //            daypartdata.push({ "value": dataSource[i].DayPartCode, "text": dataSource[i].Name, "code": dataSource[i].DayPartCode, "IsActive": dataSource[i].IsActive });
            //        }
            //    }
            //    populateDayPartSelect();

            //}, null, false);

            HttpClient.MakeRequest(CookBookMasters.GetDishCategorySiblingDataList, function (data) {
                dishcategorysiblingdata = data;
                siblingMultiDDDdata = [];
                populateDishCategoryMultiSelect();
            }, null, false);
        }

        HttpClient.MakeRequest(CookBookMasters.GetSubSectorMasterList, function (data) {

            if (data.length > 1) {
                isSubSectorApplicable = true;
                data.unshift({ "SubSectorCode": 'Select', "Name": "Select Sub Sector" })
                subsectordata = data;
                populateSubSectorDropdown();
            }
            else {
                isSubSectorApplicable = false;
            }
        }, { isAllSubSector: true }, false);
    });

    function populateSubSectorDropdown() {
        $("#inputsubsector").kendoDropDownList({
            filter: "contains",
            dataTextField: "Name",
            dataValueField: "SubSectorCode",
            dataSource: subsectordata,
            index: 0,
        });
    }

    function populateDishCategoryMultiSelect() {
        $("#inputdishcategory").kendoMultiSelect({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: siblingMultiDDDdata,
            index: 0,
            autoClose   : false
        });
    }

    function modifySiblingDishCategoryData() {
        siblingMultiDDDdata = [];
        var dcsiblingdata = [];
        if (dishcategorydata != null && dishcategorydata.length > 0 && dishcategorysiblingdata != null && dishcategorysiblingdata.length > 0) {
            if (DishCategoryCodeIntial != null && DishCategoryCodeIntial != "") {
                dcsiblingdata = dishcategorysiblingdata;
                var sbCodeData = dishcategorysiblingdata.filter(m => m.DishCategoryCode == DishCategoryCodeIntial);
                if (sbCodeData != undefined && sbCodeData != null && sbCodeData.length > 0) {
                    siblingMultiDDDdata = dishcategorydata.filter(m => m.value !== DishCategoryCodeIntial);
                    dishcategorydata.forEach(function (item) {
                        var fd = dcsiblingdata.filter(m => m.DishCategoryCode === item.value && m.SiblingCode != sbCodeData[0].SiblingCode);
                        if (fd !== null && fd.length > 0) {
                            const index = siblingMultiDDDdata.indexOf(item);
                            if (index > -1) {
                                siblingMultiDDDdata.splice(index, 1);
                            }
                        }
                    });
                    $("#sbcode").val(sbCodeData[0].SiblingCode);
                }
                else {
                    siblingMultiDDDdata = dishcategorydata.filter(m => m.value !== DishCategoryCodeIntial);
                    dishcategorydata.forEach(function (item) {
                        var fd = dcsiblingdata.filter(m => m.DishCategoryCode === item.value);
                        if (fd !== null && fd.length > 0) {
                            const index = siblingMultiDDDdata.indexOf(item);
                            if (index > -1) {
                                siblingMultiDDDdata.splice(index, 1);
                            }
                        }
                    });
                }
            }
            else {
                siblingMultiDDDdata = dishcategorydata.filter(m => m.value !== DishCategoryCodeIntial);
            }
        }
        else {
            siblingMultiDDDdata = dishcategorydata.filter(m => m.value !== DishCategoryCodeIntial);
        }
        var multiselect = $("#inputdishcategory").data("kendoMultiSelect");
        multiselect.setDataSource(new kendo.data.DataSource({ data: siblingMultiDDDdata }));
        multiselect.dataSource.filter({});
        multiselect.value([""]);
        if (DishCategoryCodeIntial != null && DishCategoryCodeIntial != "" && dcsiblingdata != null && dcsiblingdata.length>0) {
            var dcsiblingdataSel = [];
            if (dcsiblingdata != undefined && dcsiblingdata != null && dcsiblingdata.length > 0) {
                dcsiblingdata.forEach(function (item) {
                    if (item.DishCategoryCode !== DishCategoryCodeIntial) {
                        dcsiblingdataSel.push(item.DishCategoryCode);
                    }
                });
                multiselect.value(dcsiblingdataSel);
            }
        }
    }

    //function modifySiblingDishCategoryData(dpCode) {
    //    siblingMultiDDDdata = [];
    //    var dcsiblingdata = [];
    //    if (dpCode != null && dpCode.length > 0) {
    //        if (dishcategorydata != null && dishcategorydata.length > 0 && dishcategorysiblingdata != null && dishcategorysiblingdata.length > 0
    //        ) {
    //            if (DishCategoryCodeIntial != null && DishCategoryCodeIntial != "") {
    //                dcsiblingdata = dishcategorysiblingdata.filter(m => m.DayPartCode == dpCode);
    //                var sbCodeData = dishcategorysiblingdata.filter(m => m.DayPartCode == dpCode && m.DishCategoryCode == DishCategoryCodeIntial);
    //                if (sbCodeData != undefined && sbCodeData != null && sbCodeData.length > 0) {
    //                    siblingMultiDDDdata = dishcategorydata.filter(m => m.value !== DishCategoryCodeIntial);
    //                    dishcategorydata.forEach(function (item) {
    //                        var fd = dcsiblingdata.filter(m => m.DishCategoryCode === item.value && m.DayPartCode == dpCode && m.SiblingCode != sbCodeData[0].SiblingCode);
    //                        if (fd !== null && fd.length > 0) {
    //                            const index = siblingMultiDDDdata.indexOf(item);
    //                            if (index > -1) {
    //                                siblingMultiDDDdata.splice(index, 1);
    //                            }
    //                        }
    //                    });
    //                    $("#sbcode").val(sbCodeData[0].SiblingCode);
    //                }
    //                else {
    //                    siblingMultiDDDdata = dishcategorydata.filter(m => m.value !== DishCategoryCodeIntial);
    //                    dishcategorydata.forEach(function (item) {
    //                        var fd = dcsiblingdata.filter(m => m.DishCategoryCode === item.value && m.DayPartCode == dpCode);
    //                        if (fd !== null && fd.length > 0) {
    //                            const index = siblingMultiDDDdata.indexOf(item);
    //                            if (index > -1) {
    //                                siblingMultiDDDdata.splice(index, 1);
    //                            }
    //                        }
    //                    });
    //                }
    //            }
    //            else {
    //                dcsiblingdata = dishcategorysiblingdata.filter(m => m.DayPartCode == dpCode);
    //                dishcategorydata.forEach(function (item) {
    //                    var fd = dcsiblingdata.filter(m => m.DishCategoryCode === item.value);
    //                    if (fd == null || fd.length == 0) {
    //                        siblingMultiDDDdata.push(item);
    //                    }
    //                });
    //            }
    //        }
    //        else {
    //            siblingMultiDDDdata = dishcategorydata.filter(m => m.value !== DishCategoryCodeIntial);
    //        }
    //    }
    //    var multiselect = $("#inputdishcategory").data("kendoMultiSelect");
    //    multiselect.setDataSource(new kendo.data.DataSource({ data: siblingMultiDDDdata }));
    //    multiselect.dataSource.filter({});
    //    multiselect.value([""]);
    //    if (DishCategoryCodeIntial != null && DishCategoryCodeIntial != "" && dcsiblingdata != null && dcsiblingdata.length > 0) {
    //        var dcsiblingdataSel = [];
    //        if (dcsiblingdata != undefined && dcsiblingdata != null && dcsiblingdata.length > 0) {
    //            dcsiblingdata.forEach(function (item) {
    //                if (item.DishCategoryCode !== DishCategoryCodeIntial) {
    //                    dcsiblingdataSel.push(item.DishCategoryCode);
    //                }
    //            });
    //            multiselect.value(dcsiblingdataSel);
    //        }
    //    }
    //}

    //function populateDayPartSelect() {
    //    $("#inputdaypart").kendoDropDownList({
    //        filter: "contains",
    //        dataTextField: "text",
    //        dataValueField: "value",
    //        dataSource: daypartdata,
    //        index: 0,
    //    });
    //    var dropdownlist = $("#inputdaypart").data("kendoDropDownList");
    //    dropdownlist.bind("change", dropdownlist_daypartchange);
    //}

    //function dropdownlist_daypartchange(eid) {
    //    var dpCode = eid.sender.dataItem(eid.item).code;
    //    if (dpCode != undefined && dpCode != null && dpCode != "Select") {
    //        $("#sbcode").val("");
    //        modifySiblingDishCategoryData(dpCode);

    //        //if (DishCategoryCodeIntial !== null && DishCategoryCodeIntial !== "") {
    //        //    var dcSiblingCodes = []
    //        //    dishcategorysiblingdata.forEach(function (item, index) {
    //        //        if (item.DayPartCode == dpCode && item.DishCategoryCode == DishCategoryCodeIntial) {
    //        //            dcSiblingCodes.push(item.SiblingDishCategoryCode.toString());
    //        //        }
    //        //    });
    //        //    if (dcSiblingCodes != null && dcSiblingCodes.length > 0) {
    //        //        var multiSelect = $('#inputdishcategory').data("kendoMultiSelect");
    //        //        multiSelect.value(dcSiblingCodes);
    //        //    }
    //        //    else {
    //        //        var multiSelect = $('#inputdishcategory').data("kendoMultiSelect");
    //        //        multiSelect.dataSource.filter({});
    //        //        multiSelect.value([]);
    //        //    }
    //        //}
    //        //else {

    //        //}
    //    }
    //    else if (dpCode != "Select") {
    //        var multiSelect = $('#inputdishcategory').data("kendoMultiSelect");
    //        multiSelect.dataSource.filter({});
    //        multiSelect.value([]);
    //    }
    //}

    function dcvalidate() {
        var valid = true;

        if ($("#dcname").val() === "") {        
            toastr.error("Please provide input");
            $("#dcname").addClass("is-invalid");
            valid = false;
            $("#dcname").focus();
        }
        if (($.trim($("#dcname").val())).length > 30) {
            toastr.error("Dish Category accepts upto 30 charactre");
           
            $("#dcname").addClass("is-invalid");
            valid = false;
            $("#dcname").focus();
        }
        return valid;
    }
    //Dish Category Section Start

    function populateDishCategoryGrid() {

        Utility.Loading();
        var gridVariable = $("#gridDishCategory");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "DishCategoryCode", title: "Dish Category Code", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive", title: "Active", width: "75px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var dialog = $("#DishwindowEdit").data("kendoWindow");
                                //var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");
                                // get the current table row (tr)
                               
                                var item = this.dataItem(tr);          // get the date of this row
                                if (!item.IsActive) {
                                    return;
                                }// get the current table row (tr)
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive,
                                    "DishCategoryCode": item.DishCategoryCode
                                };
                                DishCategoryCodeIntial = item.DishCategoryCode;
                               
                                datamodel = model;
                                $("#dcid").val(model.ID);
                                $("#dccode").val(model.DishCategoryCode);
                                $("#dcname").val(model.Name);
                              
                                $("#dcname").focus();
                                if (user.SectorNumber == "80") {
                                    $("#sbcode").val("");

                                    //$("#inputdaypart").data('kendoDropDownList').value("Select");
                                    
                                    //if (item.DayPartCode)
                                    //    $("#inputdaypart").data('kendoDropDownList').value(item.DayPartCode);

                                    modifySiblingDishCategoryData();
                                    //var dcsiblingdata = [];
                                    //var multiselect = $("#inputdishcategory").data("kendoMultiSelect");
                                    //multiselect.dataSource.filter({});
                                    //multiselect.value([""]);
                                    //if (item.DishCategorySiblings != undefined && item.DishCategorySiblings != null) {
                                    //    item.DishCategorySiblings = item.DishCategorySiblings.replaceAll(" ", "");
                                    //    var array = item.DishCategorySiblings.split(',');
                                    //    if (item.DishCategorySiblings != undefined && item.DishCategorySiblings != null) {
                                    //        for (health of array) {
                                    //            dcsiblingdata.push(health.toString());
                                    //        }
                                    //    }
                                    //    multiselect.value(dcsiblingdata);
                                    //}

                                    dialog.setOptions({
                                        height: 228,
                                        width: 600,
                                        top: 167,
                                        left: 558
                                    });
                                }
                                else if (user.SectorNumber == "10") {
                                    $("#inputsubsector").data('kendoDropDownList').value("Select Sub Sector");
                                    if (item.SubSectorCode)
                                        $("#inputsubsector").data('kendoDropDownList').value(item.SubSectorCode);
                                }
                                else {
                                    dialog.setOptions({
                                        top: 167,
                                        left: 558

                                    });
                                }
                                dialog.open();
                                dialog.center();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetDishCategoryDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                if (user.SectorNumber == "80") {
                                    dishcategorydata = [];
                                    dishcategorydata.push({ "value": "Select", "text": "Select" });
                                    for (var i = 0; i < result.length; i++) {
                                        if (result[i].IsActive) {
                                            dishcategorydata.push({ "value": result[i].DishCategoryCode, "text": result[i].Name, "DishCategoryID": result[i].ID, "IsActive": result[i].IsActive });
                                        }
                                    }
                                   // populateDishCategoryMultiSelect();
                                }
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { type: "string" },
                            DishCategoryCode: { type: "string" },
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
             
                $(".chkbox").on("change", function () {
                   
                        var gridObj = $("#gridDishCategory").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        var trEdit = $(this).closest("tr");
                        var th = this;
                        datamodel = tr;
                        datamodel.IsActive = $(this)[0].checked;
                        var active = "";
                        if (datamodel.IsActive) {
                            active = "Active";
                        } else {
                            active = "Inactive";
                        }

                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Dish Category " + active + "?", " Dish Category Update Confirmation", "Yes", "No", function () {
                            HttpClient.MakeRequest(CookBookMasters.SaveDishCategory, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again");
                                }
                                else {
                                    toastr.success("Dish Category configuration updated successfully");
                                    $(th)[0].checked = datamodel.IsActive;
                                    if ($(th)[0].checked) {
                                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                    } else {
                                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                    }
                                }
                            }, {
                                model: datamodel
                            }, false);
                        }, function () {

                                $(th)[0].checked = !datamodel.IsActive;
                                if ($(th)[0].checked) {
                                    $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                } else {
                                    $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                }
                        });
                        return true;
                    });
               
            },
            change: function (e) {
            },
        })

      //  var gridVariable = $("#gridDishCategory").data("kendoGrid");
        //sort Grid's dataSource
        gridVariable.data("kendoGrid").dataSource.sort({ field: "DishCategoryCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }


    $("#dccancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#DishwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });
    
    $("#dcaddnew").on("click", function () {
        //
        // var jq = jQuery.noConflict();
       // alert("calick");
        var model;
        var dialog = $("#DishwindowEdit").data("kendoWindow");
        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });
        datamodel = model;
        dialog.title("New Dish Category Creation");
        $("#dcname").removeClass("is-invalid");
        $('#dcsubmit').removeAttr("disabled");
        $("#dcid").val("");
        $("#dcname").val("");
        $("#dccode").val("");
        $("#sbcode").val("");
        $("#dcname").focus();
        DishCategoryCodeIntial = null;
        if (user.SectorNumber == "80") {
            $("#inputdaypart").data('kendoDropDownList').value("Select"); 
            var multiSelect = $('#inputdishcategory').data("kendoMultiSelect");
            multiSelect.dataSource.filter({});
            multiSelect.value([]);
            dialog.setOptions({
                height: 228,
                width: 600,
                top: 167,
                left: 558
            });
        };
    });
    
    $("#dcsubmit").click(function () {
        if ($("#dcname").val() === "") {
            toastr.error("Please provide Dish Category Name");
            $("#dcname").focus();
            return;
        }

        if (dcvalidate() === true) {
            $("#dcname").removeClass("is-invalid");

            var model = datamodel;
            var selecteddcsiblingString = "";
            if (user.SectorNumber == "80") {
                var selecteddcsibling = [];
                var dropdownlistOther = $("#inputdishcategory").data("kendoMultiSelect");
                var dataItemsOther = dropdownlistOther.dataItems();
                if (dataItemsOther != undefined && dataItemsOther != null) {
                    for (dataItem of dataItemsOther) {
                        selecteddcsibling.push(dataItem.value);
                    }
                }
                //var daypartcode = $("#inputdaypart").val();
                var daypartcode = "";

                //if (selecteddcsibling != null && selecteddcsibling.length > 0 && (daypartcode === undefined || daypartcode === "" || daypartcode === "Select")) {
                //    toastr.error("Please select Mealtime");
                //};
                selecteddcsiblingString = selecteddcsibling.toString();
            }
           
            model = {
                "ID": $("#dcid").val(),
                "Name": $("#dcname").val(),
                "DishCategoryCode": DishCategoryCodeIntial,
                "DayPartCode": daypartcode,
                "DishCategorySiblings": selecteddcsiblingString,
                "SubSectorCode": user.SectorNumber == "10" ? $("#inputsubsector").val() : "",
            }
            
            if (DishCategoryCodeIntial == null) {
                model.IsActive = 1;
            }
            else {
                model.IsActive = datamodel.IsActive;
            }
            if (!sanitizeAndSend(model)) {
                return;
            }

            $("#dcsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveDishCategory, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#dcsubmit').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#dcsubmit').removeAttr("disabled");
                        toastr.error(result.message);
                        Utility.UnLoading();
                        return;
                    }
                    else {
                        if (result == false) {
                            $('#dcsubmit').removeAttr("disabled");
                            toastr.success("There was some error, the record cannot be saved");

                        }
                        else {
                            $(".k-overlay").hide();
                            $('#dcsubmit').removeAttr("disabled");

                            if (model.ID > 0)
                                toastr.success("Dish Category configuration updated successfully");
                            else
                                toastr.success("New Dish Category added successfully");
                            $(".k-overlay").hide();
                            var orderWindow = $("#DishwindowEdit").data("kendoWindow");
                            orderWindow.close();
                            $("#gridDishCategory").data("kendoGrid").dataSource.data([]);
                            $("#gridDishCategory").data("kendoGrid").dataSource.read();
                            $("#gridDishCategory").data("kendoGrid").dataSource.sort({ field: "DishCategoryCode", dir: "asc" });
                            HttpClient.MakeSyncRequest(CookBookMasters.GetDishCategorySiblingDataList, function (data) {
                                dishcategorysiblingdata = data;
                            }, null, false);
                        }
                    }
                }
            }, {
                model: model

            }, true);
        }
    });
 


});