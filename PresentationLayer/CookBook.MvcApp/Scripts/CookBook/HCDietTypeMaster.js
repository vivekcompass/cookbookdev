﻿var datamodel;
$(document).ready(function () {
    $(".k-window").hide();
    $(".k-overlay").hide();
    Utility.Loading();
    setTimeout(function () {
        onLoad();
        Utility.UnLoading();
    }, 2000);

    $("#DietTypewindowEdit").kendoWindow({
        modal: true,
        width: "250px",
        height: "166px",
        title: "Diet Type Details  ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#DietTypewindowEdit1").kendoWindow({
        modal: true,
        width: "250px",
        height: "166px",
        title: "Diet Type Details  ",
        actions: ["Close"],
        visible: false,
        animation: false
    });
});
$("#DietTypeddnew").on("click", function () {
    //
    // var jq = jQuery.noConflict();
    // alert("calick");
    var model;

    var dialog = $("#DietTypewindowEdit").data("kendoWindow");

    //dialog.open();
    dialog.open().element.closest(".k-window").css({
        top: 167,
        left: 558

    });
    // dialog.center();

    datamodel = model;

    dialog.title("New Diet Type Creation");

    $("#DietTypename").removeClass("is-invalid");
    $('#DietTypesubmit').removeAttr("disabled");
    $("#DietTypeid").val("");
    $("#DietTypename").val("");
    $("#DietTypecode").val("");
    $("#DietTypename").focus();
    DishCategoryCodeIntial = null;

});
function onLoad() {
    populateCafeGrid();
    $("#btnExport").click(function (e) {
        var grid = $("#gridMOG").data("kendoGrid");
        grid.saveAsExcel();
    });
    $('#myInput').on('input', function (e) {
        var grid = $('#gridMOG').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "DietTypeCode" || x.field == "Name" || x.field == "IsActive") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
}


$("#DietTypecancel").on("click", function () {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $(".k-overlay").hide();
            var orderWindow = $("#DietTypewindowEdit").data("kendoWindow");
            orderWindow.close();
        },
        function () {
        }
    );
});

$("#DietTypecancel1").on("click", function () {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $(".k-overlay").hide();
            var orderWindow = $("#DietTypewindowEdit1").data("kendoWindow");
            orderWindow.close();
        },
        function () {
        }
    );
});

$("#DietTypesubmit").click(function () {

    if ($("#DietTypecname").val() === "") {
        toastr.error("Please provide Diet Type Name");
        $("#DietTypecname").focus();
        return;
    }


    var model = datamodel;

    model = {

        "Name": $("#DietTypecname").val(),
        "Code": $("#DietTypeccode").val(),
        "ModuleName":"DietType"
    }
    if (!sanitizeAndSend(model)) {
        return;
    }
    $("#DietTypesubmit").attr('disabled', 'disabled');
    HttpClient.MakeRequest(CookBookMasters.SaveDietType, function (result) {
        if (result.xsssuccess !== undefined && !result.xsssuccess) {
            toastr.error(result.message);
            $('#DietTypesubmit').removeAttr("disabled");
            Utility.UnLoading();
        }
        else {
            if (result == false) {
                $('#DietTypesubmit').removeAttr("disabled");
                toastr.success("There was some error, the record cannot be saved");

            }
            else {
                $(".k-overlay").hide();

                toastr.success("New Diet Type record added successfully");
                var orderWindow = $("#DietTypewindowEdit").data("kendoWindow");
                orderWindow.close();
                $("#gridMOG").data("kendoGrid").dataSource.data([]);
                $("#gridMOG").data("kendoGrid").dataSource.read();
            }
        }
    }, {
        model: model

    }, true);

    Utility.UnLoading();
    //populateCafeGrid();
});

$("#DietTypesubmit1").click(function () {

    if ($("#DietTypecname1").val() === "") {
        toastr.error("Please provide Diet Type Name");
        $("#DietTypecname1").focus();
        return;
    }


    var model = datamodel;

    model = {

        "Name": $("#DietTypecname1").val(),
        "Code": $("#DietTypeccode1").val(),
        "IsActive": model.IsActive,
        "ModuleName": "DietType"

    }

    if (!sanitizeAndSend(model)) {
        return;
    }

    HttpClient.MakeSyncRequest(CookBookMasters.SaveDietType, function (result) {
        if (result.xsssuccess !== undefined && !result.xsssuccess) {
            toastr.error(result.message);
            $('#DietTypesubmit1').removeAttr("disabled");
            Utility.UnLoading();
        }
        else {
            if (result == false) {
                $('#DietTypesubmit1').removeAttr("disabled");
                toastr.success("There was some error, the record cannot be saved");

            }
            else {
                $(".k-overlay").hide();

                toastr.success("Diet Type record update successfully");
                var orderWindow = $("#DietTypewindowEdit1").data("kendoWindow");
                orderWindow.close();
                $("#gridMOG").data("kendoGrid").dataSource.data([]);
                $("#gridMOG").data("kendoGrid").dataSource.read();
            }
        }
    }, {
        model: model

    }, true);

    Utility.UnLoading();
    //populateCafeGrid();
});
function populateCafeGrid() {

    Utility.Loading();
    var gridVariable = $("#gridMOG").height(380);
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "DietTypeMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Total: {2} records"
            }
        },
        //pageable: true,
        groupable: false,
        //reorderable: true,
        //scrollable: true,
        height: "490px",
        columns: [
            {
                field: "DietTypeCode", title: "Diet Type Code", width: "40px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Diet Type Name", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            //{
            //    field: "IsActive", title: "Active", width: "75px", attributes: {

            //        style: "text-align: center; font-weight:normal"
            //    },
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive == "true" ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
            //},
            {
                field: "Edit", title: "Action", width: "50px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                command: [
                    {
                        name: 'Edit',
                        click: function (e) {

                            var dialog = $("#DietTypewindowEdit1").data("kendoWindow");
                            var tr = $(e.target).closest("tr");

                            var item = this.dataItem(tr);          // get the date of this row

                            var model = {

                                "DietTypeCode": item.DietTypeCode,
                                "Name": item.Name,
                                "IsActive": item.IsActive

                            };
                            // DishCategoryCodeIntial = item.DishCategoryCode;


                            dialog.open();
                            dialog.center();

                            datamodel = model;

                            $("#DietTypeccode1").val(model.DietTypeCode);

                            $("#DietTypecname1").val(model.Name);

                            $("#DietTypecname1").focus();
                            return true;
                        }
                    }
                ],
            }

        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetDietTypeMasterList, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }
                    }, null
                        //{
                        //filter: mdl
                        //}
                        , false);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        DietTypeCode: { type: "string" },
                        Name: { type: "string" },
                        IsActive: { type: "string" }

                    }
                }
            },
            //pageSize: 15,
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var items = e.sender.items();
            var grid = this;

            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);

                if (model.IsActive == 'false') {
                    $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                }
            });
            items.each(function (e) {
                //if (user.UserRoleId == 1) {
                //    $(this).find('.k-grid-Edit').text("Edit");
                //    $(this).find('.chkbox').removeAttr('disabled');

                //} else {
                //    $(this).find('.k-grid-Edit').text("View");
                //    $(this).find('.chkbox').attr('disabled', 'disabled');
                //}
            });

            $(".chkbox").on("change", function () {

                var gridObj = $("#gridMOG").data("kendoGrid");
                var tr = gridObj.dataItem($(this).closest("tr"));
                var trEdit = $(this).closest("tr");
                var th = this;
                datamodel = tr;
                datamodel.IsActive = $(this)[0].checked;
                var active = "";
                if (datamodel.IsActive) {
                    active = "Active";
                } else {
                    active = "Inactive";
                }

                var model = datamodel;
                model = {

                    "Name": datamodel.Name,
                    "Code": datamodel.DietTypeCode,
                    "ModuleName": "DietType",
                    "IsActive": model.IsActive,
                }

                Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Diet Type " + active + "?", " Diet Type Update Confirmation", "Yes", "No", function () {
                    HttpClient.MakeRequest(CookBookMasters.SaveDietType, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again");
                        }
                        else {
                            toastr.success("Diet Type updated successfully");
                            $(th)[0].checked = datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        }
                    }, {
                        model: datamodel
                    }, false);
                }, function () {

                    $(th)[0].checked = !datamodel.IsActive;
                    if ($(th)[0].checked) {
                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                    } else {
                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
                return true;
            });

        },
        change: function (e) {
        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
    //$(".k-label")[0].innerHTML.replace("items", "records");
}  