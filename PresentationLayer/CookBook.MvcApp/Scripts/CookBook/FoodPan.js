﻿$(function () {

    var user;
    var datamodel = null;
    $("#windowEdit").kendoWindow({
        modal: true,
        width: "340px",
        height: "170px",
        title: "Food Pan Details",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $(document).ready(function () {
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
        });
        populateFoodPanGrid();
    });

    function populateFoodPanGrid() {
        Utility.Loading();
        var gridVariable = $("#gridFoodPan");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "110px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Size", title: "Size", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Depth", title: "Depth", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive", title: "Active", width: "40px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                item = this.dataItem(tr);          // get the date of this row
                                var model = item;
                                datamodel = item;
                                var dialog = $("#windowEdit").data("kendoWindow");
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open();
                                dialog.center();
                                FoodPanCodeInitial = item.FoodPanCode;
                                $('#fnsubmit').removeAttr("disabled");
                               // $("#fnaddnew").css("display", "none");
                                $("#fnid").val(model.ID);
                                $("#fnname").val(model.Name);
                                $("#fnsize").val(model.Size);
                                $("#fndepth").val(model.Depth);
                                $("#fnactive").prop('checked', model.IsActive);
                                $("#fnname").focus();
                                $("#fnname").removeClass("is-invalid");
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetFoodPanDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {

                    $(this).find('.k-grid-Edit').css('display', 'inline');
                });
            },
            change: function (e) {
            },
        })
    }

    $("#btnCancel").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit").data("kendoWindow");
        orderWindow.close();
    });

    $("#fnaddnew").on("click", function () {
        datamodel = null;
        $('#fnsubmit').removeAttr("disabled");
       // $("#fnaddnew").css("display", "none");
        $("#fnname").css("display", "block");
        var dialog = $("#windowEdit").data("kendoWindow");
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        $("#fnid").val("");
        $("#fnname").val("");
        $("#fnsize").val("");
        $("#fndepth").val("");
        $("#fnname").removeClass("is-invalid");
        $("#fnactive").prop('checked', true);
        $("#fnname").focus();
        item = null;
    });

    $("#btnSubmit").click(function () {
        if ($("#fnname").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#fnname").focus();
        }
        else {
            var model;
            if (datamodel != null) {
                model = datamodel;
                model.Name = $("#fnname").val();
                model.Size = $("#fnsize").val();
                model.Depth = $("#fndepth").val();
                model.IsActive = $("#fnactive")[0].checked;
            }
            else {
                model = {
                    "ID": $("fnid").val(),
                    "Name": $("#fnname").val(),
                    "Size": $("#fnsize").val(),
                    "Depth": $("#fndepth").val(),
                    "IsActive": $("#fnactive")[0].checked,
                }
            }

            $("#btnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveFoodPanData, function (result) {
                if (result == false) {
                    $('#btnSubmit').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#fnname").focus();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#windowEdit").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit').removeAttr("disabled");
                    if (model.ID > 0) {
                        toastr.success("Food Pan updated successfully");
                    }
                    else {
                        toastr.success("Food Pan added successfully");
                    }
                    $("#gridFoodPan").data("kendoGrid").dataSource.data([]);
                    $("#gridFoodPan").data("kendoGrid").dataSource.read();
                }
            }, {
                model: model

            }, false);
        }
    });

});