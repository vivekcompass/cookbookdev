﻿var user;
var status = "";
var varname = "";
var datamodel;
var RecipeName = "";
var recipeCode = "";
var uomdata = [];
var moguomdata = [];
var recipecategoryuomdata = [];
var basedata = [];
var dishdata = [];
var baserecipedata = [];
var baserecipedatafiltered = [];
var mogdata = [];
var mogdataList = [];
var mogWiseAPLMasterdataSource = [];
var MOGCode = "MOG-00156";
var MOGName = "Test"
var sitemasterdata = [];
var regionmasterdata = [];
var SiteCode = "";
var IsMajor = 0;
var MajorPercentData = 5;
var PrescibedPercentChange = 10;
var PerCostChangePercent = 5;
var x = 'x';
var oldcost = 0;
var currentPopuatedMOGGrid = "";
var MOGSelectedArticleNumber = "";
var MOGSelectedCost = "";
var allergenmasterdata = [];
var isMainRecipe = false;
var Region_ID = 0;
var Recipe_ID = 0;
var SiteID = 0;
var ToTIngredients = 0;
var moguomdata = [];
var ItemDishRecipeNutritionData = [];
var FilterItemDishRecipeNutritionData = [];
var dishHeaderString = "";
//Krish 20-07-2022
var copyData = [];

//$(function () {
function removeDuplicateObjectFromArray(array, key) {
    let check = {};
    let res = [];
    for (let i = 0; i < array.length; i++) {
        if (!check[array[i][key]]) {
            check[array[i][key]] = true;
            res.push(array[i]);
        }
    }
    return res;
}

$(document).ready(function () {

    $('#myInputAPL').on('input', function (e) {
        var grid = $('#gridMOGWiseAPL').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ArticleNumber" || x.field == "ArticleDescription" || x.field == "UOM" || x.field == "ArticleType"
                    || x.field == "Hierlevel3" || x.field == "MerchandizeCategoryDesc" || x.field == "MOGName" || x.field == "AllergensName") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $('#myInputNutritionData').on('input', function (e) {
        var grid = $('#gridRecipeWiseNutrients').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ItemName" || x.field == "DishCode" || x.field == "UOM" || x.field == "DishName"
                    || x.field == "ServedPortion" || x.field == "Energy" || x.field == "Carbs" || x.field == "Protein" || x.field == "Total_Fat" || x.field == "Total_Dietary_Fiber"
                    || x.field == "Sodium" || x.field == "Potassium" || x.field == "Added_Sugar" || x.field == "GI_c___5" || x.field == "GL_c___4" || x.field == "Sat_Fat"
                    || x.field == "MUFA" || x.field == "PUFA" || x.field == "Insoluble__Fiber" || x.field == "Soluble_Fiber" || x.field == "Iron" || x.field == "Calcium" || x.field == "Zinc"
                    || x.field == "Vitamin_A_beta__carotene_" || x.field == "Vitamin_C" || x.field == "GI_of_ingredients"
                    || x.field == "Sat_Fat") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    HttpClient.MakeRequest(CookBookMasters.GetSiteDataListByUserId, function (data) {
        var dataSource = data;
        sitemasterdata = dataSource;
        sitemasterdata.forEach(function (item) {
            item.SiteName = item.SiteName + " - " + item.SiteCode;
            return item;
        });
        sitemasterdata = sitemasterdata.filter(item => item.IsActive != false);
        if (data.length > 1) {
            sitemasterdata.unshift({ "SiteCode": 0, "SiteName": "Select Site","Region_ID":"" });
            $("#btnGo").css("display", "inline-block");
        }
        else {
            $("#btnGo").css("display", "inline-none");
        }
        populateSiteMasterDropdown();
       

    }, { SiteCode: $("#inputsite").val()} , true);
    HttpClient.MakeSyncRequest(CookBookMasters.GetApplicationSettingDataList, function (result) {
        Utility.UnLoading();

        if (result != null) {
            applicationSettingData = result;
            for (item of applicationSettingData) {
                if (item.Code == "FLX-00001") {//||item.Name.trim() == "Site Recipe Cost Change Threshold") {
                    PerCostChangePercent = item.Value;
                    $(".asSiteCost").text(PerCostChangePercent);
                } else if (item.Code == "FLX-00006") {// ||item.Name.trim()  == "Site Region Major Identification Threshold") {
                    MajorPercentData = item.Value;
                } else if (item.Code == "FLX-00007") {//|| item.Name.trim()  == "Site Recipe Quantity Change Threshold") {
                        PrescibedPercentChange = item.Value;
                        $(".asSiteQuantity").text(PrescibedPercentChange);
                    }
                
            }

        }
        else {

        }
    }, null, false);

    HttpClient.MakeSyncRequest(CookBookMasters.GetAllergenDataList, function (result) {
        Utility.UnLoading();

        if (result != null) {
            allergenmasterdata = result;;

        }
        else {

        }
    }, null, false);
    $("#siteconfigwindowEdit1").kendoWindow({
        modal: true,
        width: "1200px",
        height: "550px",
        title: "Recipe Details - " + RecipeName,
        actions: ["Close"],
        visible: false,
        draggable: false,
        resizable: false,
        animation: false
    });

    $("#siteconfigwindowEdit2").kendoWindow({
        modal: true,
        width: "1020px",
        height: "550px",
        title: "Recipe Details Inline- " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#siteconfigwindowEdit3").kendoWindow({
        modal: true,
        width: "1020px",
        height: "550px",
        title: "Recipe Details Inline- " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#siteconfigwindowEdit4").kendoWindow({
        modal: true,
        width: "1020px",
        height: "550px",
        title: "Recipe Details Inline- " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#siteconfigwindowEdit5").kendoWindow({
        modal: true,
        width: "1020px",
        height: "550px",
        title: "Recipe Details Inline- " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#windowEditMOGWiseAPL").kendoWindow({
        modal: true,
        width: "1050px",
        height: "430px",
        left: '220px !important',
        top: '80px !important',
        title: "MOG APL Detials - " + MOGName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    instructionTextEditor('inputinstruction');
    instructionTextEditor('inputinstruction0');
    instructionTextEditor('inputinstruction1');
    instructionTextEditor('inputinstruction2');
    instructionTextEditor('inputinstruction3');
    instructionTextEditor('inputinstruction4');
    instructionTextEditor('inputinstruction5');

    $("#btnCancel").click(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $("#mainContent").show();
                $("#windowEdit").hide();
                $("#topHeading").text("Unit Recipe Configuration");
            },
            function () {
            }
        );
    });

    $("#btnCancelND").click(function (e) {
        $("#NutritionDataDiv").hide();
        $("#RecipeMaster").show();
        $("#topHeading").text("Unit Recipe Configuration");
    });

    $("#btnCancelAPL").click(function (e) {
       
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditMOGWiseAPL").data("kendoWindow");
        orderWindow.close();


        //Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        //    function () {

        //        $(".k-overlay").hide();
        //        var orderWindow = $("#windowEditMOGWiseAPL").data("kendoWindow");
        //        orderWindow.close();
        //    },
        //    function () {
        //    }
        //);
    });

    $("#btnExport").click(function (e) {
        var grid = $("#gridRecipe").data("kendoGrid");
        grid.saveAsExcel();
    });


    HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {

        user = result;
        if (user.SectorNumber == "20" ){
            $(".rcHide").show();
        }
        else {
            $(".rcHide").hide();
        }
        if (user.SectorNumber == "20") {
            $(".mfgHide").hide();
        }
        else {
            $(".mfgHide").show();
        }
    }, null , true);

   

    HttpClient.MakeRequest(CookBookMasters.GetRecipeDataList, function (data) {

        var dataSource = data;

        baserecipedata = [];
        baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].CostPerUOM == null) {
                dataSource[i].CostPerUOM = 0;
            }
            if (dataSource[i].CostPerKG == null) {
                dataSource[i].CostPerKG = 0;
            }
            if (dataSource[i].TotalCost == null) {
                dataSource[i].TotalCost = 0;
            }
            if (dataSource[i].Quantity == null) {
                dataSource[i].Quantity = 0;
            }
            if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
                baserecipedata.push({
                    "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                });
            else
            baserecipedata.push({
                "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].BaseRecipeCode
            });
        }
        baserecipedatafiltered = baserecipedata;
    }, { condition: "Base" } , true);

    HttpClient.MakeRequest(CookBookMasters.GetDishDataList, function (data) {

        var dataSource = data;
        dishdata = [];
        dishdata.push({ "value": "Select Dish", "text": "Select Dish" });
        for (var i = 0; i < dataSource.length; i++) {
            dishdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name });
        }
    }, null , true);

    //HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

    //    var dataSource = data;
    //    uomdata = [];
    //    uomdata.push({ "value": "Select", "text": "Select", "UOMCode": "" });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        uomdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode });
    //    }
    //    populateDishDropdown();
    //}, null, true);

    HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
        var dataSource = data;
        uommodulemappingdata = [];
        uommodulemappingdata.push({ "value": "Select", "text": "Select", "UOMCode": "Select" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].IsActive) {
                uommodulemappingdata.push({ "value": dataSource[i].UOM_ID, "UOM_ID": dataSource[i].UOM_ID, "text": dataSource[i].UOMName, "UOMModuleCode": dataSource[i].UOMModuleCode, "UOMCode": dataSource[i].UOMCode });
            }
        }
        moguomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001' || m.text == "Select");
        uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");
        recipecategoryuomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00006' || m.text == "Select");
        populateDishDropdown();

    }, null, false);


    basedata = [{ "value": "Select", "text": "Select" }, { "value": "No", "text": "Final Recipe" },
    { "value": "Yes", "text": "Base Recipe" }];

    $("#gridBulkChange").css("display", "none");
    populateBulkChangeControls();
    var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
    changeControls.css("background-color", "#fff");
    changeControls.css("border", "none");
    changeControls.eq(2).text("");
    changeControls.eq(2).append("<input id='quantity' style='width:90%; margin-left:3px; font-size:10.5px!important'></div>");
    changeControls.eq(3).text("");
    changeControls.eq(3).append("<div id='uom' style='width:100%; font-size:10.5px!important'></div>");
    changeControls.eq(4).text("");
    changeControls.eq(5).append("<div id='base' style='width:100%; font-size:10.5px!important'></div>");
});



$("#InitiateBulkChanges").on("click", function () {
    $("#gridBulkChange").css("display", "block");
    $("#gridBulkChange").children(".k-grid-header").css("border", "none");
    $("#InitiateBulkChanges").css("display", "none");
    $("#CancelBulkChanges").css("display", "inline");
    $("#SaveBulkChanges").css("display", "inline");
    $("#bulkerror").css("display", "none");
    $("#success").css("display", "none");
    populateUOMDropdown_Bulk();
    populateBaseDropdown_Bulk();
});

function populateDishDropdown() {
    $("#inputdish").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: dishdata,
        enable: false,
    });
}

function populateUOMDropdown_Bulk() {
    $("#uom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}

function populateUOMDropdown() {
    $("#inputuom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}

function populateBaseDropdown_Bulk() {
    $("#base").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}
function populateBaseDropdown() {
    $("#inputbase").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}

$("#CancelBulkChanges").on("click", function () {
    $("#InitiateBulkChanges").css("display", "inline");
    $("#CancelBulkChanges").css("display", "none");
    $("#SaveBulkChanges").css("display", "none");
    $("#gridBulkChange").css("display", "none");
    $("#bulkerror").css("display", "none");
    $("#success").css("display", "none");
    //  populateRecipeGrid();
});

$("#SaveBulkChanges").on("click", function () {

    var neVal = $(this).val();
    var dataFiltered = $("#gridRecipe").data("kendoGrid").dataSource.dataFiltered();


    var model = dataFiltered;
    var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
    var validChange = false;
    var uom = "";
    var quantity = "";
    var base = "";

    var uomspan = $("#uom");
    var quantityspan = $("#quantity");
    var basespan = $("#base");

    if (uomspan.val() != "Select") {
        validChange = true;
        uom = uomspan.val();
    }
    if (quantityspan.val() != "") {
        validChange = true;
        quantity = quantityspan.val();
    }

    if (basespan.val() != "Select") {
        validChange = true;
        base = basespan.val();
    }
    if (validChange == false) {
        $("#bulkerror").css("display", "block");
        $("#bulkerror").find("p").text("Please change at least one attribute for Bulk Update");
    } else {
        $("#bulkerror").css("display", "none");
        Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
            function () {
                $.each(model, function () {

                    if (uom != "")
                        this.UOM_ID = uom;
                    if (quantity != "")
                        this.Quantity = quantity;
                    if (base != "")
                        if (base == "Yes")
                            this.IsBase = true;
                        else
                            this.IsBase = false;
                });

                HttpClient.MakeRequest(CookBookMasters.SaveSiteRecipeDataList, function (result) {
                    if (result == false) {
                        $("#bulkerror").css("display", "block");
                        $("#bulkerror").find("p").text("Some error occured, please try again.");
                        setTimeout(fade_out, 10000);
                        function fade_out() {
                            $("#bulkerror").fadeOut();
                        }
                    }
                    else {
                        $(".k-overlay").hide();
                        $("#bulkerror").css("display", "none");
                      
                        toastr.success("Records have been updated successfully");
                        $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                        $("#gridRecipe").data("kendoGrid").dataSource.read();
                        $("#InitiateBulkChanges").css("display", "inline");
                        $("#CancelBulkChanges").css("display", "none");
                        $("#SaveBulkChanges").css("display", "none");
                        $("#gridBulkChange").css("display", "none");
                        setTimeout(fade_out, 10000);

                        function fade_out() {
                            $("#success").fadeOut();
                        }
                    }
                }, {
                    model: model

                } , true);

                //populateSiteGrid();
            },
            function () {
                maptype.focus();
            }
        );
    }
});
//Food Program Section Start


function populateBaseRecipeGrid(recipeID,recipeCode) {
    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe");
    gridVariable.html("");
    if (user.SectorNumber == '80') {
        Utility.Loading();
        var gridVariable = $("#gridMOG");
        gridVariable.html("");
        gridVariable.kendoGrid({
            toolbar: [{ name: "cancel", text: "Reset" }],
            groupable: false,
            editable: false,
            navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: false,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: mogdata,
                                value: options.field,
                                change: onMOGDDLChange,
                            });
                        // container.find(".k-input")[0].text = options.model.MOGName;
                    }
                },
                {
                    field: "AllergensName", title: "Allergens", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "AllergensName",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Site Qty", width: "50px",
                   // template: '# if (IsMajor == true) {#<div><input type="number"  class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputmogqty" type="number"   name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',


                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                },


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "regiontotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },

                //{
                //    title: "Action",
                //    width: "60px",
                //    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        style: "text-align: center; font-weight:normal;"
                //    },
                //    command: [
                //        //{
                //        //    name: 'View APL',
                //        //    click: function (e) {
                //        //        isMainRecipe = true;
                //        //        var gridObj = $("#gridMOG").data("kendoGrid");
                //        //        currentPopuatedMOGGrid = "";
                //        //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //        //        datamodel = tr;

                //        //        MOGName = tr.MOGName;
                //        //        MOGSelectedArticleNumber = tr.ArticleNumber;
                //        //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //        //        $("#windowEditMOGWiseAPL").kendoWindow({
                //        //            animation: false
                //        //        });
                //        //        $(".k-overlay").css("display", "block");
                //        //        $(".k-overlay").css("opacity", "0.5");
                //        //        dialog.open();
                //        //        dialog.center();
                //        //        dialog.title("MOG APL Details - " + MOGName);
                //        //        MOGCode = tr.MOGCode;
                //        //        mogWiseAPLMasterdataSource = [];
                //        //        getMOGWiseAPLMasterData(MOGCode);
                //        //        populateAPLMasterGrid();
                //        //        Utility.UnLoading();
                //        //        return true;
                //        //    }
                //        //}
                //    ],
                //}
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                $.each(obj, function (key, value) {

                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                });

                                $("#totmog").text(mogtotal.toFixed(2));
                                getdkgtotal();
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {

                                SiteCode: $("#inputsite").val(),
                                recipeID: 0,
                                recipeCode: recipeCode,
                                regionID: Region_ID,
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
        var toolbar = $("#gridMOG").find(".k-grid-toolbar");

        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        cancelChangesConfirmation("#gridMOG");
        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG .k-grid-content").addClass("gridInside");
    }
    else {
        gridVariable.kendoGrid({
            toolbar: [{ name: "cancel", text: "Reset" }

            ],
            groupable: false,
            editable: false,
            navigatable: true,
            columns: [
                {
                    field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                            + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetails(this)'></i>`;
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: false,
                                filter: "contains",
                                dataTextField: "BaseRecipeName",
                                dataValueField: "BaseRecipe_ID",
                                autoBind: true,
                                dataSource: baserecipedata,
                                value: options.field,
                                change: onBRDDLChange
                            });
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Site Qty", width: "35px",
                    template: '# if (IsMajor == true) {#<div><input class="inputbrqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputbrqty"  name="Quantity" style="background-color:transparent!important; border:1px solid transparent!important" readonly oninput="calculateItemTotal(this)" /></div>#}#',

                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "RegionIngredientPerc", title: "Qty Change %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= RegionIngredientPerc-IngredientPerc# </div>#} #',
                },
                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                //{
                //    field: "RegionTotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                //    headerAttributes: {

                //        style: "text-align: right"
                //    },
                //    attributes: {
                //        class: "regiontotalcost",
                //        style: "text-align: right; font-weight:normal"
                //    },
                //},
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteBaseRecipesByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                BaseRecipetotal = 0.0;
                                $.each(obj, function (key, value) {

                                    BaseRecipetotal = parseFloat(BaseRecipetotal) + parseFloat(value.Quantity);
                                });
                                $("#totBaseRecpQty").text(BaseRecipetotal.toFixed(2));
                                options.success(result);
                                if (result.length > 0) {
                                    $("#gridBaseRecipe").css("display", "block");
                                    $("#emptybr").css("display", "none");
                                } else {
                                    $("#gridBaseRecipe").css("display", "none");
                                    $("#emptybr").css("display", "block");
                                }

                            }
                            else {
                                options.success("");
                            }


                        },
                            {
                                recipeCode: recipeCode,
                                SiteCode: $("#inputsite").val(),
                                recipeID: recipeID,
                                regionID: Region_ID,
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false },
                            BaseRecipe_ID: { type: "number", validation: { required: true } },
                            BaseRecipeName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true } },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },

            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },

            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.brTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "BaseRecipeName",
                        dataValueField: "BaseRecipe_ID",
                        change: onBRDDLChange,
                        //  autoBind: true,
                    });
                    //  ddt.find(".k-input").val(dataItem.BaseRecipeName);
                    $(this).find('.inputbrqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
    }
    cancelChangesConfirmation("#gridMOG");
    var toolbar = $("#gridBaseRecipe").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe .k-grid-content").addClass("gridInside");
    //$(".k-label")[0].innerHTML.replace("items", "records");
}



function populateMOGGrid(recipeID, recipeCode) {
    if (user.SectorNumber == "20") {
        Utility.Loading();
        var gridVariable = $("#gridMOG");
        gridVariable.html("");
        gridVariable.kendoGrid({
            //toolbar: [{ name: "cancel", text: "Reset" }],
            toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,
            //  navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: true,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: refreshMOGDropDownData(options.model.MOGName),
                                value: options.field,
                                change: onMOGDDLChange,
                            });
                        // container.find(".k-input")[0].text = options.model.MOGName;
                    }
                },
                {
                    field: "AllergensName", title: "Allergens", width: "100px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "AllergensName",
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "UOMCode", title: "UOM", width: "60px",
                    attributes: {
                        style: "text-align: left; font-weight:normal",
                        class: ''
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="uomname" style="margin-left:8px">' + kendo.htmlEncode(dataItem.UOMName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOGUOM" text="' + options.model.UOMName + '" data-text-field="text" data-value-field="UOMCode" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "text",
                                dataValueField: "UOMCode",
                                autoBind: true,
                                dataSource: moguomdata,
                                // dataSource: refreshMOGUOMDropDownData(options.model.UOMName),
                                value: options.field,
                                change: onMOGUOMDDLChange,
                            });

                    }
                },

                //{
                //    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "si",
                //        style: "text-align: center; font-weight:normal"
                //    }
                //},

                {
                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },

                    template: '<input type="number"  name="Quantity" class="inputmogqty" oninput="calculateItemTotalMOG(this,null)"/>',
                    attributes: {

                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },

                {
                    field: "DKgTotal", title: "DKG", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },
                    attributes: {
                        class: "dkgtotal",
                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },

                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },

                {
                    title: "Action",
                    width: "80px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridMOG").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                gridObj.dataSource._data.remove(selectedRow);
                                CalculateGrandTotal('');
                                return true;
                            }
                        },
                        {
                            name: 'Change APL',
                            click: function (e) {
                                isMainRecipe = true;
                                var gridObj = $("#gridMOG").data("kendoGrid");
                                currentPopuatedMOGGrid = "";
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;

                                MOGName = tr.MOGName;
                                MOGSelectedArticleNumber = tr.ArticleNumber;
                                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                $("#windowEditMOGWiseAPL").kendoWindow({
                                    animation: false
                                });
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open();
                                dialog.center();
                                dialog.title("MOG APL Details - " + MOGName);
                                MOGCode = tr.MOGCode;
                                mogWiseAPLMasterdataSource = [];
                                getMOGWiseAPLMasterData(MOGCode);
                                populateAPLMasterGrid();
                                Utility.UnLoading();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                if (user.SectorNumber == "20") {
                                    $.each(obj, function (key, value) {
                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.DKgValue * value.Quantity);
                                    });
                                }
                                else {
                                    $.each(obj, function (key, value) {
                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                    });
                                }

                                $("#totmog").text(mogtotal.toFixed(2));
                                var tot = parseFloat($("#totmog").text()).toFixed(2);

                                $("#ToTIngredients").text(tot);
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {

                                SiteCode: $("#inputsite").val(),
                                recipeID: 0,
                                recipeCode: recipeCode,
                                regionID: Region_ID,
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            RegionQuantity: { type: "number", editable: false },
                            RegionIngredientPerc: { type: "number", editable: false },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false },
                            RegionTotalCost: { editable: false },
                            DKgValue: { editable: false },
                            DKgTotal: { editable: false },
                            ArticleNumber: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
        var toolbar = $("#gridMOG").find(".k-grid-toolbar");
        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG .k-grid-content").addClass("gridInside");
        toolbar.find(".k-grid-clearall").addClass("gridButton");
        $('#gridMOG a.k-grid-clearall').click(function (e) {
            Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    $("#gridMOG").data('kendoGrid').dataSource.data([]);
                    showHideGrid('gridMOG', 'emptymog', 'mup_gridBaseRecipe');
                    gridIdin = 'gridMOG';
                },
                function () {
                }
            );

        });
        $(".k-grid-cancel-changes").click(function (e) {
             Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    // var grid = $("#" + gridIdin + "").data("kendoGrid");
                    if (gridIdin == 'gridBaseRecipe') {
                        populateBaseRecipeGrid(resetID);
                        return;
                    } else if (gridIdin == 'gridMOG') {
                        populateMOGGrid(resetID)
                        return;
                    }
                    // grid.cancelChanges();
                },
                function () {
                }
            );

        });
    }
    else if (user.SectorNumber == "80") {
        Utility.Loading();
        var gridVariable = $("#gridMOG");
        gridVariable.html("");
        gridVariable.kendoGrid({
            toolbar: [{ name: "cancel", text: "Reset" }],
            groupable: false,
            editable: false,
            navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: false,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: mogdata,
                                value: options.field,
                                change: onMOGDDLChange,
                            });
                        // container.find(".k-input")[0].text = options.model.MOGName;
                    }
                },
                {
                    field: "AllergensName", title: "Allergens", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "AllergensName",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Site Qty", width: "50px",
                   // template: '# if (IsMajor == true) {#<div><input type="number"  class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputmogqty" type="number"   name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',


                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                },


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "regiontotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },

                {
                    title: "Action",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    //command: [
                    //    {
                    //        name: 'View APL',
                    //        click: function (e) {
                    //            isMainRecipe = true;
                    //            var gridObj = $("#gridMOG").data("kendoGrid");
                    //            currentPopuatedMOGGrid = "";
                    //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //            datamodel = tr;

                    //            MOGName = tr.MOGName;
                    //            MOGSelectedArticleNumber = tr.ArticleNumber;
                    //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                    //            $("#windowEditMOGWiseAPL").kendoWindow({
                    //                animation: false
                    //            });
                    //            $(".k-overlay").css("display", "block");
                    //            $(".k-overlay").css("opacity", "0.5");
                    //            dialog.open();
                    //            dialog.center();
                    //            dialog.title("MOG APL Details - " + MOGName);
                    //            MOGCode = tr.MOGCode;
                    //            mogWiseAPLMasterdataSource = [];
                    //            getMOGWiseAPLMasterData(MOGCode);
                    //            populateAPLMasterGrid();
                    //            Utility.UnLoading();
                    //            return true;
                    //        }
                    //    }
                    //],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                $.each(obj, function (key, value) {

                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                });

                                $("#totmog").text(mogtotal.toFixed(2));
                                getdkgtotal();
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {

                                SiteCode: $("#inputsite").val(),
                                recipeID: 0,
                                recipeCode: recipeCode,
                                regionID: Region_ID,
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
        var toolbar = $("#gridMOG").find(".k-grid-toolbar");

        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        cancelChangesConfirmation("#gridMOG");
        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG .k-grid-content").addClass("gridInside");
    }
    else {
        Utility.Loading();
        var gridVariable = $("#gridMOG");
        gridVariable.html("");
        gridVariable.kendoGrid({
            toolbar: [{ name: "cancel", text: "Reset" }],
            groupable: false,
            editable: false,
            navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: false,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: mogdata,
                                value: options.field,
                                change: onMOGDDLChange,
                            });
                        // container.find(".k-input")[0].text = options.model.MOGName;
                    }
                },
                {
                    field: "AllergensName", title: "Allergens", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "AllergensName",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Site Qty", width: "50px",
                    template: '# if (IsMajor == true) {#<div><input type="number"  class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputmogqty" type="number"   name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',


                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                },


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "regiontotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },

                {
                    title: "Action",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Change APL',
                            click: function (e) {
                                isMainRecipe = true;
                                var gridObj = $("#gridMOG").data("kendoGrid");
                                currentPopuatedMOGGrid = "";
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;

                                MOGName = tr.MOGName;
                                MOGSelectedArticleNumber = tr.ArticleNumber;
                                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                $("#windowEditMOGWiseAPL").kendoWindow({
                                    animation: false
                                });
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open();
                                dialog.center();
                                dialog.title("MOG APL Details - " + MOGName);
                                MOGCode = tr.MOGCode;
                                mogWiseAPLMasterdataSource = [];
                                getMOGWiseAPLMasterData(MOGCode);
                                populateAPLMasterGrid();
                                Utility.UnLoading();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                $.each(obj, function (key, value) {
                                    
                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                });

                                $("#totmog").text(mogtotal.toFixed(2));
                                getdkgtotal();
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {

                                SiteCode: $("#inputsite").val(),
                                recipeID: 0,
                                recipeCode: recipeCode,
                                regionID: Region_ID,
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
        var toolbar = $("#gridMOG").find(".k-grid-toolbar");

        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        cancelChangesConfirmation("#gridMOG");
        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG .k-grid-content").addClass("gridInside");
    }

   
}

function refreshMOGDropDownData(x) {

    if (mogdataList != null && mogdataList.length > 0) {
        var mogGridData = $("#gridMOG").data('kendoGrid').dataSource._data;

        if (mogGridData != null && mogGridData.length > 0) {
            mogGridData = mogGridData.filter(m => m.MOGName != "" && m.MOGName != x);
            mogdata = mogdataList;
            var mogdataListtemp = mogdataList;
            mogdataListtemp = mogdataListtemp.filter(m => m.MOG_Code != "");
            mogdataListtemp.forEach(function (item, index) {
                for (var i = 0; i < mogGridData.length; i++) {
                    if (mogGridData[i].MOGCode !== "" && item.MOG_Code === mogGridData[i].MOGCode) {
                        // mogdata.splice(index, 1);
                        mogdata = mogdata.filter(mog => mog.MOG_Code !== item.MOG_Code);
                        break;
                    }
                }
                return true;
            });
        }
    }
    return mogdata.filter(m => m.IsActive == true);
};

function onMOGUOMDDLChange(e) {
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    console.log("Data Item ")
    console.log(dataItem)

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("UOM_ID", e.sender.dataItem().UOM_ID);
    dataItem.set("UOMName", data);
    dataItem.set("UOMCode", e.sender.value());
    dataItem.UOMName = data;

    dataItem.UOM_ID = e.sender.dataItem().UOM_ID;
    dataItem.UOMCode = e.sender.value();
    //dataItem.CostPerKG = costperkg;
    //dataItem.CostPerUOM = costperuom;

    //dataItem.set("CostPerKG", costperkg);
    //dataItem.set("CostPerUOM", costperuom);
    //var dkgtotal = 0;
    //if (user.SectorNumber == '20') {
    //    dkgtotal = e.sender.dataItem().DKgValue * dataItem.Quantity;
    //    dataItem.set("DKgValue", dkgtotal);
    //    dataItem.DKgValue = e.sender.dataItem().DKgValue;
    //    $(element).closest('tr').find('.dkgtotal')[0].innerHTML = dkgtotal;
    //    $(element).closest('tr').find('.uomcost')[0].innerHTML = costperuom;
    //    dataItem.TotalCost = dataItem.CostPerUOM * dkgtotal;
    //}
    //else {
    //    $(element).closest('tr').find('.uomcost')[0].innerHTML = costperuom;
    //    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    //}

    //$(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    //CalculateGrandTotal('');
}


function onMOGDDLChange(e) {
   
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG").data("kendoGrid");
    var dataItem = grid.dataItem(row);
   

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var ArticleNumber = Obj[0].ArticleNumber;
    var UOMCode = Obj[0].UOMCode;

    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.ArticleNumber = ArticleNumber;
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.UOMCode = UOMCode;
    dataItem.CostPerUOM = UnitCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    if (user.SectorNumber == "20") {
        var Allergen = Obj[0].AllergensName;
        $(element).closest('tr').find('.AllergensName')[0].innerHTML = (Allergen === "" || Allergen === null) ? "None" : Allergen;
    }
    var dkgtotal = 0;
    if (user.SectorNumber == '20') {
        dkgtotal = e.sender.dataItem().DKgValue * dataItem.Quantity;
        dataItem.set("DKgValue", dkgtotal);
        dataItem.DKgValue = e.sender.dataItem().DKgValue;
        $(element).closest('tr').find('.dkgtotal')[0].innerHTML = dkgtotal;
        dataItem.TotalCost = dataItem.CostPerUOM * dkgtotal;
    }
    else {
        dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    }
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('');
};

function populateBulkChangeControls() {

    Utility.Loading();
    var gridVariable = $("#gridBulkChange");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: false,
        filterable: false,
        //reorderable: true,
        //scrollable: true,
        columns: [
            {
                field: "Name", title: "Recipe Name", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOMName", title: "UOM", width: "60px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },

            },
            {
                field: "IsBase", title: "Base Recipe", width: "80px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "", title: "", width: "20px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            }
        ],
        columnResize: function (e) {

            e.preventDefault();
        },
        dataBound: function (e) {
        },
        change: function (e) {
        },
    });
}

$(document).ready(function () {
    $('#myInput').on('input', function (e) {
        var grid = $('#gridRecipe').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "RecipeType" || x.field == "RecipeCode" || x.field == "Name" || x.field == "UOMName" || x.field == "IsBase"
                    || x.field == "AllergensName" || x.field == "NGAllergens") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
    $("#addbr").on("click", function () {

        $("#gridBaseRecipe").css("display", "block");
        $("#emptybr").css("display", "none");
        $("#gridBaseRecipe").data("kendoGrid").addRow();
    })

    $("#addmog").on("click", function () {
        $("#gridMOG").css("display", "block");
        $("#emptymog").css("display", "none");
        $("#gridMOG").data("kendoGrid").addRow();
    })
    $("#addbr0").on("click", function () {

        $("#gridBaseRecipe0").css("display", "block");
        $("#emptybr0").css("display", "none");
        $("#gridBaseRecipe0").data("kendoGrid").addRow();
    })

    $("#addbr1").on("click", function () {

        $("#gridBaseRecipe1").css("display", "block");
        $("#emptybr1").css("display", "none");
        $("#gridBaseRecipe1").data("kendoGrid").addRow();
    })
    $("#addbr2").on("click", function () {

        $("#gridBaseRecipe2").css("display", "block");
        $("#emptybr2").css("display", "none");
        $("#gridBaseRecipe2").data("kendoGrid").addRow();
    })
    $("#addbr3").on("click", function () {

        $("#gridBaseRecipe3").css("display", "block");
        $("#emptybr3").css("display", "none");
        $("#gridBaseRecipe3").data("kendoGrid").addRow();
    })
    $("#addbr4").on("click", function () {

        $("#gridBaseRecipe4").css("display", "block");
        $("#emptybr4").css("display", "none");
        $("#gridBaseRecipe4").data("kendoGrid").addRow();
    })
    $("#addbr5").on("click", function () {

        $("#gridBaseRecipe5").css("display", "block");
        $("#emptybr5").css("display", "none");
        $("#gridBaseRecipe5").data("kendoGrid").addRow();
    })
    $("#addmog0").on("click", function () {

        $("#gridMOG0").css("display", "block");
        $("#emptymog0").css("display", "none");
        $("#gridMOG0").data("kendoGrid").addRow();
    })
    $("#addmog1").on("click", function () {

        $("#gridMOG1").css("display", "block");
        $("#emptymog1").css("display", "none");
        $("#gridMOG1").data("kendoGrid").addRow();
    })
    $("#addmog2").on("click", function () {

        $("#gridMOG2").css("display", "block");
        $("#emptymog2").css("display", "none");
        $("#gridMOG2").data("kendoGrid").addRow();
    })
    $("#addmog3").on("click", function () {

        $("#gridMOG3").css("display", "block");
        $("#emptymog3").css("display", "none");
        $("#gridMOG3").data("kendoGrid").addRow();
    })
    $("#addmog4").on("click", function () {

        $("#gridMOG4").css("display", "block");
        $("#emptymog4").css("display", "none");
        $("#gridMOG4").data("kendoGrid").addRow();
    })
    $("#addmog5").on("click", function () {

        $("#gridMOG5").css("display", "block");
        $("#emptymog5").css("display", "none");
        $("#gridMOG5").data("kendoGrid").addRow();
    })

    $("#AddNew").on("click", function () {
        Utility.UnLoading();
        $("#gridBaseRecipe0").css("display", "None");
        $("#gridMOG0").css("display", "None");
        $("#success").css("display", "none");
        $("#error0").css("display", "none");
        var editor = $("#inputinstruction0").data("kendoEditor");
        editor.value('');
        var model;

        //$(".k-overlay").css("display", "block");
        // $(".k-overlay").css("opacity", "0.5");

        $("#windowEdit0").css("display", "block");
        if (user.SectorNumber == "20" ){
            populateRecipeCategoryUOMDropdown0();
            populateWtPerUnitUOMDropdown0();
            populateGravyWtPerUnitUOMDropdown0();
            //populateRecipeCategoryUOMDropdown("0")
        }
        populateUOMDropdown0();
        populateBaseDropdown0();
        populateBaseRecipeGrid0();
        populateMOGGrid0();
        $("#mainContent").hide();

        $("#windowEdit0").show();

        datamodel = model;
        $("#inputinstruction0").val("");
        $("#recipeid0").text("");
        $("#inputrecipename0").val("");

        $("#inputquantity0").val("");
        if (user.SectorNumber == "20" ){
            $("#inputwtperunit0").val("");
            $("#inputgravywtperunit0").val("");
            $("#inputuom0").data("kendoDropDownList").value("Select");
            $("#inputrecipecum0").data("kendoDropDownList").value("Select");
            $("#inputwtperunituom0").data("kendoDropDownList").value("Select");
            $("#inputgravywtperunituom0").data("kendoDropDownList").value("Select");

            var recipeCategory = "";
            var rCatData = $("#inputrecipecum0").data('kendoDropDownList')
            if (rCatData != null && rCatData != "Select" && rCatData != "" && rCatData.dataItem() != null && rCatData.dataItem().UOMCode != "Select") {
                recipeCategory = rCatData.dataItem().UOMCode
            }
            OnRecipeUOMCategoryChange(recipeCategory, "0");
        }

        //  $("#inputbase0").data('kendoDropDownList').value("Select");
        // $("#gridBaseRecipe0").data("kendoGrid").addRow();
        // $("#gridMOG0").data("kendoGrid").addRow();
        // hideGrid('gridBaseRecipe0', 'emptybr0', 'baseRecipePlus0', 'baseRecipeMinus0');
        // hideGrid('gridMOG0', 'emptymog0', 'mogPlus0', 'mogMinus0');
        $("#gridMOG").css("display", "none");
        $("#emptymog0").css("display", "block");
        $("#gridBaseRecipe").css("display", "none");
        $("#emptybr0").css("display", "block");
    })

    $("#btnCancel0").on("click", function () {
        $(".k-overlay").hide();
        // var orderWindow = $("#windowEdit0").data("kendoWindow");
        $("#emptybr0").css("display", "block");
        $("#gridBaseRecipe0").css("display", "none");
        // orderWindow.close();
        $("#mainContent").show();
        $("#windowEdit0").hide();
    });

    $("#btnCancel1").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#siteconfigwindowEdit1").data("kendoWindow");
        $("#emptybr1").css("display", "block");
        $("#gridBaseRecipe1").css("display", "none");
        orderWindow.close();
       // RefreshGridOnBackButtonClick("btnCancel1", 0);

    });
    $("#btnCancel2").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#siteconfigwindowEdit2").data("kendoWindow");
        $("#emptybr2").css("display", "block");
        $("#gridBaseRecipe2").css("display", "none");
        orderWindow.close();
        $("#siteconfigwindowEdit1").parent().show();
       // RefreshGridOnBackButtonClick("btnCancel2", 1);
    });
    $("#btnCancel3").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#siteconfigwindowEdit3").data("kendoWindow");
        $("#emptybr3").css("display", "block");
        $("#gridBaseRecipe3").css("display", "none");
        orderWindow.close();
        $("#siteconfigwindowEdit2").parent().show();
       // RefreshGridOnBackButtonClick("btnCancel3", 2);
    });

    $("#btnCancel4").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#siteconfigwindowEdit4").data("kendoWindow");
        $("#emptybr4").css("display", "block");
        $("#gridBaseRecipe4").css("display", "none");
        orderWindow.close();
        $("#siteconfigwindowEdit3").parent().show();
       // RefreshGridOnBackButtonClick("btnCancel4", 3);
    });
    $("#btnCancel5").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#siteconfigwindowEdit5").data("kendoWindow");
        $("#emptybr5").css("display", "block");
        $("#gridBaseRecipe5").css("display", "none");
        orderWindow.close();
        $("#siteconfigwindowEdit4").parent().show();
        //RefreshGridOnBackButtonClick("btnCancel5", 4);
    });

    function RefreshGridOnBackButtonClick(btnId, recipeLevel) {
        var btnValue = $("#" + btnId + "").val();
        if (btnValue !== undefined && btnValue !== "") {
            updateAllergenDetails(btnValue, recipeLevel);
            //var gridLevel = 0;
            //if (recipeLevel > 0) {
            //    gridLevel = recipeLevel - 1;
            //}
            //gridLevel = gridLevel == 0 ? "" : gridLevel.toString();
            //$("#gridBaseRecipe" + gridLevel + "").data("kendoGrid").dataSource.data([]);
            //$("#gridMOG" + gridLevel + "").data("kendoGrid").dataSource.read();
            
        };
    }

    function updateAllergenDetails(rCode, recipeLevel) {
        Utility.Loading();
        HttpClient.MakeSyncRequest(CookBookMasters.UpdateSiteRecipeAllergenData, function (result) {
            if (result == null) {
                toastr.error("Some error occured, please try again");
                Utility.UnLoading();
            }
            else {
                console.log(result);
                if (recipeLevel == 0) {
                    $("#recipeAllergens").html(showAllergens(result));
                    Utility.UnLoading();

                }
                else {
                    $("#recipeAllergens" + recipeLevel + "").html(showAllergens(result));
                    Utility.UnLoading();

                }
            }
        }, {
             recipeCode: rCode,
             siteCode: $("#inputsite").val(),
        }, false);
    };

    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();
        // var orderWindow = $("#siteconfigwindowEdit1").data("kendoWindow");
        // "none");
        // orderWindow.close();
    });

    $("#btnSubmit0").click(function () {

        if (!validateMapAPL('gridMOG0')) {
            toastr.error("Please map APL with each MOG.");
            return false;
        }
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie
        if (!validateDataBeforeSave("gridBaseRecipe0", 'gridMOG0')) {
            toastr.error("Invalid Data!");
            return false;
        }
        if ($("#inputquantity").val() == null || $("#inputquantity").val() == "" || $("#inputquantity").val() == undefined) {
            toastr.error("Please enter Recipe Quantity");
            Utility.UnLoading();
            return false;
        }

        if (user.SectorNumber == "20" ){

            if ($("#inputrecipecum0").val() == null || $("#inputrecipecum0").val() == "" || $("#inputrecipecum0").val() == undefined || $("#inputrecipecum0").val() == "Select") {
                toastr.error("Please select Recipe UOM Category");
                Utility.UnLoading();
                return false;
            }
        }

        if ($("#inputuom0").val() == null || $("#inputuom0").val() == "" || $("#inputuom0").val() == undefined || $("#inputuom0").val() == "Select") {
            toastr.error("Please select UOM");
            Utility.UnLoading();
            return false;
        }
        var brgrid = $("#gridBaseRecipe0").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode0").val(),
                "Recipe_ID": $("#recipeid0").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1,
                "Site_ID": data[i].Site_ID,
                "SiteCode": data[i].SiteCode,
                "Region_ID": data[i].Region_ID
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG0").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode0").val(),
                "Recipe_ID": $("#recipeid0").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1,
                "Site_ID": data[i].Site_ID,
                "SiteCode": data[i].SiteCode,
                "Region_ID": data[i].Region_ID,
                "ArticleNumber": data[i].ArticleNumber
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase0").val() == "Yes")
            base = true
        else if ($("#inputbase0").val() == "No")
            base = false
        var model = {
            "RecipeCode": $("#inputrecipecode0").val(),
            "ID": $("#recipeid0").val(),
            "Name": $("#inputrecipename0").val(),
            "UOM_ID": $("#inputuom0").val(),
            "Quantity": $("#inputquantity0").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG0").text(),
            "TotalCost": $('#grandTotal0').text(),
            "Quantity": $('#inputquantity0').val(),
            "Instructions": $("#inputinstruction0").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy,
            "Site_ID": datamodel.Site_ID,
            "SiteCode": datamodel.SiteCode,
            "UOM_ID": $("#inputuom0").val(),
            "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum0").val() : "",
            "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit0").val() : "",
            "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom0").val() : "",
            "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit0").val() : "",
            "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom0").val() : "",
            "TotalIngredientWeight": $("#ToTIngredients").text(),
        }

        if ($("#inputrecipename0").val() === "") {
            // $("#error").css("display", "flex");
            //     toastr.error("Please provide valid input(s)");
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename0").focus();
        }
        else {
            // $("#error").css("display", "none");
            var base;
            if ($("#inputbase0").val() == "Yes")
                base = true
            else if ($("#inputbase0").val() == "No")
                base = false


            $("#btnSubmit0").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveSiteRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit0').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputrecipename0").focus();
                }
                else {
                    $(".k-overlay").hide();
                    $('#btnSubmit0').removeAttr("disabled");
                    toastr.success("New Recipe added successfully");
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            } , true);
        }
    });

    $("#btnSubmit1").click(function () {
        if (!validateMapAPL('gridMOG1')) {
            toastr.error("Please map APL with each MOG.");
            return false;
        }
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        if (!validateDataBeforeSave("gridBaseRecipe1", 'gridMOG1')) {
            toastr.error("Invalid Data!");
            return false;
        }
        var brgrid = $("#gridBaseRecipe1").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode1").val(),
                "Recipe_ID": $("#recipeid1").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1,
                "Site_ID": data[i].Site_ID,
                "SiteCode": data[i].SiteCode,
                "Region_ID": data[i].Region_ID
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG1").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode1").val(),
                "Recipe_ID": $("#recipeid1").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1,
                "Site_ID": data[i].Site_ID,
                "SiteCode": data[i].SiteCode,
                "Region_ID": data[i].Region_ID
                ,"ArticleNumber": data[i].ArticleNumber
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase1").val() == "Yes")
            base = true
        else if ($("#inputbase1").val() == "No")
            base = false
        var model = {
            "RecipeCode": $("#inputrecipecode1").val(),
            "ID": $("#recipeid1").val(),
            "Name": $("#inputrecipename1").val(),
            "UOM_ID": $("#inputuom1").val(),
            "Quantity": $("#inputquantity1").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG1").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#inputquantity1').val(),
            "Instructions": $("#inputinstruction1").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy,
            "Site_ID": datamodel.Site_ID,
            "SiteCode": datamodel.SiteCode
        }

        if ($("#inputrecipename1").val() === "") {
            $("#error").css("display", "flex");
             toastr.error("Please provide valid input(s)");
            $("#inputrecipename1").focus();
        }
        else {
            var base;
            if ($("#inputbase1").val() == "Yes")
                base = true
            else if ($("#inputbase1").val() == "No")
                base = false


            $("#btnSubmit1").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveSiteRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit1').removeAttr("disabled");
                    $("#error1").css("display", "flex");
                    $("#error1").find("p").text("Some error occured, please try again");
                    $("#inputrecipename1").focus();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#siteconfigwindowEdit1").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit1').removeAttr("disabled");
                   
                  
                    if (model.ID > 0)
                        toastr.success("Recipe configuration updated successfully");
                    else
                        toastr.success("New Recipe added successfully");


                    //$("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    //$("#gridRecipe").data("kendoGrid").dataSource.read();

                    //RefreshGridOnBackButtonClick("btnCancel1", 0);
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            } , true);
        }
    });

    $("#btnSubmit2").click(function () {
        if (!validateMapAPL('gridMOG2')) {
            toastr.error("Please map APL with each MOG.");
            return false;
        }

        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        if (!validateDataBeforeSave("gridBaseRecipe2", 'gridMOG2')) {
            toastr.error("Invalid Data!");
            return false;
        }
        var brgrid = $("#gridBaseRecipe2").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode2").val(),
                "Recipe_ID": $("#recipeid2").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1,
                "Site_ID": data[i].Site_ID,
                "SiteCode": data[i].SiteCode,
                "Region_ID": data[i].Region_ID
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG2").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode2").val(),
                "Recipe_ID": $("#recipeid2").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1,
                "Site_ID": data[i].Site_ID,
                "SiteCode": data[i].SiteCode,
                "Region_ID": data[i].Region_ID,
                "ArticleNumber": data[i].ArticleNumber
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase2").val() == "Yes")
            base = true
        else if ($("#inputbase2").val() == "No")
            base = false
        var model = {
            "RecipeCode": $("#inputrecipecode2").val(),
            "ID": $("#recipeid2").val(),
            "Name": $("#inputrecipename2").val(),
            "UOM_ID": $("#inputuom2").val(),
            "Quantity": $("#inputquantity2").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG2").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#inputquantity1').val(),
            "Instructions": $("#inputinstruction2").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy,
            "Site_ID": datamodel.Site_ID,
            "SiteCode": datamodel.SiteCode
           
        }

        if ($("#inputrecipename2").val() === "") {
             toastr.error("Please provide valid input(s)");
            $("#inputrecipename2").focus();
        }
        else {
            $("#error").css("display", "none");
            var base;
            if ($("#inputbase2").val() == "Yes")
                base = true
            else if ($("#inputbase2").val() == "No")
                base = false


            $("#btnSubmit2").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveSiteRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit2').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputrecipename2").focus();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#siteconfigwindowEdit2").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit2').removeAttr("disabled");
                    toastr.success("Recipe configuration updated successfully");

                    //$("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    //$("#gridRecipe").data("kendoGrid").dataSource.read();

                   // RefreshGridOnBackButtonClick("btnCancel2", 1);
                    
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            } , true);
        }
        $("#siteconfigwindowEdit1").parent('.k-widget').css("display", "block");
    });

    $("#btnSubmit3").click(function () {

        if (!validateMapAPL('gridMOG3')) {
            toastr.error("Please map APL with each MOG.");
            return false;
        }
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        if (!validateDataBeforeSave("gridBaseRecipe3", 'gridMOG3')) {
            toastr.error("Invalid Data!");
            return false;
        }
        var brgrid = $("#gridBaseRecipe3").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode3").val(),
                "Recipe_ID": $("#recipeid3").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1,
                "Site_ID": data[i].Site_ID,
                "SiteCode": data[i].SiteCode,
                "Region_ID": data[i].Region_ID
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG3").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode3").val(),
                "Recipe_ID": $("#recipeid3").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1,
                "Site_ID": data[i].Site_ID,
                "SiteCode": data[i].SiteCode,
                "Region_ID": data[i].Region_ID,
                 "ArticleNumber": data[i].ArticleNumber
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase3").val() == "Yes")
            base = true
        else if ($("#inputbase3").val() == "No")
            base = false
        var model = {
            "RecipeCode": $("#inputrecipecode3").val(),
            "ID": $("#recipeid3").val(),
            "Name": $("#inputrecipename3").val(),
            "UOM_ID": $("#inputuom3").val(),
            "Quantity": $("#inputquantity3").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG3").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#inputquantity1').val(),
            "Instructions": $("#inputinstruction3").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy,
            "Site_ID": datamodel.Site_ID,
            "SiteCode": datamodel.SiteCode
        }

        if ($("#inputrecipename3").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename3").focus();
        }
        else {
            var base;
            if ($("#inputbase3").val() == "Yes")
                base = true
            else if ($("#inputbase3").val() == "No")
                base = false


            $("#btnSubmit3").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveSiteRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit3').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputrecipename3").focus();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#siteconfigwindowEdit3").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit3').removeAttr("disabled");
                    toastr.success("Recipe configuration updated successfully");
                   
                    //$("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    //$("#gridRecipe").data("kendoGrid").dataSource.read();

                    //RefreshGridOnBackButtonClick("btnCancel3", 2);
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            } , true);
        }
    });

    $("#btnSubmit").click(function () {
       
        if (!validateMapAPL('gridMOG')) {
            toastr.error("Please map APL with each MOG.");
            return false;
        }
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie
        if (!validateDataBeforeSave("gridBaseRecipe", 'gridMOG')) {
            toastr.error("Invalid Data!");
            return false;
        }

        var rName = $("#inputrecipename").val();
        var rAlias = $("#inputrecipealiasname").val();
        if (rName === rAlias) {
            Utility.UnLoading();
            toastr.error("Recipe Name and Recipe Alias should be different");
            return;
        }
        if ($("#inputquantity").val() == null || $("#inputquantity").val() == "" || $("#inputquantity").val() == undefined || $("#inputquantity").val() == 0) {
            toastr.error("Please enter Recipe Quantity");
            Utility.UnLoading();
            return false;
        }

        if (user.SectorNumber == "20" ){

            if ($("#inputrecipecum").val() == null || $("#inputrecipecum").val() == "" || $("#inputrecipecum").val() == undefined || $("#inputrecipecum").val() == "Select") {
                toastr.error("Please select Recipe UOM Category");
                Utility.UnLoading();
                return false;
            }
        }

        if ($("#inputuom").val() == null || $("#inputuom").val() == "" || $("#inputuom").val() == undefined || $("#inputuom").val() == "Select") {
            toastr.error("Please select UOM");
            Utility.UnLoading();
            return false;
        }

        var baseRecipes = [];
        if (user.SectorNumber != "20") {
            var brgrid = $("#gridBaseRecipe").data("kendoGrid").dataSource;
            var data = brgrid._data;
            for (var i = 0; i < brgrid._data.length; i++) {
                var obj = {
                    "RecipeCode": $("#inputrecipecode").val(),
                    "Recipe_ID": $("#recipeid").val(),
                    "BaseRecipe_ID": data[i].BaseRecipe_ID,
                    "UOM_ID": data[i].UOM_ID,
                    "UOMCode": data[i].UOMCode,
                    "CostPerUOM": data[i].CostPerUOM,
                    "CostPerKG": data[i].CostPerKG,
                    "TotalCost": data[i].TotalCost,
                    "Quantity": data[i].Quantity,
                    "IsActive": 1,
                    "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                    "CreatedBy": data[i].CreatedBy,
                    "Site_ID": SiteID,
                    "SiteCode": SiteCode,
                    "Region_ID": Region_ID,
                    "CreatedOn": data[i].BaseRecipe_ID == 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].CreatedOn),
                    "CreatedBy": data[i].BaseRecipe_ID == 0 ? user.UserId : data[i].CreatedBy,
                    "ModifiedOn": data[i].BaseRecipe_ID > 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].ModifiedOn),
                    "ModifiedBy": data[i].BaseRecipe_ID > 0 ? user.UserId : data[i].ModifiedBy,

                }
                baseRecipes.push(obj);
            }
        }
        //MOG
        var mogGrid = $("#gridMOG").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode").val(),
                "Recipe_ID": $("#recipeid").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "DKgValue": data[i].DKgTotal,
                "IsActive": 1,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy,
                "Site_ID": SiteID,
                "SiteCode": SiteCode,
                "Region_ID": Region_ID
                , "ArticleNumber": data[i].ArticleNumber,
                "CreatedOn": data[i].MOG_ID == 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].MOG_ID == 0 ? user.UserId : data[i].CreatedBy,
                "ModifiedOn": data[i].MOG_ID > 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].ModifiedOn),
                "ModifiedBy": data[i].MOG_ID > 0 ? user.UserId : data[i].ModifiedBy
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase").val() == "Yes")
            base = true
        else if ($("#inputbase").val() == "No")
            base = false

       // var model = datamodel;
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom").val();
        });

        var model = {
            "RecipeCode": $("#inputrecipecode").val(),
            "ID": $("#recipeid").val(),
            "Name": $("#inputrecipename").val(),
            "UOM_ID": $("#inputuom").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG").text().substring(1).replace(",", ""),
            "TotalCost": $('#grandTotal').text().substring(1).replace(",", ""),
           // "Quantity": $('#inputquantity').val().substring(1).replace(",", ""),
            "Instructions": $("#inputinstruction").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy,
            "Site_ID": datamodel.Site_ID,
            "SiteCode": datamodel.SiteCode,
            "Region_ID": Region_ID,
            "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum").val() : "",
            "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit").val() : "",
            "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom").val() : "",
            "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit").val() : "",
            "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom").val() : "",
            "TotalIngredientWeight": $("#ToTIngredients").text(),
        }

        if ($("#inputrecipename").val() === "") {
           
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename").focus();
        }
        else {
            $("#error").css("display", "none");
            var base;
            if ($("#inputbase").val() == "Yes")
                base = true
            else if ($("#inputbase").val() == "No")
                base = false


            $("#btnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeSyncRequest(CookBookMasters.SaveSiteRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit').removeAttr("disabled");
                    
                    toastr.error("Some error occured, please try again");
                    $("#inputrecipename").focus();
                }
                else {
                    $(".k-overlay").hide();
                   
                   
                    $('#btnSubmit').removeAttr("disabled");
                  
                    toastr.success("Recipe configuration updated successfully");
                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();

                    var gridObj = $("#gridRecipe").data("kendoGrid");
                    if (gridObj != null && gridObj._data != null) {
                        var selectedRecipe = gridObj._data.filter(m => m.RecipeCode == $("#inputrecipecode").val());
                        console.log("selected" + selectedRecipe);
                        $("#recipeAllergens").html(showAllergens(selectedRecipe[0].AllergensName));
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            } , true);
        }
    });

    $("#btnSubmitAPL").click(function () {

        var selectedVal = "";
        var selected = $("input[type='radio'][name='radioLineSelector']:checked");
        if (selected.length > 0) {
            selectedVal = selected.val();
            var aplgrid = $("" + currentPopuatedMOGGrid + "").data("kendoGrid").dataSource;
            var data = aplgrid._data;
            aplgrid._data.forEach(function (item, index) {
                if (item.MOGCode == MOGCode) {
                    item.ArticleNumber = selectedVal;
                }
            });
            $("" + currentPopuatedMOGGrid + "").data("kendoGrid").dataSource = aplgrid;
            $(".k-overlay").hide();
            var orderWindow = $("#windowEditMOGWiseAPL").data("kendoWindow");
            orderWindow.close();
        }
        else {
            toastr.error("Please select at least one APL.");
            return false;
        }
    });

    $("#btnPublish").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        
        if (!validateMapAPL('gridMOG')) {
            toastr.error("Please map APL with each MOG.");
            return false;
        }
        //Base recipie
        if (!validateDataBeforeSave("gridBaseRecipe", 'gridMOG')) {
            toastr.error("Invalid Data!");
            return false;
        }

        if (user.SectorNumber == "20" ){

            if ($("#inputrecipecum").val() == null || $("#inputrecipecum").val() == "" || $("#inputrecipecum").val() == undefined || $("#inputrecipecum").val() == "Select") {
                toastr.error("Please select Recipe UOM Category");
                Utility.UnLoading();
                return false;
            }
        }

        if ($("#inputuom").val() == null || $("#inputuom").val() == "" || $("#inputuom").val() == undefined) {
            toastr.error("Please select UOM");
            Utility.UnLoading();
            return false;
        }

        if ($("#inputquantity").val() == null || $("#inputquantity").val() == "" || $("#inputquantity").val() == undefined || $("#inputquantity").val() == 0 || $("#inputquantity").val() == "Select") {
            toastr.error("Please enter Recipe Quantity");
            Utility.UnLoading();
            return false;
        }

        var baseRecipes = [];
        if (user.SectorNumber != "20") {
            var brgrid = $("#gridBaseRecipe").data("kendoGrid").dataSource;
            var data = brgrid._data;


            for (var i = 0; i < brgrid._data.length; i++) {
                var obj = {
                    "RecipeCode": $("#inputrecipecode").val(),
                    "Recipe_ID": $("#recipeid").val(),
                    "BaseRecipe_ID": data[i].BaseRecipe_ID,
                    "UOM_ID": data[i].UOM_ID,
                    "CostPerUOM": data[i].CostPerUOM,
                    "CostPerKG": data[i].CostPerKG,
                    "TotalCost": data[i].TotalCost,
                    "Quantity": data[i].Quantity,
                    "IsActive": 1,
                    "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                    "CreatedBy": data[i].CreatedBy,
                    "Site_ID": SiteID,
                    "SiteCode": SiteCode,
                    "Region_ID": Region_ID
                }
                baseRecipes.push(obj);
            }
        }
      
        //MOG
        var mogGrid = $("#gridMOG").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode").val(),
                "Recipe_ID": $("#recipeid").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "DKgValue": data[i].DKgTotal,
                "IsActive": 1,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy,
                "Site_ID": SiteID,
                "SiteCode": SiteCode,
                "Region_ID": Region_ID
                , "ArticleNumber": data[i].ArticleNumber
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase").val() == "Yes")
            base = true
        else if ($("#inputbase").val() == "No")
            base = false

        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom").val();
        });

        // var model = datamodel;
        var model = {
            "RecipeCode": $("#inputrecipecode").val(),
            "ID": $("#recipeid").val(),
            "Name": $("#inputrecipename").val(),
            "UOM_ID": $("#inputuom").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG").text().substring(1).replace(",", ""),
            "TotalCost": $('#grandTotal').text().substring(1).replace(",", ""),
            // "Quantity": $('#inputquantity').val().substring(1).replace(",", ""),
            "Instructions": $("#inputinstruction").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy,
            "Site_ID": datamodel.Site_ID,
            "SiteCode": datamodel.SiteCode,
            "Region_ID": Region_ID,
            "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum").val() : "",
            "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit").val() : "",
            "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom").val() : "",
            "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit").val() : "",
            "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom").val() : "",
            "TotalIngredientWeight": $("#ToTIngredients").text(),
        }


        if ($("#inputrecipename").val() === "") {

            toastr.error("Please provide valid input(s)");
            $("#inputrecipename").focus();
        }
        else {
            $("#error").css("display", "none");
            var base;
            if ($("#inputbase").val() == "Yes")
                base = true
            else if ($("#inputbase").val() == "No")
                base = false


            $("#btnPublish").attr('disabled', 'disabled');
            HttpClient.MakeSyncRequest(CookBookMasters.SaveSiteRecipeData, function (result) {
                if (result == false) {
                    $('#btnPublish').removeAttr("disabled");

                    toastr.error("Some error occured, please try again");
                    $("#inputrecipename").focus();
                }
                else {
                    $(".k-overlay").hide();


                    $('#btnPublish').removeAttr("disabled");

                    toastr.success("Recipe configuration updated successfully");
                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();
                    $("#mainContent").show();

                    $("#windowEdit").hide();

                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    function validateMapAPL(mogID) {
        
        var isValid = true;
        var mgrid = $("#" + mogID + "").data("kendoGrid").dataSource;
        mgrid._data.forEach(function (item, index) {
            if (item.MOGCode !== "MOG-00590"  && item.ArticleNumber == null || item.ArticleNumber == "") {
                isValid = false;
            };
        });
        return isValid;
    }
});

//Hare Ram
function onBRDDLChange(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    

    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    
};
function onBRDDLChange0(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe0', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe0").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;

};


function onBRDDLChange1(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe1', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe1").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    // //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onBRDDLChange2(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe2', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe2").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    // //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};

function onBRDDLChange3(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe3', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe3").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};

function onBRDDLChange4(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe4', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe4").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};

function onBRDDLChange5(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe5', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe5").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};



function onMOGDDLChange0(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG0', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG0").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;

};
function populateUOMDropdown0() {
    $("#inputuom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 1,
        enable: false,
    });
}
function populateBaseDropdown0() {
    $("#inputbase0").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,

    });
}

function mogtotalQTYDelete(level) {

    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var gTotal = 0.0;
    var len1 = grid._data.length;
    for (i = 0; i < len1; i++) {
        var totalCostMOG = user.SectorNumber == "20" ? grid._data[i].DKgValue * grid._data[i].Quantity : grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {
        $("#totmog").text(Utility.MathRound(gTotal, 2));
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
        if (level == "" || levle == "0") {
            $("#ToTIngredients" + level).text(Utility.MathRound(tot, 2));
        }
    }

}

function BaseRecipetotalQTYDelete(level) {

    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");

    var gTotal = 0.0;
    var len1 = grid._data.length;

    for (i = 0; i < len1; i++) {
        var totalCostMOG = user.SectorNumber == "20" ? grid._data[i].DKgValue * grid._data[i].Quantity : grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {

        $("#totBaseRecpQty").text(Utility.MathRound(gTotal, 2));
        // parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))).toFixed(2);
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))

        if (level == "" || levle == "0") {
            $("#ToTIngredients" + level).text(Utility.MathRound(tot, 2));
        }
    }


}

function calculateItemTotal0(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal0').html(Utility.MathRound(gTotal,2));
    }
    var qty = $('#inputquantity0').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG0').html(Utility.MathRound(costPerKG,2));
}



function calculateItemTotalMOG0(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal0').html(Utility.MathRound(gTotal,2));
    }
    var qty = $('#inputquantity0').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG0').html(Utility.MathRound(costPerKG,2));
}



function populateBaseRecipeGrid5(recipeID,recipeCode) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe5");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, {
            name: "create", text: "Add"
        },
        {
            name: "new", text: "Create New"
        },

        ],

        groupable: false,
        editable: true,
        navigatable: true,
        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetails6(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input tabindex="0" id="ddlBaseRecipe5" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 1,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange5,
                            //open: function (e) { e.preventDefault();}
                        });
                }
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                editor: function (container, options) {
                    $('<input tabindex="1" data-bind="value:' + options.field + '" class="inputbrqty"  name="Quantity" onchange="calculateItemTotal5(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetSiteBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                           
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe5").css("display", "block");
                            $("#emptybr5").css("display", "none");
                        } else {
                            $("#gridBaseRecipe5").css("display", "none");
                            $("#emptybr5").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode,
                            SiteCode: $("#inputsite").val(),
                            recipeID: recipeID,
                            regionID: Region_ID,
                        }
                         , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange5
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });

    var toolbar = $("#gridBaseRecipe5").find(".k-grid-toolbar");
    cancelChangesConfirmation("#gridBaseRecipe5");

    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe5 .k-grid-content").addClass("gridInside");




}

function populateBaseRecipeGrid4(recipeID,recipeCode) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe4");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, {
            name: "create", text: "Add"
        },
        {
            name: "new", text: "Create New"
        },

        ],

        groupable: false,
        editable: true,
        navigatable: true,
        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetails5(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input tabindex="0" id="ddlBaseRecipe4" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 1,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange4,
                            //open: function (e) { e.preventDefault();}
                        });
                }
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                editor: function (container, options) {
                    $('<input tabindex="1" data-bind="value:' + options.field + '" class="inputbrqty"  name="Quantity" onchange="calculateItemTotal4(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetSiteBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe4").css("display", "block");
                            $("#emptybr4").css("display", "none");
                        } else {
                            $("#gridBaseRecipe4").css("display", "none");
                            $("#emptybr4").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode,
                            SiteCode: $("#inputsite").val(),
                            recipeID: recipeID,
                            regionID: Region_ID,
                        }
                         , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange4
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });

    var toolbar = $("#gridBaseRecipe4").find(".k-grid-toolbar");
    cancelChangesConfirmation("#gridBaseRecipe4");

    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe4 .k-grid-content").addClass("gridInside");




}

function populateBaseRecipeGrid3(recipeID,recipeCode) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe3");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }
        ],

        groupable: false,
        editable: true,
        navigatable: true,
        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetails4(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input tabindex="0" id="ddlBaseRecipe3" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 1,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange3,
                            //open: function (e) { e.preventDefault();}
                        });
                }
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                editor: function (container, options) {
                    $('<input tabindex="1" data-bind="value:' + options.field + '" class="inputbrqty"  name="Quantity" onchange="calculateItemTotal3(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetSiteBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe3").css("display", "block");
                            $("#emptybr3").css("display", "none");
                        } else {
                            $("#gridBaseRecipe3").css("display", "none");
                            $("#emptybr3").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode,
                            SiteCode: $("#inputsite").val(),
                            recipeID: recipeID,
                            regionID: Region_ID,
                        }
                         , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange2
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });

    var toolbar = $("#gridBaseRecipe3").find(".k-grid-toolbar");
    cancelChangesConfirmation("#gridBaseRecipe3");

    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe3 .k-grid-content").addClass("gridInside");



}

function populateBaseRecipeGrid2(recipeID,recipeCode) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe2");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }
        ],

        groupable: false,
        editable: true,
        navigatable: true,
        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetails3(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input tabindex="0" id="ddlBaseRecipe2" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 1,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange2,
                            //open: function (e) { e.preventDefault();}
                        });
                }
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                editor: function (container, options) {
                    $('<input tabindex="1" data-bind="value:' + options.field + '" class="inputbrqty"  name="Quantity" onchange="calculateItemTotal2(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetSiteBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe2").css("display", "block");
                            $("#emptybr2").css("display", "none");
                        } else {
                            $("#gridBaseRecipe2").css("display", "none");
                            $("#emptybr2").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode,
                            SiteCode: $("#inputsite").val(),
                            recipeID: recipeID,
                            regionID: Region_ID,
                        }
                         , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange2
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });

    var toolbar = $("#gridBaseRecipe2").find(".k-grid-toolbar");
    cancelChangesConfirmation("#gridBaseRecipe2");

    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe2 .k-grid-content").addClass("gridInside");



}

function populateBaseRecipeGrid1(recipeID,recipeCode) {

    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe1");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" },
        ],

        groupable: false,
        editable: true,
        navigatable: true,
        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetails2(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input tabindex="0" id="ddlBaseRecipe1" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 1,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange1,
                            //open: function (e) { e.preventDefault();}
                        });
                }
            },

            {
                field: "Quantity", title: "Quantity", width: "35px",
                editor: function (container, options) {
                    $('<input tabindex="1" data-bind="value:' + options.field + '" class="inputbrqty"  name="Quantity" onchange="calculateItemTotal1(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetSiteBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe1").css("display", "block");
                            $("#emptybr1").css("display", "none");
                        } else {
                            $("#gridBaseRecipe1").css("display", "none");
                            $("#emptybr1").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode,
                            SiteCode: $("#inputsite").val(),
                            recipeID: recipeID,
                            regionID: Region_ID,
                        }
                         , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });

    var toolbar = $("#gridBaseRecipe1").find(".k-grid-toolbar");
    cancelChangesConfirmation("#gridBaseRecipe1");

    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe1 .k-grid-content").addClass("gridInside");




}


function onMOGDDLChange5(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG5', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG5").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onMOGDDLChange4(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG4', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG4").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onMOGDDLChange3(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG3', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG3").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onMOGDDLChange2(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG2', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG2").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onMOGDDLChange1(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG1', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG1").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function populateMOGGrid0() {

    Utility.Loading();
    var gridVariable = $("#gridMOG0");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: true,
        navigatable: true,
        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange0,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "AllergensName", title: "Allergens", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "AllergensName",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "60px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                editor: function (container, options) {
                    $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" onchange="calculateItemTotalMOG0(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },
                //template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    onchange="calculateItemTotalMOG(this)"/>',
                attributes: {

                    style: "text-align: right; font-weight:normal"
                },
            },
          
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //    {
                //        name: 'View APL',
                //        click: function (e) {
                //            isMainRecipe = false;
                //            var gridObj = $("#gridMOG0").data("kendoGrid");
                //            currentPopuatedMOGGrid = "0";
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            MOGName = tr.MOGName;
                //            MOGSelectedArticleNumber = tr.ArticleNumber;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $("#windowEditMOGWiseAPL").kendoWindow({
                //                animation: false
                //            });
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData();
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        dataSource: {
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: mogdata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange0
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    var toolbar = $("#gridMOG0").find(".k-grid-toolbar");
    cancelChangesConfirmation("#gridMOG0");

    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG0 .k-grid-content").addClass("gridInside");
}


function populateUOMDropdown1() {
    $("#inputuom1").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}
function populateBaseDropdown1() {
    $("#inputbase1").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}
function populateUOMDropdown2() {
    $("#inputuom2").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}
function populateBaseDropdown2() {
    $("#inputbase2").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}
function populateUOMDropdown3() {
    $("#inputuom3").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}
function populateBaseDropdown3() {
    $("#inputbase3").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}
function populateUOMDropdown4() {
    $("#inputuom4").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}
function populateBaseDropdown4() {
    $("#inputbase4").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}
function populateUOMDropdown5() {
    $("#inputuom5").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}
function populateBaseDropdown5() {
    $("#inputbase5").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,

        enable: false
    });
}

function calculateItemTotal1(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal1').html(Utility.AddCurrencySymbol(gTotal,2));
    }
    var qty = $('#inputquantity1').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG1').html(Utility.AddCurrencySymbol(costPerKG,2));;
}

function calculateItemTotal2(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal2').html(Utility.AddCurrencySymbol(gTotal,2));
    }
    var qty = $('#inputquantity2').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG2').html(Utility.AddCurrencySymbol(costPerKG,2));;
}


function calculateItemTotal3(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal3').html(Utility.AddCurrencySymbol(gTotal,2));
    }
    var qty = $('#inputquantity3').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG3').html(Utility.AddCurrencySymbol(costPerKG,2));;
}

function calculateItemTotal4(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal4').html(Utility.AddCurrencySymbol(gTotal,2));
    }
    var qty = $('#inputquantity4').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG4').html(Utility.AddCurrencySymbol(costPerKG,2));;
}


function calculateItemTotal5(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal5').html(Utility.AddCurrencySymbol(gTotal,2));
    }
    var qty = $('#inputquantity5').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG5').html(Utility.AddCurrencySymbol(costPerKG,2));;
}

function calculateItemTotalMOG1(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal1').html(Utility.AddCurrencySymbol(gTotal,2));
    }
    var qty = $('#inputquantity1').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG1').html(Utility.MathRound(costPerKG,2));;
}

function calculateItemTotalMOG2(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal2').html(Utility.AddCurrencySymbol(gTotal,2));
    }
    var qty = $('#inputquantity2').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG2').html(Utility.AddCurrencySymbol(costPerKG,2));;
}

function calculateItemTotalMOG3(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal3').html(Utility.AddCurrencySymbol(gTotal,2));
    }
    var qty = $('#inputquantity3').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG3').html(Utility.AddCurrencySymbol(costPerKG,2));;
}

function calculateItemTotalMOG4(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal4').html(Utility.AddCurrencySymbol(gTotal,2));
    }
    var qty = $('#inputquantity4').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG4').html(Utility.AddCurrencySymbol(costPerKG,2));;
}

function calculateItemTotalMOG5(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal5').html(Utility.AddCurrencySymbol(gTotal,2));
    }
    var qty = $('#inputquantity5').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG5').html(Utility.AddCurrencySymbol(costPerKG,2));;
}

function populateMOGGrid1(recipeID,recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG1");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }],
        groupable: false,
        editable: true,
        navigatable: true,
        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG1" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange1,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "AllergensName", title: "Allergens", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "AllergensName",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                editor: function (container, options) {
                    $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" onchange="calculateItemTotalMOG1(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },
                // template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    onchange="calculateItemTotalMOG(this)"/>',
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //    {
                //        name: 'View APL',
                //        click: function (e) {
                //            isMainRecipe = false;
                //            var gridObj = $("#gridMOG1").data("kendoGrid");
                //            currentPopuatedMOGGrid = "1";
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            MOGName = tr.MOGName;
                //            MOGSelectedArticleNumber = tr.ArticleNumber;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $("#windowEditMOGWiseAPL").kendoWindow({
                //                animation: false
                //            });
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData();
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                          
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG1").css("display", "block");
                            $("#emptymog1").css("display", "none");
                        } else {
                            $("#gridMOG1").css("display", "none");
                            $("#emptymog1").css("display", "block");
                        }
                    },
                        {
                            SiteCode: $("#inputsite").val(),
                            recipeID: 0,
                            recipeCode: recipeCode,
                            regionID: Region_ID,
                        }
                         , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange1
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation("#gridMOG1");
    var toolbar = $("#gridMOG1").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG1 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid2(recipeID,recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG2");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: true,
        navigatable: true,
        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG2" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange2,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "AllergensName", title: "Allergens", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "AllergensName",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                editor: function (container, options) {
                    $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" onchange="calculateItemTotalMOG2(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },
                // template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    onchange="calculateItemTotalMOG(this)"/>',
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
          
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //    {
                //        name: 'View APL',
                //        click: function (e) {
                //            isMainRecipe = false;
                //            var gridObj = $("#gridMOG2").data("kendoGrid");
                //            currentPopuatedMOGGrid = "2";
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            MOGName = tr.MOGName;
                //            MOGSelectedArticleNumber = tr.ArticleNumber;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $("#windowEditMOGWiseAPL").kendoWindow({
                //                animation: false
                //            });
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData();
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG2").css("display", "block");
                            $("#emptymog2").css("display", "none");
                        } else {
                            $("#gridMOG2").css("display", "none");
                            $("#emptymog2").css("display", "block");
                        }
                    },
                        {
                            SiteCode: $("#inputsite").val(),
                            recipeID: 0,
                            recipeCode: recipeCode,
                            regionID: Region_ID,
                        }
                         , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange2
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation("#gridMOG2");
    var toolbar = $("#gridMOG2").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG2 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid3(recipeID,recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG3");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: true,
        navigatable: true,
        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG3" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange3,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "AllergensName", title: "Allergens", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "AllergensName",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                editor: function (container, options) {
                    $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" onchange="calculateItemTotalMOG3(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },
                // template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    onchange="calculateItemTotalMOG(this)"/>',
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
         
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //    {
                //        name: 'View APL',
                //        click: function (e) {
                //            isMainRecipe = false;
                //            var gridObj = $("#gridMOG3").data("kendoGrid");
                //            currentPopuatedMOGGrid = "3";
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            MOGName = tr.MOGName;
                //            MOGSelectedArticleNumber = tr.ArticleNumber;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $("#windowEditMOGWiseAPL").kendoWindow({
                //                animation: false
                //            });
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData();
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG3").css("display", "block");
                            $("#emptymog3").css("display", "none");
                        } else {
                            $("#gridMOG3").css("display", "none");
                            $("#emptymog3").css("display", "block");
                        }
                    },
                        {
                            SiteCode: $("#inputsite").val(),
                            recipeID: 0,
                            recipeCode: recipeCode,
                            regionID: Region_ID,
                        }
                         , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange3
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation("#gridMOG3");
    var toolbar = $("#gridMOG3").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG3 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid4(recipeID,recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG4");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: true,
        navigatable: true,
        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG4" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange4,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "AllergensName", title: "Allergens", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "AllergensName",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                editor: function (container, options) {
                    $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" onchange="calculateItemTotalMOG4(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },
                // template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    onchange="calculateItemTotalMOG(this)"/>',
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },
           
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //    {
                //        name: 'View APL',
                //        click: function (e) {
                //            isMainRecipe = false;
                //            var gridObj = $("#gridMOG4").data("kendoGrid");
                //            currentPopuatedMOGGrid = "4";
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            MOGName = tr.MOGName;
                //            MOGSelectedArticleNumber = tr.ArticleNumber;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $("#windowEditMOGWiseAPL").kendoWindow({
                //                animation: false
                //            });
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData();
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG4").css("display", "block");
                            $("#emptymog4").css("display", "none");
                        } else {
                            $("#gridMOG4").css("display", "none");
                            $("#emptymog4").css("display", "block");
                        }
                    },
                        {
                            SiteCode: $("#inputsite").val(),
                            recipeID: 0,
                            recipeCode: recipeCode,
                            regionID: Region_ID,
                        }
                         , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange4
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation("#gridMOG4");
    var toolbar = $("#gridMOG4").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG4 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid5(recipeID,recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG5");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: true,
        navigatable: true,
        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG5" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange5,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "AllergensName", title: "Allergens", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "AllergensName",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                editor: function (container, options) {
                    $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" onchange="calculateItemTotalMOG5(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },
                // template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    onchange="calculateItemTotalMOG(this)"/>',
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //    {
                //        name: 'View APL',
                //        click: function (e) {
                //            isMainRecipe = false;
                //            var gridObj = $("#gridMOG5").data("kendoGrid");
                //            currentPopuatedMOGGrid = "5";
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            MOGName = tr.MOGName;
                //            MOGSelectedArticleNumber = tr.ArticleNumber;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $("#windowEditMOGWiseAPL").kendoWindow({
                //                animation: false
                //            });
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData();
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG5").css("display", "block");
                            $("#emptymog5").css("display", "none");
                        } else {
                            $("#gridMOG5").css("display", "none");
                            $("#emptymog5").css("display", "block");
                        }
                    },
                        {
                            SiteCode: $("#inputsite").val(),
                            recipeID: 0,
                            recipeCode: recipeCode,
                            regionID: Region_ID,
                        }
                         , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange5
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation("#gridMOG5");
    var toolbar = $("#gridMOG5").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG5 .k-grid-content").addClass("gridInside");
}





function showDetails1(e) {
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
       if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#siteconfigwindowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.ID, tr.RecipeCode);
    populateMOGGrid1(tr.ID);
    $("#recipeAllergens1").html(showAllergens(tr.AllergensName));
    $("#inputrecipealiasname1").val(tr.RecipeAlias);
    $("#btnCancel1").val($("#inputrecipecode").val());
    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");
    $('#grandTotal1').html(Utility.AddCurrencySymbol(tr.TotalCost,2));
    $('#grandCostPerKG1').html(Utility.AddCurrencySymbol(tr.TotalCost,2));

    var editor = $("#inputinstruction1").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    recipeCategoryHTMLPopulateOnEditRecipe("1", tr);
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename1").removeAttr('disabled');
    //    $("#inputuom1").removeAttr('disabled');
    //    $("#inputquantity1").removeAttr('disabled');
    //    $("#inputbase1").removeAttr('disabled');
    //    $("#btnSubmit1").css('display', 'inline');
    //    $("#btnCancel1").css('display', 'inline');
    //} else {
    //    $("#inputrecipename1").attr('disabled', 'disabled');
    //    $("#inputuom1").attr('disabled', 'disabled');
    //    $("#inputquantity1").attr('disabled', 'disabled');
    //    $("#inputbase1").attr('disabled', 'disabled');
    //    $("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details Inline - " + RecipeName);
    return true;

}

function showDetails2(e) {
    $("#siteconfigwindowEdit1").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe1").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
       if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    // $("#siteconfigwindowEdit1").parent('.k-widget').css("display", "none");
    var dialog = $("#siteconfigwindowEdit2").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
    $("#inputrecipealiasname2").val(tr.RecipeAlias);
    populateUOMDropdown2();
    populateBaseDropdown2();
    populateBaseRecipeGrid2(tr.ID, tr.RecipeCode);
    populateMOGGrid2(tr.ID, tr.RecipeCode);
    $("#recipeAllergens2").html(showAllergens(tr.AllergensName));
    $("#inputrecipealiasname2").val(tr.RecipeAlias);
    $("#recipeid2").val(tr.ID);
    $("#btnCancel2").val($("#inputrecipecode1").val());
    $("#inputrecipecode2").val(tr.RecipeCode);
    $("#inputrecipename2").val(tr.Name);
    $("#inputuom2").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity2").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase2").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase2").data('kendoDropDownList').value("No");
    $('#grandTotal2').html(Utility.AddCurrencySymbol(tr.TotalCost,2));
    $('#grandCostPerKG2').html(Utility.AddCurrencySymbol(tr.TotalCost,2));
    

    var editor = $("#inputinstruction2").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    recipeCategoryHTMLPopulateOnEditRecipe("2", tr);
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename2").removeAttr('disabled');
    //    $("#inputuom2").removeAttr('disabled');
    //    $("#inputquantity2").removeAttr('disabled');
    //    $("#inputbase2").removeAttr('disabled');
    //    $("#btnSubmit2").css('display', 'inline');
    //    $("#btnCancel2").css('display', 'inline');
    //} else {
    //    $("#inputrecipename2").attr('disabled', 'disabled');
    //    $("#inputuom2").attr('disabled', 'disabled');
    //    $("#inputquantity2").attr('disabled', 'disabled');
    //    $("#inputbase2").attr('disabled', 'disabled');
    //    $("#btnSubmit2").css('display', 'none');
    //    $("#btnCancel2").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}

function showDetails3(e) {
    $("#siteconfigwindowEdit2").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe2").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error3").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
       if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#siteconfigwindowEdit3").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
    $("#inputrecipealiasname3").val(tr.RecipeAlias);
    populateUOMDropdown3();
    populateBaseDropdown3();
    populateBaseRecipeGrid3(tr.ID, tr.RecipeCode);
    populateMOGGrid3(tr.ID,tr.RecipeCode);
    $("#recipeAllergens3").html(showAllergens(tr.AllergensName));
    $("#btnCancel3").val($("#inputrecipecode2").val());
    $("#recipeid3").val(tr.ID);
    $("#inputrecipecode3").val(tr.RecipeCode);
    $("#inputrecipename3").val(tr.Name);
    $("#inputuom3").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity3").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase3").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase3").data('kendoDropDownList').value("No");
    $('#grandTotal3').html(Utility.AddCurrencySymbol(tr.TotalCost,2));
    $('#grandCostPerKG3').html(Utility.AddCurrencySymbol(tr.TotalCost,2));
    

    var editor = $("#inputinstruction3").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    recipeCategoryHTMLPopulateOnEditRecipe("3", tr);
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename3").removeAttr('disabled');
    //    $("#inputuom3").removeAttr('disabled');
    //    $("#inputquantity3").removeAttr('disabled');
    //    $("#inputbase3").removeAttr('disabled');
    //    $("#btnSubmit3").css('display', 'inline');
    //    $("#btnCancel3").css('display', 'inline');
    //} else {
    //    $("#inputrecipename3").attr('disabled', 'disabled');
    //    $("#inputuom3").attr('disabled', 'disabled');
    //    $("#inputquantity3").attr('disabled', 'disabled');
    //    $("#inputbase3").attr('disabled', 'disabled');
    //    $("#btnSubmit3").css('display', 'none');
    //    $("#btnCancel3").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}


function showDetails4(e) {
    $("#siteconfigwindowEdit3").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe3").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error4").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
       if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#siteconfigwindowEdit4").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
    $("#inputrecipealiasname4").val(tr.RecipeAlias);
    populateUOMDropdown4();
    populateBaseDropdown4();
    populateBaseRecipeGrid4(tr.ID, tr.RecipeCode);
    populateMOGGrid4(tr.ID, tr.RecipeCode);
    $("#btnCancel4").val($("#inputrecipecode3").val());
    $("#recipeAllergens4").html(showAllergens(tr.AllergensName));
    $("#recipeid4").val(tr.ID);
    $("#inputrecipecode4").val(tr.RecipeCode);
    $("#inputrecipename4").val(tr.Name);
    $("#inputuom4").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity4").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase4").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase4").data('kendoDropDownList').value("No");
    $('#grandTotal4').html(Utility.AddCurrencySymbol(tr.TotalCost,2));
    $('#grandCostPerKG4').html(Utility.AddCurrencySymbol(tr.TotalCost,2));
    

    var editor = $("#inputinstruction4").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    recipeCategoryHTMLPopulateOnEditRecipe("4", tr);
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename4").removeAttr('disabled');
    //    $("#inputuom4").removeAttr('disabled');
    //    $("#inputquantity4").removeAttr('disabled');
    //    $("#inputbase4").removeAttr('disabled');
    //    $("#btnSubmit4").css('display', 'inline');
    //    $("#btnCancel4").css('display', 'inline');
    //} else {
    //    $("#inputrecipename4").attr('disabled', 'disabled');
    //    $("#inputuom4").attr('disabled', 'disabled');
    //    $("#inputquantity4").attr('disabled', 'disabled');
    //    $("#inputbase4").attr('disabled', 'disabled');
    //    $("#btnSubmit4").css('display', 'none');
    //    $("#btnCancel4").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}

function showDetails5(e) {
    $("#siteconfigwindowEdit4").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe4").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error4").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
       if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#siteconfigwindowEdit5").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
    $("#inputrecipealiasname5").val(tr.RecipeAlias);
    populateUOMDropdown5();
    populateBaseDropdown5();
    populateBaseRecipeGrid5(tr.ID, tr.RecipeCode);
    populateMOGGrid5(tr.ID, tr.RecipeCode);
    $("#recipeAllergens5").html(showAllergens(tr.AllergensName));
    $("#recipeid5").val(tr.ID);
    $("#inputrecipecode5").val(tr.RecipeCode);
    $("#btnCancel5").val($("#inputrecipecode4").val());
    $("#inputrecipename5").val(tr.Name);
    $("#inputuom5").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity5").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase5").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase5").data('kendoDropDownList').value("No");
    $('#grandTotal5').html(Utility.AddCurrencySymbol(tr.TotalCost,2));
    $('#grandCostPerKG5').html(Utility.AddCurrencySymbol(tr.TotalCost,2));
    

    var editor = $("#inputinstruction5").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    recipeCategoryHTMLPopulateOnEditRecipe("5", tr);
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename5").removeAttr('disabled');
    //    $("#inputuom5").removeAttr('disabled');
    //    $("#inputquantity5").removeAttr('disabled');
    //    $("#inputbase5").removeAttr('disabled');
    //    $("#btnSubmit5").css('display', 'inline');
    //    $("#btnCancel5").css('display', 'inline');
    //} else {
    //    $("#inputrecipename5").attr('disabled', 'disabled');
    //    $("#inputuom5").attr('disabled', 'disabled');
    //    $("#inputquantity5").attr('disabled', 'disabled');
    //    $("#inputbase5").attr('disabled', 'disabled');
    //    $("#btnSubmit5").css('display', 'none');
    //    $("#btnCancel5").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}


function showDetails6(e) {
    alert("Close Previous then Proceed!!!");
}
function calculateItemTotal(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal').html(Utility.AddCurrencySymbol(gTotal,2));
    }
    var qty = $('#inputquantity').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG').html(Utility.AddCurrencySymbol(costPerKG,2));;
}



//function calculateItemTotalMOG(e) {
    
//    var element = e
//    var row = element.closest("tr");
//    var grid = $("#gridMOG").data("kendoGrid");
//    var dataItem = grid.dataItem(row);
//    var qty = $(e).val();
//    dataItem.Quantity = qty;
//    dataItem.TotalCost = qty * dataItem.CostPerUOM;
//    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost);
//    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
//    var gtotalObj = $(".totalcost");
//    var len = $('.totalcost').length;
//    var gTotal = 0.0;
//    for (i = 0; i < len; i++) {
//        if (gtotalObj[i].innerText == "") {
//            continue;
//        }
//        gTotal += Number(gtotalObj[i].innerText);
//    }
//    if (!isNaN(gTotal)) {
//        $('#grandTotal').html(Utility.AddCurrencySymbol(gTotal,2));
//    }
//    var qty = $('#inputquantity').val();
//    var costPerKG = gTotal / Number(qty);
//    $('#grandCostPerKG').html(Utility.AddCurrencySymbol(costPerKG,2));;
//}


function showHideGrid(gridId, emptyGrid, uid) {
    var gridLength = $("#" + gridId).data().kendoGrid.dataSource.data().length;

    if (gridLength == 0) {
        $("#" + emptyGrid).toggle();
        $("#" + gridId).hide();
    }
    else {
        $("#" + emptyGrid).hide();
        $("#" + gridId).toggle();
    }
    // window.scrollBy(0, 100);
    if (!($("#" + gridId).is(":visible") || $("#" + emptyGrid).is(":visible"))) {
        $("#" + uid).addClass("bottomCurve");
        // $("#" + uid).parent(".lower").hide();
    } else {
        $("#" + uid).removeClass("bottomCurve");
        //  $("#" + gridId).parent(".lower").show();
    }

}

function hideGrid(gridId, emptyGrid) {

    $("#" + emptyGrid).hide();
    $("#" + gridId).show();
    //  $("#" + hideBtn).show();
    // $("#" + showBtn).hide();
}

function instructionTextEditor(divid) {

    $("#" + divid).kendoEditor({
        stylesheets: [
            // "../content/shared/styles/editor.css",
        ],
        tools: [
            "bold",
            "italic",
            "underline",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "insertUnorderedList",
            "tableWizard",
            "createTable",
            "addRowAbove",
            "addRowBelow",
            "addColumnLeft",
            "addColumnRight",
            "deleteRow",
            "deleteColumn",
            "mergeCellsHorizontally",
            "mergeCellsVertically",
            "splitCellHorizontally",
            "splitCellVertically",
            "createLink",
            "unlink",

            {
                name: "fontName",
                items: [
                    { text: "Andale Mono", value: "Andale Mono" },
                    { text: "Arial", value: "Arial" },
                    { text: "Arial Black", value: "Arial Black" },
                    { text: "Book Antiqua", value: "Book Antiqua" },
                    { text: "Comic Sans MS", value: "Comic Sans MS" },
                    { text: "Courier New", value: "Courier New" },
                    { text: "Georgia", value: "Georgia" },
                    { text: "Helvetica", value: "Helvetica" },
                    { text: "Impact", value: "Impact" },
                    { text: "Symbol", value: "Symbol" },
                    { text: "Tahoma", value: "Tahoma" },
                    { text: "Terminal", value: "Terminal" },
                    { text: "Times New Roman", value: "Times New Roman" },
                    { text: "Trebuchet MS", value: "Trebuchet MS" },
                    { text: "Verdana", value: "Verdana" },
                ]
            },
            "fontSize",
            "foreColor",
            "backColor",
        ]
    });

}


function decodeHTMLEntities(text) {
    if (text == null || text == '' || text == 'undefined')
        return "";
    var entities = [
        ['amp', '&'],
        ['apos', '\''],
        ['#x27', '\''],
        ['#x2F', '/'],
        ['#39', '\''],
        ['#47', '/'],
        ['lt', '<'],
        ['gt', '>'],
        ['nbsp', ' '],
        ['quot', '"']
    ];

    for (var i = 0, max = entities.length; i < max; ++i)
        text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1]);

    return text;
}
$("document").ready(function () {
    $("#intialImage").show();
    $("#btnGo").on("click", function () {
        Utility.Loading();
        setTimeout(function () {
            var dropdownlist = $("#inputsite").data("kendoDropDownList");
            var dataitem = dropdownlist.dataItem();

            SiteCode = dropdownlist.value();
            Region_ID = dataitem.RegionID;
            SiteID = dataitem.SiteID;
            HttpClient.MakeRequest(CookBookMasters.GetMOGDataList, function (data) {

                var dataSource = data;

                mogdata = [];
                mogdata.push({
                    "MOG_ID": "Select MOG", "MOGName": "Select MOG", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0,
                    "MOG_Code": "", "AllergensName": "None", "ArticleNumber": 0, "UOMCode": "", "DKgValue": 0
                });
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i].CostPerUOM == null) {
                        dataSource[i].CostPerUOM = 0;
                    }
                    if (dataSource[i].CostPerKG == null) {
                        dataSource[i].CostPerKG = 0;
                    }
                    if (dataSource[i].TotalCost == null) {
                        dataSource[i].TotalCost = 0;
                    }
                    if (dataSource[i].Alias != null && dataSource[i].Alias.trim() != "" && dataSource[i].Alias != dataSource[i].Name)
                        mogdata.push({
                            "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name + " (" + dataSource[i].Alias + ") ", "UOM_Code": dataSource[i].UOMCode,
                            "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG,
                            "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive, "AllergensName": dataSource[i].AllergensName,
                            "ArticleNumber": dataSource[i].ArticleNumber, "UOMCode": dataSource[i].UOMCode, "DKgValue": dataSource[i].DKgValue
                        });
                    else
                        mogdata.push({
                            "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName,
                            "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode,
                            "AllergensName": dataSource[i].AllergensName, "ArticleNumber": dataSource[i].ArticleNumber, "UOMCode": dataSource[i].UOMCode,
                            "DKgValue": dataSource[i].DKgValue, "IsActive": dataSource[i].IsActive
                        });
                }
                mogdataList = mogdata;

            }, { siteCode: SiteCode }, true);


            populateRecipeGrid(SiteCode);
            $("#searchspan").show();
            var siteName = dropdownlist.text()
            $("#location").text(" | " + siteName);
            Utility.UnLoading();
            $("#intialImage").hide();
        }, 2000);
    });

    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();

    });
});
function getMOGWiseAPLMasterData() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG_Site, function (result) {
        Utility.UnLoading();

        if (result != null) {
            mogWiseAPLMasterdataSource = result;
        }
        else {
        }
    }, { mogCode: MOGCode, SiteCode: SiteCode}
         , true);
}
function populateAPLMasterGrid() {
    Utility.Loading();


    var gridVariable = $("#gridMOGWiseAPL");
    gridVariable.html("");
    if (isMainRecipe && user.SectorNumber !== '80') {
        gridVariable.kendoGrid({
            excel: {
                fileName: "APLMaster.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            pageable: {
                numeric: false,
                previousNext: false,
                messages: {
                    display: "Total: {2} records"
                }
            },
            groupable: false,
            scrollable: true,
            noRecords: {
                template: "No Records Available"
            },

            columns: [
                {
                    field: "ArticleNumber", title: "Article Code", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ArticleDescription", title: "Article Name", width: "120px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOM", title: "UOM", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

                //        style: "text-align: left; font-weight:normal;text-align:center;"
                //    },
                //},
                //{
                //    field: "SiteName", title: "SiteName", width: "50px", attributes: {

                //        style: "text-align: left; font-weight:normal;text-align:center;"
                //    },
                //},
                {
                    field: "AllergensName", title: "Allergens", width: "50px", attributes: {

                        style: "text-align: left; font-weight:normal;text-align:center;"
                    },
                }
                ,
                {
                    field: "ArticleCost", title: "APL Cost", width: "50px", attributes: {

                        style: "text-align: left; font-weight:normal;text-align:center;"
                    },
                },

                {
                    field: "StandardCostPerKg", title: "STD CpKg", width: "50px", attributes: {

                        style: "text-align: left; font-weight:normal;text-align:center;"
                    },
                },
                {
                    template: templateFunction,
                    headerTemplate: "<span style='margin-right : 5px;'>Change APL</span>",

                    width: "70px", title: "Include",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center; vertical-align:middle"
                    }
                }
            ],
            dataSource: {

                data: mogWiseAPLMasterdataSource,
                schema: {
                    model: {
                        id: "ArticleID",
                        fields: {
                            ArticleNumber: { type: "string" },
                            ArticleDescription: { type: "string" },
                            UOM: { type: "string" },
                            ArticleType: { type: "string" },
                            Hierlevel3: { type: "string" },
                            MerchandizeCategoryDesc: { type: "string" },
                            MOGName: { type: "string" },
                            AllergensName: { type: "string" },
                            // StandardCostPerKg: { type: "decimal" },
                            // SiteCode: { type: "string" },
                            //SiteName: { type: "string" }
                        }
                    }
                },
                sort: { field: "StandardCostPerKg", dir: "desc" },
                // pageSize: 100,
            },

            //maxHeight: 250,
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {

                }

            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        });
    }
    else {
        gridVariable.kendoGrid({
            excel: {
                fileName: "APLMaster.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            pageable: false,
            groupable: false,
            scrollable: true,
            noRecords: {
                template: "No Records Available"
            },

            columns: [
                {
                    field: "ArticleNumber", title: "Article Code", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ArticleDescription", title: "Article Name", width: "120px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOM", title: "UOM", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

                //        style: "text-align: left; font-weight:normal;text-align:center;"
                //    },
                //},
                //{
                //    field: "SiteName", title: "SiteName", width: "50px", attributes: {

                //        style: "text-align: left; font-weight:normal;text-align:center;"
                //    },
                //},
                {
                    field: "AllergensName", title: "Allergens", width: "50px", attributes: {

                        style: "text-align: left; font-weight:normal;text-align:center;"
                    },
                }
                ,
                {
                    field: "ArticleCost", title: "APL Cost", width: "50px", attributes: {

                        style: "text-align: left; font-weight:normal;text-align:center;"
                    },
                },

                {
                    field: "StandardCostPerKg", title: "STD CpKg", width: "50px", attributes: {

                        style: "text-align: left; font-weight:normal;text-align:center;"
                    },
                },
            ],
            dataSource: {

                data: mogWiseAPLMasterdataSource,
                schema: {
                    model: {
                        id: "ArticleID",
                        fields: {
                            ArticleNumber: { type: "string" },
                            ArticleDescription: { type: "string" },
                            UOM: { type: "string" },
                            ArticleType: { type: "string" },
                            Hierlevel3: { type: "string" },
                            MerchandizeCategoryDesc: { type: "string" },
                            MOGName: { type: "string" },
                            AllergensName: { type: "string" },
                            // StandardCostPerKg: { type: "decimal" },
                            // SiteCode: { type: "string" },
                            //SiteName: { type: "string" }
                        }
                    }
                },
                sort: { field: "StandardCostPerKg", dir: "desc" },
                // pageSize: 100,
            },

            //maxHeight: 250,
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {

                }

            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        });
    }


    //$(".k-label")[0].innerHTML.replace("items", "records");
}

function templateFunction(dataItem) {
    var aplGrid = $("#gridMOGWiseAPL").data("kendoGrid");

    var maxStandardCost = 0;
    if (aplGrid != null && aplGrid._data != null && aplGrid._data[0] != undefined) {
        maxStandardCost = aplGrid._data[0].StandardCostPerKg;
        console.log("MAX" + maxStandardCost)
    }

    var cell = "";
    var item = "";
    
    item += "<label>"
    if (MOGSelectedArticleNumber == null || MOGSelectedArticleNumber == "") {
        if (dataItem.IsMaxAPLCost) {
            item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "' onchange='radioButtonChange()' checked=checked />";
        } else {
            item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "' onchange='radioButtonChange()' class=''/>";
        }
    }
    else {
        if (dataItem.ArticleNumber === MOGSelectedArticleNumber) {
            item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "' onchange='radioButtonChange()' checked=checked />";
        } else {
            item += "<input type='radio' name='radioLineSelector' value='" + dataItem.ArticleNumber + "' onchange='radioButtonChange()' class=''/>";
        }
    }
    //item += categories[i].text;
    item += "</label>"
    item += "</br>";

    cell += item;
    return cell;
};

function radioButtonChange() {
    var selectedVal = "";
    var selected = $("input[type='radio'][name='radioLineSelector']:checked");
    var recipeAllergens = "";
       if (selected.length > 0) {
        selectedVal = selected.val();
           var moggrid = $("#gridMOG" + currentPopuatedMOGGrid + "").data("kendoGrid").dataSource;
           var aplGrid = $("#gridMOGWiseAPL").data("kendoGrid");
           var allergen = aplGrid._data.filter(m => m.ArticleNumber == selectedVal)
           var commaString = ",";
           moggrid._data.forEach(function (item, index) {
               if (item.MOGCode == MOGCode) {
                   item.ArticleNumber = selectedVal;
                   item.AllergensName = allergen[0].AllergensName;
                   item.CostPerUOM = allergen[0].StandardCostPerKg;
                   var qty = item.Quantity;
                   var recipeQty = $("#inputquantity").val();
                   var perc = (qty / recipeQty)*100;
                   item.Quantity = qty;
                   item.IngredientPerc = perc.toFixed(2);
                   item.TotalCost = qty * item.CostPerUOM;
                   item.TotalCost = Utility.MathRound(item.TotalCost, 2);
                   CalculateGrandTotal('');
               }
               commaString = index == 0 ? "" : ",";
               recipeAllergens = item.AllergensName == "None" ? recipeAllergens : recipeAllergens + commaString + item.AllergensName;
           });
           $("#gridMOG" + currentPopuatedMOGGrid + "").data('kendoGrid').refresh();
           bindRecipeAllergensName(recipeAllergens);
    }
    else {
        toastr.error("Please select at least one APL.");
        return false;
    }
}

function combineNestedRecipeAllergens(gridName, commaString) {
    var moggrid = $("#gridMOG" + currentPopuatedMOGGrid + "").data("kendoGrid").dataSource;
    moggrid._data.forEach(function (item, index) {
        if (commaString === "") { commaString = index == 0 ? "" : ","; }
        if (item.AllergensName !== "") {
            commaString = item.AllergensName == "None" ? commaString : + commaString + item.AllergensName;
        }
    });
    return commaString;
}

function bindRecipeAllergensName(recipeAllergens) {
    var list = recipeAllergens.split(',');
    var arr = [];
    $.each(list, function (i, val) {
        console.log(val);
        if (val !== undefined && val.trim() !== "" && arr.includes(val.trim())) {
            console.log("exist" + val.trim())
        }
        else {
            arr.push(val.trim());
            console.log("notexist" + val.trim())
        }
    });
    var allergenString = arr.toString();
    //if (allergenString.match(/,.*,/)) { // Check if there are 2 commas
    //    allergenString = allergenString.replace(',', ''); // Remove the first one
    //}
    $("#recipeAllergens" + currentPopuatedMOGGrid + "").html(showAllergens(allergenString));

}



function checkItemAlreadyExist(selectedId, gridID, isBR) {
    var dataExists = false;
    var gridObj = $("#" + gridID + "").data("kendoGrid");
    var data = gridObj.dataSource.data();
    var existingData = [];
    if (isBR) { existingData = data.filter(m => m.BaseRecipe_ID == selectedId); }
    else { existingData = data.filter(m => m.MOG_ID == selectedId); }
    if (existingData.length == 2) {
        dataExists = true;
    }
    return dataExists;
}

function validateDataBeforeSave(brGridID, mogGridId) {
    var isValid = true;

    if (user.SectorNumber != "20") {
        var brGridObj = $("#" + brGridID + "").data("kendoGrid");
        var data = brGridObj.dataSource.data();
        for (var i = 0; i < data.length; i++) {
            if (data[i].BaseRecipe_ID === '' || data[i].BaseRecipe_ID === null || data[i].Quantity === '' || data[i].Quantity === 0 || data[i].Quantity === "0") {
                isValid = false;
            }
        }
    }

    var mogGridObj = $("#" + mogGridId + "").data("kendoGrid");
    var mogdata = mogGridObj.dataSource.data();
    for (var i = 0; i < mogdata.length; i++) {
        if (mogdata[i].MOG_ID === '' || mogdata[i].MOG_ID === null || mogdata[i].Quantity === '' || mogdata[i].Quantity === 0 || mogdata[i].Quantity === "0") {
            isValid = false;
        }
    }
    return isValid;
}

function hideShowRecipeWindow(hideWIndow, showWindow) {
    $("#" + hideWIndow + "").hide();
    $("#" + showWindow + "").show();

    var dialog = $("#siteconfigwindowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
}


function populateRecipeGrid(SiteCode) {

    Utility.Loading();
    var gridVariable = $("#gridRecipe");//Hare Krishna!!
    gridVariable.html("");
    if ( user.UserRoleId == 13) {
        gridVariable.kendoGrid({
            excel: {
                fileName: "Recipe.xlsx",
                filterable: true,
                allPages: true
            },
            //selectable: "cell",
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: true,
            //reorderable: true,
            scrollable: true,
            columns: [
                {
                    field: "RecipeCode", title: "Recipe Code", width: "70px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Recipe Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "RecipeAlias", title: "Alias Name", width: "100px", attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    }
                //},
                {
                    field: "AllergensName", title: "Contains", width: "100px",
                    headerAttributes: {
                        style: "text-align: left;"
                    },
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "NGAllergens", title: "May Contains (NG)", width: "100px",
                    headerAttributes: {
                        style: "text-align: left;"
                    },
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Quantity", title: "Quantity", width: "50px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "40px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "RecipeType", title: "Recipe Type", width: "70px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: left"
                    },
                    //  template: '# if (IsBase == false) {#<div>Final Recipe</div>#} else {#<div> Base Recipe</div>#}#',
                },
                
                {
                    field: "Edit", title: "Action", width: "80px",
                    headerAttributes: {

                        style: "text-align: center"
                    },

                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    command: [
                        //{
                        //    name: 'Modify',
                        //    click: function (e) {

                        //        var gridObj = $("#gridRecipe").data("kendoGrid");
                        //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                        //        if (tr.Status == 1)
                        //            return;
                        //        Utility.Loading();
                        //        setTimeout(function () {

                        //            datamodel = tr;
                        //            RecipeName = tr.Name;
                        //            populateUOMDropdown();
                        //            recipeCategoryHTMLPopulateOnEditRecipe("", tr);
                        //            $("#mainContent").hide();
                        //            $("#windowEdit").show();


                        //            populateBaseDropdown();
                        //            Recipe_ID = tr.Recipe_ID;
                        //            populateMOGGrid(tr.Recipe_ID, tr.RecipeCode);
                        //            populateBaseRecipeGrid(tr.Recipe_ID, tr.RecipeCode);

                        //            $("#recipeAllergens").html(showAllergens(tr.AllergensName));

                        //            $("#recipeid").val(tr.ID);
                        //            $("#inputrecipecode").val(tr.RecipeCode);
                        //            $("#inputrecipename").val(tr.Name);
                        //            $("#inputrecipealiasname").val(tr.RecipeAlias);
                        //            $("#inputuom").data('kendoDropDownList').value(tr.UOM_ID);
                        //            $("#inputquantity").val(tr.Quantity);
                        //            if (tr.IsBase == true)
                        //                $("#inputbase").data('kendoDropDownList').value("Yes");
                        //            else
                        //                $("#inputbase").data('kendoDropDownList').value("No");

                        //            $('#grandTotal').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
                        //            $('#grandCostPerKG').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
                        //            $('#regiongrandTotal').html(Utility.AddCurrencySymbol(tr.RegionTotalCost, 2));
                        //            oldcost = tr.TotalCost;// tr.RegionTotalCost;
                        //            var editor = $("#inputinstruction").data("kendoEditor");
                        //            editor.value(decodeHTMLEntities(tr.Instructions));
                        //            var change = 0; //(tr.TotalCost - oldcost).toFixed(2);
                        //            var changeperc = 0; //(((change*100) / oldcost)).toFixed(2);
                        //            $(".totmoglbl").show();
                        //            if (user.SectorNumber == "20") {
                        //                $('#inputrecipename').removeAttr("disabled");
                        //                $('#inputquantity').removeAttr("disabled");
                        //                $('#inputrecipealiasname').removeAttr("disabled");
                        //                $("#inputbase").data("kendoDropDownList").enable(true); 
                        //                $(".changegrandtotal").css("display", "none");
                        //                $("#percentageChangeDiv").css("display", "none");
                        //                $("#btnSubmit").show();
                        //                $("#btnPublish").show();
                        //                $(".totmoglbl").hide();
                        //            }
                        //            else if (user.SectorNumber == "80") {
                        //                $("#btnSubmit").hide();
                        //                $("#btnPublish").hide();
                        //            }
                        //            else {
                        //                $("#btnSubmit").show();
                        //                $("#btnPublish").show();
                        //                $('#inputquantity').attr('disabled', 'disabled');
                        //                $('#inputrecipename').attr('disabled', 'disabled');
                        //                $('#inputrecipealiasname').attr('disabled', 'disabled');
                        //                $("#inputbase").data("kendoDropDownList")   .enable(false); 
                        //                $(".changegrandTotal").css("display", "block");
                        //                $("#percentageChangeDiv").css("display", "block");
                        //                $("#changegrandTotal").html(Utility.AddCurrencySymbol(change, 2));

                        //                $("#totalchangepercent").html(Utility.AddCurrencySymbol(changeperc, 2));
                        //                if (isNaN(oldcost)) {
                        //                    $("#changegrandTotal").hide();
                        //                    $("#totalchangepercent").hide();
                        //                } else {
                        //                    $("#changegrandTotal").show();
                        //                    $("#totalchangepercent").show();
                        //                }
                        //                if (change == 0) {
                        //                    $("#changegrandTotal").html("No Change");
                        //                }

                        //            }



                        //            var dropdownlist = $("#inputsite").data("kendoDropDownList");
                        //            $("#topHeading").text("Unit Recipe Configuration");//.append("(" + siteName + ")");
                        //            hideGrid('gridBaseRecipe', 'emptyBr', 'baseRecipePlus', 'baseRecipeMinus');
                        //            hideGrid('gridMOG', 'emptymog', 'mogPlus', 'mogMinus');

                        //        }, 2000);
                        //        Utility.UnLoading();
                        //    }
                        //},
                                               
                        {
                            name: 'Map Nutrients',
                            template: function (dataItem) {
                                var html = `<div title="Map Nutrients" onClick="viewNutrients(this)"  style='color: #699e9b;margin-left: 50px' class='MapNutrients'>Nutrition Data</div>`;
                                return html;
                            }
                        },
                        //{
                        //    name: 'Print',
                        //    template: function (dataItem) {
                        //        var html = '<div title="Print" onClick="recipeGridPrint(this)"  style="color:#8f9090; margin-left:8px" class="fa fa-print PrintRecipe"></div>';
                        //        return html;
                        //    },

                        //},
                    ],
                }
            ],
            dataSource: {
                sort: { field: "RecipeType", dir: "desc" },
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteRecipeDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        },
                            {
                                SiteCode: SiteCode, condition: "All"
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            RecipeCode: { type: "string", editable: false },
                            Name: { type: "string", editable: false },
                            AllergensName: { type: "string", editable: false },
                            NGAllergens: { type: "string", editable: false },
                            UOMName: { type: "string", editable: false },
                            IsBase: { type: "boolean", editable: false },
                            IsActive: { type: "boolean", editable: false },
                            RecipeType: { type: "string", editable: false },
                        }
                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            //  height: 500,
            dataBound: function (e) {

                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.Status == 1) {
                        $(this).find(".k-grid-Modify").addClass("k-state-disabled");
                        $(this).find(".PrintRecipe").hide();
                    }

                    //Krish
                    for (item of applicationSettingData) {
                        var code = "FLX-00011";
                        if (user.SectorNumber == "80")  //Healthcare
                            code = "FLX-00011";
                        else if (user.SectorNumber == "30")  //Core
                            code = "FLX-00011";
                        else if (user.SectorNumber == "20")  //Manufac
                            code = "FLX-00015";

                        if (item.Code == code && item.IsActive == true && item.Value == "0")//item.Name == "Print Recipe Button Display Sector"
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == code && item.IsActive != true)
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == code && item.IsActive == true && item.Value == "1")
                            $(this).find(".PrintRecipe").show();
                        //Krish 20-07-2022
                        var code = "FLX-00014";
                        if (user.SectorNumber == "80")  //Healthcare
                            code = "FLX-00018";
                        else if (user.SectorNumber == "30")  //Core
                            code = "FLX-00014";
                        else if (user.SectorNumber == "20")  //Manufac
                            code = "FLX-00018";
                        if (item.Code == code && item.IsActive == true && item.Value == "0")//item.Name == "Info Recipe Button Display Sector"
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == code && item.IsActive != true)
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == code && item.IsActive == true && item.Value == "1")
                            $(this).find(".InfoRecipe").show();
                    }
                });

                var items = e.sender.items();

                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }


                }
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });

                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }
        })
    }
    else {
        gridVariable.kendoGrid({
            excel: {
                fileName: "Recipe.xlsx",
                filterable: true,
                allPages: true
            },
            //selectable: "cell",
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: true,
            //reorderable: true,
            scrollable: true,
            columns: [
                {
                    field: "RecipeCode", title: "Recipe Code", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Recipe Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "RecipeAlias", title: "Alias Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Quantity", width: "50px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "60px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "RecipeType", title: "Recipe Type", width: "50px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    //  template: '# if (IsBase == false) {#<div>Final Recipe</div>#} else {#<div> Base Recipe</div>#}#',
                },
                //{
                //    field: "Status", title: "Configured", width: "50px", attributes: {
                //        class: "mycustomstatus",
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    template: '# if (Status == "1") {#<span>No</span>#} else if (Status == "2") {#<span>Yes</span>#}#',
                //    headerAttributes: {

                //        style: "text-align: center"
                //    },
                //},

                {
                    field: "Edit", title: "Action", width: "90px",
                    headerAttributes: {

                        style: "text-align: center"
                    },

                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Modify',
                            click: function (e) {

                                var gridObj = $("#gridRecipe").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (tr.Status == 1 && user.SectorNumber != "20")
                                    return;
                                Utility.Loading();
                                setTimeout(function () {

                                    datamodel = tr;
                                    RecipeName = tr.Name;
                                    populateUOMDropdown();
                                    recipeCategoryHTMLPopulateOnEditRecipe("", tr);
                                    $("#mainContent").hide();
                                    $("#windowEdit").show();


                                    populateBaseDropdown();
                                    Recipe_ID = tr.Recipe_ID;
                                    populateMOGGrid(tr.Recipe_ID, tr.RecipeCode);
                                    populateBaseRecipeGrid(tr.Recipe_ID, tr.RecipeCode);

                                    $("#recipeAllergens").html(showAllergens(tr.AllergensName));

                                    $("#recipeid").val(tr.ID);
                                    $("#inputrecipecode").val(tr.RecipeCode);
                                    $("#inputrecipename").val(tr.Name);
                                    $("#inputrecipealiasname").val(tr.RecipeAlias);
                                    $("#inputuom").data('kendoDropDownList').value(tr.UOM_ID);
                                    $("#inputquantity").val(tr.Quantity);
                                    if (tr.IsBase == true)
                                        $("#inputbase").data('kendoDropDownList').value("Yes");
                                    else
                                        $("#inputbase").data('kendoDropDownList').value("No");

                                    $('#grandTotal').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
                                    $('#grandCostPerKG').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
                                    $('#regiongrandTotal').html(Utility.AddCurrencySymbol(tr.RegionTotalCost, 2));
                                    oldcost = tr.TotalCost;// tr.RegionTotalCost;
                                    var editor = $("#inputinstruction").data("kendoEditor");
                                    editor.value(decodeHTMLEntities(tr.Instructions));
                                    var change = 0; //(tr.TotalCost - oldcost).toFixed(2);
                                    var changeperc = 0; //(((change*100) / oldcost)).toFixed(2);
                                    $(".totmoglbl").show();
                                    if (user.SectorNumber == "20") {
                                        $('#inputrecipename').removeAttr("disabled");
                                        $('#inputquantity').removeAttr("disabled");
                                        $('#inputrecipealiasname').removeAttr("disabled");
                                        $("#inputbase").data("kendoDropDownList").enable(true); 
                                        $(".changegrandtotal").css("display", "none");
                                        $("#percentageChangeDiv").css("display", "none");
                                        $("#btnSubmit").show();
                                        $("#btnPublish").show();
                                        $(".totmoglbl").hide();
                                    }
                                    else if (user.SectorNumber == "80") {
                                        $("#btnSubmit").hide();
                                        $("#btnPublish").hide();
                                    }
                                    else {
                                        $("#btnSubmit").show();
                                        $("#btnPublish").show();
                                        $('#inputquantity').attr('disabled', 'disabled');
                                        $('#inputrecipename').attr('disabled', 'disabled');
                                        $('#inputrecipealiasname').attr('disabled', 'disabled');
                                        $("#inputbase").data("kendoDropDownList")   .enable(false); 
                                        $(".changegrandTotal").css("display", "block");
                                        $("#percentageChangeDiv").css("display", "block");
                                        $("#changegrandTotal").html(Utility.AddCurrencySymbol(change, 2));

                                        $("#totalchangepercent").html(Utility.AddCurrencySymbol(changeperc, 2));
                                        if (isNaN(oldcost)) {
                                            $("#changegrandTotal").hide();
                                            $("#totalchangepercent").hide();
                                        } else {
                                            $("#changegrandTotal").show();
                                            $("#totalchangepercent").show();
                                        }
                                        if (change == 0) {
                                            $("#changegrandTotal").html("No Change");
                                        }

                                    }
                                    var dropdownlist = $("#inputsite").data("kendoDropDownList");
                                    $("#topHeading").text("Unit Recipe Configuration");//.append("(" + siteName + ")");
                                    hideGrid('gridBaseRecipe', 'emptyBr', 'baseRecipePlus', 'baseRecipeMinus');
                                    hideGrid('gridMOG', 'emptymog', 'mogPlus', 'mogMinus');
                                    getdkgtotal();
                                }, 2000);
                                Utility.UnLoading();
                            }
                        },
                        {
                            name: 'Print',
                            template: function (dataItem) {
                                var html = '<div title="Print" onClick="recipeGridPrint(this)"  style="color:#8f9090; margin-left:8px" class="fa fa-print PrintRecipe"></div>';
                                return html;
                            },

                        },
                        {
                            name: 'Info',
                            template: function (dataItem) {
                                var html = '<div title="View Recipe MOG(s)" onClick="recipeGridInfo(this)"  style="color:#8f9090; margin-left:8px" class="fa fa-eye InfoRecipe"></div>';
                                return html;
                            },

                        },
                       
                    ],
                }
            ],
            dataSource: {
                sort: { field: "RecipeType", dir: "desc" },
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteRecipeDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                //Krish 20-07-2022
                                copyData = result;
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        },
                            {
                                SiteCode: SiteCode, condition: "All"
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            RecipeCode: { type: "string", editable: false },
                            Name: { type: "string", editable: false },
                            UOMName: { type: "string", editable: false },
                            IsBase: { type: "boolean", editable: false },
                            IsActive: { type: "boolean", editable: false },
                            RecipeType: { type: "string", editable: false },
                        }
                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            //  height: 500,
            dataBound: function (e) {

                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.Status == 1 && user.SectorNumber != "20") {
                        $(this).find(".k-grid-Modify").addClass("k-state-disabled");
                        $(this).find(".PrintRecipe").hide();
                    }
                    //Krish
                    for (item of applicationSettingData) {
                        if (item.Code == "FLX-00011" && item.IsActive == true && item.Value == "0")//item.Name == "Print Recipe Button Display Sector"
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == "FLX-00011" && item.IsActive != true)
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == "FLX-00011" && item.IsActive == true && item.Value == "1")
                            $(this).find(".PrintRecipe").show();
                        //Krish 20-07-2022
                        if (item.Code == "FLX-00014" && item.IsActive == true && item.Value == "0")//item.Name == "Info Recipe Button Display Sector"
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == "FLX-00014" && item.IsActive != true)
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == "FLX-00014" && item.IsActive == true && item.Value == "1")
                            $(this).find(".InfoRecipe").show();
                    }
                });

                var items = e.sender.items();

                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }


                }
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });

                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }
        })
    }
}
//});

function addstandardDishWeightND() {
    var dishND = FilterItemDishRecipeNutritionData[0];
    var dishNDServedPortion = Utility.MathRound(dishND.ServedPortion * 1000, 2);
    dishHeaderString = dishND.DishCode + " | " + dishND.DishName + " - " + "Nutrition Data";
    $("#topHeading").text(dishHeaderString);
    
    var item  ={
        "ItemName": "Sample Data",
        "ItemCode": "-",
        "DishCode": dishND.DishCode,
        "DishName": dishND.DishName,
        "ServedPortion": .10,
        "ServedPortionUOM": "Kg",
        "Energy":Utility.MathRound((dishND.Energy/dishNDServedPortion)*100,2),
        "Carbs":Utility.MathRound((dishND.Carbs/dishNDServedPortion)*100,2),
        "Protein":Utility.MathRound((dishND.Protein/dishNDServedPortion)*100,2),
        "Total_Fat":Utility.MathRound((dishND.Total_Fat/dishNDServedPortion)*100,2),
        "Total_Dietary_Fiber":Utility.MathRound((dishND.Total_Dietary_Fiber/dishNDServedPortion)*100,2),
        "Sodium":Utility.MathRound((dishND.Sodium/dishNDServedPortion)*100,2),
        "Potassium":Utility.MathRound((dishND.Potassium/dishNDServedPortion)*100,2),
        "Added_Sugar":Utility.MathRound((dishND.Added_Sugar/dishNDServedPortion)*100,2),
        "GI_c___5":Utility.MathRound(dishND.GI_c___5,2),
        "GL_c___4":Utility.MathRound((dishND.GL_c___4/dishNDServedPortion)*100,2),
        "Sat_Fat":Utility.MathRound((dishND.Sat_Fat/dishNDServedPortion)*100,2),
        "MUFA":Utility.MathRound((dishND.MUFA/dishNDServedPortion)*100,2),
        "PUFA":Utility.MathRound((dishND.PUFA/dishNDServedPortion)*100,2),
        "Insoluble__Fiber":Utility.MathRound((dishND.Insoluble__Fiber/dishNDServedPortion)*100,2),
        "Soluble_Fiber":Utility.MathRound((dishND.Soluble_Fiber/dishNDServedPortion)*100,2),
        "Iron":Utility.MathRound((dishND.Iron/dishNDServedPortion)*100,2),
        "Calcium":Utility.MathRound((dishND.Calcium/dishNDServedPortion)*100,2),
        "Zinc":Utility.MathRound((dishND.Zinc/dishNDServedPortion)*100,2),
        "Vitamin_A_beta__carotene_":Utility.MathRound((dishND.Vitamin_A_beta__carotene_/dishNDServedPortion)*100,2),
        "Vitamin_C":Utility.MathRound((dishND.Vitamin_C/dishNDServedPortion)*100,2),
        "GI_of_ingredients": dishND.GI_of_ingredients,
    };

    FilterItemDishRecipeNutritionData.splice(0, 0, item);
}

function viewNutrients(e) {
    Utility.Loading();
    setTimeout(function () {
        FilterItemDishRecipeNutritionData = [];
        var gridObj = $("#gridRecipe").data("kendoGrid");
        var tr = gridObj.dataItem($(e).closest("tr"));
        RecipeCode = tr.RecipeCode;
        if (ItemDishRecipeNutritionData == null || ItemDishRecipeNutritionData == undefined || ItemDishRecipeNutritionData.length == 0) {
            HttpClient.MakeRequest(CookBookMasters.GetItemDishRecipeNutritionData, function (data) {
                ItemDishRecipeNutritionData = data;
                FilterItemDishRecipeNutritionData = ItemDishRecipeNutritionData.filter(m => m.RecipeCode == RecipeCode);
                if (FilterItemDishRecipeNutritionData == null || FilterItemDishRecipeNutritionData.length == 0) {
                    toastr.info("No data found");
                }
                else {
                    addstandardDishWeightND();
                    $("#NutritionDataDiv").show();
                    $("#RecipeMaster").hide();
                    populateNutrientMasterGrid();
                }
            }, { SiteCode: $("#inputsite").val() }, true);
        }
        else {
            FilterItemDishRecipeNutritionData = ItemDishRecipeNutritionData.filter(m => m.RecipeCode == RecipeCode);
            if (FilterItemDishRecipeNutritionData == null || FilterItemDishRecipeNutritionData.length == 0) {
                toastr.info("No data found");
            }
            else {
                addstandardDishWeightND();
                $("#NutritionDataDiv").show();
                $("#RecipeMaster").hide();
                populateNutrientMasterGrid();
            }
        }
       
        Utility.UnLoading();
    }, 2000);
}

function populateNutrientMasterGrid() {
    Utility.Loading();


    var gridVariable = $("#gridRecipeWiseNutrients");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "RecipeMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: false,
        //filterable: {
        //    extra: true,
        //    operators: {
        //        string: {
        //            contains: "Contains",
        //            startswith: "Starts with",
        //            eq: "Is equal to",
        //            neq: "Is not equal to",
        //            doesnotcontain: "Does not contain",
        //            endswith: "Ends with"
        //        }
        //    }
        //},
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Total: {2} records"
            }
        },
      //  reorderable: true,
        groupable: false,
       // resizable: true,
        scrollable: true,
       // width: 2200,
        columns: [
            {
                field: "ItemCode", title: "Item Code", width: "80px", locked: true,
                lockable: false, attributes: {
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "ItemName", title: "Item Name", width: "250px", locked: true,
                lockable: false, attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ServedPortionUOM", title: "Served Portion UOM", width: "70px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ServedPortion", title: "Served Portion", width: "70px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },

            { field: "Energy", title: "Energy (kal)", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Carbs", title: "Carbs (g)", width: "60px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Protein", title: "Protein (g)", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Total_Fat", title: "Total Fat (g)", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Total_Dietary_Fiber", title: "Total Dietary Fiber (g)", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Sodium", title: "Sodium (mg)", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Potassium", title: "Potassium (mg)", width: "80px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Added_Sugar", title: "Added Sugar (g)", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "GI_c___5", title: "GI-c +_5", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "GL_c___4", title: "GL-c +_4", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Sat_Fat", title: "Sat Fat (g)", width: "60px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "MUFA", title: "MUFA (mg)", width: "60px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "PUFA", title: "PUFA (mg)", width: "60px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Insoluble__Fiber", title: "Insoluble Fiber (g)", width: "80px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Soluble_Fiber", title: "Soluble Fiber (g)", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Iron", title: "Iron (mg)", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Calcium", title: "Calcium (mg)", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Zinc", title: "Zinc (mg)", width: "60px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Vitamin_A_beta__carotene_", title: "Vitamin A(beta- carotene) (mcg)", width: "120px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "Vitamin_C", title: "Vitamin C (mg)", width: "70px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },
            { field: "GI_of_ingredients", title: "GI of Ingredients", width: "90px", format: Utility.Decimal_Format, attributes: { style: "text-align: left; font-weight:normal" }, },

        ],
        dataSource: {
            data: FilterItemDishRecipeNutritionData,//ItemDishRecipeNutritionData.filter(m => m.RecipeCode == RecipeCode),
            schema: {
                model: {
                    fields: {
                        ItemName: { type: "string" },
                        DishCode: { type: "string" },
                        DishName: { type: "string" },
                        ServedPortion: { type: "string" },
                        ServedPortionUOM: { type: "string" },
                        Energy: { type: "int" },
                        Carbs: { type: "int" },
                        Protein: { type: "int" },
                        Total_Fat: { type: "int" },
                        Total_Dietary_Fiber: { type: "int" },
                        Sodium: { type: "int" },
                        Potassium: { type: "int" },
                        Added_Sugar: { type: "int" },
                        GI_c___5: { type: "int" },
                        GL_c___4: { type: "int" },
                        Sat_Fat: { type: "int" },
                        MUFA: { type: "int" },
                        PUFA: { type: "int" },
                        Insoluble__Fiber: { type: "int" },
                        Soluble_Fiber: { type: "int" },
                        Iron: { type: "int" },
                        Calcium: { type: "int" },
                        Zinc: { type: "int" },
                        Vitamin_A_beta__carotene_: { type: "int" },
                        Vitamin_C: { type: "int" },
                        GI_of_ingredients: { type: "int" },
                    }
                }
            },
          //  pageSize: 100,
        },
        height: 492,
        minHeight: 200,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
             var grid = this;
            var rows = grid.items();

            $(rows).each(function (e) {
                var row = this;
                var dataItem = grid.dataItem(row);

                if (dataItem.ItemName == "Sample Data") {
                    row.className = 'row-heighlight'
                }
            });
        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
}


function recipeCategoryHTMLPopulateOnEditRecipe(level, tr) {
    if (user.SectorNumber == "20" ){
        populateRecipeCategoryUOMDropdown(level);
        populateWtPerUnitUOMDropdown(level);
        populateGravyWtPerUnitUOMDropdown(level);
        if (tr.RecipeUOMCategory)
            $("#inputrecipecum" + level + "").data('kendoDropDownList').value(tr.RecipeUOMCategory);
        
        var recipeCategory = tr.RecipeUOMCategory;
        if (tr.RecipeUOMCategory == undefined || tr.RecipeUOMCategory == null || tr.RecipeUOMCategory == "") {
            recipeCategory = "UOM-00001";
            $("#inputrecipecum" + level + "").data('kendoDropDownList').value("UOM-00001");
        }
        if (level === "") {
            OnRecipeUOMCategoryChange(recipeCategory, level);
        }

        if (tr.WeightPerUnit)
            $("#inputwtperunit" + level + "").val(tr.WeightPerUnit);
        if (tr.WeightPerUnitGravy)
            $("#inputgravywtperunit" + level + "").val(tr.WeightPerUnitGravy);
        if (tr.WeightPerUnitUOM)
            $("#inputwtperunituom" + level + "").data("kendoDropDownList").value(tr.WeightPerUnitUOM);
        if (tr.WeightPerUnitUOMGravy)
            $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value(tr.WeightPerUnitUOMGravy);
        
    }
}

function populateWtPerUnitUOMDropdown0() {
    $("#inputwtperunituom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}

function populateGravyWtPerUnitUOMDropdown0() {
    $("#inputgravywtperunituom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}

function populateRecipeCategoryUOMDropdown0() {
    $("#inputrecipecum0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: recipecategoryuomdata,
        index: 1,
        change: function (e) {
            OnRecipeUOMCategoryChange(e.sender.dataItem().UOMCode, "0")

        }
    });
}

function populateWtPerUnitUOMDropdown(level) {
    $("#inputwtperunituom" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}

function populateGravyWtPerUnitUOMDropdown(level) {
    $("#inputgravywtperunituom" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata.filter(m => m.UOMCode == 'UOM-00001' || m.UOMCode == 'Select'),
        index: 1,
    });
}


function populateRecipeCategoryUOMDropdown(level) {
    $("#inputrecipecum" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: recipecategoryuomdata,
        index: 1,
        change: function (e) {
            OnRecipeUOMCategoryChange(e.sender.dataItem().UOMCode, level)

        }
    });
}

function OnRecipeUOMCategoryChange(recipeCategory, level) {
    $("#inputwtperunituom" + level + "").closest(".k-widget").hide();
    $("#inputgravywtperunituom" + level + "").closest(".k-widget").hide();
    $("#inputuom" + level + "").closest(".k-widget").hide();
    $("#linputwtperunituom" + level + "").text("-");
    $("#linputgravywtperunituom" + level + "").text("-");
    $("#linputuom" + level + "").text("-");

    $("#inputwtperunit" + level + "").val("");

    if ($("#inputuom" + level + "").data("kendoDropDownList") != undefined) {
        $("#inputuom" + level + "").data("kendoDropDownList").value("Select");
    }
    $("#inputgravywtperunit" + level + "").val("");
    $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("Select");
    $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("Select");
    var inputwtperunituom0 = $("#inputwtperunituom" + level + "").data("kendoDropDownList");
    inputwtperunituom0.enable(false);
    var inputgravywtperunituom0 = $("#inputgravywtperunituom" + level + "").data("kendoDropDownList");
    inputgravywtperunituom0.enable(false);

    if (recipeCategory == "UOM-00005" || recipeCategory == "UOM-00006") {
        var rUOM = recipeCategory == "UOM-00005" ? "1" : "2";
        var rUOMText = uomdata.find(m => m.value == rUOM).text;
        $("#inputwtperunit" + level + "").prop("disabled", true);
        $("#inputgravywtperunit" + level + "").prop("disabled", true);
        $("#inputuom" + level + "").data("kendoDropDownList").value(rUOM);
        $("#linputuom" + level + "").text(rUOMText);
        var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputwtperunituom" + level + "").text("-");
        $("#linputgravywtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00007") {
        $("#inputwtperunit" + level + "").prop("disabled", false);
        $("#inputgravywtperunit" + level + "").prop("disabled", true);
        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
        var rUOMText = uomdata.find(m => m.value == 3).text;
        $("#linputuom" + level + "").text(rUOMText);
        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").text;
        $("#linputwtperunituom" + level + "").text(rUText);
        var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputgravywtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00009") {
        $("#inputwtperunit" + level + "").prop("disabled", true);
        $("#inputgravywtperunit" + level + "").prop("disabled", false);
        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
        var rUOMText = uomdata.find(m => m.value == 3).text;
        $("#linputuom" + level + "").text(rUOMText);
        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").text;
        $("#linputgravywtperunituom" + level + "").text(rUText);
        var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputwtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00008") {
        $("#inputwtperunit" + level + "").prop("disabled", false);
        $("#inputgravywtperunit" + level + "").prop("disabled", false);
        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").text;
        $("#linputgravywtperunituom" + level + "").text(rUText);
        $("#linputwtperunituom" + level + "").text(rUText);
        var rUOMText = uomdata.find(m => m.value == 3).text;
        $("#linputuom" + level + "").text(rUOMText);
    }
};

//function OnRecipeUOMCategoryChange(recipeCategory, level) {
//    $("#inputwtperunit" + level + "").val("");
//    if ($("#inputuom" + level + "").data("kendoDropDownList") != undefined) {
//        $("#inputuom" + level + "").data("kendoDropDownList").value("Select");
//    }
//    $("#inputgravywtperunit" + level +"").val("");
//    $("#inputwtperunituom" + level +"").data("kendoDropDownList").value("Select");
//    $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("Select");
//      var inputwtperunituom0 = $("#inputwtperunituom" + level + "").data("kendoDropDownList");
//      inputwtperunituom0.enable(false);
//      var inputgravywtperunituom0 = $("#inputgravywtperunituom" + level + "").data("kendoDropDownList");
//      inputgravywtperunituom0.enable(false);
//    if (recipeCategory == "UOM-00005" || recipeCategory == "UOM-00006") {
//        var rUOM = recipeCategory == "UOM-00005" ? "1" : "2";
//        $("#inputwtperunit"+level+"").prop("disabled", true);
//        $("#inputgravywtperunit" + level + "").prop("disabled", true);
//        $("#inputuom" + level + "").data("kendoDropDownList").value(rUOM);
//        var inputwtperunituom0 = $("#inputuom"+level+"").data("kendoDropDownList");
//        inputwtperunituom0.enable(false);
//    }
//    else if (recipeCategory == "UOM-00007") {
//        $("#inputwtperunit"+level+"").prop("disabled", false);
//        $("#inputgravywtperunit"+level+"").prop("disabled", true);
//        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
//        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
//        var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
//        inputwtperunituom0.enable(false);
//    }
//    else if (recipeCategory == "UOM-00009") {
//        $("#inputwtperunit" + level + "").prop("disabled", true);
//        $("#inputgravywtperunit"+level+"").prop("disabled", false);
//        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
//        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
//        var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
//        inputwtperunituom0.enable(false);

//    }
//    else if (recipeCategory == "UOM-00008") {
//        $("#inputwtperunit" + level + "").prop("disabled", false);
//        $("#inputgravywtperunit" + level + "").prop("disabled", false);
//        $("#inputuom" + level + "").data("kendoDropDownList").value(3);
//        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
//        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
//    }
//};

function showAllergens(allergens) {
    var allergenHtml = '<label class="form-control-label">Allergens</label>';
    if (allergens !== null && allergens != "") {
        allergenHtml += '<label class="form-control-label" style="color: #676767!important;font - size: 14px!important;">' + allergens + '</label>';
        //var allergenArray = allergens.split(',');
        //if (allergenArray != null) {
        //    allergenArray.forEach(function (item, index) {
        //        if (item != null && item != "" && item != ",") {
        //            var allergenCode = allergenmasterdata.filter(m => m.Name == item);
        //            if (allergenCode != null && allergenCode != "") {
        //                allergenHtml += '<div class="img-with-text" style="float: left;height: 30px;"><img alt="Tree Nuts" style="display: block;margin: 0 auto;height: 15px;width: 15px;border-radius: 10px;" src="./AllergenImages/' + allergenCode[0].AllergenCode + '.jpg">';
        //                allergenHtml += '<p style="font-size: 11px;">' + item + '</p></div></div>';
        //            }
        //        }
        //    });
        //}
    }
    else {
        allergenHtml += '<label class="form-control-label" style="color: #676767!important;font - size: 14px!important;">None</label>';
    }
    return allergenHtml;
}

function showDetails(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
       if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#siteconfigwindowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
    $("#inputrecipealiasname1").val(tr.RecipeAlias);
   
    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.ID, tr.RecipeCode);
    populateMOGGrid1(tr.ID, tr.RecipeCode);
    $("#recipeAllergens1").html(showAllergens(tr.AllergensName));
    $("#btnCancel1").val($("#inputrecipecode").val());
    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity1").val(tr.Quantity);
    recipeCategoryHTMLPopulateOnEditRecipe("1", tr);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");
    $('#grandTotal1').html(Utility.AddCurrencySymbol(tr.TotalCost,2));
    $('#grandCostPerKG1').html(Utility.AddCurrencySymbol(tr.TotalCost,2));
   

    var editor = $("#inputinstruction1").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename1").removeAttr('disabled');
    //    $("#inputuom1").removeAttr('disabled');
    //    $("#inputquantity1").removeAttr('disabled');
    //    $("#inputbase1").removeAttr('disabled');
    //    $("#btnSubmit1").css('display', 'inline');
    //    $("#btnCancel1").css('display', 'inline');
    //} else {
    //    $("#inputrecipename1").attr('disabled', 'disabled');
    //    $("#inputuom1").attr('disabled', 'disabled');
    //    $("#inputquantity1").attr('disabled', 'disabled');
    //    $("#inputbase1").attr('disabled', 'disabled');
    //    $("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details Inline - " + RecipeName);
    return true;

}

function calculateItemTotal(e, level) {

    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");
    if (level == null || typeof level == 'undefined')
        level = '';
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    var recipeQty = $("#inputquantity" + level).val();
    var perc = (qty / recipeQty)*100;
    dataItem.IngredientPerc = perc.toFixed(2);
    var changePerc = (perc - dataItem.RegionIngredientPerc)*100 / dataItem.RegionIngredientPerc;
    changePerc = changePerc.toFixed(2);
    //  if (level == '' || level == '0') {
    if (user.SectorNumber !== "20") {
        $(element).closest('tr').find('.change')[0].innerHTML = changePerc;
        if (Math.abs(changePerc) > PrescibedPercentChange) {
            //        toastr.error("Quantity changes are going beyond the allowed change limit");
            $(element).closest('tr').find('.change').css('background-color', '#ffe1e1');

        } else {

            $(element).closest('tr').find('.change').css('background-color', '#d9ffb3');
        }
        if (changePerc == 0) {
            $(element).closest('tr').find('.change').css('background-color', 'inherit');
        }


        $(element).closest('tr').find('.minor')[0].innerHTML = dataItem.IngredientPerc;
    }

    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    CalculateGrandTotal(level);
}

function calculateItemTotalMOGOnAPLChange(item,level) {
    if (level == null || typeof level == 'undefined')
        level = '';
    var dataItem = item;
    var qty = item.Quantity;
    var recipeQty = $("#inputquantity" + level).val();
    var perc = (qty / recipeQty)*100;

    dataItem.Quantity = qty;
    dataItem.IngredientPerc = perc.toFixed(2);

    var changePerc = (perc - dataItem.RegionIngredientPerc)*100 / dataItem.RegionIngredientPerc;
    changePerc = changePerc.toFixed(2);
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    if (user.SectorNumber !== "20") {
        $(element).closest('tr').find('.change')[0].innerHTML = changePerc;
        $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
        $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;

        if (Math.abs(changePerc) > PrescibedPercentChange) {
            $(element).closest('tr').find('.change').css('background-color', '#ffe1e1');
        } else {
            $(element).closest('tr').find('.change').css('background-color', '#d9ffb3');
        }
        if (changePerc == 0) {
            $(element).closest('tr').find('.change').css('background-color', 'inherit');
        }
    }
    CalculateGrandTotal(level);
}

function calculateItemTotalMOG(e, level) {
    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    var recipeQty = $("#inputquantity" + level).val();
    var perc = (qty / recipeQty)*100;

    dataItem.Quantity = qty;
    dataItem.IngredientPerc = perc.toFixed(2);

    var changePerc = (perc - dataItem.RegionIngredientPerc)*100 / dataItem.RegionIngredientPerc;
    changePerc = changePerc.toFixed(2);
    if (user.SectorNumber !== "20") {
        dataItem.TotalCost = qty * dataItem.CostPerUOM;
    }
    else {
        dkgtotal = dataItem.DKgValue * qty;
        dataItem.DKgTotal = dkgtotal;
        dataItem.set("DKgTotal", dkgtotal);
        $(element).closest('tr').find('.dkgtotal')[0].innerHTML = dkgtotal;
        dataItem.TotalCost = qty * dataItem.CostPerUOM * dataItem.DKgValue;
    }
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    
    if (user.SectorNumber !== "20") {
        $(element).closest('tr').find('.change')[0].innerHTML = changePerc;
        if (Math.abs(changePerc) > PrescibedPercentChange) {
            $(element).closest('tr').find('.change').css('background-color', '#ffe1e1');
        } else {
            $(element).closest('tr').find('.change').css('background-color', '#d9ffb3');
        }
        if (changePerc == 0) {
            $(element).closest('tr').find('.change').css('background-color', 'inherit');
        }
    }
   
    mogtotalQTY(e,level);
    CalculateGrandTotal(level);
}

function CalculateGrandTotal(level) {
    var gtotalObjforBR = $("#gridBaseRecipe" + level + " .totalcost");
    var gtotalObjforMOG = $("#gridMOG" + level + " .totalcost");
    var len = gtotalObjforBR.length;
    var len1 = gtotalObjforMOG.length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObjforBR[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObjforBR[i].innerText.substring(1).replace(",", ""));
    }
    for (i = 0; i < len1; i++) {
        if (gtotalObjforMOG[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObjforMOG[i].innerText.substring(1).replace(",", ""));
    }
    if (!isNaN(gTotal)) {
        $("#grandTotal" + level).html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#inputquantity' + level).val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG' + level).html(Utility.AddCurrencySymbol(costPerKG, 2));
   
    if (user.SectorNumber !== "20") {
        var change = gTotal - oldcost;
        var changeperc = (change*100 / oldcost).toFixed(2)
        $("#changegrandTotal" + level).html(Utility.AddCurrencySymbol(change, 2));
        $("#totalchangepercent" + level).html(Utility.MathRound(changeperc, 2));
        if (PerCostChangePercent > Math.abs(changeperc)) {
            $("#changegrandTotal" + level).css('color', 'green');
            $("#totalchangepercent" + level).css('color', 'green');
            $(".grtotal").style = "background-color: #d9ffb3 !important";
        }
        else {
            $("#changegrandTotal" + level).css('color', 'red');
            $("#totalchangepercent" + level).css('color', 'red');
            $(".grtotal").style = "background-color: #ffe1e1 !important";
        }
        if (isNaN(oldcost)) {
            $("#changegrandTotal" + level).hide();
            $("#totalchangepercent" + level).hide();
        } else {
            $("#changegrandTotal" + level).show();
            $("#totalchangepercent" + level).show();
        }
    }
   
}

function validationPerCost() {
    var ele = $(".change");
    var len = ele.length;

    for (var i = 0; i < len; i++) {
        if (Math.abs(ele[i].innerText) > PrescibedPercentChange) {
            toastr.error("Quantity changes are going beyond the allowed change limit");
            return false;
        }
    }
    //var changeperc = $("#totalchangepercent").html();
    //if (PerCostChangePercent > Math.abs(changeperc)) {
    //    toastr.error("New Cost is going beyond the allowed change limit");
    //    return false;
    //}
    return true;
}
function populateSiteMasterDropdown() {
    $("#inputsite").kendoDropDownList({
        filter: "contains",
        dataTextField: "SiteName",
        dataValueField: "SiteCode",
        dataSource: sitemasterdata,
        index: 0,
    });

    if (sitemasterdata.length == 1) {
        var dropdownlist = $("#inputsite").data("kendoDropDownList");
        dropdownlist.wrapper.hide();
        Utility.Loading();
        setTimeout(function () {
            SiteCode = dropdownlist.value();
            populateRecipeGrid(SiteCode);
            $("#searchspan").show();
            var siteName = dropdownlist.text()
            $("#location").text(" | " + siteName);
            Utility.UnLoading();
        }, 2000);

    }
}
function cancelChangesConfirmation(gridId) {
    $(".k-grid-cancel-changes").unbind("mousedown");
    $(".k-grid-cancel-changes").mousedown(function (e) {
         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                var grid = $(gridId).data("kendoGrid");
                grid.cancelChanges();
            },
            function () {
            }
        );

    });
}


function BaseRecipetotalQTY(e, level) {
    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var gTotal = 0.0;
    var len1 = grid._data.length;

    for (i = 0; i < len1; i++) {
        var totalCostMOG = grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {
        $("#totBaseRecpQty").text(Utility.MathRound(gTotal, 2));
        getdkgtotal();
    }


}

function mogtotalQTY(e, level) {
    
    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var gTotal = 0.0;
    var len1 = grid._data.length;

    for (i = 0; i < len1; i++) {
        var totalCostMOG = user.SectorNumber == "20" ? grid._data[i].DKgValue * grid._data[i].Quantity : grid._data[i].Quantity;
        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {
        $("#totmog").text(Utility.MathRound(gTotal, 2));
        getdkgtotal();
    }

}

function getdkgtotal() {
    var btot = isNaN(parseFloat($("#totBaseRecpQty").text())) ? 0 : parseFloat($("#totBaseRecpQty").text());
    var mtot = isNaN(parseFloat($("#totmog").text())) ? 0 : parseFloat($("#totmog").text());
    var tot = (btot + mtot).toFixed(2);
    $("#ToTIngredients").text(tot);
}


//Krish
function recipeGridPrint(e) {
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var tr = gridObj.dataItem($(e).closest("tr"))
    //console.log(tr);
    Utility.Loading();
    setTimeout(function () {
        gridPrint(tr);

    }, 0);
    //var restorepage = document.body.innerHTML;
    //var printcontent = document.getElementById("printRecipe").innerHTML;
    ////document.body.innerHTML = printcontent;
    //window.print();
    ////document.body.innerHTML = restorepage;
}
function gridPrint(tr) {
    HttpClient.MakeSyncRequest(CookBookMasters.GetRecipePrintingData, function (data) {
        //console.log(data);
        $("#spanRecipeName").html(tr.RecipeCode + " " + tr.Name + " | ");
        $("#spanDishName").html(data.DishName + " | ");

        //if (tr.Yield == null || tr.Yield == "null") {
        //    $("#spanYield").html("");
        //$("#spanDishName").html(data.DishName);
        //}
        //else {
        $("#spanYield").html(tr.Quantity + " " + tr.UOMName);
        /*}*/

        //document.getElementById("txtaInstructions").innerHTML = tr.Instructions

        $("#txtaInstructions1").html(tr.Instructions);
        $("#txtaInstructions").html($("#txtaInstructions1").text());
        //$("#txtaInstructions").html($("#txtaInstructions2").text());

        //Table 
        var table = document.getElementById("tblRecipePrint");
        table.innerHTML = "";
        var row = table.insertRow(-1);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "MOG Code";
        row.appendChild(headerCell);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "MOG Name";
        row.appendChild(headerCell);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "UOM";
        row.appendChild(headerCell);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "Quantity";
        row.appendChild(headerCell);


        for (var i = 0; i < data.Mogs.length; i++) {
            var row1 = table.insertRow(-1);
            var innerCell = document.createElement("TD");
            innerCell.innerHTML = data.Mogs[i].MOGCode;
            row1.appendChild(innerCell);
            innerCell = document.createElement("TD");
            innerCell.innerHTML = data.Mogs[i].MOGName;
            row1.appendChild(innerCell);
            innerCell = document.createElement("TD");
            innerCell.innerHTML = data.Mogs[i].UOMName;
            row1.appendChild(innerCell);
            innerCell = document.createElement("TD");
            //var mogqty = data.Mogs[i].MOGQty.toString();
            //var qty = mogqty.slice(0, (mogqty.indexOf(".")) + 2);
            //var qty = data.Mogs[i].MOGQty.toFixed(2);
            //if (parseFloat(qty) == 0)
            //    qty = data.Mogs[i].MOGQty.toFixed(3);
            //innerCell.innerHTML = qty;
            if (data.Mogs[i].MOGQty == null) { innerCell.innerHTML = "0.00"; }
            else {
                var qty = data.Mogs[i].MOGQty.toFixed(2);
                if (parseFloat(qty) == 0)
                    qty = data.Mogs[i].MOGQty.toFixed(3);
                innerCell.innerHTML = qty;
            }
            //innerCell.innerHTML = data.Mogs[i].MOGQty.toFixed(2);
            row1.appendChild(innerCell);
        }

        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById("printRecipe").innerHTML;
        //document.body.innerHTML = printcontent;
        setTimeout(function () { document.title = "CookBook Recipe - " + tr.Name; window.print(); }, 500);
        //$("#printRecipe").show();
        //$("#mainContent").hide();

        //document.body.innerHTML = restorepage;
        //$("#printRecipe").print();
        //var divToPrint = document.getElementById("printRecipe");
        //newWin = window.open("");
        //console.log(printcontent);
        //newWin.document.write(printcontent);
        //newWin.print();
        //newWin.close();

        //$("#printRecipe").printThis();             

        Utility.UnLoading();

    }, { recipeCode: tr.RecipeCode, siteCode: tr.SiteCode, regionId: 0 }, true);
}

//Krish 20-07-2022
function recipeGridInfo(e) {
    $("#displayRecipeDetails").show();
    $("#mainContent").hide();

    var gridObj = $("#gridRecipe").data("kendoGrid");
    var tr = gridObj.dataItem($(e).closest("tr"));
   
    //console.log(tr);
    Utility.Loading();
    setTimeout(function () {
        HttpClient.MakeSyncRequest(CookBookMasters.GetRecipePrintingData, function (data) {
            
            $("#hdnRecipeCodeForPrint").val(tr.RecipeCode);
            populateMOGGridNutri(data.Mogs);

            setTimeout(function () {
                var html = $("#recipeDetails").html();
                html = html.replace("##RecipeCode##", tr.RecipeCode);
                html = html.replace("##RecipeName##", tr.Name);
                html = html.replace("##Quantity##", tr.Quantity);
                html = html.replace("##RecipeType##", tr.RecipeType);
                html = html.replace("##Allergen##", tr.AllergensName);
                html = html.replace("##AllergenNG##", tr.NGAllergens);
                //setTimeout(function () { 
                //var gridHtml = $("#gridContent").html();
                var gridHtml = $("#gridMOGNutri").html();
                //html = html.replace("##RecipeGrid##", gridHtml);
                html = html.replace("##RecipeGrid##", gridHtml);
                //console.log(gridHtml)
                var instr = tr.Instructions;
                if (tr.Instructions == "null")
                    instr = "";
                $("#instructionContent").html(instr);
                html = html.replace("##Instructions##", $("#instructionContent").text());

                $("#recipeDetailsShell").html(html);

            }, 300)

            Utility.UnLoading();

        }, { recipeCode: tr.RecipeCode, siteCode: tr.SiteCode, regionId: 0 }, true);

    }, 0);
}
function populateMOGGridNutri(recipeResult) {
    console.log(recipeResult)
    Utility.Loading();
    //$("#gridContent").html("");
    //$("#gridContent").html("<div id='gridMOGNutri' style='overflow: visible!important;'></div>")
    var gridVariable = $("#gridMOGNutri");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" }, //{ name: "create", text: "Add" }
        //],
        groupable: false,
        editable: false,

        columns: [
            {
                field: "MOGCode", title: "MOG Code", width: "100px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                }

            },
            {
                field: "MOG_ID", title: "MOG", width: "100px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },

            },
            //{
            //    field: "ColorCode", title: "Color", width: "30px", attributes: {

            //        style: "text-align: center; font-weight:normal"
            //    },
            //    headerAttributes: {
            //        style: "text-align: center"
            //    },
            //    template: '<div class="colortag"></div>',
            //},
            //{
            //    field: "AllergensName", title: "Contains", width: "50px",
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "AllergensName",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //},
            //{
            //    field: "NGAllergen", title: "May Contains (NG)", width: "70px",
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "AllergensName",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //},
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "MOGQty", title: "Quantity", width: "35px",

                headerAttributes: {
                    style: "text-align: center;width:35px;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                template: function (dataItem) {
                    var incel = "";
                    if (dataItem.MOGQty == null) { incel = "0.00"; }
                    else {
                        var qty = dataItem.MOGQty.toFixed(2);
                        if (parseFloat(qty) == 0)
                            qty = dataItem.MOGQty.toFixed(3);
                        incel = qty;
                    }
                    var html = '<span class="mogquant">' + incel + '</span>';
                    return html;
                },
            },

        ],
        dataSource: recipeResult,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {

            var grid = this;

            //grid.tbody.find("tr[role='row']").each(function () {
            //    var model = grid.dataItem(this);

            //    if (model.AllergensName != "None" && model.NGAllergen != "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00012" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //    else if (model.AllergensName == "None" && model.NGAllergen != "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00013" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //    else if (model.AllergensName != "None" && model.NGAllergen == "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00014" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //    else if (model.AllergensName == "None" && model.NGAllergen == "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00015" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //});
        },
        change: function (e) {
        },
    });

}
function doBackFromDetails() {
    $("#displayRecipeDetails").hide();
    $("#mainContent").show();
}
function doPrintFromDetails() {
    var tr = copyData.filter(a => a.RecipeCode == $("#hdnRecipeCodeForPrint").val())[0];
    gridPrint(tr);
    //var btnInfo = localStorage.getItem("infobtn");
    //var gridObj = $("#gridRecipe").data("kendoGrid");
    //var tr = gridObj.dataItem($(btnInfo).closest("tr"));
    //console.log(tr)
    //recipeGridPrint(btnInfo);
}