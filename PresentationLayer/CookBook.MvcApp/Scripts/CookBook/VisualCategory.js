﻿var user;
var conceptype2data;
var tileImage = null;
var item = null;
var UOMCodeIntial = null;
var datamodel;
var completedata = [];
$(function () {
    $('#myInput').on('input', function (e) {
        var grid = $('#gridUOM').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "VisualCategoryCode" || x.field == "Name"  ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#RTwindowEdit").kendoWindow({
            modal: true,
            width: "300px",
            height: "170px",
            title: "Visual Category Details  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });

      
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
           
            populateUOMGrid();
        }, null, false);

    
    });

 

    function uomvalidate() {
        var valid = true;

        if ($("#VisualCategoryname").val() === "") {        
            toastr.error("Please provide input");
            $("#VisualCategoryname").addClass("is-invalid");
            valid = false;
            $("#VisualCategoryname").focus();
        }
        if (($.trim($("#VisualCategoryname").val())).length > 59) {
            toastr.error("Visual Category Name accepts upto 60 character");
           
            $("#VisualCategoryname").addClass("is-invalid");
            valid = false;
            $("#VisualCategoryname").focus();
        }
        return valid;
    }
    //UOM Section Start

    function populateUOMGrid() {

        Utility.Loading();
        var gridVariable = $("#gridUOM");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "VisualCategoryCode", title: "Code", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive", title: "Active", width: "75px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                               
                                var item = this.dataItem(tr);          // get the date of this row

                                if (!item.IsActive) {
                                    return;
                                }
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive,
                                    "VisualCategoryCode": item.VisualCategoryCode,
                                    "CreatedBy": item.CreatedBy,
                                    "CreatedOn": kendo.parseDate(item.CreatedOn)
                                };
                                UOMCodeIntial = item.VisualCategoryCode;
                                var dialog = $("#RTwindowEdit").data("kendoWindow");

                                dialog.open();
                                dialog.center();

                                datamodel = model;
                               
                                $("#uomid").val(model.ID);
                                $("#VisualCategorycode").val(model.VisualCategoryCode);
                                $("#VisualCategoryname").val(model.Name);
                              
                                $("#VisualCategoryname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetUOMDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                                completedata = result;
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { type: "string" },
                            VisualCategoryCode: { type: "string" },
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
               
                    $(".chkbox").on("change", function () {
                        var gridObj = $("#gridUOM").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        var trEdit = $(this).closest("tr");
                        var th = this;
                        datamodel = tr;
                        datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
                        datamodel.IsActive = $(this)[0].checked;
                        var active = "";
                        if (datamodel.IsActive) {
                            active = "Active";
                        } else {
                            active = "Inactive";
                        }

                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Visual Category " + active + "?", " Visual Category Update Confirmation", "Yes", "No", function () {
                            HttpClient.MakeRequest(CookBookMasters.SaveUOM, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again");
                                }
                                else {
                                    toastr.success("Visual Category updated successfully");
                                    if ($(th)[0].checked) {
                                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                    } else {
                                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                    }
                                }
                            }, {
                                model: datamodel
                            }, false);
                        }, function () {

                                $(th)[0].checked = !datamodel.IsActive;
                                if ($(th)[0].checked) {
                                    $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                } else {
                                    $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                }
                        });
                        return true;
                    });
               
            },
            change: function (e) {
            },
        })

      //  var gridVariable = $("#gridUOM").data("kendoGrid");
        //sort Grid's dataSource
        gridVariable.data("kendoGrid").dataSource.sort({ field: "VisualCategoryCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }


    $("#VisualCategorycancel").on("click", function () {
        var orderWindow = $("#RTwindowEdit").data("kendoWindow");
        orderWindow.close();
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?"," Visual Category Cancel Confirmation", "Yes", "No", function () {
            $(".k-overlay").hide();
        }, function () {

              
                var orderWindow = $("#RTwindowEdit").data("kendoWindow");
                orderWindow.open();
        });
       
       
    });

    $("#VisualCategorynew").on("click", function () {
        var model;
        var dialog = $("#RTwindowEdit").data("kendoWindow");

        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });

        datamodel = model;
      
        dialog.title("New Visual Category Creation");

        $("#VisualCategoryname").removeClass("is-invalid");
        $('#VisualCategorysubmit').removeAttr("disabled");
        $("#uomid").val("");
        $("#VisualCategoryname").val("");
        $("#VisualCategorycode").val("");
        $("#VisualCategoryname").focus();
        UOMCodeIntial = null;
    });

    


    
    $("#VisualCategorysubmit").click(function () {
        if ($("#VisualCategoryname").val() === "") {
            toastr.error("Please provide Visual Category Name");
            $("#VisualCategoryname").focus();
            return;
        }
        var name = $("#VisualCategoryname").val().toLowerCase().trim();
        if (UOMCodeIntial == null) { //Duplicate Check
            for (item of completedata) {
                if (item.Name.toLowerCase().trim() == name) {
                    toastr.error("Please check Duplicate Visual Category Name");
                    $("#VisualCategoryname").focus();
                    return;
                }
            }
        } else {
            for (item of completedata) {
                if (item.Name.toLowerCase().trim() == name && item.VisualCategoryCode != UOMCodeIntial) {
                    toastr.error("Please check Duplicate  Visual Category Name is entered on editing");
                    $("#VisualCategoryname").focus();
                    return;
                }
            }
        }
        if (uomvalidate() === true) {
            $("#VisualCategoryname").removeClass("is-invalid");
          
            var model = {
                "ID": $("#uomid").val(),
                "Name": $("#VisualCategoryname").val(),
                "VisualCategoryCode": UOMCodeIntial,
                "IsActive":1
            }
            if (UOMCodeIntial== null) {
                model.IsActive = 1;
            } else {
                model.CreatedBy = datamodel.CreatedBy;
                model.CreatedOn = datamodel.CreatedOn;
            }

            $("#VisualCategorysubmit").attr('disabled', 'disabled');

            if (!sanitizeAndSend(model)) {
                return;
            }
            HttpClient.MakeRequest(CookBookMasters.SaveUOM, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#VisualCategorysubmit').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#VisualCategorysubmit').removeAttr("disabled");
                        toastr.success("There was some error, the record cannot be saved");

                    }
                    else {
                        $(".k-overlay").hide();
                        $('#VisualCategorysubmit').removeAttr("disabled");

                        if (model.ID > 0)
                            toastr.success("Visual Category record updated successfully");
                        else
                            toastr.success("New Visual Category record added successfully");
                        $(".k-overlay").hide();
                        var orderWindow = $("#RTwindowEdit").data("kendoWindow");
                        orderWindow.close();
                        $("#gridUOM").data("kendoGrid").dataSource.data([]);
                        $("#gridUOM").data("kendoGrid").dataSource.read();
                        $("#gridUOM").data("kendoGrid").dataSource.sort({ field: "VisualCategoryCode", dir: "asc" });
                    }

                   
                }
            }, {
                model: model

            }, true);
        }
    });
 


});