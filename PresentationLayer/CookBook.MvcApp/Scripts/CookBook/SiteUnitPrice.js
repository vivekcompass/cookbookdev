﻿var sitemasterdata = [];
var savedatalist = [];
var siteCode;
kendo.data.DataSource.prototype.dataFiltered = function () {
    // Gets the filter from the dataSource
    var filters = this.filter();

    // Gets the full set of data from the data source
    var allData = this.data();

    // Applies the filter to the data
    var query = new kendo.data.Query(allData);

    // Returns the filtered data
    return query.filter(filters).data;
}
HttpClient.MakeRequest(CookBookMasters.GetSiteDataListByUserId, function (data) {
  
    var dataSource = data;
    var dataSource = data;
    sitemasterdata = dataSource;
    sitemasterdata.forEach(function (item) {

        item.SiteName = item.SiteName + " - " + item.SiteCode;
        return item;
    })
    sitemasterdata = sitemasterdata.filter(item => item.IsActive != false);
    if (data.length > 1) {
        sitemasterdata.unshift({ "SiteCode": 0, "SiteName": "Select" });
        $("#btnGo").css("display", "inline-block");
    }
    else {
        $("#btnGo").css("display", "inline-none");


    }
    populateSiteMasterDropdown();
  
}, null, false);

function populateSiteMasterDropdown() {
    $("#inputsite").kendoDropDownList({
        filter: "contains",
        dataTextField: "SiteName",
        dataValueField: "SiteCode",
        dataSource: sitemasterdata,
        index: 0,
    });
    if (sitemasterdata.length == 1) {
        $("#btnGo").click();
     //   var dropdownlist = $("#inputsite").data("kendoDropDownList");
       // dropdownlist.wrapper.hide();
    }

}
function populateItemGrid() {

    Utility.Loading();
    var gridVariable = $("#gridItem");
    gridVariable.html("");
    grid = gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,

        columns: [


            {
                field: "ItemCode", title: "Item Code", width: "40px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                }
            },
            {
                field: "Name", title: "Item Name", width: "90px", attributes: {

                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.Name) + '</span>'
                        + '  <i title="Click to View Price History" style="float:right;" class="fas fa-history" onclick="showDetails(&quot;'+ dataItem.ItemCode +'&quot; ,&quot;'+dataItem.Name +'&quot;)"></i>';
                    return html;
                },
            },

            {
                field: "ItemPrice", title: "Latest Price", width: "60px",
                template: '<input type="number" class="inputbrqty"  value="ItemPrice" name="ItemPrice" oninput="savePriceChange(this,true)"/>',
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            //{
            //    field: "MaxMealCount", title: "Max Meal Count", width: "60px",
            //    template: '<input type="number" class="inputbrmaxmealcountqty"  value="MaxMealCount" name="MaxMealCount" oninput="savePriceChange(this,false)"/>',
            //    attributes: {
            //        style: "text-align: left; font-weight:normal"
            //    },
            //},
            {
                field: "UserName", title: "Last Modified By", width: "55px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ModifiedOn", title: "Last Modified On", width: "55px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "StatusLevel", title: "Status", width: "55px", attributes: {
                    class: "mycustomstatus",
                    style: "text-align: center; font-weight:normal"
                },headerAttributes: {
               
                style: "text-align: center; font-weight:normal"
            },
            },


        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetUnitPriceDataList, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                            if (result.length > 0) {
                               // savedatalist = result;//filter
                                $("#myInput").show();
                                $("#btnSaveAll").show();
                                $("#btnPricePublish").show();
                                
                                savedatalist = [];
                                for (x of result) {
                                    var model = {};
                                    if (x.PriceStatus != 3) {
                                        model.SiteCode = siteCode;
                                        model.ItemPrice = x.ItemPrice;
                                        model.MaxMealCount = x.MaxMealCount;
                                        model.ItemCode = x.ItemCode;
                                        model.PriceStatus = x.PriceStatus;
                                        if (model.PriceStatus == null)
                                            model.PriceStatus = 0;
                                        savedatalist.push(model);
                                    }
                                   
                                   
                                }
                               
                            } else {
                                $("#myInput").hide();
                                $("#btnSaveAll").hide();
                                $("#btnPricePublish").hide();
                            }
                           

                        }
                        else {
                            options.success("");
                        }
                    }, {

                        SiteCode: $("#inputsite").val()
                    }
                        , true);
                }
            },

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ItemCode: { type: "string" },
                        Name: { type: "string" },
                        ItemPrice: { type: "string" },
                        MaxMealCount: { type: "string" },
                        UserName: { type: "string" },
                        ModifiedOn: { type: "string" },
                        StatusLevel: { type: "string" }
                        
                    }

                }
            },
            pageSize: 150
        },
        columnResize: function (e) {
            //var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        //  height: 440,
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();
          
                //ddt.find(".k-input").val(dataItem.MOGName);
            items.each(function (e) {
                var dataItem = grid.dataItem(this);
                $(this).find('.inputbrqty').val(dataItem.ItemPrice);
                $(this).find('.inputbrmaxmealcountqty').val(dataItem.MaxMealCount);
            });

            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (view[i].PriceStatus == 0 || view[i].PriceStatus == null) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffe1e1");
                   

                }
                else if (view[i].PriceStatus == 2 || view[i].PriceStatus == 1) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffffbf");
                  
                }
                else {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#d9ffb3");
                   
                }

            }

            },
        change: function (e) {

        },
        // filter: { field: "ItemCode", operator: "gt", value: "69999" }
    });
    
    Utility.UnLoading();
   
}
var lastValid = 0;
var validNumber = new RegExp(/^\d*\.?\d*$/);
function validateNumber(elem) {

    if (validNumber.test(elem.value)) {
        lastValid = elem.value;
    } else {
        elem.value = lastValid;
    }
}

function savePriceChange(e, isSavePrice) {
    validateNumber(e);
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridItem").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var ItemCode = dataItem.ItemCode;
    var model = {};
    model.IsPriceChange = isSavePrice;
    if (isSavePrice) {
        if (dataItem.ItemPrice == $(e).val()) {
            return;
        }
        dataItem.ItemPrice = $(e).val();
        model.ItemPrice = dataItem.ItemPrice;
        model.MaxMealCount = dataItem.MaxMealCount;
    }

    if (!isSavePrice) {
        if (dataItem.MaxMealCount == $(e).val()) {
            return;
        }
        dataItem.MaxMealCount = $(e).val();
        model.ItemPrice = dataItem.ItemPrice;
        model.MaxMealCount = dataItem.MaxMealCount;
    }

    model.SiteCode = siteCode;
    model.ItemCode = ItemCode;
    model.PriceStatus = dataItem.PriceStatus;

    for (x of savedatalist) {
        if (x.ItemCode == model.ItemCode) {
            x.ItemPrice = model.ItemPrice;
            x.MaxMealCount = model.MaxMealCount;
            return;
        }
    }
    savedatalist.push(model);
}





$(document).ready(function () {
    $("#windowEditMOGWiseAPL").kendoWindow({
        modal: true,
        width: "600px",
        height: "300px",
        cssClass: 'aplPopup',
        top: '80px !important',
        title: "Item Price Detials - ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $('#myInput').on('input',
        function (e) {
            var grid = $('#gridItem').data('kendoGrid');
            var columns = grid.columns;

            var filter = { logic: 'or', filters: [] };
            columns.forEach(function (x) {
                if (x.field) {
                    if (x.field == "ModifiedOn" || x.field== "ItemPrice" || x.field=="Name"
                        || x.field == "StatusLevel" || x.field == "UserName" || x.field == "Status" || x.field == "ItemCode") {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;

                        if (type == 'string') {
                           
                            filter.filters.push({
                                field: x.field,
                                operator: 'contains',
                                value: e.target.value
                            })
                        }
                        else if (type == 'number') {

                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        } else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format('dd-MMM-yy', data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                }
            });
            grid.dataSource.filter(filter);
        }
    );
    $("#btnGo").click(function () {
        
        siteCode = $("#inputsite").val();
        populateItemGrid();
    })

    $("#btnSaveAll").click(function () {
        if (savedatalist.length == 0) {
            toastr.error("Kindly, update at least one Item Price")

            return;
        }
        savedatalist.forEach(function (item) {
            if (item.PriceStatus == null || item.PriceStatus == 0 || item.PriceStatus == 1)
                item.PriceStatus = 1;
            else
                item.PriceStatus = 2;
        })
        Utility.Page_Alert_Save("Existing Price data shall get overwritten. Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                HttpClient.MakeSyncRequest(CookBookMasters.SaveToSiteInheritanceDataList, function (result) {
                    if (result == false) {
                        toastr.error("Some error occured, please try again.");
                    }
                    else {
                            $(".k-overlay").hide();

                        var msg = "Price changes updated. Do not forget to Publish the same";
                            Toast.fire({
                                type: 'success',
                                title: msg
                            });
                        savedatalist = [];
                        populateItemGrid();
                    }
                }, {
                        model: savedatalist
                }, true);

                Utility.UnLoading();
            },
            function () {
                Utility.UnLoading();
            }
        );
    })

    $("#btnPricePublish").click(function () {
        if (savedatalist.length == 0) {
            toastr.error("Kindly, update at least one Item Price")

            return;
        }
        savedatalist.forEach(function (item) {
           
                item.PriceStatus = 3;
        })
        Utility.Page_Alert_Save("All Changed Price will be Published. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                HttpClient.MakeSyncRequest(CookBookMasters.SaveToSiteInheritanceDataList, function (result) {
                    if (result == false) {
                        toastr.error("Some error occured, please try again.");
                    }
                    else {
                        $(".k-overlay").hide();
                        var msg = "Price changes have been Published";
                        toastr.success(msg);
                        savedatalist = [];
                        populateItemGrid();
                    }
                }, {
                    model: savedatalist
                }, true);

                Utility.UnLoading();
            },
            function () {
                Utility.UnLoading();
            }
        );
    })

});

function showDetails(iCode,iName) {
  
    var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
    dialog.title("Item Price History Details of Item Name "+iName+" ("+iCode.trim()+")");
    populateAPLMasterGrid(iCode.trim());
}

function populateAPLMasterGrid(iCode) {
    Utility.Loading();


    var gridVariable = $("#gridMOGWiseAPL");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "APLMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        scrollable: true,
        noRecords: {
            template: "No Records Available"
        },

        columns: [
           
            {
                field: "ItemPrice", title: "Price", width: "50px", format: Utility.Cost_Format, attributes: {

                    style: "text-align: left; font-weight:normal;"
                },
            },
            {
                field: "ModifiedOn", title: "Modified On", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;"
                },
            },
            {
                field: "UserName", title: "Modified By", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            }
           
        ],
        dataSource: {
            sort: { field: "ModifiedOn", dir: "desc" },
            transport: {
                read: function (options) {
                    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteItemPriceHistory, function (result) {


                        if (result != null) {
                            options.success(result);
                            Utility.UnLoading();

                        }
                        else {
                            options.success("");
                        }
                    }, {

                        siteCode: $("#inputsite").val(),
                        itemCode: iCode
                    }
                        , true);
                  
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Name: { type: "string" },
                        ItemCode: { type: "string" },
                        ItemPrice: { type: "decimal" },
                         SiteCode: { type: "string" },
                      
                    }
                }
            },
            // pageSize: 100,
        },

        //maxHeight: 250,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {


        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
    //$(".k-label")[0].innerHTML.replace("items", "records");
}