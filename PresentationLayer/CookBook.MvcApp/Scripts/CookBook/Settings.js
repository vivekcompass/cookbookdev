﻿var user;
$(function () {

    var status = "";
    var varname = "";
    var datamodel;
    var Name = "";
    var sitedata = [];
    var dkdata = [];

    $("#windowEdit").kendoWindow({
        modal: true,
        width: "600px",
        height: "200px",
        title: "Flex Rule Details  ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $('#myInput').on('input', function (e) {
        var grid = $('#gridApplicationSetting').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ColorCode" || x.field == "Name" || x.field == "Code" || x.field == "Remarks" || x.field == "Value") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $(document).ready(function () {
        $("#btnExport").click(function (e) {
            var grid = $("#gridApplicationSetting").data("kendoGrid");
            grid.saveAsExcel();
        });
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.UserRoleId === 1) {

            //    $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {

            //    $("#AddNew").css("display", "none");
            //}
            populateApplicationSetting();
        }, null, false);


    });


    //Food Program Section Start

    function populateApplicationSetting() {

        Utility.Loading();
        var gridVariable = $("#gridApplicationSetting");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "ApplicationSetting.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "Code", title: "Flex Rule Code", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Name", width: "180px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Value", title: "Value", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Remarks", title: "Remarks", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive",
                //    title: "Status", width: "40px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal;"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "40px",

                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = $("#gridApplicationSetting").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                Name = tr.Name;
                                var dialog = $("#windowEdit").data("kendoWindow");
                               
                                dialog.open();
                                dialog.center();
                                // 
                                $("#ApplicationSettingid").val(tr.ID);
                                $("#Code").val(tr.Code);
                                $("#name").val(tr.Name);
                                $("#value").val(tr.Value);
                                $("#remarks").val(tr.Remarks);
                                //if (user.UserRoleId === 1) {
                                //    $("#name").removeAttr('disabled');

                                //    $("#btnSubmit").css('display', 'inline');
                                //    $("#btnCancel").css('display', 'inline');
                                //} else {
                                //    $("#name").attr('disabled', 'disabled');
                                //    $("#btnSubmit").css('display', 'none');
                                //    $("#btnCancel").css('display', 'none');
                                //}

                                dialog.title("Details - " + Name);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetApplicationSettingDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null

                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false, type: "string" },
                            Code: { type: "string" },
                            Value: { type: "string" },
                            Remarks: { type: "string" },
                            IsActive: { editable: false }
                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                // 
                //items.each(function (e) {
                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //        $(this).find('.chkbox').removeAttr('disabled');

                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //        $(this).find('.chkbox').attr('disabled', 'disabled');
                //    }
                //});

                $(".chkbox").on("change", function () {
                    var gridObj = $("#gridApplicationSetting").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    var th = this;
                    datamodel = tr;
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Turing this off may impact parts of this application. Proceed with the change?", "Flex Rule Update Confirmation", "Yes", "No", function () {
                        HttpClient.MakeRequest(CookBookMasters.ChangeStatus, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                toastr.success("Flex Rule configuration updated successfully");
                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {

                        $(th)[0].checked = !datamodel.IsActive;
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }



    $("#AddNew").on("click", function () {
        var model;
        var dialog = $("#windowEdit").data("kendoWindow");
       
        dialog.open();
        dialog.center();

        datamodel = model;
        $("#ApplicationSettingid").val("");
        $("#Code").val("");
        $("#value").val("");
        $("#name").val("");
        $("#remarks").val("");
        dialog.title("New Flex Rule Creation");
    })

    $("#btnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });





    $("#btnSubmit").click(function () {

        if ($("#name").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#name").focus();
        }
        else {
            var model;
            if (datamodel != null) {
                model = datamodel;
                model.Code = $("#Code").val();
                model.Name = $("#name").val();
                model.Value = $("#value").val();
                model.Remarks = $("#remarks").val();

            }
            else {
                model = {
                    "ID": $("#ApplicationSettingid").val(),
                    "Code": $("#Code").val(),
                    "Name": $("#name").val(),
                    "Value": $("#value").val(),
                    "Remarks": $("#remarks").val(),
                    "IsActive": true,
                }
            }
            if (!sanitizeAndSend(model)) {
                return;
            }

            $("#btnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveApplicationSettingData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnSubmit').removeAttr("disabled");
                }
                else {
                    if (result == false) {
                        $('#btnSubmit').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");

                    }
                    else {
                        $(".k-overlay").hide();
                        var orderWindow = $("#windowEdit").data("kendoWindow");
                        orderWindow.close();
                        $('#btnSubmit').removeAttr("disabled");
                        if (model.ID > 0) {
                            toastr.success("Flex Rule configuration updated successfully");
                        }
                        else {
                            toastr.success("New Flex Rule added successfully");
                        }

                        $("#gridApplicationSetting").data("kendoGrid").dataSource.data([]);
                        $("#gridApplicationSetting").data("kendoGrid").dataSource.read();
                    }
                }
            }, {
                model: model
            }, true);
        }
    });
});