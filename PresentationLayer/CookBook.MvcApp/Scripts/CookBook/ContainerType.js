﻿$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var Name = "";
    var sitedata = [];
    var dkdata = [];

    $("#ContTywindowEdit").kendoWindow({
        modal: true,
        width: "250px",
        height: "166px",
        title: "Container Details ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $('#myInput').on('input', function (e) {
        var grid = $('#gridContainerType').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ContainerTypeCode" || x.field == "Name" ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });



    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    var user;
    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#btnExport").click(function (e) {
            var grid = $("#gridContainerType").data("kendoGrid");
            grid.saveAsExcel();
        });
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
           
            populateContainerType();
        }, null, false);

  
    });


    //Food Program Section Start

    function populateContainerType() {
        
        Utility.Loading();
        var gridVariable = $("#gridContainerType");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "Container.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "ContainerTypeCode", title: "Container Code", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Container Name", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive",
                //    title: "Status", width: "50px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal;"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",

                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = $("#gridContainerType").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                Name = tr.Name ;
                                var dialog = $("#ContTywindowEdit").data("kendoWindow");
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open();
                                dialog.center();
                                // 
                                $("#containertypeid").val(tr.ID);
                                $("#ContainerTypeCode ").val(tr.ContainerTypeCode);
                                $("#name").val(tr.Name);
                       
                                

                                dialog.title(Name + " - Edit Details");
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetContainerTypeDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false, type: "string" },
                            ContainerTypeCode: {type:"string"},
                            IsActive: { editable: false }
                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();
              

                $(".chkbox").on("change", function () {
                    var gridObj = $("#gridContainerType").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    var th = this;
                    datamodel = tr;
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> ContainerType " + active + "?", "ContainerType Update Confirmation", "Yes", "No", function () {
                        HttpClient.MakeRequest(CookBookMasters.ChangeStatus, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                toastr.success("container Type updated successfully");
                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {
                           
                            $(th)[0].checked = !datamodel.IsActive;
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }



    $("#AddNew").on("click", function () {
        var model;
        var dialog = $("#ContTywindowEdit").data("kendoWindow");
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });
       // dialog.center();

        datamodel = model;
        $("#containertypeid").val("");
        $("#ContainerTypeCode").val("");
        $("#name").val("");

        dialog.title("Create New Container");
    })

    $("#CbtnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                //$(".k-overlay").hide();
                $(".k-window").hide();
                $(".k-overlay").hide();
                var orderWindow = $("#ContTywindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    
 

    
    $("#CbtnSubmit").click(function () {
        
        var msg;
        if ($("#name").val() === "") {
            toastr.error("Please provide Container Type Name");
            $("#name").focus();
            return;
        }
        else {
            var model;
            if (datamodel != null) {
                model = datamodel;
                model.ContainerTypeCode = $("#ContainerTypeCode").val();
                model.Name = $("#name").val();
                msg = "Container Updated Successfully";
            }
            else {
                model = {
                    "ID": $("#containertypeid").val(),
                    "ContainerTypeCode": $("#ContainerTypeCode").val(),
                    "Name": $("#name").val(),
                    "IsActive" : true,
                }
                msg = "Container Created Successfully";
            }

            if (!sanitizeAndSend(model)) {
                return;
            }
            $("#CbtnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveContainerTypeData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#CbtnSubmit').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#CbtnSubmit').removeAttr("disabled");
                        toastr.error(result.message);
                        Utility.UnLoading();
                        return;
                    }
                    else {
                        if (result == false) {
                            $('#CbtnSubmit').removeAttr("disabled");
                            toastr.error("Some error occured, please try again");

                        }
                        else {
                            $(".k-overlay").hide();
                            var orderWindow = $("#ContTywindowEdit").data("kendoWindow");
                            orderWindow.close();
                            $('#CbtnSubmit').removeAttr("disabled");
                            toastr.success(msg);
                            $("#gridContainerType").data("kendoGrid").dataSource.data([]);
                            $("#gridContainerType").data("kendoGrid").dataSource.read();
                        }
                    }
                }
            }, {
                model: model
            }, false);
        }
    });
});