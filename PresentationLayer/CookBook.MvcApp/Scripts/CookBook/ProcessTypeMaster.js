﻿
$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var processtypeName = "";
    var processtypeNameEdit = "";
    var HexCode = "";

    var sitedata = [];
    var dkdata = [];
    var processtypeData = [];

    $('#myInput').on('input', function (e) {
        var grid = $('#gridProcessType').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "Code" || x.field == "Name" || x.field == "StdTempFrom" || x.field == "StdTempTo") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $("#ProcessTypewindowEdit").kendoWindow({
        modal: true,
        width: "340px",
        height: "133px",
        title: "Process Type Details",
        actions: ["Close"],
        visible: false,
        animation: false,
        resizable: false
    });

    //$('.my-colorpicker2').colorpicker()

    //$('.my-colorpicker2').on('colorpickerChange', function (event) {
    //    if (event.color != null) {
    //        $("#inputrgbcode").val(event.color.toRgbString());
    //        $("#inputhexcode").val(event.color.toString());
    //        $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    //        $('.my-colorpicker2 .fa-square').css('background-color', event.color.toString());
    //        $('.my-colorpicker2 .input-group-text').css('background-color', event.color.toString());
    //    }
    //});

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    var user;
    $(document).ready(function () {



        $(".k-window").hide();
        $(".k-overlay").hide();
        //$("#btnExport").click(function (e) {
        //    var grid = $("#gridRangeType").data("kendoGrid");
        //    grid.saveAsExcel();
        //});
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNew").css("display", "none");
            //}
            populateProcessTypeGrid();
        }, null, false);
    });


    function populateProcessTypeGrid() {

        //Utility.Loading();
        var gridVariable = $("#gridProcessType");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "ProcessType.xlsx",
                filterable: true,
                allPages: true
            },
            // sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "Code", title: "Process Type Code", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Process Type Name", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },                
                //{
                //    field: "IsActive",
                //    title: "Status", width: "50px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {


                                $("#success").css("display", "none");
                                $("#error").css("display", "none");
                                var gridObj = $("#gridProcessType").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (!tr.IsActive) {
                                    return;
                                }
                                datamodel = tr;
                                processtypeName = tr.Name;
                                //HexCode = tr.HexCode;
                                processtypeNameEdit = tr.Name;
                                var dialog = $("#ProcessTypewindowEdit").data("kendoWindow");

                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open().element.closest(".k-window").css({
                                    top: 167,

                                });
                                //dialog.open();
                                dialog.center();
                                // 
                                $("#processtypeid").val(tr.ID);
                                $("#ProcessTypecode ").val(tr.Code);
                                $("#processtypename").val(tr.Name);
                                //$("#colorselect").val(tr.HexCode)
                                //$('.my-colorpicker2 .fa-square').css('color', tr.HexCode);
                                //$('.my-colorpicker2').colorpicker('setValue', tr.HexCode);
                                //$('.my-colorpicker2 .fa-square').css('color', tr.HexCode);
                                //$('.my-colorpicker2 .input-group-text').css('background-color', tr.HexCode);
                                
                                //$("#inputfontcode").val(tr.FontColor);
                                //$("#inputsite").data('kendoDropDownList').value(tr.SiteCode);
                                //if (user.UserRoleId === 1) {
                                //    $("#rangetypename").removeAttr('disabled');
                                //    //$("#rangetypeTo").removeAttr('disabled');
                                //    //$("#rangetypeFrom").removeAttr('disabled');
                                //    //$("#inputfontcode").removeAttr('disabled');
                                //    $("#btnSubmit").css('display', 'inline');
                                //    $("#btnCancel").css('display', 'inline');
                                //} else {
                                //    $("#rangetypename").attr('disabled', 'disabled');
                                //    //$("#rangetypeTo").attr('disabled', 'disabled');
                                //    //$("#rangetypeFrom").attr('disabled', 'disabled');
                                //    //$("#inputfontcode").attr('disabled', 'disabled');
                                //    $("#btnSubmit").css('display', 'none');
                                //    $("#btnCancel").css('display', 'none');
                                //}

                                dialog.title("Process Type Details - " + processtypeName);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetProcessTypeDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                processtypeData = result;
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false, type: "string" },
                            Code: { type: "string" },
                           //StdTempFrom: { type: "int" },
                            //StdTempTo: { type: "int" },
                            //FontColor: { type: "string" },
                            IsActive: { editable: false },
                            //CreatedOn: { type: 'Date' },
                            //ModifiedOn: { type: 'Date' }
                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    delete col.width;
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });

                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        $("#gridProcessType").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    $("#AddNew").on("click", function () {
        //alert("call");
        $("#success").css("display", "none");
        $("#error").css("display", "none");
        $('.my-colorpicker2 .fa-square').css('color', '#fff');
        $('.my-colorpicker2 .fa-square').css('background-color', '#fff');
        $('.my-colorpicker2 .input-group-text').css('background-color', '#fff');

        //$('.my-colorpicker2 .fa-square').css('background-color', '#fff');

        var model;
        var dialog = $("#ProcessTypewindowEdit").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "-0.5");



        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });
        // dialog.center();

        datamodel = model;
        $("#processtypeid").val("");
        $("#ProcessTypecode ").val("");
        $("#processtypename").val("");
        
        dialog.title("Process Type Details");

    })

    $("#btnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#ProcessTypewindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#btnSubmit").click(function () {
        //if ($("#colorselect").val() === "") {
        //    toastr.error("Please select the Color");
        //    $("#colorselect").focus();
        //    return;
        //}
        //debugger;
        if ($("#processtypename").val() === "") {
            toastr.error("Please provide Name");
            $("#processtypename").focus();
            return;
        }
        
        var namedp = $("#processtypename").val().toLowerCase().trim();
        if (datamodel == null) { //Duplicate Check
            for (item of processtypeData) {
                if (item.Name.toLowerCase().trim() == namedp) {
                    toastr.error("Kindly check Duplicate Process Type Name");
                    $("#processtypename").focus();
                    return;
                }
            }
            //var hexcode = $("#rangetypeFrom").val();
            //for (item of rangetypeData) {
            //    if (item.Name.toLowerCase().trim() == hexcode) {
            //        toastr.error("Kindly check Duplicate Hex Code");
            //        $("#rangetypeFrom").focus();
            //        return;
            //    }
            //}
        }
        //if (datamodel !== null) { //Duplicate Check
        //    for (item of rangetypeData) {
        //        if (item.Name.toLowerCase().trim() == namedp.toLowerCase().trim() && namedp.toLowerCase().trim() !== rangetypeNameEdit.toLowerCase().trim()) {
        //            toastr.error("Kindly check Duplicate Range Type Name");
        //            $("#rangetypename").focus();
        //            return;
        //        }
        //    }
        //    //var fromTemp = $("#rangetypeFrom").val();
        //    //for (item of rangetypeData) {
        //    //    if (item.HexCode.toLowerCase().trim() == hexcode.toLowerCase().trim() && hexcode.toLowerCase().trim() !== HexCode.toLowerCase().trim()) {
        //    //        toastr.error("Kindly check Duplicate Hex Code");
        //    //        $("#rangetypeFrom").focus();
        //    //        return;
        //    //    }
        //    //}
        //}
        //else {
        //        $("#error").css("display", "none");

        var model;
        
        if (datamodel != null) {

            model = datamodel;
            model.Code = $("#ProcessTypecode").val();
            model.Name = $("#processtypename").val();
           
        }
        else {
            model = {
                "ID": $("#processtypeid").val(),
                "Code": $("#ProcessTypecode").val(),
                "Name": $("#processtypename").val(),
                
                "IsActive": true,
            }
        }


        $("#btnSubmit").attr('disabled', 'disabled');
        if (!sanitizeAndSend(model)) {
            return;
        }
        HttpClient.MakeSyncRequest(CookBookMasters.SaveProcessTypeData, function (result) {
            if (result.xsssuccess !== undefined && !result.xsssuccess) {
                toastr.error(result.message);
                $('#btnSubmit').removeAttr("disabled");
                Utility.UnLoading();
            }
            else {
                if (result == false) {
                    $('#btnSubmit').removeAttr("disabled");
                    //$("#error").css("display", "flex");
                    toastr.error("Some error occured, please try again");
                    // $("#error").find("p").text("Some error occured, please try again");
                    $("#sitealias").focus();
                }
                else {
                    $(".k-overlay").hide();
                    //$("#error").css("display", "flex");
                    var orderWindow = $("#ProcessTypewindowEdit").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit').removeAttr("disabled");
                    //$("#success").css("display", "flex");
                    if (model.ID > 0) {
                        toastr.success("Process Type configuration updated successfully");
                    }
                    else {
                        toastr.success("New Process Type added successfully.");
                    }

                    // $("#success").find("p").text("Color configuration updated successfully");
                    $("#gridProcessType").data("kendoGrid").dataSource.data([]);
                    $("#gridProcessType").data("kendoGrid").dataSource.read();
                    $("#gridProcessType").data("kendoGrid").dataSource.sort({ field: "Code", dir: "asc" });
                }
            }
        }, {
            model: model

        }, true);
        // }
    });
});