﻿//import { remove } from "toastr";

$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var ColorName = "";
    var ColorNameEdit = "";
    var HexCode = "";
    
    var sitedata = [];
    var dkdata = [];
    var colordata = [];

    $('#myInput').on('input', function (e) {
        var grid = $('#gridColor').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ColorCode" || x.field == "Name" || x.field == "RGBCode" || x.field == "HexCode" || x.field == "FontColor") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $("#ColorwindowEdit").kendoWindow({
        modal: true,
        width: "340px",
        height: "250px",
        title: "Color Details - " + ColorName,
        actions: ["Close"],
        visible: false,
        animation: false,
        resizable: false
    });

    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function (event) {
        if (event.color != null) {
            $("#inputrgbcode").val(event.color.toRgbString());
            $("#inputhexcode").val(event.color.toString());
            $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
            $('.my-colorpicker2 .fa-square').css('background-color', event.color.toString());
            $('.my-colorpicker2 .input-group-text').css('background-color', event.color.toString());
        }
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    var user;
    $(document).ready(function () {

      

        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#btnExport").click(function (e) {
            var grid = $("#gridColor").data("kendoGrid");
            grid.saveAsExcel();
        });
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNew").css("display", "none");
            //}
            populateColorGrid();
        }, null, false);
    });


    function populateColorGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridColor");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "Color.xlsx",
                filterable: true,
                allPages: true
            },
           // sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "ColorCode", title: "Color Code", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Color Name", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ColorCode", title: "Color", width: "35px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    },
                    template: '<div class="colortag" style="background-color:#: RGBCode #"></div>',
                },
                {
                    field: "RGBCode", title: "RGB", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "HexCode", title: "Hex Code", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "FontColor", title: "Font Color", width: "90px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "IsActive",
                //    title: "Status", width: "50px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                               
                                
                                $("#success").css("display", "none");
                                $("#error").css("display", "none");
                                var gridObj = $("#gridColor").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (!tr.IsActive) {
                                    return;
                                }
                                datamodel = tr;
                                ColorName = tr.Name + " (" + tr.ColorCode + ")";
                                HexCode = tr.HexCode;
                                ColorNameEdit = tr.Name;
                                var dialog = $("#ColorwindowEdit").data("kendoWindow");
                               
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open().element.closest(".k-window").css({
                                    top: 167,

                                });
                                //dialog.open();
                                dialog.center();
                                // 
                                $("#colorid").val(tr.ID);
                                $("#colorcode ").val(tr.ColorCode);
                                $("#colorname").val(tr.Name);
                                $("#colorselect").val(tr.HexCode)
                                //$('.my-colorpicker2 .fa-square').css('color', tr.HexCode);
                                $('.my-colorpicker2').colorpicker('setValue', tr.HexCode);
                                $('.my-colorpicker2 .fa-square').css('color', tr.HexCode);
                                $('.my-colorpicker2 .input-group-text').css('background-color', tr.HexCode);
                                $("#inputrgbcode").val(tr.RGBCode);
                                $("#inputhexcode").val(tr.HexCode);
                                $("#inputfontcode").val(tr.FontColor);
                                //$("#inputsite").data('kendoDropDownList').value(tr.SiteCode);
                                //if (user.UserRoleId === 1) {
                                //    $("#colorname").removeAttr('disabled');
                                //    //$("#inputrgbcode").removeAttr('disabled');
                                //    //$("#inputhexcode").removeAttr('disabled');
                                //    //$("#inputfontcode").removeAttr('disabled');
                                //    $("#btnSubmit").css('display', 'inline');
                                //    $("#btnCancel").css('display', 'inline');
                                //} else {
                                //    $("#colorname").attr('disabled', 'disabled');
                                //    //$("#inputrgbcode").attr('disabled', 'disabled');
                                //    //$("#inputhexcode").attr('disabled', 'disabled');
                                //    //$("#inputfontcode").attr('disabled', 'disabled');
                                //    $("#btnSubmit").css('display', 'none');
                                //    $("#btnCancel").css('display', 'none');
                                //}

                                dialog.title("Color Details - " + ColorName);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetColorDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                colordata = result;
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false, type: "string" },
                            ColorCode: { type: "string" },
                            RGBCode: { type: "string" },
                            HexCode: { type: "string" },
                            FontColor: { type: "string" },
                            IsActive: { editable: false },
                            CreatedOn: { type: 'Date' },
                            ModifiedOn: {type:'Date'}
                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });

                $(".chkbox").on("change", function () {
                    // 
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridColor").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    var th = this;
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Color " + active + "?", "Color Update Confirmation", "Yes", "No", function () {
                        $("#error").css("display", "none");
                        HttpClient.MakeRequest(CookBookMasters.SaveColorData, function (result) {
                            if (result == false) {
                                //$("#error").css("display", "flex");
                                //$("#error").find("p").text("Some error occured, please try again");
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                //$("#error").css("display", "flex");
                                //$("#success").css("display", "flex");
                                //$("#success").find("p").text("Color configuration updated successfully");
                                toastr.success("Color configuration updated successfully");

                            }
                        }, {
                            model: JSON.stringify(datamodel)
                        }, false);
                    }, function () {
                            $(th)[0].checked = !datamodel.IsActive;
                            //$("#gridColor").data("kendoGrid").dataSource.data([]);
                            //$("#gridColor").data("kendoGrid").dataSource.read();
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    delete col.width;
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        $("#gridColor").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function generatepdf() // Make an AJAX request to fetch the PDF path
    {
        //HttpClient.MakeSyncRequest(CookBookMasters.SavePDF, function (result) {
        //    if (result == false) {
        //        $('#btnSubmit').removeAttr("disabled");
        //        //$("#error").css("display", "flex");
        //        toastr.error("Some error occured, please try again");
        //        // $("#error").find("p").text("Some error occured, please try again");
        //        $("#sitealias").focus();
        //    }
        //    else {
        //    }
        //});
        $.ajax({
            url: '/CookBook.MvcApp/Color/GeneratePDF',
            method: 'GET',
            success: function (filePath) {
                // Open the PDF in a new window for printing
                window.open(filePath, '_blank');
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    }


    $("#AddNew").on("click", function () {
        //alert("call");
      //  generatepdf();
        $("#success").css("display", "none");
        $("#error").css("display", "none");
        $('.my-colorpicker2 .fa-square').css('color', '#fff');
        $('.my-colorpicker2 .fa-square').css('background-color', '#fff');
        $('.my-colorpicker2 .input-group-text').css('background-color', '#fff');
      
        //$('.my-colorpicker2 .fa-square').css('background-color', '#fff');

        var model;
        var dialog = $("#ColorwindowEdit").data("kendoWindow");
       
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "-0.5");

       

        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558
            
        });
       // dialog.center();

        datamodel = model;
        $("#colorid").val("");
        $("#colorcode ").val("");
        $("#colorname").val("");
        $("#inputhexcode").val("");
        $("#inputrgbcode").val("");
        $("#colorselect").val("");
        $("#inputfontcode").val("#000000");

        dialog.title("New Color Creation");
      
    })

    $("#btnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#ColorwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#btnSubmit").click(function () {
        
        if ($("#colorselect").val() === "") {
            toastr.error("Please select the Color");
            $("#colorselect").focus();
            return;
        }
        if ($("#colorname").val() === "") {
            toastr.error("Please provide Color Name");
            $("#colorname").focus();
            return;
        }
        var namedp = $("#colorname").val().toLowerCase().trim();
        if (datamodel == null) { //Duplicate Check
            for (item of colordata) {
                if (item.Name.toLowerCase().trim() == namedp) {
                    toastr.error("Kindly check Duplicate Color Name");
                    $("#colorname").focus();
                    return;
                }
            }
            var hexcode = $("#inputhexcode").val();
            for (item of colordata) {
                if (item.Name.toLowerCase().trim() == hexcode) {
                    toastr.error("Kindly check Duplicate Hex Code");
                    $("#inputhexcode").focus();
                    return;
                }
            }
        }
        if (datamodel !== null ) { //Duplicate Check
            for (item of colordata) {
                if (item.Name.toLowerCase().trim() == namedp.toLowerCase().trim() && namedp.toLowerCase().trim() !== ColorNameEdit.toLowerCase().trim()) {
                    toastr.error("Kindly check Duplicate Color Name");
                    $("#colorname").focus();
                    return;
                }
            }
            var hexcode = $("#inputhexcode").val();
            for (item of colordata) {
                if (item.HexCode.toLowerCase().trim() == hexcode.toLowerCase().trim() && hexcode.toLowerCase().trim() !== HexCode.toLowerCase().trim()) {
                    toastr.error("Kindly check Duplicate Hex Code");
                    $("#inputhexcode").focus();
                    return;
                }
            }
        }

      

        //else {
    //        $("#error").css("display", "none");
             
            var model;
            if (datamodel != null) {
                model = datamodel;
                model.ColorCode = $("#colorcode").val();
                model.Name = $("#colorname").val();
                model.HexCode = $("#inputhexcode").val();
                model.RGBCode = $("#inputrgbcode").val();
                model.FontColor = $("#inputfontcode").val();
            }
            else {
                model = {
                    "ID": $("#colorid").val(),
                    "ColorCode": $("#colorcode").val(),
                    "Name": $("#colorname").val(),
                    "HexCode": $("#inputhexcode").val(),
                    "RGBCode": $("#inputrgbcode").val(),
                    "FontColor": $("#inputfontcode").val(),
                    "IsActive" : true,
                }
        }


        if (!sanitizeAndSend(model)) {
            return;
        }
            
            $("#btnSubmit").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.SaveColorData, function (result) {
            if (result.xsssuccess !== undefined && !result.xsssuccess) {
                toastr.error(result.message);
                $('#btnSubmit').removeAttr("disabled");
                $("#sitealias").focus();
                Utility.UnLoading();
            }
            else {
                if (result == false) {

                    $('#btnSubmit').removeAttr("disabled");
                    //$("#error").css("display", "flex");
                    toastr.error("Some error occured, please try again");
                    // $("#error").find("p").text("Some error occured, please try again");
                    $("#sitealias").focus();
                    Utility.UnLoading();
                }
                else {
                    $(".k-overlay").hide();
                    //$("#error").css("display", "flex");
                    var orderWindow = $("#ColorwindowEdit").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit').removeAttr("disabled");
                    //$("#success").css("display", "flex");
                    if (model.ID > 0) {
                        toastr.success("Color configuration updated successfully");
                    }
                    else {
                        toastr.success("New Color added successfully. Remember to map this to relevent Dishes");
                    }

                    // $("#success").find("p").text("Color configuration updated successfully");
                    $("#gridColor").data("kendoGrid").dataSource.data([]);
                    $("#gridColor").data("kendoGrid").dataSource.read();
                    $("#gridColor").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
                }
            }
            }, {
                model: model

            }, true);
       // }
    });
});