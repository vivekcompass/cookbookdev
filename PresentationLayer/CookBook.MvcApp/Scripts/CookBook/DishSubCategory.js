﻿var user;
var conceptype2data;
var tileImage = null;
var item = null;
var DishSubCategoryCodeIntial = null;
var datamodel;
$(function () {

  
    
   

  
    $('#myInput').on('input', function (e) {
        var grid = $('#gridDishSubCategory').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "DishSubCategoryCode" || x.field == "Name"  ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $(document).ready(function () {
        $("#windowEdit").kendoWindow({
            modal: true,
            width: "250px",
            height: "170px",
            title: "Dish Sub Category Details  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });

      
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
           
            populateDishSubCategoryGrid();
        }, null, false);

    
    });

 

    function dcvalidate() {
        var valid = true;

        if ($("#dcname").val() === "") {        
            toastr.error("Please provide input");
            $("#dcname").addClass("is-invalid");
            valid = false;
            $("#dcname").focus();
        }
        if (($.trim($("#dcname").val())).length > 30) {
            toastr.error("Dish Sub Category accepts upto 30 charactre");
           
            $("#dcname").addClass("is-invalid");
            valid = false;
            $("#dcname").focus();
        }
        return valid;
    }
    //Dish Sub Category Section Start

    function populateDishSubCategoryGrid() {

        Utility.Loading();
        var gridVariable = $("#gridDishSubCategory");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "DishSubCategoryCode", title: "Dish Sub Category Code", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive", title: "Active", width: "75px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive,
                                    "DishSubCategoryCode": item.DishSubCategoryCode
                                };
                                DishSubCategoryCodeIntial = item.DishSubCategoryCode;
                                var dialog = $("#windowEdit").data("kendoWindow");

                                dialog.open();
                                dialog.center();

                                datamodel = model;
                               
                                $("#dcid").val(model.ID);
                                $("#dccode").val(model.DishSubCategoryCode);
                                $("#dcname").val(model.Name);
                              
                                $("#dcname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetDishSubCategoryDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { type: "string" },
                            DishSubCategoryCode: { type: "string" },
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
               
                    $(".chkbox").on("change", function () {
                        var gridObj = $("#gridDishSubCategory").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        var th = this;
                        datamodel = tr;
                        datamodel.IsActive = $(this)[0].checked;
                        var active = "";
                        if (datamodel.IsActive) {
                            active = "Active";
                        } else {
                            active = "Inactive";
                        }

                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Dish Sub Category " + active + "?", " Dish Sub Category Update Confirmation", "Yes", "No", function () {
                            HttpClient.MakeRequest(CookBookMasters.SaveDishSubCategory, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again");
                                }
                                else {
                                    toastr.success("Dish Sub Category updated successfully");
                                }
                            }, {
                                model: datamodel
                            }, false);
                        }, function () {

                            $(th)[0].checked = !datamodel.IsActive;
                        });
                        return true;
                    });
               
            },
            change: function (e) {
            },
        })

      //  var gridVariable = $("#gridDishSubCategory").data("kendoGrid");
        //sort Grid's dataSource
        gridVariable.data("kendoGrid").dataSource.sort({ field: "DishSubCategoryCode", dir: "asc" });
    }


    $("#dccancel").on("click", function () {
      
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit").data("kendoWindow");
        orderWindow.close();
    });

    $("#dcaddnew").on("click", function () {
        var model;
        var dialog = $("#windowEdit").data("kendoWindow");

        dialog.open();
         dialog.center();

        datamodel = model;
      
        dialog.title("New Dish Sub Category Creation");

        $("#dcname").removeClass("is-invalid");
        $('#dcsubmit').removeAttr("disabled");
        $("#dcid").val("");
        $("#dcname").val("");
        $("#dccode").val("");
        $("#dcname").focus();
        DishSubCategoryCodeIntial = null;
    });

    


    
    $("#dcsubmit").click(function () {


        if (dcvalidate() === true) {
            $("#dcname").removeClass("is-invalid");
          
            var model = {
                "ID": $("#dcid").val(),
                "Name": $("#dcname").val(),
                "DishSubCategoryCode": DishSubCategoryCodeIntial
            }
            if (DishSubCategoryCodeIntial== null) {
                model.IsActive = 1;
            }
            if (!sanitizeAndSend(model)) {
                return;
            }
            $("#dcsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveDishSubCategory, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#dcsubmit').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#dcsubmit').removeAttr("disabled");
                        toastr.error(result.message);
                        Utility.UnLoading();
                        return;
                    }
                    else {
                        if (result == false) {
                            $('#dcsubmit').removeAttr("disabled");
                            toastr.success("There was some error, the record cannot be saved");

                        }
                        else {
                            $(".k-overlay").hide();
                            $('#dcsubmit').removeAttr("disabled");

                            if (model.ID > 0)
                                toastr.success("Dish Sub Category record updated successfully");
                            else
                                toastr.success("New Dish Sub Category record added successfully");
                            $(".k-overlay").hide();
                            var orderWindow = $("#windowEdit").data("kendoWindow");
                            orderWindow.close();
                            $("#gridDishSubCategory").data("kendoGrid").dataSource.data([]);
                            $("#gridDishSubCategory").data("kendoGrid").dataSource.read();
                            $("#gridDishSubCategory").data("kendoGrid").dataSource.sort({ field: "DishSubCategoryCode", dir: "asc" });
                        }

                    }
                }
            }, {
                model: model

            }, true);
        }
    });
 


});