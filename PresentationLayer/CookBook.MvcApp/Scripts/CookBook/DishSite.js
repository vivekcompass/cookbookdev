﻿var recipedata = [];
var recipeCustomData = [];
var mappedData = [];
var colordata = [];
var mappedRecipeCode = [];
var DishCode;
var Dish_ID;
var multiarray = [];
var checkedRecipeCode = '';
var saveModel = [];
var regionmasterdata = [];
var checkedIds = [];
var user;
var dietcategorydata = [];
var RecipeName = "";
var RegionID = 0;

function removeDuplicateObjectFromArray(array, key) {
    let check = {};
    let res = [];
    for (let i = 0; i < array.length; i++) {
        if (!check[array[i][key]]) {
            check[array[i][key]] = true;
            res.push(array[i]);
        }
    }
    return res;
}
function populateDishGrid(siteCode) {

    Utility.Loading();
    var gridVariable = $("#gridDish");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "Dish.xlsx",
            filterable: true,
            allPages: true
        },
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: false,
        //reorderable: true,
        //scrollable: true,
        columns: [
            {
                field: "DishCode", title: "Dish Code", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Dish Name", width: "90px", attributes: {
                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                },
                template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:Name#</span>',
            },
            {
                field: "DishAlias", title: "Dish Alias", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },

            {
                field: "UOMName", title: "UOM", width: "35px", attributes: {

                    style: "text-align: left; font-weight:normal; padding-left:35px"
                },
                headerAttributes: {
                    style: "text-align: center"
                },
            },
            {
                field: "ColorCode", title: "Dish Color", width: "35px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                },
                template: '<div class="colortag" style="background-color:#: RGBCode #"></div>',
            },
            {
                field: "DishCategoryName", title: "Dish Category", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },

            {
                field: "DishTypeName", title: "Type", width: "40px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "DietCategoryName", title: "", width: "1px", attributes: {

                    style: "text-align: center; font-weight:normal;"
                },
                headerAttributes: {
                    style: "text-align: center; width:1px; display:none;"
                },
                template: '<span class="itemname"></span>',
            },
            {
                field: "Edit", title: "Action", width: "60px",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                },
                command: [
                    {
                        name: 'Map Recipe',
                        click: function (e) {
                            
                            Utility.Loading();
                            setTimeout(function () {
                                var gridObj = $("#gridDish").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                DishName = tr.Name;
                                $("#dishid").val(tr.ID);
                                $("#inputdishcode1").text(tr.DishCode);
                                var cuisineText = " Cuisine: " + tr.CuisineName + " | ";
                                $("#inputcuisinelabel").text(cuisineText);
                                if (tr.Name != tr.DishAlias && tr.DishAlias != "" && tr.DishAlias != null)
                                    $("#inputdishname1").text(tr.Name + "(" + tr.DishAlias + ")");
                                else
                                    $("#inputdishname1").text(tr.Name);
                                if (tr.AllergensName != "" && tr.AllergensName != null) {
                                    var allergenText = " | Allergens: " + tr.AllergensName + "";
                                    $("#inputAllergenLabel").text(allergenText);
                                }
                                $("#inputdishtype1").text(tr.DishTypeName);
                                $("#inputdishcategory1").text(tr.DishCategoryName);
                                DishCode = tr.DishCode;
                                Dish_ID = tr.ID;
                                $("#ItemMaster").hide();
                                $("#mapRecipe").show();
                                if (tr.DietCategoryName == "Veg") {
                                    $("#itemDietCategoryStatus").removeClass("statusNonVeg statusEgg");
                                    $("#itemDietCategory").removeClass("squareNonVeg squareEgg");
                                    $("#itemDietCategoryStatus").addClass("statusVeg");
                                    $("#itemDietCategory").addClass("squareVeg")
                                } else if (tr.DietCategoryName == "Non-Veg") {
                                    $("#itemDietCategoryStatus").removeClass("statusVeg statusEgg");
                                    $("#itemDietCategory").removeClass("squareVeg squareEgg");
                                    $("#itemDietCategory").addClass("squareNonVeg")
                                    $("#itemDietCategoryStatus").addClass("statusNonVeg")
                                } else {
                                    $("#itemDietCategoryStatus").removeClass("statusNonVeg statusVeg");
                                    $("#itemDietCategory").removeClass("squareNonVeg squareVeg");
                                    $("#itemDietCategory").addClass("squareEgg")
                                    $("#itemDietCategoryStatus").addClass("statusEgg")
                                }
                                $("#iname").text(tr.Name + " (Dish Code: " + tr.DishCode + ")");

                                // $("#topHeading").text("Unit Dish Configuration - Map Recipe");
                                var dropdownlist = $("#inputsite").data("kendoDropDownList");
                                // var siteName = dropdownlist.text();
                                $("#topHeading").text("Unit Dish Configuration - Map Recipe");//.append("(" + siteName + ")");
                                //  $("#location").text(" | " + siteName);
                                //call and Grid genration
                                multiarray = [];
                                populateRecipeGridCustom();
                                Utility.UnLoading();
                            }, 2000);
                        }
                    }
                ],
            }
        ],
        pageable: {
            pageSize: 15
        },
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteDishDataList, function (result) {
                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }
                    }, {
                        siteCode: siteCode

                    }, true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        DishCode: { type: "string", editable: false },
                        Name: { type: "string", editable: false },
                        DishAlias: { type: "string", editable: false },
                        UOMName: { type: "string", editable: false },
                        DishCategoryName: { type: "string", editable: false },
                        DishTypeName: { type: "string", editable: false },
                        DietCategoryName: { type: "string", editable: false },
                        IsActive: { type: "boolean", editable: false },
                    }
                }
            },
            //pageSize: 15,
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var items = e.sender.items();

            items.each(function (e) {


            });
        },
        change: function (e) {
        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
    //$(".k-label")[0].innerHTML.replace("items", "records");
}
function populateSiteMasterDropdown() {
    $("#inputsite").kendoDropDownList({
        filter: "contains",
        dataTextField: "SiteName",
        dataValueField: "SiteCode",
        dataSource: sitemasterdata,
        index: 0,
    });

    if (sitemasterdata.length == 1) {
        var dropdownlist = $("#inputsite").data("kendoDropDownList");
        dropdownlist.wrapper.hide();
        siteCode = dropdownlist.value();
        populateDishGrid(siteCode);
        $("#myInputDishSearch").show();
        $("#actionDishSite").show();
        Utility.UnLoading();
        var siteName = dropdownlist.text();
        $("#location").text(" | " + siteName);

    }
}

HttpClient.MakeRequest(CookBookMasters.GetSiteDataListByUserId, function (data) {
    var dataSource = data;
    
    sitemasterdata = dataSource;
    sitemasterdata.forEach(function (item) {
        item.SiteName = item.SiteName + " - " + item.SiteCode;
        return item;
    })
    sitemasterdata = sitemasterdata.filter(item => item.IsActive != false);

    if (data.length > 1) {
        sitemasterdata.unshift({ "SiteCode": 0, "SiteName": "Select" });
        $("#btnGo").css("display", "inline-block");
    }
    else {
        $("#btnGo").css("display", "inline-none");
    }
    populateSiteMasterDropdown();
    

}, null, false);

HttpClient.MakeRequest(CookBookMasters.GetDietCategoryDataList, function (data) {

    var dataSource = data;
    dietcategorydata = [];
    dietcategorydata.push({ "value": "Select Diet Category", "text": "Select" });
    for (var i = 0; i < dataSource.length; i++) {
        dietcategorydata.push({ "value": dataSource[i].DietCategoryCode, "text": dataSource[i].Name });
    }
    populateDietCategoryDropdownFilter();
}, null, false);

function populateRegionMasterDropdown() {
    $("#ddlRegionMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "RegionDesc",
        dataValueField: "RegionID",
        dataSource: regionmasterdata,
        index: 0,
    });
    if (regionmasterdata.length == 1) {
        $("#btnGo").click();

    }

}

function customData(DishCode) {
    mappedRecipeCode = [];
    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteDishRecipeMappingDataList, function (data) {

        mappedData = data;
        for (var d of mappedData) {
            mappedRecipeCode.push(d.RecipeCode);
        }

    }, { SiteCode: siteCode, DishCode: DishCode } , true);
    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteRecipeDataList, function (data) {
        
        recipeCustomData = data;
        recipeCustomData = recipeCustomData.filter(function (item) {
            for (var d of mappedData) {
                if (d.RecipeCode == item.RecipeCode) {
                    item.IsDefault = d.IsDefault;
                    item.DishCode = d.DishCode;
                    item.IsActive = d.IsActive;
                    return item;
                }
            }
        })
    }, { SiteCode: siteCode, condition: "All" }, true);

}


$(function () {
  

    var datamodel;

    var dishcategorydata = [];




    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $('#myInputDish').on('input', function (e) {
        var grid = $('#gridDish').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "DishCode" || x.field == "Name" || x.field == "DishAlias" || x.field == "UOMName" || x.field == "DishCategoryName"
                    || x.field == "DishTypeName" || x.field == "DietCategoryName") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        //if (x.field == "Status") {
                        //    var pendingString = "pending";
                        //    var savedString = "saved";
                        //    var mappedString = "mapped";
                        //    if (pendingString.includes(e.target.value.toLowerCase())) {
                        //        targetValue = "1";
                        //    }
                        //    else if (savedString.includes(e.target.value.toLowerCase())) {
                        //        targetValue = "2";
                        //    }
                        //    else if (mappedString.includes(e.target.value.toLowerCase())) {
                        //        targetValue = "3";
                        //    }
                        //} || x.field == "Status"
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
    $(document).ready(function () {

        $("#siteconfigwindowEdit1").kendoWindow({
            modal: true,
            width: "1200px",
            height: "550px",
            title: "Recipe Details - " + RecipeName,
            actions: ["Close"],
            visible: false,
            draggable: true,
            resizable: true,
            animation: false
        });

        $("#siteconfigwindowEdit2").kendoWindow({
            modal: true,
            width: "1020px",
            height: "550px",
            title: "Recipe Details Inline- " + RecipeName,
            actions: ["Close"],
            visible: false,
            animation: false
        });
        $("#siteconfigwindowEdit3").kendoWindow({
            modal: true,
            width: "1020px",
            height: "550px",
            title: "Recipe Details Inline- " + RecipeName,
            actions: ["Close"],
            visible: false,
            animation: false
        });
        $("#siteconfigwindowEdit4").kendoWindow({
            modal: true,
            width: "1020px",
            height: "550px",
            title: "Recipe Details Inline- " + RecipeName,
            actions: ["Close"],
            visible: false,
            animation: false
        });
        $("#siteconfigwindowEdit5").kendoWindow({
            modal: true,
            width: "1020px",
            height: "550px",
            title: "Recipe Details Inline- " + RecipeName,
            actions: ["Close"],
            visible: false,
            animation: false
        });

        $("#windowEdit1").kendoWindow({
            modal: true,
            width: "1020px",
            height: "550px",
            title: "Region Recipe Details Inline- ",
            actions: ["Close"],
            visible: false,
            draggable: false,
            resizable: false,
            animation: false
        });

        HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            if (user.SectorNumber == 10) {
                $(".googleonly").css("display", "inline-block");
            }
            else {
                $(".googleonly").css("display", "none");
            }
            if (user.SectorNumber == "20") {
                $(".mfgHide").hide();
            }
            else {
                $(".mfgHide").show();
            }
        }, null, false);

        $("#btnGo").on("click", function () {
            var dropdownlist = $("#inputsite").data("kendoDropDownList");
            siteCode = dropdownlist.value();
            populateDishGrid(siteCode);
            $("#myInputDishSearch").show();
            $("#actionDishSite").show();
            Utility.UnLoading();
            var siteName = dropdownlist.text();
            $("#location").text(" | " + siteName);
            RegionID = dropdownlist.dataItem().RegionID;
        });

    });



    //Food Program Section Start

    


    $("#btnCancelDish").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });


    $("#btnSubmitDish").click(function () {
        ;
        if ($("#inputdishname").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputdishname").focus();
        }
        else {
            var localdishcategorydata = [];

            localdishcategorydata = dishcategorydata.filter(function (item) {

                return item.value == $("#inputdishcategory").val();
            });

            var dishcategoryid = localdishcategorydata[0].DishCategoryID;

            var model;
            if (datamodel != null) {
                model = datamodel;
                model.DishCode = $("#inputdishcode").val();
                model.Name = $("#inputdishname").val();
                model.DishAlias = $("#inputdishalias").val();
                model.DietCategoryCode = $("#inputdietcategory").val();
                model.DishCategoryCode = $("#inputdishcategory").val();
                model.DishCategory_ID = dishcategoryid;
                //model.DishSubCategoryCode= $("#inputdishsubcategory").val();
                model.UOMCode = $("#inputuom").val();
                model.ServingTemperatureCode = $("#inputservingtemperature").val();
                model.DishTypeCode = $("#inputdishtype").val();
                model.ColorCode = $(".colortagedit").val();
            }
            else {
                model = {
                    "ID": $("#dishid").val(),
                    "DishCode": $("#inputdishcode").val(),
                    "Name": $("#inputdishname").val(),
                    "DishAlias": $("#inputdishalias").val(),
                    "DietCategoryCode": $("#inputdietcategory").val(),
                    "DishCategoryCode": $("#inputdishcategory").val(),
                    "DishCategory_ID": dishcategoryid,
                    //"DishSubCategoryCode": $("#inputdishsubcategory").val(),
                    "UOMCode": $("#inputuom").val(),
                    "ServingTemperatureCode": $("#inputservingtemperature").val(),
                    "DishTypeCode": $("#inputdishtype").val(),
                    "IsActive": true,
                    "ColorCode": $(".colortagedit").val(),
                }
            }

            $("#btnSubmitDish").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveDishData, function (result) {

                if (result == false) {
                    $('#btnSubmitDish').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputdishname").focus();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#windowEditDish").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmitDish').removeAttr("disabled");
                    if (model.ID > 0)
                        toastr.success("Dish configuration updated successfully");
                    else
                        toastr.success("New Dish added successfully");
                    $("#gridDish").data("kendoGrid").dataSource.data([]);
                    $("#gridDish").data("kendoGrid").dataSource.read();
                    //});
                }
            }, {
                model: model

            } , true);
        }
    });
});

function back() {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $("#ItemMaster").show();
            $("#mapRecipe").hide();
            $("#topHeading").text("Unit Dish Configuration");
        },
        function () {
        }
    );
}

function backDish() {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $(".k-window").hide();
            $(".k-overlay").hide();   
        },
        function () {
        }
    );
}


function populateDietCategoryDropdownFilter() {

    $("#dietcategorysearch").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: dietcategorydata,
        index: 0,
        select: function (e) {
            if (e.item.data().offsetIndex == 0) {
                //e.preventDefault();
                var filter = { logic: 'or', filters: [] };
                var grid = $('#gridDish').data('kendoGrid');
                grid.dataSource.filter(filter);
                return;
            }

            var grid = $('#gridDish').data('kendoGrid');
            var columns = grid.columns;

            var filter = { logic: 'or', filters: [] };
            columns.forEach(function (x) {
                if (x.field) {
                    if (x.field == "DietCategoryName") {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;
                        var targetValue = e.sender.dataItem(e.item).text;

                        if (type == 'string') {
                            if (x.field == "Status") {
                                var pendingString = "pending";
                                var savedString = "saved";
                                var mappedString = "mapped";
                                if (pendingString.includes(targetValue.toLowerCase())) {
                                    targetValue = "1";
                                }
                                else if (savedString.includes(targetValue.toLowerCase())) {
                                    targetValue = "2";
                                }
                                else if (mappedString.includes(targetValue.toLowerCase())) {
                                    targetValue = "3";
                                }
                            }
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: targetValue
                            })
                        }
                        else if (type == 'number') {

                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        } else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format(x.format, data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(targetValue) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                }
            });
            grid.dataSource.filter(filter);

        }

    });
}


function populateRecipeGridCustom() {


    customData(DishCode);

    var gridVariable = $("#gridRecipeCustom");//Hare Krishna!!
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "Recipe.xlsx",
            filterable: true,
            allPages: true
        },
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        //pageable: {
        //    pageSize: 15
        //},
        columns: [
            {
                field: "RecipeCode", title: "Recipe Code", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Recipe Name", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.Name) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetailsCustom(this)'></i>`;
                    return html;
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "CostPerKG", title: "CostPerKG", format: Utility.Cost_Format,  width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },

            {
                field: "IsBase", title: "Base Recipe", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: '# if (IsBase == true) {#<div>Yes</div>#} else {#<div>No</div>#}#',
            },
            {

                template: templateIncludeFunction,

                width: "35px", title: "Include", field: "IsActive",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center; vertical-align:middle"
                }
            },

            {
                field: "IsDefault", title: "Default ", width: "80px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: templateFunction,
            }
        ],
        dataSource: {
            data: recipeCustomData,
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Name: { editable: false },
                        IsActive: { editable: false }
                    }
                }
            },
            pageSize: 15,
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {

        },
        change: function (e) {
        },

    })
    function templateFunction(dataItem) {
        var item = "<div class='icheck-success d-inline'>"
        if (dataItem.IsDefault) {
            item += "<input type='radio' style='margin-top: 7px;' name='" + dataItem.DishCode + "' id='" + dataItem.RecipeCode + "' onclick='setDataItem(this);'   checked=checked />";
            console.log(dataItem);
            checkedRecipeCode = dataItem.RecipeCode;
        } else {
            item += "<input  style='margin-top: 7px;' type='radio' onclick='setDataItem(this);'  id='" + dataItem.RecipeCode + "' name='" + dataItem.DishCode + "'/>";
        }
        item += "<label for='" + dataItem.RecipeCode + "' class='form-control-plaintext' style='display: inline!important'></label ></div>"
        return item;
    };

    function templateIncludeFunction(dataItem) {
        var item = "<label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px'>"
        if (dataItem.IsActive) {
            item += "<input type='checkbox' checked class='checkbox'>";
        } else {
            item += "<input type='checkbox' class='checkbox'>";
        }
        item += "<span class='checkmarkgrid'></span></label>"
        return item;
    };


    //bind click event to the checkbox
    var grid = gridVariable.data("kendoGrid");
    grid.table.on("click", ".checkbox", selectRow);
}
function selectRow() {
    var checked = this.checked,
        td = $(this).closest("td"),
        row = $(this).closest("tr"),
        grid = $("#gridRecipeCustom").data("kendoGrid"),
        dataItem = grid.dataItem(row);
    dataItem.IsActive = this.checked;
    grid.dataItem(row).IsActive = dataItem.IsActive;
    checkedIds[dataItem.id] = checked;
    console.log(dataItem);
}

function setDataItem(item) {
    var grid = $("#gridRecipeCustom").data("kendoGrid");
    var row = $(item).closest("tr");
    var dataItem = grid.dataItem(row);
    console.log(dataItem)
    dataItem.IsDefault = true;
    checkedRecipeCode = dataItem.RecipeCode;
    for (x of recipeCustomData) {
        if (x.RecipeCode == checkedRecipeCode) {
            x.IsDefault = true;
        }
        else {
            x.IsDefault = false;
        }
    }

    var gridObj = $("#gridRecipeCustom").data("kendoGrid");
    //var gridData = gridObj.dataSource._data;

    for (var i = 0; i < gridObj.dataSource._data.length; i++) {
        if (gridObj.dataSource._data[i].RecipeCode == checkedRecipeCode) {
            gridObj.dataSource._data[i].IsDefault = true;
        }
        else {
            gridObj.dataSource._data[i].IsDefault = false;
        }
    }
}

function saveDB() {

    //make model;
    //grid data
    var grid = $("#gridRecipeCustom").data("kendoGrid");
    var currentData = grid._data;
    // saveModel = [];
    //Validation check

    var validData = currentData.filter(item => item.IsActive && item.IsDefault);
    if (validData == null || validData.length == 0) {
        toastr.error("Please select at least one Default  Recipe.");
        return false;
    }
    for (item of currentData) {
        for (mainItem of mappedData) {
            if (item.RecipeCode == mainItem.RecipeCode) {
                mainItem.IsActive = item.IsActive;
                mainItem.IsDefault = item.IsDefault;
                mainItem.CreatedOn = kendo.parseDate(item.CreatedOn);
                mainItem.CreatedBy = item.CreatedBy;
                mainItem.ModifiedOn = Utility.CurrentDate();
                mainItem.ModifiedBy = user.UserId;
            }
        }
    }
    //return;
    //call and message display 
    if (mappedData.length > 0) {


        HttpClient.MakeRequest(CookBookMasters.SaveSiteDishRecipeMappingList, function (result) {
            if (result == false) {
                toastr.error("Some error occured, please try again.");
            }
            else {
                $(".k-overlay").hide();
                toastr.success("The selected Recipe(s) has been mapped to this Dish");
            }
        }, {
            model: mappedData

        });
    }
}

function showDetailsCustom(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridRecipeCustom").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    var gridObj = $("#gridRecipeCustom").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.Name == dataItem.Name)
            return x;
    })
    var dialog = $("#siteconfigwindowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    Utility.Loading();
    setTimeout(function () {
        recipeCategoryHTMLPopulateOnEditRecipe("", tr);
        $("#mainContent").hide();
        $("#windowEdit").show();
        Recipe_ID = tr.Recipe_ID;
        populateMOGGrid(tr.Recipe_ID, tr.RecipeCode);
        populateBaseRecipeGrid(tr.Recipe_ID, tr.RecipeCode);
        $("#recipeid").val(tr.ID);
        $("#inputrecipecode1").text(tr.RecipeCode);
        $("#inputrecipename1").text(tr.Name);
        $("#inputrecipealiasname1").text(tr.RecipeAlias);
        $("#inputuom1").text(tr.UOMName);
        $("#inputquantity1").text(tr.Quantity);
        if (tr.IsBase == true)
            $("#inputbase1").text("Base Recipe");
        else
            $("#inputbase1").text("Final Recipe");

        $('#grandTotal1').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
        $('#grandCostPerKG1').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
        $('#regiongrandTotal1').html(Utility.AddCurrencySymbol(tr.RegionTotalCost, 2));
        oldcost = tr.TotalCost;// tr.RegionTotalCost;
        $("#inputinstruction1").text(tr.Instructions)
        var change = 0; //(tr.TotalCost - oldcost).toFixed(2);
        var changeperc = 0; //(((change*100) / oldcost)).toFixed(2);
        $(".totmoglbl").show();
        if (user.SectorNumber == "20") {
            $(".changegrandtotal").css("display", "none");
            $("#percentageChangeDiv1").css("display", "none");
            $(".totmoglbl").hide();
        }
        
        else {
            $("#changegrandTotal1").html(Utility.AddCurrencySymbol(change, 2));
            $("#totalchangepercent1").html(Utility.AddCurrencySymbol(changeperc, 2));
            if (isNaN(oldcost)) {
                $("#changegrandTotal1").hide();
                $("#totalchangepercent1").hide();
            } else {
                $("#changegrandTotal1").show();
                $("#totalchangepercent1").show();
            }
            if (change == 0) {
                $("#changegrandTotal1").html("No Change");
            }

        }
        //hideGrid('gridBaseRecipe1', 'emptyBr', 'baseRecipePlus', 'baseRecipeMinus');
        //hideGrid('gridMOG1', 'emptymog', 'mogPlus', 'mogMinus');
        dkgtotal();
        dialog.title("Recipe Details Inline - " + RecipeName);
        return true;
    }, 2000);
    Utility.UnLoading();
    return true;
}

function dkgtotal() {
    var btot = isNaN(parseFloat($("#totBaseRecpQt1y").text())) ? 0 : parseFloat($("#totBaseRecpQty1").text());
    var mtot = isNaN(parseFloat($("#totmog1").text())) ? 0 : parseFloat($("#totmog1").text());
    var tot = (btot + mtot).toFixed(2);
    $("#ToTIngredients1").text(tot);
}

function populateUOMDropdown() {
    $("#inputuom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}

function populateBaseDropdown() {
    $("#inputbase").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}

function populateBaseRecipeGrid(recipeID, recipeCode) {
    if (recipeID == null)
        recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe1");
    gridVariable.html("");

    Utility.Loading();
    gridVariable.kendoGrid({
        groupable: false,
        editable: false,
        navigatable: true,
        columns: [
            {
                field: "BaseRecipeName", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Site Qty", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "RegionIngredientPerc", title: "Qty Change %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= RegionIngredientPerc-IngredientPerc# </div>#} #',
            },
            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },

            {
                field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            var obj = result;
                            BaseRecipetotal = 0.0;
                            $.each(obj, function (key, value) {

                                BaseRecipetotal = parseFloat(BaseRecipetotal) + parseFloat(value.Quantity);
                            });
                            $("#totBaseRecpQty1").text(BaseRecipetotal.toFixed(2));
                            options.success(result);
                            if (result.length > 0) {
                                $("#gridBaseRecipe1").css("display", "block");
                                $("#emptybr1").css("display", "none");
                            } else {
                                $("#gridBaseRecipe1").css("display", "none");
                                $("#emptybr1").css("display", "block");
                            }

                        }
                        else {
                            options.success("");
                        }


                    },
                        {
                            recipeCode: recipeCode,
                            SiteCode: $("#inputsite").val(),
                            recipeID: recipeID
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },

        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },

        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();
            items.each(function (e) {
                var dataItem = grid.dataItem(this);

            });
        },
        change: function (e) {
        },
    });

}

function populateMOGGrid(recipeID, recipeCode) {
    if (user.SectorNumber == "20") {
        Utility.Loading();
        var gridVariable = $("#gridMOG1");
        gridVariable.html("");
        gridVariable.kendoGrid({
            groupable: false,
            editable: true,
            //  navigatable: true,
            columns: [
                {
                    field: "MOGName", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },

                {
                    field: "UOMName", title: "UOM", width: "60px",
                    attributes: {
                        style: "text-align: left; font-weight:normal",
                        class: ''
                    },
                },

                {
                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: left;width:35px;"
                    },
                },

                {
                    field: "DKgTotal", title: "DKG", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },
                    attributes: {
                        class: "dkgtotal",
                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },

                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                if (user.SectorNumber == "20") {
                                    $.each(obj, function (key, value) {
                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.DKgValue * value.Quantity);
                                    });
                                }
                                else {
                                    $.each(obj, function (key, value) {
                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                    });
                                }

                                $("#totmog").text(mogtotal.toFixed(2));
                                var tot = parseFloat($("#totmog").text()).toFixed(2);

                                $("#ToTIngredients").text(tot);
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG1").css("display", "block");
                                $("#emptymog1").css("display", "none");
                            } else {
                                $("#gridMOG1").css("display", "none");
                                $("#emptymog1").css("display", "block");
                            }
                        },
                            {

                                SiteCode: $("#inputsite").val(),
                                recipeID: 0,
                                recipeCode: recipeCode,
                                regionID: RegionID
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            RegionQuantity: { type: "number", editable: false },
                            RegionIngredientPerc: { type: "number", editable: false },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false },
                            RegionTotalCost: { editable: false },
                            DKgValue: { editable: false },
                            DKgTotal: { editable: false },
                            ArticleNumber: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                   
                });
            },
            change: function (e) {
            },
        });
    }
    else if (user.SectorNumber == "80") {
        Utility.Loading();
        var gridVariable = $("#gridMOG1");
        gridVariable.html("");
        gridVariable.kendoGrid({
            groupable: false,
            editable: false,
            navigatable: true,
            columns: [
                {
                    field: "MOGName", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
               
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Site Qty", width: "50px",

                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                },


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "regiontotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                $.each(obj, function (key, value) {

                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                });

                                $("#totmog").text(mogtotal.toFixed(2));
                                dkgtotal();
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG1").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG1").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {

                                SiteCode: $("#inputsite").val(),
                                recipeID: 0,
                                recipeCode: recipeCode,
                                regionID: RegionID
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                });
            },
            change: function (e) {
            },
        });
    }
    else {
        Utility.Loading();
        var gridVariable = $("#gridMOG1");
        gridVariable.html("");
        gridVariable.kendoGrid({
            groupable: false,
            editable: false,
            navigatable: true,
            columns: [
                {
                    field: "MOGName", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
               
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "RegionQuantity", title: "Region Qty", width: "50px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Site Qty", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "RegionIngredientPerc", title: "Qty Change % ", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= ((Quantity-RegionQuantity)*100/RegionQuantity).toFixed(2)# </div>#} #',
                },


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "RegionTotalCost", title: "Region  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "regiontotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Site Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                mogtotal = 0.0;
                                $.each(obj, function (key, value) {

                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                });

                                $("#totmog").text(mogtotal.toFixed(2));
                                dkgtotal();
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG1").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG1").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {

                                SiteCode: $("#inputsite").val(),
                                recipeID: 0,
                                recipeCode: recipeCode,
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                });
            },
            change: function (e) {
            },
        });
    }
}

function showHideGrid(gridId, emptyGrid, uid) {

    var gridLength = $("#" + gridId).data().kendoGrid.dataSource.data().length;

    if (gridLength == 0) {
        $("#" + emptyGrid).toggle();
        $("#" + gridId).hide();
    }
    else {
        $("#" + emptyGrid).hide();
        $("#" + gridId).toggle();
    }

    if (!($("#" + gridId).is(":visible") || $("#" + emptyGrid).is(":visible"))) {
        $("#" + uid).addClass("bottomCurve");

    } else {
        $("#" + uid).removeClass("bottomCurve");

    }

}


function recipeCategoryHTMLPopulateOnEditRecipe(level, tr) {
    if (user.SectorNumber == "20") {
        if (tr.RecipeUOMCategory)
            $("#inputrecipecum" + level + "").text(tr.RecipeUOMCategory);

        var recipeCategory = tr.RecipeUOMCategory;
        if (tr.RecipeUOMCategory == undefined || tr.RecipeUOMCategory == null || tr.RecipeUOMCategory == "") {
            recipeCategory = "UOM-00001";
            $("#inputrecipecum" + level + "").text("UOM-00001");
        }
        if (tr.WeightPerUnit)
            $("#inputwtperunit" + level + "").text(tr.WeightPerUnit);
        if (tr.WeightPerUnitGravy)
            $("#inputgravywtperunit" + level + "").text(tr.WeightPerUnitGravy);
        if (tr.WeightPerUnitUOM)
            $("#inputwtperunituom" + level + "").text(tr.WeightPerUnitUOM);
        if (tr.WeightPerUnitUOMGravy)
            $("#inputgravywtperunituom" + level + "").text(tr.WeightPerUnitUOMGravy);

    }
}


function backCreate() {
    $("#mapRecipe").show();
    $("#windowEdit0").hide();
}

$("#btnCancel1").on("click", function () {
    $(".k-overlay").hide();
    var orderWindow = $("#siteconfigwindowEdit1").data("kendoWindow");
    orderWindow.close();
});
