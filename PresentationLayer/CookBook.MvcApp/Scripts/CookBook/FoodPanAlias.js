﻿$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var Name = "";
    var sitedata = [];
    var dkdata = [];

    $("#windowEdit").kendoWindow({
        modal: true,
        width: "400px",
        height: "150px",
        title: "Container Type Details  ",
        actions: ["Close"],
        visible: false,
        animation: false

    });

    $('#myInput').on('input', function (e) {
        var grid = $('#gridFoodPanAlias').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "FoodPanAliasCode" || x.field == "Name") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    var user;
    $(document).ready(function () {
        $("#btnExport").click(function (e) {
            var grid = $("#gridFoodPanAlias").data("kendoGrid");
            grid.saveAsExcel();
        });
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.UserRoleId === 1) {

            //    $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {

            //    $("#AddNew").css("display", "none");
            //}
            populateFoodPanAlias();
        }, null, false);


    });


    //Food Program Section Start

    function populateFoodPanAlias() {

        Utility.Loading();
        var gridVariable = $("#gridFoodPanAlias");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "FoodPanAlias.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "FoodPanAliasCode", title: "Alias Code", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Name", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive",
                    title: "Status", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                },
                {
                    field: "Edit", title: "Action", width: "50px",

                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = $("#gridFoodPanAlias").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                Name = tr.Name;
                                var dialog = $("#windowEdit").data("kendoWindow");
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open();
                                dialog.center();
                                // 
                                $("#FoodPanAliasid").val(tr.ID);
                                $("#FoodPanAliasCode ").val(tr.FoodPanAliasCode);
                                $("#name").val(tr.Name);

                                //if (user.UserRoleId === 1) {
                                //    $("#name").removeAttr('disabled');

                                //    $("#btnSubmit").css('display', 'inline');
                                //    $("#btnCancel").css('display', 'inline');
                                //} else {
                                //    $("#name").attr('disabled', 'disabled');
                                //    $("#btnSubmit").css('display', 'none');
                                //    $("#btnCancel").css('display', 'none');
                                //}

                                dialog.title("Details - " + Name);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetFoodPanAliasDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null

                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {

                            
                            Name: { editable: false, type: "string" },
                            FoodPanAliasCode: { type: "string" },
                            IsActive: { editable: false }
                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();
                // 
                //items.each(function (e) {
                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //        $(this).find('.chkbox').removeAttr('disabled');

                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //        $(this).find('.chkbox').attr('disabled', 'disabled');
                //    }
                //});

                $(".chkbox").on("change", function () {
                    var gridObj = $("#gridFoodPanAlias").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    var th = this;
                    datamodel = tr;
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> FoodPanAlias " + active + "?", "FoodPanAlias Update Confirmation", "Yes", "No", function () {
                        HttpClient.MakeRequest(CookBookMasters.ChangeStatus, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                toastr.success("container Type updated successfully");
                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {

                        $(th)[0].checked = !datamodel.IsActive;
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }



    $("#AddNew").on("click", function () {
        var model;
        var dialog = $("#windowEdit").data("kendoWindow");
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        datamodel = model;
        $("#FoodPanAliasid").val("");
        $("#FoodPanAliasCode").val("");
        $("#name").val("");

        dialog.title("New Container Type Creation");
    })

    $("#btnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });





    $("#btnSubmit").click(function () {

        if ($("#name").val() === "") {
            toastr.error("Please provide Alias Name");
            $("#name").focus();
            return;
        }
        else {
            var model;
            if (datamodel != null) {
                model = datamodel;
                model.FoodPanAliasCode = $("#FoodPanAliasCode").val();
                model.Name = $("#name").val();
            }
            else {
                model = {
                    "ID": $("#FoodPanAliasid").val(),
                    "FoodPanAliasCode": $("#FoodPanAliasCode").val(),
                    "Name": $("#name").val(),
                    "IsActive": true,
                }
            }


            $("#btnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveFoodPanAliasData, function (result) {
                if (result == false) {
                    $('#btnSubmit').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");

                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#windowEdit").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit').removeAttr("disabled");
                    toastr.success("Container Type updated successfully");
                    $("#gridFoodPanAlias").data("kendoGrid").dataSource.data([]);
                    $("#gridFoodPanAlias").data("kendoGrid").dataSource.read();
                }
            }, {
                model: model
            }, false);
        }
    });
});