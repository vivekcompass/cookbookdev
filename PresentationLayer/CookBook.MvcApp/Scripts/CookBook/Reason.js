﻿var user;
var datamodel;
var reasontypedata = [];
var reasondata = [];
var ReasonCodeIntial = null;
function populateReasonTypedrpodown() {
    $("#dpreasontype").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "ReasonTypeCode",
        dataSource: reasontypedata,
        index: 0,

        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In-Active State");
                var dropdownlist = $("#dpreasontype").data("kendoDropDownList");
                dropdownlist.select("");
            }

        }
    });
}

$(document).ready(function () {
    $(".k-window").hide();
    $(".k-overlay").hide(); 
    $("#ReaswindowEdit").kendoWindow({
        modal: true,
        width: "300px",

        title: "Reason Details  ",
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $('#myInput').on('input', function (e) {
        var grid = $('#gridReason').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ReasonCode" || x.field == "ReasonDescription" || x.field =="ReasonTypeName") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    HttpClient.MakeSyncRequest(CookBookMasters.GetReasonTypeDataList, function (result) {
        dataSource = result;
        reasontypedata.push({ "value": null, "text": "Select", "ReasonTypeCode": null, "IsActive": 1 });
        for (var i = 0; i < dataSource.length; i++) {
            reasontypedata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "ReasonTypeCode": dataSource[i].ReasonTypeCode, "IsActive": dataSource[i].IsActive });
        }
        populateReasonTypedrpodown();
      
       populateReasonGrid();
    }, null, false);
});

function populateReasonGrid() {

    Utility.Loading();
    var gridVariable = $("#gridReason");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        columns: [
            {
                field: "ReasonCode", title: "Code", width: "70px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ReasonDescription", title: "Description", width: "275px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ReasonTypeName", title: "Reason Type", width: "250px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            //{
            //    field: "IsActive", title: "Active", width: "100px", attributes: {

            //        style: "text-align: center; font-weight:normal"
            //    },
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
            //},
            {
                field: "Edit", title: "Action", width: "100px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                command: [
                    {
                        name: 'Edit',
                        click: function (e) {
                           var tr = $(e.target).closest("tr");    // get the current table row (tr)

                            var item = this.dataItem(tr);
                            if (!item.IsActive) {
                                return;
                            }// get the date of this row
                            var model = {
                                "ID": item.ID,
                                "ReasonDescription": item.ReasonDescription,
                                "IsActive": item.IsActive,
                                "ReasonCode": item.ReasonCode,
                                "ApplicableTo": null,
                                "CreatedBy": item.CreatedBy,
                                "CreatedOn": kendo.parseDate(item.CreatedOn)

                            };
                            ReasonCodeIntial = item.ReasonCode;
                            var dialog = $("#ReaswindowEdit").data("kendoWindow");
                               dialog.title("Reason Details : ");
                            model.ApplicableTo = item.ApplicableTo;
                           $("#dpreasontype").data('kendoDropDownList').value(model.ApplicableTo);
                           
                            dialog.open();
                            dialog.center();

                            datamodel = model;

                            $("#dpid").val(model.ID);
                            $("#dpcode").val(model.ReasonCode);
                            $("#dpname").val(model.ReasonDescription);
                            $("#dpname").focus();


                            return true;
                        }
                    }
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetReasonDataList, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            //Result modifiy
                            reasondata = result;
                          
                                result.forEach(function (item) {
                                    if (item.ApplicableTo == null) {
                                        item.ApplicableTo = null;
                                        return item;
                                    }
                                    for (i of reasontypedata) {
                                        if (item.ApplicableTo == i.ReasonTypeCode) {
                                            item.ReasonTypeName = i.text;
                                            return item;
                                        }

                                    }
                                    item.ReasonTypeName = null;
                                    return item;
                                })
                          

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }
                    }, null
                        //{
                        //filter: mdl
                        //}
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ReasonTypeName: { type: "string" },
                        ReasonCode: { type: "string" },
                        ReasonDescription: { type: "string" },
                        IsActive: { type: "Boolean" },
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var items = e.sender.items();
            var grid = this;
            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);

                if (!model.IsActive) {
                    $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                }
            });
            
            $(".chkbox").on("change", function () {
                var gridObj = $("#gridReason").data("kendoGrid");
                var tr = gridObj.dataItem($(this).closest("tr"));
                var trEdit = $(this).closest("tr");
                var th = this;
                datamodel = tr;
                datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);

                datamodel.IsActive = $(this)[0].checked;
                var active = "";
                var msg = "";
                if (datamodel.IsActive) {
                    active = "Active";
                } else {
                    active = "Inactive";
                }
                     msg = "Reason ";

                

                Utility.Page_Alert_Save("Turing this off may impact parts of this application. Proceed with the change?", " " + msg + " Update Confirmation", "Yes", "No", function () {
                    HttpClient.MakeRequest(CookBookMasters.SaveReason, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again");
                        }
                        else {
                            toastr.success(msg + " configuration updated successfully");
                            $(th)[0].checked = datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        }
                    }, {
                        model: datamodel
                    }, false);
                }, function () {
                    datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);

                    $(th)[0].checked = !datamodel.IsActive;
                    if ($(th)[0].checked) {
                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                    } else {
                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
                return true;
            });

        },
        change: function (e) {
        },
    })
    gridVariable.data("kendoGrid").dataSource.sort({ field: "ReasonCode", dir: "asc" });
  
}
function dpvalidate() {
    var valid = true;

    if ($("#dpname").val() === "") {
        toastr.error("Please provide input");
        $("#dpname").addClass("is-invalid");
        valid = false;
        $("#dpname").focus();
    }
    if (($.trim($("#dpname").val())).length > 59) {
        toastr.error("Reason Description can't accepts upto 60 charactre");

        $("#dpname").addClass("is-invalid");
        valid = false;
        $("#dpname").focus();
    }
    return valid;
}
$(document).ready(function (){
    $("#dpaddnew").on("click", function () {
        var model;
        var dialog = $("#ReaswindowEdit").data("kendoWindow");

        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });

        datamodel = model;

        dialog.title("New Reason Details Creation");




        $("#dpname").removeClass("is-invalid");
        $('#secredpsubmit').removeAttr("disabled");
        $("#dpid").val("");
        $("#dpname").val("");
        $("#dpcode").val("");
        $("#dpname").focus();
        $("#dpreasontype").data('kendoDropDownList').value(null)

        ReasonCodeIntial = null;
    });

    $("#secredpcancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#ReaswindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });
    $("#secredpsubmit").click(function () {
        var msg = "";
       // if (user.SectorNumber == "20") {
            msg = "Reason";
       
        var namedp = $("#dpname").val().toLowerCase().trim();
        if (ReasonCodeIntial == null) { //Duplicate Check
            for (item of reasondata) {
                if (item.ReasonDescription.toLowerCase().trim() == namedp) {
                    toastr.error("Kindly check Duplicate " + msg + " Description");
                    $("#dpname").focus();
                    return;
                }
            }
        } else {
            for (item of reasondata) {
                if (item.ReasonDescription?.toLowerCase().trim() == namedp && item.ReasonCode != ReasonCodeIntial) {
                    toastr.error("Kindly check Duplicate  " + msg + " Description is entered on editing");
                    $("#dpname").focus();
                    return;
                }
            }
        }
        if ($("#dpname").val() === "" || $("#dpname").val() == null) {
            toastr.error("Please provide " + msg + " Name");
            $("#dpname").focus();
            return;
        }
       // if (user.SectorNumber == "20") {
            if ($("#dpreasontype").val() == "" || $("#dpreasontype").val() == null) {
                toastr.error("Please provide " + msg + " Reason Type");
                $("#dpreasontype").focus();
                return;
            }
       // }
        if (dpvalidate() === true) {
            $("#dpname").removeClass("is-invalid");

            var model = {
                "ID": $("#dpid").val(),
                "ReasonDescription": $("#dpname").val(),
                "ReasonCode": ReasonCodeIntial,
                "ApplicableTo": null
            }
            if (ReasonCodeIntial == null) {
                model.IsActive = 1;
            } else {
                model.IsActive = 1;
                model.CreatedBy = datamodel.CreatedBy;
                model.CreatedOn = datamodel.CreatedOn;

            }
           // if (user.SectorNumber == "20") {
            model.ApplicableTo = $("#dpreasontype").data('kendoDropDownList').value()
           // }

            $("#secredpsubmit").attr('disabled', 'disabled');
            if (!sanitizeAndSend(model)) {
                return;
            }
            HttpClient.MakeRequest(CookBookMasters.SaveReason, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#secredpsubmit').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#secredpsubmit').removeAttr("disabled");
                        toastr.success("There was some error, the record cannot be saved");

                    }
                    else {
                        $(".k-overlay").hide();
                        $('#secredpsubmit').removeAttr("disabled");

                        if (model.ID > 0)
                            toastr.success(msg + " configuration updated successfully");
                        else
                            toastr.success("New" + msg + " added successfully");
                        $(".k-overlay").hide();
                        var orderWindow = $("#ReaswindowEdit").data("kendoWindow");
                        orderWindow.close();
                        $("#gridReason").data("kendoGrid").dataSource.data([]);
                        $("#gridReason").data("kendoGrid").dataSource.read();
                        $("#gridReason").data("kendoGrid").dataSource.sort({ field: "ReasonCode", dir: "asc" });

                    }

                }
            }, {
                model: model

            }, true);
        }
    });


});
