﻿var sitetypedata = [];
var xresult;
var SCode;
var daypartdata = [];
var siteDayPartResult = [];
var daypartdataInactive = [];
function populateDayPartMultiSelect() {
    $("#OtherSiteinputdaypart").kendoMultiSelect({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "code",
        dataSource: daypartdata,
        index: 0,
        autoClose: false
    });
}
function GetDayPartMapping() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteDayPartMappingDataList, function (data) {

        siteDayPartResult = data;

    },
        { siteCode: SCode }, true);
    var multiarray = siteDayPartResult.map(item => item.DayPartCode);
    var multiselect = $("#OtherSiteinputdaypart").data("kendoMultiSelect");
    multiselect.value(multiarray);
    Utility.UnLoading();
}
function SaveDayPartMapping() {

    HttpClient.MakeSyncRequest(CookBookMasters.SaveSiteDayPartMappingDataList, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again.");
            dayPartArray = [];
            Utility.UnLoading();
        }
        else {

            if (user.SectorNumber != "20")
                toastr.success("Food Program Mealtime Mapping saved");
            else
                toastr.success("Food Program Service Type Mapping saved");

            Utility.UnLoading();
        }
    }, {
        model: dayPartArray
    }, true);
}

$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var SiteName = "";
    var cpudata = [];
    var dkdata = [];
    var maptypedata = [];
    var billingendtypedata = [];

    var retaildata = [];
    var isSubSectorApplicable = false;

    $("#OtherEditwindowEdit").kendoWindow({
        modal: true,
        width: "680px",
        // height: "400px",
        title: "Site Details - " + SiteName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    //HttpClient.MakeRequest(CookBookMasters.GetDayPartDataList, function (data) {
    //    var dataSource = da

    //    ta;
    //    daypartdata = [];

    //    for (var i = 0; i < dataSource.length; i++) {
    //        if (!dataSource[i].IsActive)
    //            daypartdataInactive.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "code": dataSource[i].DayPartCode, "IsActive": dataSource[i].IsActive });

    //        daypartdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "code": dataSource[i].DayPartCode, "IsActive": dataSource[i].IsActive });
    //    }
    //    populateDayPartMultiSelect();

    //}, null, false);


    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $('#myInput').on('input', function (e) {
        //$(".k-grid-filter k-state-active").css("background-color", "");
        var grid = $('#NationalgridSite').data('kendoGrid');
        
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "SectorName" || x.field == "SiteCode" || x.field == "SiteAlias" || x.field == "City" || x.field == "RegionDesc"
                    || x.field == "CPUName" || x.field == "MapType" || x.field == "Name" || x.field == "Retail") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
        
    });
    $("#btnExport").click(function (e) {
        var grid = $("#NationalgridSite").data("kendoGrid");
        grid.saveAsExcel();
    });
    var user;
    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        //$("#billingstartdate").kendoDatePicker({
        //    format: "dd/MM/yyyy",
        //    dateInput: true
        //});

        //$("#billingenddate").kendoDatePicker({
        //    format: "dd/MM/yyyy",
        //    dateInput: true
        //});

        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteTypeDataList, function (result) {

            sitetypedata.push({ "text": "Select", value: null });

            for (var item of result) {
                sitetypedata.push({ "text": item.Name, "value": item.SiteTypeCode })
            }

        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //}
            if (user.SectorNumber == 20) {
                $(".manufacturing").css("display", "block");
                $("#btnExport").click(function (e) {
                    var grid = $("#NationalgridSite").data("kendoGrid");
                    grid.saveAsExcel();
                });
            }
            else {
                $(".manufacturing").css("display", "none");
            }

            populateSiteGrid();
        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetCPUDataList, function (data) {

            var dataSource = data;
            cpudata = [];
            cpudata.push({ "value": null, "text": "None (Pure Onsite)" });
            for (var i = 0; i < dataSource.length; i++) {
                var SiteName = dataSource[i].SiteAlias;
                if (SiteName == null)
                    SiteName = dataSource[i].SiteName;
                cpudata.push({ "value": dataSource[i].SiteCode, "text": SiteName });
            }
            $("#inputcpu").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: cpudata,
                index: 0,
            });

            $("#inputcpu").data("kendoDropDownList").select(function (dataItem) {
                return dataItem.value === dataSource[0].value;
            });
            $("#inputcpu").data("kendoDropDownList").select(0);
        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetDKDataList, function (data) {
            var dataSource = data;
            dkdata = [];
            dkdata.push({ "value": null, "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                var SiteName = dataSource[i].SiteAlias;
                if (SiteName == null)
                    SiteName = dataSource[i].SiteName;
                dkdata.push({ "value": dataSource[i].SiteCode, "text": SiteName });
            }
            $("#inputdk").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: dkdata,
                index: 0,
            });

            $("#inputdk").data("kendoDropDownList").select(function (dataItem) {
                return dataItem.value === dataSource[0].value;
            });
            $("#inputdk").data("kendoDropDownList").select(0);
        }, null, false);


        HttpClient.MakeRequest(CookBookMasters.GetSubSectorMasterList, function (data) {

            if (data.length > 1) {
                isSubSectorApplicable = true;
                data.unshift({ "SubSectorCode": '0', "Name": "Select Sub Sector" })
                $("#subsector").css("display", "block");
                $("#inputsubsector").kendoDropDownList({
                    filter: "contains",
                    dataTextField: "Name",
                    dataValueField: "SubSectorCode",
                    dataSource: data,
                    index: 0,
                });
                var dialog1 = $("#OtherEditwindowEdit").data("kendoWindow");
                //dialog1.setOptions({
                //    height: 409,
                //});
            }
            else {
                isSubSectorApplicable = false;
                $("#subsector").css("display", "none");
                var dialog1 = $("#OtherEditwindowEdit").data("kendoWindow");
                //dialog1.setOptions({
                //    height: 360,
                //});
            }

        }, { isAllSubSector: false }, false);

        maptypedata = [
            { text: "Select", value: null },
            { text: "MAP 1", value: "MAP 1" },
            { text: "MAP 2", value: "MAP 2" },
            { text: "D-MAP 1", value: "D-MAP 1" },
            { text: "D-MAP 2", value: "D-MAP 2" },
        ];

        //sitetypedata = [
        //    { text: "Select", value: null },
        //    { text: "Onsite", value: "Onsite" },
        //    { text: "Offsite", value: "Offsite" },
        //    { text: "Hybrid", value: "Hybrid" },
        //];

        retaildata = [
            { text: "Select", value: null },
            { text: "Retail", value: "Retail" },
            { text: "Non-Retail", value: "Non-Retail" },
        ];

        $("#inputmaptype").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: maptypedata,
            index: 0,
        });

        $("#inputsitetype").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: sitetypedata,
            index: 0,
        });

        $("#inputretail").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: retaildata,
            index: 0,
        });

        $('input[type=radio][name=r3]').change(function () {
            //alert(this.value);
            if (this.value == "CPU") {
                //$("#inputIsCPU").prop('checked', true);
                // $("#inputsitetype").data('kendoDropDownList').select(1);
                $("#inputsitetype").data('kendoDropDownList').enable(false);
                // $("#inputcpu").data('kendoDropDownList').select(0);
                $("#inputcpu").data('kendoDropDownList').enable(false);
                //  $("#inputdk").data('kendoDropDownList').select(0);
                $("#inputdk").data('kendoDropDownList').enable(false);
            } else if (this.value == "Site") {
                // $("#inputIsCPU").prop('checked', true);
                //  $("#inputsitetype").data('kendoDropDownList').select(1);
                $("#inputsitetype").data('kendoDropDownList').enable(true);
                //  $("#inputcpu").data('kendoDropDownList').select(0);
                $("#inputcpu").data('kendoDropDownList').enable(true);
                // $("#inputdk").data('kendoDropDownList').select(0);
                $("#inputdk").data('kendoDropDownList').enable(true);
            } else {
                //  $("#inputsitetype").data('kendoDropDownList').select(1);
                $("#inputsitetype").data('kendoDropDownList').enable(true);
                //   $("#inputcpu").data('kendoDropDownList').select(0);
                $("#inputcpu").data('kendoDropDownList').enable(true);
                //  $("#inputdk").data('kendoDropDownList').select(0);
                $("#inputdk").data('kendoDropDownList').enable(true);
            }
        });

        $('input[type=radio][name=billingendtype]').change(function () {
            console.log(this.value)
            if (this.value == "Custom") {
                $("#inputbillingenddate").data('kendoDropDownList').enable(true);
            } else {
                $("#inputbillingenddate").data('kendoDropDownList').enable(false);
            }
        });

        populateBillingTypeDateDropDown();
    });

    function populateBillingTypeDateDropDown() {
        billingendtypedata.push({ "value": "select", "text": "select" });
        for (var i = 1; i <= 31; i++) {
            billingendtypedata.push({ "value": i, "text": i });
        }
        $("#inputbillingenddate").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: billingendtypedata,
            index: 0,
        });
        $("#inputbillingenddate").data('kendoDropDownList').enable(false);
    }

    $("#InitiateBulkChanges").on("click", function () {
        populateBulkChangeControls();
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        changeControls.css("background-color", "#fff");
        changeControls.css("border", "none");
        $("#gridBulkChange").css("display", "block");
        $("#gridBulkChange").children(".k-grid-header").css("border", "none");
        $("#InitiateBulkChanges").css("display", "none");
        $("#CancelBulkChanges").css("display", "inline");
        $("#SaveBulkChanges").css("display", "inline");
        $("#bulkerror").css("display", "none");
        $("#success").css("display", "none");
        changeControls.eq(4).text("");
        changeControls.eq(4).append("<div id='cpu' style='width:90%; margin-left:3px; font-size:13.5px!important'></div>");
        changeControls.eq(5).text("");
        changeControls.eq(5).append("<div id='maptype' style='width:90%; margin-left:3px; font-size:13.5px!important'></div>");
        changeControls.eq(6).text("");
        changeControls.eq(6).append("<div id='sitetype' style='width:90%; margin-left:3px; font-size:13.5px!important'></div>");
        changeControls.eq(7).text("");
        changeControls.eq(7).append("<div id='retail' style='width:90%; margin-left:3px; font-size:13.5px!important'></div>");

        $("#maptype").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: maptypedata,
            index: 0,
        });

        $("#sitetype").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: sitetypedata,
            index: 0,
        });

        $("#retail").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: retaildata,
            index: 0,
        });

        $("#cpu").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: cpudata,
            index: 0,
        });
        Utility.UnLoading();
    });

    //$("#retail").on("change", function () {
    //    
    //    var neVal = $(this).val();
    //    var dataFiltered = $("#NationalgridSite").data("kendoGrid").dataSource.dataFiltered();
    //    $.each(dataFiltered, function (index, item) {
    //        item.Retail = neVal;
    //    });
    //});


    $("#CancelBulkChanges").on("click", function () {
        $("#InitiateBulkChanges").css("display", "inline");
        $("#CancelBulkChanges").css("display", "none");
        $("#SaveBulkChanges").css("display", "none");
        $("#gridBulkChange").css("display", "none");
        $("#bulkerror").css("display", "none");
        $("#success").css("display", "none");
        populateSiteGrid();
    });

    function checkFilterApplied() {
        var isFilterApplied = false;
        var grid = $('#NationalgridSite').data('kendoGrid');
        if (grid.dataSource._data.length > grid.dataSource.dataFiltered().length) {
            isFilterApplied = true;
        }
        return isFilterApplied;
    };

    $("#SaveBulkChanges").on("click", function () {
        if (!checkFilterApplied()) {
            toastr.error("Please apply at least one filter for Bulk Update");
            return false;
        }
        var neVal = $(this).val();
        var dataFiltered = $("#NationalgridSite").data("kendoGrid").dataSource.dataFiltered();


        var model = dataFiltered;
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        var validChange = false;
        var cpu = "";
        var maptype = "";
        var sitetype = "";
        var retail = "";

        var cpuspan = $("#cpu");//  changeControls.eq(4).children("span").children("span").children(".k-input");
        var maptypespan = changeControls.eq(5).children("span").children("span").children(".k-input");
        var sitetypespan = changeControls.eq(6).children("span").children("span").children(".k-input");
        var retailtypespan = changeControls.eq(7).children("span").children("span").children(".k-input");

        if (cpuspan.val() != "Select") {
            validChange = true;
            cpu = cpuspan.val();
        }
        if (maptypespan.text() != "Select") {
            validChange = true;
            maptype = maptypespan.text();
        }
        if (sitetypespan.text() != "Select") {
            validChange = true;
            sitetype = sitetypespan.text();
        }
        if (retailtypespan.text() != "Select") {
            validChange = true;
            retail = retailtypespan.text();
        }

        if (validChange == false) {
            toastr.error("Please change at least one attribute for Bulk Update");
        } else {
            Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                function () {
                    $.each(model, function () {
                        this.ModifiedDate = Utility.CurrentDate();
                        this.ModifiedBy = user.UserId;
                        this.CityName = this.City;
                        this.CreatedDate = kendo.parseDate(this.CreatedDate);
                        if (cpu.length > 0)
                            this.CPUCode = cpu;
                        if (maptype.length > 0)
                            this.MapType = maptype;
                        if (sitetype.length > 0)
                            this.SiteType = sitetype;
                        if (retail.length > 0)
                            this.Retail = retail;
                    });

                    
                    HttpClient.MakeRequest(CookBookMasters.SaveSiteDataList, function (result) {
                        if (result == false) {
                            $("#bulkerror").css("display", "block");
                            toastr.error("Some error occured, please try again.");
                        }
                        else {
                            $(".k-overlay").hide();
                            toastr.success("Records have been updated successfully");
                            $("#NationalgridSite").data("kendoGrid").dataSource.data([]);
                            $("#NationalgridSite").data("kendoGrid").dataSource.read();
                            $("#InitiateBulkChanges").css("display", "inline");
                            $("#CancelBulkChanges").css("display", "none");
                            $("#SaveBulkChanges").css("display", "none");
                            $("#gridBulkChange").css("display", "none");
                        }
                    }, {
                        model: model

                    }, true);

                    //populateSiteGrid();
                },
                function () {
                    maptype.focus();
                }
            );
        }
    });
    //Food Program Section Start

    function populateSiteGrid() {

        Utility.Loading();
        var gridVariable = $("#NationalgridSite");
        gridVariable.html("");
        if (user.SectorNumber == "20") {
            gridVariable.kendoGrid({
                excel: {
                    fileName: "Site.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: true,
                pageable: {
                    pageSize: 150,
                    messages: {
                        display: "{0}-{1} of {2} records"
                    }
                },
                groupable: false,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    //{
                    //    field: "SectorName", title: "Sector Name", width: "60px", attributes: {
                    //        style: "text-align: left; font-weight:normal"
                    //    }
                    //},
                    {
                        field: "SiteCode", title: "Site Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "SiteAlias", title: "Site Alias", width: "100px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "City", title: "City", width: "70px", attributes: {

                    //        style: "text-align: left; font-weight:normal"
                    //    },
                    //},
                    {
                        field: "RegionDesc", title: "Region", width: "55px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "CPUName", title: "CPU Name", width: "90px", attributes: {

                    //        style: "text-align: left; font-weight:normal"
                    //    },
                    //},
                    {
                        field: "MapType", title: "MAP Type", width: "60px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                    },
                    //{
                    //    field: "Name", title: "Site Type", width: "50px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //},
                    //{
                    //    field: "Retail", title: "Retails", width: "50px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //},
                    //{
                    //    field: "IsActive",
                    //    title: "Status", width: "50px",
                    //    attributes: {
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    template: '<label class= "switch"><input type="checkbox" disabled class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                    //}

                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetSiteDataList, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    result = result.filter(m => m.SectorCode == user.SectorNumber);
                                    result.forEach(function (item) {
                                        if (item.SiteType != null) {
                                            for (x of sitetypedata)
                                                if (x.value == item.SiteType)
                                                    item.SiteNameType = x.text;
                                        }
                                        else {
                                            item.SiteNameType = null;
                                        }
                                        return item;
                                    });

                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , false);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                SectorName: { type: "string" },
                                SiteCode: { type: "string" },
                                SiteAlias: { type: "string" },
                                City: { type: "string" },
                                RegionDesc: { type: "string" },
                                CPUName: { type: "string" },
                                MapType: { type: "string" },
                                Name: { type: "string" },
                                Retail: { type: "string" },
                                IsActive: { editable: false }
                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },

                dataBound: function (e) {
                    var items = e.sender.items();
                    // 
                    //items.each(function (e) {
                    //    if (user.UserRoleId == 1) {
                    //        $(this).find('.k-grid-Edit').text("Edit");
                    //        $(this).find('.chkbox').removeAttr('disabled');

                    //    } else {
                    //        $(this).find('.k-grid-Edit').text("View");
                    //        $(this).find('.chkbox').attr('disabled', 'disabled');
                    //    }
                    //});

                    $(".chkbox").on("change", function () {
                        // 
                        var gridObj = $("#NationalgridSite").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        var trEdit = $(this).closest("tr");
                        var th = this;
                        datamodel = tr;
                        datamodel.IsActive = $(this)[0].checked;
                        var active = "";
                        if (datamodel.IsActive) {
                            active = "Active";
                        } else {
                            active = "Inactive";
                        }

                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.SiteName + "</b> Site " + active + "?", "Site Update Confirmation", "Yes", "No", function () {
                            $("#error").css("display", "none");
                            HttpClient.MakeRequest(CookBookMasters.ChangeStatus, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again");
                                }
                                else {
                                    toastr.success("Site updated successfully");

                                    if ($(th)[0].checked) {
                                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                    } else {
                                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                    }
                                }
                            }, {
                                model: datamodel
                            }, false);
                        }, function () {
                            $(th)[0].checked = !datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        });
                        return true;
                    });
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
        }
        else {
            gridVariable.kendoGrid({
                excel: {
                    fileName: "Site.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: true,
                pageable: {
                    pageSize: 150,
                    messages: {
                        display: "{0}-{1} of {2} records"
                    }
                },
                groupable: false,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    //{
                    //    field: "SectorName", title: "Sector Name", width: "60px", attributes: {
                    //        style: "text-align: left; font-weight:normal"
                    //    }
                    //},
                    {
                        field: "SiteCode", title: "Site Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "SiteAlias", title: "Site Alias", width: "100px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "City", title: "City", width: "70px", attributes: {

                    //        style: "text-align: left; font-weight:normal"
                    //    },
                    //},
                    {
                        field: "RegionDesc", title: "Region", width: "55px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    {
                        field: "CPUName", title: "CPU Name", width: "90px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    {
                        field: "MapType", title: "MAP Type", width: "60px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                    },
                    {
                        field: "Name", title: "Site Type", width: "50px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                    },
                    {
                        field: "Retail", title: "Retails", width: "50px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                    },
                    //{
                    //    field: "IsActive",
                    //    title: "Status", width: "50px",
                    //    attributes: {
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    template: '<label class= "switch"><input type="checkbox" disabled class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                    //}

                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetSiteDataList, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    result = result.filter(m => m.SectorCode == user.SectorNumber);
                                    result.forEach(function (item) {
                                        if (item.SiteType != null) {
                                            for (x of sitetypedata)
                                                if (x.value == item.SiteType)
                                                    item.SiteNameType = x.text;
                                        }
                                        else {
                                            item.SiteNameType = null;
                                        }
                                        return item;
                                    });

                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , false);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                SectorName: { type: "string" },
                                SiteCode: { type: "string" },
                                SiteAlias: { type: "string" },
                                City: { type: "string" },
                                RegionDesc: { type: "string" },
                                CPUName: { type: "string" },
                                MapType: { type: "string" },
                                Name: { type: "string" },
                                Retail: { type: "string" },
                                IsActive: { editable: false }
                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },

                dataBound: function (e) {
                    var items = e.sender.items();
                    // 
                    //items.each(function (e) {
                    //    if (user.UserRoleId == 1) {
                    //        $(this).find('.k-grid-Edit').text("Edit");
                    //        $(this).find('.chkbox').removeAttr('disabled');

                    //    } else {
                    //        $(this).find('.k-grid-Edit').text("View");
                    //        $(this).find('.chkbox').attr('disabled', 'disabled');
                    //    }
                    //});

                    $(".chkbox").on("change", function () {
                        // 
                        var gridObj = $("#NationalgridSite").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        var trEdit = $(this).closest("tr");
                        var th = this;
                        datamodel = tr;
                        datamodel.IsActive = $(this)[0].checked;
                        var active = "";
                        if (datamodel.IsActive) {
                            active = "Active";
                        } else {
                            active = "Inactive";
                        }

                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.SiteName + "</b> Site " + active + "?", "Site Update Confirmation", "Yes", "No", function () {
                            $("#error").css("display", "none");
                            HttpClient.MakeRequest(CookBookMasters.ChangeStatus, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again");
                                }
                                else {
                                    toastr.success("Site updated successfully");

                                    if ($(th)[0].checked) {
                                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                    } else {
                                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                    }
                                }
                            }, {
                                model: datamodel
                            }, false);
                        }, function () {
                            $(th)[0].checked = !datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        });
                        return true;
                    });
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
        }
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function populateBulkChangeControls() {

        Utility.Loading();
        var gridVariable = $("#gridBulkChange");
        gridVariable.html("");
        gridVariable.kendoGrid({
            selectable: "cell",
            sortable: false,
            filterable: false,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "", title: "", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "", title: "", width: "125px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "80px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "40px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "CPUName", title: "CPU Name", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "MapType", title: "MAP Type", width: "60px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                },
                {
                    field: "SiteType", title: "Site Type", width: "60px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                },
                {
                    field: "Retail", title: "Retails", width: "60px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                },
                {
                    field: "", title: "", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                }
            ],
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
            },
            change: function (e) {
            },
        });
    }

    $("#btnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#OtherEditwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });


    //$(".switch").on("click", function () {
    //    // 
    //    $("#success").css("display", "none");
    //    $("#error").css("display", "none");
    //    var gridObj = $("#NationalgridSite").data("kendoGrid");
    //    var tr = gridObj.dataItem($(this).closest("tr"));
    //    datamodel = tr;
    //    //sitename = tr.SiteName + " (" + tr.SiteCode + ")";
    //    //var dialog = $("#OtherEditwindowEdit").data("kendoWindow");
    //    //$("#OtherEditwindowEdit").kendoWindow({
    //    //    animation: false
    //    //});
    //    //$(".k-overlay").css("display", "block");
    //    //$(".k-overlay").css("opacity", "0.5");
    //    //dialog.open();
    //    //dialog.center();

    //    //dialog.title("Site Details - " + sitename);
    //    return true;
    //});

    function checkSiteExists() {
        var selectedSiteAlias = $("#sitealias").val();
        var siteCode = $("#sitecode").text();
        var searchNameFound = $("#NationalgridSite").data("kendoGrid").dataSource.data().some(
            function (dataItem) {
                return dataItem.SiteAlias == selectedSiteAlias && dataItem.SiteCode != siteCode;
            });
        return searchNameFound;
    }

    $("#btnSubmit").click(function () {
        if (checkSiteExists()) {
            toastr.error("Site Alias already exists.");
            return false;
        }
        if ($("#sitealias").val().trim() === "") {
            toastr.error("Please provide valid input(s)");
            $("#sitealias").focus();
        }
        if (isSubSectorApplicable && user.SectorNumber == 10 && $("#inputsubsector").val().trim() === "0") {
            toastr.error("Please provide valid input(s)");
            $("#inputsubsector").focus();
        }
        else {
            var model = datamodel;
            if ($("#inputIsCPU")[0].checked == true) {
                model.IsCPU = true;
                model.IsDK = false;
                $("#inputsitetype").data('kendoDropDownList').select(1);
                $("#inputsitetype").data('kendoDropDownList').enable(false);
                $("#inputcpu").data('kendoDropDownList').select(0);
                $("#inputcpu").data('kendoDropDownList').enable(false);
                $("#inputdk").data('kendoDropDownList').select(0);
                $("#inputdk").data('kendoDropDownList').enable(false);
            }



            model.SiteAlias = $("#sitealias").val();
            model.CPUCode = $("#inputcpu").val();
            model.DKCode = $("#inputdk").val();
            model.MapType = $("#inputmaptype").val();
            model.SiteType = $("#inputsitetype").val();
            model.Retail = $("#inputretail").val();
            model.SubSectorCode = $("#inputsubsector").val();
            model.CityName = datamodel.City;
            model.CreatedDate = kendo.parseDate(datamodel.CreatedDate);
            if (user.SectorNumber == 20) {
                if ($("#inputIsCustom")[0].checked == true) {
                    model.IsCustomBillingEndDate = true;
                    model.BillingEndDate = $("#inputbillingenddate").val();
                }
                else {
                    model.IsCustomBillingEndDate = false;
                }
            }

            if (model.SiteType == "offsite" || model.SiteType == "Offsite") {
                if (model.CPUCode == "" || model.CPUCode == null || model.CPUCode == "Select" || model.CPUCode == "None") {
                    toastr.error("Kindly Select Catered By CPU");
                    return;
                }

            }
            if ($("#inputIsSite")[0].checked == true) {
                model.IsCPU = false;
                model.IsDK = false;
            }

            if ($("#inputIsDK")[0].checked == true) {
                model.IsCPU = false;
                model.IsDK = true;
                if (model.CPUCode == "" || model.CPUCode == null || model.CPUCode == "Select") {
                    toastr.error("Kindly Select Catered By CPU");
                    return;
                }

            }
            if (!sanitizeAndSend(model)) {
                return;
            }
            $("#btnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveSiteData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $("#sitealias").focus();
                    $('#btnSubmit').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnSubmit').removeAttr("disabled");
                        toastr.error("Some error occured, please try again.");
                        $("#sitealias").focus();
                    }
                    else {
                        $(".k-overlay").hide();
                        var orderWindow = $("#OtherEditwindowEdit").data("kendoWindow");
                        orderWindow.close();
                        $('#btnSubmit').removeAttr("disabled");
                        toastr.success("Site updated successfully");
                        $("#NationalgridSite").data("kendoGrid").dataSource.data([]);
                        $("#NationalgridSite").data("kendoGrid").dataSource.read();
                    }
                }
            }, {
                model: model

            }, false);


            //dayPartArray = [];

            //var multiselect = $("#OtherSiteinputdaypart").data("kendoMultiSelect");
            //var dataItems = multiselect.dataItems();
            //dayPartArray = [];
            //for (itm of dataItems) {
            //    var dayPartModel = {
            //        "ID": 0,
            //        "SiteCode": SCode,
            //        "DayPartCode": itm.code,
            //        "IsActive": 1
            //    }
            //    dayPartArray.push(dayPartModel);
            //}
            //if (dayPartArray.length > 0) {
            //    SaveDayPartMapping();
            //} else if (dayPartArray.length == 0) {

            //    var dayPartModel = {
            //        "ID": 0,
            //        "SiteCode": SCode,
            //        "DayPartCode": null,
            //        "IsActive": 1
            //    }
            //    dayPartArray.push(dayPartModel);
            //    SaveDayPartMapping();


            //}
        }
    });
});