﻿var datamodel;
var completedata = [];
$(document).ready(function () {
    $(".k-window").hide();
    $(".k-overlay").hide();
    Utility.Loading();
    setTimeout(function () {
        onLoad();
        Utility.UnLoading();
    }, 2000);

    $("#PlanningTagwindowEdit").kendoWindow({
        modal: true,
        width: "420px",
        height: "157px",
        title: "Planning Tag Details  ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#PlanningTagwindowEdit1").kendoWindow({
        modal: true,
        width: "420px",
        height: "157px",
        title: "Planning Tag Details  ",
        actions: ["Close"],
        visible: false,
        animation: false
    });
});
$("#PlanningTagddnew").on("click", function () {
    //
    // var jq = jQuery.noConflict();
    // alert("calick");
    var model;

    var dialog = $("#PlanningTagwindowEdit").data("kendoWindow");

    //dialog.open();
    dialog.open().element.closest(".k-window").css({
        top: 167,
        left: 558

    });
    // dialog.center();

    datamodel = model;

    dialog.title("New Planning Tag Creation");

    $("#PlanningTagname").removeClass("is-invalid");
    $('#PlanningTagsubmit').removeAttr("disabled");
    $("#PlanningTagid").val("");
    $("#PlanningTagname").val("");
    $("#PlanningTagcode").val("");
    $("#PlanningTagDisplayCode").val("");
    $("#PlanningTagDescription").val("");
    $("#PlanningTagname").focus();
    DishCategoryCodeIntial = null;

});
function onLoad() {
    populateCafeGrid();
    $("#btnExport").click(function (e) {
        var grid = $("#gridMOG").data("kendoGrid");
        grid.saveAsExcel();
    });
    $('#myInput').on('input', function (e) {
        var grid = $('#gridMOG').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "TagCode" || x.field == "TagName" || x.field == "TagDisplayCode" || x.field == "TagDescription" || x.field == "IsActive") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
}


$("#PlanningTagcancel").on("click", function () {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $(".k-overlay").hide();
            var orderWindow = $("#PlanningTagwindowEdit").data("kendoWindow");
            orderWindow.close();
        },
        function () {
        }
    );
});

$("#PlanningTagcancel1").on("click", function () {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $(".k-overlay").hide();
            var orderWindow = $("#PlanningTagwindowEdit1").data("kendoWindow");
            orderWindow.close();
        },
        function () {
        }
    );
});

$("#PlanningTagsubmit").click(function () {
    var name = $("#PlanningTagname").val();
    if ($("#PlanningTagname").val() === "") {
        toastr.error("Please provide Planning Tag Name");
        $("#PlanningTagname").focus();
        return;
    }
    if ($("#PlanningTagDisplayCode").val() === "") {
        toastr.error("Please provide Planning Tag Display Code");
        $("#PlanningTagDisplayCode").focus();
        return;
    }
    for (item of completedata) {
        if (item.TagName.toLowerCase().trim() == name.toLowerCase().trim()) {
            toastr.error("Please check Duplicate Tag Name");
            $("#PlanningTagname").focus();
            return;
        }
    }


    var model = datamodel;

    model = {

        "TagName": $("#PlanningTagname").val(),
        "TagCode": $("#PlanningTagcode").val(),
        "TagDisplayCode": $("#PlanningTagDisplayCode").val(),
        "TagDescription": $("#PlanningTagDescription").val(),
    }

    $("#PlanningTagsubmit").attr('disabled', 'disabled');
    if (!sanitizeAndSend(model)) {
        return;
    }
    HttpClient.MakeRequest(CookBookMasters.SavePlanningTag, function (result) {
        if (result.xsssuccess !== undefined && !result.xsssuccess) {
            toastr.error(result.message);
            $('#PlanningTagsubmit').removeAttr("disabled");
            Utility.UnLoading();
        }
        else {
            if (result == false) {
                $('#PlanningTagsubmit').removeAttr("disabled");
                toastr.success("There was some error, the record cannot be saved");

            }
            else {
                $(".k-overlay").hide();

                toastr.success("New Planning Tag record added successfully");
                var orderWindow = $("#PlanningTagwindowEdit").data("kendoWindow");
                orderWindow.close();
                $("#gridMOG").data("kendoGrid").dataSource.data([]);
                $("#gridMOG").data("kendoGrid").dataSource.read();
            }
        }
    }, {
        model: model

    }, true);

    Utility.UnLoading();
    //populateCafeGrid();
});

$("#PlanningTagsubmit1").click(function () {
    var TagCodeIntial = $("#PlanningTagccode1").val();
    var name = $("#PlanningTagcname1").val();
    if ($("#PlanningTagcname1").val() === "") {
        toastr.error("Please provide Planning Tag Name");
        $("#PlanningTagcname1").focus();
        return;
    }
    if ($("#PlanningTagDisplayCode1").val() === "") {
        toastr.error("Please provide Planning Tag Display Code");
        $("#PlanningTagDisplayCode1").focus();
        return;
    }
    debugger;
    for (item of completedata) {
        if (item.TagName.toLowerCase().trim() == name.toLowerCase().trim() && item.TagCode != TagCodeIntial) {
            toastr.error("Please check Duplicate  Tag Name is entered on editing");
            $("#PlanningTagcname1").focus();
            return;
        }
    }

    var model = datamodel;

    model = {

        "TagName": $("#PlanningTagcname1").val(),
        "TagCode": $("#PlanningTagccode1").val(),
        "TagDisplayCode": $("#PlanningTagDisplayCode1").val(),
        "TagDescription": $("#PlanningTagDescription1").val(),
        "IsActive": model.IsActive,

    }
    if (!sanitizeAndSend(model)) {
        return;
    }

    HttpClient.MakeSyncRequest(CookBookMasters.SavePlanningTag, function (result) {
        if (result.xsssuccess !== undefined && !result.xsssuccess) {
            toastr.error(result.message);
            $('#PlanningTagsubmit1').removeAttr("disabled");
            Utility.UnLoading();
        }
        else {
            if (result == false) {
                $('#PlanningTagsubmit1').removeAttr("disabled");
                toastr.success("There was some error, the record cannot be saved");

            }
            else {
                $(".k-overlay").hide();

                toastr.success("Planning Tag record update successfully");
                var orderWindow = $("#PlanningTagwindowEdit1").data("kendoWindow");
                orderWindow.close();
                $("#gridMOG").data("kendoGrid").dataSource.data([]);
                $("#gridMOG").data("kendoGrid").dataSource.read();
            }
        }
    }, {
        model: model

    }, true);

    Utility.UnLoading();
    //populateCafeGrid();
});
function populateCafeGrid() {

    Utility.Loading();
    var gridVariable = $("#gridMOG").height(380);
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "PlanningTagMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Total: {2} records"
            }
        },
        //pageable: true,
        groupable: false,
        //reorderable: true,
        //scrollable: true,
        height: "490px",
        columns: [
            {
                field: "TagCode", title: "Tag Code", width: "40px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "TagName", title: "Tag Name", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "TagDisplayCode", title: "Display Code", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "TagDescription", title: "Tag Description", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            //{
            //    field: "IsActive", title: "Active", width: "75px", attributes: {

            //        style: "text-align: center; font-weight:normal"
            //    },
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive == "true" ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
            //},
            {
                field: "Edit", title: "Action", width: "50px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                command: [
                    {
                        name: 'Edit',
                        click: function (e) {

                            var dialog = $("#PlanningTagwindowEdit1").data("kendoWindow");
                            var tr = $(e.target).closest("tr");
                            var item = this.dataItem(tr);          // get the date of this row
                            var model = {

                                "TagCode": item.TagCode,
                                "TagName": item.TagName,
                                "TagDescription": item.TagDescription,
                                "TagDisplayCode": item.TagDisplayCode,
                                "IsActive": item.IsActive
                            };
                            // DishCategoryCodeIntial = item.DishCategoryCode;
                            dialog.open();
                            dialog.center();
                            datamodel = model;
                            $("#PlanningTagccode1").val(model.TagCode);

                            $("#PlanningTagcname1").val(model.TagName);
                            $("#PlanningTagDisplayCode1").val(model.TagDisplayCode);
                            $("#PlanningTagDescription1").val(model.TagDescription);

                            $("#PlanningTagcname1").focus();
                            return true;
                        }
                    }
                ],
            }

        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetPlanningTagMasterList, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                            completedata = result;
                        }
                        else {
                            options.success("");
                        }
                    }, null
                        //{
                        //filter: mdl
                        //}
                        , false);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        TagCode: { type: "string" },
                        TagDisplayCode: { type: "string" },
                        TagName: { type: "string" },
                        TagDescription: { type: "string" },
                        IsActive: { type: "string" }

                    }
                }
            },
            //pageSize: 15,
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var items = e.sender.items();
            var grid = this;

            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);

                if (model.IsActive == 'false') {
                    $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                }
            });
            items.each(function (e) {
                //if (user.UserRoleId == 1) {
                //    $(this).find('.k-grid-Edit').text("Edit");
                //    $(this).find('.chkbox').removeAttr('disabled');

                //} else {
                //    $(this).find('.k-grid-Edit').text("View");
                //    $(this).find('.chkbox').attr('disabled', 'disabled');
                //}
            });

            $(".chkbox").on("change", function () {

                var gridObj = $("#gridMOG").data("kendoGrid");
                var tr = gridObj.dataItem($(this).closest("tr"));
                var trEdit = $(this).closest("tr");
                var th = this;
                datamodel = tr;
                datamodel.IsActive = $(this)[0].checked;
                var active = "";
                if (datamodel.IsActive) {
                    active = "Active";
                } else {
                    active = "Inactive";
                }

                var model = datamodel;
                model = {

                    "TagName": datamodel.Name,
                    "TagCode": datamodel.TagCode,
                    "TagDisplayCode": datamodel.TagDisplayCode,
                    "TagDescription": datamodel.TagDescription,
                    "ModuleName": "PlanningTag",
                    "IsActive": model.IsActive,
                }

                Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Planning Tag " + active + "?", " Planning Tag Update Confirmation", "Yes", "No", function () {
                    HttpClient.MakeRequest(CookBookMasters.SavePlanningTag, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again");
                        }
                        else {
                            toastr.success("Planning Tag updated successfully");
                            $(th)[0].checked = datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        }
                    }, {
                        model: datamodel
                    }, false);
                }, function () {

                    $(th)[0].checked = !datamodel.IsActive;
                    if ($(th)[0].checked) {
                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                    } else {
                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
                return true;
            });

        },
        change: function (e) {
        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers"
                    || col == "uid" || col == "CreatedBy" || col == "CreatedOn" || col == "ModifiedBy" || col == "ModifiedOn") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
    //$(".k-label")[0].innerHTML.replace("items", "records");
}  