﻿
var capringMasterList;
var userMasterList;
var table;
function cmList() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetCapringMasterDataList, function (result) {
        Utility.UnLoading();

        if (result != null) {
            capringMasterList = result;
        }
        else {

        }
    }, null

        , false);
}

function umList() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetUserMasterDataList, function (result) {
        Utility.UnLoading();

        if (result != null) {
            userMasterList = result;
         
        }
        else {

        }
    }, null

        , false);
}

function titleCase(str) {
    return str.toLowerCase().split(' ').map(function (word) {
        return word.replace(word[0], word[0].toUpperCase());
    }).join(' ');
}
function createUserCapringMatrix() {
    cmList();
    umList();
    ulen = userMasterList.length;
    clen = capringMasterList.length;
    table = $('<table></table>').addClass('foo');
    row = $('<tr></tr>');
    var rowData = $('<th></th>').addClass('bar').text('*');
    row.append(rowData);
    for (var j = 0; j < clen; j++) {
         rowData = $('<th></th>').addClass('bar').text(capringMasterList[j].Name);
        row.append(rowData);
    }
    table.append(row);
    for (var i = 0; i < ulen; i++) {
        row = $('<tr></tr>');
        var uname = titleCase(userMasterList[i].UserName);
        var rowData = $('<td></td>').addClass('bar').text(uname);
        row.append(rowData);
        for (var j = 0; j < clen; j++) {
            rowData = $('<td></td>').addClass('bar').text('Chk1');
            row.append(rowData);
        }
        table.append(row);
    }
    $("#UCMaster").append(table);
}

$(document).ready(function () {
    createUserCapringMatrix();
});

 