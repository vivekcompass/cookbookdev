﻿var user;
var datamodel;
var NutritionElementType = [];
var NutritionMasterdata = [];
var uomdata = [];
var NutritionElementCodeIntial = null;

function populateNutritionElementTypedrpodown() {
    $("#dpnutritionelementtype").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "Code",
        dataSource: NutritionElementType,
        index: 0
    });
}

function populateUOMDropdown() {
    $("#dpnutritionelementuom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}

$(document).ready(function () {
    $(".k-window").hide();
    $(".k-overlay").hide(); 
    $("#NutritionMasterWindowEdit").kendoWindow({
        modal: true,
        width: "435px",

        title: "Nutrient  Master Details  ",
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $('#myInput').on('input', function (e) {
        var grid = $('#gridNutritionMaster').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "NutritionElementCode" || x.field == "Name" || x.field == "NutritionElementUOMName" || x.field == "NutritionElementTypeName"
                  ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    HttpClient.MakeSyncRequest(CookBookMasters.GetNutritionElementTypeList, function (result) {
        dataSource = result;
        NutritionElementType.push({ "value": null, "text": "Select", "Code": null, "IsActive": 1 });
        for (var i = 0; i < dataSource.length; i++) {
            NutritionElementType.push({ "value": dataSource[i].Code, "text": dataSource[i].Name, "Code": dataSource[i].Code, "IsActive": dataSource[i].IsActive });
        }
        populateNutritionElementTypedrpodown();
      
       populateNutritionMasterGrid();
    }, null, false);

    HttpClient.MakeRequest(CookBookMasters.GetNutrientUOMDataList, function (data) {
        var dataSource = data;
        uomdata = [];
        uomdata.push({ "value": "Select", "text": "Select" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].IsActive) {
                uomdata.push({ "value": dataSource[i].Code, "text": dataSource[i].Name });
            }
        }
        populateUOMDropdown();

    }, {
        "ModuleName": "NUTRIENTUOM"
    }, false);

});

function populateNutritionMasterGrid() {

    Utility.Loading();
    var gridVariable = $("#gridNutritionMaster");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Total: {2} records"
            }
        },
        columns: [
            {
                field: "NutritionElementCode", title: "Code", width: "70px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Nutrient", width: "180px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "NutritionElementUOMName", title: "Nutrient UOM", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "NutritionElementTypeName", title: "Nutrient Type", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DisplayOrder", title: "Display Order", width: "70px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "IsDisplay", title: "Display On UI", width: "70px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            //{
            //    field: "IsActive", title: "Active", width: "40px", attributes: {

            //        style: "text-align: center; font-weight:normal"
            //    },
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
            //},
            {
                field: "Edit", title: "Action", width: "40px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                command: [
                    {
                        name: 'Edit',
                        click: function (e) {
                           var tr = $(e.target).closest("tr");    // get the current table row (tr)
                            var item = this.dataItem(tr);
                            if (!item.IsActive) {
                                return;
                            }// get the date of this row
                            var model = {
                                "ID": item.ID,
                                "Name": item.Name,
                                "IsActive": item.IsActive,
                                "NutritionElementCode": item.NutritionElementCode,
                                "NutritionElementUOMCod": null,
                                "NutritionElementTypeCode": null,
                                "CreatedBy": item.CreatedBy,
                                "CreatedOn": kendo.parseDate(item.CreatedOn),
                                "DisplayOrder":item.DisplayOrder,
                                "IsDisplay": item.IsDisplay,

                            };
                            NutritionElementCodeIntial = item.NutritionElementCode;
                            var dialog = $("#NutritionMasterWindowEdit").data("kendoWindow");
                               dialog.title("Nutrient  Details : ");
                            model.NutritionElementUOMCode = item.NutritionElementUOMCode;
                            model.NutritionElementTypeCode = item.NutritionElementTypeCode;
                           
                           
                            dialog.open();
                            dialog.center();

                            datamodel = model;

                            $("#dpid").val(model.ID);
                            $("#dpcode").val(model.NutritionElementCode);
                            $("#dpnutritionelement").val(model.Name);
                            $("#inputdisplayorder").val(model.DisplayOrder);
                            if (model.IsDisplay) {
                                $("#isdisplay").attr("checked", "checked");
                            }
                            else {
                                $("#isdisplay").removeAttr("checked");
                            }
                            $("#dpnutritionelementuom").data('kendoDropDownList').value(model.NutritionElementUOMCode);
                            $("#dpnutritionelementtype").data('kendoDropDownList').value(model.NutritionElementTypeCode);
                            $("#dpnutritionelement").focus();


                            return true;
                        }
                    }
                ],
            }
        ],
        dataSource: {
            sort: { field: "DisplayOrder", dir: "asc" },
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetNutritionMasterDataList, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            //Result modifiy
                           // result = result.sort((a, b) => a.DisplayOrder - b.DisplayOrder)
                            NutritionMasterdata = result;
                          
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }
                    }, null
                        //{
                        //filter: mdl
                        //}
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Name: { type: "string" },
                        NutritionElementCode: { type: "string" },
                        NutritionElementUOMName: { type: "string" },
                        NutritionElementTypeName: { type: "string" },
                        IsActive: { type: "Boolean" },
                        DisplayOrder: { type: "int" },
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var items = e.sender.items();
            var grid = this;
            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);

                if (!model.IsActive) {
                    $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                }
            });
            
            $(".chkbox").on("change", function () {
                var gridObj = $("#gridNutritionMaster").data("kendoGrid");
                var tr = gridObj.dataItem($(this).closest("tr"));
                var trEdit = $(this).closest("tr");
                var th = this;
                datamodel = tr;
                datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);

                datamodel.IsActive = $(this)[0].checked;
                var active = "";
                var msg = "";
                if (datamodel.IsActive) {
                    active = "Active";
                } else {
                    active = "Inactive";
                }
                     msg = "NutritionMaster ";

                

                Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b>" + msg + " " + active + "?", " " + msg + " Update Confirmation", "Yes", "No", function () {
                    HttpClient.MakeRequest(CookBookMasters.SaveNutritionMaster, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again");
                        }
                        else {
                            toastr.success(msg + " updated successfully");
                            $(th)[0].checked = datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        }
                    }, {
                        model: datamodel
                    }, false);
                }, function () {
                    datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);

                    $(th)[0].checked = !datamodel.IsActive;
                    if ($(th)[0].checked) {
                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                    } else {
                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
                return true;
            });

        },
        change: function (e) {
        },
    })
   // gridVariable.data("kendoGrid").dataSource.sort({ field: "Dispal", dir: "asc" });
  
}
function dpvalidate() {
    var valid = true;

    if ($("#dpnutritionelement").val() === "") {
        toastr.error("Please provide input");
        $("#dpnutritionelement").addClass("is-invalid");
        valid = false;
        $("#dpnutritionelement").focus();
    }
    if (($.trim($("#dpnutritionelement").val())).length > 59) {
        toastr.error("NutritionMaster Description can't accepts upto 60 charactre");

        $("#dpnutritionelement").addClass("is-invalid");
        valid = false;
        $("#dpnutritionelement").focus();
    }
    return valid;
}
$(document).ready(function (){
    $("#dpaddnew").on("click", function () {
        var model;
        var dialog = $("#NutritionMasterWindowEdit").data("kendoWindow");

        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });

        datamodel = model;

        dialog.title("New Nutrient  Details Creation");




        $("#dpnutritionelement").removeClass("is-invalid");
        $('#secredpsubmit').removeAttr("disabled");
        $("#dpid").val("");
        $("#dpnutritionelement").val("");
        $("#dpcode").val("");
        $("#dpnutritionelement").focus();
        $("#inputdisplayorder").val("");
        $("#isdisplay").prop('checked', false);
        $("#dpnutritionelementtype").data('kendoDropDownList').value(null);
        $("#dpnutritionelementuom").data('kendoDropDownList').value("Select");

        NutritionElementCodeIntial = null;
    });

    $("#secredpcancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#NutritionMasterWindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });
    $("#secredpsubmit").click(function () {
        var msg = "";
       // if (user.SectorNumber == "20") {
            msg = "Nutritio nMaster";
       
        var namedp = $("#dpnutritionelement").val().toLowerCase().trim();
        if (NutritionElementCodeIntial == null) { //Duplicate Check
            for (item of NutritionMasterdata) {
                if (item.Name.toLowerCase().trim() == namedp) {
                    toastr.error("Kindly check Duplicate " + msg + " Description");
                    $("#dpnutritionelement").focus();
                    return;
                }
            }
        }
        else {
            for (item of NutritionMasterdata) {
                if (item.Name?.toLowerCase().trim() == namedp && item.NutritionElementCode != NutritionElementCodeIntial) {
                    toastr.error("Kindly check Duplicate  " + msg + " Description is entered on editing");
                    $("#dpnutritionelement").focus();
                    return;
                }
            }
        }

        var displayorder = $("#inputdisplayorder").val();
        if (NutritionElementCodeIntial == null) { //Duplicate Check
            for (item of NutritionMasterdata) {
                if (item.DisplayOrder == displayorder) {
                    toastr.error("Kindly check Duplicate " + msg + " Display Order");
                    $("#inputdisplayorder").focus();
                    return;
                }
            }
        }
        else {
            for (item of NutritionMasterdata) {
                if (item.DisplayOrder == displayorder && item.NutritionElementCode != NutritionElementCodeIntial) {
                    toastr.error("Kindly check Duplicate  " + msg + " Display Order is entered on editing");
                    $("#inputdisplayorder").focus();
                    return;
                }
            }
        }
       
        if ($("#dpnutritionelement").val() === "" || $("#dpnutritionelement").val() == null) {
            toastr.error("Please provide " + msg + " Name");
            $("#dpnutritionelement").focus();
            return;
        }
            if ($("#dpnutritionelementtype").val() == "" || $("#dpnutritionelementtype").val() == null) {
                toastr.error("Please provide " + msg + "Nutrient  Type");
                $("#dpnutritionelementtype").focus();
                return;
        }

        //if ($("#dpnutritionelementuom").val() == "" || $("#dpnutritionelementuom").val() == null) {
        //    toastr.error("Please provide " + msg + "Nutrient  UOM");
        //    $("#dpnutritionelementuom").focus();
        //    return;
        //}
        if ($("#inputdisplayorder").val() == "" || $("#inputdisplayorder").val() == null) {
            toastr.error("Please provide " + msg + "Display Order");
            $("#inputdisplayorder").focus();
            return;
        }

        if (dpvalidate() === true) {
            $("#dpnutritionelement").removeClass("is-invalid");

            var model = {
                "ID": $("#dpid").val(),
                "Name": $("#dpnutritionelement").val(),
                "NutritionElementCode": NutritionElementCodeIntial,
                "NutritionElementTypeCode": $("#dpnutritionelementtype").val(),
                "NutritionElementUOMCode": $("#dpnutritionelementuom").val(),
                "DisplayOrder": $("#inputdisplayorder").val(),
                "IsDisplay": $('#isdisplay').is(':checked')
            }
            if (NutritionElementCodeIntial == null) {
                model.IsActive = 1;
            } else {
                model.IsActive = 1;
                model.CreatedBy = datamodel.CreatedBy;
                model.CreatedOn = datamodel.CreatedOn;

            }
            model.NutritionElementTypeCode = $("#dpnutritionelementtype").data('kendoDropDownList').value()

            $("#secredpsubmit").attr('disabled', 'disabled');
            if (!sanitizeAndSend(model)) {
                return;
            }
            HttpClient.MakeRequest(CookBookMasters.SaveNutritionMaster, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#secredpsubmit').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#secredpsubmit').removeAttr("disabled");
                        toastr.success("There was some error, the record cannot be saved");

                    }
                    else {
                        $(".k-overlay").hide();
                        $('#secredpsubmit').removeAttr("disabled");

                        if (model.ID > 0)
                            toastr.success(msg + " record updated successfully");
                        else
                            toastr.success("New" + msg + " record added successfully");
                        $(".k-overlay").hide();
                        var orderWindow = $("#NutritionMasterWindowEdit").data("kendoWindow");
                        orderWindow.close();
                        $("#gridNutritionMaster").data("kendoGrid").dataSource.data([]);
                        $("#gridNutritionMaster").data("kendoGrid").dataSource.read();
                        $("#gridNutritionMaster").data("kendoGrid").dataSource.sort({ field: "DisplayOrder", dir: "asc" });

                    }

                }
            }, {
                model: model

            }, true);
        }
    });


});
