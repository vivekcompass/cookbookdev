﻿var user;
var status = "";
var varname = "";
var datamodel;
var RecipeName = "";
var uomdata = [];
var moguomdata = [];
var recipecategoryuomdata = [];
var basedata = [];
var dishdata = [];
var baserecipedata = [];
var baserecipedatafiltered = [];
var mogdata = [];
var mogdataList = [];
var mogWiseAPLMasterdataSource = [];
var MOGCode = "MOG-00156";
var MOGName = "Test"
var sitemasterdata = [];
var regionmasterdata = [];
var IsMajor = 0;
var MajorPercentData = 5;
var PrescibedPercentChange = 10;
var PerCostChangePercent = 5;
var x = 'x';
var oldcost = 0;
var isSubSectorApplicable = false;
var subsectorMasterData = [];
var subSectorCodeDD = "";
var Recipe_ID = 0;
var ToTIngredients = 0;
var moguomdata = [];
//Krish 20-07-2022
var copyData = [];
var rCode = "";

//$(function () {
function compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const bandA = a.RegionDesc.toUpperCase();
    const bandB = b.RegionDesc.toUpperCase();

    let comparison = 0;
    if (bandA > bandB) {
        comparison = 1;
    } else if (bandA < bandB) {
        comparison = -1;
    }
    return comparison;
}
function removeDuplicateObjectFromArray(array, key) {
    let check = {};
    let res = [];
    for (let i = 0; i < array.length; i++) {
        if (!check[array[i][key]]) {
            check[array[i][key]] = true;
            res.push(array[i]);
        }
    }
    return res;
}


function populateRegionMasterDropdown() {

    $("#ddlRegionMaster").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "RegionID",
        dataSource: regionmasterdata,
        index: 0,
    });
    if (regionmasterdata.length == 1) {
        Utility.Loading();
        setTimeout(function () {
            var dropdownlist = $("#ddlRegionMaster").data("kendoDropDownList");
            regionCode = dropdownlist.value();
            populateRecipeGrid(regionCode);
            $("#searchspan").show();
            var regionName = dropdownlist.text();

            $("#location").text(" | " + regionName);
            Utility.UnLoading();
        }, 2000);

    }

}

//array type id class  step1 multiple dics ids




$(document).ready(function () {
    $(".k-window").hide();
    $(".k-overlay").hide();
    $('#myInput').on('input', function (e) {
        var grid = $('#gridRecipe').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "RecipeType" || x.field == "RecipeCode" || x.field == "Name" || x.field == "UOMName"
                    || x.field == "IsBase" || x.field == "Status" || x.field == "SubSectorName") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        if (x.field == "Status") {
                            var pendingString = "yes";
                            var savedString = "no";

                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });


    $('#myInputAPL').on('input', function (e) {
        var grid = $('#gridMOGWiseAPL').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ArticleNumber" || x.field == "ArticleDescription" || x.field == "UOM" || x.field == "ArticleType"
                    || x.field == "Hierlevel3" || x.field == "MerchandizeCategoryDesc" || x.field == "MOGName") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    HttpClient.MakeSyncRequest(CookBookMasters.GetRegionMasterListByUserId, function (data) {
        var dataSource = data;
        sitemasterdata = dataSource;
        regionmasterdata = sitemasterdata//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
        // regionmasterdata.sort(compare);
        if (data.length > 1) {
            regionmasterdata.unshift({ "RegionID": 0, "Name": "Select Region" })
            $("#btnGo").css("display", "inline-block");
        }

        else {
            $("#btnGo").css("display", "inline-none");
        }
        populateRegionMasterDropdown();

    }, null, true);


    HttpClient.MakeRequest(CookBookMasters.GetSubSectorMasterList, function (data) {

        subsectorMasterData = data;

        if (data.length > 1) {
            isSubSectorApplicable = true;
            subsectorMasterData.unshift({ "SubSectorCode": '0', "Name": "Select Sub Sector" })
            $("#inputSubSector").css("display", "block");
            $(".googleonly").css("display", "block");
            populateSubSectorMasterDropdown();
        }
        else {
            isSubSectorApplicable = false;
            $(".googleonly").css("display", "none");
        }

    }, { isAllSubSector: false }, false);

    HttpClient.MakeRequest(CookBookMasters.GetApplicationSettingDataList, function (result) {
        Utility.UnLoading();

        if (result != null) {
            applicationSettingData = result;
            for (item of applicationSettingData) {
                if (item.Code == "FLX-00002") {//||item.Name == "Region Major Identification Threshold"
                    MajorPercentData = item.Value;
                    $(".asQuantity").text(MajorPercentData);
                }
                else
                    if (item.Code == "FLX-00005") {//||item.Name == "Recipe Quantity Change Threshold"
                        PrescibedPercentChange = item.Value;
                        $(".asRegionQuantity").text(PrescibedPercentChange);
                    }
                    else if (item.Code == "FLX-00003") {//||item.Name == "Recipe Cost Change Threshold"
                        PerCostChangePercent = item.Value;
                        $(".asRegionCost").text(PerCostChangePercent);
                    }
            }

        }
        else {

        }
    }, null

        , false);
    $("#RecipeReConfgwindowEdit1").kendoWindow({
        modal: true,
        width: "1020px",
        height: "550px",
        title: "Recipe Details Inline- " + RecipeName,
        actions: ["Close"],
        visible: false,
        draggable: false,
        resizable: false,
        animation: false
    });

    $("#RecipeReConfgwindowEdit2").kendoWindow({
        modal: true,
        width: "1020px",
        height: "550px",
        title: "Recipe Details Inline- " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#RecipeReConfgwindowEdit3").kendoWindow({
        modal: true,
        width: "1020px",
        height: "550px",
        title: "Recipe Details Inline- " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#RecipeReConfgwindowEdit4").kendoWindow({
        modal: true,
        width: "1020px",
        height: "550px",
        title: "Recipe Details Inline- " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#RecipeReConfgwindowEdit5").kendoWindow({
        modal: true,
        width: "1020px",
        height: "550px",
        title: "Recipe Details Inline- " + RecipeName,
        actions: ["Close"],
        visible: false,
        animation: false
    });


    $("#windowEditMOGWiseAPL").kendoWindow({
        modal: true,
        width: "900px",
        height: "300px",
        cssClass: 'aplPopup',
        top: '80px !important',
        title: "MOG APL Detials - " + MOGName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    instructionTextEditor('RegRecinputinstruction');
    instructionTextEditor('RegRecinputinstruction0');
    instructionTextEditor('RegRecinputinstruction1');
    instructionTextEditor('RegRecinputinstruction2');
    instructionTextEditor('RegRecinputinstruction3');
    instructionTextEditor('RegRecinputinstruction4');
    instructionTextEditor('RegRecinputinstruction5');

    $("#btnCancel").click(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $("#mainContent").show();
                $("#windowEdit").hide();
                $("#topHeading").text("Regional Recipe Configuration");
            },
            function () {
            }
        );
    });

    $("#btnExport").click(function (e) {
        var grid = $("#gridRecipe").data("kendoGrid");
        grid.saveAsExcel();
    });


    HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {

            user = result;
            if (user.SectorNumber == "20" ){
                $(".rcHide").show();
            }
            else {
                $(".rcHide").hide();
            }
            if (user.SectorNumber == "20") {
                $(".mfgHide").hide();
            }
            else {
                $(".mfgHide").show();
            }
        }, null , true);



    HttpClient.MakeRequest(CookBookMasters.GetRecipeDataList, function (data) {

        var dataSource = data;

        baserecipedata = [];
        baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].CostPerUOM == null) {
                dataSource[i].CostPerUOM = 0;
            }
            if (dataSource[i].CostPerKG == null) {
                dataSource[i].CostPerKG = 0;
            }
            if (dataSource[i].TotalCost == null) {
                dataSource[i].TotalCost = 0;
            }
            if (dataSource[i].Quantity == null) {
                dataSource[i].Quantity = 0;
            }
            if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
                baserecipedata.push({
                    "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                });
            else
                baserecipedata.push({
                    "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].BaseRecipeCode
                });
        }
        baserecipedatafiltered = baserecipedata;
    }, { condition: "Base" }, true);

    HttpClient.MakeRequest(CookBookMasters.GetDishDataList, function (data) {

        var dataSource = data;
        dishdata = [];
        dishdata.push({ "value": "Select Dish", "text": "Select Dish" });
        for (var i = 0; i < dataSource.length; i++) {
            dishdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name });
        }
    }, null, true);

    //HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

    //    var dataSource = data;
    //    uomdata = [];
    //    uomdata.push({ "value": "Select", "text": "Select", "UOMCode": "" });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        uomdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode });
    //    }
    //    populateDishDropdown();
    //}, null , true);

    HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
        var dataSource = data;
        uommodulemappingdata = [];
        uommodulemappingdata.push({ "value": "Select", "text": "Select", "UOMCode": "Select" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].IsActive) {
                uommodulemappingdata.push({ "value": dataSource[i].UOM_ID, "UOM_ID": dataSource[i].UOM_ID, "text": dataSource[i].UOMName, "UOMModuleCode": dataSource[i].UOMModuleCode, "UOMCode": dataSource[i].UOMCode });
            }
        }
        moguomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001' || m.text == "Select");
        uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");
        recipecategoryuomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00006' || m.text == "Select");
        populateDishDropdown();

    }, null, false);

    basedata = [{ "value": "Select", "text": "Select" }, { "value": "No", "text": "Final Recipe" },
    { "value": "Yes", "text": "Base Recipe" }];

    $("#gridBulkChange").css("display", "none");
    populateBulkChangeControls();
    var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
    changeControls.css("background-color", "#fff");
    changeControls.css("border", "none");
    changeControls.eq(2).text("");
    changeControls.eq(2).append("<input id='quantity' style='width:90%; margin-left:3px; font-size:10.5px!important'></div>");
    changeControls.eq(3).text("");
    changeControls.eq(3).append("<div id='uom' style='width:100%; font-size:10.5px!important'></div>");
    changeControls.eq(4).text("");
    changeControls.eq(5).append("<div id='base' style='width:100%; font-size:10.5px!important'></div>");
});

function populateSubSectorMasterDropdown() {
    $("#inputSubSector").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "SubSectorCode",
        dataSource: subsectorMasterData,
        index: 0,
    });
}



$("#InitiateBulkChanges").on("click", function () {
    $("#gridBulkChange").css("display", "block");
    $("#gridBulkChange").children(".k-grid-header").css("border", "none");
    $("#InitiateBulkChanges").css("display", "none");
    $("#CancelBulkChanges").css("display", "inline");
    $("#SaveBulkChanges").css("display", "inline");
    $("#bulkerror").css("display", "none");
    $("#success").css("display", "none");
    populateUOMDropdown_Bulk();
    populateBaseDropdown_Bulk();
});

function populateDishDropdown() {
    $("#inputdish").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: dishdata,
        enable: false,
    });
}

function populateUOMDropdown_Bulk() {
    $("#uom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}

function populateUOMDropdown() {
    $("#RegRecinputuom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}

function populateBaseDropdown_Bulk() {
    $("#base").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}
function populateBaseDropdown() {
    $("#RegRecinputbase").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}

$("#CancelBulkChanges").on("click", function () {
    $("#InitiateBulkChanges").css("display", "inline");
    $("#CancelBulkChanges").css("display", "none");
    $("#SaveBulkChanges").css("display", "none");
    $("#gridBulkChange").css("display", "none");
    $("#bulkerror").css("display", "none");
    $("#success").css("display", "none");
    //  populateRecipeGrid();
});

$("#SaveBulkChanges").on("click", function () {

    var neVal = $(this).val();
    var dataFiltered = $("#gridRecipe").data("kendoGrid").dataSource.dataFiltered();


    var model = dataFiltered;
    var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
    var validChange = false;
    var uom = "";
    var quantity = "";
    var base = "";

    var uomspan = $("#uom");
    var quantityspan = $("#quantity");
    var basespan = $("#base");

    if (uomspan.val() != "Select") {
        validChange = true;
        uom = uomspan.val();
    }
    if (quantityspan.val() != "") {
        validChange = true;
        quantity = quantityspan.val();
    }

    if (basespan.val() != "Select") {
        validChange = true;
        base = basespan.val();
    }
    if (validChange == false) {
        $("#bulkerror").css("display", "block");
        $("#bulkerror").find("p").text("Please change at least one attribute for Bulk Update");
    } else {
        $("#bulkerror").css("display", "none");
        Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
            function () {
                $.each(model, function () {

                    if (uom != "")
                        this.UOM_ID = uom;
                    if (quantity != "")
                        this.Quantity = quantity;
                    if (base != "")
                        if (base == "Yes")
                            this.IsBase = true;
                        else
                            this.IsBase = false;
                });

                HttpClient.MakeRequest(CookBookMasters.SaveRegionRecipeDataList, function (result) {
                    if (result == false) {
                        $("#bulkerror").css("display", "block");
                        $("#bulkerror").find("p").text("Some error occured, please try again.");
                        setTimeout(fade_out, 10000);
                        function fade_out() {
                            $("#bulkerror").fadeOut();
                        }
                    }
                    else {
                        $(".k-overlay").hide();
                        $("#bulkerror").css("display", "none");
                        $("#success").css("display", "flex");
                        $("#success").find("p").text("Records have been updated successfully");
                        $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                        $("#gridRecipe").data("kendoGrid").dataSource.read();
                        $("#InitiateBulkChanges").css("display", "inline");
                        $("#CancelBulkChanges").css("display", "none");
                        $("#SaveBulkChanges").css("display", "none");
                        $("#gridBulkChange").css("display", "none");
                        setTimeout(fade_out, 10000);

                        function fade_out() {
                            $("#success").fadeOut();
                        }
                    }
                }, {
                    model: model

                }, true);

                //populateSiteGrid();
            },
            function () {
                maptype.focus();
            }
        );
    }
});
//Food Program Section Start


function populateBaseRecipeGrid(recipeCode) {
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe");
    gridVariable.html("");
    if (user.SectorNumber == "80") {
        gridVariable.kendoGrid({
            toolbar: [{ name: "cancel", text: "Reset" }

            ],
            groupable: false,
            editable: true,

            columns: [
                {
                    field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                            + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='RecipeRegshowDetails(this)'></i>`;
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: false,
                                filter: "contains",
                                dataTextField: "BaseRecipeName",
                                dataValueField: "BaseRecipe_ID",
                                autoBind: true,
                                dataSource: baserecipedata,
                                value: options.field,
                                change: onBRDDLChange
                            });
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Region Qty", width: "35px",
                    // template: '# if (IsMajor == true) {#<div><input type="number"  class="inputbrqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputbrqty"  name="Quantity" style="background-color:transparent!important; border:1px solid transparent!important" readonly oninput="calculateItemTotal(this)" /></div>#}#',
                    // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',

                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "SectorIngredientPerc", title: "Qty Change %", width: "50px", format: "{0:0.00}", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= (SectorIngredientPerc-IngredientPerc).toFixed(2)# </div>#} #',
                },
                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "SectorTotalCost", title: "Sector Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "sectortotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetRegionBaseRecipesByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                BaseRecipetotal = 0.0;
                                $.each(obj, function (key, value) {

                                    BaseRecipetotal = parseFloat(BaseRecipetotal) + parseFloat(value.Quantity);
                                });
                                $("#totBaseRecpQty").text(BaseRecipetotal.toFixed(2));
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridBaseRecipe").css("display", "block");
                                $("#emptybr").css("display", "none");
                            } else {
                                $("#gridBaseRecipe").css("display", "none");
                                $("#emptybr").css("display", "block");
                            }

                        },
                            {
                                regionID: $("#ddlRegionMaster").val(),
                                recipeCode: recipeCode,
                                subSectorCode: subSectorCodeDD
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false },
                            BaseRecipe_ID: { editable: false },
                            BaseRecipeName: { editable: false },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            IngredientPerc: { editable: false },
                            SectorIngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false },
                            SectorTotalCost: { editable: false }
                        }
                    }
                }
            },

            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },

            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.brTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "BaseRecipeName",
                        dataValueField: "BaseRecipe_ID",
                        change: onBRDDLChange,
                        //  autoBind: true,
                    });
                    //  ddt.find(".k-input").val(dataItem.BaseRecipeName);
                    $(this).find('.inputbrqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
    } else {
        gridVariable.kendoGrid({
            toolbar: [{ name: "cancel", text: "Reset" }

            ],
            groupable: false,
            editable: true,

            columns: [
                {
                    field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                            + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='RecipeRegshowDetails(this)'></i>`;
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: false,
                                filter: "contains",
                                dataTextField: "BaseRecipeName",
                                dataValueField: "BaseRecipe_ID",
                                autoBind: true,
                                dataSource: baserecipedata,
                                value: options.field,
                                change: onBRDDLChange
                            });
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Region Qty", width: "35px",
                    template: '# if (IsMajor == true) {#<div><input type="number"  class="inputbrqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputbrqty"  name="Quantity" style="background-color:transparent!important; border:1px solid transparent!important" readonly oninput="calculateItemTotal(this)" /></div>#}#',
                    // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',

                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "SectorIngredientPerc", title: "Qty Change %", width: "50px", format: "{0:0.00}", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= (SectorIngredientPerc-IngredientPerc).toFixed(2)# </div>#} #',
                },
                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "SectorTotalCost", title: "Sector Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "sectortotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetRegionBaseRecipesByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                var obj = result;
                                BaseRecipetotal = 0.0;
                                $.each(obj, function (key, value) {

                                    BaseRecipetotal = parseFloat(BaseRecipetotal) + parseFloat(value.Quantity);
                                });
                                $("#totBaseRecpQty").text(BaseRecipetotal.toFixed(2));
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridBaseRecipe").css("display", "block");
                                $("#emptybr").css("display", "none");
                            } else {
                                $("#gridBaseRecipe").css("display", "none");
                                $("#emptybr").css("display", "block");
                            }

                        },
                            {
                                regionID: $("#ddlRegionMaster").val(),
                                recipeCode: recipeCode,
                                subSectorCode: subSectorCodeDD
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false },
                            BaseRecipe_ID: { editable: false },
                            BaseRecipeName: { editable: false },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            IngredientPerc: { editable: false },
                            SectorIngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false },
                            SectorTotalCost: { editable: false }
                        }
                    }
                }
            },

            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },

            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.brTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "BaseRecipeName",
                        dataValueField: "BaseRecipe_ID",
                        change: onBRDDLChange,
                        //  autoBind: true,
                    });
                    //  ddt.find(".k-input").val(dataItem.BaseRecipeName);
                    $(this).find('.inputbrqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
    }


    var toolbar = $("#gridBaseRecipe").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");
    cancelChangesConfirmation('#gridBaseRecipe');
    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe .k-grid-content").addClass("gridInside");
}



function populateMOGGrid(recipeCode) {
    if (user.SectorNumber == "20") {
        Utility.Loading();
        var gridVariable = $("#gridMOG");
        gridVariable.html("");
        gridVariable.kendoGrid({
            toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,
            //navigatable: true,
            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: true,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: refreshMOGDropDownData(options.model.MOGName),
                                value: options.field,
                                change: onMOGDDLChange,
                            });
                    }
                },
                //{
                //    field: "UOMName", title: "UOM", width: "50px",
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "uomname",
                //        style: "text-align: center; font-weight:normal"
                //    },
                //},
                {
                    field: "UOMCode", title: "UOM", width: "40px",
                    attributes: {
                        style: "text-align: left; font-weight:normal",
                        class: ''
                    },
                    template: function (dataItem) {
                        var html = `  <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                            '<span class="uomname" style="margin-left:8px">' + kendo.htmlEncode(dataItem.UOMName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOGUOM" text="' + options.model.UOMName + '" data-text-field="text" data-value-field="UOMCode" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "text",
                                dataValueField: "UOMCode",
                                autoBind: true,
                                dataSource: moguomdata,
                                // dataSource: refreshMOGUOMDropDownData(options.model.UOMName),
                                value: options.field,
                                change: onMOGUOMDDLChange,
                            });

                    }
                },

                //{
                //    field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "si",
                //        style: "text-align: center; font-weight:normal"
                //    }
                //},
                {
                    field: "Quantity", title: "Region Qty", width: "50px",
                    template: '# if (IsMajor == true) {#<div><input type="number" class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input type="number"  class="inputmogqty"  name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',

                    // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',


                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },

                //{
                //    field: "SectorIngredientPerc", title: "Qty Change % ", width: "50px", format: "{0:0.00}", editable: false,
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "change",
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    template: '#  {#<div>#= ((Quantity-SectorQuantity)*100/SectorQuantity).toFixed(2)# </div>#} #',
                //},

                {
                    field: "DKgTotal", title: "DKG", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },
                    attributes: {
                        class: "dkgtotal",
                        style: "text-align: center; font-weight:normal;width:35px;"
                    },
                },
                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                //{
                //    field: "SectorTotalCost", title: "Sector  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                //    headerAttributes: {

                //        style: "text-align: right"
                //    },
                //    attributes: {
                //        class: "sectortotalcost",
                //        style: "text-align: right; font-weight:normal"
                //    },
                //},
                {
                    field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Action",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",
                            //iconClass: "fa fa-trash-alt",
                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridMOG").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                gridObj.dataSource._data.remove(selectedRow);
                                CalculateGrandTotal('');
                                return true;
                            }
                        },
                        //{
                        //    name: 'View APL',
                        //    click: function (e) {
                        //        var gridObj = $("#gridMOG").data("kendoGrid");
                        //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                        //        datamodel = tr;
                        //        console.log("MOG APL");
                        //        console.log(tr);
                        //        MOGName = tr.MOGName;
                        //        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                        //        $(".k-overlay").css("display", "block");
                        //        $(".k-overlay").css("opacity", "0.5");
                        //        dialog.open();
                        //        dialog.center();
                        //        dialog.title("MOG APL Details - " + MOGName);
                        //        MOGCode = tr.MOGCode;
                        //        mogWiseAPLMasterdataSource = [];
                        //        getMOGWiseAPLMasterData(MOGCode);
                        //        populateAPLMasterGrid();
                        //        return true;
                        //    }
                        //}
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeRequest(CookBookMasters.GetRegionMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                console.log(result);
                                options.success(result);
                                var obj = result;
                                mogtotal = 0.0;
                                if (user.SectorNumber == "20") {
                                    $.each(obj, function (key, value) {
                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.DKgValue * value.Quantity);
                                    });
                                }
                                else {
                                    $.each(obj, function (key, value) {
                                        mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                    });
                                }

                                $("#totmog").text(mogtotal.toFixed(2));
                                var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                $("#ToTIngredients").text(tot);
                            }
                            else {
                                options.success("");
                            }
                            $("#gridMOG").css("display", "block");
                            $("#emptymog").css("display", "none");
                            //if (result.length > 0) {
                            //    $("#gridMOG").css("display", "block");
                            //    $("#emptymog").css("display", "none");
                            //} else {
                            //    $("#gridMOG").css("display", "none");
                            //    $("#emptymog").css("display", "block");
                            //}
                        },
                            {
                                regionID: $("#ddlRegionMaster").val(),
                                recipeCode: recipeCode,
                                subSectorCode: subSectorCodeDD
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { editable: true },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            IngredientPerc: { editable: false },
                            SectorIngredientPerc: { editable: false },
                            SectorTotalCost: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false },
                            SectorQuantity: { editable: false },
                            DKgValue: { editable: false },
                            DKgTotal: { editable: false },
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });


        var toolbar = $("#gridMOG").find(".k-grid-toolbar");
        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG .k-grid-content").addClass("gridInside");
        toolbar.find(".k-grid-clearall").addClass("gridButton");
        $('#gridMOG a.k-grid-clearall').click(function (e) {
             Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    $("#gridMOG").data('kendoGrid').dataSource.data([]);
                    showHideGrid('gridMOG', 'emptymog', 'mup_gridBaseRecipe');
                    gridIdin = 'gridMOG';
                },
                function () {
                }
            );

        });
        $(".k-grid-cancel-changes").click(function (e) {
             Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    // var grid = $("#" + gridIdin + "").data("kendoGrid");
                    if (gridIdin == 'gridBaseRecipe') {
                        populateBaseRecipeGrid(resetID);
                        return;
                    } else if (gridIdin == 'gridMOG') {
                        populateMOGGrid(resetID)
                        return;
                    }
                    // grid.cancelChanges();
                },
                function () {
                }
            );

        });
    }
    else if (user.SectorNumber == "80") {
        Utility.Loading();
        var gridVariable = $("#gridMOG");
        gridVariable.html("");
        gridVariable.kendoGrid({
            // toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: false,

            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: false,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: mogdata,
                                value: options.field,
                                change: onMOGDDLChange,
                            });
                        // container.find(".k-input")[0].text = options.model.MOGName;
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Region Qty", width: "50px",
                    //template: '# if (IsMajor == true) {#<div><input type="number" class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input type="number"  class="inputmogqty"  name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',

                    // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',


                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },

                //{
                //    field: "IngredientPerc", title: "Region%", width: "50px", format: "{0:0.00}", editable: false,
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "minor",
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    //template: '# if (IsMajor == true) {#<div>#= IngredientPerc# %</div>#} else {#<div>-</div>#}#',
                //},

                {
                    field: "SectorIngredientPerc", title: "Qty Change % ", width: "50px", format: "{0:0.00}", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= ((Quantity-SectorQuantity)*100/SectorQuantity).toFixed(2)# </div>#} #',
                },


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "SectorTotalCost", title: "Sector  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "sectortotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                //{
                //    field: "SectorTotalCost", title: "Change Cost % ", width: "50px", format: "{0:0.00}", editable: false,
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "costchange",
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    template: '#  {#<div>#= ((TotalCost - SectorTotalCost)*100/SectorTotalCost).toFixed(2)# </div>#} #',
                //},
                //{
                //    title: "Action",
                //    width: "60px",
                //    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        style: "text-align: center; font-weight:normal;"
                //    },
                //    //command: [
                //    //    {
                //    //        name: 'View APL',
                //    //        click: function (e) {
                //    //            var gridObj = $("#gridMOG").data("kendoGrid");
                //    //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //    //            datamodel = tr;
                //    //            console.log("MOG APL");
                //    //            console.log(tr);
                //    //            MOGName = tr.MOGName;
                //    //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //    //            $(".k-overlay").css("display", "block");
                //    //            $(".k-overlay").css("opacity", "0.5");
                //    //            dialog.open();
                //    //            dialog.center();
                //    //            dialog.title("MOG APL Details - " + MOGName);
                //    //            MOGCode = tr.MOGCode;
                //    //            mogWiseAPLMasterdataSource = [];
                //    //            getMOGWiseAPLMasterData(MOGCode);
                //    //            populateAPLMasterGrid();
                //    //            return true;
                //    //        }
                //    //    }
                //    //],
                //}
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeRequest(CookBookMasters.GetRegionMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                console.log(result);
                                options.success(result);
                                var obj = result;
                                mogtotal = 0.0;
                                $.each(obj, function (key, value) {

                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                });

                                $("#totmog").text(mogtotal.toFixed(2));
                                var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                $("#ToTIngredients").text(tot);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {
                                regionID: $("#ddlRegionMaster").val(),
                                recipeCode: recipeCode,
                                subSectorCode: subSectorCodeDD
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { editable: false },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            IngredientPerc: { editable: false },
                            SectorIngredientPerc: { editable: false },
                            SectorTotalCost: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
        var toolbar = $("#gridMOG").find(".k-grid-toolbar");

        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();

        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton");
        cancelChangesConfirmation('#gridMOG');
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG .k-grid-content").addClass("gridInside");
    }
    else {
        Utility.Loading();
        var gridVariable = $("#gridMOG");
        gridVariable.html("");
        gridVariable.kendoGrid({
            // toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: false,

            columns: [
                {
                    field: "MOG_ID", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                enable: false,
                                filter: "contains",
                                dataTextField: "MOGName",
                                dataValueField: "MOG_ID",
                                autoBind: true,
                                dataSource: mogdata,
                                value: options.field,
                                change: onMOGDDLChange,
                            });
                        // container.find(".k-input")[0].text = options.model.MOGName;
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "si",
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Region Qty", width: "50px",
                    template: '# if (IsMajor == true) {#<div><input type="number" class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input type="number"  class="inputmogqty"  name="Quantity" oninput="calculateItemTotalMOG(this)"/></div>#}#',

                    // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',


                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },

                //{
                //    field: "IngredientPerc", title: "Region%", width: "50px", format: "{0:0.00}", editable: false,
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "minor",
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    //template: '# if (IsMajor == true) {#<div>#= IngredientPerc# %</div>#} else {#<div>-</div>#}#',
                //},

                {
                    field: "SectorIngredientPerc", title: "Qty Change % ", width: "50px", format: "{0:0.00}", editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "change",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#  {#<div>#= ((Quantity-SectorQuantity)*100/SectorQuantity).toFixed(2)# </div>#} #',
                },


                {
                    field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "SectorTotalCost", title: "Sector  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "sectortotalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                //{
                //    field: "SectorTotalCost", title: "Change Cost % ", width: "50px", format: "{0:0.00}", editable: false,
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        class: "costchange",
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    template: '#  {#<div>#= ((TotalCost - SectorTotalCost)*100/SectorTotalCost).toFixed(2)# </div>#} #',
                //},
                //{
                //    title: "Action",
                //    width: "60px",
                //    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    attributes: {
                //        style: "text-align: center; font-weight:normal;"
                //    },
                //    //command: [
                //    //    {
                //    //        name: 'View APL',
                //    //        click: function (e) {
                //    //            var gridObj = $("#gridMOG").data("kendoGrid");
                //    //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //    //            datamodel = tr;
                //    //            console.log("MOG APL");
                //    //            console.log(tr);
                //    //            MOGName = tr.MOGName;
                //    //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //    //            $(".k-overlay").css("display", "block");
                //    //            $(".k-overlay").css("opacity", "0.5");
                //    //            dialog.open();
                //    //            dialog.center();
                //    //            dialog.title("MOG APL Details - " + MOGName);
                //    //            MOGCode = tr.MOGCode;
                //    //            mogWiseAPLMasterdataSource = [];
                //    //            getMOGWiseAPLMasterData(MOGCode);
                //    //            populateAPLMasterGrid();
                //    //            return true;
                //    //        }
                //    //    }
                //    //],
                //}
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";
                        HttpClient.MakeRequest(CookBookMasters.GetRegionMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                console.log(result);
                                options.success(result);
                                var obj = result;
                                mogtotal = 0.0;
                                $.each(obj, function (key, value) {

                                    mogtotal = parseFloat(mogtotal) + parseFloat(value.Quantity);
                                });

                                $("#totmog").text(mogtotal.toFixed(2));
                                var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
                                $("#ToTIngredients").text(tot);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {
                                regionID: $("#ddlRegionMaster").val(),
                                recipeCode: recipeCode,
                                subSectorCode: subSectorCodeDD
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOG_ID: { editable: false },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { type: "number", validation: { required: true, min: 1 } },
                            UOMName: { editable: false },
                            CostPerUOM: { editable: false },
                            IngredientPerc: { editable: false },
                            SectorIngredientPerc: { editable: false },
                            SectorTotalCost: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        enable: false,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
        var toolbar = $("#gridMOG").find(".k-grid-toolbar");

        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();

        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton");
        cancelChangesConfirmation('#gridMOG');
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG .k-grid-content").addClass("gridInside");
    }
}

function refreshMOGDropDownData(x) {

    if (mogdataList != null && mogdataList.length > 0) {
        var mogGridData = $("#gridMOG").data('kendoGrid').dataSource._data;

        if (mogGridData != null && mogGridData.length > 0) {
            mogGridData = mogGridData.filter(m => m.MOGName != "" && m.MOGName != x);
            mogdata = mogdataList;
            var mogdataListtemp = mogdataList;
            mogdataListtemp = mogdataListtemp.filter(m => m.MOG_Code != "");
            mogdataListtemp.forEach(function (item, index) {
                for (var i = 0; i < mogGridData.length; i++) {
                    if (mogGridData[i].MOGCode !== "" && item.MOG_Code === mogGridData[i].MOGCode) {
                        // mogdata.splice(index, 1);
                        mogdata = mogdata.filter(mog => mog.MOG_Code !== item.MOG_Code);
                        break;
                    }
                }
                return true;
            });
        }
    }
    return mogdata.filter(m => m.IsActive == true);
};


function onMOGUOMDDLChange(e) {
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    console.log("Data Item ")
    console.log(dataItem)

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("UOM_ID", e.sender.dataItem().UOM_ID);
    dataItem.set("UOMName", data);
    dataItem.set("UOMCode", e.sender.value());
    dataItem.UOMName = data;

    dataItem.UOM_ID = e.sender.dataItem().UOM_ID;
    dataItem.UOMCode = e.sender.value();
    //dataItem.CostPerKG = costperkg;
    //dataItem.CostPerUOM = costperuom;

    //dataItem.set("CostPerKG", costperkg);
    //dataItem.set("CostPerUOM", costperuom);
    //var dkgtotal = 0;
    //if (user.SectorNumber == '20') {
    //    dkgtotal = e.sender.dataItem().DKgValue * dataItem.Quantity;
    //    dataItem.set("DKgValue", dkgtotal);
    //    dataItem.DKgValue = e.sender.dataItem().DKgValue;
    //    $(element).closest('tr').find('.dkgtotal')[0].innerHTML = dkgtotal;
    //    $(element).closest('tr').find('.uomcost')[0].innerHTML = costperuom;
    //    dataItem.TotalCost = dataItem.CostPerUOM * dkgtotal;
    //}
    //else {
    //    $(element).closest('tr').find('.uomcost')[0].innerHTML = costperuom;
    //    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    //}

    //$(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    //CalculateGrandTotal('');
}

function onMOGDDLChange(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG', false)) {
        toastr.error("MOG already exists.");
        var dropdownlist = $("#ddlMOG").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);

    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var UOMCode = Obj[0].UOM_Code;

    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    dataItem.UOMCode = UOMCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    var dkgtotal = 0;
    if (user.SectorNumber == '20') {
        dkgtotal = e.sender.dataItem().DKgValue * dataItem.Quantity;
        dataItem.set("DKgValue", dkgtotal);
        dataItem.DKgValue = e.sender.dataItem().DKgValue;
        $(element).closest('tr').find('.dkgtotal')[0].innerHTML = dkgtotal;
        dataItem.TotalCost = dataItem.CostPerUOM * dkgtotal;
    }
    else {
        dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    }
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('');
};

function populateBulkChangeControls() {

    Utility.Loading();
    var gridVariable = $("#gridBulkChange");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: false,
        filterable: false,
        //reorderable: true,
        //scrollable: true,
        columns: [
            {
                field: "Name", title: "Recipe Name", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOMName", title: "UOM", width: "60px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },

            },
            {
                field: "IsBase", title: "Base Recipe", width: "80px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "", title: "", width: "20px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            }
        ],
        columnResize: function (e) {

            e.preventDefault();
        },
        dataBound: function (e) {
        },
        change: function (e) {
        },
    });
}

$(document).ready(function () {
    $("#addbr").on("click", function () {

        $("#gridBaseRecipe").css("display", "block");
        $("#emptybr").css("display", "none");
        $("#gridBaseRecipe").data("kendoGrid").addRow();
    })

    $("#addmog").on("click", function () {
        $("#gridMOG").css("display", "block");
        $("#emptymog").css("display", "none");
        $("#gridMOG").data("kendoGrid").addRow();
    })
    $("#addbr0").on("click", function () {

        $("#gridBaseRecipe0").css("display", "block");
        $("#emptybr0").css("display", "none");
        $("#gridBaseRecipe0").data("kendoGrid").addRow();
    })

    $("#addbr1").on("click", function () {

        $("#gridBaseRecipe1").css("display", "block");
        $("#emptybr1").css("display", "none");
        $("#gridBaseRecipe1").data("kendoGrid").addRow();
    })
    $("#addbr2").on("click", function () {

        $("#gridBaseRecipe2").css("display", "block");
        $("#emptybr2").css("display", "none");
        $("#gridBaseRecipe2").data("kendoGrid").addRow();
    })
    $("#addbr3").on("click", function () {

        $("#gridBaseRecipe3").css("display", "block");
        $("#emptybr3").css("display", "none");
        $("#gridBaseRecipe3").data("kendoGrid").addRow();
    })
    $("#addbr4").on("click", function () {

        $("#gridBaseRecipe4").css("display", "block");
        $("#emptybr4").css("display", "none");
        $("#gridBaseRecipe4").data("kendoGrid").addRow();
    })
    $("#addbr5").on("click", function () {

        $("#gridBaseRecipe5").css("display", "block");
        $("#emptybr5").css("display", "none");
        $("#gridBaseRecipe5").data("kendoGrid").addRow();
    })
    $("#addmog0").on("click", function () {

        $("#gridMOG0").css("display", "block");
        $("#emptymog0").css("display", "none");
        $("#gridMOG0").data("kendoGrid").addRow();
    })
    $("#addmog1").on("click", function () {

        $("#gridMOG1").css("display", "block");
        $("#emptymog1").css("display", "none");
        $("#gridMOG1").data("kendoGrid").addRow();
    })
    $("#addmog2").on("click", function () {

        $("#gridMOG2").css("display", "block");
        $("#emptymog2").css("display", "none");
        $("#gridMOG2").data("kendoGrid").addRow();
    })
    $("#addmog3").on("click", function () {

        $("#gridMOG3").css("display", "block");
        $("#emptymog3").css("display", "none");
        $("#gridMOG3").data("kendoGrid").addRow();
    })
    $("#addmog4").on("click", function () {

        $("#gridMOG4").css("display", "block");
        $("#emptymog4").css("display", "none");
        $("#gridMOG4").data("kendoGrid").addRow();
    })
    $("#addmog5").on("click", function () {

        $("#gridMOG5").css("display", "block");
        $("#emptymog5").css("display", "none");
        $("#gridMOG5").data("kendoGrid").addRow();
    })

    $("#AddNew").on("click", function () {
        $("#gridBaseRecipe0").css("display", "None");
        $("#gridMOG0").css("display", "None");
        $("#success").css("display", "none");
        $("#error0").css("display", "none");
        var editor = $("#RegRecinputinstruction0").data("kendoEditor");
        editor.value('');
        var model;

        //$(".k-overlay").css("display", "block");
        // $(".k-overlay").css("opacity", "0.5");

        $("#windowEdit0").css("display", "block");
        if (user.SectorNumber == "20" ){
            populateRecipeCategoryUOMDropdown0();
            populateWtPerUnitUOMDropdown0();
            populateGravyWtPerUnitUOMDropdown0();
            //populateRecipeCategoryUOMDropdown("0")
            $("#inputwtperunituom1").closest(".k-widget").hide();
            $("#inputgravywtperunituom1").closest(".k-widget").hide();
            $("#RegRecinputuom1").closest(".k-widget").hide();
        }
        populateUOMDropdown0();
        populateBaseDropdown0();
        populateBaseRecipeGrid0();
        populateMOGGrid0();
        $("#mainContent").hide();

        $("#windowEdit0").show();

        datamodel = model;
        $("#RegRecinputinstruction0").val("");
        $("#RegRecrecipeid0").text("");
        $("#RegRecinputrecipename0").val("");

        $("#RegRecinputquantity0").val("");
        if (user.SectorNumber == "20" ){
            $("#inputwtperunit0").val("");
            $("#inputgravywtperunit0").val("");
            $("#RegRecinputuom0").data("kendoDropDownList").value("Select");
            $("#inputrecipecum0").data("kendoDropDownList").value("Select");
            $("#inputwtperunituom0").data("kendoDropDownList").value("Select");
            $("#inputgravywtperunituom0").data("kendoDropDownList").value("Select");

            var recipeCategory = "";
            var rCatData = $("#inputrecipecum0").data('kendoDropDownList')
            if (rCatData != null && rCatData != "Select" && rCatData != "" && rCatData.dataItem() != null && rCatData.dataItem().UOMCode != "Select") {
                recipeCategory = rCatData.dataItem().UOMCode
            }
            OnRecipeUOMCategoryChange(recipeCategory, "0");
        }
        //  $("#RegRecinputbase0").data('kendoDropDownList').value("Select");
        // $("#gridBaseRecipe0").data("kendoGrid").addRow();
        // $("#gridMOG0").data("kendoGrid").addRow();
        // hideGrid('gridBaseRecipe0', 'emptybr0', 'baseRecipePlus0', 'baseRecipeMinus0');
        // hideGrid('gridMOG0', 'emptymog0', 'mogPlus0', 'mogMinus0');
        $("#gridMOG").css("display", "none");
        $("#emptymog0").css("display", "block");
        $("#gridBaseRecipe").css("display", "none");
        $("#emptybr0").css("display", "block");
    })

    $("#btnCancel0").on("click", function () {
        $(".k-overlay").hide();
        // var orderWindow = $("#windowEdit0").data("kendoWindow");
        $("#emptybr0").css("display", "block");
        $("#gridBaseRecipe0").css("display", "none");
        // orderWindow.close();
        $("#mainContent").show();
        $("#windowEdit0").hide();
    });

    $("#btnCancel1").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#RecipeReConfgwindowEdit1").data("kendoWindow");
        $("#emptybr1").css("display", "block");
        $("#gridBaseRecipe1").css("display", "none");
        orderWindow.close();
    });
    $("#btnCancel2").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#RecipeReConfgwindowEdit2").data("kendoWindow");
        $("#emptybr2").css("display", "block");
        $("#gridBaseRecipe2").css("display", "none");
        orderWindow.close();
        $("#RecipeReConfgwindowEdit1").parent().show();
    });
    $("#btnCancel3").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#RecipeReConfgwindowEdit3").data("kendoWindow");
        $("#emptybr3").css("display", "block");
        $("#gridBaseRecipe3").css("display", "none");
        orderWindow.close();
        $("#RecipeReConfgwindowEdit2").parent().show();
    });

    $("#btnCancel4").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#RecipeReConfgwindowEdit4").data("kendoWindow");
        $("#emptybr4").css("display", "block");
        $("#gridBaseRecipe4").css("display", "none");
        orderWindow.close();
        $("#RecipeReConfgwindowEdit3").parent().show();
    });
    $("#btnCancel5").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#RecipeReConfgwindowEdit5").data("kendoWindow");
        $("#emptybr5").css("display", "block");
        $("#gridBaseRecipe5").css("display", "none");
        orderWindow.close();
        $("#RecipeReConfgwindowEdit4").parent().show();
    });
    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();
        // var orderWindow = $("#RecipeReConfgwindowEdit1").data("kendoWindow");
        // "none");
        // orderWindow.close();
    });

    $("#btnSubmit0").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie
        if (!validateDataBeforeSave("gridBaseRecipe0", 'gridMOG0')) {
            toastr.error("Invalid Data!");
            return false;
        }
        if ($("#RegRecinputquantity").val() == null || $("#RegRecinputquantity").val() == "" || $("#RegRecinputquantity").val() == undefined) {
            toastr.error("Please enter Recipe Quantity");
            Utility.UnLoading();
            return false;
        }

        if (user.SectorNumber == "20" ){

            if ($("#inputrecipecum0").val() == null || $("#inputrecipecum0").val() == "" || $("#inputrecipecum0").val() == undefined || $("#inputrecipecum0").val() == "Select") {
                toastr.error("Please select Recipe UOM Category");
                Utility.UnLoading();
                return false;
            }
        }

        if ($("#RegRecinputuom0").val() == null || $("#RegRecinputuom0").val() == "" || $("#RegRecinputuom0").val() == undefined || $("#RegRecinputuom0").val() == "Select") {
            toastr.error("Please select UOM");
            Utility.UnLoading();
            return false;
        }
        var brgrid = $("#gridBaseRecipe0").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "Region_ID": $("#ddlRegionMaster").val(),
                "RecipeCode": $("#RegRecinputrecipecode0").val(),
                "Recipe_ID": $("#RegRecrecipeid0").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG0").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "Region_ID": $("#ddlRegionMaster").val(),
                "RecipeCode": $("#RegRecinputrecipecode0").val(),
                "Recipe_ID": $("#RegRecrecipeid0").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#RegRecinputbase0").val() == "Yes")
            base = true
        else if ($("#RegRecinputbase0").val() == "No")
            base = false

        var model = {
            "Region_ID": $("#ddlRegionMaster").val(),
            "RecipeCode": $("#RegRecinputrecipecode0").val(),
            "ID": $("#RegRecrecipeid0").val(),
            "Name": $("#RegRecinputrecipename0").val(),
            "UOM_ID": $("#RegRecinputuom0").val(),
            "Quantity": $("#RegRecinputquantity0").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG0").text(),
            "TotalCost": $('#grandTotal0').text(),
            "Quantity": $('#RegRecinputquantity0').val(),
            "Instructions": $("#RegRecinputinstruction0").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy,
            "UOM_ID": $("#RegRecinputuom0").val(),
            "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum0").val() : "",
            "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit0").val() : "",
            "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom0").val() : "",
            "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit0").val() : "",
            "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom0").val() : "",
            "TotalIngredientWeight": $("#ToTIngredients").text(),
        }

        if ($("#RegRecinputrecipename0").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#RegRecinputrecipename0").focus();
        }
        else {

            var base;
            if ($("#RegRecinputbase0").val() == "Yes")
                base = true
            else if ($("#RegRecinputbase0").val() == "No")
                base = false


            $("#btnSubmit0").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRegionRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit0').removeAttr("disabled");

                    toastr.error("Some error occured, please try again");
                    $("#RegRecinputrecipename0").focus();
                }
                else {
                    $(".k-overlay").hide();
                    $('#btnSubmit0').removeAttr("disabled");
                    toastr.success("New Recipe added successfully");

                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnSubmit1").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        if (!validateDataBeforeSave("gridBaseRecipe1", 'gridMOG1')) {
            toastr.error("Invalid Data!");
            return false;
        }
        var brgrid = $("#gridBaseRecipe1").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "Region_ID": $("#ddlRegionMaster").val(),
                "RecipeCode": $("#RegRecinputrecipecode1").val(),
                "Recipe_ID": $("#RegRecrecipeid1").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG1").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "Region_ID": $("#ddlRegionMaster").val(),
                "RecipeCode": $("#RegRecinputrecipecode1").val(),
                "Recipe_ID": $("#RegRecrecipeid1").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#RegRecinputbase1").val() == "Yes")
            base = true
        else if ($("#RegRecinputbase1").val() == "No")
            base = false
        var model = {
            "Region_ID": $("#ddlRegionMaster").val(),
            "RecipeCode": $("#RegRecinputrecipecode1").val(),
            "ID": $("#RegRecrecipeid1").val(),
            "Name": $("#RegRecinputrecipename1").val(),
            "UOM_ID": $("#RegRecinputuom1").val(),
            "Quantity": $("#RegRecinputquantity1").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG1").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#RegRecinputquantity1').val(),
            "Instructions": $("#RegRecinputinstruction1").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy
        }

        if ($("#RegRecinputrecipename1").val() === "") {
            $("#error").css("display", "flex");
            $("#error").find("p").text("Please provide valid input(s)");
            $("#RegRecinputrecipename1").focus();
        }
        else {
            $("#error").css("display", "none");
            var base;
            if ($("#RegRecinputbase1").val() == "Yes")
                base = true
            else if ($("#RegRecinputbase1").val() == "No")
                base = false


            $("#btnSubmit1").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRegionRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit1').removeAttr("disabled");
                    $("#error1").css("display", "flex");
                    $("#error1").find("p").text("Some error occured, please try again");
                    $("#RegRecinputrecipename1").focus();
                }
                else {
                    $(".k-overlay").hide();
                    $("#error1").css("display", "flex");
                    var orderWindow = $("#RecipeReConfgwindowEdit1").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit1').removeAttr("disabled");
                    $("#success").css("display", "flex");
                    if (model.ID > 0)
                        $("#success").find("p").text("Recipe configuration updated successfully");
                    else
                        $("#success").find("p").text("New Recipe added successfully");
                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();
                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#success").fadeOut();
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnSubmit2").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        if (!validateDataBeforeSave("gridBaseRecipe2", 'gridMOG2')) {
            toastr.error("Invalid Data!");
            return false;
        }
        var brgrid = $("#gridBaseRecipe2").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "Region_ID": $("#ddlRegionMaster").val(),
                "RecipeCode": $("#RegRecinputrecipecode2").val(),
                "Recipe_ID": $("#RegRecrecipeid2").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG2").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "Region_ID": $("#ddlRegionMaster").val(),
                "RecipeCode": $("#RegRecinputrecipecode2").val(),
                "Recipe_ID": $("#RegRecrecipeid2").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#RegRecinputbase2").val() == "Yes")
            base = true
        else if ($("#RegRecinputbase2").val() == "No")
            base = false
        var model = {
            "Region_ID": $("#ddlRegionMaster").val(),
            "RecipeCode": $("#RegRecinputrecipecode2").val(),
            "ID": $("#RegRecrecipeid2").val(),
            "Name": $("#RegRecinputrecipename2").val(),
            "UOM_ID": $("#RegRecinputuom2").val(),
            "Quantity": $("#RegRecinputquantity2").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG2").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#RegRecinputquantity1').val(),
            "Instructions": $("#RegRecinputinstruction2").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy
        }

        if ($("#RegRecinputrecipename2").val() === "") {
            $("#error").css("display", "flex");
            $("#error").find("p").text("Please provide valid input(s)");
            $("#RegRecinputrecipename2").focus();
        }
        else {
            $("#error").css("display", "none");
            var base;
            if ($("#RegRecinputbase2").val() == "Yes")
                base = true
            else if ($("#RegRecinputbase2").val() == "No")
                base = false


            $("#btnSubmit2").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRegionRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit2').removeAttr("disabled");
                    $("#error2").css("display", "flex");
                    $("#error2").find("p").text("Some error occured, please try again");
                    $("#RegRecinputrecipename2").focus();
                }
                else {
                    $(".k-overlay").hide();
                    $("#error2").css("display", "flex");
                    var orderWindow = $("#RecipeReConfgwindowEdit2").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit2').removeAttr("disabled");
                    $("#success").css("display", "flex");
                    if (model.ID > 0)
                        $("#success").find("p").text("Recipe configuration updated successfully");
                    else
                        $("#success").find("p").text("New Recipe added successfully");
                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();
                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#success").fadeOut();
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
        $("#RecipeReConfgwindowEdit1").parent('.k-widget').css("display", "block");
    });

    $("#btnSubmit3").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        if (!validateDataBeforeSave("gridBaseRecipe3", 'gridMOG3')) {
            toastr.error("Invalid Data!");
            return false;
        }
        var brgrid = $("#gridBaseRecipe3").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "Region_ID": $("#ddlRegionMaster").val(),
                "RecipeCode": $("#RegRecinputrecipecode3").val(),
                "Recipe_ID": $("#RegRecrecipeid3").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG3").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "Region_ID": $("#ddlRegionMaster").val(),
                "RecipeCode": $("#RegRecinputrecipecode3").val(),
                "Recipe_ID": $("#RegRecrecipeid3").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#RegRecinputbase3").val() == "Yes")
            base = true
        else if ($("#RegRecinputbase3").val() == "No")
            base = false
        var model = {
            "Region_ID": $("#ddlRegionMaster").val(),
            "RecipeCode": $("#RegRecinputrecipecode3").val(),
            "ID": $("#RegRecrecipeid3").val(),
            "Name": $("#RegRecinputrecipename3").val(),
            "UOM_ID": $("#RegRecinputuom3").val(),
            "Quantity": $("#RegRecinputquantity3").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG3").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#RegRecinputquantity1').val(),
            "Instructions": $("#RegRecinputinstruction3").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy
        }

        if ($("#RegRecinputrecipename3").val() === "") {
            $("#error").css("display", "flex");
            $("#error").find("p").text("Please provide valid input(s)");
            $("#RegRecinputrecipename3").focus();
        }
        else {
            $("#error").css("display", "none");
            var base;
            if ($("#RegRecinputbase3").val() == "Yes")
                base = true
            else if ($("#RegRecinputbase3").val() == "No")
                base = false


            $("#btnSubmit3").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRegionRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit1').removeAttr("disabled");
                    $("#error3").css("display", "flex");
                    $("#error3").find("p").text("Some error occured, please try again");
                    $("#RegRecinputrecipename3").focus();
                }
                else {
                    $(".k-overlay").hide();
                    $("#error3").css("display", "flex");
                    var orderWindow = $("#RecipeReConfgwindowEdit3").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit2').removeAttr("disabled");
                    $("#success").css("display", "flex");
                    if (model.ID > 0)
                        $("#success").find("p").text("Recipe configuration updated successfully");
                    else
                        $("#success").find("p").text("New Recipe added successfully");
                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();
                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#success").fadeOut();
                    }
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnSubmit").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie
        if (!validateDataBeforeSave("gridBaseRecipe", 'gridMOG')) {
            toastr.error("Invalid Data!");
            return false;
        }
        if (!validationPerCost()) {

            return false;
        }
        var rName = $("#RegRecinputrecipename").val();
        var rAlias = $("#RegRecinputrecipealiasname").val();
        if (rName === rAlias) {
            Utility.UnLoading();
            toastr.error("Recipe Name and Recipe Alias should be different");
            return;
        }
        if ($("#RegRecinputquantity").val() == null || $("#RegRecinputquantity").val() == "" || $("#RegRecinputquantity").val() == undefined || $("#RegRecinputquantity").val() == 0) {
            toastr.error("Please enter Recipe Quantity");
            Utility.UnLoading();
            return false;
        }

        if (user.SectorNumber == "20" ){

            if ($("#inputrecipecum").val() == null || $("#inputrecipecum").val() == "" || $("#inputrecipecum").val() == undefined || $("#inputrecipecum").val() == "Select") {
                toastr.error("Please select Recipe UOM Category");
                Utility.UnLoading();
                return false;
            }
        }

        if ($("#RegRecinputuom").val() == null || $("#RegRecinputuom").val() == "" || $("#RegRecinputuom").val() == undefined || $("#RegRecinputuom").val() == "Select") {
            toastr.error("Please select UOM");
            Utility.UnLoading();
            return false;
        }

        var baseRecipes = [];

        if (user.SectorNumber != "20") {
            var brgrid = $("#gridBaseRecipe").data("kendoGrid").dataSource;
            var data = brgrid._data;
            for (var i = 0; i < brgrid._data.length; i++) {
                var obj = {
                    "ID": data[i].ID,
                    "IngredientPerc": data[i].IngredientPerc,
                    "RecipeCode": $("#RegRecinputrecipecode").val(),
                    "Recipe_ID": Recipe_ID,
                    "BaseRecipe_ID": data[i].BaseRecipe_ID,
                    "UOM_ID": data[i].UOM_ID,
                    "CostPerUOM": data[i].CostPerUOM,
                    "CostPerKG": data[i].CostPerKG,
                    "TotalCost": data[i].TotalCost,
                    "Quantity": data[i].Quantity,
                    "IsActive": 1,
                    "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                    "CreatedBy": data[i].CreatedBy,
                    "Region_ID": regionCode,
                    "CreatedOn": data[i].BaseRecipe_ID == 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].CreatedOn),
                    "CreatedBy": data[i].BaseRecipe_ID == 0 ? user.UserId : data[i].CreatedBy,
                    "ModifiedOn": data[i].BaseRecipe_ID > 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].ModifiedOn),
                    "ModifiedBy": data[i].BaseRecipe_ID > 0 ? user.UserId : data[i].ModifiedBy,


                }
                baseRecipes.push(obj);
            }
        }
        //MOG
        var mogGrid = $("#gridMOG").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "ID": data[i].ID,
                "RecipeCode": $("#RegRecinputrecipecode").val(),
                "Recipe_ID": Recipe_ID,
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "DKgValue": data[i].DKgTotal,
                "IsActive": 1,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy,
                "Region_ID": regionCode,
                "IngredientPerc": data[i].IngredientPerc,
                "CreatedOn": data[i].MOG_ID == 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].MOG_ID == 0 ? user.UserId : data[i].CreatedBy,
                "ModifiedOn": data[i].MOG_ID > 0 ? Utility.CurrentDate() : kendo.parseDate(data[i].ModifiedOn),
                "ModifiedBy": data[i].MOG_ID > 0 ? user.UserId : data[i].ModifiedBy

            }
            mogs.push(obj);
        }

        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#RegRecinputuom").val();
        });

        ////Top Level Actual Recipie
        var model = {
            "ID": $("#RegRecrecipeid").val(),
            "RecipeCode": $("#RegRecinputrecipecode").val(),
            "CostPerKG": $("#grandCostPerKG").text().substring(1).replace(",", ""),
            "TotalCost": $('#grandTotal').text().substring(1).replace(",", ""),
            "Quantity": $('#RegRecinputquantity').val(),
            "Instructions": $("#RegRecinputinstruction").val(),
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy,
            "ModifiedOn": Utility.CurrentDate(),
            "ModifiedBy": user.UserId,
            "UOM_ID": $("#RegRecinputuom").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Region_ID": regionCode,
            "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum").val() : "",
            "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit").val() : "",
            "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom").val() : "",
            "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit").val() : "",
            "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom").val() : "",
            "TotalIngredientWeight": $("#ToTIngredients").text(),

        }

        if ($("#RegRecinputrecipename").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#RegRecinputrecipename").focus();
        }
        else {

            $("#btnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeSyncRequest(CookBookMasters.SaveRegionRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit').removeAttr("disabled");

                    toastr.error("Some error occured, please try again");
                    $("#RegRecinputrecipename").focus();
                }
                else {
                    $(".k-overlay").hide();

                    Toast.fire({
                        type: 'success',
                        title: 'Changes have been saved. Click Publish to make the Recipe available for Dish mapping'
                    });

                    $('#btnSubmit').removeAttr("disabled");


                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();

                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });
    $("#btnPublish").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie
        if (!validateDataBeforeSave("gridBaseRecipe", 'gridMOG')) {
            toastr.error("Invalid Data!");
            return false;
        }
        if (!validationPerCost()) {
            return false;
        }
        var rName = $("#RegRecinputrecipename").val();
        var rAlias = $("#RegRecinputrecipealiasname").val();
        if (rName === rAlias) {
            Utility.UnLoading();
            toastr.error("Recipe Name and Recipe Alias should be different");
            return;
        }

        if (user.SectorNumber == "20" ){

            if ($("#inputrecipecum").val() == null || $("#inputrecipecum").val() == "" || $("#inputrecipecum").val() == undefined || $("#inputrecipecum").val() == "Select") {
                toastr.error("Please select Recipe UOM Category");
                Utility.UnLoading();
                return false;
            }
        }

        if ($("#RegRecinputuom").val() == null || $("#RegRecinputuom").val() == "" || $("#RegRecinputuom").val() == undefined) {
            toastr.error("Please select UOM");
            Utility.UnLoading();
            return false;
        }

        if ($("#RegRecinputquantity").val() == null || $("#RegRecinputquantity").val() == "" || $("#RegRecinputquantity").val() == undefined || $("#RegRecinputquantity").val() == 0
            || $("#RegRecinputquantity").val() == "Select") {
            toastr.error("Please enter Recipe Quantity");
            Utility.UnLoading();
            return false;
        }
        var baseRecipes = [];
        if (user.SectorNumber != "20") {
            var brgrid = $("#gridBaseRecipe").data("kendoGrid").dataSource;
            var data = brgrid._data;

            for (var i = 0; i < brgrid._data.length; i++) {
                var obj = {
                    "ID": data[i].ID,
                    "IngredientPerc": data[i].IngredientPerc,
                    "RecipeCode": $("#RegRecinputrecipecode").val(),
                    "Recipe_ID": Recipe_ID,
                    "BaseRecipe_ID": data[i].BaseRecipe_ID,
                    "UOM_ID": data[i].UOM_ID,
                    "CostPerUOM": data[i].CostPerUOM,
                    "CostPerKG": data[i].CostPerKG,
                    "TotalCost": data[i].TotalCost,
                    "Quantity": data[i].Quantity,
                    "IsActive": 1,
                    "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                    "CreatedBy": data[i].CreatedBy,
                    "Region_ID": regionCode

                }
                baseRecipes.push(obj);
            }
        }
        //MOG
        var mogGrid = $("#gridMOG").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var obj = {
                "ID": data[i].ID,
                "RecipeCode": $("#RegRecinputrecipecode").val(),
                "Recipe_ID": Recipe_ID,
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "DKgValue": data[i].DKgTotal,
                "IsActive": 1,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy,
                "Region_ID": regionCode,
                "IngredientPerc": data[i].IngredientPerc,

            }
            mogs.push(obj);
        }



        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#RegRecinputuom").val();
        });
        ////Top Level Actual Recipie
        var model = {
            "ID": $("#RegRecrecipeid").val(),
            "RecipeCode": $("#RegRecinputrecipecode").val(),
            "CostPerKG": $("#grandCostPerKG").text().substring(1).replace(",", ""),
            "TotalCost": $('#grandTotal').text().substring(1).replace(",", ""),
            "Quantity": $('#RegRecinputquantity').val(),
            "Instructions": $("#RegRecinputinstruction").val(),
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy,
            "ModifiedOn": Utility.CurrentDate(),
            "ModifiedBy": user.UserId,
            "Region_ID": regionCode,
            "UOM_ID": $("#RegRecinputuom").val(),
            "UOMCode": objUOM[0].UOMCode,
            "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum").val() : "",
            "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit").val() : "",
            "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom").val() : "",
            "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit").val() : "",
            "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom").val() : "",
            "TotalIngredientWeight": $("#ToTIngredients").text(),
        }

        if ($("#RegRecinputrecipename").val() === "") {

            toastr.error("Please provide valid input(s)");
            $("#RegRecinputrecipename").focus();
        }
        else {


            $("#btnPublish").attr('disabled', 'disabled');
            HttpClient.MakeSyncRequest(CookBookMasters.SaveRegionRecipeData, function (result) {
                if (result == false) {
                    $('#btnPublish').removeAttr("disabled");

                    toastr.error("Some error occured, please try again");
                    $("#RegRecinputrecipename").focus();
                }
                else {
                    $(".k-overlay").hide();

                    $('#btnPublish').removeAttr("disabled");

                    toastr.success("Recipe configuration updated successfully");
                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();
                    $("#mainContent").show();

                    $("#windowEdit").hide();

                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });
});

//Hare Ram
function onBRDDLChange(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    //baserecipedatafiltered = baserecipedata.filter(function (recipeObj) {
    //    return recipeObj.BaseRecipe_ID != e.sender.value();
    //});

    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    // $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onBRDDLChange0(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe0', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe0").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;

};


function onBRDDLChange1(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe1', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe1").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    // //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onBRDDLChange2(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe2', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe2").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    // //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};

function onBRDDLChange3(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe3', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe3").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};

function onBRDDLChange4(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe4', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe4").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};

function onBRDDLChange5(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe5', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe5").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};



function onMOGDDLChange0(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG0', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG0").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;

};
function populateUOMDropdown0() {
    $("#RegRecinputuom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 1,
        enable: false,
    });
}
function populateBaseDropdown0() {
    $("#RegRecinputbase0").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,

    });
}

function mogtotalQTYDelete(level) {

    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var gTotal = 0.0;
    var len1 = grid._data.length;
    for (i = 0; i < len1; i++) {
        var totalCostMOG = user.SectorNumber == "20" ? grid._data[i].DKgValue * grid._data[i].Quantity : grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {
        $("#totmog").text(Utility.MathRound(gTotal, 2));
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
        if (level == "" || levle == "0") {
            $("#ToTIngredients" + level).text(Utility.MathRound(tot, 2));
        }
    }

}

function BaseRecipetotalQTYDelete(level) {

    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");

    var gTotal = 0.0;
    var len1 = grid._data.length;

    for (i = 0; i < len1; i++) {
        var totalCostMOG = user.SectorNumber == "20" ? grid._data[i].DKgValue * grid._data[i].Quantity : grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {

        $("#totBaseRecpQty").text(Utility.MathRound(gTotal, 2));
        // parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))).toFixed(2);
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))
        if (level == "" || levle == "0") {
            $("#ToTIngredients" + level).text(Utility.MathRound(tot, 2));
        }
    }


}

function calculateItemTotal0(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal0').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity0').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG0').html(Utility.MathRound(costPerKG, 2));
}



function calculateItemTotalMOG0(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG0").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal0').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity0').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG0').html(Utility.MathRound(costPerKG, 2));
}



function populateBaseRecipeGrid5(recipeCode) {

   
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe5");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, {
            name: "create", text: "Add"
        },
            //{
            //  name: "new", text: "Create New"
            // },

        ],

        groupable: false,
        editable: false,


        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='RecipeRegshowDetails6(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange
                        });
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Region Qty", width: "35px",
                template: '# if (IsMajor == true) {#<div><input class="inputbrqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputbrqty"  name="Quantity" style="background-color:transparent!important; border:1px solid transparent!important" readonly oninput="calculateItemTotal(this)" /></div>#}#',
                // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorIngredientPerc", title: "Qty Change %", width: "50px", format: "{0:0.00}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= (SectorIngredientPerc-IngredientPerc).toFixed(2)# </div>#} #',
            },
            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "SectorTotalCost", title: "Sector Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "sectortotalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetRegionBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe5").css("display", "block");
                            $("#emptybr5").css("display", "none");
                        } else {
                            $("#gridBaseRecipe5").css("display", "none");
                            $("#emptybr5").css("display", "block");
                        }

                    },
                        {
                            regionID: $("#ddlRegionMaster").val(),
                            recipeCode: recipeCode,
                            subSectorCode: subSectorCodeDD
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange5
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });

    var toolbar = $("#gridBaseRecipe5").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");
    cancelChangesConfirmation('#gridBaseRecipe5');
    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe5 .k-grid-content").addClass("gridInside");




}

function populateBaseRecipeGrid4(recipeCode) {

    
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe4");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, {
            name: "create", text: "Add"
        },
        {
            name: "new", text: "Create New"
        },

        ],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='RecipeRegshowDetails5(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange
                        });
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Region Qty", width: "35px",
                template: '# if (IsMajor == true) {#<div><input class="inputbrqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputbrqty"  name="Quantity" style="background-color:transparent!important; border:1px solid transparent!important" readonly oninput="calculateItemTotal(this)" /></div>#}#',
                // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorIngredientPerc", title: "Qty Change %", width: "50px", format: "{0:0.00}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= (SectorIngredientPerc-IngredientPerc).toFixed(2)# </div>#} #',
            },
            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "SectorTotalCost", title: "Sector Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "sectortotalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetRegionBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe4").css("display", "block");
                            $("#emptybr4").css("display", "none");
                        } else {
                            $("#gridBaseRecipe4").css("display", "none");
                            $("#emptybr4").css("display", "block");
                        }

                    },
                        {
                            regionID: $("#ddlRegionMaster").val(),
                            recipeCode: recipeCode, subSectorCode: subSectorCodeDD
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange4
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });

    var toolbar = $("#gridBaseRecipe4").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");
    cancelChangesConfirmation('#gridBaseRecipe4');
    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe4 .k-grid-content").addClass("gridInside");




}

function populateBaseRecipeGrid3(recipeCode) {

   
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe3");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }
        ],

        groupable: false,
        editable: false,


        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='RecipeRegshowDetails4(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange
                        });
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Region Qty", width: "35px",
                template: '# if (IsMajor == true) {#<div><input class="inputbrqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputbrqty"  name="Quantity" style="background-color:transparent!important; border:1px solid transparent!important" readonly oninput="calculateItemTotal(this)" /></div>#}#',
                // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorIngredientPerc", title: "Qty Change %", width: "50px", format: "{0:0.00}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= (SectorIngredientPerc-IngredientPerc).toFixed(2)# </div>#} #',
            },
            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "SectorTotalCost", title: "Sector Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "sectortotalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetRegionBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe3").css("display", "block");
                            $("#emptybr3").css("display", "none");
                        } else {
                            $("#gridBaseRecipe3").css("display", "none");
                            $("#emptybr3").css("display", "block");
                        }

                    },
                        {
                            regionID: $("#ddlRegionMaster").val(),
                            recipeCode: recipeCode, subSectorCode: subSectorCodeDD
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange2
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });

    var toolbar = $("#gridBaseRecipe3").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");
    cancelChangesConfirmation('#gridBaseRecipe3');
    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe3 .k-grid-content").addClass("gridInside");



}

function populateBaseRecipeGrid2(recipeCode) {

   
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe2");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }
        ],

        groupable: false,
        editable: false,


        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='RecipeRegshowDetails3(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange
                        });
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Region Qty", width: "35px",
                template: '# if (IsMajor == true) {#<div><input class="inputbrqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputbrqty"  name="Quantity" style="background-color:transparent!important; border:1px solid transparent!important" readonly oninput="calculateItemTotal(this)" /></div>#}#',
                // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorIngredientPerc", title: "Qty Change %", width: "50px", format: "{0:0.00}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= (SectorIngredientPerc-IngredientPerc).toFixed(2)# </div>#} #',
            },
            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "SectorTotalCost", title: "Sector Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "sectortotalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetRegionBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe2").css("display", "block");
                            $("#emptybr2").css("display", "none");
                        } else {
                            $("#gridBaseRecipe2").css("display", "none");
                            $("#emptybr2").css("display", "block");
                        }

                    },
                        {
                            regionID: $("#ddlRegionMaster").val(),
                            recipeCode: recipeCode, subSectorCode: subSectorCodeDD
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange2
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });

    var toolbar = $("#gridBaseRecipe2").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");
    cancelChangesConfirmation('#gridBaseRecipe2');
    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe2 .k-grid-content").addClass("gridInside");



}

function populateBaseRecipeGrid1(recipeCode) {

   
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe1");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" },
        ],

        groupable: false,
        editable: false,

        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='RecipeRegshowDetails2(this)'></i>`;
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "BaseRecipeName",
                            dataValueField: "BaseRecipe_ID",
                            autoBind: true,
                            dataSource: baserecipedata,
                            value: options.field,
                            change: onBRDDLChange
                        });
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Region Qty", width: "35px",
                template: '# if (IsMajor == true) {#<div><input class="inputbrqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputbrqty"  name="Quantity" style="background-color:transparent!important; border:1px solid transparent!important" readonly oninput="calculateItemTotal(this)" /></div>#}#',
                // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',

                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorIngredientPerc", title: "Qty Change %", width: "50px", format: "{0:0.00}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= (SectorIngredientPerc-IngredientPerc).toFixed(2)# </div>#} #',
            },
            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "SectorTotalCost", title: "Sector Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "sectortotalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            }
        ],
        ////  columns: [
        //      {
        //          field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
        //          attributes: {
        //              style: "text-align: left; font-weight:normal"
        //          },
        //          template: function (dataItem) {
        //              var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
        //                  + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='RecipeRegshowDetails2(this)'></i>`;
        //              return html;
        //          },
        //          editor: function (container, options) {
        //              $('<input class="brTemplate" id="ddlBaseRecipe" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
        //                  .appendTo(container)
        //                  .kendoDropDownList({
        //                      enable: false,
        //                      filter: "contains",
        //                      dataTextField: "BaseRecipeName",
        //                      dataValueField: "BaseRecipe_ID",
        //                      autoBind: true,
        //                      dataSource: baserecipedata,
        //                      value: options.field,
        //                      change: onBRDDLChange1
        //                  });
        //          }
        //      },
        //    {
        //        field: "Quantity", title: "Quantity", width: "35px",
        //        template: '# if (IsMajor == true) {#<div><input class="inputbrqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#} else {#<div><input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)" readonly/></div>#}#',

        //        // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',


        //        headerAttributes: {
        //            style: "text-align: center;"
        //        },
        //        attributes: {
        //            style: "text-align: center; font-weight:normal"
        //        },
        //    },
        //    {
        //        field: "UOMName", title: "UOM", width: "50px",
        //        headerAttributes: {
        //            style: "text-align: center;"
        //        },
        //        attributes: {
        //            class: "uomname",
        //            style: "text-align: center; font-weight:normal"
        //        },
        //    },
        //    {
        //        field: "IngredientPerc", title: "Region%", width: "50px", format: "{0:0.00}", editable: false,
        //        headerAttributes: {
        //            style: "text-align: center;"
        //        },
        //        attributes: {
        //            class: "minor",
        //            style: "text-align: center; font-weight:normal"
        //        },
        //        //template: '# if (IsMajor == true) {#<div>#= IngredientPerc# %</div>#} else {#<div>-</div>#}#',
        //    },

        //    {
        //        field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.00}", editable: false,
        //        headerAttributes: {
        //            style: "text-align: center;"
        //        },
        //        attributes: {
        //            class: "si",
        //            style: "text-align: center; font-weight:normal"
        //        }
        //    },

        //    {
        //        field: "SectorIngredientPerc", title: "Change %", width: "50px", format: "{0:0.00}", editable: false,
        //        headerAttributes: {
        //            style: "text-align: center;"
        //        },
        //        attributes: {
        //            class: "change",
        //            style: "text-align: center; font-weight:normal"
        //        },
        //        template: '#  {#<div>#= SectorIngredientPerc-IngredientPerc# </div>#} #',
        //    },


        //    {
        //        field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false, format: Utility.Cost_Format,
        //        headerAttributes: {

        //            style: "text-align: right"
        //        },
        //        attributes: {
        //            class: "uomcost",
        //            style: "text-align: right; font-weight:normal"
        //        },
        //    },
        //    {
        //        field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
        //        headerAttributes: {

        //            style: "text-align: right"
        //        },
        //        attributes: {
        //            class: "totalcost",
        //            style: "text-align: right; font-weight:normal"
        //        },
        //    },
        //    {
        //        field: "SectorTotalCost", title: "Sector Cost", width: "50px", editable: false, format: Utility.Cost_Format,
        //        headerAttributes: {

        //            style: "text-align: right"
        //        },
        //        attributes: {
        //            class: "sectortotalcost",
        //            style: "text-align: right; font-weight:normal"
        //        },
        //    },
        //],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetRegionBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe1").css("display", "block");
                            $("#emptybr1").css("display", "none");
                        } else {
                            $("#gridBaseRecipe1").css("display", "none");
                            $("#emptybr1").css("display", "block");
                        }

                    },
                        {
                            regionID: $("#ddlRegionMaster").val(),
                            recipeCode: recipeCode, subSectorCode: subSectorCodeDD
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });

    var toolbar = $("#gridBaseRecipe1").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");
    cancelChangesConfirmation('#gridBaseRecipe1');
    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe1 .k-grid-content").addClass("gridInside");




}


function onMOGDDLChange5(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG5', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG5").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onMOGDDLChange4(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG4', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG4").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onMOGDDLChange3(e) {

    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG3', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG3").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onMOGDDLChange2(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG2', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG2").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function onMOGDDLChange1(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG1', false)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlMOG1").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);
    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = UnitCost;
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    //   $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};
function populateMOGGrid0() {

    Utility.Loading();
    var gridVariable = $("#gridMOG0");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: false,

        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange0,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "Quantity", title: "Quantity", width: "60px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                editor: function (container, options) {
                    $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" onchange="calculateItemTotalMOG0(this)"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            spinners: false,
                            decimals: 4
                        });
                },
                //template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    onchange="calculateItemTotalMOG(this)"/>',
                attributes: {

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px",
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //                       {
                //        name: 'View APL',
                //        click: function (e) {
                //            var gridObj = $("#gridMOG0").data("kendoGrid");
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            MOGName = tr.MOGName;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData(MOGCode);
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        dataSource: {
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: mogdata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange0
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    var toolbar = $("#gridMOG0").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");
    cancelChangesConfirmation('#gridMOG0');
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG0 .k-grid-content").addClass("gridInside");
}


function populateUOMDropdown1() {
    $("#RegRecinputuom1").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}
function populateBaseDropdown1() {
    $("#RegRecinputbase1").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}
function populateUOMDropdown2() {
    $("#RegRecinputuom2").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}
function populateBaseDropdown2() {
    $("#RegRecinputbase2").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}
function populateUOMDropdown3() {
    $("#RegRecinputuom3").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}
function populateBaseDropdown3() {
    $("#RegRecinputbase3").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}
function populateUOMDropdown4() {
    $("#RegRecinputuom4").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}
function populateBaseDropdown4() {
    $("#RegRecinputbase4").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        enable: false,
    });
}
function populateUOMDropdown5() {
    $("#RegRecinputuom5").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        enable: false,
    });
}
function populateBaseDropdown5() {
    $("#RegRecinputbase5").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,

        enable: false
    });
}

function calculateItemTotal1(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal1').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity1').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG1').html(Utility.MathRound(costPerKG, 2));
}

function calculateItemTotal2(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal2').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity2').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG2').html(Utility.MathRound(costPerKG, 2));
}


function calculateItemTotal3(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal3').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity3').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG3').html(Utility.MathRound(costPerKG, 2));
}

function calculateItemTotal4(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal4').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity4').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG4').html(Utility.MathRound(costPerKG, 2));
}


function calculateItemTotal5(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal5').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity5').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG5').html(Utility.MathRound(costPerKG, 2));
}

function calculateItemTotalMOG1(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG1").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal1').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity1').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG1').html(Utility.MathRound(costPerKG, 2));
}

function calculateItemTotalMOG2(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG2").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal2').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity2').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG2').html(Utility.MathRound(costPerKG, 2));
}

function calculateItemTotalMOG3(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG3").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal3').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity3').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG3').html(Utility.MathRound(costPerKG, 2));
}

function calculateItemTotalMOG4(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG4").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal4').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity4').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG4').html(Utility.MathRound(costPerKG, 2));
}

function calculateItemTotalMOG5(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG5").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal5').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity5').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG5').html(Utility.MathRound(costPerKG, 2));
}

function populateMOGGrid1(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG1");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }],
        groupable: false,
        editable: false,
        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Region Qty", width: "50px",
                template: '# if (true) {#<div><input class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#}#',

                // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            //{
            //    field: "IngredientPerc", title: "Region%", width: "50px", format: "{0:0.00}", editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "minor",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    //template: '# if (IsMajor == true) {#<div>#= IngredientPerc# %</div>#} else {#<div>-</div>#}#',
            //},

            {
                field: "SectorIngredientPerc", title: "Qty Change % ", width: "50px", format: "{0:0.00}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= ((Quantity-SectorQuantity)*100/SectorQuantity).toFixed(2)# </div>#} #',
            },


            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "SectorTotalCost", title: "Sector  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "sectortotalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    field: "SectorTotalCost", title: "Change Cost % ", width: "50px", format: "{0:0.00}", editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "costchange",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    template: '#  {#<div>#= ((TotalCost - SectorTotalCost)*100/SectorTotalCost).toFixed(2)# </div>#} #',
            //},
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //    {
                //        name: 'View APL',
                //        click: function (e) {
                //            var gridObj = $("#gridMOG1").data("kendoGrid");
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            console.log("MOG APL");
                //            console.log(tr);
                //            MOGName = tr.MOGName;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData(MOGCode);
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetRegionMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG1").css("display", "block");
                            $("#emptymog1").css("display", "none");
                        } else {
                            $("#gridMOG1").css("display", "none");
                            $("#emptymog1").css("display", "block");
                        }
                    },
                        {
                            regionID: $("#ddlRegionMaster").val(),
                            recipeCode: recipeCode, subSectorCode: subSectorCodeDD
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange1
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    var toolbar = $("#gridMOG1").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    cancelChangesConfirmation('#gridMOG1');
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG1 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid2(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG2");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: false,
        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Region Qty", width: "50px",
                template: '# if (true) {#<div><input class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#}#',

                // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            //{
            //    field: "IngredientPerc", title: "Region%", width: "50px", format: "{0:0.00}", editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "minor",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    //template: '# if (IsMajor == true) {#<div>#= IngredientPerc# %</div>#} else {#<div>-</div>#}#',
            //},

            {
                field: "SectorIngredientPerc", title: "Qty Change % ", width: "50px", format: "{0:0.00}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= ((Quantity-SectorQuantity)*100/SectorQuantity).toFixed(2)# </div>#} #',
            },


            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "SectorTotalCost", title: "Sector  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "sectortotalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    field: "SectorTotalCost", title: "Change Cost % ", width: "50px", format: "{0:0.00}", editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "costchange",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    template: '#  {#<div>#= ((TotalCost - SectorTotalCost)*100/SectorTotalCost).toFixed(2)# </div>#} #',
            //},
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //    {
                //        name: 'View APL',
                //        click: function (e) {
                //            var gridObj = $("#gridMOG2").data("kendoGrid");
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            console.log("MOG APL");
                //            console.log(tr);
                //            MOGName = tr.MOGName;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData(MOGCode);
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        //columns: [
        //    {
        //        field: "MOG_ID", title: "MOG", width: "120px",
        //        attributes: {
        //            style: "text-align: left; font-weight:normal"
        //        },
        //        template: function (dataItem) {
        //            var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
        //            return html;
        //        },
        //        editor: function (container, options) {

        //            $('<input class="mogTemplate" id="ddlMOG2" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
        //                .appendTo(container)
        //                .kendoDropDownList({
        //                   enable: false,
        //                    filter: "contains",
        //                    dataTextField: "MOGName",
        //                    dataValueField: "MOG_ID",
        //                    autoBind: true,
        //                    dataSource: mogdata,
        //                    value: options.field,
        //                    change: onMOGDDLChange2,
        //                });
        //            // container.find(".k-input")[0].text = options.model.MOGName;
        //        }
        //    },
        //    {
        //        field: "Quantity", title: "Quantity", width: "35px",
        //        headerAttributes: {
        //            style: "text-align: center;"
        //        },
        //        editor: function (container, options) {
        //            $('<input data-bind="value:' + options.field + '" class="inputmogqty"  name="Quantity" onchange="calculateItemTotalMOG2(this)"/>')
        //                .appendTo(container)
        //                .kendoNumericTextBox({
        //                    spinners: false,
        //                    decimals: 4
        //                });
        //        },
        //        // template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    onchange="calculateItemTotalMOG(this)"/>',
        //        attributes: {

        //            style: "text-align: center; font-weight:normal"
        //        },
        //    },
        //    {
        //        field: "UOMName", title: "UOM", width: "50px",
        //        headerAttributes: {
        //            style: "text-align: center;"
        //        },
        //        attributes: {

        //            class: 'uomname',
        //            style: "text-align: center; font-weight:normal"
        //        },
        //    },
        //    {
        //        field: "CostPerUOM", title: "Cost/UOM", width: "50px",
        //        headerAttributes: {
        //            style: "text-align: right;"
        //        },
        //        attributes: {
        //            class: "uomcost",

        //            style: "text-align: right; font-weight:normal"
        //        },
        //    },
        //    {
        //        field: "TotalCost", title: "Total Cost", width: "50px",
        //        headerAttributes: {
        //            style: "text-align: right;"
        //        },
        //        attributes: {
        //            class: "totalcost",
        //            style: "text-align: right; font-weight:normal"
        //        },
        //    },
        //    {
        //        title: "Action",
        //        width: "60px",
        //        headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
        //        headerAttributes: {
        //            style: "text-align: center;"
        //        },
        //        attributes: {
        //            style: "text-align: center; font-weight:normal;"
        //        },
        //        command: [
        //            {
        //                name: 'View APL',
        //                click: function (e) {
        //                    var gridObj = $("#gridMOG2").data("kendoGrid");
        //                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
        //                    datamodel = tr;
        //                    MOGName = tr.MOGName;
        //                    var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
        //                    $("#windowEditMOGWiseAPL").kendoWindow({
        //                        animation: false
        //                    });
        //                    $(".k-overlay").css("display", "block");
        //                    $(".k-overlay").css("opacity", "0.5");
        //                    dialog.open();
        //                    dialog.center();
        //                    dialog.title("MOG APL Details - " + MOGName);
        //                    MOGCode = tr.MOGCode;
        //                    mogWiseAPLMasterdataSource = [];
        //                    getMOGWiseAPLMasterData(MOGCode);
        //                    populateAPLMasterGrid();
        //                    return true;
        //                }
        //            }
        //        ],
        //    }
        //],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetRegionMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG2").css("display", "block");
                            $("#emptymog2").css("display", "none");
                        } else {
                            $("#gridMOG2").css("display", "none");
                            $("#emptymog2").css("display", "block");
                        }
                    },
                        {
                            regionID: $("#ddlRegionMaster").val(),
                            recipeCode: recipeCode, subSectorCode: subSectorCodeDD
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange2
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    var toolbar = $("#gridMOG2").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    cancelChangesConfirmation('#gridMOG2');
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG2 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid3(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG3");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: false,

        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Region Qty", width: "50px",
                template: '# if (true) {#<div><input class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#}#',

                // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            //{
            //    field: "IngredientPerc", title: "Region%", width: "50px", format: "{0:0.00}", editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "minor",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    //template: '# if (IsMajor == true) {#<div>#= IngredientPerc# %</div>#} else {#<div>-</div>#}#',
            //},

            {
                field: "SectorIngredientPerc", title: "Qty Change % ", width: "50px", format: "{0:0.00}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= ((Quantity-SectorQuantity)*100/SectorQuantity).toFixed(2)# </div>#} #',
            },


            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "SectorTotalCost", title: "Sector  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "sectortotalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    field: "SectorTotalCost", title: "Change Cost % ", width: "50px", format: "{0:0.00}", editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "costchange",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    template: '#  {#<div>#= ((TotalCost - SectorTotalCost)*100/SectorTotalCost).toFixed(2)# </div>#} #',
            //},
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //    {
                //        name: 'View APL',
                //        click: function (e) {
                //            var gridObj = $("#gridMOG3").data("kendoGrid");
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            console.log("MOG APL");
                //            console.log(tr);
                //            MOGName = tr.MOGName;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData(MOGCode);
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetRegionMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG3").css("display", "block");
                            $("#emptymog3").css("display", "none");
                        } else {
                            $("#gridMOG3").css("display", "none");
                            $("#emptymog3").css("display", "block");
                        }
                    },
                        {
                            regionID: $("#ddlRegionMaster").val(),
                            recipeCode: recipeCode, subSectorCode: subSectorCodeDD
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange3
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    var toolbar = $("#gridMOG3").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    cancelChangesConfirmation('#gridMOG3');
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG3 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid4(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG4");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: false,
        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Region Qty", width: "50px",
                template: '# if (true) {#<div><input class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#}#',

                // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            //{
            //    field: "IngredientPerc", title: "Region%", width: "50px", format: "{0:0.00}", editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "minor",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    //template: '# if (IsMajor == true) {#<div>#= IngredientPerc# %</div>#} else {#<div>-</div>#}#',
            //},

            {
                field: "SectorIngredientPerc", title: "Qty Change % ", width: "50px", format: "{0:0.00}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= ((Quantity-SectorQuantity)*100/SectorQuantity).toFixed(2)# </div>#} #',
            },


            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "SectorTotalCost", title: "Sector  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "sectortotalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    field: "SectorTotalCost", title: "Change Cost % ", width: "50px", format: "{0:0.00}", editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "costchange",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    template: '#  {#<div>#= ((TotalCost - SectorTotalCost)*100/SectorTotalCost).toFixed(2)# </div>#} #',
            //},
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //    {
                //        name: 'View APL',
                //        click: function (e) {
                //            var gridObj = $("#gridMOG4").data("kendoGrid");
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            console.log("MOG APL");
                //            console.log(tr);
                //            MOGName = tr.MOGName;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData(MOGCode);
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetRegionMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG4").css("display", "block");
                            $("#emptymog4").css("display", "none");
                        } else {
                            $("#gridMOG4").css("display", "none");
                            $("#emptymog4").css("display", "block");
                        }
                    },
                        {
                            regionID: $("#ddlRegionMaster").val(),
                            recipeCode: recipeCode, subSectorCode: subSectorCodeDD
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange4
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    var toolbar = $("#gridMOG4").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    cancelChangesConfirmation('#gridMOG4');
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG4 .k-grid-content").addClass("gridInside");
}

function populateMOGGrid5(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG5");
    gridVariable.html("");
    gridVariable.kendoGrid({
        toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
        groupable: false,
        editable: false,

        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {
                    $('<input class="mogTemplate" id="ddlMOG" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            enable: false,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange,
                        });
                    // container.find(".k-input")[0].text = options.model.MOGName;
                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "SectorQuantity", title: "Sector Qty", width: "50px", format: "{0:0.000}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "si",
                    style: "text-align: center; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Region Qty", width: "50px",
                template: '# if (true) {#<div><input class="inputmogqty"  style="background-color:transparent!important; border:1px solid transparent!important" name="Quantity" readonly/></div>#}#',

                // template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this)"/>',


                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
            },

            //{
            //    field: "IngredientPerc", title: "Region%", width: "50px", format: "{0:0.00}", editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "minor",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    //template: '# if (IsMajor == true) {#<div>#= IngredientPerc# %</div>#} else {#<div>-</div>#}#',
            //},

            {
                field: "SectorIngredientPerc", title: "Qty Change % ", width: "50px", format: "{0:0.00}", editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "change",
                    style: "text-align: center; font-weight:normal"
                },
                template: '#  {#<div>#= ((Quantity-SectorQuantity)*100/SectorQuantity).toFixed(2)# </div>#} #',
            },


            {
                field: "CostPerUOM", title: "CpKg", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "SectorTotalCost", title: "Sector  Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "sectortotalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Region Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    field: "SectorTotalCost", title: "Change Cost % ", width: "50px", format: "{0:0.00}", editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "costchange",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    template: '#  {#<div>#= ((TotalCost - SectorTotalCost)*100/SectorTotalCost).toFixed(2)# </div>#} #',
            //},
            {
                title: "Action",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                //command: [
                //    {
                //        name: 'View APL',
                //        click: function (e) {
                //            var gridObj = $("#gridMOG").data("kendoGrid");
                //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //            datamodel = tr;
                //            console.log("MOG APL");
                //            console.log(tr);
                //            MOGName = tr.MOGName;
                //            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                //            $(".k-overlay").css("display", "block");
                //            $(".k-overlay").css("opacity", "0.5");
                //            dialog.open();
                //            dialog.center();
                //            dialog.title("MOG APL Details - " + MOGName);
                //            MOGCode = tr.MOGCode;
                //            mogWiseAPLMasterdataSource = [];
                //            getMOGWiseAPLMasterData(MOGCode);
                //            populateAPLMasterGrid();
                //            return true;
                //        }
                //    }
                //],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetRegionMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG5").css("display", "block");
                            $("#emptymog5").css("display", "none");
                        } else {
                            $("#gridMOG5").css("display", "none");
                            $("#emptymog5").css("display", "block");
                        }
                    },
                        {
                            regionID: $("#ddlRegionMaster").val(),
                            recipeCode: recipeCode, subSectorCode: subSectorCodeDD
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { type: "number", validation: { required: true } },
                        UOMName: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    enable: false,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange5
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    var toolbar = $("#gridMOG5").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    cancelChangesConfirmation('#gridMOG5');
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG5 .k-grid-content").addClass("gridInside");
}





function RecipeRegshowDetails1(e) {



    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe0").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#RecipeReConfgwindowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.RecipeCode);
    populateMOGGrid1(tr.RecipeCode);

    $("#RegRecrecipeid1").val(tr.ID);
    $("#RegRecinputrecipecode1").val(tr.RecipeCode);
    $("#RegRecinputrecipename1").val(tr.Name);
    $("#RegRecinputrecipealiasname1").val(tr.RecipeAlias);
    $("#RegRecinputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#RegRecinputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#RegRecinputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#RegRecinputbase1").data('kendoDropDownList').value("No");
    $('#grandTotal1').html(Utility.MathRound(tr.TotalCost, 2));
    $('#grandCostPerKG1').html(Utility.MathRound(tr.CostPerKG2));
    var editor = $("#RegRecinputinstruction1").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#RegRecinputrecipename1").removeAttr('disabled');
    //    $("#RegRecinputuom1").removeAttr('disabled');
    //    $("#RegRecinputquantity1").removeAttr('disabled');
    //    $("#RegRecinputbase1").removeAttr('disabled');
    //    $("#btnSubmit1").css('display', 'inline');
    //    $("#btnCancel1").css('display', 'inline');
    //} else {
    //    $("#RegRecinputrecipename1").attr('disabled', 'disabled');
    //    $("#RegRecinputuom1").attr('disabled', 'disabled');
    //    $("#RegRecinputquantity1").attr('disabled', 'disabled');
    //    $("#RegRecinputbase1").attr('disabled', 'disabled');
    //    $("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details Inline - " + RecipeName);
    return true;

}

function RecipeRegshowDetails2(e) {
    $("#RecipeReConfgwindowEdit1").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe1").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    // $("#RecipeReConfgwindowEdit1").parent('.k-widget').css("display", "none");
    var dialog = $("#RecipeReConfgwindowEdit2").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown2();
    populateBaseDropdown2();
    populateBaseRecipeGrid2(tr.RecipeCode);
    populateMOGGrid2(tr.RecipeCode);

    $("#RegRecrecipeid2").val(tr.ID);
    $("#RegRecinputrecipecode2").val(tr.RecipeCode);
    $("#RegRecinputrecipename2").val(tr.Name);
    $("#inputrecipealiasname2").val(tr.RecipeAlias);
    $("#RegRecinputuom2").data('kendoDropDownList').value(tr.UOM_ID);
    $("#RegRecinputquantity2").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#RegRecinputbase2").data('kendoDropDownList').value("Yes");
    else
        $("#RegRecinputbase2").data('kendoDropDownList').value("No");
    $('#grandTotal2').html(Utility.MathRound(tr.TotalCost, 2));
    $('#grandCostPerKG2').html(Utility.MathRound(tr.CostPerKG, 2));
    var editor = $("#RegRecinputinstruction2").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    recipeCategoryHTMLPopulateOnEditRecipe("2", tr);

    //if (user.UserRoleId === 1) {
    //    $("#RegRecinputrecipename2").removeAttr('disabled');
    //    $("#RegRecinputuom2").removeAttr('disabled');
    //    $("#RegRecinputquantity2").removeAttr('disabled');
    //    $("#RegRecinputbase2").removeAttr('disabled');
    //    $("#btnSubmit2").css('display', 'inline');
    //    $("#btnCancel2").css('display', 'inline');
    //} else {
    //    $("#RegRecinputrecipename2").attr('disabled', 'disabled');
    //    $("#RegRecinputuom2").attr('disabled', 'disabled');
    //    $("#RegRecinputquantity2").attr('disabled', 'disabled');
    //    $("#RegRecinputbase2").attr('disabled', 'disabled');
    //    $("#btnSubmit2").css('display', 'none');
    //    $("#btnCancel2").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}

function RecipeRegshowDetails3(e) {
    $("#RecipeReConfgwindowEdit2").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe2").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error3").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#RecipeReConfgwindowEdit3").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown3();
    populateBaseDropdown3();
    populateBaseRecipeGrid3(tr.RecipeCode);
    populateMOGGrid3(tr.RecipeCode);
    $("#inputrecipealiasname3").val(tr.RecipeAlias);
    $("#RegRecrecipeid3").val(tr.ID);
    $("#RegRecinputrecipecode3").val(tr.RecipeCode);
    $("#RegRecinputrecipename3").val(tr.Name);
    $("#RegRecinputuom3").data('kendoDropDownList').value(tr.UOM_ID);
    $("#RegRecinputquantity3").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#RegRecinputbase3").data('kendoDropDownList').value("Yes");
    else
        $("#RegRecinputbase3").data('kendoDropDownList').value("No");
    $('#grandTotal3').html(Utility.MathRound(tr.TotalCost, 2));
    $('#grandCostPerKG3').html(Utility.MathRound(tr.CostPerKG, 2));
    var editor = $("#RegRecinputinstruction3").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    recipeCategoryHTMLPopulateOnEditRecipe("3", tr);
    //if (user.UserRoleId === 1) {
    //    $("#RegRecinputrecipename3").removeAttr('disabled');
    //    $("#RegRecinputuom3").removeAttr('disabled');
    //    $("#RegRecinputquantity3").removeAttr('disabled');
    //    $("#RegRecinputbase3").removeAttr('disabled');
    //    $("#btnSubmit3").css('display', 'inline');
    //    $("#btnCancel3").css('display', 'inline');
    //} else {
    //    $("#RegRecinputrecipename3").attr('disabled', 'disabled');
    //    $("#RegRecinputuom3").attr('disabled', 'disabled');
    //    $("#RegRecinputquantity3").attr('disabled', 'disabled');
    //    $("#RegRecinputbase3").attr('disabled', 'disabled');
    //    $("#btnSubmit3").css('display', 'none');
    //    $("#btnCancel3").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}


function RecipeRegshowDetails4(e) {
    $("#RecipeReConfgwindowEdit3").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe3").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error4").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#RecipeReConfgwindowEdit4").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
    $("#inputrecipealiasname4").val(tr.RecipeAlias);
    populateUOMDropdown4();
    populateBaseDropdown4();
    populateBaseRecipeGrid4(tr.RecipeCode);
    populateMOGGrid4(tr.RecipeCode);

    $("#RegRecrecipeid4").val(tr.ID);
    $("#RegRecinputrecipecode4").val(tr.RecipeCode);
    $("#RegRecinputrecipename4").val(tr.Name);
    $("#RegRecinputuom4").data('kendoDropDownList').value(tr.UOM_ID);
    $("#RegRecinputquantity4").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#RegRecinputbase4").data('kendoDropDownList').value("Yes");
    else
        $("#RegRecinputbase4").data('kendoDropDownList').value("No");
    $('#grandTotal4').html(Utility.MathRound(tr.TotalCost, 2));
    $('#grandCostPerKG4').html(Utility.MathRound(tr.CostPerKG, 2));

    var editor = $("#RegRecinputinstruction4").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    recipeCategoryHTMLPopulateOnEditRecipe("4", tr);
    //if (user.UserRoleId === 1) {
    //    $("#RegRecinputrecipename4").removeAttr('disabled');
    //    $("#RegRecinputuom4").removeAttr('disabled');
    //    $("#RegRecinputquantity4").removeAttr('disabled');
    //    $("#RegRecinputbase4").removeAttr('disabled');
    //    $("#btnSubmit4").css('display', 'inline');
    //    $("#btnCancel4").css('display', 'inline');
    //} else {
    //    $("#RegRecinputrecipename4").attr('disabled', 'disabled');
    //    $("#RegRecinputuom4").attr('disabled', 'disabled');
    //    $("#RegRecinputquantity4").attr('disabled', 'disabled');
    //    $("#RegRecinputbase4").attr('disabled', 'disabled');
    //    $("#btnSubmit4").css('display', 'none');
    //    $("#btnCancel4").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}

function RecipeRegshowDetails5(e) {
    $("#RecipeReConfgwindowEdit4").parent().hide();
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe4").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error4").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#RecipeReConfgwindowEdit5").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown5();
    populateBaseDropdown5();
    populateBaseRecipeGrid5(tr.RecipeCode);
    populateMOGGrid5(tr.RecipeCode);
    $("#inputrecipealiasname5").val(tr.RecipeAlias);
    $("#RegRecrecipeid5").val(tr.ID);
    $("#RegRecinputrecipecode5").val(tr.RecipeCode);
    $("#RegRecinputrecipename5").val(tr.Name);
    $("#RegRecinputuom5").data('kendoDropDownList').value(tr.UOM_ID);
    $("#RegRecinputquantity5").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#RegRecinputbase5").data('kendoDropDownList').value("Yes");
    else
        $("#RegRecinputbase5").data('kendoDropDownList').value("No");
    $('#grandTotal5').html(Utility.MathRound(tr.TotalCost, 2));
    $('#grandCostPerKG5').html(Utility.MathRound(tr.CostPerKG, 2));

    var editor = $("#RegRecinputinstruction5").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    recipeCategoryHTMLPopulateOnEditRecipe("5", tr);
    //if (user.UserRoleId === 1) {
    //    $("#RegRecinputrecipename5").removeAttr('disabled');
    //    $("#RegRecinputuom5").removeAttr('disabled');
    //    $("#RegRecinputquantity5").removeAttr('disabled');
    //    $("#RegRecinputbase5").removeAttr('disabled');
    //    $("#btnSubmit5").css('display', 'inline');
    //    $("#btnCancel5").css('display', 'inline');
    //} else {
    //    $("#RegRecinputrecipename5").attr('disabled', 'disabled');
    //    $("#RegRecinputuom5").attr('disabled', 'disabled');
    //    $("#RegRecinputquantity5").attr('disabled', 'disabled');
    //    $("#RegRecinputbase5").attr('disabled', 'disabled');
    //    $("#btnSubmit5").css('display', 'none');
    //    $("#btnCancel5").css('display', 'none');
    //}

    dialog.title("Recipe Details View - " + RecipeName);
    return true;

}


function RecipeRegshowDetails6(e) {
    alert("Close Previous then Proceed!!!");
}
function calculateItemTotal(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG').html(Utility.MathRound(costPerKG, 2));
}



function calculateItemTotalMOG(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOG").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = dataItem.TotalCost;
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    var gtotalObj = $(".totalcost");
    var len = $('.totalcost').length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObj[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObj[i].innerText);
    }
    if (!isNaN(gTotal)) {
        $('#grandTotal').html(Utility.MathRound(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity').val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG').html(Utility.MathRound(costPerKG, 2));
}


function showHideGrid(gridId, emptyGrid, uid) {
    var gridLength = $("#" + gridId).data().kendoGrid.dataSource.data().length;

    if (gridLength == 0) {
        $("#" + emptyGrid).toggle();
        $("#" + gridId).hide();
    }
    else {
        $("#" + emptyGrid).hide();
        $("#" + gridId).toggle();
    }
    // window.scrollBy(0, 100);
    if (!($("#" + gridId).is(":visible") || $("#" + emptyGrid).is(":visible"))) {
        $("#" + uid).addClass("bottomCurve");
        // $("#" + uid).parent(".lower").hide();
    } else {
        $("#" + uid).removeClass("bottomCurve");
        //  $("#" + gridId).parent(".lower").show();
    }

}

function hideGrid(gridId, emptyGrid) {

    $("#" + emptyGrid).hide();
    $("#" + gridId).show();
    //  $("#" + hideBtn).show();
    // $("#" + showBtn).hide();
}

function instructionTextEditor(divid) {

    $("#" + divid).kendoEditor({
        stylesheets: [
            // "../content/shared/styles/editor.css",
        ],
        tools: [
            "bold",
            "italic",
            "underline",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "insertUnorderedList",
            "tableWizard",
            "createTable",
            "addRowAbove",
            "addRowBelow",
            "addColumnLeft",
            "addColumnRight",
            "deleteRow",
            "deleteColumn",
            "mergeCellsHorizontally",
            "mergeCellsVertically",
            "splitCellHorizontally",
            "splitCellVertically",
            "createLink",
            "unlink",

            {
                name: "fontName",
                items: [
                    { text: "Andale Mono", value: "Andale Mono" },
                    { text: "Arial", value: "Arial" },
                    { text: "Arial Black", value: "Arial Black" },
                    { text: "Book Antiqua", value: "Book Antiqua" },
                    { text: "Comic Sans MS", value: "Comic Sans MS" },
                    { text: "Courier New", value: "Courier New" },
                    { text: "Georgia", value: "Georgia" },
                    { text: "Helvetica", value: "Helvetica" },
                    { text: "Impact", value: "Impact" },
                    { text: "Symbol", value: "Symbol" },
                    { text: "Tahoma", value: "Tahoma" },
                    { text: "Terminal", value: "Terminal" },
                    { text: "Times New Roman", value: "Times New Roman" },
                    { text: "Trebuchet MS", value: "Trebuchet MS" },
                    { text: "Verdana", value: "Verdana" },
                ]
            },
            "fontSize",
            "foreColor",
            "backColor",
        ]
    });

}


function decodeHTMLEntities(text) {
    if (text == null || text == '' || text == 'undefined')
        return "";
    var entities = [
        ['amp', '&'],
        ['apos', '\''],
        ['#x27', '\''],
        ['#x2F', '/'],
        ['#39', '\''],
        ['#47', '/'],
        ['lt', '<'],
        ['gt', '>'],
        ['nbsp', ' '],
        ['quot', '"']
    ];

    for (var i = 0, max = entities.length; i < max; ++i)
        text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1]);

    return text;
}
$("document").ready(function () {
    $("#intialImage").show();
    $("#btnGo").on("click", function () {
        Utility.Loading();
        setTimeout(function () {
            if (user.SectorNumber == 10 && isSubSectorApplicable) {
                subSectorCodeDD = $("#inputSubSector").val();
            }
            var dropdownlist = $("#ddlRegionMaster").data("kendoDropDownList");
            regionCode = dropdownlist.value();
            HttpClient.MakeRequest(CookBookMasters.GetMOGDataList, function (data) {
                var dataSource = data;

                mogdata = [];
                mogdata.push({ "MOG_ID": "Select MOG", "MOGName": "Select MOG", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "DKgValue": 0, "MOG_Code": "", "IsActive": false });

                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i].CostPerUOM == null) {
                        dataSource[i].CostPerUOM = 0;
                    }
                    if (dataSource[i].CostPerKG == null) {
                        dataSource[i].CostPerKG = 0;
                    }
                    if (dataSource[i].TotalCost == null) {
                        dataSource[i].TotalCost = 0;
                    }
                    if (dataSource[i].Alias != null && dataSource[i].Alias.trim() != "" && dataSource[i].Alias != dataSource[i].Name)
                        mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name + " (" + dataSource[i].Alias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "DKgValue": dataSource[i].DKgValue, "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive });
                    else
                        mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode, "DKgValue": dataSource[i].DKgValue, "IsActive": dataSource[i].IsActive });
                }

                mogdataList = mogdata;

            }, { regionID: regionCode }, true);

            populateRecipeGrid(regionCode);
            $("#searchspan").show();
            var regionName = dropdownlist.text();

            $("#location").text(" | " + regionName);
            $("#intialImage").hide();
            Utility.UnLoading();
        }, 2000);
    });

    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();

    });
});
function getMOGWiseAPLMasterData(MOGCode) {
    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG_Region, function (result) {
        Utility.UnLoading();

        if (result != null) {
            mogWiseAPLMasterdataSource = result;
        }
        else {
        }
    }, { mogCode: MOGCode, Region_ID: regionCode }
        , true);
}
function populateAPLMasterGrid() {
    Utility.Loading();


    var gridVariable = $("#gridMOGWiseAPL");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "APLMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Total: {2} records"
            }
        },
        groupable: false,
        scrollable: true,
        noRecords: {
            template: "No Records Available"
        },

        columns: [
            {
                field: "ArticleNumber", title: "Article Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ArticleDescription", title: "Article Name", width: "150px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOM", title: "UOM", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            },
            {
                field: "SiteName", title: "SiteName", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            }
            ,
            {
                field: "ArticleCost", title: "Standard Cost", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            },

            {
                field: "StandardCostPerKg", title: "Cost Per KG", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            }
        ],
        dataSource: {
            data: mogWiseAPLMasterdataSource,
            schema: {
                model: {
                    id: "ArticleID",
                    fields: {
                        ArticleNumber: { type: "string" },
                        ArticleDescription: { type: "string" },
                        UOM: { type: "string" },
                        ArticleType: { type: "string" },
                        Hierlevel3: { type: "string" },
                        MerchandizeCategoryDesc: { type: "string" },
                        MOGName: { type: "string" },
                        SiteCode: { type: "string" },
                        SiteName: { type: "string" }
                    }
                }
            },
            // pageSize: 100,
        },

        //maxHeight: 250,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {


        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
    //$(".k-label")[0].innerHTML.replace("items", "records");
}


function checkItemAlreadyExist(selectedId, gridID, isBR) {
    var dataExists = false;
    var gridObj = $("#" + gridID + "").data("kendoGrid");
    var data = gridObj.dataSource.data();
    var existingData = [];
    if (isBR) { existingData = data.filter(m => m.BaseRecipe_ID == selectedId); }
    else { existingData = data.filter(m => m.MOG_ID == selectedId); }
    if (existingData.length == 2) {
        dataExists = true;
    }
    return dataExists;
}

function validateDataBeforeSave(brGridID, mogGridId) {
    var isValid = true;
    if (user.SectorNumber != "20") {
        var brGridObj = $("#" + brGridID + "").data("kendoGrid");
        var data = brGridObj.dataSource.data();
        for (var i = 0; i < data.length; i++) {
            if (data[i].BaseRecipe_ID === '' || data[i].BaseRecipe_ID === null || data[i].Quantity === '' || data[i].Quantity === 0 || data[i].Quantity === "0") {
                isValid = false;
            }
        }
    }
    var mogGridObj = $("#" + mogGridId + "").data("kendoGrid");
    var mogdata = mogGridObj.dataSource.data();
    for (var i = 0; i < mogdata.length; i++) {
        if (mogdata[i].MOG_ID === '' || mogdata[i].MOG_ID === null || mogdata[i].Quantity === '' || mogdata[i].Quantity === 0 || mogdata[i].Quantity === "0") {
            isValid = false;
        }
    }
    return isValid;
}

function hideShowRecipeWindow(hideWIndow, showWindow) {
    $("#" + hideWIndow + "").hide();
    $("#" + showWindow + "").show();
    var dialog = $("#RecipeReConfgwindowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
}


function populateRecipeGrid(RegionID) {

    Utility.Loading();
    var gridVariable = $("#gridRecipe");//Hare Krishna!!
    gridVariable.html("");
    if (user.SectorNumber == "10" && isSubSectorApplicable) {
        gridVariable.kendoGrid({
            excel: {
                fileName: "Recipe.xlsx",
                filterable: true,
                allPages: true
            },
            //selectable: "cell",
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: true,
            //reorderable: true,
            scrollable: true,
            columns: [
                {
                    field: "RecipeCode", title: "Code", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Recipe Name", width: "90px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "RecipeAlias", title: "Recipe Alias", width: "90px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "SubSectorName", title: "Sub Sector", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Quantity", width: "35px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "RecipeType", title: "Type", width: "70px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    //    template: '# if (IsBase == true) {#<div>Base Recipe</div>#} else {#<div>Final Recipe</div>#}#',
                },

                {
                    field: "Status", title: "Configured", width: "50px", attributes: {
                        class: "mycustomstatus",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (Status == "1") {#<span>No</span>#} else if (Status == "2") {#<span>Yes</span>#}#',
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "Edit", title: "Action", width: "40px",
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    attributes: {
                        style: "text-align:center; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Modify',

                            click: function (e) {
                                var gridObj = $("#gridRecipe").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;

                                if (user.SectorNumber != "20" && tr.Status == 1) {
                                    //toastr.error("Configured is no");
                                    //$("#RegRecinputbase").addClass("color", "#b7aeae");

                                    return;
                                }
                                else {
                                    Utility.Loading();
                                    setTimeout(function () {

                                        RecipeName = tr.Name;
                                        rCode = tr.RecipeCode;
                                        populateUOMDropdown();
                                        recipeCategoryHTMLPopulateOnEditRecipe("", tr);
                                        $("#mainContent").hide();
                                        $("#windowEdit").show();
                                        Recipe_ID = tr.Recipe_ID;

                                        populateBaseDropdown();
                                        populateBaseRecipeGrid(tr.RecipeCode);
                                        populateMOGGrid(tr.RecipeCode);
                                        $("#RegRecrecipeid").val(tr.ID);
                                        //$("#recipeid").val(tr.ID);
                                        $("#RegRecinputrecipecode").val(tr.RecipeCode);
                                        $("#RegRecinputrecipename").val(tr.Name);
                                        $("#inputrecipealiasname").val(tr.RecipeAlias);
                                        $("#RegRecinputuom").data('kendoDropDownList').value(tr.UOM_ID);
                                        $("#RegRecinputquantity").val(tr.Quantity);
                                        if (tr.IsBase == true)
                                            $("#RegRecinputbase").data('kendoDropDownList').value("Yes");
                                        else
                                            $("#RegRecinputbase").data('kendoDropDownList').value("No");
                                        $('#grandTotal').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
                                        $('#grandCostPerKG').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
                                        $('#sectorgrandTotal').html(Utility.AddCurrencySymbol(tr.SectorTotalCost, 2));
                                        oldcost = tr.TotalCost; //tr.SectorTotalCost;
                                        var editor = $("#RegRecinputinstruction").data("kendoEditor");
                                        editor.value(decodeHTMLEntities(tr.Instructions));
                                        var change = 0;//(tr.TotalCost - oldcost).toFixed(2);
                                        var changeperc = 0;//(((change * 100) / oldcost)).toFixed(2);

                                        $("#changegrandTotal").html(Utility.AddCurrencySymbol(change, 2));
                                        console.log(changeperc)
                                        $("#totalchangepercent").html(Utility.MathRound(changeperc, 2));
                                        if (isNaN(oldcost)) {
                                            $("#changegrandTotal").hide();
                                            $("#totalchangepercent").hide();
                                        } else {
                                            $("#changegrandTotal").show();
                                            $("#totalchangepercent").show();
                                        }
                                        if (change == 0) {
                                            $("#changegrandTotal").html("No Change");
                                        }

                                        //if (user.UserRoleId === 1) {
                                        //    $("#RegRecinputrecipename").removeAttr('disabled');
                                        //    $("#RegRecinputuom").removeAttr('disabled');
                                        //    $("#RegRecinputquantity").removeAttr('disabled');
                                        //    $("#RegRecinputbase").removeAttr('disabled');
                                        //    $("#btnSubmit").css('display', 'inline');
                                        //    $("#btnCancel").css('display', 'inline');
                                        //} else {
                                        //    $("#RegRecinputrecipename").attr('disabled', 'disabled');
                                        //    $("#RegRecinputuom").attr('disabled', 'disabled');
                                        //    $("#RegRecinputquantity").attr('disabled', 'disabled');
                                        //    $("#RegRecinputbase").attr('disabled', 'disabled');
                                        //    $("#btnSubmit").css('display', 'none');
                                        //    $("#btnCancel").css('display', 'none');
                                        //}

                                        hideGrid('gridBaseRecipe', 'emptyBr', 'baseRecipePlus', 'baseRecipeMinus');
                                        hideGrid('gridMOG', 'emptymog', 'mogPlus', 'mogMinus');
                                        var dropdownlist = $("#ddlRegionMaster").data("kendoDropDownList");
                                        var regionName = dropdownlist.text();
                                        $("#topHeading").text("Regional Recipe Configuration");//.append("(" + regionName + ")");
                                        $("#location").text(" | " + regionName);
                                        //if (user.SectorNumber == "10") {
                                        //    var dsubsectorText = " | " + $("#inputSubSector").data("kendoDropDownList").text() + "";
                                        //    $("#location").text(" | " + regionName + dsubsectorText);
                                        //}
                                        //else {
                                        //    $("#location").text(" | " + regionName);
                                        //}
                                        return true;
                                        Utility.UnLoading();
                                    }, 2000);
                                }
                            }
                        },
                        {
                            name: 'Print',
                            template: function (dataItem) {
                                var html = '<div title="Print" onClick="recipeGridPrint(this)"  style="color:#8f9090; margin-left:8px" class="fa fa-print PrintRecipe"></div>';
                                return html;
                            },

                        },
                        {
                            name: 'Info',
                            template: function (dataItem) {
                                var html = '<div title="View Recipe MOG(s)" onClick="recipeGridInfo(this)"  style="color:#8f9090; margin-left:8px" class="fa fa-eye InfoRecipe"></div>';
                                return html;
                            },

                        },
                    ],
                }

            ],
            dataSource: {
                sort: { field: "RecipeType", dir: "desc" },
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetRegionRecipeDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                //Krish 20-07-2022
                                copyData = result;
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        },
                            {
                                RegionID: RegionID, condition: "All", SubSectorCode: subSectorCodeDD
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            RecipeCode: { type: "string", editable: false },
                            Status: { type: "string", editable: false },
                            Name: { type: "string", editable: false },
                            UOMName: { type: "string", editable: false },
                            RecipeType: { type: "string", editable: false },
                            IsBase: { type: "boolean", editable: false },
                            IsActive: { type: "boolean", editable: false },
                            SubSectorName: { type: "string", editable: false },
                        }
                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            // height: 500,
            dataBound: function (e) {

                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (user.SectorNumber != "20" && tr.Status == 1) {
                        $(this).find(".k-grid-Modify").addClass("k-state-disabled");
                        $(this).find(".PrintRecipe").hide();
                    }

                    //Krish
                    for (item of applicationSettingData) {
                        var code = "FLX-00010";
                        if (user.SectorNumber == "80")  //Healthcare
                            code = "FLX-00010";
                        else if (user.SectorNumber == "30")  //Core
                            code = "FLX-00010";
                        else if (user.SectorNumber == "20")  //Manufac
                            code = "FLX-00014";

                        if (item.Code == "FLX-00010" && item.IsActive == true && item.Value == "0")//item.Name == "Print Recipe Button Display Sector"
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == "FLX-00010" && item.IsActive != true)
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == "FLX-00010" && item.IsActive == true && item.Value == "1")
                            $(this).find(".PrintRecipe").show();
                        //Krish 20-07-2022
                        code = "FLX-00013";
                        if (user.SectorNumber == "80")  //Healthcare
                            code = "FLX-00017";
                        else if (user.SectorNumber == "30")  //Core
                            code = "FLX-00013";
                        else if (user.SectorNumber == "20")  //Manufac
                            code = "FLX-00017";

                        if (item.Code == "FLX-00013" && item.IsActive == true && item.Value == "0")//item.Name == "Info Recipe Button Display Sector"
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == "FLX-00013" && item.IsActive != true)
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == "FLX-00013" && item.IsActive == true && item.Value == "1")
                            $(this).find(".InfoRecipe").show();
                    }
                    
                });

                var items = e.sender.items();
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");
                        // $(this).find(".k-grid-Modify").addClass("k-state-disabled");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }


                }

            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });

                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }
        })
    }
    else {
        gridVariable.kendoGrid({
            excel: {
                fileName: "Recipe.xlsx",
                filterable: true,
                allPages: true
            },
            //selectable: "cell",
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: true,
            //reorderable: true,
            scrollable: true,
            columns: [
                {
                    field: "RecipeCode", title: "Code", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Recipe Name", width: "90px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "RecipeAlias", title: "Recipe Alias", width: "90px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Quantity", title: "Quantity", width: "50px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "60px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "RecipeType", title: "Type", width: "70px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    //    template: '# if (IsBase == true) {#<div>Base Recipe</div>#} else {#<div>Final Recipe</div>#}#',
                },

                {
                    field: "Status", title: "Configured", width: "50px", attributes: {
                        class: "mycustomstatus",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (Status == "1") {#<span>No</span>#} else if (Status == "2") {#<span>Yes</span>#}#',
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },

                {
                    field: "Edit", title: "Action", width: "80px",
                    headerAttributes: {

                        style: "text-align: center"
                    },
                    attributes: {
                        style: "text-align:center; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Modify',

                            click: function (e) {
                                var gridObj = $("#gridRecipe").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;

                                if (user.SectorNumber != "20" && tr.Status == 1) {
                                    //toastr.error("Configured is no");
                                    //$("#RegRecinputbase").addClass("color", "#b7aeae");

                                    return;
                                }
                                else {
                                    Utility.Loading();
                                    setTimeout(function () {
                                        RecipeName = tr.Name;
                                        populateUOMDropdown();
                                        recipeCategoryHTMLPopulateOnEditRecipe("", tr);
                                        Recipe_ID = tr.Recipe_ID;
                                        $("#mainContent").hide();
                                        $("#windowEdit").show();


                                        populateBaseDropdown();
                                        if (user.SectorNumber != "20") {
                                            populateBaseRecipeGrid(tr.RecipeCode);
                                        }
                                        populateMOGGrid(tr.RecipeCode);
                                        $("#RegRecrecipeid").val(tr.ID);
                                        //$("#recipeid").val(tr.ID);
                                        $("#RegRecinputrecipecode").val(tr.RecipeCode);
                                        $("#RegRecinputrecipename").val(tr.Name);
                                        $("#inputrecipealiasname").val(tr.RecipeAlias);
                                        $("#RegRecinputuom").data('kendoDropDownList').value(tr.UOM_ID);
                                        $("#RegRecinputquantity").val(tr.Quantity);
                                        if (tr.IsBase == true)
                                            $("#RegRecinputbase").data('kendoDropDownList').value("Yes");
                                        else
                                            $("#RegRecinputbase").data('kendoDropDownList').value("No");
                                        if (tr.TotalCost == "" || tr.TotalCost == null || tr.TotalCost == undefined) {
                                            //CalculateGrandTotal("");
                                        }
                                        else {
                                            $('#grandTotal').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
                                            $('#grandCostPerKG').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
                                        }

                                        $('#sectorgrandTotal').html(Utility.AddCurrencySymbol(tr.SectorTotalCost, 2));
                                        oldcost = tr.TotalCost; //tr.SectorTotalCost;
                                        var editor = $("#RegRecinputinstruction").data("kendoEditor");
                                        editor.value(decodeHTMLEntities(tr.Instructions));
                                        var change = 0;//(tr.TotalCost - oldcost).toFixed(2);
                                        var changeperc = 0;//(((change * 100) / oldcost)).toFixed(2);
                                        $(".totmoglbl").show();
                                        if (user.SectorNumber == "20") {
                                            $('#RegRecinputrecipename').removeAttr("disabled");
                                            $('#RegRecinputquantity').removeAttr("disabled");
                                            $('#RegRecinputrecipealiasname').removeAttr("disabled");
                                            $("#RegRecinputbase").data("kendoDropDownList").enable(true);
                                            //var RegRecinputuom = $("#RegRecinputuom").data("kendoDropDownList");
                                            //RegRecinputuom.enable(true);
                                            $(".changegrandTotalDiv").hide();
                                            $("#percentageChangeDiv").hide();
                                            $(".totmoglbl").hide();

                                        }
                                        else if (user.SectorNumber == "80") {
                                            $("#btnSubmit").hide();
                                            $("#btnPublish").hide();
                                        }
                                        else {
                                            $('#RegRecinputquantity').attr('disabled', 'disabled');
                                            $('#RegRecinputrecipename').attr('disabled', 'disabled');
                                            $('#RegRecinputrecipealiasname').attr('disabled', 'disabled');
                                            $("#RegRecinputbase").data("kendoDropDownList").enable(false);
                                            var RegRecinputuom = $("#RegRecinputuom").data("kendoDropDownList");
                                            RegRecinputuom.enable(false);
                                            $(".changegrandTotalDiv").show();
                                            $("#percentageChangeDiv").show();
                                            $("#changegrandTotal").html(Utility.AddCurrencySymbol(change, 2));

                                            $("#totalchangepercent").html(Utility.AddCurrencySymbol(changeperc, 2));
                                            if (isNaN(oldcost)) {
                                                $("#changegrandTotal").hide();
                                                $("#totalchangepercent").hide();
                                            } else {
                                                $("#changegrandTotal").show();
                                                $("#totalchangepercent").show();
                                            }
                                            if (change == 0) {
                                                $("#changegrandTotal").html("No Change");
                                            }

                                        }

                                        hideGrid('gridBaseRecipe', 'emptyBr', 'baseRecipePlus', 'baseRecipeMinus');
                                        hideGrid('gridMOG', 'emptymog', 'mogPlus', 'mogMinus');
                                        var dropdownlist = $("#ddlRegionMaster").data("kendoDropDownList");
                                        var regionName = dropdownlist.text();
                                        $("#topHeading").text("Regional Recipe Configuration");//.append("(" + regionName + ")");
                                        $("#location").text(" | " + regionName);
                                        //if (user.SectorNumber == "10") {
                                        //    var dsubsectorText = " | " + $("#inputSubSector").data("kendoDropDownList").text() + "";
                                        //    $("#location").text(" | " + regionName + dsubsectorText);
                                        //}
                                        //else {
                                        //    $("#location").text(" | " + regionName);
                                        //}
                                        return true;
                                        Utility.UnLoading();
                                    }, 2000);
                                }
                            }
                        },
                        {
                            name: 'Print',
                            template: function (dataItem) {
                                var html = '<div title="Print" onClick="recipeGridPrint(this)"  style="color:#8f9090; margin-left:8px" class="fa fa-print PrintRecipe"></div>';
                                return html;
                            },

                        },
                        {
                            name: 'Info',
                            template: function (dataItem) {
                                var html = '<div title="View Recipe MOG(s)" onClick="recipeGridInfo(this)"  style="color:#8f9090; margin-left:8px" class="fa fa-eye InfoRecipe"></div>';
                                return html;
                            },

                        },
                    ],
                }

            ],
            dataSource: {
                sort: { field: "RecipeType", dir: "desc" },
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetRegionRecipeDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                //Krish 20-07-2022
                                copyData = result;
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        },
                            {
                                RegionID: RegionID, condition: "All", SubSectorCode: subSectorCodeDD
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            RecipeCode: { type: "string", editable: false },
                            Status: { type: "string", editable: false },
                            Name: { type: "string", editable: false },
                            UOMName: { type: "string", editable: false },
                            SubSectorName: { type: "string", editable: false },
                            RecipeType: { type: "string", editable: false },
                            IsBase: { type: "boolean", editable: false },
                            IsActive: { type: "boolean", editable: false }
                        }
                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            // height: 500,
            dataBound: function (e) {

                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (user.SectorNumber !== "20" && model.Status == 1) {
                        $(this).find(".k-grid-Modify").addClass("k-state-disabled");
                        $(this).find(".PrintRecipe").hide();
                    }
                    //Krish
                    for (item of applicationSettingData) {
                        if (item.Code == "FLX-00010" && item.IsActive == true && item.Value == "0")//item.Name == "Print Recipe Button Display Sector"
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == "FLX-00010" && item.IsActive != true)
                            $(this).find(".PrintRecipe").hide();
                        if (item.Code == "FLX-00010" && item.IsActive == true && item.Value == "1")
                            $(this).find(".PrintRecipe").show();
                        //Krish 20-07-2022
                        if (item.Code == "FLX-00013" && item.IsActive == true && item.Value == "0")//item.Name == "Info Recipe Button Display Sector"
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == "FLX-00013" && item.IsActive != true)
                            $(this).find(".InfoRecipe").hide();
                        if (item.Code == "FLX-00013" && item.IsActive == true && item.Value == "1")
                            $(this).find(".InfoRecipe").show();
                    }                   

                });

                var items = e.sender.items();
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");
                        // $(this).find(".k-grid-Modify").addClass("k-state-disabled");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }


                }
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });

                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }
        })
    }
    //$(".k-label")[0].innerHTML.replace("items", "records");
}
//});


function recipeCategoryHTMLPopulateOnEditRecipe(level, tr) {
    if (user.SectorNumber == "20" ){
        populateRecipeCategoryUOMDropdown(level);
        populateWtPerUnitUOMDropdown(level);
        populateGravyWtPerUnitUOMDropdown(level);
        if (tr.RecipeUOMCategory)
            $("#inputrecipecum" + level + "").data('kendoDropDownList').value(tr.RecipeUOMCategory);
        var recipeCategory = tr.RecipeUOMCategory;
        if (tr.RecipeUOMCategory == undefined || tr.RecipeUOMCategory == null || tr.RecipeUOMCategory == "") {
            recipeCategory = "UOM-00001";
            $("#inputrecipecum" + level + "").data('kendoDropDownList').value("UOM-00001");
        }
        if (level === "") {
            OnRecipeUOMCategoryChange(recipeCategory, level);
        }
        if (tr.WeightPerUnit)
            $("#inputwtperunit" + level + "").val(tr.WeightPerUnit);
        if (tr.WeightPerUnitGravy)
            $("#inputgravywtperunit" + level + "").val(tr.WeightPerUnitGravy);
        if (tr.WeightPerUnitUOM)
            $("#inputwtperunituom" + level + "").data("kendoDropDownList").value(tr.WeightPerUnitUOM);
        if (tr.WeightPerUnitUOMGravy)
            $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value(tr.WeightPerUnitUOMGravy);
    }
}

function populateWtPerUnitUOMDropdown0() {
    $("#inputwtperunituom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 1,
    });
}

function populateGravyWtPerUnitUOMDropdown0() {
    $("#inputgravywtperunituom0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata,
        index: 1,
    });
}

function populateRecipeCategoryUOMDropdown0() {
    $("#inputrecipecum0").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: recipecategoryuomdata,
        index: 1,
        change: function (e) {
            OnRecipeUOMCategoryChange(e.sender.dataItem().UOMCode, "0")

        }
    });

}

function populateWtPerUnitUOMDropdown(level) {
    $("#inputwtperunituom" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata,
        index: 1,
    });
}

function populateGravyWtPerUnitUOMDropdown(level) {
    $("#inputgravywtperunituom" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: uomdata,
        index: 1,
    });
}

function populateRecipeCategoryUOMDropdown(level) {
    $("#inputrecipecum" + level + "").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "UOMCode",
        dataSource: recipecategoryuomdata,
        index: 1,
        change: function (e) {
            OnRecipeUOMCategoryChange(e.sender.dataItem().UOMCode, level)

        }
    });
}

//function OnRecipeUOMCategoryChange(recipeCategory, level) {
//    $("#inputwtperunit" + level + "").val("");
//    if ($("#RegRecinputuom" + level + "").data("kendoDropDownList") != undefined) {
//        $("#RegRecinputuom" + level + "").data("kendoDropDownList").value("Select");
//    }
//    $("#inputgravywtperunit" + level + "").val("");
//    $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("Select");
//    $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("Select");
//    var inputwtperunituom0 = $("#inputwtperunituom" + level + "").data("kendoDropDownList");
//    inputwtperunituom0.enable(false);
//    var inputgravywtperunituom0 = $("#inputgravywtperunituom" + level + "").data("kendoDropDownList");
//    inputgravywtperunituom0.enable(false);
//    if (recipeCategory == "UOM-00005" || recipeCategory == "UOM-00006") {
//        var rUOM = recipeCategory == "UOM-00005" ? "1" : "2";
//        $("#inputwtperunit" + level + "").prop("disabled", true);
//        $("#inputgravywtperunit" + level + "").prop("disabled", true);
//        $("#RegRecinputuom" + level + "").data("kendoDropDownList").value(rUOM);
//        var inputwtperunituom0 = $("#RegRecinputuom" + level + "").data("kendoDropDownList");
//        inputwtperunituom0.enable(false);
//    }
//    else if (recipeCategory == "UOM-00007") {
//        $("#inputwtperunit" + level + "").prop("disabled", false);
//        $("#inputgravywtperunit" + level + "").prop("disabled", true);
//        $("#RegRecinputuom" + level + "").data("kendoDropDownList").value(3);
//        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
//        var inputwtperunituom0 = $("#RegRecinputuom" + level + "").data("kendoDropDownList");
//        inputwtperunituom0.enable(false);
//    }
//    else if (recipeCategory == "UOM-00009") {
//        $("#inputwtperunit" + level + "").prop("disabled", true);
//        $("#inputgravywtperunit" + level + "").prop("disabled", false);
//        $("#RegRecinputuom" + level + "").data("kendoDropDownList").value(3);
//        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
//        var inputwtperunituom0 = $("#RegRecinputuom" + level + "").data("kendoDropDownList");
//        inputwtperunituom0.enable(false);

//    }
//    else if (recipeCategory == "UOM-00008") {
//        $("#inputwtperunit" + level + "").prop("disabled", false);
//        $("#inputgravywtperunit" + level + "").prop("disabled", false);
//        $("#RegRecinputuom" + level + "").data("kendoDropDownList").value(3);
//        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
//        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
//    }
//};

function OnRecipeUOMCategoryChange(recipeCategory, level) {
    $("#inputwtperunituom" + level + "").closest(".k-widget").hide();
    $("#inputgravywtperunituom" + level + "").closest(".k-widget").hide();
    $("#RegRecinputuom" + level + "").closest(".k-widget").hide();
    $("#linputwtperunituom" + level + "").text("-");
    $("#linputgravywtperunituom" + level + "").text("-");
    $("#lRegRecinputuom" + level + "").text("-");

    $("#inputwtperunit" + level + "").val("");

    if ($("#RegRecinputuom" + level + "").data("kendoDropDownList") != undefined) {
        $("#RegRecinputuom" + level + "").data("kendoDropDownList").value("Select");
    }
    $("#inputgravywtperunit" + level + "").val("");
    $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("Select");
    $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("Select");
    var inputwtperunituom0 = $("#inputwtperunituom" + level + "").data("kendoDropDownList");
    inputwtperunituom0.enable(false);
    var inputgravywtperunituom0 = $("#inputgravywtperunituom" + level + "").data("kendoDropDownList");
    inputgravywtperunituom0.enable(false);

    if (recipeCategory == "UOM-00005" || recipeCategory == "UOM-00006") {
        var rUOM = recipeCategory == "UOM-00005" ? "1" : "2";
        var rUOMText = uomdata.find(m => m.value == rUOM).text;
        $("#inputwtperunit" + level + "").prop("disabled", true);
        $("#inputgravywtperunit" + level + "").prop("disabled", true);
        $("#RegRecinputuom" + level + "").data("kendoDropDownList").value(rUOM);
        $("#lRegRecinputuom" + level + "").text(rUOMText);
        var inputwtperunituom0 = $("#RegRecinputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputwtperunituom" + level + "").text("-");
        $("#linputgravywtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00007") {
        $("#inputwtperunit" + level + "").prop("disabled", false);
        $("#inputgravywtperunit" + level + "").prop("disabled", true);
        $("#RegRecinputuom" + level + "").data("kendoDropDownList").value(3);
        var rUOMText = uomdata.find(m => m.value == 3).text;
        $("#lRegRecinputuom" + level + "").text(rUOMText);
        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").UOMName;
        $("#linputwtperunituom" + level + "").text(rUText);
        var inputwtperunituom0 = $("#RegRecinputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputgravywtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00009") {
        $("#inputwtperunit" + level + "").prop("disabled", true);
        $("#inputgravywtperunit" + level + "").prop("disabled", false);
        $("#RegRecinputuom" + level + "").data("kendoDropDownList").value(3);
        var rUOMText = uomdata.find(m => m.value == 3).text;
        $("#lRegRecinputuom" + level + "").text(rUOMText);
        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").text;
        $("#linputgravywtperunituom" + level + "").text(rUText);
        var inputwtperunituom0 = $("#RegRecinputuom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        $("#linputwtperunituom" + level + "").text("-");
    }
    else if (recipeCategory == "UOM-00008") {
        $("#inputwtperunit" + level + "").prop("disabled", false);
        $("#inputgravywtperunit" + level + "").prop("disabled", false);
        $("#RegRecinputuom" + level + "").data("kendoDropDownList").value(3);
        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
        var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").text;
        $("#linputgravywtperunituom" + level + "").text(rUText);
        $("#linputwtperunituom" + level + "").text(rUText);
        var rUOMText = uomdata.find(m => m.value == 3).text;
        $("#lRegRecinputuom" + level + "").text(rUOMText);
    }
};

function RecipeRegshowDetails(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.RecipeCode.trim() == dataItem.BaseRecipeCode)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    var dialog = $("#RecipeReConfgwindowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open().element.closest(".k-window").css({
        top: 94,
        left: 284.5
    });
    dialog.center();
    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.RecipeCode);
    populateMOGGrid1(tr.RecipeCode);

    $("#RegRecrecipeid1").val(tr.ID);
    $("#RegRecinputrecipecode1").val(tr.RecipeCode);
    $("#RegRecinputrecipename1").val(tr.Name);
    $("#RegRecinputrecipealiasname1").val(tr.RecipeAlias);
    recipeCategoryHTMLPopulateOnEditRecipe("1", tr);
    $("#RegRecinputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#RegRecinputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#RegRecinputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#RegRecinputbase1").data('kendoDropDownList').value("No");
    $('#grandTotal1').html(Utility.MathRound(tr.TotalCost, 2));
    $('#grandCostPerKG1').html(Utility.MathRound(tr.CostPerKG, 2));
    var editor = $("#RegRecinputinstruction1").data("kendoEditor");
    editor.value(decodeHTMLEntities(tr.Instructions));
    //if (user.UserRoleId === 1) {
    //    $("#RegRecinputrecipename1").removeAttr('disabled');
    //    $("#RegRecinputuom1").removeAttr('disabled');
    //    $("#RegRecinputquantity1").removeAttr('disabled');
    //    $("#RegRecinputbase1").removeAttr('disabled');
    //    $("#btnSubmit1").css('display', 'inline');
    //    $("#btnCancel1").css('display', 'inline');
    //} else {
    //    $("#RegRecinputrecipename1").attr('disabled', 'disabled');
    //    $("#RegRecinputuom1").attr('disabled', 'disabled');
    //    $("#RegRecinputquantity1").attr('disabled', 'disabled');
    //    $("#RegRecinputbase1").attr('disabled', 'disabled');
    //    $("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details Inline - " + RecipeName);
    return true;

}
function calculateItemTotal(e, level) {

    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");
    if (level == null || typeof level == 'undefined')
        level = '';
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    dataItem.Quantity = qty;
    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    var recipeQty = $("#RegRecinputquantity" + level).val();

    if (user.SectorNumber !== "20") {
        var perc = (qty / recipeQty) * 100;
        dataItem.IngredientPerc = perc.toFixed(2);
        var changePerc = (perc - dataItem.SectorIngredientPerc);//* 100 / dataItem.SectorIngredientPerc;
        changePerc = changePerc.toFixed(2);
        //  if (level == '' || level == '0') {
        $(element).closest('tr').find('.change')[0].innerHTML = changePerc;
        if (Math.abs(changePerc) > PrescibedPercentChange) {
            //        toastr.error("Quantity changes are going beyond the allowed change limit");
            $(element).closest('tr').find('.change').css('background-color', '#ffe1e1');

        } else {

            $(element).closest('tr').find('.change').css('background-color', '#d9ffb3');
        }
        if (changePerc == 0) {
            $(element).closest('tr').find('.change').css('background-color', 'inherit');
        }
        $(element).closest('tr').find('.minor')[0].innerHTML = dataItem.IngredientPerc;
    }

    dataItem.TotalCost = qty * dataItem.CostPerUOM;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.MathRound(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputbrqty')[0].innerHTML = qty;
    CalculateGrandTotal(level);
}

function BaseRecipetotalQTY(e, level) {
    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridBaseRecipe" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var gTotal = 0.0;
    var len1 = grid._data.length;

    for (i = 0; i < len1; i++) {
        var totalCostMOG = grid._data[i].Quantity;

        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {

        $("#totBaseRecpQty").text(Utility.MathRound(gTotal, 2));
        // parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))).toFixed(2);
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
        $("#ToTIngredients").text(tot);
    }


}

function mogtotalQTY(e, level) {

    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var gTotal = 0.0;
    var len1 = grid._data.length;

    for (i = 0; i < len1; i++) {
        var totalCostMOG = user.SectorNumber == "20" ? grid._data[i].DKgValue * grid._data[i].Quantity : grid._data[i].Quantity;
        if (totalCostMOG == "") {
            continue;
        }
        gTotal += Number(totalCostMOG);
    }
    if (!isNaN(gTotal)) {
        $("#totmog").text(Utility.MathRound(gTotal, 2));
        var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
        $("#ToTIngredients").text(tot);
    }

}


function calculateItemTotalMOG(e, level) {
    var element = e
    var row = element.closest("tr");
    if (level == null || typeof level == 'undefined')
        level = '';
    var grid = $("#gridMOG" + level).data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var qty = $(e).val();
    var recipeQty = $("#RegRecinputquantity" + level).val();
    var perc = (qty / recipeQty) * 100;

    dataItem.Quantity = qty;
    dataItem.IngredientPerc = perc.toFixed(2);

    if (user.SectorNumber !== "20") {
        dataItem.TotalCost = qty * dataItem.CostPerUOM;
    }
    else {
        var dkgValue = 0;
        var dkgData = mogdata.filter(m => m.MOG_Code == dataItem.MOGCode);
        if (dkgData != null && dkgData.length > 0) {
            dkgValue = dkgData[0].DKgValue;
        }
        dkgtotal = dkgValue * qty;
        dataItem.DKgTotal = dkgtotal;
        dataItem.set("DKgTotal", dkgtotal);
        $(element).closest('tr').find('.dkgtotal')[0].innerHTML = dkgtotal;
        dataItem.TotalCost = qty * dataItem.CostPerUOM * dkgValue;
    }


    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    $(element).closest('tr').find('.inputmogqty')[0].innerHTML = qty;
    if (user.SectorNumber !== "20") {
        var changePerc = (dataItem.Quantity - dataItem.SectorQuantity) * 100 / dataItem.SectorQuantity;
        changePerc = changePerc.toFixed(2);
        $(element).closest('tr').find('.change')[0].innerHTML = changePerc;
        if (Math.abs(changePerc) > PrescibedPercentChange) {
            $(element).closest('tr').find('.change').css('background-color', '#ffe1e1');
        } else {
            $(element).closest('tr').find('.change').css('background-color', '#d9ffb3');
        }
        if (changePerc == 0) {
            $(element).closest('tr').find('.change').css('background-color', 'inherit');
        }

    }

    CalculateGrandTotal(level);
    mogtotalQTY(e, level);
}
function CalculateGrandTotal(level) {
    var gtotalObjforBR = $("#gridBaseRecipe" + level + " .totalcost");
    var gtotalObjforMOG = $("#gridMOG" + level + " .totalcost");
    var len = gtotalObjforBR.length;
    var len1 = gtotalObjforMOG.length;
    var gTotal = 0.0;
    for (i = 0; i < len; i++) {
        if (gtotalObjforBR[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObjforBR[i].innerText.substring(1).replace(",", ""));
    }
    for (i = 0; i < len1; i++) {
        if (gtotalObjforMOG[i].innerText == "") {
            continue;
        }
        gTotal += Number(gtotalObjforMOG[i].innerText.substring(1).replace(",", ""));
    }
    if (!isNaN(gTotal)) {
        $("#grandTotal" + level).html(Utility.AddCurrencySymbol(gTotal, 2));
    }
    var qty = $('#RegRecinputquantity' + level).val();
    var costPerKG = gTotal / Number(qty);
    $('#grandCostPerKG' + level).html(Utility.AddCurrencySymbol(costPerKG, 2));
    var change = gTotal - oldcost;
    var changeperc = (change * 100 / oldcost).toFixed(2)
    $("#changegrandTotal" + level).html(Utility.AddCurrencySymbol(change, 2));
    $("#totalchangepercent" + level).html(Utility.MathRound(changeperc, 2));
    if (PerCostChangePercent > Math.abs(changeperc)) {
        $("#changegrandTotal" + level).css('color', 'green');
        $("#totalchangepercent" + level).css('color', 'green');
        $(".grtotal").style = "background-color: #d9ffb3 !important";
        //$(".grtotal").css("background-color", '#d9ffb3 !important');
    }
    else {
        $("#changegrandTotal" + level).css('color', 'red');
        $("#totalchangepercent" + level).css('color', 'red');
        $(".grtotal").style = "background-color: #ffe1e1 !important";
        //$(".grtotal").style.setProperty("background-color", "#ffe1e1", "important");
        //$(".grtotal").css("background-color", '#ffe1e1 !important');
    }
    if (isNaN(oldcost)) {
        $("#changegrandTotal" + level).hide();
        $("#totalchangepercent" + level).hide();
    } else {
        $("#changegrandTotal" + level).show();
        $("#totalchangepercent" + level).show();
    }
    mogtotalQTYDelete(level);
    if (user.SectorNumber != "20") {
        $(".changegrandTotalDiv").show();
        BaseRecipetotalQTYDelete(level);
    }
    else {
        $(".changegrandTotalDiv").hide();
    }
}

function validationPerCost() {
    var ele = $(".change");
    var len = ele.length;
    if (user.SectorNumber !== "20") {
        for (var i = 0; i < len; i++) {
            if (Math.abs(ele[i].innerText) > PrescibedPercentChange) {
                toastr.error("Quantity changes are going beyond the allowed change limit");
                return false;
            }
        }
    }
    //var changeperc = $("#totalchangepercent").html();
    //if (PerCostChangePercent > Math.abs(changeperc)) {
    //    toastr.error("New Cost is going beyond the allowed change limit");
    //    return false;
    //}
    return true;
}

function cancelChangesConfirmation(gridId) {
    $(".k-grid-cancel-changes").unbind("mousedown");
    $(".k-grid-cancel-changes").mousedown(function (e) {
         Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                var grid = $(gridId).data("kendoGrid");
                grid.cancelChanges();
            },
            function () {
            }
        );

    });
}

//Krish
function recipeGridPrint(e) {
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var tr = gridObj.dataItem($(e).closest("tr"))
    //console.log(tr);
    Utility.Loading();
    setTimeout(function () {
        gridPrint(tr);
       

    }, 0);
    //var restorepage = document.body.innerHTML;
    //var printcontent = document.getElementById("printRecipe").innerHTML;
    ////document.body.innerHTML = printcontent;
    //window.print();
    ////document.body.innerHTML = restorepage;
}
function gridPrint(tr) {
    HttpClient.MakeSyncRequest(CookBookMasters.GetRecipePrintingData, function (data) {
        //console.log(data);
        $("#spanRecipeName").html(tr.RecipeCode + " " + tr.Name + " | ");
        $("#spanDishName").html(data.DishName + " | ");

        //if (tr.Yield == null || tr.Yield == "null") {
        //    $("#spanYield").html("");
        //    $("#spanDishName").html(data.DishName);
        //}
        //else {
        $("#spanYield").html(tr.Quantity + " " + tr.UOMName);
        //}

        //document.getElementById("txtaInstructions").innerHTML = tr.Instructions

        $("#txtaInstructions1").html(tr.Instructions);
        $("#txtaInstructions").html($("#txtaInstructions1").text());
        //$("#txtaInstructions").html($("#txtaInstructions2").text());

        //Table 
        var table = document.getElementById("tblRecipePrint");
        table.innerHTML = "";
        var row = table.insertRow(-1);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "MOG Code";
        row.appendChild(headerCell);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "MOG Name";
        row.appendChild(headerCell);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "UOM";
        row.appendChild(headerCell);
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = "Quantity";
        row.appendChild(headerCell);


        for (var i = 0; i < data.Mogs.length; i++) {
            var row1 = table.insertRow(-1);
            var innerCell = document.createElement("TD");
            innerCell.innerHTML = data.Mogs[i].MOGCode;
            row1.appendChild(innerCell);
            innerCell = document.createElement("TD");
            innerCell.innerHTML = data.Mogs[i].MOGName;
            row1.appendChild(innerCell);
            innerCell = document.createElement("TD");
            innerCell.innerHTML = data.Mogs[i].UOMName;
            row1.appendChild(innerCell);
            innerCell = document.createElement("TD");
            //var mogqty = data.Mogs[i].MOGQty.toString();
            //var qty = mogqty.slice(0, (mogqty.indexOf(".")) + 2);
            if (data.Mogs[i].MOGQty == null) { innerCell.innerHTML = "0.00"; }
            else {
                var qty = data.Mogs[i].MOGQty.toFixed(2);
                if (parseFloat(qty) == 0)
                    qty = data.Mogs[i].MOGQty.toFixed(3);
                innerCell.innerHTML = qty;
            }
            //innerCell.innerHTML = data.Mogs[i].MOGQty.toFixed(2);
            row1.appendChild(innerCell);
        }

        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById("printRecipe").innerHTML;
        //document.body.innerHTML = printcontent;
        setTimeout(function () { document.title = "CookBook Recipe - " + tr.Name; window.print(); }, 500);
        //$("#printRecipe").show();
        //$("#mainContent").hide();

        //document.body.innerHTML = restorepage;
        //$("#printRecipe").print();
        //var divToPrint = document.getElementById("printRecipe");
        //newWin = window.open("");
        //console.log(printcontent);
        //newWin.document.write(printcontent);
        //newWin.print();
        //newWin.close();

        //$("#printRecipe").printThis();             

        Utility.UnLoading();

    }, { recipeCode: tr.RecipeCode, siteCode: "", regionId: tr.Region_ID }, true);
}


//Krish 20-07-2022
function recipeGridInfo(e) {
    $("#displayRecipeDetails").show();
    $("#mainContent").hide();

    var gridObj = $("#gridRecipe").data("kendoGrid");
    var tr = gridObj.dataItem($(e).closest("tr"));
    
    //console.log(tr);
    Utility.Loading();
    setTimeout(function () {
        HttpClient.MakeSyncRequest(CookBookMasters.GetRecipePrintingData, function (data) {
            
            $("#hdnRecipeCodeForPrint").val(tr.RecipeCode);
            populateMOGGridNutri(data.Mogs);

            setTimeout(function () {
                var html = $("#recipeDetails").html();
                html = html.replace("##RecipeCode##", tr.RecipeCode);
                html = html.replace("##RecipeName##", tr.Name);
                html = html.replace("##Quantity##", tr.Quantity);
                html = html.replace("##RecipeType##", tr.RecipeType);
                html = html.replace("##Allergen##", tr.AllergensName);
                html = html.replace("##AllergenNG##", tr.NGAllergens);
                //setTimeout(function () { 
                //var gridHtml = $("#gridContent").html();
                var gridHtml = $("#gridMOGNutri").html();
                //html = html.replace("##RecipeGrid##", gridHtml);
                html = html.replace("##RecipeGrid##", gridHtml);
                //console.log(gridHtml)
                var instr = tr.Instructions;
                if (tr.Instructions == "null")
                    instr = "";
                $("#instructionContent").html(instr);
                html = html.replace("##Instructions##", $("#instructionContent").text());

                $("#recipeDetailsShell").html(html);

            }, 300)

            
            Utility.UnLoading();

        }, { recipeCode: tr.RecipeCode, siteCode: "", regionId: tr.Region_ID }, true);

    }, 0);
}
function populateMOGGridNutri(recipeResult) {
    console.log(recipeResult)
    Utility.Loading();
    //$("#gridContent").html("");
    //$("#gridContent").html("<div id='gridMOGNutri' style='overflow: visible!important;'></div>")
    var gridVariable = $("#gridMOGNutri");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" }, //{ name: "create", text: "Add" }
        //],
        groupable: false,
        editable: false,

        columns: [
            {
                field: "MOGCode", title: "MOG Code", width: "100px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                }

            },
            {
                field: "MOG_ID", title: "MOG", width: "100px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },

            },
            //{
            //    field: "ColorCode", title: "Color", width: "30px", attributes: {

            //        style: "text-align: center; font-weight:normal"
            //    },
            //    headerAttributes: {
            //        style: "text-align: center"
            //    },
            //    template: '<div class="colortag"></div>',
            //},
            //{
            //    field: "AllergensName", title: "Contains", width: "50px",
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "AllergensName",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //},
            //{
            //    field: "NGAllergen", title: "May Contains (NG)", width: "70px",
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "AllergensName",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //},
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "MOGQty", title: "Quantity", width: "35px",

                headerAttributes: {
                    style: "text-align: center;width:35px;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                template: function (dataItem) {
                    var incel = "";
                    if (dataItem.MOGQty == null) { incel = "0.00"; }
                    else {
                        var qty = dataItem.MOGQty.toFixed(2);
                        if (parseFloat(qty) == 0)
                            qty = dataItem.MOGQty.toFixed(3);
                        incel = qty;
                    }
                    var html = '<span class="mogquant">' + incel + '</span>';
                    return html;
                },
            },

        ],
        dataSource: recipeResult,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {

            var grid = this;

            //grid.tbody.find("tr[role='row']").each(function () {
            //    var model = grid.dataItem(this);

            //    if (model.AllergensName != "None" && model.NGAllergen != "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00012" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //    else if (model.AllergensName == "None" && model.NGAllergen != "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00013" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //    else if (model.AllergensName != "None" && model.NGAllergen == "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00014" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //    else if (model.AllergensName == "None" && model.NGAllergen == "None") {
            //        var getBothColor = applicationSettingData.filter(a => a.Code == "FLX-00015" && a.IsActive == true)[0];
            //        if (getBothColor != undefined)
            //            $(this).find(".colortag").css('background-color', getBothColor.Value);
            //    }
            //});
        },
        change: function (e) {
        },
    });

}
function doBackFromDetails() {
    $("#displayRecipeDetails").hide();
    $("#mainContent").show();
}
function doPrintFromDetails() {
    var tr = copyData.filter(a => a.RecipeCode == $("#hdnRecipeCodeForPrint").val())[0];
    gridPrint(tr);
    //var btnInfo = localStorage.getItem("infobtn");
    //var gridObj = $("#gridRecipe").data("kendoGrid");
    //var tr = gridObj.dataItem($(btnInfo).closest("tr"));
    //console.log(tr)
    //recipeGridPrint(btnInfo);
}