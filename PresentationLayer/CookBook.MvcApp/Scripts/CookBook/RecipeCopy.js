﻿var RecipeName = "";
var uomdata = [];
var moguomdata = [];
var recipecategoryuomdata = [];
var basedata = [];
var dishdata = [];
var baserecipedata = [];
var baserecipedatafiltered = [];
var mogdata = [];
var mogdataList = [];
var mogWiseAPLMasterdataSource = [];
var MOGCode = "MOG-00156";
var MOGName = "Test"
var sitemasterdata = [];
var multisitemasterdata = [];
var regionmasterdata = [];
var SiteCode = "";
var IsMajor = 0;
var MajorPercentData = 5;
var PrescibedPercentChange = 10;
var PerCostChangePercent = 5;
var x = 'x';
var oldcost = 0;
var currentPopuatedMOGGrid = "";
var MOGSelectedArticleNumber = "";
var MOGSelectedCost = "";
var allergenmasterdata = [];
var isMainRecipe = false;
var Region_ID = 0;
var Recipe_ID = 0;
var SiteID = 0;
var ToTIngredients = 0;
var moguomdata = [];
var ItemDishRecipeNutritionData = [];
var FilterItemDishRecipeNutritionData = [];
var dishHeaderString = "";
var selectedsiteids = [];
var tempsiterecipes = [];

$("document").ready(function () {
    $("#intialImage").show();

    $("#btnGo").on("click", function () {
        var msg = "";
        var dropdownlist = $("#inputsite").data("kendoDropDownList");
        console.log(dropdownlist.value())
        if (dropdownlist != undefined && dropdownlist.value() == 0) {
            msg = msg + "Please select Copy from Unit </br>";
        }
        var multiselectsite = $("#inputsitemulti").data("kendoMultiSelect");
        //Utility.Loading();

        //selectedsiteids = [];
        if (multiselectsite != undefined) {
            var items = multiselectsite.value();
            //tempsiterecipes = [];
            if (items.length == 0) {
                msg = msg + "Please select Copy to Unit </br>";
            }
        }
        else
            msg = msg + "Please select Copy to Unit </br>";
        if (checkedRecipeIds.length == 0)
            msg = msg + "Please select recipes to copy </br>";

        if (msg.length == 0) {
            //tempsiterecipes
            //11-07-2022
            var multiselectsite = $("#inputsitemulti").data("kendoMultiSelect");
            var items = multiselectsite.value();
            tempsiterecipes = [];
            if (items.length > 0) {
                Utility.UnLoading();
                for (var i = 0; i < items.length; i++) {
                    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteRecipeDataList, function (result) {
                        Utility.UnLoading();
                        tempsiterecipes.push({ SiteCode: items[i], Recipes: result });
                        //if (result != null) {

                        //    options.success(result);
                        //}
                        //else {
                        //    options.success("");
                        //}
                    },
                        {
                            SiteCode: items[i], condition: "All"
                        }
                        , true);
                    selectedsiteids.push(items[i]);
                }
            }

            if (selectedsiteids.length > 0) {
                for (var i = 0; i < selectedsiteids.length; i++) {
                    var getrecipes = tempsiterecipes.filter(a => a.SiteCode == selectedsiteids[i])[0];
                    if (getrecipes != undefined) {

                        var result = getrecipes.Recipes.filter(function (arr1) {
                            return checkedRecipeIds.some(function (arr2) {
                                return arr1.RecipeCode === arr2;
                            });
                        });
                        //console.log(result);
                        if (result.length > 0) {
                            console.log(multisitemasterdata)
                            var sitename = multisitemasterdata.filter(a => a.SiteCode == selectedsiteids[i])[0].SiteName;
                            msg = msg + sitename + ". \n";
                            console.log(msg);
                        }
                    }
                }
                if (msg.length > 0) {
                    Utility.Page_Alert_Save("One or more recipe already exist in the site \n" + msg + "\n Do you want to overwrite exising recipe?", "Confirmation", "Yes", "No",
                        function () {
                            Utility.Loading();
                            HttpClient.MakeRequest(CookBookMasters.SaveRecipeCopy, function (result) {
                                Utility.UnLoading();
                                if (result == "true") {
                                    onSiteChangeDisplayGrid('off');

                                    toastr.success("Successfully Copied");
                                    //populateSiteMasterDropdown();
                                }
                                else
                                    toastr.error("Failed to copy");
                            }, {
                                model: tempsiterecipes,
                                recipes: checkedRecipeIds,
                                sourceSite: $("#inputsite").val()

                            }, true);
                        },
                        function () {
                        }
                    );
                }
                else {
                    Utility.Loading();
                    HttpClient.MakeRequest(CookBookMasters.SaveRecipeCopy, function (result) {
                        Utility.UnLoading();
                        if (result == "true") {
                            onSiteChangeDisplayGrid('off');

                            toastr.success("Successfully Copied");
                            //populateSiteMasterDropdown();

                            //Unselect all chcekboxes
                            //var grid = $("#gridRecipe").data("kendoGrid");
                            //console.log(grid);
                            //grid.tbody.find("tr[role='row'] .checkbox").each(function () {
                            //    console.log($(this).attr('checked'))
                            //    if ($(this).attr('checked') == 'checked')
                            //        $(this).click();
                            //});
                        }
                        else
                            toastr.error("Failed to copy");
                    }, {
                        model: tempsiterecipes,
                        recipes: checkedRecipeIds,
                        sourceSite: $("#inputsite").val()

                    }, true);
                }
            }
        }
        else {
            toastr.error(msg);
        }

    });

    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();

    });

    $('#myInput').on('input', function (e) {
        var grid = $('#gridRecipe').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "RecipeAlias" || x.field == "RecipeType" || x.field == "PublishedDateAsString" || x.field == "RecipeCode" || x.field == "Name"
                    || x.field == "UOMName" || x.field == "Quantity" || x.field == "IsBase" || x.field == "Status" || x.field == "IsActive" || x.field == "IsElementoryString") {
                    if (grid.dataSource.options.schema.model.fields[x.field] != undefined) {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;
                        if (type == 'string') {
                            var targetValue = e.target.value;
                            if (x.field == "Status") {
                                var pendingString = "pending";
                                var savedString = "saved";
                                var mappedString = "published";
                                if (pendingString.includes(e.target.value.toLowerCase())) {
                                    targetValue = "1";
                                }
                                else if (savedString.includes(e.target.value.toLowerCase())) {
                                    targetValue = "2";
                                }
                                else if (mappedString.includes(e.target.value.toLowerCase())) {
                                    targetValue = "3";
                                }
                            }

                            filter.filters.push({
                                field: x.field,
                                operator: 'contains',
                                value: targetValue
                            })
                        }
                        else if (type == 'number') {
                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        }
                        else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format(x.format, data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && e.target.value !== null) {
                            // var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    HttpClient.MakeRequest(CookBookMasters.GetSiteDataListByUserId, function (data) {
        var dataSource = data;
        sitemasterdata = dataSource;
        sitemasterdata.forEach(function (item) {
            item.SiteName = item.SiteName + " - " + item.SiteCode;
            return item;
        });
        sitemasterdata = sitemasterdata.filter(item => item.IsActive != false);
        if (data.length > 1) {
            sitemasterdata.unshift({ "SiteCode": 0, "SiteName": "Select Site", "Region_ID": "" });
            $("#btnGo").css("display", "inline-block");
        }
        else {
            $("#btnGo").css("display", "inline-none");
        }
        populateSiteMasterDropdown();


    }, { SiteCode: $("#inputsite").val() }, true);
});


//HttpClient.MakeSyncRequest(CookBookMasters.GetApplicationSettingDataList, function (result) {
//    Utility.UnLoading();

//    if (result != null) {
//        applicationSettingData = result;
//        for (item of applicationSettingData) {
//            if (item.Code == "FLX-00001") {//||item.Name.trim() == "Site Recipe Cost Change Threshold") {
//                PerCostChangePercent = item.Value;
//                $(".asSiteCost").text(PerCostChangePercent);
//            } else if (item.Code == "FLX-00006") {// ||item.Name.trim()  == "Site Region Major Identification Threshold") {
//                MajorPercentData = item.Value;
//            } else if (item.Code == "FLX-00007") {//|| item.Name.trim()  == "Site Recipe Quantity Change Threshold") {
//                PrescibedPercentChange = item.Value;
//                $(".asSiteQuantity").text(PrescibedPercentChange);
//            }

//        }

//    }
//    else {

//    }
//}, null, false);

function populateSiteMasterDropdown() {
    $("#inputsite").kendoDropDownList({
        filter: "contains",
        dataTextField: "SiteName",
        dataValueField: "SiteCode",
        dataSource: sitemasterdata,
        index: 0,
        change: onSiteChangeDisplayGrid
    });

    if (sitemasterdata.length == 1) {
        var dropdownlist = $("#inputsite").data("kendoDropDownList");
        dropdownlist.wrapper.hide();
        Utility.Loading();
        setTimeout(function () {
            SiteCode = dropdownlist.value();
            populateRecipeGrid(SiteCode);
            $("#searchspan").show();
            var siteName = dropdownlist.text()
            $("#location").text(" | " + siteName);
            Utility.UnLoading();
        }, 2000);

    }
    checkedRecipeIds = [];
    populateMultiSiteDropdown();
}
function populateMultiSiteDropdown() {
    //$("#NewSimServiceType").kendoDropDownList({
    //    filter: "contains",
    //    dataTextField: "Name",
    //    dataValueField: "ID",
    //    dataSource: daypartdata,
    //    index: 0,
    //    change: onDayPartChange
    //});
    var multiselect = $("#inputsitemulti").data("kendoMultiSelect");
    if (multiselect == undefined) {

        $("#inputsitemulti").kendoMultiSelect({
            filter: "contains",
            dataTextField: "SiteName",
            dataValueField: "SiteCode",
            dataSource: multisitemasterdata,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
            change: onSiteMultiChange
        });
    }
    else {
        var multiselect = $("#inputsitemulti").data("kendoMultiSelect");
        var ds = new kendo.data.DataSource({ data: multisitemasterdata });
        multiselect.setDataSource(ds);
    }
}

function onSiteMultiChange() {
    //11-07-2022
    //var multiselectsite = $("#inputsitemulti").data("kendoMultiSelect");
    ////Utility.Loading();
    //selectedsiteids = [];
    //if (multiselectsite != undefined) {
    //    var items = multiselectsite.value();
    //    tempsiterecipes = [];
    //    if (items.length > 0) {
    //        for (var i = 0; i < items.length; i++) {
    //            HttpClient.MakeSyncRequest(CookBookMasters.GetSiteRecipeDataList, function (result) {
    //                //Utility.UnLoading();
    //                tempsiterecipes.push({ SiteCode: items[i], Recipes: result });
    //                //if (result != null) {

    //                //    options.success(result);
    //                //}
    //                //else {
    //                //    options.success("");
    //                //}
    //            },
    //                {
    //                    SiteCode: items[i], condition: "All"
    //                }
    //                , true);
    //            selectedsiteids.push(items[i]);
    //        }
    //    }
    //    else {
    //        //Utility.UnLoading();
    //    }
    //}
    //console.log(tempsiterecipes)
}

function onSiteChangeDisplayGrid(checkonoff) {
    Utility.Loading();
    setTimeout(function () {
        var dropdownlist = $("#inputsite").data("kendoDropDownList");
        var dataitem = dropdownlist.dataItem();

        SiteCode = dropdownlist.value();
        Region_ID = dataitem.RegionID;
        SiteID = dataitem.SiteID;

        multisitemasterdata = sitemasterdata.filter(a => a.SiteID != dataitem.SiteID && a.SiteCode != 0);
        populateMultiSiteDropdown();
        //HttpClient.MakeRequest(CookBookMasters.GetMOGDataList, function (data) {

        //    var dataSource = data;

        //    mogdata = [];
        //    mogdata.push({
        //        "MOG_ID": "Select MOG", "MOGName": "Select MOG", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0,
        //        "MOG_Code": "", "AllergensName": "None", "ArticleNumber": 0, "UOMCode": "", "DKgValue": 0
        //    });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        if (dataSource[i].CostPerUOM == null) {
        //            dataSource[i].CostPerUOM = 0;
        //        }
        //        if (dataSource[i].CostPerKG == null) {
        //            dataSource[i].CostPerKG = 0;
        //        }
        //        if (dataSource[i].TotalCost == null) {
        //            dataSource[i].TotalCost = 0;
        //        }
        //        if (dataSource[i].Alias != null && dataSource[i].Alias.trim() != "" && dataSource[i].Alias != dataSource[i].Name)
        //            mogdata.push({
        //                "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name + " (" + dataSource[i].Alias + ") ", "UOM_Code": dataSource[i].UOMCode,
        //                "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG,
        //                "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive, "AllergensName": dataSource[i].AllergensName,
        //                "ArticleNumber": dataSource[i].ArticleNumber, "UOMCode": dataSource[i].UOMCode, "DKgValue": dataSource[i].DKgValue
        //            });
        //        else
        //            mogdata.push({
        //                "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName,
        //                "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode,
        //                "AllergensName": dataSource[i].AllergensName, "ArticleNumber": dataSource[i].ArticleNumber, "UOMCode": dataSource[i].UOMCode,
        //                "DKgValue": dataSource[i].DKgValue, "IsActive": dataSource[i].IsActive
        //            });
        //    }
        //    mogdataList = mogdata;

        //}, { siteCode: SiteCode }, true);


        populateRecipeGrid(SiteCode, checkonoff);
        $("#searchspan").show();
        var siteName = dropdownlist.text()
        $("#location").text(" | " + siteName);
        Utility.UnLoading();
        $("#intialImage").hide();
    }, 2000);
}

function populateRecipeGrid(SiteCode, checkonoff) {

    Utility.Loading();
    var gridVariable = $("#gridRecipe");//Hare Krishna!!
    var gridTable;
    gridVariable.html("");
    //if (user.SectorNumber == "80" && user.UserRoleId == 13) {
    //    gridTable = gridVariable.kendoGrid({
    //        excel: {
    //            fileName: "Recipe.xlsx",
    //            filterable: true,
    //            allPages: true
    //        },
    //        //selectable: "cell",
    //        sortable: true,
    //        filterable: {
    //            extra: true,
    //            operators: {
    //                string: {
    //                    contains: "Contains",
    //                    startswith: "Starts with",
    //                    eq: "Is equal to",
    //                    neq: "Is not equal to",
    //                    doesnotcontain: "Does not contain",
    //                    endswith: "Ends with"
    //                }
    //            }
    //        },
    //        groupable: false,
    //        pageable: true,
    //        //reorderable: true,
    //        scrollable: true,
    //        columns: [
    //            {
    //                field: "RecipeCode", title: "Recipe Code", width: "50px", attributes: {
    //                    style: "text-align: left; font-weight:normal"
    //                }
    //            },
    //            {
    //                field: "Name", title: "Recipe Name", width: "100px", attributes: {
    //                    style: "text-align: left; font-weight:normal"
    //                }
    //            },
    //            {
    //                field: "RecipeAlias", title: "Alias Name", width: "100px", attributes: {
    //                    style: "text-align: left; font-weight:normal"
    //                }
    //            },
    //            {
    //                field: "Quantity", title: "Quantity", width: "50px", attributes: {

    //                    style: "text-align: center; font-weight:normal"
    //                },
    //                headerAttributes: {

    //                    style: "text-align: center"
    //                },
    //            },
    //            {
    //                field: "UOMName", title: "UOM", width: "60px", attributes: {

    //                    style: "text-align: center; font-weight:normal"
    //                },
    //                headerAttributes: {

    //                    style: "text-align: center"
    //                },
    //            },

    //            {
    //                field: "RecipeType", title: "Recipe Type", width: "50px", attributes: {

    //                    style: "text-align: center; font-weight:normal"
    //                },
    //                headerAttributes: {

    //                    style: "text-align: center"
    //                },
    //                //  template: '# if (IsBase == false) {#<div>Final Recipe</div>#} else {#<div> Base Recipe</div>#}#',
    //            },
    //            //{
    //            //    field: "Status", title: "Configured", width: "50px", attributes: {
    //            //        class: "mycustomstatus",
    //            //        style: "text-align: center; font-weight:normal"
    //            //    },
    //            //    template: '# if (Status == "1") {#<span>No</span>#} else if (Status == "2") {#<span>Yes</span>#}#',
    //            //    headerAttributes: {

    //            //        style: "text-align: center"
    //            //    },
    //            //},

    //            {
    //                field: "Edit", title: "Action", width: "90px",
    //                headerAttributes: {

    //                    style: "text-align: center"
    //                },

    //                attributes: {
    //                    style: "text-align: center; font-weight:normal"
    //                },
    //                command: [
    //                    //{
    //                    //    name: 'Modify',
    //                    //    click: function (e) {

    //                    //        var gridObj = $("#gridRecipe").data("kendoGrid");
    //                    //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
    //                    //        if (tr.Status == 1)
    //                    //            return;
    //                    //        Utility.Loading();
    //                    //        setTimeout(function () {

    //                    //            datamodel = tr;
    //                    //            RecipeName = tr.Name;
    //                    //            populateUOMDropdown();
    //                    //            recipeCategoryHTMLPopulateOnEditRecipe("", tr);
    //                    //            $("#mainContent").hide();
    //                    //            $("#windowEdit").show();


    //                    //            populateBaseDropdown();
    //                    //            Recipe_ID = tr.Recipe_ID;
    //                    //            populateMOGGrid(tr.Recipe_ID, tr.RecipeCode);
    //                    //            populateBaseRecipeGrid(tr.Recipe_ID, tr.RecipeCode);

    //                    //            $("#recipeAllergens").html(showAllergens(tr.AllergensName));

    //                    //            $("#recipeid").val(tr.ID);
    //                    //            $("#inputrecipecode").val(tr.RecipeCode);
    //                    //            $("#inputrecipename").val(tr.Name);
    //                    //            $("#inputrecipealiasname").val(tr.RecipeAlias);
    //                    //            $("#inputuom").data('kendoDropDownList').value(tr.UOM_ID);
    //                    //            $("#inputquantity").val(tr.Quantity);
    //                    //            if (tr.IsBase == true)
    //                    //                $("#inputbase").data('kendoDropDownList').value("Yes");
    //                    //            else
    //                    //                $("#inputbase").data('kendoDropDownList').value("No");

    //                    //            $('#grandTotal').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    //                    //            $('#grandCostPerKG').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    //                    //            $('#regiongrandTotal').html(Utility.AddCurrencySymbol(tr.RegionTotalCost, 2));
    //                    //            oldcost = tr.TotalCost;// tr.RegionTotalCost;
    //                    //            var editor = $("#inputinstruction").data("kendoEditor");
    //                    //            editor.value(decodeHTMLEntities(tr.Instructions));
    //                    //            var change = 0; //(tr.TotalCost - oldcost).toFixed(2);
    //                    //            var changeperc = 0; //(((change*100) / oldcost)).toFixed(2);
    //                    //            $(".totmoglbl").show();
    //                    //            if (user.SectorNumber == "20") {
    //                    //                $('#inputrecipename').removeAttr("disabled");
    //                    //                $('#inputquantity').removeAttr("disabled");
    //                    //                $('#inputrecipealiasname').removeAttr("disabled");
    //                    //                $("#inputbase").data("kendoDropDownList").enable(true); 
    //                    //                $(".changegrandtotal").css("display", "none");
    //                    //                $("#percentageChangeDiv").css("display", "none");
    //                    //                $("#btnSubmit").show();
    //                    //                $("#btnPublish").show();
    //                    //                $(".totmoglbl").hide();
    //                    //            }
    //                    //            else if (user.SectorNumber == "80") {
    //                    //                $("#btnSubmit").hide();
    //                    //                $("#btnPublish").hide();
    //                    //            }
    //                    //            else {
    //                    //                $("#btnSubmit").show();
    //                    //                $("#btnPublish").show();
    //                    //                $('#inputquantity').attr('disabled', 'disabled');
    //                    //                $('#inputrecipename').attr('disabled', 'disabled');
    //                    //                $('#inputrecipealiasname').attr('disabled', 'disabled');
    //                    //                $("#inputbase").data("kendoDropDownList")   .enable(false); 
    //                    //                $(".changegrandTotal").css("display", "block");
    //                    //                $("#percentageChangeDiv").css("display", "block");
    //                    //                $("#changegrandTotal").html(Utility.AddCurrencySymbol(change, 2));

    //                    //                $("#totalchangepercent").html(Utility.AddCurrencySymbol(changeperc, 2));
    //                    //                if (isNaN(oldcost)) {
    //                    //                    $("#changegrandTotal").hide();
    //                    //                    $("#totalchangepercent").hide();
    //                    //                } else {
    //                    //                    $("#changegrandTotal").show();
    //                    //                    $("#totalchangepercent").show();
    //                    //                }
    //                    //                if (change == 0) {
    //                    //                    $("#changegrandTotal").html("No Change");
    //                    //                }

    //                    //            }



    //                    //            var dropdownlist = $("#inputsite").data("kendoDropDownList");
    //                    //            $("#topHeading").text("Unit Recipe Configuration");//.append("(" + siteName + ")");
    //                    //            hideGrid('gridBaseRecipe', 'emptyBr', 'baseRecipePlus', 'baseRecipeMinus');
    //                    //            hideGrid('gridMOG', 'emptymog', 'mogPlus', 'mogMinus');

    //                    //        }, 2000);
    //                    //        Utility.UnLoading();
    //                    //    }
    //                    //},

    //                    //{
    //                    //    name: 'Map Nutrients',
    //                    //    template: function (dataItem) {
    //                    //        var html = `<div title="Map Nutrients" onClick="viewNutrients(this)"  style='color: #699e9b;margin-left: 50px' class='MapNutrients'>Nutrition Data</div>`;
    //                    //        return html;
    //                    //    }
    //                    //},
    //                    //{
    //                    //    name: 'Print',
    //                    //    template: function (dataItem) {
    //                    //        var html = '<div title="Print" onClick="recipeGridPrint(this)"  style="color:#8f9090; margin-left:8px" class="fa fa-print PrintRecipe"></div>';
    //                    //        return html;
    //                    //    },

    //                    //},
    //                    ,
    //                    {
    //                        title: 'Select',
    //                        //headerTemplate: "<input type='checkbox' id='header-chb' class='k-checkbox-md k-rounded-md header-checkbox'>",
    //                        template: function (dataItem) {
    //                            //return "<input type='checkbox' id='" + dataItem.Id + "' data-code='"+dataItem.RecipeCode+"' class='k-checkbox-md k-rounded-md row-checkbox'>";
    //                            return "<label class='checkbox-container' style='display: inline-block;padding-top:5px' ><input type='checkbox' class='checkbox' id='" + dataItem.Id + "' data-code='" + dataItem.RecipeCode + "' ><span class='checkmarkgrid'></span></label>"
    //                        },
    //                        width: "50px",
    //                        attributes: { style: "text-align: center; font-weight:normal" },
    //                        headerAttributes: { style: "text-align: center" },
    //                    }
    //                ],
    //            }
    //        ],
    //        dataSource: {
    //            sort: { field: "RecipeType", dir: "desc" },
    //            transport: {
    //                read: function (options) {

    //                    var varCodes = "";

    //                    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteRecipeDataList, function (result) {
    //                        Utility.UnLoading();

    //                        if (result != null) {

    //                            options.success(result);
    //                        }
    //                        else {
    //                            options.success("");
    //                        }
    //                    },
    //                        {
    //                            SiteCode: SiteCode, condition: "All"
    //                        }
    //                        , true);
    //                }
    //            },
    //            schema: {
    //                model: {
    //                    id: "ID",
    //                    fields: {
    //                        RecipeCode: { type: "string", editable: false },
    //                        Name: { type: "string", editable: false },
    //                        UOMName: { type: "string", editable: false },
    //                        IsBase: { type: "boolean", editable: false },
    //                        IsActive: { type: "boolean", editable: false },
    //                        RecipeType: { type: "string", editable: false },
    //                    }
    //                }
    //            },
    //            pageSize: 150,
    //        },
    //        columnResize: function (e) {
    //            var grid = gridVariable.data("kendoGrid");
    //            e.preventDefault();
    //        },
    //        noRecords: {
    //            template: "No Records Available"
    //        },
    //        height: 440,
    //        dataBound: function (e) {

    //            var grid = this;
    //            //grid.tbody.find("tr[role='row']").each(function () {
    //            //    var model = grid.dataItem(this);

    //            //    //if (model.Status == 1) {
    //            //    //    $(this).find(".k-grid-Modify").addClass("k-state-disabled");

    //            //    //}

    //            //    ////Krish
    //            //    //for (item of applicationSettingData) {
    //            //    //    if (item.Code == "FLX-00011" && item.IsActive == true && item.Value == "0")//item.Name == "Print Recipe Button Display Sector"
    //            //    //        $(this).find(".PrintRecipe").hide();
    //            //    //    if (item.Code == "FLX-00011" && item.IsActive != true)
    //            //    //        $(this).find(".PrintRecipe").hide();
    //            //    //    if (item.Code == "FLX-00011" && item.IsActive == true && item.Value == "1")
    //            //    //        $(this).find(".PrintRecipe").show();
    //            //    //}
    //            //});
    //            //Krish
    //            grid.table.on("click", ".checkbox", selectRow);
    //            if (checkonoff != "off") {
    //                grid.tbody.find("tr[role='row'] .checkbox").each(function () {
    //                    $(this).click();
    //                });
    //            }
    //            var items = e.sender.items();

    //            var view = this.dataSource.view();
    //            for (var i = 0; i < view.length; i++) {
    //                if (view[i].Status == 1) {
    //                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
    //                        .find(".mycustomstatus").css("background-color", "#ffe1e1");

    //                }
    //                else if (view[i].Status == 2) {
    //                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
    //                        .find(".mycustomstatus").css("background-color", "#ffffbf");
    //                }


    //            }
    //        },
    //        change: function (e) {
    //        },
    //        excelExport: function onExcelExport(e) {
    //            var sheet = e.workbook.sheets[0];
    //            var data = e.data;
    //            var cols = Object.keys(data[0])
    //            var columns = cols.filter(function (col) {
    //                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
    //                else
    //                    return col;
    //            });
    //            var columns1 = columns.map(function (col) {
    //                return {
    //                    value: col,
    //                    autoWidth: true,
    //                    background: "#7a7a7a",
    //                    color: "#fff"
    //                };
    //            });

    //            var rows = [{ cells: columns1, type: "header" }];

    //            for (var i = 0; i < data.length; i++) {
    //                var rowCells = [];
    //                for (var j = 0; j < columns.length; j++) {
    //                    var cellValue = data[i][columns[j]];
    //                    rowCells.push({ value: cellValue });
    //                }
    //                rows.push({ cells: rowCells, type: "data" });
    //            }
    //            sheet.rows = rows;
    //        }
    //    })
    //}
    //else {
    gridTable = gridVariable.kendoGrid({
        excel: {
            fileName: "Recipe.xlsx",
            filterable: true,
            allPages: true
        },
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        //pageable: true,
        pageable: {
            messages: {
                display: "{0}-{1} of {2} records"
            }
        },
        //reorderable: true,
        scrollable: true,
        columns: [
            {
                field: "RecipeCode", title: "Recipe Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Recipe Name", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "RecipeAlias", title: "Alias Name", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Quantity", title: "Quantity", width: "50px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },

            {
                field: "RecipeType", title: "Recipe Type", width: "50px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                //  template: '# if (IsBase == false) {#<div>Final Recipe</div>#} else {#<div> Base Recipe</div>#}#',
            },
  
            {
                title: 'Select',
                headerTemplate: "<span style='margin-right : 5px;'>Include All</span><label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px' ><input type='checkbox' id='header-chb' class='checkbox1' checked><span class='checkmarkgrid'></span></label>",
                //headerTemplate: "<label><input type='checkbox' id='header-chb' class='k-checkbox-md k-rounded-md header-checkbox' checked> Select All</label>",
                template: function (dataItem) {
                    return "<label class='checkbox-container' style='display: inline-block;padding-top:5px' ><input type='checkbox' class='checkbox' id='" + dataItem.Id + "' data-code='" + dataItem.RecipeCode + "' ><span class='checkmarkgrid'></span></label>"
                },
                width: "50px",
                attributes: { style: "text-align: center; font-weight:normal" },
                headerAttributes: { style: "text-align: center" },
            },
            //{
            //    field: "Status", title: "Configured", width: "50px", attributes: {
            //        class: "mycustomstatus",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    template: '# if (Status == "1") {#<span>No</span>#} else if (Status == "2") {#<span>Yes</span>#}#',
            //    headerAttributes: {

            //        style: "text-align: center"
            //    },
            //},

            //{
            //    field: "Edit", title: "Action", width: "90px",
            //    headerAttributes: {

            //        style: "text-align: center"
            //    },

            //    attributes: {
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    //command: [
            //    //    {
            //    //        name: 'Modify',
            //    //        click: function (e) {

            //    //            var gridObj = $("#gridRecipe").data("kendoGrid");
            //    //            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //    //            if (tr.Status == 1)
            //    //                return;
            //    //            Utility.Loading();
            //    //            setTimeout(function () {

            //    //                datamodel = tr;
            //    //                RecipeName = tr.Name;
            //    //                populateUOMDropdown();
            //    //                recipeCategoryHTMLPopulateOnEditRecipe("", tr);
            //    //                $("#mainContent").hide();
            //    //                $("#windowEdit").show();


            //    //                //populateBaseDropdown();
            //    //                Recipe_ID = tr.Recipe_ID;
            //    //                //populateMOGGrid(tr.Recipe_ID, tr.RecipeCode);
            //    //                //populateBaseRecipeGrid(tr.Recipe_ID, tr.RecipeCode);

            //    //                //$("#recipeAllergens").html(showAllergens(tr.AllergensName));

            //    //                $("#recipeid").val(tr.ID);
            //    //                $("#inputrecipecode").val(tr.RecipeCode);
            //    //                $("#inputrecipename").val(tr.Name);
            //    //                $("#inputrecipealiasname").val(tr.RecipeAlias);
            //    //                $("#inputuom").data('kendoDropDownList').value(tr.UOM_ID);
            //    //                $("#inputquantity").val(tr.Quantity);
            //    //                if (tr.IsBase == true)
            //    //                    $("#inputbase").data('kendoDropDownList').value("Yes");
            //    //                else
            //    //                    $("#inputbase").data('kendoDropDownList').value("No");

            //    //                $('#grandTotal').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
            //    //                $('#grandCostPerKG').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
            //    //                $('#regiongrandTotal').html(Utility.AddCurrencySymbol(tr.RegionTotalCost, 2));
            //    //                oldcost = tr.TotalCost;// tr.RegionTotalCost;
            //    //                var editor = $("#inputinstruction").data("kendoEditor");
            //    //                editor.value(decodeHTMLEntities(tr.Instructions));
            //    //                var change = 0; //(tr.TotalCost - oldcost).toFixed(2);
            //    //                var changeperc = 0; //(((change*100) / oldcost)).toFixed(2);
            //    //                $(".totmoglbl").show();
            //    //                if (user.SectorNumber == "20") {
            //    //                    $('#inputrecipename').removeAttr("disabled");
            //    //                    $('#inputquantity').removeAttr("disabled");
            //    //                    $('#inputrecipealiasname').removeAttr("disabled");
            //    //                    $("#inputbase").data("kendoDropDownList").enable(true);
            //    //                    $(".changegrandtotal").css("display", "none");
            //    //                    $("#percentageChangeDiv").css("display", "none");
            //    //                    $("#btnSubmit").show();
            //    //                    $("#btnPublish").show();
            //    //                    $(".totmoglbl").hide();
            //    //                }
            //    //                else if (user.SectorNumber == "80") {
            //    //                    $("#btnSubmit").hide();
            //    //                    $("#btnPublish").hide();
            //    //                }
            //    //                else {
            //    //                    $("#btnSubmit").show();
            //    //                    $("#btnPublish").show();
            //    //                    $('#inputquantity').attr('disabled', 'disabled');
            //    //                    $('#inputrecipename').attr('disabled', 'disabled');
            //    //                    $('#inputrecipealiasname').attr('disabled', 'disabled');
            //    //                    $("#inputbase").data("kendoDropDownList").enable(false);
            //    //                    $(".changegrandTotal").css("display", "block");
            //    //                    $("#percentageChangeDiv").css("display", "block");
            //    //                    $("#changegrandTotal").html(Utility.AddCurrencySymbol(change, 2));

            //    //                    $("#totalchangepercent").html(Utility.AddCurrencySymbol(changeperc, 2));
            //    //                    if (isNaN(oldcost)) {
            //    //                        $("#changegrandTotal").hide();
            //    //                        $("#totalchangepercent").hide();
            //    //                    } else {
            //    //                        $("#changegrandTotal").show();
            //    //                        $("#totalchangepercent").show();
            //    //                    }
            //    //                    if (change == 0) {
            //    //                        $("#changegrandTotal").html("No Change");
            //    //                    }

            //    //                }
            //    //                var dropdownlist = $("#inputsite").data("kendoDropDownList");
            //    //                $("#topHeading").text("Unit Recipe Configuration");//.append("(" + siteName + ")");
            //    //                hideGrid('gridBaseRecipe', 'emptyBr', 'baseRecipePlus', 'baseRecipeMinus');
            //    //                hideGrid('gridMOG', 'emptymog', 'mogPlus', 'mogMinus');
            //    //                dkgtotal();
            //    //            }, 2000);
            //    //            Utility.UnLoading();
            //    //        }
            //    //    },

            //    //],
            //}
        ],
        dataSource: {
            sort: { field: "RecipeType", dir: "desc" },
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteRecipeDataList, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }
                    },
                        {
                            SiteCode: SiteCode, condition: "All"
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        RecipeCode: { type: "string", editable: false },
                        Name: { type: "string", editable: false },
                        UOMName: { type: "string", editable: false },
                        IsBase: { type: "boolean", editable: false },
                        IsActive: { type: "boolean", editable: false },
                        RecipeType: { type: "string", editable: false },
                    }
                }
            },
            pageSize: 150,
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        height: 440,
        dataBound: function (e) {

            var grid = this;
            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);

                if (model.Status == 1) {
                    $(this).find(".k-grid-Modify").addClass("k-state-disabled");

                }


            });

            //Krish
            grid.table.on("click", ".checkbox", selectRow);
            //if (checkonoff != "off") {
            grid.tbody.find("tr[role='row'] .checkbox").each(function () {
                $(this).click();
            });
            //}
            //else {
            //    checkedRecipeIds = [];
            //}

            var items = e.sender.items();

            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (view[i].Status == 1) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffe1e1");

                }
                else if (view[i].Status == 2) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffffbf");
                }

                //Krish
                if (checkedIds[view[i].id]) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .addClass("k-state-selected1")
                        .find(".k-checkbox-md")
                        .attr("checked", "checked");
                }
            }

        },
        change: function (e) {
        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });

            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }
    })

    //gridTable.data("kendoGrid").table.on("click", ".checkbox", selectRow);

    $('#header-chb').change(function (ev) {
        var checked = ev.target.checked;
        Utility.Loading();
        $('.checkbox').each(function (idx, item) {
            $(item).click();
            //if (checked) {
            //    if (!($(item).closest('tr').is('.k-state-selected1'))) {
            //        $(item).click();
            //    }
            //} else {
            //    if ($(item).closest('tr').is('.k-state-selected1')) {
            //        $(item).click();
            //    }
            //}
        });
        Utility.UnLoading();
    });

    $("#showSelection").bind("click", function () {
        var checked = [];
        for (var i in checkedIds) {
            if (checkedIds[i]) {
                checked.push(i);
            }
        }

        alert(checked);
    });
    //}
}

var checkedIds = {};
var checkedRecipeIds = [];

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = $("#gridRecipe").data("kendoGrid"),
        dataItem = grid.dataItem(row);
    //console.log(dataItem)
    checkedIds[dataItem.Id] = checked;
    checkedRecipeIds.push(dataItem.RecipeCode);

    if (checked) {
        //-select the row
        row.addClass("k-state-selected1");

        //var checkHeader = true;

        //$.each(grid.items(), function (index, item) {
        //    if (!($(item).hasClass("k-state-selected1"))) {
        //        checkHeader = false;
        //    }
        //});

        //$("#header-chb")[0].checked = checkHeader;
    } else {
        //-remove selection
        //row.removeClass("k-state-selected1");
        checkedRecipeIds = checkedRecipeIds.filter(a => a != dataItem.RecipeCode);
        //$("#header-chb")[0].checked = false;
    }

    //console.log(checkedRecipeIds)
}

//on dataBound event restore previous selected rows:
//function onDataBound(e) {
//    var view = this.dataSource.view();
//    for (var i = 0; i < view.length; i++) {
//        if (checkedIds[view[i].id]) {
//            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
//                .addClass("k-state-selected")
//                .find(".k-checkbox")
//                .attr("checked", "checked");
//        }
//    }
//}