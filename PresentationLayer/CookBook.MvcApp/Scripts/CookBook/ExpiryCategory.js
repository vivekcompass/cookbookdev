﻿var user;
var datamodel;
var ShelfLifeUOMData = [];
var ExpiryCategorydata = [];
var ExpiryCategoryCodeIntial = null;
function populateShelfLifedrpodown() {
    $("#dpShelfLifeUOM").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "ShelfLifeUOMCode",
        dataSource: ShelfLifeUOMData,
        index: 0,

        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In-Active State");
                var dropdownlist = $("#dpShelfLifeUOM").data("kendoDropDownList");
                dropdownlist.select("");
            }

        }
    });
}

$(document).ready(function () {
    $(".k-window").hide();
    $(".k-overlay").hide(); 
    $("#ReaswindowEdit").kendoWindow({
        modal: true,
        width: "300px",

        title: "ExpiryCategory Details  ",
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $('#myInput').on('input', function (e) {
        var grid = $('#gridExpiryCategory').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ExpiryCategoryCode" || x.field == "ShelfLife" || x.field == "ShelfLifeName" || x.field == "Name" || x.field == "ShelfLife") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    HttpClient.MakeSyncRequest(CookBookMasters.GetShelfLifeUOMDataList, function (result) {
        dataSource = result;
        ShelfLifeUOMData.push({ "value": null, "text": "Select", "ShelfLifeUOMCode": null, "IsActive": 1 });
        for (var i = 0; i < dataSource.length; i++) {
            ShelfLifeUOMData.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "ShelfLifeUOMCode": dataSource[i].ShelfLifeUOMCode, "IsActive": dataSource[i].IsActive });
        }
        populateShelfLifedrpodown();
      
       populateExpiryCategoryGrid();
    }, null, false);
});

function populateExpiryCategoryGrid() {

    Utility.Loading();
    var gridVariable = $("#gridExpiryCategory");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: {
            pageSize: 15,
            messages: {
                display: "{0}-{1} of {2} records"
            }
        },
        groupable: false,
        columns: [
            {
                field: "ExpiryCategoryCode", title: "Code", width: "70px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Expiry Category", width: "275px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ShelfLife", title: "Shelf Life", width: "250px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ShelfLifeName", title: "Shelf Life UOM", width: "250px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            //{
            //    field: "IsActive", title: "Active", width: "100px", attributes: {

            //        style: "text-align: center; font-weight:normal"
            //    },
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
            //},
            {
                field: "Edit", title: "Action", width: "70px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                command: [
                    {
                        name: 'Edit',
                        click: function (e) {
                           var tr = $(e.target).closest("tr");    // get the current table row (tr)
                            var item = this.dataItem(tr);
                            if (!item.IsActive) {
                                return;
                            }// get the date of this row
                            var model = {
                                "ID": item.ID,
                                "Name": item.Name,
                                "IsActive": item.IsActive,
                                "ExpiryCategoryCode": item.ExpiryCategoryCode,
                                "ShelfLifeUOMCode": null,
                                "ShelfLife": null,
                                "CreatedBy": item.CreatedBy,
                                "CreatedOn": kendo.parseDate(item.CreatedOn)

                            };
                            ExpiryCategoryCodeIntial = item.ExpiryCategoryCode;
                            var dialog = $("#ReaswindowEdit").data("kendoWindow");
                               dialog.title("Expiry Category Details : ");
                            model.ShelfLifeUOMCode = item.ShelfLifeUOMCode;
                            model.ShelfLife = item.ShelfLife;
                           
                           
                            dialog.open();
                            dialog.center();

                            datamodel = model;

                            $("#dpid").val(model.ID);
                            $("#dpcode").val(model.ExpiryCategoryCode);
                            $("#dpname").val(model.Name);
                            $("#dpshelflife").val(model.ShelfLife);
                            $("#dpShelfLifeUOM").data('kendoDropDownList').value(model.ShelfLifeUOMCode);
                            $("#dpname").focus();


                            return true;
                        }
                    }
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetExpiryCategoryDataList, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            //Result modifiy
                            ExpiryCategorydata = result;
                          
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }
                    }, null
                        //{
                        //filter: mdl
                        //}
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ShelfLifeUOMName: { type: "string" },
                        ExpiryCategoryCode: { type: "string" },
                        ExpiryCategoryDescription: { type: "string" },
                        IsActive: { type: "Boolean" },
                        Name: {type:"string"},
                        ShelfLife: { type: "string" },
                        ShelfLifeName: { type: "string" },
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var items = e.sender.items();
            var grid = this;
            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);

                if (!model.IsActive) {
                    $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                }
            });
            
            $(".chkbox").on("change", function () {
                var gridObj = $("#gridExpiryCategory").data("kendoGrid");
                var tr = gridObj.dataItem($(this).closest("tr"));
                var trEdit = $(this).closest("tr");
                var th = this;
                datamodel = tr;
                datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);

                datamodel.IsActive = $(this)[0].checked;
                var active = "";
                var msg = "";
                if (datamodel.IsActive) {
                    active = "Active";
                } else {
                    active = "Inactive";
                }
                     msg = "ExpiryCategory ";

                

                Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b>" + msg + " " + active + "?", " " + msg + " Update Confirmation", "Yes", "No", function () {
                    HttpClient.MakeRequest(CookBookMasters.SaveExpiryCategory, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again");
                        }
                        else {
                            toastr.success(msg + " updated successfully");
                            $(th)[0].checked = datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        }
                    }, {
                        model: datamodel
                    }, false);
                }, function () {
                    datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);

                    $(th)[0].checked = !datamodel.IsActive;
                    if ($(th)[0].checked) {
                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                    } else {
                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
                return true;
            });

        },
        change: function (e) {
        },
    })
    gridVariable.data("kendoGrid").dataSource.sort({ field: "ExpiryCategoryCode", dir: "asc" });
  
}
function dpvalidate() {
    var valid = true;

    if ($("#dpname").val() === "") {
        toastr.error("Please provide input");
        $("#dpname").addClass("is-invalid");
        valid = false;
        $("#dpname").focus();
    }
    if (($.trim($("#dpname").val())).length > 59) {
        toastr.error("ExpiryCategory Description can't accepts upto 60 charactre");

        $("#dpname").addClass("is-invalid");
        valid = false;
        $("#dpname").focus();
    }
    return valid;
}
$(document).ready(function (){
    $("#dpaddnew").on("click", function () {
        var model;
        var dialog = $("#ReaswindowEdit").data("kendoWindow");

        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });

        datamodel = model;

        dialog.title("New ExpiryCategory Details Creation");




        $("#dpname").removeClass("is-invalid");
        $('#secredpsubmit').removeAttr("disabled");
        $("#dpid").val("");
        $("#dpname").val("");
        $("#dpcode").val("");
        $("#dpshelflife").val("");
        $("#dpname").focus();
        $("#dpShelfLifeUOM").data('kendoDropDownList').value(null)

        ExpiryCategoryCodeIntial = null;
    });

    $("#secredpcancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#ReaswindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });
    $("#secredpsubmit").click(function () {
        var msg = "";
       // if (user.SectorNumber == "20") {
            msg = "ExpiryCategory";
       
        var namedp = $("#dpname").val().toLowerCase().trim();
        if (ExpiryCategoryCodeIntial == null) { //Duplicate Check
            for (item of ExpiryCategorydata) {
                if (item.Name.toLowerCase().trim() == namedp) {
                    toastr.error("Kindly check Duplicate " + msg + " Description");
                    $("#dpname").focus();
                    return;
                }
            }
        } else {
            for (item of ExpiryCategorydata) {
                if (item.Name?.toLowerCase().trim() == namedp && item.ExpiryCategoryCode != ExpiryCategoryCodeIntial) {
                    toastr.error("Kindly check Duplicate  " + msg + " Description is entered on editing");
                    $("#dpname").focus();
                    return;
                }
            }
        }
        if ($("#dpname").val() === "" || $("#dpname").val() == null) {
            toastr.error("Please provide " + msg + " Name");
            $("#dpname").focus();
            return;
        }
       // if (user.SectorNumber == "20") {
            if ($("#dpShelfLifeUOM").val() == "" || $("#dpShelfLifeUOM").val() == null) {
                toastr.error("Please provide " + msg + " Shelf Life UOM");
                $("#dpShelfLifeUOM").focus();
                return;
            }
       // }
        if (dpvalidate() === true) {
            $("#dpname").removeClass("is-invalid");

            var model = {
                "ID": $("#dpid").val(),
                "Name": $("#dpname").val(),
                "ExpiryCategoryCode": ExpiryCategoryCodeIntial,
                "ShelfLifeUOMCode": $("#dpShelfLifeUOM").val(),
                "ShelfLife": $("#dpshelflife").val(),
            }
            if (ExpiryCategoryCodeIntial == null) {
                model.IsActive = 1;
            } else {
                model.IsActive = 1;
                model.CreatedBy = datamodel.CreatedBy;
                model.CreatedOn = datamodel.CreatedOn;

            }
            model.ShelfLifeUOMCode = $("#dpShelfLifeUOM").data('kendoDropDownList').value()
            if (!sanitizeAndSend(model)) {
                return;
            }
            $("#secredpsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveExpiryCategory, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#secredpsubmit').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#secredpsubmit').removeAttr("disabled");
                        toastr.error(result.message);
                        Utility.UnLoading();
                        return;
                    }
                    else {
                        if (result == false) {
                            $('#secredpsubmit').removeAttr("disabled");
                            toastr.success("There was some error, the record cannot be saved");

                        }
                        else {
                            $(".k-overlay").hide();
                            $('#secredpsubmit').removeAttr("disabled");

                            if (model.ID > 0)
                                toastr.success(msg + " record updated successfully");
                            else
                                toastr.success("New" + msg + " record added successfully");
                            $(".k-overlay").hide();
                            var orderWindow = $("#ReaswindowEdit").data("kendoWindow");
                            orderWindow.close();
                            $("#gridExpiryCategory").data("kendoGrid").dataSource.data([]);
                            $("#gridExpiryCategory").data("kendoGrid").dataSource.read();
                            $("#gridExpiryCategory").data("kendoGrid").dataSource.sort({ field: "ExpiryCategoryCode", dir: "asc" });
                        }
                    }
                }
            }, {
                model: model

            }, true);
        }
    });


});
