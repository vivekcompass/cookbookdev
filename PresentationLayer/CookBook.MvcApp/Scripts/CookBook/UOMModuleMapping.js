﻿var user;
var conceptype2data;
var tileImage = null;
var item = null;
var UOMCodeIntial = null;
var datamodel;
var completedata = [];
var uomdata = [];
var uommoduledata = [];
var uommodulemappingdata = [];

$(function () {
    $('#myInput').on('input', function (e) {
        var grid = $('#gridUOM').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "UOMName" || x.field == "UOMModuleName") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#RTwindowEdit").kendoWindow({
            modal: true,
            width: "440px",
            height: "200px",
            title: "UOM Module Mapping Details  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });


        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;

            populateUOMGrid();
        }, null, false);

        HttpClient.MakeSyncRequest(CookBookMasters.GetUOMModuleDataList, function (result) {
            dataSource = result;
            uommodulemappingdata = [];
            uommodulemappingdata = dataSource;
            uommoduledata = [];
            uommoduledata.push({ "value": null, "text": "Select", "UOMModuleCode": null, "IsActive": 1 });
            for (var i = 0; i < dataSource.length; i++) {
                uommoduledata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "UOMModuleCode": dataSource[i].UOMModuleCode, "IsActive": dataSource[i].IsActive });
            }
            populateUOMModuledrpodown();

        }, null, false);

        HttpClient.MakeSyncRequest(CookBookMasters.GetUOMDataList, function (result) {
            dataSource = result;

            uomdata = [];
            uomdata.push({ "value": null, "text": "Select", "UOMCode": null, "IsActive": 1 });
            for (var i = 0; i < dataSource.length; i++) {
                uomdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode, "IsActive": dataSource[i].IsActive });
            }
            populateUOMMultiselectMasterDropdown();

        }, null, false);
    });



    function populateUOMdrpodown() {
        $("#ddluom").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "UOMCode",
            dataSource: uomdata,
            index: 0,
        });
    }

    function populateUOMModuledrpodown() {
        $("#ddluommodule").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "UOMModuleCode",
            dataSource: uommoduledata,
            index: 0,
        });
    }

    function populateUOMMultiselectMasterDropdown() {

        $("#inputuom").kendoMultiSelect({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "UOMCode",
            dataSource: uomdata,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
        });
        var multiselect = $("#inputuom").data("kendoMultiSelect");

        //clear filter
        //multiselect.dataSource.filter({});

        //set value
        // multiselect.value(mogWiseAPLArray);

    }

    function uomvalidate() {
        var valid = true;

        if ($("#UOMModuleMappingname").val() === "") {
            toastr.error("Please provide input");
            $("#UOMModuleMappingname").addClass("is-invalid");
            valid = false;
            $("#UOMModuleMappingname").focus();
        }
        if (($.trim($("#UOMModuleMappingname").val())).length > 59) {
            toastr.error("UOM Module Mapping Name accepts upto 60 character");

            $("#UOMModuleMappingname").addClass("is-invalid");
            valid = false;
            $("#UOMModuleMappingname").focus();
        }
        return valid;
    }
    //UOM Section Start

    function populateUOMGrid() {

        Utility.Loading();
        var gridVariable = $("#gridUOM");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [

                {
                    field: "UOMModuleName", title: "Module", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive", title: "Active", width: "75px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)

                                var item = this.dataItem(tr);          // get the date of this row

                                if (!item.IsActive) {
                                    return;
                                }
                                var model = {
                                    "ID": item.ID,
                                    "UOMCode": item.UOMCode,
                                    "UOMModuleCode": item.UOMModuleCode,
                                    "IsActive": item.IsActive,
                                    "CreatedBy": item.CreatedBy,
                                    "CreatedOn": kendo.parseDate(item.CreatedOn)
                                };
                                UOMCodeIntial = item.ID;
                                var dialog = $("#RTwindowEdit").data("kendoWindow");

                                dialog.open();
                                dialog.center();

                                datamodel = model;

                                $("#uomid").val(model.ID);
                                $("#ddluommodule").data('kendoDropDownList').value(model.UOMModuleCode);
                                var UOMCodeData = [];
                                var multiselect = $("#inputuom").data("kendoMultiSelect");
                                multiselect.dataSource.filter({});
                                multiselect.value([""]);
                                if (item.UOMCode != undefined && item.UOMCode != null) {
                                    item.UOMCode = item.UOMCode.replaceAll(" ", "");
                                    var array = item.UOMCode.split(',');
                                    if (item.UOMCode != undefined && item.UOMCode != null) {
                                        for (health of array) {
                                            UOMCodeData.push(health.toString());
                                        }
                                    }
                                    multiselect.value(UOMCodeData);
                                }
                                $("#UOMModuleMappingname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetUOMModuleMappingGridData, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                if (user.SectorNumber !== "20") {
                                    result = result.filter(m => m.UOMModuleCode != "UMM-00004"
                                        && m.UOMModuleCode != "UMM-00005" && m.UOMModuleCode != "UMM-00006");
                                }
                                options.success(result);
                                completedata = result;
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            UOMModuleName: { type: "string" },
                            UOMName: { type: "string" },

                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
                
                $(".chkbox").on("change", function () {
                    var gridObj = $("#gridUOM").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    var trEdit = $(this).closest("tr");
                    var th = this;
                    datamodel = tr;
                    datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.UOMModuleName + "</b> UOM Module Mapping " + active + "?", " UOM Module Mapping Update Confirmation", "Yes", "No", function () {
                        HttpClient.MakeRequest(CookBookMasters.SaveUOMModuleMapping, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                toastr.success("UOM Module Mapping updated successfully");
                                if ($(th)[0].checked) {
                                    $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                } else {
                                    $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                }
                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {

                        $(th)[0].checked = !datamodel.IsActive;
                        if ($(th)[0].checked) {
                            $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                        } else {
                            $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                        }
                    });
                    return true;
                });

            },
            change: function (e) {
            },
        })

        //  var gridVariable = $("#gridUOM").data("kendoGrid");
        //sort Grid's dataSource
        gridVariable.data("kendoGrid").dataSource.sort({ field: "UOMModuleMappingCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function modifyUOMModuleMappingGrid() {
        const outputObj = completedata.reduce((accum, { UOMModuleCode, UOMName }) => {
            if (!accum[UOMModuleCode]) accum[UOMModuleCode] = UOMName;
            else accum[UOMModuleCode] += ',' + UOMName;
            return accum;
        }, {});
        const output = Object.entries(outputObj).map(([UOMModuleCode, UOMName]) => ({ UOMModuleCode, UOMName }));
        console.log(output);
    }


    $("#UOMModuleMappingcancel").on("click", function () {
        var orderWindow = $("#RTwindowEdit").data("kendoWindow");
        orderWindow.close();
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?"," UOM Module Mapping Cancel Confirmation", "Yes", "No", function () {
            $(".k-overlay").hide();
        }, function () {


            var orderWindow = $("#RTwindowEdit").data("kendoWindow");
            orderWindow.open();
        });


    });

    $("#UOMModuleMappingnew").on("click", function () {
        var model;
        var dialog = $("#RTwindowEdit").data("kendoWindow");

        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });

        datamodel = model;

        dialog.title("New UOM Mapping Creation");

        $("#UOMModuleMappingname").removeClass("is-invalid");
        $('#UOMModuleMappingsubmit').removeAttr("disabled");
        $("#uomid").val("");
        $("#ddluommodule").data('kendoDropDownList').value(null)
        var multiSelect = $('#inputuom').data("kendoMultiSelect");
        multiSelect.value([]);
        $("#ddluom").focus();
        UOMCodeIntial = null;
    });

    $("#UOMModuleMappingsubmit").click(function () {
        var inputuom = [];
        var dropdownlist = $("#inputuom").data("kendoMultiSelect");
        var dataItems = dropdownlist.dataItems();
        if (dataItems != undefined && dataItems != null) {
            for (dataItem of dataItems) {
                inputuom.push(dataItem.UOMCode);
            }
        }

        if (inputuom === null || inputuom === "" || inputuom === undefined || inputuom.length === 0) {
            toastr.error("Please select a UOM");
            $("#inputuom").focus();
            return;
        }
        if ($("#ddluommodule").val() === "") {
            toastr.error("Please select a UOM Module");
            $("#ddluommodule").focus();
            return;
        }
        if (UOMCodeIntial == null) { //Duplicate Check
            debugger;
            for (item of completedata) {
                if (item.UOMModuleCode == $("#ddluommodule").val()) {
                    toastr.error("Please check UOM Module Mapping already Exists.");
                    $("#dduom").focus();
                    return;
                }
            }
        }

      

        var model = {
            "ID": $("#uomid").val(),
            "UOMCode": inputuom.toString(),
            "UOMModuleCode": $("#ddluommodule").val(),
            "IsActive": 1
        }
        if (UOMCodeIntial == null) {
            model.IsActive = 1;
        } else {
            model.CreatedBy = datamodel.CreatedBy;
            model.CreatedOn = datamodel.CreatedOn;
        }
        if (!sanitizeAndSend(model)) {
            return;
        }
        $("#UOMModuleMappingsubmit").attr('disabled', 'disabled');
        HttpClient.MakeRequest(CookBookMasters.SaveUOMModuleMapping, function (result) {
            if (result.xsssuccess !== undefined && !result.xsssuccess) {
                toastr.error(result.message);
                $('#UOMModuleMappingsubmit').removeAttr("disabled");
                Utility.UnLoading();
            }
            else {
                if (result == false) {
                    $('#UOMModuleMappingsubmit').removeAttr("disabled");
                    toastr.success("There was some error, the record cannot be saved");

                }
                else {
                    $(".k-overlay").hide();
                    $('#UOMModuleMappingsubmit').removeAttr("disabled");

                    if (model.ID > 0)
                        toastr.success("UOM Module Mapping configuration updated successfully");
                    else
                        toastr.success("New UOM Module Mapping added successfully");
                    $(".k-overlay").hide();
                    var orderWindow = $("#RTwindowEdit").data("kendoWindow");
                    orderWindow.close();
                    $("#gridUOM").data("kendoGrid").dataSource.data([]);
                    $("#gridUOM").data("kendoGrid").dataSource.read();
                    $("#gridUOM").data("kendoGrid").dataSource.sort({ field: "UOMModuleMappingCode", dir: "asc" });
                }
            }
        }, {
            model: model

        }, true);

    });



});