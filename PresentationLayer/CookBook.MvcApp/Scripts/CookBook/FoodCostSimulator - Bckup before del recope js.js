﻿//import { remove } from "toastr";
var dishdata = [];
var sitedishcatlist = [];
var dishcatwithdishData = [];
var viewOnly = 0;
$(function () {
    var filteredSimulationDataSource = [];
    var status = "";
    var varname = "";
    var datamodel;
    var ColorName = "";
    var sitedata = [];
    var simdata = [];
    var regionmasterdata = [];
    var sitemasterdata = [];
    var daypartdata = [];
    var itemdata = [];
    var dkdata = [];
    var user;
    var itemsMulti;
    var itemsid = [];
    var selItems = [];
    var selDparts = [];
    var sitedishlist = [];
    var SiteCode;


    $("#FCSimulatorAdd").hide();
    $('#myInput').on('input', function (e) {
        var grid = $('#gridSimulationMaster').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "SimulationCode" || x.field == "Name" || x.field == "Status" || x.field == "Version") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    //$("#ColorwindowEdit").kendoWindow({
    //    modal: true,
    //    width: "340px",
    //    height: "250px",
    //    title: "Color Details - " + ColorName,
    //    actions: ["Close"],
    //    visible: false,
    //    animation: false,
    //    resizable: false
    //});

    //$('.my-colorpicker2').colorpicker()

    //$('.my-colorpicker2').on('colorpickerChange', function (event) {
    //    if (event.color != null) {
    //        $("#inputrgbcode").val(event.color.toRgbString());
    //        $("#inputhexcode").val(event.color.toString());
    //        $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    //        $('.my-colorpicker2 .fa-square').css('background-color', event.color.toString());
    //        $('.my-colorpicker2 .input-group-text').css('background-color', event.color.toString());
    //    }
    //});

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    var user;
    $(document).ready(function () {
        Utility.Loading();
        $("#topHeading").text("Food Cost Simulator");
        $("#radNewSimulation").attr("checked", true);
        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#divDishCategory").kendoWindow({
            modal: true,
            //width: "250px",
            //height: "170px",
            title: "Dish Categories  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });
        $("#btnExport").click(function (e) {
            var grid = $("#gridColor").data("kendoGrid");
            grid.saveAsExcel();
        });

        HttpClient.MakeRequest(CookBookMasters.GetSimulationDataListWithPaging, function (result) {
            Utility.UnLoading();
            filteredSimulationDataSource = result;
            //For Simulation dropdown
            var dataSource = result;
            simdata = [];
            simdata.push({ "value": "Select", "text": "Select Simulation" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].IsActive) {
                    var version = "";
                    if (dataSource[i].Version != null && dataSource[i].Version != "null")
                        version = dataSource[i].Version;
                    simdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name + " " + version });
                }
            }
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNew").css("display", "none");
            //}
            populateSimulationGrid();
        }, null, false);

        //User sector
        HttpClient.MakeSyncRequest(CookBookLayout.GetLoginUserDetailsUrl, function (result) {
            user = result;
        }, null, false);

        getDishCategoriesWithDishes();
    });

    function populateSimulationGrid() {

        var gridVariable = $("#gridSimulationMaster");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "FoodCostSimulation.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                //{
                //    field: "SimulationCode", title: "Simulation Code", width: "40px", attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    }
                //},
                {
                    field: "SimulationCode",
                    title: "Simulation Code", width: "140px",
                    attributes: {
                        style: "text-align: left; font-weight:normal;text-transform: uppercase;"
                    },
                    headerAttributes: {
                        style: "text-align: left;"
                    }
                },
                {
                    field: "Name", title: "Name", width: "auto", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Status", title: "Status", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "Version", title: "Version", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "CreateOn", title: "Date & Time", width: "135px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    template: '#= kendo.toString(kendo.parseDate(CreateOn), "dd MMM yyyy hh:mm tt")#'
                },
                {
                    field: "IsActive",
                    title: "Active", width: "75px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                },
                {
                    field: "Edit", title: "Action", width: "100px",
                    field: "Edit", title: "Action", width: "100px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = $("#gridSimulationMaster").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                var simId = tr.ID;
                                HttpClient.MakeRequest(CookBookMasters.GetSimulationBoardData, function (result) {
                                    console.log("result");
                                    console.log(result);

                                    $("#SimulationID").val(result.ID);
                                    $("#NewSimNoOfDays").val(result.NoOfDays);
                                    $("#txtSimulationName").val(result.SimulationName);
                                    var version = "";
                                    if (result.Version != null && result.Version != "null")
                                        version = result.Version;

                                    $("#divSimConfig input").attr("disabled", "disabled");
                                    $("#divSimConfig").addClass("divDisabled");

                                    //dropdownlist.enable(true);
                                    viewOnly = 0;

                                    $("#spanHeadertxt").html("Update Simulation " + result.SimulationName + " " + version);

                                    //get sitedish data if not generated
                                    //if(sitedish)

                                    populateExistingBoard(result);


                                    var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
                                    dropdownlist.enable(false);
                                    dropdownlist = $("#selectFromSite").data("kendoDropDownList");
                                    dropdownlist.enable(false);

                                }, { simId: simId }, false);

                                return true;
                            }
                        }
                        ,
                        {
                            name: 'View',
                            click: function (e) {
                                var gridObj = $("#gridSimulationMaster").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                var simId = tr.ID;
                                Utility.Loading();
                                HttpClient.MakeRequest(CookBookMasters.GetSimulationBoardData, function (result) {

                                    $("#SimulationID").val(result.ID);
                                    $("#NewSimNoOfDays").val(result.NoOfDays);
                                    $("#txtSimulationName").val(result.SimulationName);
                                    var version = "";
                                    if (result.Version != null && result.Version != "null")
                                        version = result.Version;

                                    $("#divSimConfig input").attr("disabled", "disabled");
                                    $("#divSimConfig").addClass("divDisabled");

                                    //dropdownlist.enable(true);
                                    viewOnly = 1;
                                    $("#spanHeadertxt").html("View Simulation " + result.SimulationName + " " + version);
                                    populateExistingBoard(result);

                                    $("#divSimConfig").hide();
                                    $("#divSimFilter").hide();
                                    $(".card-tools").hide();

                                    var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
                                    dropdownlist.enable(false);
                                    dropdownlist = $("#selectFromSite").data("kendoDropDownList");
                                    dropdownlist.enable(false);


                                }, { simId: simId }, false);
                                return true;
                            }
                        },
                        {
                            name: 'Print',
                            click: function (e) {
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                data: filteredSimulationDataSource,
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false, type: "string" },
                            SimulationCode: { type: "string" },
                            Status: { type: "string" },
                            Version: { type: "string" },
                            IsActive: { editable: false },
                            CreateOn: { type: 'string' },
                            RegionID: { type: "int" },
                            SiteID: { type: "int" },
                            NoOfDays: { type: "int" },
                            CreatedBy: { type: "int" },
                            ModifiedBy: { type: "int" },
                            ModifiedOn: { type: "string" },
                        }
                    }
                },
                pageSize: 100,
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive || model.Status == "Final") {
                        //$(this).find(".k-grid-Edit").addClass("k-state-disabled");
                        $(this).find(".k-grid-Edit").hide();
                    }
                    if (model.Status == "Final") {
                        $(this).find(".k-grid-View").show();
                    }
                    else {
                        $(this).find(".k-grid-View").hide();
                    }
                });

                $(".chkbox").on("change", function () {
                    // 
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridSimulationMaster").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    var th = this;
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }
                    console.log("statusmodel")
                    console.log(datamodel)
                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Simulation " + active + "?", "Simulation Update Confirmation", "Yes", "No", function () {
                        $("#error").css("display", "none");
                        HttpClient.MakeRequest(CookBookMasters.SaveStatusSimulationData, function (result) {
                            if (result == false) {
                                //$("#error").css("display", "flex");
                                //$("#error").find("p").text("Some error occured, please try again");
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                //$("#error").css("display", "flex");
                                //$("#success").css("display", "flex");
                                //$("#success").find("p").text("Color updated successfully");
                                toastr.success("Simulation updated successfully");

                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {
                        $(th)[0].checked = !datamodel.IsActive;
                        //$("#gridColor").data("kendoGrid").dataSource.data([]);
                        //$("#gridColor").data("kendoGrid").dataSource.read();
                    });
                    return true;
                });
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            excelExport: function onExcelExport(e) {
                //var sheet = e.workbook.sheets[0];
                //var data = e.data;
                //var cols = Object.keys(data[0])
                //var columns = cols.filter(function (col) {
                //    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                //    else
                //        return col;
                //});
                //var columns1 = columns.map(function (col) {
                //    return {
                //        value: col,
                //        autoWidth: true,
                //        background: "#7a7a7a",
                //        color: "#fff"
                //    };
                //});
                //console.log(columns1);
                //var rows = [{ cells: columns1, type: "header" }];

                //for (var i = 0; i < data.length; i++) {
                //    var rowCells = [];
                //    for (var j = 0; j < columns.length; j++) {
                //        var cellValue = data[i][columns[j]];
                //        rowCells.push({ value: cellValue });
                //    }
                //    rows.push({ cells: rowCells, type: "data" });
                //}
                //sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function populateExistingBoard(result) {

        var foodCostBoard = result.FoodCostSimulationBoard;
        //Data Object
        //itemdata.push({ "value": "Select", "text": "Select Items", "code": "Select", "dpcode": "", "DishCategories": [] });
        //for (var i = 0; i < dataSource.length; i++) {
        //    itemdata.push({ "value": dataSource[i].ItemID, "text": dataSource[i].ItemName + " - " + dataSource[i].DayPartName, "code": dataSource[i].ItemCode, "dpcode": dataSource[i].DayPartCode, "DishCategories": dataSource[i].DishCategoryList });
        //} 
        var idata = [];
        var dpartdata = [];

        var dishes = []
        var dataO = [];
        //for (var i = 0; i < foodCostBoard.length; i++) {
        //    //for (var jdp = 0; jdp < foodCostBoard.length; jdp++) {
        //    //    for (var kdc = 0; kdc < foodCostBoard.length; kdc++) {
        //    //        for (var lds = 0; lds < foodCostBoard.length; lds++) {
        //    //            for (var mdo = 0; mdo < foodCostBoard.length; mdo++) {

        //    //            }
        //    //        }
        //    //    }
        //    //}
        //    var chk = dishes.filter(it => it.DishID == foodCostBoard[i].DIshID && it.DayPartCode == foodCostBoard[i].DayPartCode);
        //    //console.log(chk)
        //    if (chk.length == 0) {
        //        dishes.push({ "DishID": foodCostBoard[i].DishID, "DishName": foodCostBoard[i].DishName });
        //    }
        //}
        var dishcats = []

        //for (var i = 0; i < foodCostBoard.length; i++) {
        //    var chk = dishcats.filter(it => it.DishCategoryID == foodCostBoard[i].DishCategoryID && it.DayPartCode == foodCostBoard[i].DayPartCode);
        //    //console.log(chk)
        //    if (chk.length == 0) {
        //        dishcats.push({
        //            "DishCategoryID": foodCostBoard[i].DishCategoryID, "DishCategoryName": foodCostBoard[i].DishCategoryName, "DishCategoryCode": foodCostBoard[i].DishCategoryCode, "ItemID": foodCostBoard[i].ItemID, "ItemName": foodCostBoard[i].ItemName,"Dishes":dishes });
        //    }
        //}
        //Data object

        //console.log("reassigned");
        //console.log(idata);
        //console.log("sitedishdata")
        //console.log(sitedata)

        //console.log("selItems")
        //console.log(selItems)

        if (result.RegionID > 0) {
            HttpClient.MakeSyncRequest(CookBookMasters.GetRegionMasterListByUserId, function (data) {
                var dataSource = data;
                //sitemasterdata = dataSource;
                regionmasterdata = dataSource//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
                // regionmasterdata.sort(compare);
                if (data.length > 1) {
                    regionmasterdata.unshift({ "RegionID": 0, "Name": "Select Region" })
                    //$("#btnGo").css("display", "inline-block");
                }
                Utility.UnLoading();

                populateRegionMasterDropdown();
                //populateItemsMultiDropdown();
                //if (regionid != null) {
                //    //$("#NewSimRegion").val(regionid);
                $("#NewSimRegion").data('kendoDropDownList').value(result.RegionID);
                var dropDownList = $("#NewSimRegion").getKendoDropDownList();
                dropDownList.trigger("change");


                //if (user.StateRegionId > 0) {
                //    var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
                //    //$("#NewSimRegion").val(user.StateRegionId);
                //    regDropdown.enable(false);
                //}
                //else {
                //    var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
                //    regDropdown.enable(true);
                //}
                if (regionmasterdata.length == 1) {
                    var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
                    regDropdown.enable(false);
                }
                else if (regionmasterdata.length == 0) {
                    $("#NewSimRegion").hide();
                }
                else {
                    var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
                    regDropdown.enable(true);
                }

                showAddNewSection(null, null);
                //populateSiteDropdown();
                HttpClient.MakeSyncRequest(CookBookMasters.GetSiteByRegionList, function (data) {
                    Utility.UnLoading();
                    var dataSource = data;
                    sitedata = []

                    sitedata.push({ "value": "Select", "text": "Select Site", "code": "" });
                    for (var i = 0; i < dataSource.length; i++) {
                        if (dataSource[i].IsActive) {
                            sitedata.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName, "code": dataSource[i].SiteCode });
                        }
                    }
                    //Utility.UnLoading();
                    populateSiteDropdown();
                    //Multi select items
                    //$("#ItemsForSimulation").html('');
                    //itemdata = [];

                    setTimeout(function () {
                        //Generate dishes
                        populateSiteDishDropdown(result.SiteCode);
                        //Create data object
                        for (var i = 0; i < foodCostBoard.length; i++) {
                            var chk = idata.filter(it => it.value == foodCostBoard[i].ItemID + "_" + foodCostBoard[i].DayPartCode && it.dpcode == foodCostBoard[i].DayPartCode);
                            //console.log(chk)
                            if (chk.length == 0) {
                                dishcats = [];
                                //Dish category
                                for (var j = 0; j < foodCostBoard.length; j++) {
                                    var chk = dishcats.filter(it => it.DishCategoryID == foodCostBoard[j].DishCategoryID && it.ItemID == foodCostBoard[i].ItemID && it.DayPartCode == foodCostBoard[i].DayPartCode);
                                    if (chk.length == 0) {
                                        //for (var k = 0; k < foodCostBoard.length; k++) {
                                        //    //Dishes
                                        //    var chk3 = dishes.filter(ds => ds.DishID == foodCostBoard[k].DishID && ds.DishCategoryID == foodCostBoard[j].DishCategoryID && ds.ItemID == foodCostBoard[i].ItemID && ds.DayPartCode == foodCostBoard[i].DayPartCode)
                                        //    if (chk3.length == 0) {
                                        //        dishes.push({
                                        //            "DishID": foodCostBoard[k].DishID, "DishName": foodCostBoard[k].DishName, "DishCategoryID": foodCostBoard[j].DishCategoryID, "DishCategoryName": foodCostBoard[j].DishCategoryName, "DishCategoryCode": foodCostBoard[j].DishCategoryCode, "ItemID": foodCostBoard[i].ItemID, "ItemName": foodCostBoard[i].ItemName, "DayPartCode": foodCostBoard[i].DayPartCode
                                        //        });
                                        //    }
                                        //}
                                        var filterDish = foodCostBoard.filter(ds => ds.DishCategoryID == foodCostBoard[j].DishCategoryID && ds.ItemID == foodCostBoard[i].ItemID && ds.DayPartCode == foodCostBoard[i].DayPartCode);
                                        //console.log("filterDish")
                                        //console.log(filterDish)
                                        dishes = [];
                                        for (var k = 0; k < filterDish.length; k++) {
                                            var chk3 = dishes.filter(ds => ds.DishID == filterDish[k].DishID);
                                            if (chk3.length == 0) {
                                                //dataO
                                                dataO = [];
                                                var noofdays = result.NoOfDays;
                                                var da = filterDish.filter(ds => ds.DishID == filterDish[k].DishID);
                                                for (var l = 0; l < da.length; l++) {

                                                    dataO.push({ "index": l + 1, "paxTxt": da[l].Pax, "grmTxt": da[l].Gm, "costTxt": da[l].Cost });
                                                }
                                                var getcost = 0;
                                                if (dishdata.length > 0)
                                                    getcost = dishdata.filter(ds => ds.DishID == filterDish[k].DishID)[0].CostPerKG
                                                dishes.push({
                                                    "DishID": filterDish[k].DishID, "DishName": filterDish[k].DishName, "DishCode": filterDish[k].DishCode, "CostPerKG": getcost, "dataO": dataO
                                                });
                                            }
                                        }

                                        var chkDishCatAvail = foodCostBoard.filter(it => it.DishCategoryID == foodCostBoard[j].DishCategoryID && it.ItemID == foodCostBoard[i].ItemID && it.DayPartCode == foodCostBoard[i].DayPartCode);

                                        //Fix issue if dish category  not available in the list for item and daypart dont show it.
                                        if (chkDishCatAvail.length > 0) {
                                            dishcats.push({
                                                "DishCategoryID": foodCostBoard[j].DishCategoryID, "DishCategoryName": foodCostBoard[j].DishCategoryName, "DishCategoryCode": foodCostBoard[j].DishCategoryCode, "ItemID": foodCostBoard[i].ItemID, "ItemName": foodCostBoard[i].ItemName, "DayPartCode": foodCostBoard[i].DayPartCode, "Dishes": dishes
                                            });
                                        }
                                    }
                                }
                                idata.push({ "value": foodCostBoard[i].ItemID + "_" + foodCostBoard[i].DayPartCode, "text": foodCostBoard[i].ItemName + " - " + foodCostBoard[i].DayPartName, "code": foodCostBoard[i].ItemCode, "dpcode": foodCostBoard[i].DayPartCode, "DishCategories": dishcats });
                                //console.log("weird item id")
                                //console.log(foodCostBoard[i].ItemID)
                                selItems.push(foodCostBoard[i].ItemID + "_" + foodCostBoard[i].DayPartCode);
                                //console.log(foodCostBoard)
                            }
                            var chk2 = dpartdata.filter(dp => dp.ID == foodCostBoard[i].DayPartID);
                            if (chk2.length == 0) {
                                dpartdata.push({ "ID": foodCostBoard[i].DayPartID, "Name": foodCostBoard[i].DayPartName, "DayPartCode": foodCostBoard[i].DayPartCode });
                                selDparts.push(foodCostBoard[i].DayPartID);
                            }
                        }

                        //Bind after few seconds
                        if (result.SiteID != null) {
                            //$("#NewSimSiteByRegion").val(result.SiteID);
                            $("#NewSimSiteByRegion").data('kendoDropDownList').value(result.SiteID);
                            var dropDownList = $("#NewSimSiteByRegion").getKendoDropDownList();
                            dropDownList.trigger("change");
                        }
                        getItemsMultiDropdownData();
                        //daypart = multiselectdp.dataItems();
                        //selecteditem = multiselectitem.dataItems();
                        //Bind board
                        //performSimulationBoardFill(noOfDays, region, daypart, itemsid);
                        performSimulationBoardFill(result.NoOfDays, result.RegionID, dpartdata, idata);
                        expandConfig($("#btnExpandConfig"));
                    }, 500)


                }, { regionId: result.RegionID }, false);

                //}
            }, null, true);
        }
        //showAddNewSection(result.RegionID, result.SiteID);
        //Bind board                                    
        //performSimulationBoardFill(noOfDays, region, daypart, selecteditem);
    }
    function populateSimulationDropdown() {
        $("#selectSimulation").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: simdata,
            enable: true,
            open: onSimulationDropdownClick,
            change: onExistingSimulationChange
        });
    }
    function populateSiteMasterDropdown() {
        $("#selectFromSite").kendoDropDownList({
            filter: "contains",
            dataTextField: "SiteName",
            dataValueField: "SiteID",
            dataSource: sitemasterdata,
            index: 0,
            open: onSiteMasterDropdownClick
        });
    }
    //Middle Row dropdowns
    function populateSiteDropdown() {
        //TODO
        //Site dropdown if user is admin all site of the region will come
        //if user is not admin data will come from usersitemapping table
        $("#NewSimSiteByRegion").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: sitedata,
            enable: true,
            change: onSiteChange
        });
    }
    function populateRegionMasterDropdown() {
        //var multiselect = $("#NewSimRegion").data("kendoDropDownList");
        //if (multiselect == undefined) {

        //TODO
        //Pre Select  if list length is 1,
        //if 0 dont show region dropdown
        $("#NewSimRegion").kendoDropDownList({
            filter: "contains",
            dataTextField: "Name",
            dataValueField: "RegionID",
            dataSource: regionmasterdata,
            index: 0,
            change: onRegionChange
        });

    }

    function populateSiteDishDropdown(SiteCode) {
        var sitedishdata = [];
        Utility.Loading();
        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteDishDataList, function (data) {
            sitedishdata = data;

        }, { siteCode: SiteCode }, true);

        HttpClient.MakeSyncRequest(CookBookMasters.GetDishList, function (data) {

            var dataSource = data;
            dishdata = [];
            for (var i = 0; i < dataSource.length; i++) {
                sitedishdata.forEach(function (yi) {
                    if (yi.DishCode == dataSource[i].DishCode) {
                        dataSource[i].CostPerKG = yi.CostPerKG;
                    }
                });
            }
            //console.log("chkdishcateg")
            //console.log(sitedishdata);

            dishdata.push({ "value": 0, "text": "Select", "price": 0 });
            for (var i = 0; i < dataSource.length; i++) {
                dishdata.push({
                    "DishID": dataSource[i].ID, "DishName": dataSource[i].Name, "DishCode": dataSource[i].DishCode,
                    "DishCategoryID": dataSource[i].DishCategory_ID, "DishCategoryName": dataSource[i].DishCategoryName,
                    "DishCategoryCode": dataSource[i].DishCategoryCode,
                    "CostPerKG": dataSource[i].CostPerKG, "tooltip": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
                    "DietCategoryCode": dataSource[i].DietCategoryCode,
                    "CuisineName": dataSource[i].CuisineName, "RecipeMapStatus": dataSource[i].RecipeMapStatus
                });
            }
            dishcatwithdishData = [];
            for (var i = 0; i < sitedishcatlist.length; i++) {
                var filteredDishes = dishdata.filter(ds => ds.DishCategoryID == sitedishcatlist[i].ID);
                dishcatwithdishData.push({
                    "DishCategoryName": sitedishcatlist[i].Name,
                    "DishCategoryID": sitedishcatlist[i].ID,
                    "Dishes": filteredDishes,
                    "DishCategoryCode": sitedishcatlist[i].DishCategoryCode
                });
                //dcsData.push({ "DishCategoryName": filteredDc[j].DishCategoryName, "DishCategoryID": filteredDc[j].DishCategoryID, "Dishes": filteredDc[j].Dishes, "DishCategoryCode": filteredDc[j].DishCategoryCode })
            }
            console.log("dishcatwithdishData");
            console.log(dishcatwithdishData)
            //console.log("dishdata")
            //console.log(dishdata)
            Utility.UnLoading();
        }, { siteCode: SiteCode }, true);
    }

    function getDishCategoriesWithDishes() {

        HttpClient.MakeRequest(CookBookMasters.GetDishCategoryList, function (data) {
            //Utility.UnLoading();
            var dataSource = data;
            sitedishcatlist = []

            console.log("getDishCategoriesWithDishes")
            console.log(dataSource);
            sitedishcatlist = dataSource;
            //sitedishcatlist.push({ "value": "Select", "text": "Select Site" });
            //for (var i = 0; i < dataSource.length; i++) {
            //    if (dataSource[i].IsActive) {
            //        sitedishcatlist.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName });
            //    }
            //}
            ////Utility.UnLoading();
            //populateSiteDropdown();
            ////Multi select items
            ////$("#ItemsForSimulation").html('');
            ////itemdata = [];
            //getItemsMultiDropdownData();

        }, null, false);
    }
    function getDishes() {
        HttpClient.MakeRequest(CookBookMasters.GetDishList, function (data) {
            //Utility.UnLoading();
            var dataSource = data;
            sitedishlist = []

            console.log("getDishes")
            console.log(dataSource);

            sitedishlist = dataSource;
            //sitedishcatlist.push({ "value": "Select", "text": "Select Site" });
            //for (var i = 0; i < dataSource.length; i++) {
            //    if (dataSource[i].IsActive) {
            //        sitedishcatlist.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName });
            //    }
            //}
            ////Utility.UnLoading();
            //populateSiteDropdown();
            ////Multi select items
            ////$("#ItemsForSimulation").html('');
            ////itemdata = [];
            //getItemsMultiDropdownData();
        }, null, false);

    }
    function populateDayPartDropdown() {
        //$("#NewSimServiceType").kendoDropDownList({
        //    filter: "contains",
        //    dataTextField: "Name",
        //    dataValueField: "ID",
        //    dataSource: daypartdata,
        //    index: 0,
        //    change: onDayPartChange
        //});
        var multiselect = $("#NewSimServiceType").data("kendoMultiSelect");
        if (multiselect == undefined) {
            $("#NewSimServiceType").kendoMultiSelect({
                filter: "contains",
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: daypartdata,
                placeholder: " ",
                index: 0,
                autoBind: true,
                autoClose: true,
                change: onDayPartChange
            });
        }
    }

    function onExistingSimulationChange() {
        var simid = $("#selectSimulation").val();
        HttpClient.MakeRequest(CookBookMasters.GetSimulationBoardData, function (result) {
            console.log("result");
            console.log(result);

            //$("#SimulationID").val(result.ID);
            $("#NewSimNoOfDays").val(result.NoOfDays);
            $("#txtSimulationName").val(result.SimulationName);
            //$("#spanHeadertxt").html("Create From Simulation " + result.SimulationName + " " + result.Version);
            populateExistingBoard(result);


        }, { simId: simid }, false);
    }

    function onSiteChange() {
        var getsiteid = $("#NewSimSiteByRegion").val();

        if (getsiteid != "" && getsiteid > 0) {
            getDayPartMultiData();

            var code = sitedata.filter(s => s.value == getsiteid)[0].code;
            //console.log("csitedataode")
            //console.log(sitedata)
            //Skip when in edit mode
            console.log("add mode")
            console.log($("#SimulationID").val())
            if ($("#SimulationID").val() == 0) {

                $("#NewSimNoOfDays").focus();
            }
            //populateSiteDishDropdown(code)
            SiteCode = code;
            //populateDayPartDropdown();
            setTimeout(function () { populateSiteDishDropdown(SiteCode) }, 500);
        }
    }
    function onDayPartChange() {
        getItemsMultiDropdownData();
    }
    function onItemChange() {
        var multiselectit = $("#ItemsForSimulation").data("kendoMultiSelect");

        //alert(multiselectit.value());
        itemsid = [];
        var dishcategs = [];
        var selecteditems = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();
        itemsid = selecteditems;
        //if (multiselectit != undefined && multiselectit.value().length>0) {
        //    var items = multiselectit.value();
        //    Utility.Loading();
        //    var selecteditems = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();

        //    /*console.log("items" + selecteditems);*/

        //    //for (var i = 0; i < selecteditems.length; i++) {
        //    //    console.log("items" + selecteditems[i].value);
        //    //    HttpClient.MakeRequest(CookBookMasters.GetSimulationRegionItemDishCategories, function (data) {
        //    //        //var jar = JSON.parse(data);
        //    //        for (var j = 0; j < data.length; j++) {

        //    //            dishcategs.push({ "DishCategoryName": data[j].DishCategoryName, "DishCategoryID": data[j].DishCategoryID,"Dishes":data[j].Dishes});
        //    //        }
        //    //        itemsid.push({ "value": selecteditems[i].value, "text": selecteditems[i].text, "DishCategories": dishcategs });
        //    //        Utility.UnLoading();
        //    //        //console.log("itemsid" + itemsid);
        //    //    }, { itemId: selecteditems[i].value }, false);
        //    //    //itemsid.push(items[i]);
        //    //}

        //    itemsid = [];
        //    var newitemid = [];
        //    for (var i = 0; i < selecteditems.length; i++) {
        //        console.log(selecteditems[i].DishCategories);
        //        newitemid.push(selecteditems[i].value);
        //        //Utility.Loading();
        //        //var val = selecteditems[i].value;
        //        //var tex = selecteditems[i].text;
        //        //var cod = selecteditems[i].code;
        //        //var dpcod = selecteditems[i].dpcode;
        //        ////console.log("items" + selecteditem[i].value);
        //        //HttpClient.MakeRequest(CookBookMasters.GetSimulationRegionItemDishCategories, function (data) {
        //        //    //var jar = JSON.parse(data);
        //        //    dishcategs = [];
        //        //    if (data.length > 0) {
        //        //        for (var j = 0; j < data.length; j++) {
        //        //            console.log(data[j])
        //        //            dishcategs.push({ "DishCategoryName": data[j].DishCategoryName, "DishCategoryID": data[j].DishCategoryID, "Dishes": data[j].Dishes });
        //        //        }
        //        //    }

        //        //    itemsid.push({ "value": val, "text": tex, "code": cod, "dpcode": dpcod, "DishCategories": dishcategs });


        //        //    Utility.UnLoading();
        //        //    //console.log("itemsid" + itemsid);
        //        //}, { itemId: selecteditems[i].value }, false);
        //        //itemsid.push(items[i]);
        //    }
        //    //HttpClient.MakeRequest(CookBookMasters.GetSimulationRegionItemDishCategories, function (data) {
        //    //        //var jar = JSON.parse(data);
        //    //        dishcategs = [];
        //    //        if (data.length > 0) {
        //    //            for (var j = 0; j < data.length; j++) {
        //    //                console.log(data[j])
        //    //                dishcategs.push({ "DishCategoryName": data[j].DishCategoryName, "DishCategoryID": data[j].DishCategoryID, "Dishes": data[j].Dishes });
        //    //            }
        //    //        }

        //    //        itemsid.push({ "value": val, "text": tex, "code": cod, "dpcode": dpcod, "DishCategories": dishcategs });


        //    //        Utility.UnLoading();
        //    //        //console.log("itemsid" + itemsid);
        //    //}, { itemIds: selecteditems }, false);
        //}
    }
    function onRegionChange() {
        Utility.Loading();
        var userRole = user.UserRoleId;
        var regId = $("#NewSimRegion").val();
        if (userRole == 1) {
            //Admin
            HttpClient.MakeSyncRequest(CookBookMasters.GetSiteByRegionList, function (data) {
                Utility.UnLoading();
                var dataSource = data;
                sitedata = []

                console.log("user")
                console.log(user)
                sitedata.push({ "value": "Select", "text": "Select Site", "code": "" });
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i].IsActive) {
                        //sitedata.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName });
                        sitedata.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName, "code": dataSource[i].SiteCode });
                    }
                }
                //Utility.UnLoading();
                populateSiteDropdown();
                //Multi select items
                //$("#ItemsForSimulation").html('');
                //itemdata = [];
                getItemsMultiDropdownData();
            }, { regionId: regId }, false);
        }
        else {
            //Non-Admin
            HttpClient.MakeSyncRequest(CookBookMasters.GetSiteByUserList, function (data) {
                Utility.UnLoading();
                var dataSource = data;
                dataSource = dataSource.filter(ds => ds.RegionID == regId)[0];
                console.log("dataSource")
                console.log(dataSource)
                sitedata = []

                sitedata.push({ "value": "Select", "text": "Select Site", "code": "" });
                for (var i = 0; i < dataSource.length; i++) {
                    //if (dataSource[i].IsActive) {
                    //sitedata.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName });
                    sitedata.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName, "code": dataSource[i].SiteCode });
                    //}
                }
                //Utility.UnLoading();
                populateSiteDropdown();
                //Multi select items
                //$("#ItemsForSimulation").html('');
                //itemdata = [];
                getItemsMultiDropdownData();
            }, null, false);
        }
    }
    function onSiteMasterDropdownClick() {
        $("#radCopyFromSite").attr("checked", true);
    }
    function onSimulationDropdownClick() {
        $("#radCopyFromSimulation").attr("checked", true);
    }
    function getDayPartMultiData() {
        var siteid = $("#NewSimSiteByRegion").val();
        HttpClient.MakeSyncRequest(CookBookMasters.GetDayPartBySiteList, function (data) {
            var dataSource = data;
            //sitemasterdata = dataSource;
            daypartdata = dataSource//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
            // regionmasterdata.sort(compare);
            var defName = "Select Service Type";
            if (user.SectorNumber != "20")
                defName = "Select Mealtime";
            if (data.length > 1) {
                daypartdata.unshift({ "ID": 0, "Name": defName })
                //$("#btnGo").css("display", "inline-block");
            }

            var multiselect = $("#NewSimServiceType").data("kendoMultiSelect");
            var ds = new kendo.data.DataSource({ data: daypartdata });
            multiselect.setDataSource(ds);
            if (selDparts.length > 0) {
                console.log("selDparts")
                console.log(selDparts)
                multiselect.value(selDparts);
            }
            Utility.UnLoading();
        }, { siteId: siteid }, true);
    }
    function getItemsMultiDropdownData() {
        Utility.Loading();
        var siteid = $("#NewSimSiteByRegion").val();
        var regionid = $("#NewSimRegion").val();
        //var daypartid = $("#NewSimServiceType").val();
        var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");
        var daypartid = [];
        if (multiselectdp != undefined) {
            var items = multiselectdp.value();
            for (var i = 0; i < items.length; i++) {
                daypartid.push(items[i]);
            }
        }
        if (daypartid.length == 0)
            daypartid.push(0);

        //console.log(siteid);
        //console.log(regionid);
        //console.log(daypartid);
        if (siteid == "Select" || siteid == "")
            siteid = 0;
        if (regionid == "Select")
            regionid = 0;
        //if (daypartid.length != 0) {
        HttpClient.MakeRequest(CookBookMasters.GetSimulationItemList, function (data) {
            Utility.UnLoading();
            var dataSource = data;
            itemdata = [];

            itemdata.push({ "value": "Select", "text": "Select Items", "code": "Select", "dpcode": "", "DishCategories": [] });
            for (var i = 0; i < dataSource.length; i++) {
                itemdata.push({ "value": dataSource[i].ItemID + "_" + dataSource[i].DayPartCode, "text": dataSource[i].ItemName + " - " + dataSource[i].DayPartName, "code": dataSource[i].ItemCode, "dpcode": dataSource[i].DayPartCode, "DishCategories": dataSource[i].DishCategoryList });
            }
            console.log("itemdata")
            console.log(itemdata)
            Utility.UnLoading();
            var multiselect = $("#ItemsForSimulation").data("kendoMultiSelect");
            var ds = new kendo.data.DataSource({ data: itemdata });
            multiselect.setDataSource(ds);
            //multiselect.dataSource.filter({});
            //multiselect.value(itemdata);
            if (selItems.length > 0) {
                console.log("selItems")
                console.log(selItems)
                console.log(itemdata)
                multiselect.value(selItems);
            }
        }, { siteId: siteid, regionId: regionid, daypartId: daypartid }, false);
        //}
    }
    function populateItemsMultiDropdown() {
        var multiselect = $("#ItemsForSimulation").data("kendoMultiSelect");
        if (multiselect == undefined) {
            itemsMulti = $("#ItemsForSimulation").kendoMultiSelect({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: itemdata,
                placeholder: " ",
                index: 0,
                autoBind: true,
                autoClose: true,
                change: onItemChange
            }).data("kendoMultiSelect");
        }
    }

    $("#radNewSimulation").click(function () {
        var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
        if (dropdownlist != undefined)
            dropdownlist.enable(false);
        dropdownlist = $("#selectFromSite").data("kendoDropDownList");
        if (dropdownlist != undefined)
            dropdownlist.enable(false);
    });
    $("#radCopyFromSimulation").click(function () {
        var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
        if (dropdownlist != undefined)
            dropdownlist.enable(true);
        dropdownlist = $("#selectFromSite").data("kendoDropDownList");
        if (dropdownlist != undefined)
            dropdownlist.enable(false);
    });
    $("#radCopyFromSite").click(function () {
        var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
        if (dropdownlist != undefined)
            dropdownlist.enable(false);
        dropdownlist = $("#selectFromSite").data("kendoDropDownList");
        if (dropdownlist != undefined)
            dropdownlist.enable(true);
    });

    $("#AddNew").on("click", function () {
        ////alert("call");
        $("#spanHeadertxt").html("Create New Simulation");
        $("#divSimConfig").removeAttr("disabled");
        var multiselect = $("#selectSimulation").data("kendoDropDownList");
        if (multiselect != undefined)
            multiselect.value("Select");
        multiselect = $("#selectFromSite").data("kendoDropDownList");
        if (multiselect != undefined)
            multiselect.value("Select");

        showAddNewSection(null, null);
        if ($("#btnExpandConfig").text() == "Expand") {
            $("#btnExpandConfig").text("Collapse");
            $(".divConfigFcs").slideToggle(0, function () { });
        }

        $("#radNewSimulation").click();
        //$('.my-colorpicker2 .fa-square').css('color', '#fff');
        //$('.my-colorpicker2 .fa-square').css('background-color', '#fff');
        //$('.my-colorpicker2 .input-group-text').css('background-color', '#fff');

        ////$('.my-colorpicker2 .fa-square').css('background-color', '#fff');

        //var model;
        //var dialog = $("#ColorwindowEdit").data("kendoWindow");

        //$(".k-overlay").css("display", "block");
        //$(".k-overlay").css("opacity", "-0.5");



        //dialog.open().element.closest(".k-window").css({
        //    top: 167,
        //    left: 558

        //});
        //// dialog.center();

        //datamodel = model;
        //$("#colorid").val("");
        //$("#colorcode ").val("");
        //$("#colorname").val("");
        //$("#inputhexcode").val("");
        //$("#inputrgbcode").val("");
        //$("#colorselect").val("");
        //$("#inputfontcode").val("#000000");

        //dialog.title("New Color Creation");

    })

    $("#btnGo").on("click", function () {

        //var noOfDays = $("#NewSimNoOfDays").val();
        //var region = $("#NewSimRegion").val();
        var daypart = [];
        var selecteditem = [];
        //daypart = $("#NewSimServiceType").data("kendoMultiSelect").dataItems();
        //selecteditem = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();
        var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");
        var multiselectitem = $("#ItemsForSimulation").data("kendoMultiSelect");
        var noOfDays = $("#NewSimNoOfDays").val();
        var region = $("#NewSimRegion").val();
        var msg = validateSubmit(noOfDays, region, multiselectdp, multiselectitem);
        ////Region check
        //if (region == 0) {
        //    msg += "Please Select Region<br>";
        //}
        ////Number of days check                              
        //if (noOfDays.length == 0) {
        //    msg += "Please Enter Number of Days<br>";
        //}
        ////Service type check
        //var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");
        //var defName = "Please Select Service Type<br>";
        //if (user.SectorNumber != "20")
        //    defName = "Please Select Mealtime<br>";
        //if (multiselectdp != undefined) {
        //    var items = multiselectdp.value();
        //    if (items.length == 0)
        //        msg += defName;
        //    else {
        //        //for (var i = 0; i < items.length; i++) {
        //        //    daypart.push(items[i]);
        //        //}
        //        daypart = $("#NewSimServiceType").data("kendoMultiSelect").dataItems();
        //    }
        //}
        //else
        //    msg += defName;
        ////Items check
        //var multiselectitem = $("#ItemsForSimulation").data("kendoMultiSelect");
        //if (multiselectitem != undefined) {
        //    var items = multiselectitem.value();
        //    if (items.length == 0)
        //        msg += "Please Select Item<br>";
        //    else {
        //        selecteditem = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();
        //        //selecteditem = itemsid;
        //    }
        //}
        //else
        //    msg += "Please Select Item<br>";

        if (msg.length > 0)
            toastr.error(msg);
        else {
            Utility.Loading();

            daypart = multiselectdp.dataItems();
            selecteditem = multiselectitem.dataItems();
            console.log("selecteditem")
            console.log(selecteditem)
            console.log(daypart)
            //Bind board
            //performSimulationBoardFill(noOfDays, region, daypart, itemsid);
            performSimulationBoardFill(noOfDays, region, daypart, selecteditem);

            expandConfig($("#btnExpandConfig"));
            //Utility.Loading();
            //if (multiselectit != undefined) {
            //var items = multiselectit.value();
            //var selecteditems = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();

            /*console.log("items" + selecteditems);*/


            //}

        }
    });

    $("#btnCancel").on("click", function () {
        if (viewOnly != 1) {
            Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
                function () {
                    clearAndShowList();


                },
                function () {
                }
            );
        }
        else
            clearAndShowList();

    });
    $("#btnSaveAsDraft").click(function () {
        saveSimulationBoard("Draft", this);

    });
    $("#btnUpdateVer").click(function () {
        saveSimulationBoard("Versioned", this);

    });
    $("#btnFinalize").click(function () {
        saveSimulationBoard("Final", this);

    });

    //Recipe
    var baserecipedata = [];
    var baserecipedatafiltered = [];
    var basedataList = [];
    var MOGCode;
    var mogWiseAPLMasterdataSource = [];
    var mogdata = [];
    var mogdataList = [];
    var isShowElmentoryRecipe = false;
    var uommodulemappingdata = [];
    var moguomdata = [];
    var uomdata = [];
    var recipecategoryuomdata = [];

    $("#AddNewRecipe").click(function () {
        $("#windowEdit0").show();
        $("#windowEdit").hide();

        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGDataList, function (data) {

            var dataSource = data;

            mogdata = [];
            mogdata.push({ "MOG_ID": "Select MOG", "MOGName": "Select MOG", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "MOG_Code": "", "IsActive": false });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].CostPerUOM == null) {
                    dataSource[i].CostPerUOM = 0;
                }
                if (dataSource[i].CostPerKG == null) {
                    dataSource[i].CostPerKG = 0;
                }
                if (dataSource[i].TotalCost == null) {
                    dataSource[i].TotalCost = 0;
                }
                if (dataSource[i].Alias != null && dataSource[i].Alias.trim() != "" && dataSource[i].Alias != dataSource[i].Name)
                    mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name + " (" + dataSource[i].Alias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive });
                else
                    mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive });
            }

            mogdataList = mogdata;

        }, null, true);

        HttpClient.MakeSyncRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
            var dataSource = data;
            uommodulemappingdata = [];
            uommodulemappingdata.push({ "value": "Select", "text": "Select", "UOMCode": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].IsActive) {
                    uommodulemappingdata.push({
                        "value": dataSource[i].UOM_ID, "UOM_ID": dataSource[i].UOM_ID, "UOMName": dataSource[i].UOMName, "text": dataSource[i].UOMName,
                        "UOMModuleCode": dataSource[i].UOMModuleCode, "UOMCode": dataSource[i].UOMCode
                    });
                }
            }
            moguomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001');
            uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");
            recipecategoryuomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00006' || m.text == "Select");

            //populateDishDropdown();

        }, null, false);

        populateNewRecipeScreen();
    });
    $("#btnCancelRecipe").click(function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
            function () {
                $("#divSimConfigMain").show();
                $("#divBoard").show();
                $("#windowEdit").hide();

                $("#hdnDishId").val("");
                $("#hdnItemId").val("");
                $("#hdnDishCatCode").val("");
                $("#hdnDishCode").val("");
                //$("#topHeading").text("Unit Recipe Configuration");
            },
            function () {
            }
        );
    });
    $("#btnSubmitRecipe").click(function () {

    });
    //New recipe screen
    $("#btnCancel0").click(function () {

    });
    $("#btnSubmit0").click(function () {

    });
    $("#btnPublish0").click(function () {

    });
    

    //Recipe section
    function populateNewRecipeScreen() {

        var siteId = $("#NewSimSiteByRegion").val();
        var regionId = $("#NewSimRegion").val();
        var dishcode = $("#hdnDishCode").val();
        var code = sitedata.filter(s => s.value == siteId)[0].code;
        //Populate base recipe
        HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {

            var dataSource = data;

            baserecipedata = [];
            baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].CostPerUOM == null) {
                    dataSource[i].CostPerUOM = 0;
                }
                if (dataSource[i].CostPerKG == null) {
                    dataSource[i].CostPerKG = 0;
                }
                if (dataSource[i].TotalCost == null) {
                    dataSource[i].TotalCost = 0;
                }
                if (dataSource[i].Quantity == null) {
                    dataSource[i].Quantity = 0;
                }
                if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });
                else
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });

            }
            baserecipedatafiltered = baserecipedata;
            basedataList = baserecipedata;
        }, { condition: "Base", DishCode: dishcode }, true);

        $("#gridBaseRecipe0").css("display", "None");
        $("#gridMOG0").css("display", "None");
        $("#success").css("display", "none");
        $("#error0").css("display", "none");

        var editor = $("#inputinstruction0").data("kendoEditor");
        editor.value('');
        var model;

        //$(".k-overlay").css("display", "block");
        // $(".k-overlay").css("opacity", "0.5");

        $("#windowEdit0").css("display", "block");

        populateUOMDropdown0();
        if (user.SectorNumber == "20" || user.SectorNumber == "10") {

            populateRecipeCategoryUOMDropdown0();
            populateWtPerUnitUOMDropdown0();
            populateGravyWtPerUnitUOMDropdown0();
            //populateRecipeCategoryUOMDropdown("0")
            $("#inputwtperunituom0").closest(".k-widget").hide();
            $("#inputgravywtperunituom0").closest(".k-widget").hide();
            $("#inputuom0").closest(".k-widget").hide();
        }

        populateBaseDropdown0();
        populateBaseRecipeGrid0();
        populateMOGGrid0(); inputuom0
        $("#mainContent").hide();

        $("#windowEdit0").show();

        datamodel = model;
        $("#inputinstruction0").val("");
        $("#recipeid0").text("");
        $("#inputrecipename0").val("");
        $("#inputrecipealiasname0").val("");
        if (user.SectorNumber == "20" || user.SectorNumber == "10") {
            $("#inputwtperunit0").val("");
            $("#inputgravywtperunit0").val("");
            $("#inputuom0").data("kendoDropDownList").value("Select");
            $("#inputrecipecum0").data("kendoDropDownList").value("Select");
            $("#inputwtperunituom0").data("kendoDropDownList").value("Select");
            $("#inputgravywtperunituom0").data("kendoDropDownList").value("Select");
            $("#inputbase0").data('kendoDropDownList').value("No");
            var recipeCategory = "";
            var rCatData = $("#inputrecipecum0").data('kendoDropDownList')
            if (rCatData != null && rCatData != "Select" && rCatData != "" && rCatData.dataItem() != null && rCatData.dataItem().UOMCode != "Select") {
                recipeCategory = rCatData.dataItem().UOMCode
            }
            OnRecipeUOMCategoryChange(recipeCategory, "0");
        }
        else {
            var inputwtperunituom0 = $("#inputuom0").data("kendoDropDownList");
            inputwtperunituom0.enable(false);
        }

        $("#inputquantity0").val("10");

        // $("#gridBaseRecipe0").data("kendoGrid").addRow();
        // $("#gridMOG0").data("kendoGrid").addRow();
        // hideGrid('gridBaseRecipe0', 'emptybr0', 'baseRecipePlus0', 'baseRecipeMinus0');
        // hideGrid('gridMOG0', 'emptymog0', 'mogPlus0', 'mogMinus0');
        $("#gridMOG").css("display", "none");
        $("#emptymog0").css("display", "block");
        $("#gridBaseRecipe").css("display", "none");
        $("#emptybr0").css("display", "block");
        if (user.SectorNumber == "20" || user.SectorNumber == "10") {
            $('#inputquantity0').removeAttr("disabled");
        }
        else {
            $('#inputquantity0').attr('disabled', 'disabled');
        }
        Utility.UnLoading();

        //Populate from recipe
        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteRecipeList, function (result) {
            Utility.UnLoading();
            if (result != null) {
                copyData = result;
                if (isShowElmentoryRecipe) {
                    result = result.filter(m => m.IsElementory);
                }
                //options.success(result);

                copyData.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
                populateRecipeCopyDropDown();

            }
            else {
                //options.success("");
            }
        },
            {
                SiteCode: code,
                condition: "All"
            }
            , true);

    }
    function populateMOGGrid0() {

        Utility.Loading();
        var gridVariable = $("#gridMOG0");
        gridVariable.html("");
        if (user.SectorNumber == "20") {
            gridVariable.kendoGrid({
                toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
                groupable: false,
                editable: true,

                columns: [
                    {
                        field: "MOG_ID", title: "MOG", width: "120px",
                        attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        template: function (dataItem) {
                            var html = ` <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                                '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                            return html;
                        },
                        editor: function (container, options) {

                            $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    index: 0,
                                    filter: "contains",
                                    dataTextField: "MOGName",
                                    dataValueField: "MOG_ID",
                                    autoBind: true,
                                    dataSource: mogdata,
                                    value: options.field,
                                    change: onMOGDDLChange0,
                                });

                        }
                    },
                    {
                        field: "UOMName", title: "UOM", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {

                            class: 'uomname',
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "UOMCode", title: "UOM", width: "120px",
                    //    attributes: {
                    //        style: "text-align: left; font-weight:normal",
                    //        class: ''
                    //    },
                    //    template: function (dataItem) {
                    //        var html = `  <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                    //            '<span class="uomname" style="margin-left:8px">' + kendo.htmlEncode(dataItem.UOMName) + '</span>';
                    //        return html;
                    //    },
                    //    editor: function (container, options) {
                    //        $('<input class="mogTemplate" id="ddlMOGUOM0" text="' + options.model.UOMName + '" data-text-field="text" data-value-field="UOMCode" data-bind="value:' + options.field + '"/>')
                    //            .appendTo(container)
                    //            .kendoDropDownList({
                    //                index: 0,
                    //                filter: "contains",
                    //                dataTextField: "text",
                    //                dataValueField: "UOMCode",
                    //                autoBind: true,
                    //                dataSource: moguomdata,
                    //                // dataSource: refreshMOGUOMDropDownData(options.model.UOMName),
                    //                value: options.field,
                    //                change: onMOGUOMDDLChange0,
                    //            });

                    //    }
                    //},

                    {
                        field: "Quantity", title: "Quantity", width: "35px",
                        headerAttributes: {
                            style: "text-align: right;"
                        },

                        template: '<input type="number" min="0" name="Quantity" class="inputmogqty"   oninput="calculateItemTotalMOG(this,0)"/>',
                        attributes: {

                            style: "text-align: right; font-weight:normal"
                        },
                    },

                    {
                        field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "uomcost",

                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "totalcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        title: "Delete",
                        width: "60px",
                        headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            style: "text-align: center; font-weight:normal;"
                        },
                        command: [
                            {
                                name: 'Delete',
                                text: "Delete",

                                click: function (e) {
                                    if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                        return false;
                                    }
                                    var gridObj = $("#gridMOG0").data("kendoGrid");
                                    var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal(0);
                                    return true;
                                }
                            },
                            {
                                name: 'View APL',
                                click: function (e) {

                                    var gridObj = $("#gridMOG0").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    MOGName = tr.MOGName;
                                    if (MOGName == "") {
                                        return false;
                                    }
                                    else {
                                        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                        //$("#windowEditMOGWiseAPL").kendoWindow({
                                        //    animation: false
                                        //});
                                        $(".k-overlay").css("display", "block");
                                        $(".k-overlay").css("opacity", "0.5");
                                        dialog.open();
                                        dialog.center();

                                        dialog.title("MOG APL Details - " + MOGName);
                                        MOGCode = tr.MOGCode;
                                        mogWiseAPLMasterdataSource = [];
                                        getMOGWiseAPLMasterData();
                                        populateAPLMasterGrid();
                                        return true;
                                    }
                                }
                            }
                        ],
                    }
                ],
                dataSource: {
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                MOG_ID: { type: "number", validation: { required: true } },
                                MOGName: { type: "string", validation: { required: true } },
                                Quantity: { editable: false },
                                UOMName: { editable: false },
                                IngredientPerc: { editable: false },
                                CostPerUOM: { editable: false },
                                CostPerKG: { editable: false },
                                TotalCost: { editable: false }
                            }
                        }
                    }
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var grid = e.sender;
                    var items = e.sender.items();

                    items.each(function (e) {

                        var dataItem = grid.dataItem(this);

                        var ddt = $(this).find('.mogTemplate');

                        $(ddt).kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            value: dataItem.value,
                            dataSource: mogdata,
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            change: onMOGDDLChange0
                        });
                        //ddt.find(".k-input").val(dataItem.MOGName);

                        $(this).find('.inputmogqty').val(dataItem.Quantity);
                    });
                },
                change: function (e) {
                },
            });
        }
        else {
            gridVariable.kendoGrid({
                toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
                groupable: false,
                editable: true,

                columns: [
                    {
                        field: "MOG_ID", title: "MOG", width: "120px",
                        attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        template: function (dataItem) {
                            var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                                '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                            return html;
                        },
                        editor: function (container, options) {

                            $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    index: 0,
                                    filter: "contains",
                                    dataTextField: "MOGName",
                                    dataValueField: "MOG_ID",
                                    autoBind: true,
                                    dataSource: mogdata,
                                    value: options.field,
                                    change: onMOGDDLChange0,
                                });

                        }
                    },

                    {
                        field: "UOMName", title: "UOM", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {

                            class: 'uomname',
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    {
                        field: "Quantity", title: "Quantity", width: "35px",
                        headerAttributes: {
                            style: "text-align: right;"
                        },

                        template: '<input type="number" min="0" name="Quantity" class="inputmogqty"   oninput="calculateItemTotalMOG(this,0)"/>',
                        attributes: {

                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        field: "IngredientPerc", title: "Major %", width: "50px", format: "{0:0.000}", editable: false,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "major",
                            style: "text-align: right; font-weight:normal"
                        },
                        template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
                    },
                    {
                        field: "IngredientPerc", title: "Minor %", width: "50px", format: "{0:0.000}", editable: false,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "minor",
                            style: "text-align: right; font-weight:normal"
                        },
                        template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
                    },
                    {
                        field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "uomcost",

                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "totalcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        title: "Delete",
                        width: "60px",
                        headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            style: "text-align: center; font-weight:normal;"
                        },
                        command: [
                            {
                                name: 'Delete',
                                text: "Delete",

                                click: function (e) {
                                    if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                        return false;
                                    }
                                    var gridObj = $("#gridMOG0").data("kendoGrid");
                                    var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal(0);
                                    return true;
                                }
                            },
                            {
                                name: 'View APL',
                                click: function (e) {

                                    var gridObj = $("#gridMOG0").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    MOGName = tr.MOGName;
                                    if (MOGName == "") {
                                        return false;
                                    }
                                    else {
                                        var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                        //$("#windowEditMOGWiseAPL").kendoWindow({
                                        //    animation: false
                                        //});
                                        $(".k-overlay").css("display", "block");
                                        $(".k-overlay").css("opacity", "0.5");
                                        dialog.open();
                                        dialog.center();

                                        dialog.title("MOG APL Details - " + MOGName);
                                        MOGCode = tr.MOGCode;
                                        mogWiseAPLMasterdataSource = [];
                                        getMOGWiseAPLMasterData();
                                        populateAPLMasterGrid();
                                        return true;
                                    }
                                }
                            }
                        ],
                    }
                ],
                dataSource: {
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                MOG_ID: { type: "number", validation: { required: true } },
                                MOGName: { type: "string", validation: { required: true } },
                                Quantity: { editable: false },
                                UOMName: { editable: false },
                                IngredientPerc: { editable: false },
                                CostPerUOM: { editable: false },
                                CostPerKG: { editable: false },
                                TotalCost: { editable: false }
                            }
                        }
                    }
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var grid = e.sender;
                    var items = e.sender.items();

                    items.each(function (e) {

                        var dataItem = grid.dataItem(this);

                        var ddt = $(this).find('.mogTemplate');

                        $(ddt).kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            value: dataItem.value,
                            dataSource: mogdata,
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            change: onMOGDDLChange0
                        });
                        //ddt.find(".k-input").val(dataItem.MOGName);

                        $(this).find('.inputmogqty').val(dataItem.Quantity);
                    });
                },
                change: function (e) {
                },
            });
        }

        cancelChangesConfirmation('gridMOG0');
        var toolbar = $("#gridMOG0").find(".k-grid-toolbar");


        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();

        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG0 .k-grid-content").addClass("gridInside");
        toolbar.find(".k-grid-clearall").addClass("gridButton");
        $('#gridMOG0 a.k-grid-clearall').click(function (e) {
            Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    $("#gridMOG0").data('kendoGrid').dataSource.data([]);
                    showHideGrid('gridMOG0', 'emptymog0', 'mup_gridBaseRecipe0');
                },
                function () {
                }
            );

        });
    }
    function showHideGrid(gridId, emptyGrid, uid) {

        var gridLength = $("#" + gridId).data().kendoGrid.dataSource.data().length;

        if (gridLength == 0) {
            $("#" + emptyGrid).toggle();
            $("#" + gridId).hide();
        }
        else {
            $("#" + emptyGrid).hide();
            $("#" + gridId).toggle();
        }

        if (!($("#" + gridId).is(":visible") || $("#" + emptyGrid).is(":visible"))) {
            $("#" + uid).addClass("bottomCurve");

        } else {
            $("#" + uid).removeClass("bottomCurve");

        }

    }
    function populateUOMDropdown0() {
        $("#inputuom0").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: uomdata,
            index: 1
        });
    }
    function OnRecipeUOMCategoryChange(recipeCategory, level) {
        $("#inputwtperunituom" + level + "").closest(".k-widget").hide();
        $("#inputgravywtperunituom" + level + "").closest(".k-widget").hide();
        $("#inputuom" + level + "").closest(".k-widget").hide();
        $("#linputwtperunituom" + level + "").text("-");
        $("#linputgravywtperunituom" + level + "").text("-");
        $("#linputuom" + level + "").text("-");

        $("#inputwtperunit" + level + "").val("");

        if ($("#inputuom" + level + "").data("kendoDropDownList") != undefined) {
            $("#inputuom" + level + "").data("kendoDropDownList").value("Select");
        }
        $("#inputgravywtperunit" + level + "").val("");
        $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("Select");
        $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("Select");
        var inputwtperunituom0 = $("#inputwtperunituom" + level + "").data("kendoDropDownList");
        inputwtperunituom0.enable(false);
        var inputgravywtperunituom0 = $("#inputgravywtperunituom" + level + "").data("kendoDropDownList");
        inputgravywtperunituom0.enable(false);

        if (recipeCategory == "UOM-00005" || recipeCategory == "UOM-00006") {
            var rUOM = recipeCategory == "UOM-00005" ? "1" : "2";
            var rUOMText = uomdata.find(m => m.UOM_ID == rUOM).UOMName;
            $("#inputwtperunit" + level + "").prop("disabled", true);
            $("#inputgravywtperunit" + level + "").prop("disabled", true);
            $("#inputuom" + level + "").data("kendoDropDownList").value(rUOM);
            $("#linputuom" + level + "").text(rUOMText);
            var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
            inputwtperunituom0.enable(false);
            $("#linputwtperunituom" + level + "").text("-");
            $("#linputgravywtperunituom" + level + "").text("-");
        }
        else if (recipeCategory == "UOM-00007") {
            $("#inputwtperunit" + level + "").prop("disabled", false);
            $("#inputgravywtperunit" + level + "").prop("disabled", true);
            $("#inputuom" + level + "").data("kendoDropDownList").value(3);
            var rUOMText = uomdata.find(m => m.UOM_ID == 3).UOMName;
            $("#linputuom" + level + "").text(rUOMText);
            $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
            var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").UOMName;
            $("#linputwtperunituom" + level + "").text(rUText);
            var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
            inputwtperunituom0.enable(false);
            $("#linputgravywtperunituom" + level + "").text("-");
        }
        else if (recipeCategory == "UOM-00009") {
            $("#inputwtperunit" + level + "").prop("disabled", true);
            $("#inputgravywtperunit" + level + "").prop("disabled", false);
            $("#inputuom" + level + "").data("kendoDropDownList").value(3);
            var rUOMText = uomdata.find(m => m.UOM_ID == 3).UOMName;
            $("#linputuom" + level + "").text(rUOMText);
            $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
            var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").UOMName;
            $("#linputgravywtperunituom" + level + "").text(rUText);
            var inputwtperunituom0 = $("#inputuom" + level + "").data("kendoDropDownList");
            inputwtperunituom0.enable(false);
            $("#linputwtperunituom" + level + "").text("-");
        }
        else if (recipeCategory == "UOM-00008") {
            $("#inputwtperunit" + level + "").prop("disabled", false);
            $("#inputgravywtperunit" + level + "").prop("disabled", false);
            $("#inputuom" + level + "").data("kendoDropDownList").value(3);
            $("#inputgravywtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
            $("#inputwtperunituom" + level + "").data("kendoDropDownList").value("UOM-00001");
            var rUText = uomdata.find(m => m.UOMCode == "UOM-00001").UOMName;
            $("#linputgravywtperunituom" + level + "").text(rUText);
            $("#linputwtperunituom" + level + "").text(rUText);
            var rUOMText = uomdata.find(m => m.UOM_ID == 3).UOMName;
            $("#linputuom" + level + "").text(rUOMText);
        }
    };

    function populateRecipeCopyDropDown() {
        $("#copyRecipe").kendoDropDownList({
            filter: "contains",
            dataTextField: "Name",
            dataValueField: "ID",
            dataSource: copyData,
            index: 0,
        });
        var dropdownlist = $("#copyRecipe").data("kendoDropDownList");
        dropdownlist.bind("change", dropdownlist_selectMain);
    }
    function dropdownlist_selectMain(eid) {
        console.log(eid.sender.dataItem(eid.item));
        var ID = eid.sender.dataItem(eid.item).ID;
        //var ItemCode = eid.sender.dataItem(eid.item).ItemCode;
        if (ID == 0) {
            return;
        }
        //  alert(ID)
        populateBaseRecipeGridCopy(ID);
        populateMOGGridCopy(ID);
    }
    function populateBaseRecipeGridCopy(recipeID) {

        if (recipeID == null)
            recipeID = 0
        Utility.Loading();
        var gridVariable = $("#gridBaseRecipe0");
        gridVariable.html("");
        gridVariable.kendoGrid({
            toolbar: [
                //    { name: "cancel", text: "Reset" }, {
                //    name: "create", text: "Add"
                //},
                //{
                //    name: "new", text: "Create New"
                //},

            ],

            groupable: false,
            editable: false,

            columns: [
                {
                    field: "BaseRecipe_ID", title: "Base Recipe", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                            + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails1(this)'></i>`;
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input tabindex="0" id="ddlBaseRecipe0" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "BaseRecipeName",
                                dataValueField: "BaseRecipe_ID",
                                autoBind: true,
                                dataSource: baserecipedata,
                                value: options.field,
                                change: onBRDDLChange0,
                                //open: function (e) { e.preventDefault();}
                            });
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "uomname",
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "Quantity", title: "Quantity", width: "35px",
                    template: '<input class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,0)"/>',


                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "major",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
                },
                {
                    field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "minor",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
                },

                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "uomcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {

                        style: "text-align: right"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",

                            click: function (e) {
                                if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                    return false;
                                }
                                var gridObj = $("#gridBaseRecipe0").data("kendoGrid");
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                gridObj.dataSource._data.remove(selectedRow);
                                CalculateGrandTotal(0);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridBaseRecipe0").css("display", "block");
                                $("#emptybr0").css("display", "none");
                                CalculateGrandTotal(0);
                            } else {
                                $("#gridBaseRecipe0").css("display", "none");
                                $("#emptybr0").css("display", "block");
                            }

                        },
                            {
                                recipeID: recipeID
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false },
                            BaseRecipe_ID: { type: "number", validation: { required: true } },
                            BaseRecipeName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();

                items.each(function (e) {

                    var dataItem = grid.dataItem(this);

                    var ddt = $(this).find('.brTemplate');

                    $(ddt).kendoDropDownList({
                        index: 0,
                        filter: "contains",
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "BaseRecipeName",
                        dataValueField: "BaseRecipe_ID",
                        change: onBRDDLChange
                    });
                    ddt.find(".k-input").val(dataItem.BaseRecipeName);

                    $(this).find('.inputbrqty').val(dataItem.Quantity);
                });
            },
            change: function (e) {
            },
        });
        cancelChangesConfirmation('gridBaseRecipe0');
        var toolbar = $("#gridBaseRecipe1").find(".k-grid-toolbar");


        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();
        toolbar.find(".k-grid-new").addClass("gridButton");
        toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

        toolbar.find(".k-grid-add").addClass("gridButton");
        $("#gridBaseRecipe1 .k-grid-content").addClass("gridInside");

        $('a.k-grid-new').click(function (e) {
            var model;
            var dialog = $("#windowEditx").data("kendoWindow");
            $("#windowEditx").kendoWindow({
                animation: false
            });
            $(".k-overlay").css("display", "block");
            $(".k-overlay").css("opacity", "0.5");
            dialog.open();
            dialog.center();

            populateUOMDropdownx();
            populateBaseDropdownx();
            populateBaseRecipeGridx();
            populateMOGGridx();

            datamodel = model;
            $("#recipeidx").text("");
            $("#inputrecipenamex").val("");
            // $("#inputuom0").data('kendoDropDownList').value("Select");
            $("#inputquantityx").val("10");
            $("#inputbasex").data('kendoDropDownList').value("Select");
            dialog.title("New Base Recipe Creation");
        });
    }
    function populateMOGGridCopy(recipeID) {

        Utility.Loading();
        var gridVariable = $("#gridMOG0");
        gridVariable.html("");
        if (user.SectorNumber == "20") {
            gridVariable.kendoGrid({
                //toolbar: [{ name: "cancel", text: "Reset" }, //{ name: "create", text: "Add" }
                //],
                groupable: false,
                editable: true,

                columns: [
                    {
                        field: "MOG_ID", title: "MOG", width: "120px",
                        attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        template: function (dataItem) {
                            var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                                '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                            return html;
                        },
                        editor: function (container, options) {

                            $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    index: 0,
                                    filter: "contains",
                                    dataTextField: "MOGName",
                                    dataValueField: "MOG_ID",
                                    autoBind: true,
                                    dataSource: mogdata,
                                    value: options.field,
                                    change: onMOGDDLChange0,
                                });

                        }
                    },
                    {
                        field: "UOMName", title: "UOM", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {

                            class: 'uomname',
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    //{
                    //    field: "UOMCode", title: "UOM", width: "120px",
                    //    attributes: {
                    //        style: "text-align: left; font-weight:normal",
                    //        class: ''
                    //    },
                    //    template: function (dataItem) {
                    //        var html = `  <i title="Click to Change UOM" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                    //            '<span class="uomname" style="margin-left:8px">' + kendo.htmlEncode(dataItem.UOMName) + '</span>';
                    //        return html;
                    //    },
                    //    editor: function (container, options) {
                    //        $('<input class="mogTemplate" id="ddlMOGUOM0" text="' + options.model.UOMName + '" data-text-field="text" data-value-field="UOMCode" data-bind="value:' + options.field + '"/>')
                    //            .appendTo(container)
                    //            .kendoDropDownList({
                    //                index: 0,
                    //                filter: "contains",
                    //                dataTextField: "text",
                    //                dataValueField: "UOMCode",
                    //                autoBind: true,
                    //                dataSource: moguomdata,
                    //                // dataSource: refreshMOGUOMDropDownData(options.model.UOMName),
                    //                value: options.field,
                    //                change: onMOGUOMDDLChange0,
                    //            });

                    //    }
                    //},
                    {
                        field: "Quantity", title: "Quantity", width: "35px",
                        headerAttributes: {
                            style: "text-align: right;"
                        },

                        template: '<input type="number" min="0" name="Quantity" class="inputmogqty"   oninput="calculateItemTotalMOG(this,0)"/>',
                        attributes: {

                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "uomcost",

                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "totalcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        title: "Delete",
                        width: "60px",
                        headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            style: "text-align: center; font-weight:normal;"
                        },
                        command: [
                            {
                                name: 'Delete',
                                text: "Delete",

                                click: function (e) {
                                    if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                        return false;
                                    }
                                    var gridObj = $("#gridMOG0").data("kendoGrid");
                                    var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal(0);
                                    return true;
                                }
                            },
                            {
                                name: 'View APL',
                                click: function (e) {
                                    var gridObj = $("#gridMOG0").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    MOGName = tr.MOGName;
                                    var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                    $("#windowEditMOGWiseAPL").kendoWindow({
                                        animation: false
                                    });
                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open();
                                    dialog.center();
                                    dialog.title("MOG APL Details - " + MOGName);
                                    MOGCode = tr.MOGCode;
                                    mogWiseAPLMasterdataSource = [];
                                    getMOGWiseAPLMasterData();
                                    populateAPLMasterGrid();
                                    return true;
                                }
                            }
                        ],
                    }
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                                Utility.UnLoading();

                                if (result != null) {

                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }

                                if (result.length > 0) {
                                    $("#gridMOG0").css("display", "block");
                                    $("#emptymog0").css("display", "none");
                                    CalculateGrandTotal(0);
                                } else {
                                    $("#gridMOG0").css("display", "none");
                                    $("#emptymog0").css("display", "block");
                                }
                            },
                                {
                                    recipeID: recipeID
                                }
                                , true);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                MOG_ID: { type: "number", validation: { required: true } },
                                MOGName: { type: "string", validation: { required: true } },
                                Quantity: { editable: false },
                                UOMName: { editable: false },
                                IngredientPerc: { editable: false },
                                CostPerUOM: { editable: false },
                                CostPerKG: { editable: false },
                                TotalCost: { editable: false }
                            }
                        }
                    }
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var grid = e.sender;
                    var items = e.sender.items();

                    items.each(function (e) {

                        var dataItem = grid.dataItem(this);

                        var ddt = $(this).find('.mogTemplate');

                        $(ddt).kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            value: dataItem.value,
                            dataSource: baserecipedata,
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            change: onMOGDDLChange0
                        });
                        //ddt.find(".k-input").val(dataItem.MOGName);

                        $(this).find('.inputmogqty').val(dataItem.Quantity);
                    });
                },
                change: function (e) {
                },
            });
        }
        else {
            gridVariable.kendoGrid({
                //toolbar: [{ name: "cancel", text: "Reset" }, //{ name: "create", text: "Add" }
                //],
                groupable: false,
                editable: true,

                columns: [
                    {
                        field: "MOG_ID", title: "MOG", width: "120px",
                        attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        template: function (dataItem) {
                            var html = `  <i title="Click to Change MOG" style='float:left;margin-top:5px;color:#8f9090' class='fa fa-pen'></i>` +
                                '<span class="mogspan" style="margin-left:8px">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                            return html;
                        },
                        editor: function (container, options) {

                            $('<input class="mogTemplate" id="ddlMOG0" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    index: 0,
                                    filter: "contains",
                                    dataTextField: "MOGName",
                                    dataValueField: "MOG_ID",
                                    autoBind: true,
                                    dataSource: mogdata,
                                    value: options.field,
                                    change: onMOGDDLChange0,
                                });

                        }
                    },
                    {
                        field: "UOMName", title: "UOM", width: "50px",
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {

                            class: 'uomname',
                            style: "text-align: center; font-weight:normal"
                        },
                    },
                    {
                        field: "Quantity", title: "Quantity", width: "35px",
                        headerAttributes: {
                            style: "text-align: right;"
                        },

                        template: '<input type="number" min="0" name="Quantity" class="inputmogqty"   oninput="calculateItemTotalMOG(this,0)"/>',
                        attributes: {

                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        field: "IngredientPerc", title: "Major %", width: "50px", format: "{0:0.000}", editable: false,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "major",
                            style: "text-align: right; font-weight:normal"
                        },
                        template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
                    },
                    {
                        field: "IngredientPerc", title: "Minor %", width: "50px", format: "{0:0.000}", editable: false,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "minor",
                            style: "text-align: right; font-weight:normal"
                        },
                        template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
                    },
                    {
                        field: "CostPerUOM", title: "Cost/UOM", width: "50px", editable: false,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "uomcost",

                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                        headerAttributes: {
                            style: "text-align: right;"
                        },
                        attributes: {
                            class: "totalcost",
                            style: "text-align: right; font-weight:normal"
                        },
                    },
                    {
                        title: "Delete",
                        width: "60px",
                        headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        attributes: {
                            style: "text-align: center; font-weight:normal;"
                        },
                        command: [
                            {
                                name: 'Delete',
                                text: "Delete",

                                click: function (e) {
                                    if ($(e.currentTarget).hasClass("k-state-disabled")) {
                                        return false;
                                    }
                                    var gridObj = $("#gridMOG0").data("kendoGrid");
                                    var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    gridObj.dataSource._data.remove(selectedRow);
                                    CalculateGrandTotal(0);
                                    return true;
                                }
                            },
                            {
                                name: 'View APL',
                                click: function (e) {
                                    var gridObj = $("#gridMOG0").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    MOGName = tr.MOGName;
                                    var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                                    $("#windowEditMOGWiseAPL").kendoWindow({
                                        animation: false
                                    });
                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open();
                                    dialog.center();
                                    dialog.title("MOG APL Details - " + MOGName);
                                    MOGCode = tr.MOGCode;
                                    mogWiseAPLMasterdataSource = [];
                                    getMOGWiseAPLMasterData();
                                    populateAPLMasterGrid();
                                    return true;
                                }
                            }
                        ],
                    }
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                                Utility.UnLoading();

                                if (result != null) {

                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }

                                if (result.length > 0) {
                                    $("#gridMOG0").css("display", "block");
                                    $("#emptymog0").css("display", "none");
                                    CalculateGrandTotal(0);
                                } else {
                                    $("#gridMOG0").css("display", "none");
                                    $("#emptymog0").css("display", "block");
                                }
                            },
                                {
                                    recipeID: recipeID
                                }
                                , true);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                MOG_ID: { type: "number", validation: { required: true } },
                                MOGName: { type: "string", validation: { required: true } },
                                Quantity: { editable: false },
                                UOMName: { editable: false },
                                IngredientPerc: { editable: false },
                                CostPerUOM: { editable: false },
                                CostPerKG: { editable: false },
                                TotalCost: { editable: false }
                            }
                        }
                    }
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                dataBound: function (e) {
                    var grid = e.sender;
                    var items = e.sender.items();

                    items.each(function (e) {

                        var dataItem = grid.dataItem(this);

                        var ddt = $(this).find('.mogTemplate');

                        $(ddt).kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            value: dataItem.value,
                            dataSource: baserecipedata,
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            change: onMOGDDLChange0
                        });
                        //ddt.find(".k-input").val(dataItem.MOGName);

                        $(this).find('.inputmogqty').val(dataItem.Quantity);
                    });
                },
                change: function (e) {
                },
            });
        }
        cancelChangesConfirmation('gridMOG0');
        var toolbar = $("#gridMOG0").find(".k-grid-toolbar");
        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();

        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridMOG0 .k-grid-content").addClass("gridInside");
    }
    function onMOGDDLChange0(e) {

        if (checkItemAlreadyExist(e.sender.value(), 'gridMOG0', false)) {
            toastr.error("MOG is already selected.");
            var dropdownlist = $("#ddlMOG0").data("kendoDropDownList");
            dropdownlist.select("");
            //  dropdownlist.trigger("change");
            return false;
        }
        var element = e.sender.element;
        var row = element.closest("tr");
        var grid = $("#gridMOG0").data("kendoGrid");
        var dataItem = grid.dataItem(row);
        var data = row.find(".k-input")[0].innerText;
        dataItem.set("MOG_ID", e.sender.value());
        dataItem.set("MOGName", data);
        var Obj = mogdata.filter(function (mogObj) {
            return mogObj.MOG_ID == e.sender.value();
        });
        var UID = Obj[0].UOM_ID;
        var UOMNAME = Obj[0].UOM_NAME;
        var UnitCost = Obj[0].CostPerUOM;
        var KGCost = Obj[0].CostPerKG;
        dataItem.set("BaseRecipe_ID", e.sender.value());
        dataItem.set("BaseRecipeName", data);
        dataItem.UOMName = UOMNAME;
        dataItem.UOMCode = Obj[0].UOM_Code;
        dataItem.UOM_ID = UID;
        dataItem.CostPerUOM = UnitCost;
        dataItem.CostPerKG = KGCost;
        dataItem.MOGCode = Obj[0].MOG_Code;
        $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
        $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
        dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
        $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
        CalculateGrandTotal(0);

    }

    function checkItemAlreadyExist(selectedId, gridID, isBR) {
        var dataExists = false;
        var gridObj = $("#" + gridID).data("kendoGrid");
        var data = gridObj.dataSource.data();
        var existingData = [];
        if (isBR) { existingData = data.filter(m => m.BaseRecipe_ID == selectedId); }
        else {
            existingData = data.filter(m => m.MOG_ID == selectedId);
            //if (!existingData[0]?.IsActive)
            //    return true;
        }
        if (existingData.length == 2) {
            dataExists = true;
        }
        return dataExists;
    }
    function getMOGWiseAPLMasterData() {
        HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG, function (result) {
            Utility.UnLoading();

            if (result != null) {
                mogWiseAPLMasterdataSource = result;
                console.log(result);
            }
            else {
            }
        }, { mogCode: MOGCode }
            , true);
    }
    function populateAPLMasterGrid() {
        Utility.Loading();


        var gridVariable = $("#gridMOGWiseAPL");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "APLMaster.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            // pageable: true,
            pageable: {
                numeric: false,
                previousNext: false,
                messages: {
                    display: "Total: {2} records"
                }
            },
            groupable: false,
            //reorderable: true,
            scrollable: true,
            columns: [
                {
                    field: "ArticleNumber", title: "Article Code", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ArticleDescription", title: "Article Name", width: "150px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOM", title: "UOM", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

                        style: "text-align: left; font-weight:normal;text-align:center;"
                    },
                },
                {
                    field: "SiteName", title: "SiteName", width: "50px", attributes: {

                        style: "text-align: left; font-weight:normal;text-align:center;"
                    },
                }
                ,
                {
                    field: "ArticleCost", title: "Standard Cost", width: "50px", format: Utility.Cost_Format,
                    attributes: {

                        style: "text-align: left; font-weight:normal;text-align:center;"
                    },
                },
                {
                    field: "StandardCostPerKg", title: "Cost Per KG", width: "50px", format: Utility.Cost_Format,
                    attributes: {

                        style: "text-align: left; font-weight:normal;text-align:center;"
                    },
                }

            ],
            dataSource: {
                data: mogWiseAPLMasterdataSource,
                schema: {
                    model: {
                        id: "ArticleID",
                        fields: {
                            ArticleNumber: { type: "string" },
                            ArticleDescription: { type: "string" },
                            UOM: { type: "string" },
                            ArticleCost: { type: "string" },
                            StandardCostPerKg: { type: "string" },
                            MerchandizeCategoryDesc: { type: "string" },
                            MOGName: { type: "string" },
                            StandardCostPerKg: { editable: false },
                            ArticleCost: { editable: false },
                            SiteCode: { type: "string" },
                            SiteName: { type: "string" }

                        }
                    }
                },
                // pageSize: 5,
            },
            // height: 404,
            noRecords: {
                template: "No Records Available"
            },

            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {


            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        $(".k-label")[0].innerHTML.replace("items", "records");
    }
    function cancelChangesConfirmation(gridId) {
        //  return;
        $(".k-grid-cancel-changes").unbind("mousedown");
        $(".k-grid-cancel-changes").mousedown(function (e) {
            Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
                function () {
                    //  var grid = $("#" + gridIdin + "").data("kendoGrid");
                    //  if (gridIdin == 'gridBaseRecipe') {
                    //      populateBaseRecipeGrid(resetID);
                    //      return;
                    //  } else if (gridIdin == 'gridMOG') {
                    //      populateMOGGrid(resetID)
                    //      return;
                    //}
                    //  grid.cancelChanges();
                },
                function () {
                }
            );

        });
    }
    function CalculateGrandTotal(level) {
        var gtotalObjforBR = $("#gridBaseRecipe" + level + " .totalcost");
        var gtotalObjforMOG = $("#gridMOG" + level + " .totalcost");
        var len = gtotalObjforBR.length;
        var len1 = gtotalObjforMOG.length;
        var gTotal = 0.0;

        for (i = 0; i < len; i++) {
            var totalCostBR = gtotalObjforBR[i].innerText;
            totalCostBR = totalCostBR.substring(1).replace(',', '');
            if (totalCostBR == "") {
                continue;
            }
            gTotal += Number(totalCostBR);
        }
        for (i = 0; i < len1; i++) {
            var totalCostMOG = gtotalObjforMOG[i].innerText;
            totalCostMOG = totalCostMOG.substring(1).replace(',', '');
            if (totalCostMOG == "") {
                continue;
            }
            gTotal += Number(totalCostMOG);
        }
        if (!isNaN(gTotal)) {
            $("#grandTotal" + level).html(Utility.AddCurrencySymbol(gTotal, 2));
        }
        var qty = $('#inputquantity' + level).val();
        var costPerKG = gTotal / Number(qty);
        $('#grandCostPerKG' + level).html(Utility.AddCurrencySymbol(costPerKG, 2));
        mogtotalQTYDelete(level);
        BaseRecipetotalQTYDelete(level);
    }
    function mogtotalQTYDelete(level) {

        if (level == null || typeof level == 'undefined')
            level = '';
        var grid = $("#gridMOG" + level).data("kendoGrid");
        var gTotal = 0.0;
        var len1 = grid._data.length;
        for (i = 0; i < len1; i++) {
            var totalCostMOG = grid._data[i].Quantity;

            if (totalCostMOG == "") {
                continue;
            }
            gTotal += Number(totalCostMOG);
        }
        if (!isNaN(gTotal)) {

            $("#totmog").text(Utility.MathRound(gTotal, 2));
            //parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))).toFixed(2);
            //parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())));
            var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text())).toFixed(2);
            $("#ToTIngredients").text(Utility.MathRound(tot, 2));
        }
    }
    function BaseRecipetotalQTYDelete(level) {

        if (level == null || typeof level == 'undefined')
            level = '';
        var grid = $("#gridBaseRecipe" + level).data("kendoGrid");

        var gTotal = 0.0;
        var len1 = grid._data.length;

        for (i = 0; i < len1; i++) {
            var totalCostMOG = grid._data[i].Quantity;

            if (totalCostMOG == "") {
                continue;
            }
            gTotal += Number(totalCostMOG);
        }
        if (!isNaN(gTotal)) {

            $("#totBaseRecpQty").text(Utility.MathRound(gTotal, 2));
            // parseFloat($("#ToTIngredients").text(parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))).toFixed(2);
            var tot = (parseFloat($("#totBaseRecpQty").text()) + parseFloat($("#totmog").text()))
            $("#ToTIngredients").text(tot);
        }


    }


    function saveSimulationBoard(status, btnEle) {
        var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");
        var multiselectitem = $("#ItemsForSimulation").data("kendoMultiSelect");
        var noOfDays = $("#NewSimNoOfDays").val();
        var region = $("#NewSimRegion").val();
        var site = $("#NewSimSiteByRegion").val();
        var msg = validateSubmit(noOfDays, region, multiselectdp, multiselectitem);
        var txtSimulationName = $("#txtSimulationName").val();
        if (txtSimulationName.length == 0)
            msg += "Please Enter Simulation Name<br>";
        if (msg.length > 0) {
            toastr.error(msg);
        } else {
            //Post values
            var contextData = JSON.parse(localStorage.getItem("contextitem"));
            console.log(contextData);
            console.log("contextData")
            for (var i = 0; i < contextData.length; i++) {
                for (var j = 0; j < contextData[i].DishCategories.length; j++) {
                    for (var k = 0; k < contextData[i].DishCategories[j].Dishes.length; k++) {
                        var currentDishData = contextData[i].DishCategories[j].Dishes[k];
                        var currentDcData = contextData[i].DishCategories[j];
                        var currentItem = contextData[i];
                        var itemidvalue = currentItem.value.split('_')[0];
                        var dataO = [];
                        for (var l = 1; l <= noOfDays; l++) {
                            var paxTxt = "#txtPax_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
                            var grmTxt = ".txtGrmroot_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
                            var costTxt = "#lblCost_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;

                            dataO.push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });
                            //contextData[i].DishCategories[j].Dishes[k].push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });

                        }
                        contextData[i].DishCategories[j].Dishes[k].dataO = dataO;
                    }
                }
                var itAvgCost = ".lblavgItemTotal_" + contextData[i].dpcode + "_" + contextData[i].value.split('_')[0];
                contextData[i].TotalAvgCost = $(itAvgCost).text();

            }
            console.log("data")
            console.log(contextData)
            $(btnEle).attr('disabled', 'disabled');
            Utility.Loading();
            HttpClient.MakeSyncRequest(CookBookMasters.SaveSimulationData, function (result) {
                Utility.UnLoading();

                if (result == false) {
                    $(btnEle).removeAttr("disabled");
                    //$("#error").css("display", "flex");
                    toastr.error("Some error occured, please try again");
                    // $("#error").find("p").text("Some error occured, please try again");
                    //$("#sitealias").focus();
                }
                else {
                    //$(".k-overlay").hide();
                    //$("#error").css("display", "flex");
                    //var orderWindow = $("#ColorwindowEdit").data("kendoWindow");
                    //orderWindow.close();
                    $(btnEle).removeAttr("disabled");
                    //$("#success").css("display", "flex");
                    if (parseInt($("#SimulationID").val()) > 0) {
                        toastr.success("Updated successfully");
                    }
                    else {
                        toastr.success("Added successfully");
                    }
                    if (status == "Versioned" || status == "Final")
                        clearAndShowList();
                    // $("#success").find("p").text("Color updated successfully");
                    //$("#gridColor").data("kendoGrid").dataSource.data([]);
                    //$("#gridColor").data("kendoGrid").dataSource.read();
                    //$("#gridColor").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
                }
            }, {
                model: contextData,
                simId: $("#SimulationID").val(),
                noOfDays: noOfDays,
                siteId: site,
                regionId: region,
                status: status,
                simName: $("#txtSimulationName").val()
            }, true);
        }
    }
    function showAddNewSection(siteid, regionid) {
        Utility.Loading();
        $("#success").css("display", "none");
        $("#error").css("display", "none");
        //$("#topHeading").text("Create New Simulation");
        $("#FCSimulatorAdd").show();
        $("#FCSimulator").hide();
        //Sector based label for service type / mealtime
        if (user.SectorNumber == "20")
            $("#lblNewSimServiceType").text("Select Service Type");
        else
            $("#lblNewSimServiceType").text("Select Mealtime");

        populateSimulationDropdown();

        HttpClient.MakeSyncRequest(CookBookMasters.GetRegionMasterListByUserId, function (data) {
            var dataSource = data;
            //sitemasterdata = dataSource;
            regionmasterdata = dataSource//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
            // regionmasterdata.sort(compare);
            if (data.length > 1) {
                regionmasterdata.unshift({ "RegionID": 0, "Name": "Select Region" })
                //$("#btnGo").css("display", "inline-block");
            }
            Utility.UnLoading();

            populateRegionMasterDropdown();


            if (regionmasterdata.length == 1) {
                var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
                setTimeout(function () {
                    regDropdown.value(user.StateRegionId);
                    regDropdown.trigger("change");
                    regDropdown.enable(false);
                }, 100);
            }
            else if (regionmasterdata.length == 0) {
                $("#NewSimRegion").hide();
            }
            else {
                var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
                regDropdown.enable(true);
            }
            //if (regionid != null) {
            //    //$("#NewSimRegion").val(regionid);
            //    $("#NewSimRegion").data('kendoDropDownList').value(regionid);
            //    var dropDownList = $("#NewSimRegion").getKendoDropDownList();
            //    dropDownList.trigger("change");
            //}
        }, null, true);

        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMasterList, function (data) {
            //console.log("Site Master Data");
            //console.log(data);
            sitemasterdata = data;
            sitemasterdata.forEach(function (item) {
                item.SiteName = item.SiteName + " - " + item.SiteCode;
                return item;
            })
            sitemasterdata = sitemasterdata.filter(item => item.IsActive != false);
            if (data.length > 1) {
                sitemasterdata.unshift({ "SiteID": 0, "SiteName": "Select Site" });
                /*$("#btnGo").css("display", "inline-block");*/
            }
            else {
                $("#btnGo").css("display", "inline-none");
            }
            populateSiteMasterDropdown();
            //if (siteid != null) {
            //    $("#NewSimSiteByRegion").val(siteid);
            //    var dropDownList = $("#NewSimSiteByRegion").getKendoDropDownList();
            //    dropDownList.trigger("change");
            //}
        }, null, false);

        //Get dish category master
        //getDishCategoriesWithDishes();
        //Get dishes master
        //getDishes();

        populateItemsMultiDropdown();
        populateDayPartDropdown();
    }

    function clearAndShowList() {
        Utility.Loading();

        HttpClient.MakeRequest(CookBookMasters.GetSimulationDataListWithPaging, function (result) {
            Utility.UnLoading();
            filteredSimulationDataSource = result;
            //For Simulation dropdown
            var dataSource = result;
            simdata = [];
            simdata.push({ "value": "Select", "text": "Select Simulation" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].IsActive) {
                    /*simdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name + " " + dataSource[i].Version });*/
                    var version = "";
                    if (dataSource[i].Version != null && dataSource[i].Version != "null")
                        version = dataSource[i].Version;
                    simdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name + " " + version });
                }
            }
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNew").css("display", "none");
            //}
            populateSimulationGrid();
            $("#FCSimulatorAdd").hide();
            $("#FCSimulator").show();
            $("#topHeading").text("Food Cost Simulator");

            $("#SimulationID").val("0");
            $("#NewSimNoOfDays").val("");
            $("#txtSimulationName").val("");

            $("#NewSimRegion").val("0");
            selDparts = [];
            selItems = [];
            sitedata = [];
            populateSiteDropdown();
            daypartdata = [];
            var multiselect = $("#NewSimServiceType").data("kendoMultiSelect");
            var ds = new kendo.data.DataSource({ data: daypartdata });
            multiselect.setDataSource(ds);

            itemdata = [];
            var multiselect = $("#ItemsForSimulation").data("kendoMultiSelect");
            var ds = new kendo.data.DataSource({ data: itemdata });
            multiselect.setDataSource(ds);
            //$("#NewSimSiteByRegion").html("");
            //$("#NewSimServiceType").html("");
            //$("#ItemsForSimulation").html("");
            $("#divBoard").hide("");
            $("#gridSimulationBoard").html("");

            $("#divSimConfig input").removeAttr("disabled");
            $("#divSimConfig").removeClass("divDisabled");
            var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
            dropdownlist.enable(true);
            dropdownlist = $("#selectFromSite").data("kendoDropDownList");
            dropdownlist.enable(true);

            //View board these divs are hiding
            $("#divSimConfig").show();
            $("#divSimFilter").show();
            $(".card-tools").show();
            viewOnly = 0;

            clearRecipePanel();
        }, null, false);
    }


    $("#btnSubmit").click(function () {
        //if ($("#colorselect").val() === "") {
        //    toastr.error("Please select the Color");
        //    $("#colorselect").focus();
        //    return;
        //}
        //if ($("#colorname").val() === "") {
        //    toastr.error("Please provide Color Name");
        //    $("#colorname").focus();
        //    return;
        //}
        //else {
        //    //        $("#error").css("display", "none");

        //    var model;
        //    if (datamodel != null) {
        //        model = datamodel;
        //        model.ColorCode = $("#colorcode").val();
        //        model.Name = $("#colorname").val();
        //        model.HexCode = $("#inputhexcode").val();
        //        model.RGBCode = $("#inputrgbcode").val();
        //        model.FontColor = $("#inputfontcode").val();
        //    }
        //    else {
        //        model = {
        //            "ID": $("#colorid").val(),
        //            "ColorCode": $("#colorcode").val(),
        //            "Name": $("#colorname").val(),
        //            "HexCode": $("#inputhexcode").val(),
        //            "RGBCode": $("#inputrgbcode").val(),
        //            "FontColor": $("#inputfontcode").val(),
        //            "IsActive": true,
        //        }
        //    }


        //    $("#btnSubmit").attr('disabled', 'disabled');
        //    HttpClient.MakeSyncRequest(CookBookMasters.SaveColorData, function (result) {
        //        if (result == false) {
        //            $('#btnSubmit').removeAttr("disabled");
        //            //$("#error").css("display", "flex");
        //            toastr.error("Some error occured, please try again");
        //            // $("#error").find("p").text("Some error occured, please try again");
        //            $("#sitealias").focus();
        //        }
        //        else {
        //            $(".k-overlay").hide();
        //            //$("#error").css("display", "flex");
        //            var orderWindow = $("#ColorwindowEdit").data("kendoWindow");
        //            orderWindow.close();
        //            $('#btnSubmit').removeAttr("disabled");
        //            //$("#success").css("display", "flex");
        //            if (model.ID > 0) {
        //                toastr.success("Color updated successfully");
        //            }
        //            else {
        //                toastr.success("Color added successfully");
        //            }

        //            // $("#success").find("p").text("Color updated successfully");
        //            $("#gridColor").data("kendoGrid").dataSource.data([]);
        //            $("#gridColor").data("kendoGrid").dataSource.read();
        //            $("#gridColor").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
        //        }
        //    }, {
        //        model: model

        //    }, true);
        //}
    });
});




function validateSubmit(noOfDays, region, multiselectdp, multiselectitem) {
    var msg = "";
    //var noOfDays = $("#NewSimNoOfDays").val();
    //var region = $("#NewSimRegion").val();
    //var daypart = [];
    //var selecteditem = [];
    //Region check
    if (region == 0) {
        msg += "Please Select Region<br>";
    }
    //Number of days check                              
    if (noOfDays.length == 0) {
        msg += "Please Enter Number of Days<br>";
    }
    //Service type check
    /*var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");*/
    var defName = "Please Select Service Type<br>";
    if (user.SectorNumber != "20")
        defName = "Please Select Mealtime<br>";
    if (multiselectdp != undefined) {
        var items = multiselectdp.value();
        if (items.length == 0)
            msg += defName;
        else {
            //for (var i = 0; i < items.length; i++) {
            //    daypart.push(items[i]);
            //}
            //daypart = $("#NewSimServiceType").data("kendoMultiSelect").dataItems();
        }
    }
    else
        msg += defName;
    //Items check
    /*var multiselectitem = $("#ItemsForSimulation").data("kendoMultiSelect");*/
    if (multiselectitem != undefined) {
        var items = multiselectitem.value();
        if (items.length == 0)
            msg += "Please Select Item<br>";
        else {
            //selecteditem = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();
            //selecteditem = itemsid;
        }
    }
    else
        msg += "Please Select Item<br>";
    return msg;
}

function expandConfig(btnEle) {
    if ($(btnEle).text() == "Expand") {
        $(btnEle).text("Collapse");
    }
    else {
        $(btnEle).text("Expand");
    }
    $(".divConfigFcs").slideToggle(0, function () { });
}

function clearRecipePanel() {
    $("#windowEdit").hide();
}
//function backToSimulationList() {

//    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
//        function () {

//            $("#FCSimulatorAdd").hide();
//            $("#FCSimulator").show();
//            $("#topHeading").text("Food Cost Simulator");
//        },
//        function () {
//        }
//    );
//}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
