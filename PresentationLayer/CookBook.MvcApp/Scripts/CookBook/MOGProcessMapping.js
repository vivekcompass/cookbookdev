﻿//import { Util } from "../bootstrap/js/bootstrap.bundle";
var nationalmogdata = [];
var mogWisenutrientMasterdataSource = [];
var checkedNutrientCodes = [];
var nutrientFilterDataSource = [];
var nutrientMasterdataSource = [];
var mogWiseNutrientArray = [];

var checkedIds = {};

var user;
var status = "";
var varname = "";
var datamodel;
var MOGName = "";
var BATCHNUMBER = "";
var NewMOGName = "";
var MOGCode = "";
var MOGID = 0;
var uomdata = [];
var sectorMasterdataSource = [];
var isShowPreview = false;
var CheckedTrueAPLCodes = [];
var mogImpactedData = [];
var isStatusSave = false;
var mogStatus = false;
var isRIEnabled = false;
var isMOGAPLMappingSave = false;
var isShowElmentoryMOG = false;
var elementaryCode = '';
var IsElementoryMOG = false;

var uommodulemappingdata = [];
//Krish
//13-07-2022
var applicationSettingData = [];
var colorFilterData = [];
//Krish 18-10-2022
var rangeTypeData = [];
var processTypeData = [];
var processmogmappingdata = [];

$(function () {

    $("#NationalwindowEdit").kendoWindow({
        modal: true,
        width: "475px",
        height: "150px",
        title: "MOG Details - " + MOGName,
        actions: ["Close"],
        visible: false,
        animation: false
    });


    $("#UploadExcelFile").kendoWindow({
        modal: true,
        width: "575px",
        height: "105px",
        title: "MOG EXCEL",
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#GETALLMogViewALLHistoryBATCHWISE").kendoWindow({
        modal: true,
        width: "975px",
        height: "105px",
        title: "MOG EXCEL",
        actions: ["Close"],
        visible: false,
        animation: false

    });

    $("#windowEditMOGChangeConfirm").kendoWindow({
        modal: true,
        width: "600px",
        height: "220px",
        title: "Alert",
        // actions: ["Close"],
        visible: false,
        animation: false
    });


    $("#windowEditMOGDependencies").kendoWindow({
        modal: true,
        width: "1050px",
        height: "570px",
        left: '220px !important',
        top: '20px !important',
        title: "MOG - " + MOGName + " Dependencies",
        actions: ["Close"],
        visible: false,
        animation: false,
        close: function (e) {
            if (isStatusSave) {
                $("#chkbox-" + MOGID + "").prop('checked', !datamodel.IsActive);
            }
            else if (isMOGAPLMappingSave) {
                $("#windowEditMOGWiseNutrient").parent().show();
            }
        }
    });

    $("#windowEditMOGAPLDependencies").kendoWindow({
        modal: true,
        width: "1050px",
        height: "570px",
        left: '220px !important',
        top: '20px !important',
        title: "MOG - " + MOGName + " Dependencies",
        actions: ["Close"],
        visible: false,
        animation: false,
        close: function (e) {
            if (isStatusSave) {
                $("#chkbox-" + MOGID + "").prop('checked', !datamodel.IsActive);
            }
            else if (isMOGAPLMappingSave) {
                $("#windowEditMOGWiseNutrient").parent().show();
            }
        }
    });

    $("#windowEditMOGWiseNutrient").kendoWindow({
        modal: true,
        width: "1050px",
        height: "560px",
        left: '220px !important',
        top: '80px !important',
        title: MOGName + " Details: ",
        actions: ["Close"],
        visible: false,
        animation: false
    });
    //Krish 18-10-2022

    $("#RangeTypeMOGwindowEdit").kendoWindow({
        modal: true,
        width: "500px",
        //height: "400px",
        title: "", //"Range Type Details - " + rangetypeName,
        actions: ["Close"],
        visible: false,
        animation: false,
        resizable: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $('#myInput').on('input', function (e) {
        var grid = $('#gridMOG').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "MOGCode" || x.field == "Name" || x.field == "UOMName" || x.field == "Alias" || x.field == "Status"
                    || x.field == "Sectorids" || x.field == "AllergenName" || x.field == "NGAllergens" || x.field =="Process") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    //console.log(type + " :: " + x.field);
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })                 
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        Utility.Loading();

        if (user.UserRoleId == 13) {
            $(".healthcareonly").show();
        }
        else {
            $(".healthcareonly").hide();
        }
        onLoad();
        // Utility.UnLoading();
        //setTimeout(function () {
        //    onLoad();
        //    Utility.UnLoading();
        //},2000);
    });

    function onLoad() {
        isRIEnabled = $("#isRIEnabled").val();

        $("#windowEditMOGChangeConfirm").parent().find(".k-window-action").css("visibility", "hidden");

        $("#btnExport").click(function (e) {
            var grid = $("#gridMOG").data("kendoGrid");
            grid.saveAsExcel();
        });
        $("#BtnExportTemp").click(function (e) {
            //$.ajax({
            //    url:  '/MOG/DownloadExcTemplate',
            //    type: 'GET',       
            //    contentType: 'application/json; charset=utf-8',
            //    success: function (data) {
            //        alert("success");
            //    },
            //    error: function (data) {
            //        alert(data);
            //    }
            //});

            window.location.href = baseUrl + 'MOG/DownloadExcTemplate';
        });
        $("#UploadMOGExcel").click(function (e) {

            if ($("#file").val() != "") {
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
                /*Checks whether the file is a valid excel file*/
                if (!regex.test($("#file").val().toLowerCase())) {
                    toastr.error("Please upload a valid Excel file!");
                    return false;
                }
                else {
                    UploadSelectedExcelsheet();

                }
            }
            else {
                toastr.error("Please upload a Excel file!");
                return false;
            }
        });
        $("#APLMOGbtnback").click(function () {
            //alert("call");
            $("#MOGMaster").show();
            $("#mogviewhistorymain").hide();
        });
        $("#Btnviewuploadhistory").click(function (e) {
            // window.location.href = 'MOG/GetMOGAPLHISTORY';
            populateCafeGrid();
            populateCafeGridDetails();

            $("#MOGMaster").hide();
            $("#mogviewhistorymain").show();
            $("#gridMogViewALLHistory").hide();



            $("#gridMogViewCustomHistory").show();
            $("#gridMogViewCustomHistoryDetails").show();
            $("#ALLBATCHVIEW").show();
            $("#batchsearch").hide();
            $("#GETALLMogViewALLHistoryBATCHWISE").hide();

        });
        $("#ALLBATCHVIEW").click(function (e) {
            ViewALLHistoryBatch();
            $("#gridMogViewALLHistory").show();
            $("#gridMogViewCustomHistory").hide();
            $("#gridMogViewCustomHistoryDetails").hide();

            $("#ALLBATCHVIEW").hide();
            $("#batchsearch").show();
            $("#GETALLMogViewALLHistoryBATCHWISE").show();
        });
        $('#BatchmyInput').on('input', function (e) {
            var grid = $('#gridMogViewALLHistory').data('kendoGrid');
            var columns = grid.columns;

            var filter = { logic: 'or', filters: [] };
            columns.forEach(function (x) {
                if (x.field) {
                    if (x.field == "BATCHNUMBER" || x.field == "FLAG" || x.field == "CreationTime" || x.field == "TOTALRECORDS"
                        || x.field == "FAILED" || x.field == "CreationTime" || x.field == "UPLOADBY") {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;

                        if (type == 'string') {
                            var targetValue = e.target.value;
                            filter.filters.push({
                                field: x.field,
                                operator: 'contains',
                                value: targetValue
                            })
                        }
                        else if (type == 'number') {

                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        } else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format(x.format, data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                }
            });
            grid.dataSource.filter(filter);
        });
        function populateCafeGrid() {

            Utility.Loading();
            var gridVariable = $("#gridMogViewCustomHistory").height(180);
            gridVariable.html("");
            gridVariable.kendoGrid({
                excel: {
                    fileName: "MogAPLHistory.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                //pageable: true,
                groupable: false,
                //reorderable: true,
                //scrollable: true,
                height: "180px",
                columns: [
                    {
                        field: "BATCHNUMBER", title: "Batch Number", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "MOGCode", title: "MOG Code", width: "80px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "ArticalNumber", title: "Article Number", width: "100px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    {
                        field: "BATCHSTATUS", title: "Error Message", width: "100px", attributes: {
                            "class": "FLAGCELL",
                            style: "text-align: left; font-weight:normal"
                        },
                    }


                    //{
                    //    field: "Edit", title: "Action", width: "50px",

                    //    attributes: {
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    command: [
                    //        {
                    //            name: 'Edit'
                    //        }
                    //    ],
                    //}
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetMOGAPLHISTORY, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , false);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                BATCHNUMBER: { type: "string" },
                                MOGCode: { type: "string" },
                                ArticalNumber: { type: "string" },
                                BATCHSTATUS: { type: "string" }

                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {
                    var items = e.sender.items();
                    var grid = this;
                    grid.tbody.find("tr[role='row']").each(function () {
                        var model = grid.dataItem(this);

                        if (model.BATCHSTATUS == "Uploaded") {

                            $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                            //alert("call");
                        }
                        else {
                            $(this).find(".FLAGCELL").addClass("batchstatuscolorred");

                        }
                    });
                    items.each(function (e) {
                        if (user.UserRoleId == 1) {
                            $(this).find('.k-grid-Edit').text("Edit");
                            $(this).find('.chkbox').removeAttr('disabled');

                        } else {
                            $(this).find('.k-grid-Edit').text("View");
                            $(this).find('.chkbox').attr('disabled', 'disabled');
                        }
                    });


                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
            //$(".k-label")[0].innerHTML.replace("items", "records");
        }

        var content = "";
        $("#gridMogViewALLHistory").kendoTooltip({
            filter: "td:nth-child(2), th:nth-child(2)",
            position: "center",


            content: function (e) {

                if (e.target.is("th")) {

                    return e.target.text();
                }
                var dataItem = $("#gridMogViewALLHistory").data("kendoGrid").dataItem(e.target.closest("tr"));
                console.log(dataItem);
                if (dataItem.FLAG == "1") {
                    content = "Success";
                }
                else if (dataItem.FLAG == "2") {
                    content = "Partially Success";
                }
                else {
                    content = "Failed";
                }
                return content;
            }
        }).data("kendoTooltip");
        $("#gridMogViewCustomHistoryDetails").kendoTooltip({
            filter: "td:nth-child(2), th:nth-child(2)",
            position: "center",


            content: function (e) {

                if (e.target.is("th")) {

                    return e.target.text();
                }

                var dataItem = $("#gridMogViewCustomHistoryDetails").data("kendoGrid").dataItem(e.target.closest("tr"));
                console.log(dataItem);
                if (dataItem.FLAG == "1") {
                    content = "Success";
                }
                else if (dataItem.FLAG == "2") {
                    content = "Partially Success";
                }
                else {
                    content = "Failed";
                }
                return content;
            }
        }).data("kendoTooltip");

        function populateCafeGridDetails() {

            Utility.Loading();
            var gridVariable = $("#gridMogViewCustomHistoryDetails");
            gridVariable.html("");
            gridVariable.kendoGrid({
                excel: {
                    fileName: "MogAPLHistory.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                //pageable: true,
                groupable: false,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    {
                        field: "BATCHNUMBER", title: "Batch Number", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "FLAG", title: "Batch Status", width: "80px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        }, template: '<div class="colortag"></div>'
                    },
                    {
                        field: "TOTALRECORDS", title: "Records", width: "100px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "FAILED", title: "Failed", width: "100px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        }
                    },


                    //{
                    //    field: "View", title: "View Details", width: "50px",

                    //    attributes: {
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    command: [
                    //        {
                    //            name: 'View',
                    //            click: function (e) {

                    //                var gridObj = $("#gridMogViewCustomHistoryDetails").data("kendoGrid");
                    //                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //                datamodel = tr;
                    //                BATCHNUMBER = tr.BATCHNUMBER;

                    //                alert(BATCHNUMBER);
                    //                return true;
                    //            }
                    //        }
                    //    ],
                    //}
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetMOGAPLHISTORYDetails, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , false);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                BATCHNUMBER: { type: "string" },
                                FLAG: { type: "string" },
                                TOTALRECORDS: { type: "string" },
                                FAILED: { type: "string" }

                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {
                    var items = e.sender.items();
                    var grid = this;
                    grid.tbody.find("tr[role='row']").each(function () {
                        var model = grid.dataItem(this);

                        if (model.FLAG == "1") {
                            console.log($(this));
                            console.log($(this).find(".colortag"));
                            $(this).find(".colortag").css("background-color", "green");

                        }
                        else if (model.FLAG == "2") {
                            $(this).find(".colortag").css("background-color", "orange");

                        }
                        else {

                            $(this).find(".colortag").css("background-color", "red");
                        }

                        //if (model.FLAG != "Uploaded") {
                        //    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                        //    //alert("call");
                        //}
                        //else {
                        //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //}


                    });
                    items.each(function (e) {
                        if (user.UserRoleId == 1) {
                            $(this).find('.k-grid-Edit').text("Edit");
                            $(this).find('.chkbox').removeAttr('disabled');

                        } else {
                            $(this).find('.k-grid-Edit').text("View");
                            $(this).find('.chkbox').attr('disabled', 'disabled');
                        }
                    });


                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
            //$(".k-label")[0].innerHTML.replace("items", "records");
        }

        function populateGetallBatchwishDetails(BATCHNUMBER) {

            Utility.Loading();
            var gridVariable = $("#GETALLMogViewALLHistoryBATCHWISE").height(180);
            gridVariable.html("");
            gridVariable.kendoGrid({
                excel: {
                    fileName: "MogAPLHistory.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                //pageable: true,
                groupable: false,
                //reorderable: true,
                scrollable: true,
                // height: "20px",
                columns: [
                    {
                        field: "BATCHNUMBER", title: "Batch Number", width: "80px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "MOGCode", title: "MOG Code", width: "80px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "ArticalNumber", title: "Article Number", width: "100px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "FLAG", title: "Record Status", width: "100px", attributes: {
                            "class": "FLAGCELL",
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "CreationTime", title: "Upload Time", width: "100px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        }
                    }



                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetshowGetALLHISTORYBATCHWISE, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, {
                                BATCHNUMBER: BATCHNUMBER
                            }
                                //{
                                //filter: mdl
                                //}
                                , false);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                BATCHNUMBER: { type: "string" },
                                MOGCode: { type: "string" },
                                ArticalNumber: { type: "string" },
                                FLAG: { type: "string" },
                                CreationTime: { type: "string" }

                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {
                    var items = e.sender.items();
                    var grid = this;
                    grid.tbody.find("tr[role='row']").each(function () {
                        var model = grid.dataItem(this);

                        if (model.FLAG == "Success") {
                            $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                            //alert("call");
                        }
                        else if (model.FLAG == "Uploaded" || model.FLAG == "Uploaded") {
                            $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                            //alert("call");
                        }
                        else {
                            $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                        }
                    });
                    items.each(function (e) {
                        if (user.UserRoleId == 1) {
                            $(this).find('.k-grid-Edit').text("Edit");
                            $(this).find('.chkbox').removeAttr('disabled');

                        } else {
                            $(this).find('.k-grid-Edit').text("View");
                            $(this).find('.chkbox').attr('disabled', 'disabled');
                        }
                    });


                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
            //$(".k-label")[0].innerHTML.replace("items", "records");
        }

        function ViewALLHistoryBatch() {

            Utility.Loading();
            var gridVariable = $("#gridMogViewALLHistory");
            gridVariable.html("");
            gridVariable.kendoGrid({
                excel: {
                    fileName: "MogAPLHistory.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                //pageable: true,
                groupable: false,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    {
                        field: "BATCHNUMBER", title: "Batch Number", width: "120px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "FLAG", title: "Batch Status", width: "60px", attributes: {
                            style: "text-align: center; font-weight:normal",

                        }, template: '<div class="colortag"></div>',
                        headerAttributes: {
                            style: "text-align: center;"
                        }
                    },
                    {
                        field: "TOTALRECORDS", title: "Records", width: "60px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center;"
                        }
                    },
                    {
                        field: "CreationTime", title: "Batch Upload Time", width: "80px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center;"
                        }
                    },
                    {
                        field: "FAILED", title: "Failed", width: "30px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center;"
                        }
                    },
                    {
                        field: "UPLOADBY", title: "Upload By", width: "80px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center;"
                        }
                    },


                    {
                        field: "View", title: "View Details", width: "50px",

                        attributes: {
                            style: "text-align: center; font-weight:normal;color:red!important",
                            "class": "ViewBatchcell"
                        },
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        command: [
                            {
                                name: 'View',
                                click: function (e) {
                                    var MDgridObj = $("#gridMogViewALLHistory").data("kendoGrid");
                                    var tr = MDgridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    BATCHNUMBER = tr.BATCHNUMBER;
                                    console.log(e.currentTarget);

                                    var trEdit = $(this).closest("tr");
                                    $(trEdit).find(".k-grid-View").addClass("k-state-disabled");


                                    var dialogs = $("#GETALLMogViewALLHistoryBATCHWISE").data("kendoWindow");
                                    populateGetallBatchwishDetails(BATCHNUMBER);
                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialogs.open().element.closest(".k-window").css({
                                        left: 305,
                                        top: 215
                                    });
                                    // dialogs.center();


                                    dialogs.title("Batch Details");

                                    return false;

                                }
                            }
                        ],
                    }
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetALLHISTORYDetails, function (result) {
                                Utility.UnLoading();

                                if (result != null) {
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , false);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                BATCHNUMBER: { type: "string" },
                                FLAG: { type: "string" },
                                TOTALRECORDS: { type: "string" },
                                FAILED: { type: "string" },
                                CreationTime: { type: "string" },
                                UPLOADBY: { type: "string" }

                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {
                    var items = e.sender.items();
                    var grid = this;
                    grid.tbody.find("tr[role='row']").each(function () {
                        var model = grid.dataItem(this);
                        $(this).find(".k-grid-View").addClass("ViewBatch");
                        //if (model.FLAG != "Uploaded") {
                        //    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                        //    //alert("call");
                        //}
                        //else {
                        //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //}
                        if (model.FLAG == "1") {

                            $(this).find(".colortag").css("background-color", "green");

                        }
                        else if (model.FLAG == "2") {
                            $(this).find(".colortag").css("background-color", "orange");

                        }
                        else {

                            $(this).find(".colortag").css("background-color", "red");
                        }
                    });
                    items.each(function (e) {
                        if (user.UserRoleId == 1) {
                            $(this).find('.k-grid-Edit').text("Edit");
                            $(this).find('.chkbox').removeAttr('disabled');

                        } else {
                            $(this).find('.k-grid-Edit').text("View");
                            $(this).find('.chkbox').attr('disabled', 'disabled');
                        }
                    });


                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
            //$(".k-label")[0].innerHTML.replace("items", "records");
        }

        function UploadSelectedExcelsheet() {

            var data = new FormData();
            var i = 0;
            var fl = $("#file").get(0).files[0];

            if (fl != undefined) {

                data.append("file", fl);

            }
            //HttpClient.MakeSyncRequest(CookBookMasters.UploadExcelsheet, function () { },
            //    {
            //        data: data
            //    } ,
            // null, true);
            Utility.Loading();
            $.ajax({
                type: "POST",
                url: CookBookMasters.UploadExcelsheetMOG,
                contentType: false,
                processData: false,
                async: false,
                data: data,
                success: function (result) {
                    if (result == 'Artical Number is null') {
                        toastr.error("Can not be Artical Number is Null");
                        $(".k-window").hide();
                        $(".k-overlay").hide();


                        //return true;
                        Utility.UnLoading();
                    }

                    else if (result == 'Artical Number is Dupliacte') {
                        toastr.error("Artical Number is Dupliacte");
                        $(".k-window").hide();
                        $(".k-overlay").hide();

                        //return true;
                        Utility.UnLoading();
                    }

                    else if (result == 'Success') {
                        toastr.success("Upload successfully");
                        $(".k-window").hide();
                        $(".k-overlay").hide();
                        $("#gridMOG").data("kendoGrid").dataSource.data([]);
                        $("#gridMOG").data("kendoGrid").dataSource.read();
                        // return true;
                        Utility.UnLoading();
                    }
                    else if (result == 'Artical Number does not exits in APL Master') {
                        toastr.error("Artical Number does not exits in APL Master");
                        $(".k-window").hide();
                        $(".k-overlay").hide();

                        // return true;
                        Utility.UnLoading();
                    }
                    result = "";
                    return false;
                },
                error: function (xhr, status, p3, p4) {
                    var err = "Error " + " " + status + " " + p3 + " " + p4;
                    if (xhr.responseText && xhr.responseText[0] == "{")
                        err = JSON.parse(xhr.responseText).Message;
                    alert(err);
                    Utility.UnLoading();
                    return false;
                }
            });
        }
        //Krish
        //13-07-2022
        var elementaryData = [];

        elementaryData.push({ "Name": "Both", "Value": "All" });
        elementaryData.push({ "Name": "Elementary", "Value": "Elementory" });
        elementaryData.push({ "Name": "Non Elementary", "Value": "NonElementory" });
        populateElementaryFilter();

        HttpClient.MakeRequest(CookBookMasters.GetApplicationSettingDataList, function (result) {

            if (result != null) {
                applicationSettingData = result;
                colorFilterData = [];
                for (item of applicationSettingData) {
                    //if (item.Code == "ASS-00004")//item.Name == "Major Identification Threshold" || 
                    //    MajorPercentData = item.Value;
                    //$(".asPercent").text(MajorPercentData);
                    if (item.Code == "ASS-00012" || item.Code == "ASS-00013" || item.Code == "ASS-00014" || item.Code == "ASS-00015") {
                        colorFilterData.push({ "text": item.Value, "value": item.Code });
                        //if (item.Code == "ASS-00013") {
                        //    $("#selColor1").css("background-color", item.Value);
                        //    $("#selColor1").attr("data-colorcode", item.Code);
                        //}
                        //if (item.Code == "ASS-00014") {
                        //    $("#selColor2").css("background-color", item.Value);
                        //    $("#selColor2").attr("data-colorcode", item.Code);
                        //}
                        //if (item.Code == "ASS-00012") {
                        //    $("#selColor3").css("background-color", item.Value);
                        //    $("#selColor3").attr("data-colorcode", item.Code);
                        //}
                        //if (item.Code == "ASS-00015") {
                        //    $("#selColor4").css("background-color", item.Value);
                        //    $("#selColor4").attr("data-colorcode", item.Code);
                        //}
                    }
                }
                //Krish 18-07-2022
                //populateColorFilter();
                //elementaryData.push({ "Name": "All", "Value": "All" });
                //elementaryData.push({ "Name": "Elementary", "Value": "Elementory" });
                //elementaryData.push({ "Name": "Non Elementary", "Value": "NonElementory" });
                //populateElementaryFilter();
            }
            else {

            }
        }, null

            , false);

        //Krish 18-07-2022
        //$(".checkboxcol").click(function (e) {
        //    var sourceId = e.target.id;
        //    //if (sourceId == "chkColorAll1") {
        //    //}
        //    //else if (sourceId == "chkColorAll2") {
        //    //}
        //    //else if (sourceId == "chkColorAll3") {
        //    //}
        //    //else if (sourceId == "chkColorAll4") {
        //    //}
        //    populateMOGGrid();
        //});
        $(".checkboxcol").click(function (e) {
            var sourceId = e.target.id;
            if (sourceId == "chkColorAll") {
                //var isChecked = $(this).is(":checked");
                //$(".checkbox").each(function () {
                //    if (isChecked)
                //        $(this).attr('checked', 'checked');
                //    else
                //        $(this).removeAttr('checked');
                //});
                $("#chkColorContains").removeAttr('checked');
                $("#chkColorNg").removeAttr('checked');
            }
            else {
                //$(".checkbox").each(function () {
                //    if ($(this).id != sourceId)
                //        $(this).removeAttr('checked');
                //});
                $("#chkColorAll").removeAttr('checked');
            }
            if ($("#chkColorAll").is(":checked")) {
                //var color = colorFilterData.filter(a => a.value == "ASS-00012")[0].text;
                //$("#selColor").css("background-color", color);
                $("#selColor").css("background-color", "#fff");
                $("#selColor").attr("data-colorcode", "");
            }
            else if ($("#chkColorContains").is(":checked") && !$("#chkColorNg").is(":checked")) {
                var color = colorFilterData.filter(a => a.value == "ASS-00013")[0].text;
                $("#selColor").css("background-color", color);
                $("#selColor").attr("data-colorcode", "ASS-00013");
            }
            else if ($("#chkColorNg").is(":checked") && !$("#chkColorContains").is(":checked")) {
                var color = colorFilterData.filter(a => a.value == "ASS-00014")[0].text;
                $("#selColor").css("background-color", color);
                $("#selColor").attr("data-colorcode", "ASS-00014");
            }
            else if (!$("#chkColorNg").is(":checked") && !$("#chkColorContains").is(":checked")) {
                var color = colorFilterData.filter(a => a.value == "ASS-00015")[0].text;
                $("#selColor").css("background-color", color);
                $("#selColor").attr("data-colorcode", "ASS-00015");
            }
            else if ($("#chkColorNg").is(":checked") && $("#chkColorContains").is(":checked")) {
                var color = colorFilterData.filter(a => a.value == "ASS-00012")[0].text;
                $("#selColor").css("background-color", color);
                $("#selColor").attr("data-colorcode", "ASS-00012");
            }

            //$(".checkbox").each(function () {
            //    if ($(this).is(":checked")) {
            //        var id = $(this).id;
            //        if (id == "chkColorAll") {
            //            var color = colorFilterData.filter(a => a.value == "ASS-00012")[0].text;
            //            $("#selColor").css("background-color", color);
            //        }
            //        else if (id == "chkColorContains") {
            //            var color = colorFilterData.filter(a => a.value == "ASS-00012")[0].text;
            //            $("#selColor").css("background-color", color);
            //        }
            //    }
            //});
            populateMOGGrid();
        });

        function populateColorFilter() {
            $("#msColorFilter").kendoMultiSelect({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: colorFilterData,
                placeholder: " ",
                index: 0,
                autoBind: true,
                autoClose: false,
                change: onColorFilterMultiChange
            });
            $("#msColorFilter_listbox li").each(function () {
                $(this).css('background-color', $(this).html());
                $(this).html('')
            });
        }
        function populateElementaryFilter() {

            $("#msElementaryFilter").kendoDropDownList({
                filter: "contains",
                dataTextField: "Name",
                dataValueField: "Value",
                dataSource: elementaryData,
                index: 0,
                change: onElementaryFilterChange
            });
        }

        function onElementaryFilterChange(eid) {
            var val = eid.sender.dataItem(eid.item).Value;
            console.log(val)
            //if (val == "Non Elementary")
            //    isShowElmentoryMOG = false;
            //else if (val == "Elementary")
            //    isShowElmentoryMOG = true;
            //else
            //    isShowElmentoryMOG = false;
            elementaryCode = val;
            populateMOGGrid();
        }

        function onColorFilterMultiChange() {
            //var originalData = [];
            //var multiselectsite = $("#msColorFilter").data("kendoMultiSelect");
            ////console.log(multiselectsite.value());
            //var selectedFilter = multiselectsite.value();
            ////if (originalData.length == 0)
            ////originalData.push(nationalmogdata);
            //var dataFiltered = $("#gridMOG").data("kendoGrid").dataSource.dataFiltered();
            ////console.log(nationalmogdata)
            //if (selectedFilter.length == 0) {
            //    var dataSource = new kendo.data.DataSource({
            //        data: dataFiltered
            //    });
            //    var grid = $("#gridMOG").data("kendoGrid");
            //    grid.setDataSource(dataSource);
            //}
            //else {
            //    var filteredData = [];
            //    for (var i = 0; i < selectedFilter.length; i++) {
            //        //console.log(selectedFilter[i])
            //        if (selectedFilter[i] == "ASS-00012") {
            //            var data = dataFiltered.filter(a => a.AllergenName != "None" && a.NGAllergens != "None");
            //            //console.log(data)
            //            //filteredData.push(data);
            //            data.filter(a => filteredData.push(a));
            //        }
            //        else if (selectedFilter[i] == "ASS-00013") {
            //            var data = dataFiltered.filter(a => a.AllergenName == "None" && a.NGAllergens != "None");
            //            data.filter(a => filteredData.push(a));
            //        }
            //        else if (selectedFilter[i] == "ASS-00014") {
            //            var data = dataFiltered.filter(a => a.AllergenName != "None" && a.NGAllergens == "None")
            //            data.filter(a => filteredData.push(a));
            //        }
            //        else if (selectedFilter[i] == "ASS-00015") {
            //            var data = dataFiltered.filter(a => a.AllergenName == "None" && a.NGAllergens == "None")
            //            data.filter(a => filteredData.push(a));
            //        }

            //    }
            //    //console.log(filteredData)
            //    var dataSource = new kendo.data.DataSource({
            //        data: filteredData
            //    });
            //    var grid = $("#gridMOG").data("kendoGrid");
            //    grid.setDataSource(dataSource);
            //    grid.refresh();
            //}
            populateMOGGrid();
            $("#msColorFilter_taglist li").each(function () {
                $(this).css('background-color', $('span:first', this).html());
                $(this).addClass('kbuttonhover');

                $(this).mouseenter(function () {
                    $(this).attr("style", "background-color:" + $('span:first', this).html() + "!important");
                    //.css("border-radius", "3px");
                }).mouseleave(function () {
                    $(this).attr("style", "background-color:" + $('span:first', this).html());
                    //.css("border-radius", "0px");
                });
            });
        }



        HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            if (user.UserRoleId == 13) {
                $(".healthcareonly").show();
                $(".healthcarehide").hide();
            }
            else {
                $(".healthcareonly").hide();
                $(".healthcarehide").show();
            }
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNew").css("display", "none");
            //}
            //Krish
            //13-07-2022
            //populateMOGGrid();
            populateMOGGrid("refresh");
                $("#AddNew").hide();
                $("#btnExport").hide();
                $("#sectorSelector").hide();
                $("#searchInput").css("float", "right");
                HttpClient.MakeRequest(CookBookMasters.GetProcessMappingDataList, function (result) {
                    //console.log(result)
                    processmogmappingdata = [];
                    processmogmappingdata = result;
                });
                HttpClient.MakeRequest(CookBookMasters.GetProcessDataList, function (result) {
                    processTypeData = [];
                    //processTypeData.push({ "value": "Select", "text": "Select" });
                    //rangeTypeData = result;
                    //console.log(result)

                    var dataSource = result.filter(a => a.IsActive == true);

                    for (var i = 0; i < dataSource.length; i++) {
                        processTypeData.push({ "value": dataSource[i].Code, "text": dataSource[i].Name });
                    }
                    //console.log(processTypeData)
                    //populateProcessDropdown();
                });
        }, null, true);

        HttpClient.MakeSyncRequest(CookBookMasters.GetNutritionMasterDataList, function (data) {
            nutrientMasterdataSource = data.filter(m => m.IsActive && m.NutritionElementCode != "NTR-00021" && m.NutritionElementCode != "NTR-00022");
        }, null, true);

        HttpClient.MakeSyncRequest(CookBookMasters.GetSectorDataList, function (data) {
            var dataSource = data;
            sectorMasterdataSource = [];
            var chkboxele = "<table>";
            for (var i = 0; i < dataSource.length; i++) {
                sectorMasterdataSource.push({ "value": dataSource[i].ID, "text": dataSource[i].SectorName, "SectorNumber": dataSource[i].SectorNumber });

                //Krish 18-07-2022
                //if (dataSource[i].SectorName.toLowerCase() != "heroca") {
                //    if (i == 0)
                //        chkboxele += "<tr>";
                //    chkboxele += "<td style='width:auto;white-space: nowrap;'><label class='checkbox-container' style='display: inline-block;padding-top:1px;margin-right: 4px;' ><input type='checkbox' onchange='sectorFilter();' class='checkboxsec' id='" + dataSource[i].SectorName + "' ><span class='checkmarkgrid'></span></label>" + dataSource[i].SectorName + "&nbsp;&nbsp;</td>";
                //    if (user.UserRoleId == 13) {
                //        if (i == 3)
                //            chkboxele += "<tr/><tr>";
                //    }
                //}
            }
            //Krish 07-08-2022
            var coredata = dataSource.filter(a => a.SectorNumber == "30")[0];
            //if (dataSource[i].SectorName.toLowerCase() != "heroca") {
            //   if (i == 0)
            console.log(coredata)
            if (coredata != undefined) {
                chkboxele += "<tr>";
                chkboxele += "<td style='width:auto;white-space: nowrap;'><label class='checkbox-container' style='display: inline-block;padding-top:1px;margin-right: 4px;' ><input type='checkbox' onchange='sectorFilter();' class='checkboxsec' id='" + coredata.SectorName + "' ><span class='checkmarkgrid'></span></label>" + coredata.SectorName + "&nbsp;&nbsp;</td>";
                coredata = dataSource.filter(a => a.SectorNumber == "80")[0];
                chkboxele += "<td style='width:auto;white-space: nowrap;'><label class='checkbox-container' style='display: inline-block;padding-top:1px;margin-right: 4px;' ><input type='checkbox' onchange='sectorFilter();' class='checkboxsec' id='" + coredata.SectorName + "' ><span class='checkmarkgrid'></span></label>" + coredata.SectorName + "&nbsp;&nbsp;</td>";
                coredata = dataSource.filter(a => a.SectorNumber == "100")[0];
                chkboxele += "<td style='width:auto;white-space: nowrap;'><label class='checkbox-container' style='display: inline-block;padding-top:1px;margin-right: 4px;' ><input type='checkbox' onchange='sectorFilter();' class='checkboxsec' id='" + coredata.SectorName + "' ><span class='checkmarkgrid'></span></label>" + coredata.SectorName + "&nbsp;&nbsp;</td>";
                coredata = dataSource.filter(a => a.SectorNumber == "10")[0];
                chkboxele += "<td style='width:auto;white-space: nowrap;'><label class='checkbox-container' style='display: inline-block;padding-top:1px;margin-right: 4px;' ><input type='checkbox' onchange='sectorFilter();' class='checkboxsec' id='" + coredata.SectorName + "' ><span class='checkmarkgrid'></span></label>" + coredata.SectorName + "&nbsp;&nbsp;</td>";
                if (user.UserRoleId == 13) {
                    //if (i == 3)
                    chkboxele += "<tr/><tr>";
                }
                coredata = dataSource.filter(a => a.SectorNumber == "20")[0];
                chkboxele += "<td style='width:auto;white-space: nowrap;'><label class='checkbox-container' style='display: inline-block;padding-top:1px;margin-right: 4px;' ><input type='checkbox' onchange='sectorFilter();' class='checkboxsec' id='" + coredata.SectorName + "' ><span class='checkmarkgrid'></span></label>" + coredata.SectorName + "&nbsp;&nbsp;</td>";
            }
            //}

            chkboxele += "<tr/></table>";
            if (user.UserRoleId != 13) {
                $("#msSectorFilter").parent().closest('div').removeClass("col-md-8").addClass("col-md-12");
            }
            $("#msSectorFilter").html(chkboxele);
            populateSectorMasterDropdown();

        }, null, true);

        //HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

        //    var dataSource = data;
        //    uomdata = [];
        //    uomdata.push({ "value": "Select", "text": "Select" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        uomdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].Name });
        //    }
        //    console.log(uomdata)
        //    populateUOMDropdown();
        //    populateUOMDropdown_Bulk();
        //}, null, true);

        HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
            var dataSource = data;
            uommodulemappingdata = [];
            uommodulemappingdata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                uommodulemappingdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].UOMName, "UOMModuleCode": dataSource[i].UOMModuleCode });
            }
            uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001' || m.text == "Select");
            populateUOMDropdown();
            populateUOMDropdown_Bulk();

        }, null, false);

        $("#gridBulkChange").css("display", "none");
        populateBulkChangeControls();
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        changeControls.css("background-color", "#fff");
        changeControls.css("border", "none");
        changeControls.eq(3).text("");
        changeControls.eq(3).append("<div id='uom' style='width:100%; font-size:10.5px!important'></div>");


    }


    $("#InitiateBulkChanges").on("click", function () {
        $("#gridBulkChange").css("display", "block");
        $("#gridBulkChange").children(".k-grid-header").css("border", "none");
        $("#InitiateBulkChanges").css("display", "none");
        $("#CancelBulkChanges").css("display", "inline");
        $("#SaveBulkChanges").css("display", "inline");
    });

    function populateUOMDropdown_Bulk() {
        $("#uom").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: uomdata,
            index: 0,
        });
    }
    function populateUOMDropdown() {
        $("#inputuom").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: uomdata,
            index: 0,
        });
    }

    //$("#inputcpu").data("kendoDropDownList").select(function (dataItem) {
    //    return dataItem.value === dataSource[0].value;
    //});
    //$("#inputcpu").data("kendoDropDownList").select(0);

    $("#CancelBulkChanges").on("click", function () {
        $("#InitiateBulkChanges").css("display", "inline");
        $("#CancelBulkChanges").css("display", "none");
        $("#SaveBulkChanges").css("display", "none");
        $("#gridBulkChange").css("display", "none");
        populateDishGrid();
    });

    $("#SaveBulkChanges").on("click", function () {

        var neVal = $(this).val();
        var dataFiltered = $("#gridMOG").data("kendoGrid").dataSource.dataFiltered();


        var model = dataFiltered;
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        var validChange = false;
        var uom = "";

        var uomspan = $("#uom");

        if (uomspan.val() != "Select") {
            validChange = true;
            uom = uomspan.val();
        }
        if (validChange == false) {
            toastr.error("Please change at least one attribute for Bulk Update");
        } else {
            Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                function () {
                    $.each(model, function () {
                        this.CreatedOn = kendo.parseDate(this.CreatedOn);
                        this.ModifiedOn = Utility.CurrentDate();
                        this.ModifiedBy = user.UserId;
                        if (uom != "")
                            this.UOM_ID = uom;
                    });

                    HttpClient.MakeRequest(CookBookMasters.SaveMOGDataList, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again.");
                        }
                        else {
                            $(".k-overlay").hide();
                            toastr.success("Records have been updated successfully");
                            $("#gridMOG").data("kendoGrid").dataSource.data([]);
                            $("#gridMOG").data("kendoGrid").dataSource.read();
                            $("#InitiateBulkChanges").css("display", "inline");
                            $("#CancelBulkChanges").css("display", "none");
                            $("#SaveBulkChanges").css("display", "none");
                            $("#gridBulkChange").css("display", "none");
                        }
                    }, {
                        model: model

                    }, true);
                },
                function () {
                    maptype.focus();
                }
            );
        }
    });
    //Food Program Section Start




    function populateBulkChangeControls() {


        var gridVariable = $("#gridBulkChange");
        gridVariable.html("");
        gridVariable.kendoGrid({
            selectable: "cell",
            sortable: false,
            filterable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "MOGCode", title: "", width: "50px", attributes: {
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "MOG Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "", title: "MOG Alias", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },

                {
                    field: "", title: "", width: "40px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "40px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "30px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "35px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                }
            ],
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
            },
            change: function (e) {
            },
        });
    }

    //add by gaurav saini

    $("#ExcelUpload").on("click", function () {

        if ($("#file").val() != "") {
            $("#file").val('');
        }
        var dialogs = $("#UploadExcelFile").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialogs.open().element.closest(".k-window").css({
            top: 167,
        });
        dialogs.center();

        dialogs.title("Upload Excel");
    })


    $("#AddNew").on("click", function () {
        var model;
        var dialog = $("#NationalwindowEdit").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open().element.closest(".k-window").css({
            top: 167,
        });
        dialog.center();
        datamodel = model;

        $(".mogEditDiv").css("display", "block");
        $(".mogViewDiv").css("display", "none");

        $("#mogid").text("");
        $("#mogcode").val("");
        $("#inputmogname").val("").removeAttr("disabled");
        $("#inputmogalias").val("");

        $("#inputuom").data('kendoDropDownList').value("Select");
        $("#inputuom").data('kendoDropDownList').enable(true);
        dialog.title("New MOG Creation");
    })

    $("#NatiMogbtnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#NationalwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#btnDoLater").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditMOGDependencies").data("kendoWindow");
        orderWindow.close();
        if (isStatusSave) {
            $("#chkbox-" + MOGID + "").prop('checked', !datamodel.IsActive);
        }
        else if (isMOGAPLMappingSave) {
            $("#windowEditMOGWiseNutrient").parent().show();
        }
    });

    $("#btnAgree").on("click", function () {
        if (isStatusSave) {
            if (mogImpactedData.IsMOGImpacted) {
                return;
            }
            SaveMOGStatus();
        }
        else
            saveMOGData();
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditMOGDependencies").data("kendoWindow");
        orderWindow.close();
    });

    $("#btnViewDependencies").click(function () {

        showMOGDependencies();
    });



    function sendMOGEmail() {
        Utility.Loading();
        HttpClient.MakeRequest(CookBookMasters.SendMOGEmail, function (result) {
            if (result == false) {
                // toastr.error("Some error occured, please try again");
            }
            else {
                if (isStatusSave) {
                    //toastr.success("MOG status updated successfully");
                }
                else {
                    //   toastr.success("MOG updated successfully");
                }
            }
        }, {
            model: datamodel
        }, true);
    }


    $("#NatiMogbtnSubmit").click(function () {

        if ($("#inputmogname").val() === "") {
            toastr.error("Please provide MOG Name");
            $("#inputmogname").focus();
            return;
        }
        debugger;
        var mogname = $("#inputmogname").val().toLowerCase().trim();
        var mogalias = $("#inputmogalias").val().toLowerCase().trim();

        if (datamodel == null) { //Duplicate Check
            for (item of nationalmogdata) {
                if (item.Name.toLowerCase().trim() == mogname.toLowerCase().trim()) {
                    if (item.IsActive) { toastr.error("Please check Duplicate MOG Name"); }
                    else { toastr.error("Please check MOG Name already exists which is deactivated. Please connect with IT team to re-active it."); }
                    $("#inputmogname").focus();
                    return;
                }
            }
        } else {
            for (item of nationalmogdata) {
                if (item.Alias != null && mogalias != null && mogalias != "" && item.Alias.toLowerCase().trim() == mogalias.toLowerCase().trim() && item.MOGCode != datamodel.MOGCode) {
                    if (item.IsActive) { toastr.error("Please check Duplicate  MOG Alias is entered on editing"); }
                    else { toastr.error("Please check MOG Alias already exists which is deactivated. Please connect with IT team to re-active it."); }
                    $("#inputmogalias").focus();
                    return;
                }
            }
        }
        if ($("#inputuom").val() === "" || $("#inputuom").val() == "Select") {
            toastr.error("Please select UOM");
            $("#inputuom").focus();
            return;
        }


        else {
            NewMOGName = $("#inputmogname").val();

            if ($("#inputmogname").val() !== MOGName) {
                $(".oldmogname").text(MOGName);
                $(".oldmogcode").text(MOGCode);
                $(".newmogname").text(NewMOGName);
                $("#NationalwindowEdit").parent().hide();
                getMOGDependenciesData();
                if (mogImpactedData.IsMOGImpacted) {
                    showMOGDependencies();
                }
                else {
                    saveMOGData();
                }
            }
            else {
                saveMOGData();
            }
        }
    });



    function SaveMOGStatus() {

        HttpClient.MakeSyncRequest(CookBookMasters.SaveMOGData, function (result) {
            if (result == false) {
                toastr.error("Some error occured, please try again");
            }
            else {
                toastr.success("MOG status updated successfully");
                $("#gridMOG").data("kendoGrid").dataSource.data([]);
                $("#gridMOG").data("kendoGrid").dataSource.read();
                if (mogImpactedData.IsMOGImpacted) {
                    sendMOGEmail();
                }
            }
        }, {
            model: datamodel
        }, true);
    }

    function saveMOGData() {
        var sectordata = [];
        var sectors = "";
        var dropdownlist = $("#inputSector").data("kendoMultiSelect");

        var dataItems = dropdownlist.dataItems();
        if (dataItems != undefined && dataItems != null) {
            for (dataItem of dataItems) {
                sectordata.push(dataItem.value);
            }
            sectors = sectordata.toString();
        }

        var model;
        if (datamodel != null) {
            model = datamodel;
            model.MOGCode = $("#mogcode").val();
            model.Name = $("#inputmogname").val();
            model.Alias = $("#inputmogalias").val();
            model.UOMCode = $("#inputuom").val();
            model.OldMOGName = MOGName;
            model.CreatedOn = kendo.parseDate(model.CreatedOn);
            model.Sectors = sectors;
            model.IsNationalMOGData = true;
        }
        else {
            model = {
                "ID": $("#dishid").val(),
                "MOGCode": $("#mogcode").val(),
                "Name": $("#inputmogname").val(),
                "Alias": $("#inputmogalias").val(),
                "UOMCode": $("#inputuom").val(),
                "IsActive": true,
                "Status": 1,
                "OldMOGName": MOGName,
                "Sectors": sectors,
                "IsNationalMOGData": true
            }
        }
        if (!sanitizeAndSend(model)) {
            return;
        }
        $("#NatiMogbtnSubmit").attr('disabled', 'disabled');
        HttpClient.MakeRequest(CookBookMasters.SaveMOGData, function (result) {
            if (result == false) {
                $('#NatiMogbtnSubmit').removeAttr("disabled");
                toastr.error("Some error occured, please try again");
                $("#inputmogname").focus();
            }
            else {
                $(".k-overlay").hide();
                var orderWindow = $("#NationalwindowEdit").data("kendoWindow");
                orderWindow.close();
                $('#NatiMogbtnSubmit').removeAttr("disabled");
                if (model.ID > 0) {


                    toastr.success("MOG configuration updated successfully");
                }
                else {
                    toastr.success("New MOG added successfully");
                    sendNewMOGEmail(model);
                }

                $("#gridMOG").data("kendoGrid").dataSource.data([]);
                $("#gridMOG").data("kendoGrid").dataSource.read();
                if (mogImpactedData.IsMOGImpacted) {
                    datamodel = model;
                    sendMOGEmail();
                }
            }
        }, {
            model: model
        }, true);
    }

    function sendNewMOGEmail(model) {
        HttpClient.MakeRequest(CookBookMasters.SendNewMOGEmail, function (result) {
            if (result == false) {
            }
            else {
            }
        }, {
            model: model
        }, true);
    }


    function populateNutrientsMasterDropdown() {
        HttpClient.MakeSyncRequest(CookBookMasters.GetNutritionMasterDataList, function (data) {
            nutrientMasterdataSource = data.filter(m => m.IsActive);
            //nutrientMasterdataSource.unshift({ "NutritionElementCode": "Select", "Name": "Select Nutrient" })

        }, null, true);

        nutrientFilterDataSource = nutrientMasterdataSource.filter(function (item) {
            if (!item.MOGCode)
                return item;
        });


        $("#inputNutrient").kendoMultiSelect({
            filter: "contains",
            dataTextField: "Name",
            dataValueField: "NutritionElementCode",
            dataSource: nutrientFilterDataSource,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
        });
        var multiselect = $("#inputNutrient").data("kendoMultiSelect");

        //clear filter
        multiselect.dataSource.filter({});

        //set value
        // multiselect.value(mogWiseNutrientArray);

    }

    function populateSectorMasterDropdown() {

        //var sectorFilterDataSource = sectorMasterdataSource.filter(function (item) {
        //    if (mogWiseNutrientArray.indexOf(parseInt(item.ArticleID)) == -1) {
        //        return item;
        //    }
        //});

        $("#inputSector").kendoMultiSelect({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "text",
            dataSource: sectorMasterdataSource,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
        });
        var multiselect = $("#inputSector").data("kendoMultiSelect");

        //clear filter
        //multiselect.dataSource.filter({});

        //set value
        // multiselect.value(mogWiseNutrientArray);

        //Krish
        //13-07-2022
        //$("#msSectorFilter").kendoMultiSelect({
        //    filter: "contains",
        //    dataTextField: "text",
        //    dataValueField: "text",
        //    dataSource: sectorMasterdataSource,
        //    placeholder: " ",
        //    index: 0,
        //    autoBind: true,
        //    autoClose: false,
        //    change: onChangeSectorFilter
        //});
    }
    function onChangeSectorFilter() {
        populateMOGGrid();
    }
    $("#showSelection").bind("click", function () {
        var checked = [];
        for (var i in checkedIds) {
            if (checkedIds[i]) {
                checked.push(i);
            }
        }
    });

    $('input[type=radio][name=elementary]').change(function () {
        elementaryCode = this.value;
        populateMOGGrid();
    });

});

function SaveMOGElementoryStatus() {

    HttpClient.MakeSyncRequest(CookBookMasters.SaveMOGData, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again");
        }
        else {
            toastr.success("MOG status updated successfully");
            $("#gridMOG").data("kendoGrid").dataSource.data([]);
            $("#gridMOG").data("kendoGrid").dataSource.read();
        }
    }, {
        model: datamodel
    }, true);
}


function populateMOGGrid(refresh) {
    //Krish 13-07-2022
    if (refresh != undefined)
        Utility.Loading();
    setTimeout(function () {
        var gridVariable = $("#gridMOG");
        gridVariable.html("");
            //Krish 15-10-2022
            getProcessAndRangeTypeData();
            //$("#AddNew").hide();
            //$("#btnExport").hide();
            //$("#sectorSelector").hide();
            gridVariable.kendoGrid({
                excel: {
                    fileName: "MOG.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                groupable: false,
                noRecords: {
                    template: "No Records Available"
                },
                pageable: {
                    pageSize: 100,
                    messages: {
                        display: "{0}-{1} of {2} records"
                    }
                },
                //pageable: true,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    {
                        field: "MOGCode", title: "MOG Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: left;"
                        }
                    },
                    {
                        field: "Name", title: "MOG Name", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                        //,
                        //template: '# if(isRIEnabled){#<span sytle="cursor: pointer !important;" onClick="showMOGImpacts(this)"><i title="View Dependencies" class="fas fa-link m-link"></i></span>#}#<span class="mogname"> #:Name#</span>',
                    },
                    {
                        field: "Alias", title: "MOG Alias", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    //{
                    //    field: "UOMCode", title: "UOM", width: "20px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    template: '# if (UOMCode == "UOM-00001") {#<span>Kg</span>#} else if (UOMCode == "UOM-00002") {#<span>Ltr</span>#} else if (UOMCode == "UOM-00003") {#Pcs#}#',

                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    }
                    //},
                    //{
                    //    field: "Sectorids", title: "Sector Name", width: "60px", attributes: {
                    //        style: "text-align: left; font-weight:normal"
                    //    },
                    //    template: '<div class="sectordiv"></div>',
                    //    //template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Mapped#}#',
                    //},
                    {
                        field: "UOMName", title: "UOM", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                        //,template: '# if(isRIEnabled){#<span sytle="cursor: pointer !important;" onClick="showMOGImpacts(this)"><i title="View Dependencies" class="fas fa-link m-link"></i></span>#}#<span class="mogname"> #:Name#</span>',
                    },
                    {
                        field: "Process", title: "Process", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                        //,template: '<div class="processdiv"></div>',
                        //template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Mapped#}#',
                    },
                    //{
                    //    field: "IsActive",
                    //    title: "Active", width: "40px",
                    //    attributes: {
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    template: '<label class= "switch"><input type="checkbox" id="chkbox-#:ID#" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                    //},

                    {
                        field: "Edit", title: "Action", width: "60px",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                        command: [
                            {
                                name: 'Map Process',
                                click: function (e) {
                                    var gridObj = $("#gridMOG").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    MOGName = tr.Name;
                                    MOGCode = tr.MOGCode;
                                    MOGID = tr.ID;
                                    $("#rtMOGCode").text(tr.MOGCode);
                                    $("#rtMOGName").text(tr.Name);
                                    $("#rtUom").text(tr.UOMName);

                                    var inputmultiProcessdata = [];
                                    var multiselect = $("#inputmultiProcess").data("kendoMultiSelect");
                                    if (multiselect != undefined) {
                                        multiselect.dataSource.filter({});
                                        multiselect.value([""]);
                                    }

                                    HttpClient.MakeRequest(CookBookMasters.GetProcessTypeByMOGDataList, function (result) {
                                        //console.log(result)
                                        if (result.length > 0) {
                                            var pvalues = result.map(function (v) {
                                                return v.ProcessTypeCode;
                                            });
                                            if (multiselect != undefined)
                                                multiselect.value(pvalues);


                                            //var stdDuration = result.filter(a => a.StandardDuration != null)[0];
                                            //if (stdDuration != undefined && stdDuration.length>0)

                                            $("#inputStdDuration").val(result[0].StandardDuration);

                                            //var rangeType = result.filter(a => a.RangeTypeCode != null)[0];
                                            //if (rangeType != undefined && rangeType.length > 0)
                                            $("#rtRangeType").data('kendoDropDownList').value(result[0].RangeTypeCode);


                                            onmultiProcessChange("edit");
                                        }
                                        else {
                                            $("#inputStdDuration").val("");
                                            onmultiProcessChange();
                                        }
                                    }, { mogCode: tr.MOGCode });





                                    var dialog = $("#RangeTypeMOGwindowEdit").data("kendoWindow");

                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open().element.closest(".k-window").css({
                                        top: 167,

                                    });
                                    //dialog.open();
                                    dialog.center();
                                    dialog.title(tr.Name);


                                    //isStatusSave = false;
                                    //var gridObj = $("#gridMOG").data("kendoGrid");
                                    //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    //datamodel = tr;
                                    //MOGName = tr.Name;
                                    //MOGCode = tr.MOGCode;
                                    //MOGID = tr.ID;
                                    //var dialog = $("#NationalwindowEdit").data("kendoWindow");

                                    //$(".k-overlay").css("display", "block");
                                    //$(".k-overlay").css("opacity", "0.5");
                                    //dialog.open().element.closest(".k-window").css({
                                    //    top: 167
                                    //});
                                    //dialog.center();

                                    //$("#mogCodeDiv").css('display', 'block')

                                    //$("#mogcode").attr('disabled', 'disabled');
                                    //$("#mogid").val(tr.ID);

                                    //$("#mogcodelabel").text(tr.MOGCode);
                                    //$("#inputmognamelabel").text(tr.Name);
                                    //$("#inputuomnamelabel").text(tr.UOMCode);
                                    //$(".mogEditDiv").css("display", "none");
                                    //$(".mogViewDiv").css("display", "block");
                                    //$("#inputmogname").val(tr.Name);
                                    //$("#inputmogname").attr("disabled", "disabled");
                                    //$("#inputmogalias").val(tr.Alias);
                                    //$("#mogcode").val(tr.MOGCode);
                                    //$("#inputuom").data('kendoDropDownList').value(tr.UOMCode);
                                    //$("#inputuom").data('kendoDropDownList').enable(false);

                                    //var Allergendata = [];
                                    //var multiselect = $("#inputSector").data("kendoMultiSelect");

                                    //multiselect.dataSource.filter({});
                                    //multiselect.value([""]);
                                    //if (tr.Sectorids != undefined && tr.Sectorids != null) {
                                    //    tr.Sectorids = tr.Sectorids.replace(/,\s*$/, "");
                                    //    var array = tr.Sectorids.split(',');
                                    //    if (tr.Sectorids != undefined && tr.Sectorids != null) {
                                    //        for (alergen of array) {
                                    //            Allergendata.push(alergen.toString());
                                    //        }
                                    //    }

                                    //    multiselect.value(Allergendata);
                                    //    //if (array.length == 3) {
                                    //    //    multiselect.readonly(true);
                                    //    //}
                                    //    //else {
                                    //    //    multiselect.readonly(false);
                                    //    //}
                                    //}
                                    //dialog.title("MOG Details - " + MOGName);
                                    return true;
                                }
                            }

                        ],
                    },


                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetMOGDataList, function (result) {

                                if (result != null) {
                                    nationalmogdata = result;
                                    result = result.filter(m => m.IsActive);

                                    if (isShowElmentoryMOG) {
                                        result = result.filter(m => m.IsElementoryMOG);
                                    }

                                    $(".checkboxsec").each(function () {
                                        if ($(this).is(":checked")) {

                                            var thisid = $(this).attr("id");
                                            result = result.filter(a => a.Sectorids.toLowerCase().trim().includes(thisid.toLowerCase().trim()));
                                        }
                                    });

                                    Utility.UnLoading();
                                    options.success(result);
                                    console.log(result)
                                }
                                else {
                                    Utility.UnLoading();
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , false);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                MOGCode: { type: "string" },
                                Name: { type: "string" },
                                UOMName: { type: "string" },
                                Alias: { type: "string" },
                                Status: { type: "string" },
                                PublishedDate: { type: "date" },
                                CreatedOn: { type: "date" },
                                ModifiedOn: { type: "date" },
                                Sectorids: { type: "string" },
                                IsElementoryString: { type: "string", editable: false },
                                Process: { type: "string" },
                            }
                        }
                    },
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {

                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if (view[i].Status == 1) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#ffe1e1");

                        }
                        else if (view[i].Status == 2) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#ffffbf");
                        }
                        else {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#d9ffb3");
                        }

                    }
                    var items = e.sender.items();
                    var grid = this;

                    grid.tbody.find("tr[role='row']").each(function () {
                        var model = grid.dataItem(this);
                        //Krish 20-10-2022
                       
                        //var process = "";
                        //var getMog = processmogmappingdata.filter(a => a.MOGCode == model.MOGCode);
                        
                       
                        //if (getMog != undefined) {
                        //    for (var i = 0; i < getMog.length; i++) {
                        //        var getProcName = processTypeData.filter(a => a.value == getMog[i].ProcessTypeCode)[0];
                                
                        //        process = process + getProcName.text + ", ";
                        //    }
                        //    if (process.indexOf(',') >= 0)
                        //        process = process.substring(0, process.length - 2);

                        //    $(this).find(".processdiv").html(process);
                        //}

                        if (!model.IsElementoryMOG) {
                            $(this).find(".k-grid-MapNutrients").addClass("k-state-disabled");
                        }

                        var reFormatSector = model.Sectorids.split(',');
                        //console.log(reFormatSector)
                        if (reFormatSector.length > 0) {
                            //for (var i = 0; i < reFormatSector.length; i++) {
                            //    var NewSectorids = document.createElement("div");
                            //    NewSectorids.className = "sectordivList";

                            //    var reAssign = reFormatSector[i];
                            //    var chk = reFormatSector[i].indexOf("FS - ");
                            //    if (chk >= 0)
                            //        reAssign = reAssign.replace("FS - ", "");
                            //    NewSectorids.innerHTML = reAssign.trim().charAt(0);
                            //    if (reAssign.trim().charAt(0) == "C")
                            //        NewSectorids.classList.add("coresector");
                            //    else if (reAssign.trim().charAt(0) == "M")
                            //        NewSectorids.classList.add("manusector");
                            //    else if (reAssign.trim().charAt(0) == "G")
                            //        NewSectorids.classList.add("googlesector");
                            //    else if (reAssign.trim().charAt(0) == "H")
                            //        NewSectorids.classList.add("healthsector");
                            //    else if (reAssign.trim().charAt(0) == "E")
                            //        NewSectorids.classList.add("edusector");
                            //    $(this).find(".sectordiv").append(NewSectorids);
                            //}

                            for (var i = 0; i < reFormatSector.length; i++) {
                                var NewSectorids = document.createElement("div");
                                NewSectorids.className = "sectordivList";

                                var reAssign = reFormatSector[i];
                                var chk = reFormatSector[i].indexOf("FS - ");
                                if (chk >= 0)
                                    reAssign = reAssign.replace("FS - ", "");

                                NewSectorids.innerHTML = reAssign.trim().charAt(0);
                                //Krish 18-07-2022
                                if (reAssign.trim().charAt(0) == "C")
                                    NewSectorids.classList.add("coresector");
                                else if (reAssign.trim().charAt(0) == "H")
                                    NewSectorids.classList.add("healthsector");
                                else if (reAssign.trim().charAt(0) == "E")
                                    NewSectorids.classList.add("edusector");
                                else if (reAssign.trim().charAt(0) == "G")
                                    NewSectorids.classList.add("googlesector");
                                else if (reAssign.trim().charAt(0) == "M")
                                    NewSectorids.classList.add("manusector");

                                $(this).find(".sectordiv").append(NewSectorids);
                            }
                            var secDiv = $(this).find(".sectordiv");
                            var findCore = $(secDiv).find(".coresector");
                            var findHealth = $(secDiv).find(".healthsector");
                            var findEdu = $(secDiv).find(".edusector");
                            var findGog = $(secDiv).find(".googlesector");
                            var findManu = $(secDiv).find(".manusector");

                            var sortedSecDiv = document.createElement("div");
                            if (findCore != undefined)
                                $(sortedSecDiv).append($(findCore));
                            if (findHealth != undefined)
                                $(sortedSecDiv).append($(findHealth));
                            if (findEdu != undefined)
                                $(sortedSecDiv).append($(findEdu));
                            if (findGog != undefined)
                                $(sortedSecDiv).append($(findGog));
                            if (findManu != undefined)
                                $(sortedSecDiv).append($(findManu));
                            $(secDiv).html($(sortedSecDiv).html());

                        }
                        else
                            $(this).find(".sectordiv").html(model.Sectorids);
                    });

                    $(".chkbox").on("change", function (e) {
                        // 
                        isStatusSave = true;
                        isMOGAPLMappingSave = false;
                        $("#success").css("display", "none");
                        $("#error").css("display", "none");
                        var gridObj = $("#gridMOG").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        datamodel = tr;
                        MOGName = tr.Name;
                        MOGCode = tr.MOGCode;
                        datamodel.IsNationalMOGData = true;
                        MOGID = tr.ID;
                        datamodel.IsActive = $(this)[0].checked;
                        mogStatus = datamodel.IsActive;
                        var active = "";
                        if (datamodel.IsActive) {
                            active = "Active";
                        } else {
                            active = "Inactive";
                        }
                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> MOG " + active + "?", "MOG Update Confirmation", "Yes", "No", function () {
                            getMOGDependenciesData();
                            if (mogImpactedData.IsMOGImpacted) {
                                $(".mogDepStatus").text(datamodel.IsActive ? "active" : "inactive");
                                showMOGDependencies();
                            }
                            else {
                                SaveMOGStatus();
                            }
                        }, function () {
                            $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsActive);
                            // $("#chkbox-" + datamodel.ID + "").checked = !datamodel.IsActive;
                        });
                        return true;
                    });

                    $(".chkboxElementory").on("change", function (e) {
                        // 
                        isStatusSave = true;
                        isMOGAPLMappingSave = false;
                        $("#success").css("display", "none");
                        $("#error").css("display", "none");
                        var gridObj = $("#gridMOG").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        datamodel = tr;
                        MOGName = tr.Name;
                        MOGCode = tr.MOGCode;
                        MOGID = tr.ID;
                        datamodel.IsElementoryMOG = $(this)[0].checked;
                        mogStatus = datamodel.IsElementoryMOG;
                        datamodel.IsNationalMOGData = true;
                        var active = "";
                        if (datamodel.IsElementoryMOG) {
                            active = "Elementary";
                        } else {
                            active = "Non Elementary";
                        }
                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> MOG " + active + "?", "MOG Update Confirmation", "Yes", "No", function () {
                            SaveMOGElementoryStatus();
                        }, function () {
                            $("#chkboxElem-" + datamodel.ID + "").prop('checked', !datamodel.IsElementoryMOG);
                        });
                        return true;
                    });
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "MOGCode" || col == "Name" || col == "UOMName" || col == "Alias" || col == "Sectorids") {
                            return col;
                        }
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
        
    }, 3000);
}
function populateRangeTypeDropdown() {

    $("#rtRangeType").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: rangeTypeData,
        index: 0,
    });
}
function populateProcessDropdown() {

    $("#inputmultiProcess").kendoMultiSelect({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: processTypeData,
        index: -1,
        autoClose: false,
        change: onmultiProcessChange
    });
}
function onmultiProcessChange(key) {
    var multi = $("#inputmultiProcess").getKendoMultiSelect();
    var multiDataItems = multi.dataItems();
    var pvalues = multi.dataItems().map(function (v) {
        return v.text;
    });
    for (var i = 0; i < pvalues.length; i++) {
        if (pvalues[i] == "Thawing") {
            $("#divThwaing").show();
            if (key != "edit") {
                //var dropdownlist = $("#rtRangeType").data("kendoDropDownList");
                //dropdownlist.select(0);
            }
            break;
        }
        else {
            $("#divThwaing").hide();
        }
    }
    for (var i = 0; i < pvalues.length; i++) {
        if (pvalues[i] == "Disinfection") {
            $("#divDisinfection").show();
            if (key != "edit") {
                //$("#inputStdDuration").val("");
            }
            break;
        }
        else {
            $("#divDisinfection").hide();
        }
    }
    if (pvalues.length == 0) {
        $("#divThwaing").hide();
        $("#divDisinfection").hide();
    }
    //for (var i = 0; i < multiDataItems.length; i += 1) {
    //    var currentItem = multiDataItems[i].text;
    //    if (currentItem == "Thawing") {
    //        $("#divThwaing").show();
    //        var dropdownlist = $("#rtRangeType").data("kendoDropDownList");
    //        dropdownlist.select(0);
    //    }
    //    //else {
    //    //    $("#divThwaing").hide();            
    //    //}
    //    if (currentItem == "Disinfection") {
    //        $("#divDisinfection").show();
    //        $("#inputStdDuration").val("");
    //    }
    //    //else {
    //    //    $("#divDisinfection").hide();
    //    //}
    //}

}
//Krish 18-10-2022
function getProcessAndRangeTypeData() {
    HttpClient.MakeRequest(CookBookMasters.GetRangeTypeDataList, function (result) {
        rangeTypeData = [];
        rangeTypeData.push({ "value": "Select", "text": "Select" });
        //rangeTypeData = result;

        var dataSource = result.filter(a => a.IsActive == true);

        for (var i = 0; i < dataSource.length; i++) {
            rangeTypeData.push({ "value": dataSource[i].Code, "text": dataSource[i].Name + " (" + dataSource[i].StdTempFrom + " - " + dataSource[i].StdTempTo + ")" });
        }

        populateRangeTypeDropdown();
    });
    //HttpClient.MakeRequest(CookBookMasters.GetProcessDataList, function (result) {
    //    processTypeData = [];
    //    //processTypeData.push({ "value": "Select", "text": "Select" });
    //    //rangeTypeData = result;
    //    //console.log(result)

    //    var dataSource = result.filter(a => a.IsActive == true);

    //    for (var i = 0; i < dataSource.length; i++) {
    //        processTypeData.push({ "value": dataSource[i].Code, "text": dataSource[i].Name });
    //    }
    //    //console.log(processTypeData)
    //    populateProcessDropdown();
    //});
    if (processTypeData.length > 0) { populateProcessDropdown(); }
}
//Krish 07-10-2022
$("#rtbtnSubmit").on("click", function () {
    var multi = $("#inputmultiProcess").getKendoMultiSelect();
    var multiDataItems = multi.dataItems();
    var selectedProcessTypes = [];
    if (multiDataItems.length > 0) {
        for (var i = 0; i < multiDataItems.length; i += 1) {
            var currentItem = multiDataItems[i].text;
            selectedProcessTypes.push(multiDataItems[i].value);

            if (currentItem == "Thawing") {
                var rangetype = $("#rtRangeType").data("kendoDropDownList").value();
                if (rangetype == "Select") {
                    toastr.error("Please select Range Type");
                    return;
                }
            }
            if (currentItem == "Disinfection") {
                $("#divDisinfection").show();

                if ($("#inputStdDuration").val().length == 0) {
                    toastr.error("Please enter Std Duration");
                    return;
                }
            }
        }

    }
    else {
        toastr.error("Please select Process Type");
        return;
    }
    $("#rtbtnSubmit").attr('disabled', 'disabled');
    if ($("#inputStdDuration").val().length == 0)
        $("#inputStdDuration").val("0");
    HttpClient.MakeSyncRequest(CookBookMasters.SaveProcessTypeMogData, function (result) {
        if (result == false) {
            $('#rtbtnSubmit').removeAttr("disabled");
            //$("#error").css("display", "flex");
            toastr.error("Some error occured, please try again");
            // $("#error").find("p").text("Some error occured, please try again");
            $("#sitealias").focus();
        }
        else {
            $(".k-overlay").hide();
            //$("#error").css("display", "flex");
            var orderWindow = $("#RangeTypeMOGwindowEdit").data("kendoWindow");
            orderWindow.close();
            $('#rtbtnSubmit').removeAttr("disabled");
            //$("#success").css("display", "flex");
            //if (model.ID > 0) {
            toastr.success("Updated successfully");
            //}
            //else {
            //    toastr.success("New Range Type added successfully.");/
            //}

            // $("#success").find("p").text("Color configuration updated successfully");
            //populateDishGrid();
            $("#gridMOG").data("kendoGrid").dataSource.data([]);
            $("#gridMOG").data("kendoGrid").dataSource.read();
            //$("#gridMOG").data("kendoGrid").dataSource.sort({ field: "Name", dir: "asc" });
        }
        Utility.UnLoading();
    }, {
        processTypes: selectedProcessTypes,
        mogCode: $("#rtMOGCode").text(),
        rangeTypeCode: rangetype,
        inputStdDuration: $("#inputStdDuration").val()

    }, true);

});
$("#rtbtnCancel").on("click", function () {
    var orderWindow = $("#RangeTypeMOGwindowEdit").data("kendoWindow");
    orderWindow.close();
    $("#rtRangeType").data('kendoDropDownList').value("Select");
    var multiselect = $("#inputmultiProcess").data("kendoMultiSelect");
    multiselect.dataSource.filter({});
    multiselect.value([""]);
});
//Krish
//13-07-2022
function doDataBoundWorks(result, options) {
    result = result.filter(m => m.IsActive);
    if (elementaryCode == 'Elementory') {
        result = result.filter(m => m.IsElementoryMOG == 1);
    }
    else if (elementaryCode == 'NonElementory') {
        result = result.filter(m => m.IsElementoryMOG == 0);
    }

    //Krish
    //13-07-2022
    //var multiselectsite = $("#msColorFilter").data("kendoMultiSelect");

    //if (multiselectsite != undefined) {
    //    var selectedFilter = multiselectsite.value();

    //    if (selectedFilter.length == 0) {

    //    }
    //    else {
    //        var filteredData = [];
    //        var newResult = [];
    //        result.filter(a => filteredData.push(a));
    //        for (var i = 0; i < selectedFilter.length; i++) {

    //            if (selectedFilter[i] == "ASS-00012") {
    //                result = filteredData.filter(a => a.AllergenName != "None" && a.NGAllergens != "None");

    //            }
    //            else if (selectedFilter[i] == "ASS-00013") {
    //                result = filteredData.filter(a => a.AllergenName == "None" && a.NGAllergens != "None");

    //            }
    //            else if (selectedFilter[i] == "ASS-00014") {
    //                result = filteredData.filter(a => a.AllergenName != "None" && a.NGAllergens == "None")

    //            }
    //            else if (selectedFilter[i] == "ASS-00015") {
    //                result = filteredData.filter(a => a.AllergenName == "None" && a.NGAllergens == "None")

    //            }
    //            result.filter(a => newResult.push(a));
    //        }
    //        result = [];
    //        newResult.filter(a => result.push(a));
    //        //return newResult;
    //    }
    //}
    //Krish 18-07-2022
    //if ($("#chkColorAll1").is(":checked") || $("#chkColorAll2").is(":checked") || $("#chkColorAll3").is(":checked") || $("#chkColorAll4").is(":checked")) {
    //    var filteredData = [];
    //    var newResult = [];
    //    result.filter(a => filteredData.push(a));
    //    if ($("#chkColorAll1").is(":checked")) {
    //        //13
    //        var data = filteredData.filter(a => a.AllergenName == "None" && a.NGAllergens != "None");
    //        data.filter(a => newResult.push(a));
    //    }
    //    if ($("#chkColorAll2").is(":checked")) {
    //        //14
    //        var data = filteredData.filter(a => a.AllergenName != "None" && a.NGAllergens == "None");
    //        data.filter(a => newResult.push(a));
    //    }
    //    if ($("#chkColorAll3").is(":checked")) {
    //        //12
    //        var data = filteredData.filter(a => a.AllergenName != "None" && a.NGAllergens != "None");
    //        data.filter(a => newResult.push(a));
    //    }
    //    if ($("#chkColorAll4").is(":checked")) {
    //        //15
    //        var data = filteredData.filter(a => a.AllergenName == "None" && a.NGAllergens == "None");
    //        data.filter(a => newResult.push(a));
    //    }
    //    result = [];
    //    newResult.filter(a => result.push(a));
    //}

    var code = $("#selColor").attr("data-colorcode");

    if (code != undefined && code.length > 0) {
        var filteredData = [];
        var newResult = [];
        result.filter(a => filteredData.push(a));
        if (code == "ASS-00012") {
            var data = filteredData.filter(a => a.AllergenName != "None" && a.NGAllergens != "None");
            data.filter(a => newResult.push(a));
        }
        else if (code == "ASS-00013") {
            var data = filteredData.filter(a => a.AllergenName == "None" && a.NGAllergens != "None");
            data.filter(a => newResult.push(a));
        }
        else if (code == "ASS-00014") {
            var data = filteredData.filter(a => a.AllergenName != "None" && a.NGAllergens == "None")
            data.filter(a => newResult.push(a));
        }
        else if (code == "ASS-00015") {
            var data = filteredData.filter(a => a.AllergenName == "None" && a.NGAllergens == "None")
            data.filter(a => newResult.push(a));
        }

        //result.filter(a => newResult.push(a));
        result = [];
        newResult.filter(a => result.push(a));
    }
    $(".checkboxsec").each(function () {
        if ($(this).is(":checked")) {

            var thisid = $(this).attr("id");
            result = result.filter(a => a.Sectorids.toLowerCase().trim().includes(thisid.toLowerCase().trim()));
        }
    });
    //var multiselectsec = $("#msSectorFilter").data("kendoMultiSelect");

    //if (multiselectsec != undefined) {
    //    var selectedSectorFilter = multiselectsec.value();
    //    if (selectedSectorFilter.length > 0) {
    //        //console.log(result)
    //        for (var i = 0; i < selectedSectorFilter.length; i++) {
    //            //console.log(selectedSectorFilter[i])
    //            result = result.filter(a => a.Sectorids.toLowerCase().trim().includes(selectedSectorFilter[i].toLowerCase().trim()))

    //        }

    //    }
    //}
    options.success(result);
}

function elementoryMOG(e) {
    isShowElmentoryMOG = e.checked;
    populateMOGGrid();
}

function changeNationalMOGURL() {

}

function populateNutrientMasterGrid() {
    Utility.Loading();


    var gridVariable = $("#gridMOGWiseNutrients");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "APLMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: true,
        groupable: false,
        //reorderable: true,
        //scrollable: true,
        columns: [
            {
                field: "ElementCode", title: "Nutrient Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ElementName", title: "Nutrient", width: "150px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "TypeName", title: "Nutrient Type", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "Nutrient UOM", width: "40px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },

            {
                field: "Quantity", title: "Nutrient Quantity", width: "60px",
                //  template: '<input type="number" class="inputbrntrqty"  value="Quantity" name="ElementQuantity" oninput="savePriceChange(this,true)"/>',
                template: '#if (IsElementoryMOG) {# <input type="number" class="inputbrntrqty"  value="Quantity" name="ElementQuantity" oninput="savePriceChange(this,true)"/> #} else {# <span style="text-align: center;">-<span> #}#',
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center;"
                },
            }
        ],
        dataSource: {
            data: mogWisenutrientMasterdataSource.sort((a, b) => a.DisplayOrder - b.DisplayOrder),
            schema: {
                model: {
                    fields: {
                        ElementCode: { type: "string" },
                        ElementName: { type: "string" },
                        UOMName: { type: "string" },
                        TypeName: { type: "string" },
                        Quantity: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
        height: 495,
        minHeight: 200,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();
            items.each(function (e) {
                var dataItem = grid.dataItem(this);
                $(this).find('.inputbrntrqty').val(dataItem.Quantity);
            });
        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })

    //bind click event to the checkbox
    var grid = gridVariable.data("kendoGrid")
    grid.table.on("click", ".checkbox", selectRow);
    //$(".k-label")[0].innerHTML.replace("items", "records");
}

//function getMOGWiseAPLMasterData() {
//    mogWiseNutrientArray = [];
//    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG, function (result) {
//        Utility.UnLoading();

//        if (result != null) {
//            mogWisenutrientMasterdataSource = result;
//            mogWiseNutrientArray = mogWisenutrientMasterdataSource.map(
//                item => {
//                    return item.ArticleID;
//                });
//        }
//        else {
//        }
//    }, { mogCode: MOGCode }
//         , true);
//}

function populateNutrientMasterGridPreview() {
    var previewDataSource = [];
    Utility.Loading();
    if (isShowPreview) {

        previewDataSource = mogWisenutrientMasterdataSource.filter(function (item) {
            if (CheckedTrueAPLCodes.indexOf(item.ArticleID) != -1) {
                return item;
            }
        });
    }
    else {
        previewDataSource = mogWisenutrientMasterdataSource.filter(function (item) {
            if ((checkedNutrientCodes.indexOf("1_" + item.ArticleID) != -1) || (checkedNutrientCodes.indexOf("0_" + item.ArticleID) != -1)) {
                return item;
            }
        });
    }

    var gridVariable = $("#gridMOGWiseNutrients");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "APLMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: true,
        groupable: false,
        //reorderable: true,
        //scrollable: true, 
        columns: [
            {
                field: "ArticleNumber", title: "Nutrient Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ArticleDescription", title: "Nutrient Element", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOM", title: "Nutrient Type", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ArticleType", title: "Nutrient UOM", width: "40px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "Hierlevel3", title: "Nutrient Quantity", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },

        ],
        dataSource: {
            data: previewDataSource,
            schema: {
                model: {
                    id: "ArticleID",
                    fields: {
                        ArticleNumber: { type: "string" },
                        ArticleDescription: { type: "string" },
                        UOM: { type: "string" },
                        ArticleType: { type: "string" },
                        Hierlevel3: { type: "string" },
                        MerchandizeCategoryDesc: { type: "string" },
                        MOGName: { type: "string" }
                    }
                }
            },
            pageSize: 100,
        },
        height: 420,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {

            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (checkedIds[view[i].id]) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");
                }
                if (checkedIds[view[i].id] == false) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .removeAttr("checked");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffe1e1");
                    continue;
                }
                else {
                    if (isShowPreview) {
                        if ((checkedNutrientCodes.indexOf("1_" + view[i].ArticleID) != -1) || (checkedNutrientCodes.indexOf("0_" + view[i].ArticleID) != -1)) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#dcf5ff");
                        }
                    }
                    else {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#dcf5ff");
                    }
                    if (view[i].MOGName != "") {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".checkbox")
                            .attr("checked", "checked");

                    }
                }
            }
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        }
    });
    var grid = gridVariable.data("kendoGrid")
    grid.table.on("click", ".checkbox", selectRow);
}

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        td = $(this).closest("td"),
        row = $(this).closest("tr"),
        grid = $("#gridMOGWiseNutrients").data("kendoGrid"),
        dataItem = grid.dataItem(row);
    checkedIds[dataItem.id] = checked;
    if (checked) {
        var filteredAry = checkedNutrientCodes.filter(function (e) { return e !== "0_" + dataItem.ArticleID });
        checkedNutrientCodes = filteredAry;
        checkedNutrientCodes.push("1_" + dataItem.ArticleID);
        CheckedTrueAPLCodes.push(view[i].ArticleID);
    }
    else {
        var filteredAry = checkedNutrientCodes.filter(function (e) { return e !== "1_" + dataItem.ArticleID });
        checkedNutrientCodes = filteredAry;
        checkedNutrientCodes.push("0_" + dataItem.ArticleID);
        //CheckedTrueAPLCodes.pop(dataItem.ArticleID);
        CheckedTrueAPLCodes = CheckedTrueAPLCodes.filter(m => m != dataItem.ArticleID);
    }


}

function preview(e) {
    isShowPreview = false;
    if (e.checked)
        populateNutrientMasterGridPreview();
    else
        populateNutrientMasterGrid();

    Utility.UnLoading();
}

function showChecked(e) {
    isShowPreview = true;
    if (e.checked)
        populateNutrientMasterGridPreview();
    else
        populateNutrientMasterGrid();
    Utility.UnLoading();
}

function showMOGImpacts(e) {
    var gridObj = $("#gridMOG").data("kendoGrid");
    var tr = gridObj.dataItem($(e).closest("tr")); // 'e' is the HTML Nutrient <a>
    datamodel = tr;
    MOGName = datamodel.Name;
    MOGCode = datamodel.MOGCode;
    MOGID = datamodel.ID;
    getMOGDependenciesData();
    if (mogImpactedData.IsMOGImpacted) {
        showMOGDependencies();
        $("#mogMsg").css('display', 'none');
        $("#btnAgree").css('display', 'none');
        $("#btnDoLater").text("Ok");
        $("#btnDoLater").addClass("btn-info");
        $("#btnDoLater").removeClass("btn-tool");
    }
    else {
        Utility.Page_Alert_Save_Info("No Impact Found!");
        $("#confirmSave_No").css('display', 'none');
        $("#confirmSave_Ok").addClass('colored-btn');
        $("#confirmSave_Ok").removeClass('white-btn');
        Utility.UnLoading();

    }
}

function getMOGDependenciesData() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetMOGImpactedDataList, function (data) {
        mogImpactedData = data;
    }, { mogID: MOGID }, true);
}

function showMOGDependencies() {
    $(".oldmogname").text(MOGName);
    $(".oldmogcode").text(MOGCode);
    $(".newmogname").text(NewMOGName);
    $("#mogMsg").css('display', 'block');
    var dialog = $("#windowEditMOGDependencies").data("kendoWindow");
    dialog.title("MOG Dependencies");
    if (isStatusSave) {
        $("#mogNameChangeMsgDiv").css('display', 'none');
        $("#mogInactiveMsgDiv").css('display', 'block');
        $("#mogAPLMappingMsgDiv").css('display', 'none');
        $("#btnAgree").css('display', 'none');
        $("#btnDoLater").text("Ok");
        $("#btnDoLater").addClass("btn-info");
        $("#btnDoLater").removeClass("btn-tool");

    }
    else if (isMOGAPLMappingSave) {
        $("#mogNameChangeMsgDiv").css('display', 'none');
        $("#mogInactiveMsgDiv").css('display', 'none');
        $("#mogAPLMappingMsgDiv").css('display', 'block');
        $("#btnAgree").css('display', 'none');
        $("#btnDoLater").text("Ok");
        $("#btnDoLater").addClass("btn-info");
        $("#btnDoLater").removeClass("btn-tool");
        $("#windowEditMOGWiseNutrient").parent().hide();
        dialog.title("MOG-APL Dependencies");
    }
    else {
        $("#mogNameChangeMsgDiv").css('display', 'block');
        $("#mogInactiveMsgDiv").css('display', 'none');
        $("#mogAPLMappingMsgDiv").css('display', 'none');
        $("#btnAgree").css('display', 'block');
        $("#btnDoLater").text("I understand the impact, Proceed");
        $("#btnDoLater").removeClass("btn-info");
        $("#btnDoLater").addClass("btn-tool");
    }

    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    $("#windowEditMOGChangeConfirm").parent().hide();
    populateMOGDependencies();
}

function showMOGAPLDependencies() {
    var dialog = $("#windowEditMOGAPLDependencies").data("kendoWindow");
    dialog.title("MOG APL Dependencies");

    $("#mogNameChangeMsgDiv").css('display', 'none');
    $("#mogInactiveMsgDiv").css('display', 'none');
    $("#mogAPLMappingMsgDiv").css('display', 'block');
    $("#btnAgree").css('display', 'none');
    $("#btnDoLater").text("Ok");
    $("#btnDoLater").addClass("btn-info");
    $("#btnDoLater").removeClass("btn-tool");

    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    $("#windowEditMOGChangeConfirm").parent().hide();
    populateMOGAPLDependencies();
}





function addNutrient() {

    for (dataItem of nutrientMasterdataSource) {
        var nutmodel = {
            "TypeCode": dataItem.NutritionElementTypeCode,
            "TypeName": dataItem.NutritionElementTypeName,
            "ElementCode": dataItem.NutritionElementCode,
            "ElementName": dataItem.Name,
            "Quantity": 0,
            "UOMCode": dataItem.NutritionElementUOMCode,
            "UOMName": dataItem.NutritionElementUOMName,
            "DisplayOrder": dataItem.DisplayOrder,
            "IsDisplay": dataItem.IsDisplay
        }
        mogWisenutrientMasterdataSource.unshift(nutmodel);
        checkedNutrientCodes.push("1_" + dataItem.NutritionElementCode);
        var grid = $("#gridMOGWiseNutrients").data("kendoGrid");
        grid.dataSource.insert(0, nutmodel);
    }
}

function filterMultiSelectDropdown() {
    var grid = $("#gridMOGWiseNutrients").data("kendoGrid");
    var multiselect = $("#inputNutrient").data("kendoMultiSelect");

    var modifiedDataSource = nutrientMasterdataSource.filter(function (array_el) {
        return grid.dataSource._data.filter(function (anotherOne_el) {
            return anotherOne_el.ElementCode == array_el.NutritionElementCode;
        }).length == 0
    });

    multiselect.value([]);
    multiselect.setDataSource(modifiedDataSource);
}



function checkMOGNutrientChange() {
    var isChanged = true;

    return isChanged;
}

function saveAll() {
    if (checkMOGNutrientChange()) {
        $("#confirmSave_No").css('display', 'block');
        $("#confirmSave_Ok").addClass('white-btn');
        $("#confirmSave_Ok").addClass('white-btn');
        $("#confirmSave_Ok").removeClass('colored-btn');
        $("#confirmSave_Box").css('z-index', '99999');
        Utility.UnLoading();
        Utility.Page_Alert_Save("Proceeding will perform change MOG Nutrients mapping. Are you sure to proceed?", "MOG-Nutrients Mapping Change Confirmation", "Yes", "No",
            function () {
                saveMOGNutrientMapping();
            },
            function () {

            }
        );
    }

    Utility.UnLoading();
}

var lastValid = 0;
var validNumber = new RegExp(/^\d*\.?\d*$/);
function validateNumber(elem) {

    if (validNumber.test(elem.value)) {
        lastValid = elem.value;
    } else {
        elem.value = lastValid;
    }
}

function savePriceChange(e, isSavePrice) {
    validateNumber(e);
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridMOGWiseNutrients").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var ElementCode = dataItem.ElementCode;
    if (dataItem.Quantity == $(e).val()) {
        return;
    }
    dataItem.Quantity = $(e).val();
    dataItem.set('Quantity', $(e).val());
    //mogWisenutrientMasterdataSource = mogWisenutrientMasterdataSource.forEach(function (item, index) {
    //    if (item.ElementCode == ElementCode) {
    //        item.Quantity = $(e).val();
    //    };
    //});
}

function saveMOGNutrientMapping() {
    var mogQuantity = $("#mogquantity").text();
    if (mogQuantity == null || mogQuantity == "" || mogQuantity == undefined || mogquantity == 0) {
        toastr.error("Please enter MOG Quantity for each Nutrients First.");
        Utility.UnLoading();
        return;
    }
    var grid = $("#gridMOGWiseNutrients").data("kendoGrid");
    var nutritionData = JSON.stringify(grid._data);

    var model;
    if (datamodel != null) {
        model = datamodel;
        model.MOGCode = MOGCode;
        model.NutrientMOGQuantity = mogQuantity;
        model.NutritionData = nutritionData;
        model.CreatedOn = kendo.parseDate(model.CreatedOn);
        model.IsMOGNutrientMappingData = true;
    }

    //if (filterData == null || filterData.length == 0) {
    //    toastr.error("Please update or map Nutrients data First.");
    //    return;
    //}
    //else {

    $("#NatiMogbtnSubmit").attr('disabled', 'disabled');
    if (!sanitizeAndSend(model)) {
        return;
    }
    HttpClient.MakeSyncRequest(CookBookMasters.SaveMOGNutrientMasterData, function (result) {
        if (result == false) {
            $('#NatiMogbtnSubmit').removeAttr("disabled");
            toastr.error("Some error occured, please try again");
            $("#inputmog").focus();
            Utility.UnLoading();
        }
        else {
            $(".k-overlay").hide();
            var orderWindow = $("#windowEditMOGWiseNutrient").data("kendoWindow");
            orderWindow.close();
            $('#NatiMogbtnSubmit').removeAttr("disabled");
            toastr.success("MOG Nutrient mapping saved successfully.");
            Utility.UnLoading();
            //Krish
            //13-07-2022
            populateMOGGrid("refresh");
        }
    }, {
        model: datamodel

    }, true);
    // }
}

function populateMOGDependencies() {

    $("#totalImapactedBaseRecipes").text(mogImpactedData.TotalImapactedBaseRecipes);
    $("#totalImapactedFinalRecipes").text(mogImpactedData.TotalImapactedFinalRecipes);
    $("#totalImapactedDishes").text(mogImpactedData.TotalImapactedDishes);
    $("#totalImapactedItems").text(mogImpactedData.TotalImapactedItems);
    $("#totalImapactedSites").text(mogImpactedData.TotalImapactedSites);
    populateBaseRecipesGrid();
    populateFinalRecipesGrid();
    populateDishGrid();
    populateItemGrid();
    populateSiteGrid();
    Utility.UnLoading();
}

function populateBaseRecipesGrid() {
    Utility.Loading();
    var gridVariable = $("#baseRecipeGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        noRecords: true,
        columns: [
            {
                field: "BaseRecipeCode", title: "Base Recipe Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "BaseRecipeName", title: "Base Recipe Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedFinalRecipeName", title: "Impacted Final Recipes (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogImpactedData.MOGImpactedBaseRecipesData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        BaseRecipeCode: { type: "string" },
                        BaseRecipeName: { type: "string" },
                        ImpactedFinalRecipeName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}



function populateFinalRecipesGrid() {
    Utility.Loading();
    var gridVariable = $("#finalRecipeGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        noRecords: true,
        columns: [
            {
                field: "FinalRecipeCode", title: "Final Recipe Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "FinalRecipeName", title: "Final Recipe Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedDishName", title: "Impacted Dishes (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogImpactedData.MOGImpactedFinalRecipesData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        BaseFinalCode: { type: "string" },
                        BaseFinalName: { type: "string" },
                        ImpactedDishCode: { type: "string" },
                        ImpactedDishName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateDishGrid() {
    Utility.Loading();
    var gridVariable = $("#dishGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        noRecords: true,
        columns: [
            {
                field: "DishCode", title: "Dish Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishName", title: "Dish Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishType", title: "Dish Type", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedItemName", title: "Impacted Item (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogImpactedData.MOGImpactedDishesData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        DishCode: { type: "string" },
                        DishName: { type: "string" },
                        DishType: { type: "string" },
                        ImpactedItemCode: { type: "string" },
                        ImpactedItemName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateItemGrid() {
    Utility.Loading();
    var gridVariable = $("#itemGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        noRecords: true,
        scrollable: true,
        columns: [
            {
                field: "ItemCode", title: "Item Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ItemName", title: "Item Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedSiteName", title: "Impacted Site (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogImpactedData.MOGImpactedItemsData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        ItemCode: { type: "string" },
                        ItemName: { type: "string" },
                        ImpactedSiteCode: { type: "string" },
                        ImpactedSiteName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateSiteGrid() {
    Utility.Loading();
    var gridVariable = $("#siteGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        noRecords: true,
        columns: [
            {
                field: "ArticleNumber", title: "Article Number", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ArticleDescription", title: "Article Description", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOM", title: "UOM", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ArticleType", title: "Article Type", width: "40px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogWisenutrientMasterdataSource,
            schema: {
                model: {
                    id: "ArticleID",
                    fields: {
                        ArticleNumber: { type: "string" },
                        ArticleDescription: { type: "string" },
                        UOM: { type: "string" },
                        ArticleType: { type: "string" },
                        Hierlevel3: { type: "string" },
                        MerchandizeCategoryDesc: { type: "string" },
                        MOGName: { type: "string" }
                    }
                }
            },
            pageSize: 100,
        },
    })
}
//Krish 18-07-2022
function sectorFilter() {

    populateMOGGrid();
}

$("document").ready(function () {
    // showMOGAPLDependencies()
    $(".k-window").hide();
    $(".k-overlay").hide();
    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();

    });
});
