﻿var user = [];
var secname = "";
var userID = 0;
var userroledata = [];
var userrolename = "";

$(window).load(function () {
    Utility.UnLoading();
});

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});


var sectordata = [];
var moduledata = [];
var secname = "";

$(function () {
    $('#confirmSave_Box').on('shown.bs.modal', function (e) {
        $("#confirmSave_Ok").focus();
    });

    $('#confirmbox').on('shown.bs.modal', function (e) {
        $("#confirmOk").focus();
    });

    var current = location.pathname;
    $('a.nav-link').each(function () {
        var $this = $(this);
        // if the current path is like this link, make it active
        if ($this.attr('href').indexOf(current) !== -1) {
            $this.addClass('active');
        }
    })
});

$(document).ready(function () {

    $(document).keyup(function (e) {

        if (e.keyCode === 27) {
            $(".k-window").hide();
            $(".k-overlay").hide();
        }// $('.cancel').click();   // esc
    });

    $(document).keydown("keypress", function (e) {
        if (e.keyCode == kendo.keys.ESC) {
            $('#confirmSave_Box').modal('hide');
            $("#confirmbox").modal('hide');
            $("#confirmbox_optional").modal('hide');
            $("#confirmbox_warning").modal('hide');
            $('#confirmSave_Box1').modal('hide');
        }
    });

    var pageLoadTime = moment();
    setInterval(refreshAuthenticationCookies, 1000);

    function refreshAuthenticationCookies() {
        if (moment().diff(pageLoadTime, "seconds") > 1800) {
            document.getElementById("refreshAuthenticationIframe").contentDocument.location = baseUrl + "Home/ForceSessionRefresh";
            pageLoadTime = moment();
        }
    }

    $('[data-toggle="tooltip"]').tooltip();

    HttpClient.MakeSyncRequest(CookBookLayout.GetLoginUserDetailsUrl, function (result) {
        user = result;

    }, null, false);


    HttpClient.MakeSyncRequest(CookBookLayout.GetSectorDataList, function (data) {
        var dataSource = data;
        sectordata = [];
        for (var i = 0; i < dataSource.length;) {
            sectordata.push({ "value": dataSource[i].ID, "text": dataSource[i].SectorName, "SectorNumber": dataSource[i].SectorNumber });
        }
        secname = user.SectorName
        userID = user.UserId
        populateSectorDataDropdown();
    }, null, false);
    //Krish
    
    HttpClient.MakeSyncRequest(CookBookLayout.GetUserRoleListDataByUser, function (data) {
        var dataSource = data;
        userroledata = [];
        for (var i = 0; i < dataSource.length;) {
            userroledata.push({ "value": dataSource[i].UserRoleId, "text": dataSource[i].UserRoleName});
        }
        userrolename = user.UserRoleName;
        //userID = user.UserId
        //populateSectorDataDropdown();
    }, null, false);

    HttpClient.MakeSyncRequest(CookBookLayout.GetModulesByUserID, function (data) {
        var dataSource = data;
        moduledata = [];
        for (var i = 0; i < dataSource.length;) {
            if (dataSource[i].MenuDisplay == true) {
                moduledata.push({
                    "ModuleCode": dataSource[i].ModuleCode, "MenuDisplayName": dataSource[i].MenuDisplayName, "PageUrl": dataSource[i].PageUrl,
                    "MenuHeading": dataSource[i].MenuHeading, "MenuParent": dataSource[i].MenuParent
                });
            }
        }
        moduledata.sort((a, b) => (a.DisplayOrder < b.DisplayOrder) ? -1 : 1)
        populateSideNavBar();
    }, { "userID": userID }, false);


});

function SetControlState(userID, moduleCode) {
    HttpClient.MakeRequest(CookBookLayout.GetUserPermissionsByModuleCode, function (data) {
        var dataSource = data;
        var permissiondata = [];
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].OperationCode == '02') {
                //var dropdownlist = $(".canbedisabled_ddl").data("kendoDropDownList");
                if (dataSource[i].Allowed == 1) {
                    $(".canbecreated").show();
                    $(".canbehidden").show();
                    $(".canbedisabled").removedAttr("ReadOnly");
                    //dropdownlist.enable(true);
                } else {
                    $(".canbecreated").hide();
                    $(".canbehidden").hide();
                    $(".canbedisabled").attr("ReadOnly", "ReadOnly");
                    //dropdownlist.enable(false);
                }
            }
            if (dataSource[i].OperationCode == '03') {
                //var dropdownlist = $(".canbedisabled_ddl").data("kendoDropDownList");
                if (dataSource[i].Allowed == 1) {
                    $(".canbehidden").show();
                    $(".canbedisabled").removedAttr("ReadOnly");
                    //dropdownlist.enable(true);
                } else {
                    $(".canbehidden").hide();
                    $(".canbedisabled").attr("ReadOnly", "ReadOnly");
                    //dropdownlist.enable(false);
                }
            }
            if (dataSource[i].OperationCode == '04') {
                if (dataSource[i].Allowed == 1) {
                    $(".canbebulkchanged").show();
                } else {
                    $(".canbebulkchanged").hide();
                }
            }
            if (dataSource[i].OperationCode == '05') {
                if (dataSource[i].Allowed == 1) {
                    $(".canbemapped").removeAttr("ReadOnly");
                } else {
                    $(".canbemapped").attr("ReadOnly", "ReadOnly");
                    $(".canbemappedcaption").val("View");
                }
            }
            if (dataSource[i].OperationCode == '06') {
                if (dataSource[i].Allowed == 1) {
                    $(".canbeexported").show();
                } else {
                    $(".canbeexported").hide();
                }
            }
            if (dataSource[i].OperationCode == '07') {
                if (dataSource[i].Allowed == 1) {
                    $(".canbedeactivated").removeAttr("ReadOnly");
                } else {
                    $(".canbedeactivated").attr("ReadOnly", "ReadOnly");
                }
            }
        }

    }, { "userID": userID, "moduleCode": moduleCode }, false);
}

function populateSectorDataDropdown() {
    var nav = $("#sectorlist");
    var lhtml = "";
    if (sectordata.length == 1) {

        lhtml = "<a class='nav-link' data-toggle='dropdown' href='#'>" + secname + "</a >";
    } else {
        lhtml = "<a class='nav-link' title='Click here to switch the Sector' data-toggle='dropdown' href='#'>" + secname + "<span id='location'><span> (Click here to switch the Sector)</a >";
        lhtml = lhtml + "\n" +
            "<div class='dropdown-menu dropdown-menu-lg dropdown-menu-right'>\n" +
            "<span class='dropdown-item dropdown-header'>Change Sector</span>\n" +
            "<div class='dropdown-divider'></div>\n";


        for (sector of sectordata) {
            // var swch = "";
            if (sector.text == secname) {
                continue;
            } else
                if (sector.text != secname) {
                    // swch = "Switch";
                }
            lhtml = lhtml + "\n" +
                "<div class='dropdown-divider'></div>\n" +
                "<a href='#' onclick=ChangeSector('" + sector.value + "') class='dropdown-item'>\n" +
                "<i class='fas mr-2'></i>" + sector.text + "\n" +
                //  "<span class='float-right text-muted text-sm' onclick=ChangeSector('" + sector.value + "')>"+ swch +"</span>\n" +
                "</a>";
        }
        lhtml = lhtml + "\n </div>";
    }
    nav.append(lhtml);
}
function populateRoleDataDropdown() {
    var nav = $("#rolelist");
    var lhtml = "";
    if (userroledata.length == 1) {

        lhtml = "<a class='nav-link' data-toggle='dropdown' href='#'>" + userrolename + "</a >";
    } else {
        lhtml = "<a class='nav-link' title='Click here to switch the Role' data-toggle='dropdown' href='#'>" + userrolename + "<span id='location'><span> (Click here to switch the Role)</a >";
        lhtml = lhtml + "\n" +
            "<div class='dropdown-menu dropdown-menu-lg dropdown-menu-right'>\n" +
            "<span class='dropdown-item dropdown-header'>Change Role</span>\n" +
            "<div class='dropdown-divider'></div>\n";


        for (urole of userroledata) {
            // var swch = "";
            if (urole.text == userrolename) {
                continue;
            } else
                if (urole.text != userrolename) {
                    // swch = "Switch";
                }
            lhtml = lhtml + "\n" +
                "<div class='dropdown-divider'></div>\n" +
                "<a href='#'  onclick=ChangeUserRole('" + urole.value + "') class='dropdown-item'>\n" +
                "<i class='fas mr-2'></i>" + urole.text + "\n" +
                //  "<span class='float-right text-muted text-sm' onclick=ChangeSector('" + sector.value + "')>"+ swch +"</span>\n" +
                "</a>";
        }
        lhtml = lhtml + "\n </div>";
    }
    nav.append(lhtml);
}

function populateSideNavBar() {
    var navmmt2 = $("nav.mt-2 .sectormenu");
    
    var lhtml = "";
    var header = "";
    var parent = "";
    for (module of moduledata) {
        if (header != module.MenuHeading) {
            if (parent != "") {
                if (parent != "blank") {
                    lhtml = lhtml + " </ul></li>";
                }
            }
            header = module.MenuHeading;
            lhtml = lhtml + "<li class='nav-header'>" + header + "</li>";
        }
        if (parent != module.MenuParent && header == module.MenuHeading) {
            if (parent != "") {
                if (parent != "blank") {
                    lhtml = lhtml + " </ul></li>";
                }
            }
            if (module.MenuParent == "null") {
                parent = "blank";
            } else {
                parent = module.MenuParent;
                lhtml = lhtml + "<li class='nav-item has-treeview'>" +
                    "    <a href='#' class='nav-link'>" +
                    "       <i class='nav-icon fas fa-copy'></i>" +
                    "       <p>" + module.MenuParent + " <i class='fas fa-angle-right right'></i></p>" +
                    "    </a>" +
                    "    <ul class='nav nav-treeview'>";
            }
        }
        var link = baseUrl + module.PageUrl;
        if (module.MenuParent == "null") {
            parent = "blank";
            lhtml = lhtml + "\n" +
                "       <li class='nav-item'>" +
                "           <a href= " + link + " class='nav-link'>" +
                "                <i class='nav-icon fas fa-copy'></i>" +
                "                <p>" + module.MenuDisplayName + "</p>" +
                "           </a>" +
                "       </li>";
        } else {
            parent = module.MenuParent
            lhtml = lhtml + "\n" +
                "       <li class='nav-item'>" +
                "           <a href=" + link + " class='nav-link'>" +
                "                <i class='far nav-icon'></i>" +
                "                <p>" + module.MenuDisplayName + "</p>" +
                "           </a>" +
                "       </li>";
        }

    }
    navmmt2.append(lhtml);
}

function ChangeSector(newSectorID) {
    var newSectorName = '';
    for (sec of sectordata) {
        if (sec.value == newSectorID) {
            newSectorName = sec.text;
        }
    }
    //  Utilit y.Page_Alert_Save("The action will reload the application and discard the unsaved changes. Click 'Proceed' to Switch the Sector or click 'Cancel' to stay on the same Sector", "Swith Sector", "Proceed", "Cancel",
    Utility.Page_Alert_Save("You have triggered a command to Switch " + secname + " to " + newSectorName + ". This will discard all unsaved work.", "Swith Sector", "Proceed", "Cancel",
        function () {
            HttpClient.MakeRequest(CookBookLayout.ChangeSectorData, function (result) {
                if (result == false) {
                    alert("Oops! Something went wrong. Please reachout to IT Helpdesk for further assistance");
                }
                else {
                    HttpClient.MakeRequest(CookBookLayout.GetSectorDataList, function (data) {
                        var dataSource = data;
                        sectordata = [];
                        for (var i = 0; i < dataSource.length; i++) {
                            sectordata.push({ "value": dataSource[i].ID, "text": dataSource[i].SectorName, "SectorNumber": dataSource[i].SectorNumber });
                        }

                        populateSectorDataDropdown();
                        window.location.href = baseUrl + "Home/Index";
                    }, null, false);
                }
            }, { "SectorID": newSectorID }, true);
        },
        function () {
            //
        }
    );
}



function ChangeUserRole(userRoleID) {
    var newRoleName = '';
    for (urole of userroledata) {
        if (urole.value == userRoleID) {
            newRoleName = urole.text;
        }
    }
    //  Utilit y.Page_Alert_Save("The action will reload the application and discard the unsaved changes. Click 'Proceed' to Switch the Sector or click 'Cancel' to stay on the same Sector", "Swith Sector", "Proceed", "Cancel",
    Utility.Page_Alert_Save("You have triggered a command to Switch " + secname + " to " + newRoleName + ". This will discard all unsaved work.", "Swith Role", "Proceed", "Cancel",
        function () {
            HttpClient.MakeRequest(CookBookLayout.ChangeSectorData, function (result) {
                if (result == false) {
                    alert("Oops! Something went wrong. Please reachout to IT Helpdesk for further assistance");
                }
                else {
                    HttpClient.MakeRequest(CookBookLayout.GetSectorDataList, function (data) {
                        var dataSource = data;
                        sectordata = [];
                        for (var i = 0; i < dataSource.length; i++) {
                            sectordata.push({ "value": dataSource[i].ID, "text": dataSource[i].SectorName, "SectorNumber": dataSource[i].SectorNumber });
                        }

                        populateSectorDataDropdown();
                        window.location.href = baseUrl + "Home/Index";
                    }, null, false);
                }
            }, { "SectorID": newSectorID }, true);
        },
        function () {
            //
        }
    );
}