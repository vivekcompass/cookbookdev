﻿var datamodel;
$(document).ready(function () {
    $(".k-window").hide();
    $(".k-overlay").hide();
    Utility.Loading();
    setTimeout(function () {
        onLoad();
        Utility.UnLoading();
    }, 2000);

    $("#CuisinewindowEdit").kendoWindow({
        modal: true,
        width: "250px",
        height: "166px",
        title: "Cuisine Details  ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#CuisinewindowEdit1").kendoWindow({
        modal: true,
        width: "250px",
        height: "166px",
        title: "Cuisine Details  ",
        actions: ["Close"],
        visible: false,
        animation: false
    });
});
$("#Cuisineddnew").on("click", function () {
    //
    // var jq = jQuery.noConflict();
    // alert("calick");
    var model;

    var dialog = $("#CuisinewindowEdit").data("kendoWindow");

    //dialog.open();
    dialog.open().element.closest(".k-window").css({
        top: 167,
        left: 558

    });
    // dialog.center();

    datamodel = model;

    dialog.title("New Cuisine Creation");

    $("#dcname").removeClass("is-invalid");
    $('#dcsubmit').removeAttr("disabled");
    $("#dcid").val("");
    $("#dcname").val("");
    $("#dccode").val("");
    $("#dcname").focus();
    DishCategoryCodeIntial = null;

});
function onLoad() {
    populateCafeGrid();
    $("#btnExport").click(function (e) {
        var grid = $("#gridMOG").data("kendoGrid");
        grid.saveAsExcel();
    });
    $('#myInput').on('input', function (e) {
        var grid = $('#gridMOG').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "CuisineCode" || x.field == "Name" || x.field == "IsActive") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
}


$("#Cuisinecancel").on("click", function () {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $(".k-overlay").hide();
            var orderWindow = $("#CuisinewindowEdit").data("kendoWindow");
            orderWindow.close();
        },
        function () {
        }
    );
});

$("#Cuisinecancel1").on("click", function () {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $(".k-overlay").hide();
            var orderWindow = $("#CuisinewindowEdit1").data("kendoWindow");
            orderWindow.close();
        },
        function () {
        }
    );
});

$("#Cuisinesubmit").click(function () {
    debugger;
    if ($("#Cuisinecname").val() === "") {
        toastr.error("Please provide Cuisine Name");
        $("#Cuisinecname").focus();
        return;
    }

   
        var model = datamodel;

        model = {
            
            "Name": $("#Cuisinecname").val(),
            "CuisineCode": $("#Cuisineccode").val()
            
        }
    if (!sanitizeAndSend(model)) {
        return;
    }
    $("#Cuisinesubmit").attr('disabled', 'disabled');
    HttpClient.MakeRequest(CookBookMasters.SaveCuisine, function (result) {
        if (result.xsssuccess !== undefined && !result.xsssuccess) {
            toastr.error(result.message);
            $('#Cuisinesubmit').removeAttr("disabled");
            Utility.UnLoading();
        }
        else {
            if (result == false) {
                $('#Cuisinesubmit').removeAttr("disabled");
                toastr.error(result.message);
                Utility.UnLoading();
                return;
            }
            else {
                if (result == false) {
                    $('#Cuisinesubmit').removeAttr("disabled");
                    toastr.success("There was some error, the record cannot be saved");

                }
                else {
                    $(".k-overlay").hide();

                    toastr.success("New Cuisine added successfully");
                    var orderWindow = $("#CuisinewindowEdit").data("kendoWindow");
                    orderWindow.close();
                    $("#gridMOG").data("kendoGrid").dataSource.data([]);
                    $("#gridMOG").data("kendoGrid").dataSource.read();
                }
            }
        }
        }, {
            model: model

        }, true);

    Utility.UnLoading();
    //populateCafeGrid();
});

$("#Cuisinesubmit1").click(function () {
    debugger;
    if ($("#Cuisinecname1").val() === "") {
        toastr.error("Please provide Cuisine Name");
        $("#Cuisinecname1").focus();
        return;
    }


    var model = datamodel;

    model = {

        "Name": $("#Cuisinecname1").val(),
        "CuisineCode": $("#Cuisineccode1").val()

    }

    if (!sanitizeAndSend(model)) {
        return;
    }
    HttpClient.MakeSyncRequest(CookBookMasters.UpdateCuisine, function (result) {
        if (result.xsssuccess !== undefined && !result.xsssuccess) {
            toastr.error(result.message);
            $('#Cuisinesubmit1').removeAttr("disabled");
            Utility.UnLoading();
        }
        else {
            if (result == false) {
                $('#Cuisinesubmit1').removeAttr("disabled");
                toastr.success("There was some error, the record cannot be saved");

            }
            else {
                $(".k-overlay").hide();

                toastr.success("Cuisine configuration update successfully");
                var orderWindow = $("#CuisinewindowEdit1").data("kendoWindow");
                orderWindow.close();
                $("#gridMOG").data("kendoGrid").dataSource.data([]);
                $("#gridMOG").data("kendoGrid").dataSource.read();
            }
        }
    }, {
        model: model

    }, true);

    Utility.UnLoading();
    //populateCafeGrid();
});
function populateCafeGrid() {
    
    Utility.Loading();
    var gridVariable = $("#gridMOG").height(380);
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "CuisineMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        //pageable: true,
        groupable: false,
        //reorderable: true,
        //scrollable: true,
        height: "410px",
        columns: [
            {
                field: "CuisineCode", title: "Cuisine Code", width: "40px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Cuisine Name", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "IsActive", title: "IsActive", width: "40px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: "<lable>True </lable>"
            },
            {
                field: "Edit", title: "Action", width: "50px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                command: [
                    {
                        name: 'Edit',
                        click: function (e) {
                            debugger;
                            var dialog = $("#CuisinewindowEdit1").data("kendoWindow");                            
                            var tr = $(e.target).closest("tr");                          

                            var item = this.dataItem(tr);          // get the date of this row
                            
                            var model = {
                                
                                "CuisineCode": item.CuisineCode,
                                "Name": item.Name
                                
                            };
                           // DishCategoryCodeIntial = item.DishCategoryCode;


                            dialog.open();
                            dialog.center();

                            datamodel = model;

                            $("#Cuisineccode1").val(model.CuisineCode);
                            
                            $("#Cuisinecname1").val(model.Name);

                            $("#Cuisinecname1").focus();
                            return true;
                        }
                    }
                ],
            }

        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetCuisinMasterList, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }
                    }, null
                        //{
                        //filter: mdl
                        //}
                        , false);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        CuisineCode: { type: "string" },
                        Name: { type: "string" },
                        IsActive: { type: "string" }

                    }
                }
            },
            //pageSize: 15,
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var items = e.sender.items();
            var grid = this;
            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);

                if (model.BATCHSTATUS == "IsActive") {

                    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                    //alert("call");
                }
                else {
                    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");

                }
            });
            items.each(function (e) {
                if (user.UserRoleId == 1) {
                    $(this).find('.k-grid-Edit').text("Edit");
                    $(this).find('.chkbox').removeAttr('disabled');

                } else {
                    $(this).find('.k-grid-Edit').text("View");
                    $(this).find('.chkbox').attr('disabled', 'disabled');
                }
            });


        },
        change: function (e) {
        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
    //$(".k-label")[0].innerHTML.replace("items", "records");
}  