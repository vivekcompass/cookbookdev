﻿
$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var rangetypeName = "";
    var rangetypeNameEdit = "";
    var HexCode = "";

    var sitedata = [];
    var dkdata = [];
    var rangetypeData = [];

    $('#myInput').on('input', function (e) {
        var grid = $('#gridRangeType').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "Code" || x.field == "Name" || x.field == "StdTempFrom" || x.field == "StdTempTo") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $("#RangeTypewindowEdit").kendoWindow({
        modal: true,
        width: "340px",
        height: "166px",
        title: "Range Type Details",
        actions: ["Close"],
        visible: false,
        animation: false,
        resizable: false
    });

    //$('.my-colorpicker2').colorpicker()

    //$('.my-colorpicker2').on('colorpickerChange', function (event) {
    //    if (event.color != null) {
    //        $("#inputrgbcode").val(event.color.toRgbString());
    //        $("#inputhexcode").val(event.color.toString());
    //        $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    //        $('.my-colorpicker2 .fa-square').css('background-color', event.color.toString());
    //        $('.my-colorpicker2 .input-group-text').css('background-color', event.color.toString());
    //    }
    //});

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    var user;
    $(document).ready(function () {



        $(".k-window").hide();
        $(".k-overlay").hide();
        //$("#btnExport").click(function (e) {
        //    var grid = $("#gridRangeType").data("kendoGrid");
        //    grid.saveAsExcel();
        //});
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNew").css("display", "none");
            //}
            populateRangeTypeGrid();
        }, null, false);
    });


    function populateRangeTypeGrid() {

        //Utility.Loading();
        var gridVariable = $("#gridRangeType");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "RangeType.xlsx",
                filterable: true,
                allPages: true
            },
            // sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "Code", title: "Range Type Code", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Range Type Name", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "StdTempFrom", title: "Standard Temparature From", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "StdTempTo", title: "Standard Temparature To", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "IsActive",
                //    title: "Status", width: "50px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {


                                $("#success").css("display", "none");
                                $("#error").css("display", "none");
                                var gridObj = $("#gridRangeType").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                if (!tr.IsActive) {
                                    return;
                                }
                                datamodel = tr;
                                rangetypeName = tr.Name ;
                                //HexCode = tr.HexCode;
                                rangetypeNameEdit = tr.Name;
                                var dialog = $("#RangeTypewindowEdit").data("kendoWindow");

                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open().element.closest(".k-window").css({
                                    top: 167,

                                });
                                //dialog.open();
                                dialog.center();
                                // 
                                $("#rangetypeid").val(tr.ID);
                                $("#RangeTypecode ").val(tr.Code);
                                $("#rangetypename").val(tr.Name);
                                //$("#colorselect").val(tr.HexCode)
                                //$('.my-colorpicker2 .fa-square').css('color', tr.HexCode);
                                //$('.my-colorpicker2').colorpicker('setValue', tr.HexCode);
                                //$('.my-colorpicker2 .fa-square').css('color', tr.HexCode);
                                //$('.my-colorpicker2 .input-group-text').css('background-color', tr.HexCode);
                                $("#rangetypeTo").val(tr.StdTempTo);
                                $("#rangetypeFrom").val(tr.StdTempFrom);
                                //$("#inputfontcode").val(tr.FontColor);
                                //$("#inputsite").data('kendoDropDownList').value(tr.SiteCode);
                                //if (user.UserRoleId === 1) {
                                //    $("#rangetypename").removeAttr('disabled');
                                //    //$("#rangetypeTo").removeAttr('disabled');
                                //    //$("#rangetypeFrom").removeAttr('disabled');
                                //    //$("#inputfontcode").removeAttr('disabled');
                                //    $("#btnSubmit").css('display', 'inline');
                                //    $("#btnCancel").css('display', 'inline');
                                //} else {
                                //    $("#rangetypename").attr('disabled', 'disabled');
                                //    //$("#rangetypeTo").attr('disabled', 'disabled');
                                //    //$("#rangetypeFrom").attr('disabled', 'disabled');
                                //    //$("#inputfontcode").attr('disabled', 'disabled');
                                //    $("#btnSubmit").css('display', 'none');
                                //    $("#btnCancel").css('display', 'none');
                                //}

                                dialog.title("Range Type Details - " + rangetypeName);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetRangeTypeDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                rangetypeData = result;
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false, type: "string" },
                            Code: { type: "string" },
                            StdTempFrom: { type: "int" },
                            StdTempTo: { type: "int" },
                            //FontColor: { type: "string" },
                            IsActive: { editable: false },
                            //CreatedOn: { type: 'Date' },
                            //ModifiedOn: { type: 'Date' }
                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    delete col.width;
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });

                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        $("#gridRangeType").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    $("#AddNew").on("click", function () {
        //alert("call");
        $("#success").css("display", "none");
        $("#error").css("display", "none");
        $('.my-colorpicker2 .fa-square').css('color', '#fff');
        $('.my-colorpicker2 .fa-square').css('background-color', '#fff');
        $('.my-colorpicker2 .input-group-text').css('background-color', '#fff');

        //$('.my-colorpicker2 .fa-square').css('background-color', '#fff');

        var model;
        var dialog = $("#RangeTypewindowEdit").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "-0.5");



        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });
        // dialog.center();

        datamodel = model;
        $("#rangetypeid").val("");
        $("#RangeTypecode ").val("");
        $("#rangetypename").val("");
        $("#rangetypeFrom").val("");
        $("#rangetypeTo").val("");
        $("#colorselect").val("");
        $("#inputfontcode").val("#000000");

        dialog.title("New Range Type");

    })

    $("#btnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#RangeTypewindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#btnSubmit").click(function () {
        //if ($("#colorselect").val() === "") {
        //    toastr.error("Please select the Color");
        //    $("#colorselect").focus();
        //    return;
        //}
        //debugger;
        if ($("#rangetypename").val() === "") {
            toastr.error("Please provide Name");
            $("#rangetypename").focus();
            return;
        }
        if ($("#rangetypeFrom").val() === "") {
            toastr.error("Please provide Std Temp From");
            $("#rangetypeFrom").focus();
            return;
        }
        if ($("#rangetypeTo").val() === "") {
            toastr.error("Please provide Std Temp To");
            $("#rangetypeTo").focus();
            return;
        }
        if ($("#rangetypeTo").val() != "" && $("#rangetypeFrom").val() != "") {
            var from = parseInt($("#rangetypeFrom").val());
            var to = parseInt($("#rangetypeTo").val());
            if (from >= to) {
                toastr.error("Std Temp From must be lower than Std Temp To");
                //$("#rangetypeTo").focus();
                return;
            }
            else {
                if (datamodel == null) { //Duplicate Check
                    for (item of rangetypeData) {
                        if (item.StdTempFrom == from && item.StdTempTo == to) {
                            toastr.error("Temparature range already exists.");
                            $("#rangetypename").focus();
                            return;
                        }
                    }                    
                }

            }
        }
        var namedp = $("#rangetypename").val().toLowerCase().trim();
        if (datamodel == null) { //Duplicate Check
            for (item of rangetypeData) {
                if (item.Name.toLowerCase().trim() == namedp) {
                    toastr.error("Kindly check Duplicate Range Type Name");
                    $("#rangetypename").focus();
                    return;
                }
            }
            //var hexcode = $("#rangetypeFrom").val();
            //for (item of rangetypeData) {
            //    if (item.Name.toLowerCase().trim() == hexcode) {
            //        toastr.error("Kindly check Duplicate Hex Code");
            //        $("#rangetypeFrom").focus();
            //        return;
            //    }
            //}
        }
        //if (datamodel !== null) { //Duplicate Check
        //    for (item of rangetypeData) {
        //        if (item.Name.toLowerCase().trim() == namedp.toLowerCase().trim() && namedp.toLowerCase().trim() !== rangetypeNameEdit.toLowerCase().trim()) {
        //            toastr.error("Kindly check Duplicate Range Type Name");
        //            $("#rangetypename").focus();
        //            return;
        //        }
        //    }
        //    //var fromTemp = $("#rangetypeFrom").val();
        //    //for (item of rangetypeData) {
        //    //    if (item.HexCode.toLowerCase().trim() == hexcode.toLowerCase().trim() && hexcode.toLowerCase().trim() !== HexCode.toLowerCase().trim()) {
        //    //        toastr.error("Kindly check Duplicate Hex Code");
        //    //        $("#rangetypeFrom").focus();
        //    //        return;
        //    //    }
        //    //}
        //}
        //else {
        //        $("#error").css("display", "none");

        var model;
        
        if (datamodel != null) {

            model = datamodel;
            model.Code = $("#RangeTypecode").val();
            model.Name = $("#rangetypename").val();
            model.StdTempFrom = $("#rangetypeFrom").val();
            model.StdTempTo = $("#rangetypeTo").val();

        }
        else {
            model = {
                "ID": $("#rangetypeid").val(),
                "Code": $("#RangeTypecode").val(),
                "Name": $("#rangetypename").val(),
                "StdTempFrom": $("#rangetypeFrom").val(),
                "StdTempTo": $("#rangetypeTo").val(),
                "IsActive": true,
            }
        }


        $("#btnSubmit").attr('disabled', 'disabled');
        if (!sanitizeAndSend(model)) {
            return;
        }
        HttpClient.MakeSyncRequest(CookBookMasters.SaveRangeTypeData, function (result) {
            if (result.xsssuccess !== undefined && !result.xsssuccess) {
                toastr.error(result.message);
                $('#btnSubmit').removeAttr("disabled");
                Utility.UnLoading();
            }
            else {
                if (result == false) {
                    $('#btnSubmit').removeAttr("disabled");
                    //$("#error").css("display", "flex");
                    toastr.error("Some error occured, please try again");
                    // $("#error").find("p").text("Some error occured, please try again");
                    $("#sitealias").focus();
                }
                else {
                    $(".k-overlay").hide();
                    //$("#error").css("display", "flex");
                    var orderWindow = $("#RangeTypewindowEdit").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit').removeAttr("disabled");
                    //$("#success").css("display", "flex");
                    if (model.ID > 0) {
                        toastr.success("Range Type configuration updated successfully");
                    }
                    else {
                        toastr.success("New Range Type added successfully.");
                    }

                    // $("#success").find("p").text("Color configuration updated successfully");
                    $("#gridRangeType").data("kendoGrid").dataSource.data([]);
                    $("#gridRangeType").data("kendoGrid").dataSource.read();
                    $("#gridRangeType").data("kendoGrid").dataSource.sort({ field: "Code", dir: "asc" });
                }
            }
        }, {
            model: model

        }, true);
        // }
    });
});