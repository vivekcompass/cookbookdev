﻿//import { remove } from "toastr";
var dishdata = [];
var sitedishcatlist = [];
var dishcatwithdishData = [];
var viewOnly = 0;
var user;
var filteredSimulationDataSource = [];
var status = "";
var varname = "";
var datamodel;
var ColorName = "";
var sitedata = [];
var simdata = [];
var regionmasterdata = [];
var sitemasterdata = [];
var daypartdata = [];
var itemdata = [];
var dkdata = [];

var itemsMulti;
var itemsid = [];
var selItems = [];
var selDparts = [];
var sitedishlist = [];
var SiteCode;
var resetResult = [];
var newsitedishcats = [];
$(function () {
    //var filteredSimulationDataSource = [];
    //var status = "";
    //var varname = "";
    //var datamodel;
    //var ColorName = "";
    //var sitedata = [];
    //var simdata = [];
    //var regionmasterdata = [];
    //var sitemasterdata = [];
    //var daypartdata = [];
    //var itemdata = [];
    //var dkdata = [];

    //var itemsMulti;
    //var itemsid = [];
    //var selItems = [];
    //var selDparts = [];
    //var sitedishlist = [];
    //var SiteCode;


    $("#FCSimulatorAdd").hide();
    $('#myInput').on('input', function (e) {
        var grid = $('#gridSimulationMaster').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "SimulationCode" || x.field == "Name" || x.field == "Status" || x.field == "Version" || x.field == "CreateOnFormatted") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {

                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });


    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }
    //$("#windowEditMOGWiseAPL").kendoWindow({
    //    modal: true,
    //    width: "900px",
    //    height: "300px",
    //    cssClass: 'aplPopup',
    //    top: '80px !important',
    //    title: "MOG APL Detials - " + MOGName,
    //    actions: ["Close"],
    //    visible: false,
    //    animation: false
    //});
    $("#windowEditMOGWiseAPL").kendoWindow({
        modal: true,
        width: "1050px",
        height: '430px !important',
        left: '220px !important',
        top: '80px !important',
        title: "MOG APL Detials - " + MOGName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    var user;
    $(document).ready(function () {
        Utility.Loading();
        //$("#topHeading").text("Food Cost Simulator");
        $("#radNewSimulation").attr("checked", true);
        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#divDishCategory").kendoWindow({
            modal: true,
            //width: "250px",
            //height: "170px",
            title: "Dish Categories  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });
        $("#btnExport").click(function (e) {
            var grid = $("#gridColor").data("kendoGrid");
            grid.saveAsExcel();
        });

        HttpClient.MakeRequest(CookBookMasters.GetSimulationDataListWithPaging, function (result) {

            filteredSimulationDataSource = result;
            //For Simulation dropdown
            var dataSource = result;
            simdata = [];
            simdata.push({ "value": "Select", "text": "Select Simulation" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].IsActive) {
                    var version = "";
                    if (dataSource[i].Version != null && dataSource[i].Version != "null")
                        version = dataSource[i].Version;
                    simdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name + " " + version });
                }
            }
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNew").css("display", "none");
            //}
            populateSimulationGrid();

        }, null, false);

        //User sector
        HttpClient.MakeSyncRequest(CookBookLayout.GetLoginUserDetailsUrl, function (result) {
            user = result;
        }, null, false);



        //Recipe js
        instructionTextEditor('inputinstruction');
        instructionTextEditor('inputinstruction0');
        //instructionTextEditor('inputinstruction1');
        //instructionTextEditor('inputinstruction2');
        //instructionTextEditor('inputinstruction3');
        //instructionTextEditor('inputinstruction4');
        //instructionTextEditor('inputinstruction5');
        //instructionTextEditor('inputinstructionx');



        //var editor1 = $("#inputinstruction1").data("kendoEditor"),
        //    editorBody1 = $(editor1.body);
        //editorBody1.removeAttr("contenteditable").find("a").on("click.readonly", false)
        //var editor2 = $("#inputinstruction2").data("kendoEditor"),
        //    editorBody2 = $(editor2.body);
        //editorBody2.removeAttr("contenteditable").find("a").on("click.readonly", false)
        //var editor3 = $("#inputinstruction3").data("kendoEditor"),
        //    editorBody3 = $(editor3.body);
        //editorBody3.removeAttr("contenteditable").find("a").on("click.readonly", false)
        //var editor4 = $("#inputinstruction4").data("kendoEditor"),
        //    editorBody4 = $(editor4.body);
        //editorBody4.removeAttr("contenteditable").find("a").on("click.readonly", false)
        //var editor5 = $("#inputinstruction5").data("kendoEditor"),
        //    editorBody5 = $(editor5.body);
        //editorBody5.removeAttr("contenteditable").find("a").on("click.readonly", false)


        //$("#btnCancelRecipe").click(function (e) {
        //    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        //        function () {
        //            //$("#windowEdit").show();
        //            //$("#windowEdit0").hide();
        //            //$("#gridMOG0").html("");
        //            //$("#gridMOG0").hide();
        //            //$("#emptymog0").show();
        //            //$("#inputrecipename0").val("");
        //            //$("#inputrecipealiasname0").val("");
        //            //$("#inputquantity0").val("10");
        //            //$("#inputrecipecum0").data("kendoDropDownList").value("Select");
        //            //$("#copyRecipe").data("kendoDropDownList").value("Select");
        //        },
        //        function () {
        //        }
        //    );
        //});

        $("#btnExport").click(function (e) {
            var grid = $("#gridRecipe").data("kendoGrid");
            grid.saveAsExcel();
        });
        loadFormData();
        testCheckGridHeight();
    });


    $("#radNewSimulation").click(function () {
        var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
        if (dropdownlist != undefined) {
            //dropdownlist.val("");
            dropdownlist.select(0);
            dropdownlist.enable(false);
        }
        dropdownlist = $("#selectFromSite").data("kendoDropDownList");
        if (dropdownlist != undefined) {
            //dropdownlist.val("");
            dropdownlist.select(0);
            dropdownlist.enable(false);
        }
        var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
        if (regDropdown != undefined) {
            //regDropdown.val("");
            regDropdown.select(0);
            regDropdown.enable(true);
        }

        regDropdown = $("#NewSimSiteByRegion").data("kendoDropDownList");
        if (regDropdown != undefined) {
            regDropdown.select(0);
            //regDropdown.val("");
            regDropdown.enable(true);
        }

        var mulDropdown = $("#NewSimServiceType").data("kendoMultiSelect");
        if (mulDropdown != undefined) {
            //regDropdown.val("");
            mulDropdown.value([]);
            mulDropdown.dataSource.data([]);
        }
        mulDropdown = $("#ItemsForSimulation").data("kendoMultiSelect");
        if (mulDropdown != undefined) {
            //regDropdown.val("");
            mulDropdown.value([]);
            mulDropdown.dataSource.data([]);
        }
        $("#divBoard").hide();
        $("#gridSimulationBoard").html("");
        $("#lblavgTotal2").text("");
        $("#NewSimNoOfDays").val("");
        $("#txtSimulationName").val("");
        selItems = [];
        selDparts = [];
    });
    $("#radCopyFromSimulation").click(function () {
        var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
        if (dropdownlist != undefined) {
            dropdownlist.enable(true);
            dropdownlist.select(0);
        }
        dropdownlist = $("#selectFromSite").data("kendoDropDownList");
        if (dropdownlist != undefined) {
            dropdownlist.enable(false);
            dropdownlist.select(0);
        }

        var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
        if (regDropdown != undefined) {
            regDropdown.select(0);
            regDropdown.enable(true);
        }
        regDropdown = $("#NewSimSiteByRegion").data("kendoDropDownList");
        if (regDropdown != undefined) {
            regDropdown.select(0);
            regDropdown.enable(true);
        }
        var mulDropdown = $("#NewSimServiceType").data("kendoMultiSelect");
        if (mulDropdown != undefined) {
            //regDropdown.val("");
            mulDropdown.value([]);
            mulDropdown.dataSource.data([]);
        }
        mulDropdown = $("#ItemsForSimulation").data("kendoMultiSelect");
        if (mulDropdown != undefined) {
            //regDropdown.val("");
            mulDropdown.value([]);
            mulDropdown.dataSource.data([]);
        }
        $("#divBoard").hide();
        $("#gridSimulationBoard").html("");
        $("#lblavgTotal2").text("");
        $("#NewSimNoOfDays").val("");
        $("#txtSimulationName").val("");
        selItems = [];
        selDparts = [];
    });
    $("#radCopyFromSite").click(function () {
        var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
        if (dropdownlist != undefined) {
            dropdownlist.enable(false);
            dropdownlist.select(0);
        }
        dropdownlist = $("#selectFromSite").data("kendoDropDownList");
        if (dropdownlist != undefined) {
            dropdownlist.enable(true);
            dropdownlist.select(0);
        }

        var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
        if (regDropdown != undefined) {
            regDropdown.enable(false);
            regDropdown.select(0);
        }
        regDropdown = $("#NewSimSiteByRegion").data("kendoDropDownList");
        if (regDropdown != undefined) {
            regDropdown.enable(false);
            regDropdown.select(0);
        }
        var mulDropdown = $("#NewSimServiceType").data("kendoMultiSelect");
        if (mulDropdown != undefined) {
            //regDropdown.val("");
            mulDropdown.value([]);
            mulDropdown.dataSource.data([]);
        }
        mulDropdown = $("#ItemsForSimulation").data("kendoMultiSelect");
        if (mulDropdown != undefined) {
            //regDropdown.val("");
            mulDropdown.value([]);
            mulDropdown.dataSource.data([]);
        }
        $("#divBoard").hide();
        $("#gridSimulationBoard").html("");
        $("#lblavgTotal2").text("");
        $("#NewSimNoOfDays").val("");
        $("#txtSimulationName").val("");
        selItems = [];
        selDparts = [];
    });

    $("#AddNew").click(function () {
        ////alert("call");
        Utility.Loading();
        setTimeout(function () {
            //console.log(localStorage.getItem("contextitem"))
            if (localStorage.getItem("contextitem") != undefined) {

                localStorage.removeItem("contextitem");
            }

            $("#spanHeadertxt").html("Create New Simulation");
            $("#divSimConfig").removeAttr("disabled");
            var multiselect = $("#selectSimulation").data("kendoDropDownList");
            if (multiselect != undefined)
                multiselect.value("Select");
            multiselect = $("#selectFromSite").data("kendoDropDownList");
            if (multiselect != undefined)
                multiselect.value("Select");

            showAddNewSection(null, null);
            if ($("#btnExpandConfig").text() == "Expand") {
                $("#btnExpandConfig").text("Collapse");
                $(".divConfigFcs").slideToggle(0, function () { });
            }

            $("#topHeading").text("Food Cost Simulator");

            $("#radNewSimulation").click();
        }, 100);
        //$('.my-colorpicker2 .fa-square').css('color', '#fff');
        //$('.my-colorpicker2 .fa-square').css('background-color', '#fff');
        //$('.my-colorpicker2 .input-group-text').css('background-color', '#fff');

        ////$('.my-colorpicker2 .fa-square').css('background-color', '#fff');

        //var model;
        //var dialog = $("#ColorwindowEdit").data("kendoWindow");

        //$(".k-overlay").css("display", "block");
        //$(".k-overlay").css("opacity", "-0.5");



        //dialog.open().element.closest(".k-window").css({
        //    top: 167,
        //    left: 558

        //});
        //// dialog.center();

        //datamodel = model;
        //$("#colorid").val("");
        //$("#colorcode ").val("");
        //$("#colorname").val("");
        //$("#inputhexcode").val("");
        //$("#inputrgbcode").val("");
        //$("#colorselect").val("");
        //$("#inputfontcode").val("#000000");

        //dialog.title("New Color Creation");

    })

    $("#btnGo").on("click", function () {

        var simulationId = $("#SimulationID").val();
        if (simulationId > 0) {
            //validate 
            var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");
            var multiselectitem = $("#ItemsForSimulation").data("kendoMultiSelect");
            var noOfDays = $("#NewSimNoOfDays").val();

            var txtSimulationName = $("#txtSimulationName").val();

            var msg = "";

            var defName = "Please Select Service Type<br>";
            if (user.SectorNumber != "20")
                defName = "Please Select Mealtime<br>";
            if (multiselectdp != undefined) {
                var items = multiselectdp.value();

                if (items.length == 0)
                    msg += defName;
                else {
                    items = items.filter(a => a == 0);
                    if (items.length > 0) {
                        if (user.SectorNumber != "20")
                            msg += "Please select valid mealtime";
                        else
                            msg += "Please select valid service type";
                    }

                    //for (var i = 0; i < items.length; i++) {
                    //    daypart.push(items[i]);
                    //}
                    //daypart = $("#NewSimServiceType").data("kendoMultiSelect").dataItems();
                }
            }
            else
                msg += defName;
            if (multiselectitem != undefined) {
                var items = multiselectitem.value();

                if (items.length == 0)
                    msg += "Please Select Item<br>";
                else {
                    items = items.filter(a => a == "Select");
                    if (items.length > 0) {
                        msg += "Please select valid item";
                    }
                    //selecteditem = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();
                    //selecteditem = itemsid;
                }
            }
            else
                msg += "Please Select Item<br>";
            if (txtSimulationName.length == 0)
                msg += "Please Enter Simulation Name<br>";

            if (msg.length > 0) {
                toastr.error(msg);
                Utility.UnLoading();
            }
            else {
                //New Updated selecteditem 5thApril
                var siteid = $("#NewSimSiteByRegion").val();
                var regionid = $("#NewSimRegion").val();
                var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");
                var daypartid = [];
                if (multiselectdp != undefined) {
                    var items = multiselectdp.value();
                    for (var i = 0; i < items.length; i++) {
                        daypartid.push(items[i]);
                    }
                }
                if (daypartid.length == 0)
                    daypartid.push(0);
                HttpClient.MakeRequest(CookBookMasters.GetSimulationItemList, function (data) {
                    var dataSource = data;
                    itemdata = [];
                    itemdata.push({ "value": "Select", "text": "Select Items", "code": "Select", "dpcode": "", "DishCategories": [] });
                    for (var i = 0; i < dataSource.length; i++) {
                        itemdata.push({ "value": dataSource[i].ItemID + "_" + dataSource[i].DayPartCode, "text": dataSource[i].ItemName + " - " + dataSource[i].DayPartName, "code": dataSource[i].ItemCode, "dpcode": dataSource[i].DayPartCode, "DishCategories": dataSource[i].DishCategoryList });
                    }
                    console.log("itemdata when clciked on go and sim>0")
                    console.log(itemdata)

                    var multiselect = $("#ItemsForSimulation").data("kendoMultiSelect");
                    var ds = new kendo.data.DataSource({ data: itemdata });
                    multiselect.setDataSource(ds);
                    if (selItems.length > 0) {
                        console.log("selItems")
                        console.log(selItems)

                        var selectedp = multiselectdp.dataItems();
                        var selItemNew = [];
                        selItems.forEach(function (item, index) {
                            var dp = item.split('_')[1];

                            var chkindplist = selectedp.filter(a => a.DayPartCode == dp);
                            if (chkindplist.length > 0)
                                selItemNew.push(item);
                        });
                        multiselect.value(selItemNew);
                    }


                    //Added parts after ajax call here 5th April   
                    //------------------------------------------
                    saveSimulationBoard($("#hdnStatus").val(), $("#btnGo"), "btngo");


                }, { siteId: siteid, regionId: regionid, daypartId: daypartid, checkSiteItem: true }, false);


                //saveSimulationBoard($("#hdnStatus").val(), $("#btnGo"));
            }
        }
        else {
            //var noOfDays = $("#NewSimNoOfDays").val();
            //var region = $("#NewSimRegion").val();
            Utility.Loading();
            var daypart = [];
            var selecteditem = [];

            //daypart = $("#NewSimServiceType").data("kendoMultiSelect").dataItems();
            //selecteditem = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();
            var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");
            var multiselectitem = $("#ItemsForSimulation").data("kendoMultiSelect");
            var noOfDays = $("#NewSimNoOfDays").val();
            var region = $("#NewSimRegion").val();
            var msg = validateSubmit(noOfDays, region, multiselectdp, multiselectitem);
            var txtSimulationName = $("#txtSimulationName").val();
            if (txtSimulationName.length == 0)
                msg += "Please Enter Simulation Name<br>";
            ////Region check
            //if (region == 0) {
            //    msg += "Please Select Region<br>";
            //}
            ////Number of days check                              
            //if (noOfDays.length == 0) {
            //    msg += "Please Enter Number of Days<br>";
            //}
            ////Service type check
            //var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");
            //var defName = "Please Select Service Type<br>";
            //if (user.SectorNumber != "20")
            //    defName = "Please Select Mealtime<br>";
            //if (multiselectdp != undefined) {
            //    var items = multiselectdp.value();
            //    if (items.length == 0)
            //        msg += defName;
            //    else {
            //        //for (var i = 0; i < items.length; i++) {
            //        //    daypart.push(items[i]);
            //        //}
            //        daypart = $("#NewSimServiceType").data("kendoMultiSelect").dataItems();
            //    }
            //}
            //else
            //    msg += defName;
            ////Items check
            //var multiselectitem = $("#ItemsForSimulation").data("kendoMultiSelect");
            //if (multiselectitem != undefined) {
            //    var items = multiselectitem.value();
            //    if (items.length == 0)
            //        msg += "Please Select Item<br>";
            //    else {
            //        selecteditem = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();
            //        //selecteditem = itemsid;
            //    }
            //}
            //else
            //    msg += "Please Select Item<br>";
            var simcode = $("#SimulationCode").val();
            if (simcode.length == 0) {
                var chkDup = simdata.filter(a => a.text.toLowerCase().trim() == txtSimulationName.toLowerCase().trim());
                if (chkDup.length > 0)
                    msg += "Simulation name already exist.<br>";
            }

            if (msg.length > 0) {
                toastr.error(msg);
                Utility.UnLoading();
            }
            else {
                //Utility.Loading();
                //saveSimulationBoard("Incomplete", this);

                var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");
                var multiselectitem = $("#ItemsForSimulation").data("kendoMultiSelect");
                var noOfDays = $("#NewSimNoOfDays").val();
                var region = $("#NewSimRegion").val();
                var site = $("#NewSimSiteByRegion").val();
                //var msg = validateSubmit(noOfDays, region, multiselectdp, multiselectitem);
                //var txtSimulationName = $("#txtSimulationName").val();
                //if (txtSimulationName.length == 0)
                //    msg += "Please Enter Simulation Name<br>";

                //Post values
                var contextData = JSON.parse(localStorage.getItem("contextitem"));
                //console.log(contextData);
                //console.log("contextData")
                //for (var i = 0; i < contextData.length; i++) {
                //    for (var j = 0; j < contextData[i].DishCategories.length; j++) {
                //        for (var k = 0; k < contextData[i].DishCategories[j].Dishes.length; k++) {
                //            var currentDishData = contextData[i].DishCategories[j].Dishes[k];
                //            var currentDcData = contextData[i].DishCategories[j];
                //            var currentItem = contextData[i];
                //            var itemidvalue = currentItem.value.split('_')[0];
                //            var dataO = [];
                //            for (var l = 1; l <= noOfDays; l++) {
                //                var paxTxt = "#txtPax_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
                //                var grmTxt = ".txtGrmroot_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
                //                var costTxt = "#lblCost_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;

                //                dataO.push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });
                //                //contextData[i].DishCategories[j].Dishes[k].push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });

                //            }
                //            contextData[i].DishCategories[j].Dishes[k].dataO = dataO;
                //        }
                //    }
                //    var itAvgCost = ".lblavgItemTotal_" + contextData[i].dpcode + "_" + contextData[i].value.split('_')[0];
                //    contextData[i].TotalAvgCost = $(itAvgCost).text();

                //}
                console.log("data")
                console.log(contextData)
                //$(btnEle).attr('disabled', 'disabled');
                //var contextData = [];
                //Utility.Loading();



                daypart = multiselectdp.dataItems();
                selecteditem = multiselectitem.dataItems();
                console.log("selecteditem")
                console.log(selecteditem)
                console.log(daypart)
                //Bind board
                //performSimulationBoardFill(noOfDays, region, daypart, itemsid);

                //performSimulationBoardFill(noOfDays, region, daypart, selecteditem);

                Utility.Loading();

                //expandConfig($("#btnExpandConfig"));

                //var contextData = JSON.parse(localStorage.getItem("contextitem"));
                setTimeout(function () {
                    var contextData = selecteditem;
                    for (var i = 0; i < contextData.length; i++) {
                        for (var j = 0; j < contextData[i].DishCategories.length; j++) {
                            for (var k = 0; k < contextData[i].DishCategories[j].Dishes.length; k++) {
                                var currentDishData = contextData[i].DishCategories[j].Dishes[k];
                                var currentDcData = contextData[i].DishCategories[j];
                                var currentItem = contextData[i];
                                var itemidvalue = currentItem.value.split('_')[0];
                                var dataO = [];
                                for (var l = 1; l <= noOfDays; l++) {
                                    var paxTxt = "#txtPax_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
                                    var grmTxt = ".txtGrmroot_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
                                    var costTxt = "#lblCost_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;

                                    dataO.push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });
                                    //contextData[i].DishCategories[j].Dishes[k].push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });

                                }
                                contextData[i].DishCategories[j].Dishes[k].dataO = dataO;
                            }
                        }
                        var itAvgCost = ".lblavgItemTotal_" + contextData[i].dpcode + "_" + contextData[i].value.split('_')[0];
                        contextData[i].TotalAvgCost = $(itAvgCost).text();
                        //if (removedDishContext.length>0) {
                        //    contextData[i].DeletedDishes = removedDishContext;
                        //}
                    }
                    //var remboarddishid = [];
                    //if (removedDishContext.length > 0) {
                    //    removedDishContext.forEach(function (item, index) {
                    //        item.dataO.forEach(function (item1, index1) {
                    //            remboarddishid.push(item1.boardId);
                    //        });                        
                    //    });
                    //}
                    //6th April
                    var status = "Incomplete";
                    if ($("#hdnStatus").val().length > 0)
                        status = $("#hdnStatus").val();

                    console.log("contextData before saving")
                    console.log(contextData)

                    HttpClient.MakeSyncRequest(CookBookMasters.SaveSimulationData, function (result) {
                        //Utility.UnLoading();
                        var dataResult = "" + result;
                        if (dataResult == "false") {
                            //$(btnEle).removeAttr("disabled");
                            //$("#error").css("display", "flex");
                            toastr.error("Some error occured, please try again");
                            // $("#error").find("p").text("Some error occured, please try again");
                            //$("#sitealias").focus();
                            Utility.UnLoading();
                        }
                        else if (result == "Duplicate") {
                            //$(btnEle).removeAttr("disabled");
                            //$("#error").css("display", "flex");
                            toastr.error("Simulation name already exist.<br>");
                            Utility.UnLoading();
                        }
                        else {
                            //$(".k-overlay").hide();
                            //$("#error").css("display", "flex");
                            //var orderWindow = $("#ColorwindowEdit").data("kendoWindow");
                            //orderWindow.close();
                            //$(btnEle).removeAttr("disabled");
                            //$("#success").css("display", "flex");
                            //if (parseInt($("#SimulationID").val()) > 0) {
                            //    toastr.success("Updated successfully");
                            //}
                            //else {
                            //    toastr.success("Added successfully");
                            //}
                            if (dataResult.indexOf('_') >= 0) {
                                $("#SimulationID").val(dataResult.split('_')[0]);
                                $("#SimulationCode").val(dataResult.split('_')[1]);

                            }
                            if (status == "Versioned" || status == "Final")
                                clearAndShowList();
                            // $("#success").find("p").text("Color updated successfully");
                            //$("#gridColor").data("kendoGrid").dataSource.data([]);
                            //$("#gridColor").data("kendoGrid").dataSource.read();
                            //$("#gridColor").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
                            //For trial below section
                            //Utility.UnLoading();

                            resetResult = [];
                            HttpClient.MakeRequest(CookBookMasters.GetSimulationBoardData, function (result) {
                                console.log("result");
                                console.log(result);
                                Utility.Loading();
                                $("#SimulationID").val(result.ID);
                                $("#SimulationCode").val(result.SimCode);
                                $("#hdnStatus").val(result.Status);
                                //14th April reset purpose
                                resetResult = result;
                                if (result.Status == "Incomplete") {
                                    $("#btnUpdateVer").attr('disabled', 'disabled');
                                    $("#btnFinalize").attr('disabled', 'disabled');
                                }
                                else {
                                    $("#btnUpdateVer").removeAttr('disabled');
                                    $("#btnFinalize").removeAttr('disabled');
                                }

                                $("#NewSimNoOfDays").val(result.NoOfDays);
                                $("#txtSimulationName").val(result.SimulationName);
                                var version = "";
                                if (result.Version != null && result.Version != "null")
                                    version = result.Version;

                                $("#divSimConfig input").attr("disabled", "disabled");
                                $("#divSimConfig").addClass("divDisabled");

                                //dropdownlist.enable(true);
                                viewOnly = 0;

                                $("#spanHeadertxt").html("Update Simulation " + result.SimulationName + " " + version);

                                //get sitedish data if not generated
                                //if(sitedish)

                                populateExistingBoard(result);


                                var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
                                dropdownlist.enable(false);
                                dropdownlist = $("#selectFromSite").data("kendoDropDownList");
                                dropdownlist.enable(false);

                                var regDropdown = $("#NewSimSiteByRegion").data("kendoDropDownList");
                                if (regDropdown != undefined)
                                    regDropdown.enable(true);

                                //Utility.UnLoading();
                            }, { simId: dataResult.split('_')[0] }, false);

                        }
                    }, {
                        model: contextData,
                        simId: $("#SimulationID").val(),
                        noOfDays: noOfDays,
                        siteId: site,
                        regionId: region,
                        //status: "Incomplete",
                        status: status,
                        saveOnly: false,
                        simName: $("#txtSimulationName").val()
                    }, true);

                }, 100);
                //Utility.Loading();
                //if (multiselectit != undefined) {
                //var items = multiselectit.value();
                //var selecteditems = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();

                /*console.log("items" + selecteditems);*/
                //}
            }
        }
    });

    $("#btnCancel").on("click", function () {
        if (viewOnly != 1) {
            Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
                function () {
                    clearAndShowList();


                },
                function () {
                }
            );
        }
        else
            clearAndShowList();

    });
    $("#btnCancelVO").on("click", function () {
        clearAndShowList();
        $("#btnCancelVO").hide();
        $("#btnResetBoard").show();
        $("#AddNew").show();
        $("#btnCancel").show();
        $("#btnGo").show();
        $("#NewSimNoOfDays").removeAttr("disabled");
        $("#txtSimulationName").removeAttr("disabled");
        setTimeout(function () {
            var dropdownlist = $("#NewSimRegion").data("kendoDropDownList");
            dropdownlist.enable(true);
            dropdownlist = $("#NewSimSiteByRegion").data("kendoDropDownList");
            dropdownlist.enable(true);
            dropdownlist = $("#ItemsForSimulation").data("kendoMultiSelect");
            dropdownlist.enable(true);
            dropdownlist = $("#NewSimServiceType").data("kendoMultiSelect");
            dropdownlist.enable(true);
        }, 100);
    });
    $("#btnSaveAsDraft").click(function () {
        if (parseInt($("#SimulationID").val()) > 0)
            saveSimulationBoard("Draft", this);
        else {
            $("#hdnStatus").val("Draft");
            $("#btnGo").click();
        }
    });
    $("#btnUpdateVer").click(function () {
        saveSimulationBoard("Versioned", this);

    });
    $("#btnFinalize").click(function () {
        saveSimulationBoard("Final", this);

    });

    //14th April
    $("#btnResetBoard").click(function () {
        $(this).attr('disabled', 'disabled');
        Utility.Loading();
        setTimeout(function () {
            populateExistingBoard(resetResult);
            $("#btnResetBoard").removeAttr('disabled');
        }, 0);

    });

    //Recipe
    //var baserecipedata = [];
    //var baserecipedatafiltered = [];
    //var basedataList = [];
    //var MOGCode;
    //var mogWiseAPLMasterdataSource = [];
    //var mogdata = [];
    //var mogdataList = [];
    //var isShowElmentoryRecipe = false;
    //var uommodulemappingdata = [];
    //var moguomdata = [];
    //var uomdata = [];
    //var recipecategoryuomdata = [];

    //$("#AddNewRecipe").click(function () {
    //    $("#windowEdit0").show();
    //    $("#windowEdit").hide();

    //    //HttpClient.MakeSyncRequest(CookBookMasters.GetMOGDataList, function (data) {

    //    //    var dataSource = data;

    //    //    mogdata = [];
    //    //    mogdata.push({ "MOG_ID": "Select MOG", "MOGName": "Select MOG", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "MOG_Code": "", "IsActive": false });
    //    //    for (var i = 0; i < dataSource.length; i++) {
    //    //        if (dataSource[i].CostPerUOM == null) {
    //    //            dataSource[i].CostPerUOM = 0;
    //    //        }
    //    //        if (dataSource[i].CostPerKG == null) {
    //    //            dataSource[i].CostPerKG = 0;
    //    //        }
    //    //        if (dataSource[i].TotalCost == null) {
    //    //            dataSource[i].TotalCost = 0;
    //    //        }
    //    //        if (dataSource[i].Alias != null && dataSource[i].Alias.trim() != "" && dataSource[i].Alias != dataSource[i].Name)
    //    //            mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name + " (" + dataSource[i].Alias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive });
    //    //        else
    //    //            mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive });
    //    //    }

    //    //    mogdataList = mogdata;

    //    //}, null, true);

    //    //HttpClient.MakeSyncRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
    //    //    var dataSource = data;
    //    //    uommodulemappingdata = [];
    //    //    uommodulemappingdata.push({ "value": "Select", "text": "Select", "UOMCode": "Select" });
    //    //    for (var i = 0; i < dataSource.length; i++) {
    //    //        if (dataSource[i].IsActive) {
    //    //            uommodulemappingdata.push({
    //    //                "value": dataSource[i].UOM_ID, "UOM_ID": dataSource[i].UOM_ID, "UOMName": dataSource[i].UOMName, "text": dataSource[i].UOMName,
    //    //                "UOMModuleCode": dataSource[i].UOMModuleCode, "UOMCode": dataSource[i].UOMCode
    //    //            });
    //    //        }
    //    //    }
    //    //    moguomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001');
    //    //    uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");
    //    //    recipecategoryuomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00006' || m.text == "Select");

    //    //    //populateDishDropdown();

    //    //}, null, false);

    //    //populateNewRecipeScreen();
    //});
    $("#btnCancelRecipe").click(function () {
        if (viewOnly == 1) {
            $("#divSimConfigMain").show();
            $("#divBoard").show();
            $("#windowEdit").hide();

            $("#hdnDishId").val("");
            $("#hdnItemId").val("");
            $("#hdnDishCatCode").val("");
            $("#hdnDishCode").val("");

            //Top nav
            $("#divSimTopNav").show();
        }
        else {
            Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
                function () {
                    $("#divSimConfigMain").show();
                    $("#divBoard").show();
                    $("#windowEdit").hide();

                    $("#hdnDishId").val("");
                    $("#hdnItemId").val("");
                    $("#hdnDishCatCode").val("");
                    $("#hdnDishCode").val("");

                    //Top nav
                    $("#divSimTopNav").show();
                    //$("#topHeading").text("Unit Recipe Configuration");
                },
                function () {
                }
            );
        }
    });
    $("#btnSubmitRecipe").click(function () {

    });
    //New recipe screen
    $("#btnCancel0").click(function () {

    });
    $("#btnSubmit0").click(function () {

    });
    $("#btnPublish0").click(function () {

    });
    $("#btnCancelAPL").click(function (e) {

        $(".k-overlay").hide();
        var orderWindow = $("#windowEditMOGWiseAPL").data("kendoWindow");
        orderWindow.close();

    });


    function saveSimulationBoard(status, btnEle, isBtnGo) {
        var btnid = $(btnEle).attr("id");
        var saveOnly = false;
        if (btnid == "btnGo") {
            saveOnly = true;
        }

        var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");
        var multiselectitem = $("#ItemsForSimulation").data("kendoMultiSelect");
        var noOfDays = $("#NewSimNoOfDays").val();
        var region = $("#NewSimRegion").val();
        var site = $("#NewSimSiteByRegion").val();
        var msg = validateSubmit(noOfDays, region, multiselectdp, multiselectitem);
        var txtSimulationName = $("#txtSimulationName").val();
        if (txtSimulationName.length == 0)
            msg += "Please Enter Simulation Name<br>";

        if (parseInt($("#SimulationID").val()) == 0) {
            var chkDup = simdata.filter(a => a.name.toLowerCase().trim() == txtSimulationName.toLowerCase().trim());
            if (chkDup.length > 0)
                msg += "Simulation name already exist.<br>";
        }
        if (msg.length > 0) {
            toastr.error(msg);
        } else {
            //Post values
            var contextData = JSON.parse(localStorage.getItem("contextitem"));
            var befcont = [];
            befcont.push(contextData);
            console.log("befcont");

            console.log(contextData);
            console.log("contextData")
            Utility.Loading();
            setTimeout(function () {
                if (contextData.length > 0) {
                    for (var i = 0; i < contextData.length; i++) {
                        if (contextData[i].DishCategories.length > 0) {
                            for (var j = 0; j < contextData[i].DishCategories.length; j++) {
                                //8th April
                                if (contextData[i].DishCategories[j].Dishes.length > 0) {
                                    if (isBtnGo == "btngo") {
                                        console.log("btngo itemdata")
                                        console.log(itemdata);
                                        var filItem = itemdata.filter(a => a.code == contextData[i].code && a.dpcode == contextData[i].dpcode)[0];
                                        if (filItem != undefined) {
                                            var filDishCat = filItem.DishCategories.filter(a => a.DishCategoryCode == contextData[i].DishCategories[j].DishCategoryCode)[0];
                                            if (filDishCat != undefined) {
                                                contextData[i].DishCategories[j].Dishes = filDishCat.Dishes;
                                            }
                                        }
                                    }
                                    for (var k = 0; k < contextData[i].DishCategories[j].Dishes.length; k++) {
                                        var currentDishData = contextData[i].DishCategories[j].Dishes[k];
                                        var currentDcData = contextData[i].DishCategories[j];
                                        var currentItem = contextData[i];
                                        var itemidvalue = currentItem.value.split('_')[0];
                                        var dataO = [];
                                        for (var l = 1; l <= noOfDays; l++) {
                                            var paxTxt = "#txtPax_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
                                            var grmTxt = ".txtGrmroot_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
                                            var costTxt = "#lblCost_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;

                                            dataO.push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });
                                            //contextData[i].DishCategories[j].Dishes[k].push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });

                                        }
                                        contextData[i].DishCategories[j].Dishes[k].dataO = dataO;
                                    }
                                }
                                else {
                                    toastr.error("Please add dishcategory and dish.");
                                    return;
                                }
                            }
                            var itAvgCost = ".lblavgItemTotal_" + contextData[i].dpcode + "_" + contextData[i].value.split('_')[0];
                            contextData[i].TotalAvgCost = $(itAvgCost).text();
                        }
                        else {
                            //contextData.splice(i, 1);
                            toastr.error("Please add dishcategory and dish.");
                            return;
                        }
                    }
                    console.log("data")
                    console.log(contextData)
                    $(btnEle).attr('disabled', 'disabled');
                    //TODO
                    //var remboarddishid = [];
                    //if (removedDishContext.length > 0) {
                    //    removedDishContext.forEach(function (item, index) {
                    //        item.dataO.forEach(function (item1, index1) {
                    //            remboarddishid.push(item1.boardId);
                    //        });
                    //    });
                    //}
                    console.log("contextData before saving")
                    console.log(contextData)
                    HttpClient.MakeSyncRequest(CookBookMasters.SaveSimulationData, function (result) {
                        //Utility.UnLoading();

                        if (result == false) {
                            $(btnEle).removeAttr("disabled");
                            //$("#error").css("display", "flex");
                            toastr.error("Some error occured, please try again");
                            // $("#error").find("p").text("Some error occured, please try again");
                            //$("#sitealias").focus();
                        }
                        else if (result == "Duplicate") {
                            $(btnEle).removeAttr("disabled");
                            //$("#error").css("display", "flex");
                            toastr.error("Simulation name already exist.<br>");
                        }
                        else {
                            //$(".k-overlay").hide();
                            //$("#error").css("display", "flex");
                            //var orderWindow = $("#ColorwindowEdit").data("kendoWindow");
                            //orderWindow.close();
                            $(btnEle).removeAttr("disabled");
                            //$("#success").css("display", "flex");
                            if (parseInt($("#SimulationID").val()) > 0) {
                                toastr.success("Updated successfully");
                                resetResult = [];
                                HttpClient.MakeRequest(CookBookMasters.GetSimulationBoardData, function (result) {
                                    console.log("result");
                                    console.log(result);
                                    //14th April reset purpose
                                    resetResult = result;
                                    $("#SimulationID").val(result.ID);
                                    $("#SimulationCode").val(result.SimCode);
                                    $("#hdnStatus").val(result.Status);

                                    $("#NewSimNoOfDays").val(result.NoOfDays);
                                    $("#txtSimulationName").val(result.SimulationName);
                                    var version = "";
                                    if (result.Version != null && result.Version != "null")
                                        version = result.Version;

                                    $("#divSimConfig input").attr("disabled", "disabled");
                                    $("#divSimConfig").addClass("divDisabled");


                                    //dropdownlist.enable(true);
                                    viewOnly = 0;

                                    $("#spanHeadertxt").html("Update Simulation " + result.SimulationName + " " + version);

                                    //get sitedish data if not generated
                                    //if(sitedish)

                                    populateExistingBoard(result);


                                    var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
                                    dropdownlist.enable(false);
                                    dropdownlist = $("#selectFromSite").data("kendoDropDownList");
                                    dropdownlist.enable(false);

                                    var regDropdown = $("#NewSimSiteByRegion").data("kendoDropDownList");
                                    if (regDropdown != undefined)
                                        regDropdown.enable(true);

                                    //Utility.UnLoading();
                                }, { simId: $("#SimulationID").val() }, false);
                            }
                            else {
                                toastr.success("Added successfully");
                            }
                            if (!saveOnly) {
                                if (status == "Versioned" || status == "Final")
                                    clearAndShowList();
                            }
                            else {
                                //$("#SimulationID").val()
                                resetResult = [];
                                HttpClient.MakeRequest(CookBookMasters.GetSimulationBoardData, function (result) {
                                    console.log("result");
                                    console.log(result);
                                    //14th April reset purpose
                                    resetResult = result;
                                    $("#SimulationID").val(result.ID);
                                    $("#SimulationCode").val(result.SimCode);
                                    $("#hdnStatus").val(result.Status);

                                    $("#NewSimNoOfDays").val(result.NoOfDays);
                                    $("#txtSimulationName").val(result.SimulationName);
                                    var version = "";
                                    if (result.Version != null && result.Version != "null")
                                        version = result.Version;

                                    $("#divSimConfig input").attr("disabled", "disabled");
                                    $("#divSimConfig").addClass("divDisabled");


                                    //dropdownlist.enable(true);
                                    viewOnly = 0;

                                    $("#spanHeadertxt").html("Update Simulation " + result.SimulationName + " " + version);

                                    //get sitedish data if not generated
                                    //if(sitedish)

                                    populateExistingBoard(result);


                                    var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
                                    dropdownlist.enable(false);
                                    dropdownlist = $("#selectFromSite").data("kendoDropDownList");
                                    dropdownlist.enable(false);

                                    var regDropdown = $("#NewSimSiteByRegion").data("kendoDropDownList");
                                    if (regDropdown != undefined)
                                        regDropdown.enable(true);

                                    //Utility.UnLoading();
                                }, { simId: $("#SimulationID").val() }, false);

                            }
                            // $("#success").find("p").text("Color updated successfully");
                            //$("#gridColor").data("kendoGrid").dataSource.data([]);
                            //$("#gridColor").data("kendoGrid").dataSource.read();
                            //$("#gridColor").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
                            Utility.UnLoading();
                        }
                    }, {
                        model: contextData,
                        simId: $("#SimulationID").val(),
                        noOfDays: noOfDays,
                        siteId: site,
                        regionId: region,
                        status: status,
                        saveOnly: saveOnly,
                        simName: $("#txtSimulationName").val()
                    }, true);
                }
                else {
                    toastr.error("Please add at least one dishcategory and dish to run food cost simulator.");
                }
            }, 100);
        }
    }



    $("#btnSubmit").click(function () {
        //if ($("#colorselect").val() === "") {
        //    toastr.error("Please select the Color");
        //    $("#colorselect").focus();
        //    return;
        //}
        //if ($("#colorname").val() === "") {
        //    toastr.error("Please provide Color Name");
        //    $("#colorname").focus();
        //    return;
        //}
        //else {
        //    //        $("#error").css("display", "none");

        //    var model;
        //    if (datamodel != null) {
        //        model = datamodel;
        //        model.ColorCode = $("#colorcode").val();
        //        model.Name = $("#colorname").val();
        //        model.HexCode = $("#inputhexcode").val();
        //        model.RGBCode = $("#inputrgbcode").val();
        //        model.FontColor = $("#inputfontcode").val();
        //    }
        //    else {
        //        model = {
        //            "ID": $("#colorid").val(),
        //            "ColorCode": $("#colorcode").val(),
        //            "Name": $("#colorname").val(),
        //            "HexCode": $("#inputhexcode").val(),
        //            "RGBCode": $("#inputrgbcode").val(),
        //            "FontColor": $("#inputfontcode").val(),
        //            "IsActive": true,
        //        }
        //    }


        //    $("#btnSubmit").attr('disabled', 'disabled');
        //    HttpClient.MakeSyncRequest(CookBookMasters.SaveColorData, function (result) {
        //        if (result == false) {
        //            $('#btnSubmit').removeAttr("disabled");
        //            //$("#error").css("display", "flex");
        //            toastr.error("Some error occured, please try again");
        //            // $("#error").find("p").text("Some error occured, please try again");
        //            $("#sitealias").focus();
        //        }
        //        else {
        //            $(".k-overlay").hide();
        //            //$("#error").css("display", "flex");
        //            var orderWindow = $("#ColorwindowEdit").data("kendoWindow");
        //            orderWindow.close();
        //            $('#btnSubmit').removeAttr("disabled");
        //            //$("#success").css("display", "flex");
        //            if (model.ID > 0) {
        //                toastr.success("Color updated successfully");
        //            }
        //            else {
        //                toastr.success("Color added successfully");
        //            }

        //            // $("#success").find("p").text("Color updated successfully");
        //            $("#gridColor").data("kendoGrid").dataSource.data([]);
        //            $("#gridColor").data("kendoGrid").dataSource.read();
        //            $("#gridColor").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
        //        }
        //    }, {
        //        model: model

        //    }, true);
        //}
    });



    //Recipe part
    //-------

    //array type id class  step1 multiple dics ids

    $('#inputquantity').val(10);
    $('#inputquantity0').val(10);


    //kendo.data.DataSource.prototype.dataFiltered = function () {
    //    // Gets the filter from the dataSource
    //    var filters = this.filter();

    //    // Gets the full set of data from the data source
    //    var allData = this.data();

    //    // Applies the filter to the data
    //    var query = new kendo.data.Query(allData);

    //    // Returns the filtered data
    //    return query.filter(filters).data;
    //}

    function loadFormData() {
        //HttpClient.MakeRequest(CookBookMasters.GetApplicationSettingDataList, function (result) {

        //    if (result != null) {
        //        applicationSettingData = result;
        //        for (item of applicationSettingData) {
        //            if (item.Code == "FLX-00004")//item.Name == "Major Identification Threshold" || 
        //                MajorPercentData = item.Value;
        //            $(".asPercent").text(MajorPercentData);
        //        }

        //    }
        //    else {

        //    }
        //}, null

        //    , false);       


        if (user.SectorNumber == "80") {
            $(".healthcareonly").show();

        }
        else {
            $(".healthcareonly").hide();
        }

        if (user.SectorNumber == "20" || user.SectorNumber == "10") {
            $(".rcHide").show();
        }
        else {
            $(".rcHide").hide();
        }

        if (user.SectorNumber == "20") {
            $(".mfgHide").hide();
        }
        else {
            $(".mfgHide").show();
        }

        //TEMP COMMENT KRISH 5PM
        HttpClient.MakeRequest(CookBookMasters.GetMOGDataList, function (data) {

            var dataSource = data;

            mogdata = [];
            mogdata.push({ "MOG_ID": "Select MOG", "MOGName": "Select MOG", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "MOG_Code": "", "IsActive": false });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].CostPerUOM == null) {
                    dataSource[i].CostPerUOM = 0;
                }
                if (dataSource[i].CostPerKG == null) {
                    dataSource[i].CostPerKG = 0;
                }
                if (dataSource[i].TotalCost == null) {
                    dataSource[i].TotalCost = 0;
                }
                if (dataSource[i].Alias != null && dataSource[i].Alias.trim() != "" && dataSource[i].Alias != dataSource[i].Name)
                    mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name + " (" + dataSource[i].Alias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive });
                else
                    mogdata.push({ "MOG_ID": dataSource[i].ID, "MOGName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "MOG_Code": dataSource[i].MOGCode, "IsActive": dataSource[i].IsActive });
            }

            mogdataList = mogdata;

        }, null, true);

        HttpClient.MakeRequest(CookBookMasters.GetRecipeDataList, function (data) {

            var dataSource = data;

            baserecipedata = [];
            baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].CostPerUOM == null) {
                    dataSource[i].CostPerUOM = 0;
                }
                if (dataSource[i].CostPerKG == null) {
                    dataSource[i].CostPerKG = 0;
                }
                if (dataSource[i].TotalCost == null) {
                    dataSource[i].TotalCost = 0;
                }
                if (dataSource[i].Quantity == null) {
                    dataSource[i].Quantity = 0;
                }
                if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });
                else
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });

            }
            baserecipedatafiltered = baserecipedata;
            basedataList = baserecipedata;
        }, { condition: "Base", DishCode: "0" }, true);

        //HttpClient.MakeRequest(CookBookMasters.GetDishList, function (data) {

        //    var dataSource = data;
        //    dishdata = [];
        //    dishdata.push({ "value": "Select Dish", "text": "Select Dish" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        dishdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name });
        //    }
        //    console.log("dishdata")
        //    console.log(dishdata);
        //}, {
        //    siteCode: $("#hdnSiteCode").val()
        //}, true);

        //HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

        //    var dataSource = data;
        //    uomdata = [];
        //    uomdata.push({ "value": "Select", "text": "Select", "UOMCode": "" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        uomdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode });
        //    }
        //    populateDishDropdown();
        //}, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
            var dataSource = data;
            uommodulemappingdata = [];
            uommodulemappingdata.push({ "value": "Select", "text": "Select", "UOMCode": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].IsActive) {
                    uommodulemappingdata.push({
                        "value": dataSource[i].UOM_ID, "UOM_ID": dataSource[i].UOM_ID, "UOMName": dataSource[i].UOMName, "text": dataSource[i].UOMName,
                        "UOMModuleCode": dataSource[i].UOMModuleCode, "UOMCode": dataSource[i].UOMCode
                    });
                }
            }
            moguomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001');
            uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");
            recipecategoryuomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00006' || m.text == "Select");

            populateDishDropdown();

        }, null, false);

        //HttpClient.MakeSyncRequest(CookBookMasters.GetNutritionMasterDataList, function (data) {
        //    nutrientMasterdataSource = data.filter(m => m.IsActive);

        //}, null, true);


        basedata = [{ "value": "Select", "text": "Select" }, { "value": "No", "text": "Final Recipe" },
        { "value": "Yes", "text": "Base Recipe" }];

        //$("#gridBulkChange").css("display", "none");
        //populateBulkChangeControls();
        //var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        //changeControls.css("background-color", "#fff");
        //changeControls.css("border", "none");
        //changeControls.eq(2).text("");
        //changeControls.eq(2).append("<div id='uom' style='width:100%; font-size:10.5px!important'></div>");
        //changeControls.eq(3).text("");
        //changeControls.eq(3).append("<input id='quantity' style='width:90%; margin-left:3px; font-size:10.5px!important'></div>");
        //changeControls.eq(4).text("");
        //changeControls.eq(4).append("<div id='base' style='width:100%; font-size:10.5px!important'></div>");
    }


    //$("#InitiateBulkChanges").on("click", function () {
    //    $("#gridBulkChange").css("display", "block");
    //    $("#gridBulkChange").children(".k-grid-header").css("border", "none");
    //    $("#InitiateBulkChanges").css("display", "none");
    //    $("#CancelBulkChanges").css("display", "");
    //    $("#SaveBulkChanges").css("display", "");
    //    $("#bulkerror").css("display", "none");
    //    $("#success").css("display", "none");
    //    populateUOMDropdown_Bulk();
    //    populateBaseDropdown_Bulk();
    //});



    //$("#CancelBulkChanges").on("click", function () {
    //    $("#InitiateBulkChanges").css("display", "");
    //    $("#CancelBulkChanges").css("display", "none");
    //    $("#SaveBulkChanges").css("display", "none");
    //    $("#gridBulkChange").css("display", "none");
    //    $("#bulkerror").css("display", "none");
    //    $("#success").css("display", "none");
    //    populateRecipeGrid();
    //});

    //$("#SaveBulkChanges").on("click", function () {

    //    var neVal = $(this).val();
    //    var dataFiltered = $("#gridRecipe").data("kendoGrid").dataSource.dataFiltered();
    //    var model = dataFiltered;
    //    var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
    //    var validChange = false;
    //    var uom = "";
    //    var quantity = "";
    //    var base = "";

    //    var uomspan = $("#uom");
    //    var quantityspan = $("#quantity");
    //    var basespan = $("#base");

    //    if (uomspan.val() != "Select") {
    //        validChange = true;
    //        uom = uomspan.val();
    //    }
    //    if (quantityspan.val() != "") {
    //        validChange = true;
    //        quantity = quantityspan.val();
    //    }

    //    if (basespan.val() != "Select") {
    //        validChange = true;
    //        base = basespan.val();
    //    }
    //    if (validChange == false) {
    //        toastr.error("Please change at least one attribute for Bulk Update.");
    //    } else {
    //        $("#bulkerror").css("display", "none");
    //        Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
    //            function () {
    //                $.each(model, function () {
    //                    this.CreatedOn = kendo.parseDate(this.CreatedOn);
    //                    this.ModifiedBy = user.UserId;
    //                    this.ModifiedOn = Utility.CurrentDate();
    //                    if (uom != "")
    //                        this.UOM_ID = uom;
    //                    if (quantity != "")
    //                        this.Quantity = quantity;
    //                    if (base != "")
    //                        if (base == "Yes")
    //                            this.IsBase = true;
    //                        else
    //                            this.IsBase = false;
    //                });

    //                HttpClient.MakeRequest(CookBookMasters.SaveRecipeDataList, function (result) {
    //                    if (result == false) {
    //                        toastr.error("Some error occured, please try again.");
    //                    }
    //                    else {
    //                        toastr.success("Records have been updated successfully.");
    //                        $("#gridRecipe").data("kendoGrid").dataSource.data([]);
    //                        $("#gridRecipe").data("kendoGrid").dataSource.read();
    //                        $("#InitiateBulkChanges").css("display", "");
    //                        $("#CancelBulkChanges").css("display", "none");
    //                        $("#SaveBulkChanges").css("display", "none");
    //                        $("#gridBulkChange").css("display", "none");
    //                        setTimeout(fade_out, 10000);

    //                        function fade_out() {
    //                            $("#success").fadeOut();
    //                        }
    //                    }
    //                }, {
    //                    model: model

    //                }, true);

    //                //populateSiteGrid();
    //            },
    //            function () {
    //                maptype.focus();
    //            }
    //        );
    //    }
    //    Utility.UnLoading();
    //});
    //Food Program Section Start
    $('#myInputAPL').on('input', function (e) {
        var grid = $('#gridMOGWiseAPL').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ArticleNumber" || x.field == "ArticleDescription" || x.field == "UOM" || x.field == "ArticleType"
                    || x.field == "Hierlevel3" || x.field == "MerchandizeCategoryDesc" || x.field == "MOGName" || x.field == "StandardCostPerKg" || x.field == "ArticleCost") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                    else if (type == 'decimal') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });


    //$('#myInput').on('input', function (e) {
    //    var grid = $('#gridRecipe').data('kendoGrid');
    //    var columns = grid.columns;

    //    var filter = { logic: 'or', filters: [] };
    //    columns.forEach(function (x) {
    //        if (x.field) {
    //            if (x.field == "RecipeAlias" || x.field == "RecipeType" || x.field == "PublishedDateAsString" || x.field == "RecipeCode" || x.field == "Name"
    //                || x.field == "UOMName" || x.field == "Quantity" || x.field == "IsBase" || x.field == "Status" || x.field == "IsActive" || x.field == "IsElementoryString") {
    //                var type = grid.dataSource.options.schema.model.fields[x.field].type;
    //                if (type == 'string') {
    //                    var targetValue = e.target.value;
    //                    if (x.field == "Status") {
    //                        var pendingString = "pending";
    //                        var savedString = "saved";
    //                        var mappedString = "published";
    //                        if (pendingString.includes(e.target.value.toLowerCase())) {
    //                            targetValue = "1";
    //                        }
    //                        else if (savedString.includes(e.target.value.toLowerCase())) {
    //                            targetValue = "2";
    //                        }
    //                        else if (mappedString.includes(e.target.value.toLowerCase())) {
    //                            targetValue = "3";
    //                        }
    //                    }

    //                    filter.filters.push({
    //                        field: x.field,
    //                        operator: 'contains',
    //                        value: targetValue
    //                    })
    //                }
    //                else if (type == 'number') {
    //                    if (isNumeric(e.target.value)) {
    //                        filter.filters.push({
    //                            field: x.field,
    //                            operator: 'eq',
    //                            value: e.target.value
    //                        });
    //                    }
    //                }
    //                else if (type == 'date') {
    //                    var data = grid.dataSource.data();
    //                    for (var i = 0; i < data.length; i++) {
    //                        var dateStr = kendo.format(x.format, data[i][x.field]);
    //                        if (dateStr.startsWith(e.target.value)) {
    //                            filter.filters.push({
    //                                field: x.field,
    //                                operator: 'eq',
    //                                value: data[i][x.field]
    //                            })
    //                        }
    //                    }
    //                } else if (type == 'boolean' && e.target.value !== null) {
    //                    // var bool = getBoolean(e.target.value);
    //                    filter.filters.push({
    //                        field: x.field,
    //                        operator: 'eq',
    //                        value: e.target.value
    //                    });
    //                }
    //            }
    //        }
    //    });
    //    grid.dataSource.filter(filter);
    //});


    $("#addbr").on("click", function () {

        $("#gridBaseRecipe").css("display", "block");
        $("#emptybr").css("display", "none");
        $("#gridBaseRecipe").data("kendoGrid").addRow();
    })

    $("#addmog").on("click", function () {
        $("#gridMOG").css("display", "block");
        $("#emptymog").css("display", "none");
        $("#gridMOG").data("kendoGrid").addRow();
    })

    $("#addbrx").on("click", function () {

        $("#gridBaseRecipex").css("display", "block");
        $("#emptybrx").css("display", "none");
        $("#gridBaseRecipex").data("kendoGrid").addRow();
    })

    $("#addbr0").on("click", function () {

        $("#gridBaseRecipe0").css("display", "block");
        $("#emptybr0").css("display", "none");
        $("#gridBaseRecipe0").data("kendoGrid").addRow();
    })


    //$("#addbr1").on("click", function () {
    //    return;
    //    $("#gridBaseRecipe1").css("display", "block");
    //    $("#emptybr1").css("display", "none");
    //    $("#gridBaseRecipe1").data("kendoGrid").addRow();
    //})
    //$("#addbr2").on("click", function () {
    //    return;
    //    $("#gridBaseRecipe2").css("display", "block");
    //    $("#emptybr2").css("display", "none");
    //    $("#gridBaseRecipe2").data("kendoGrid").addRow();
    //})
    //$("#addbr3").on("click", function () {
    //    return;
    //    $("#gridBaseRecipe3").css("display", "block");
    //    $("#emptybr3").css("display", "none");
    //    $("#gridBaseRecipe3").data("kendoGrid").addRow();
    //})
    //$("#addbr4").on("click", function () {
    //    return;
    //    $("#gridBaseRecipe4").css("display", "block");
    //    $("#emptybr4").css("display", "none");
    //    $("#gridBaseRecipe4").data("kendoGrid").addRow();
    //})
    //$("#addbr5").on("click", function () {
    //    return;
    //    $("#gridBaseRecipe5").css("display", "block");
    //    $("#emptybr5").css("display", "none");
    //    $("#gridBaseRecipe5").data("kendoGrid").addRow();
    //})
    $("#addmogx").on("click", function () {

        $("#gridMOGx").css("display", "block");
        $("#emptymogx").css("display", "none");
        $("#gridMOGx").data("kendoGrid").addRow();
    })
    $("#addmog0").on("click", function () {

        $("#gridMOG0").css("display", "block");
        $("#emptymog0").css("display", "none");
        $("#gridMOG0").data("kendoGrid").addRow();
    })
    //$("#addmog1").on("click", function () {
    //    return;
    //    $("#gridMOG1").css("display", "block");
    //    $("#emptymog1").css("display", "none");
    //    $("#gridMOG1").data("kendoGrid").addRow();
    //})
    //$("#addmog2").on("click", function () {
    //    return;
    //    $("#gridMOG2").css("display", "block");
    //    $("#emptymog2").css("display", "none");
    //    $("#gridMOG2").data("kendoGrid").addRow();
    //})
    //$("#addmog3").on("click", function () {
    //    return;
    //    $("#gridMOG3").css("display", "block");
    //    $("#emptymog3").css("display", "none");
    //    $("#gridMOG3").data("kendoGrid").addRow();
    //})
    //$("#addmog4").on("click", function () {
    //    return;
    //    $("#gridMOG4").css("display", "block");
    //    $("#emptymog4").css("display", "none");
    //    $("#gridMOG4").data("kendoGrid").addRow();
    //})
    //$("#addmog5").on("click", function () {
    //    return;
    //    $("#gridMOG5").css("display", "block");
    //    $("#emptymog5").css("display", "none");
    //    $("#gridMOG5").data("kendoGrid").addRow();
    //})
    //function populateBulkChangeControls() {

    //    var gridVariable = $("#gridBulkChange");
    //    gridVariable.html("");
    //    gridVariable.kendoGrid({
    //        //selectable: "cell",
    //        sortable: false,
    //        filterable: false,
    //        //reorderable: true,
    //        //scrollable: true,
    //        columns: [
    //            {
    //                field: "RecipeCode", title: "Recipe Code", width: "40px", attributes: {
    //                    style: "text-align: left; font-weight:normal"
    //                }
    //            },
    //            {
    //                field: "Name", title: "Recipe Name", width: "100px", attributes: {
    //                    style: "text-align: left; font-weight:normal"
    //                }
    //            },
    //            {
    //                field: "UOMName", title: "UOM", width: "60px", attributes: {

    //                    style: "text-align: left; font-weight:normal"
    //                },
    //            },
    //            {
    //                field: "Quantity", title: "Quantity", width: "35px", attributes: {

    //                    style: "text-align: center; font-weight:normal"
    //                },

    //            },
    //            {
    //                field: "IsBase", title: "Base Recipe", width: "80px", attributes: {

    //                    style: "text-align: center; font-weight:normal"
    //                },
    //            },
    //            {
    //                field: "", title: "", width: "50px", attributes: {

    //                    style: "text-align: center; font-weight:normal"
    //                },
    //                headerAttributes: {

    //                    style: "text-align: center"
    //                },
    //            },
    //            {
    //                field: "", title: "", width: "30px", attributes: {

    //                    style: "text-align: center; font-weight:normal"
    //                },
    //                headerAttributes: {

    //                    style: "text-align: center"
    //                },
    //            },

    //            {
    //                field: "", title: "", width: "20px",
    //                attributes: {
    //                    style: "text-align: left; font-weight:normal"
    //                },
    //            }
    //        ],
    //        columnResize: function (e) {

    //            e.preventDefault();
    //        },
    //        dataBound: function (e) {
    //        },
    //        change: function (e) {
    //        },
    //    });
    //}

    $("#AddNewRecipe").on("click", function () {
        HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {

            var dataSource = data;

            baserecipedata = [];
            baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].CostPerUOM == null) {
                    dataSource[i].CostPerUOM = 0;
                }
                if (dataSource[i].CostPerKG == null) {
                    dataSource[i].CostPerKG = 0;
                }
                if (dataSource[i].TotalCost == null) {
                    dataSource[i].TotalCost = 0;
                }
                if (dataSource[i].Quantity == null) {
                    dataSource[i].Quantity = 0;
                }
                if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });
                else
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });

            }
            baserecipedatafiltered = baserecipedata;
            basedataList = baserecipedata;
        }, { condition: "Base", DishCode: "0" }, true);
        $("#gridBaseRecipe0").css("display", "None");
        $("#gridMOG0").css("display", "None");
        $("#success").css("display", "none");
        $("#error0").css("display", "none");

        var editor = $("#inputinstruction0").data("kendoEditor");
        editor.value('');
        var model;

        //$(".k-overlay").css("display", "block");
        // $(".k-overlay").css("opacity", "0.5");

        //$("#windowEdit0").css("display", "block");

        populateUOMDropdown0();
        if (user.SectorNumber == "20" || user.SectorNumber == "10") {

            populateRecipeCategoryUOMDropdown0();
            populateWtPerUnitUOMDropdown0();
            populateGravyWtPerUnitUOMDropdown0();
            //populateRecipeCategoryUOMDropdown("0")
            $("#inputwtperunituom0").closest(".k-widget").hide();
            $("#inputgravywtperunituom0").closest(".k-widget").hide();
            $("#inputuom0").closest(".k-widget").hide();
        }

        populateBaseDropdown0();
        populateBaseRecipeGrid0();
        populateMOGGrid0(); inputuom0

        //$("#mainContent").hide();
        //$("#windowEdit0").show();
        $("#windowEdit0").show();
        $("#windowEdit").hide();

        datamodel = model;
        $("#inputinstruction0").val("");
        $("#recipeid0").text("");
        $("#inputrecipename0").val("");
        $("#inputrecipealiasname0").val("");
        if (user.SectorNumber == "20" || user.SectorNumber == "10") {
            $("#inputwtperunit0").val("");
            $("#inputgravywtperunit0").val("");
            $("#inputuom0").data("kendoDropDownList").value("Select");
            $("#inputrecipecum0").data("kendoDropDownList").value("Select");
            $("#inputwtperunituom0").data("kendoDropDownList").value("Select");
            $("#inputgravywtperunituom0").data("kendoDropDownList").value("Select");
            $("#inputbase0").data('kendoDropDownList').value("No");
            var recipeCategory = "";
            var rCatData = $("#inputrecipecum0").data('kendoDropDownList')
            if (rCatData != null && rCatData != "Select" && rCatData != "" && rCatData.dataItem() != null && rCatData.dataItem().UOMCode != "Select") {
                recipeCategory = rCatData.dataItem().UOMCode
            }
            OnRecipeUOMCategoryChange(recipeCategory, "0");
        }
        else {
            var inputwtperunituom0 = $("#inputuom0").data("kendoDropDownList");
            inputwtperunituom0.enable(false);
        }

        $("#inputquantity0").val("10");

        // $("#gridBaseRecipe0").data("kendoGrid").addRow();
        // $("#gridMOG0").data("kendoGrid").addRow();
        // hideGrid('gridBaseRecipe0', 'emptybr0', 'baseRecipePlus0', 'baseRecipeMinus0');
        // hideGrid('gridMOG0', 'emptymog0', 'mogPlus0', 'mogMinus0');
        $("#gridMOG").css("display", "none");
        $("#emptymog0").css("display", "block");
        $("#gridBaseRecipe").css("display", "none");
        $("#emptybr0").css("display", "block");
        if (user.SectorNumber == "20" || user.SectorNumber == "10") {
            $('#inputquantity0').removeAttr("disabled");
        }
        else {
            $('#inputquantity0').attr('disabled', 'disabled');
        }
        //Recipe fetch by dish
        var dishCode = $("#hdnDishCode").val();
        //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (result) {
        //    Utility.UnLoading();
        //    if (result != null) {
        //        copyData = result;
        //        if (isShowElmentoryRecipe) {
        //            result = result.filter(m => m.IsElementory);
        //        }
        //        //options.success(result);
        //        //copyRecipe
        //copyData.unshift({ "ID": 0, "RecipeCode": 0, "Name": "Select Recipe" });
        populateRecipeCopyDropDown();

        //    }
        //    else {
        //        //options.success("");
        //    }
        //},
        //    {
        //        condition: "All",
        //        DishCode: dishCode
        //    }
        //    , true);

        Utility.UnLoading();
    })

    $("#btnCancel0").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                $("#emptybr0").css("display", "block");

                $("#windowEdit").show();
                $("#windowEdit0").hide();
                $("#gridMOG0").html("");
                $("#gridMOG0").hide();
                $("#emptymog0").show();
                $("#inputrecipename0").val("");
                $("#inputrecipealiasname0").val("");
                $("#inputquantity0").val("10");
                if ($("#inputrecipecum0").data("kendoDropDownList") != undefined)
                    $("#inputrecipecum0").data("kendoDropDownList").value("Select");
                $("#copyRecipe").data("kendoDropDownList").value("Select");

                //$("#gridBaseRecipe0").css("display", "none");
                //$("#mainContent").show();
                //$("#windowEdit0").hide();
                //$("#gridRecipe").data("kendoGrid").dataSource.data([]);
                //$("#gridRecipe").data("kendoGrid").dataSource.read();
            },
            function () {
            }
        );
    });

    function backFromAddNewRecipe() {
        $(".k-overlay").hide();
        $("#emptybr0").css("display", "block");

        $("#windowEdit").show();
        $("#windowEdit0").hide();
        $("#gridMOG0").html("");
        $("#gridMOG0").hide();
        $("#emptymog0").show();
        $("#inputrecipename0").val("");
        $("#inputrecipealiasname0").val("");
        $("#inputquantity0").val("10");
        if ($("#inputrecipecum0").data("kendoDropDownList") != undefined)
        $("#inputrecipecum0").data("kendoDropDownList").value("Select");
        $("#copyRecipe").data("kendoDropDownList").value("Select");

        //$("#gridBaseRecipe0").css("display", "none");
        //$("#mainContent").show();
        //$("#windowEdit0").hide();
        //$("#gridRecipe").data("kendoGrid").dataSource.data([]);
        //$("#gridRecipe").data("kendoGrid").dataSource.read();
    }

    $("#btnCancelx").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEditx").data("kendoWindow");
                $("#emptybrx").css("display", "block");
                $("#gridBaseRecipex").css("display", "none");
                orderWindow.close();
            },
            function () {
            }
        );
    });


    $("#btnCancel1").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit1").data("kendoWindow");
        $("#emptybr1").css("display", "block");
        $("#gridBaseRecipe1").css("display", "none");
        orderWindow.close();


    });

    $("#btnNutritionData").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditNutritionInfo").data("kendoWindow");
        orderWindow.open();

    });


    $("#btnCancel2").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit2").data("kendoWindow");
        $("#emptybr2").css("display", "block");
        $("#gridBaseRecipe2").css("display", "none");
        orderWindow.close();
        $("#windowEdit1").parent().show();
    });
    $("#btnCancel3").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit3").data("kendoWindow");
        $("#emptybr3").css("display", "block");
        $("#gridBaseRecipe3").css("display", "none");
        orderWindow.close();
        $("#windowEdit2").parent().show();
    });

    $("#btnCancel4").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit4").data("kendoWindow");
        $("#emptybr4").css("display", "block");
        $("#gridBaseRecipe4").css("display", "none");
        orderWindow.close();
        $("#windowEdit3").parent().show();
    });
    $("#btnCancel5").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit5").data("kendoWindow");
        $("#emptybr5").css("display", "block");
        $("#gridBaseRecipe5").css("display", "none");
        orderWindow.close();
        $("#windowEdit4").parent().show();
    });
    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();
        // var orderWindow = $("#windowEdit1").data("kendoWindow");
        // "none");
        // orderWindow.close();
    });
    //$("#btnCancel").on("click", function () {
    //    $(".k-overlay").hide();
    //    var orderWindow = $("#windowEdit").data("kendoWindow");
    //    orderWindow.close();
    //});
    $("#btnSubmit0").click(function () {
        isPublishBtnClick = false;
        Utility.Loading();
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie
        if (!validateDataBeforeSave("gridBaseRecipe0", 'gridMOG0')) {
            toastr.error("Invalid Data!");
            Utility.UnLoading();
            return false;
        }
        if ($("#inputquantity").val() == null || $("#inputquantity").val() == "" || $("#inputquantity").val() == undefined) {
            toastr.error("Please fill in all Mandatory Fields");
            Utility.UnLoading();
            return false;
        }

        if (user.SectorNumber == "20" || user.SectorNumber == "10") {

            if ($("#inputrecipecum0").val() == null || $("#inputrecipecum0").val() == "" || $("#inputrecipecum0").val() == undefined || $("#inputrecipecum0").val() == "Select") {
                toastr.error("Please fill in all Mandatory Fields");
                Utility.UnLoading();
                return false;
            }
        }

        if ($("#inputuom0").val() == null || $("#inputuom0").val() == "" || $("#inputuom0").val() == undefined || $("#inputuom0").val() == "Select") {
            toastr.error("Please fill in all Mandatory Fields");
            Utility.UnLoading();
            return false;
        }
        if ($("#inputrecipename0").val().trim().length == 0) {
            toastr.error("Please fill in all Mandatory Fields");
            $("#inputrecipename0").focus();
            Utility.UnLoading();
            return false;
        }


        var base;
        if ($("#inputbase0").val() == "Yes")
            base = true
        else if ($("#inputbase0").val() == "No")
            base = false
        else {
            toastr.error("Select Recipe Type!");
            Utility.UnLoading();
            return false;
        }
        //if ($("#inputrecipename0").val() == "") {
        //    toastr.error("Select provide Recipe Name");
        //    $("#inputrecipename0").focus();
        //    Utility.UnLoading();
        //    return false;
        //}
        var brgrid = $("#gridBaseRecipe0").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= MajorPercentData) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode").val(),
                "Recipe_ID": $("#recipeid").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy,
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG0").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= MajorPercentData) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode0").val(),
                "Recipe_ID": $("#recipeid0").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie

        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom0").val();
        });

        var model = {
            "RecipeCode": $("#inputrecipecode0").val(),
            "ID": $("#recipeid0").val(),
            "Name": titleCase($("#inputrecipename0").val()),
            "RecipeAlias": titleCase($("#inputrecipealiasname0").val()),
            "UOMCode": objUOM[0].UOMCode,
            "UOM_ID": $("#inputuom0").val(),
            "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum0").val() : "",
            "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit0").val() : "",
            "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom0").val() : "",
            "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit0").val() : "",
            "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom0").val() : "",
            "TotalIngredientWeight": $("#ToTIngredients").text(),
            "Quantity": $("#inputquantity0").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG0").text().substring(1).replace(",", ""),
            "CostPerUOM": $("#grandCostPerKG0").text().substring(1).replace(",", ""),
            "TotalCost": $('#grandTotal0').text().substring(1).replace(",", ""),
            "Quantity": $('#inputquantity0').val(),
            "Instructions": $("#inputinstruction0").val(),
            "IsActive": 1,
            "Status": isPublishBtnClick ? 3 : 2,
            "CreatedOn": (datamodel != undefined && datamodel != null && datamodel.CreatedOn != undefined) ? kendo.parseDate(datamodel.CreatedOn) : Utility.CurrentDate(),
            "CreatedBy": (datamodel != undefined && datamodel != null && datamodel.CreatedBy != undefined) ? datamodel.CreatedBy : user.UserId,
            "Site_ID": $("#NewSimSiteByRegion").val(),
            "Region_ID": $("#NewSimRegion").val(),
            "SiteCode": $("#hdnSiteCode").val(),
            "DishID": $("#hdnDishId").val(),
            "DishCode": $("#hdnDishCode").val()
        }
        var rmodel = {
            "RecipeCode": $("#inputrecipecode0").val(),
            "ID": $("#recipeid0").val(),
            "Name": $("#inputrecipename0").val(),
            "RecipeAlias": $("#inputrecipealiasname0").val(),
            "OldRecipeName": RecipeName,
            "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum0").val() : "",
            "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit0").val() : "",
            "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom0").val() : "",
            "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit0").val() : "",
            "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom0").val() : "",
            "TotalIngredientWeight": $("#ToTIngredients").text(),
            "UOM_ID": $("#inputuom0").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity0").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG0").text().substring(1).replace(",", ""),
            "CostPerUOM": $("#grandCostPerKG0").text().substring(1).replace(",", ""),
            "TotalCost": $('#grandTotal0').text().substring(1).replace(",", ""),
            "Quantity": $('#inputquantity0').val(),
            "Instructions": $("#inputinstruction0").val(),
            "IsActive": 1,
            "IsRecipeNameChange": isBRNameChange,
            "IsRecipeStatusChange": isStatusSave,
            "Status": isPublishBtnClick ? 3 : 2,
            "CreatedOn": (datamodel != undefined && datamodel != null && datamodel.CreatedOn != undefined) ? kendo.parseDate(datamodel.CreatedOn) : Utility.CurrentDate(),
            "CreatedBy": (datamodel != undefined && datamodel != null && datamodel.CreatedBy != undefined) ? datamodel.CreatedBy : user.UserId
        }

        if ($("#inputrecipename0").val() === "") {
            // $("#error").css("display", "flex");
            //    $("#error").find("p").text("Please provide valid input(s)");
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename0").focus();
            Utility.UnLoading();
        }
        else {
            // $("#error").css("display", "none");
            var base;
            if ($("#inputbase0").val() == "Yes")
                base = true
            else if ($("#inputbase0").val() == "No")
                base = false
            else {
                toastr.error("Select Recipe Type!");
                Utility.UnLoading();
                return false;
            }

            //$("#btnSubmit0").attr('disabled', 'disabled');
            //var chkNulkl = recipeCustomData.filter(a => a.Name == null)[0];
            //console.log("chkNulkl")
            //console.log(chkNulkl)
            var chkDup = recipeCustomData.filter(a => a.Name != null && a.Name.toLowerCase() == $("#inputrecipename0").val().toLowerCase());
            if (chkDup.length > 0) {
                $('#btnSubmit0').removeAttr("disabled");
                toastr.error("Recipe Name already exits , kindly check");
                $("#inputrecipename").focus();
                Utility.UnLoading();
            }
            //else {
            //    var contextData = JSON.parse(localStorage.getItem("contextitem"));

            //    console.log(contextData);
            //    console.log("contextData");
            //    var dishCode = $("#hdnDishCode").val();
            //    var itemId = $("#hdnItemId").val();
            //    var dpCode = $("#hdnDayPartCode").val();
            //    //var noOfDays = $("#NewSimNoOfDays").val();
            //    for (var i = 0; i < contextData.length; i++) {
            //        for (var j = 0; j < contextData[i].DishCategories.length; j++) {
            //            for (var k = 0; k < contextData[i].DishCategories[j].Dishes.length; k++) {
            //                //var currentDishData = contextData[i].DishCategories[j].Dishes[k];
            //                //var currentDcData = contextData[i].DishCategories[j];
            //                //var currentItem = contextData[i];
            //                //var itemidvalue = currentItem.value.split('_')[0];
            //                //var dataO = [];
            //                //for (var l = 1; l <= noOfDays; l++) {
            //                //    var paxTxt = "#txtPax_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
            //                //    var grmTxt = ".txtGrmroot_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
            //                //    var costTxt = "#lblCost_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;

            //                //    dataO.push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });
            //                //    //contextData[i].DishCategories[j].Dishes[k].push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });

            //                //}
            //                if (contextData[i].DishCategories[j].Dishes[k].DishCode == dishCode && contextData[i].DishCategories[j].ItemID == itemId && contextData[i].DishCategories[j].DayPartCode == dpCode) {
            //                    contextData[i].DishCategories[j].Dishes[k].recipeDetails = model;
            //                    contextData[i].DishCategories[j].Dishes[k].baseRecipes = baseRecipes;
            //                    contextData[i].DishCategories[j].Dishes[k].mogs = mogs;
            //                }
            //            }
            //        }

            //    }
            //    console.log("data")
            //    console.log(contextData)

            //    //TODO--

            //    //backFromAddNewRecipe();
            //    //Toast.fire({
            //    //    type: 'success',
            //    //    title: 'Recipe data saved'
            //    //});

            //}
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit0').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputrecipename0").focus();
                    Utility.UnLoading();
                }
                else if (result == "Duplicate") {
                    $('#btnSubmit0').removeAttr("disabled");
                    toastr.error("Recipe Name already exits");
                    $("#inputrecipename").focus();
                    Utility.UnLoading();
                }
                else {
                    $(".k-overlay").hide();
                    $('#btnSubmit0').removeAttr("disabled");
                    Toast.fire({
                        type: 'success',
                        title: 'Recipe data saved'
                    });
                    if (model.ID == "") {
                        //$("#gridRecipe").data("kendoGrid").dataSource.data([]);
                        //$("#gridRecipe").data("kendoGrid").dataSource.read();
                        //onSaveRecipeSetData();
                    }
                    backFromAddNewRecipe();
                    var dishid = $("#hdnDishId").val();
                    var itemid = $("#hdnItemId").val();
                    var dishcategorycode = $("#hdnDishCatCode").val();
                    var dishcode = $("#hdnDishCode").val();
                    var dpcode = $("#hdnDayPartCode").val();
                    openRecipe(itemid, dishid, dishcategorycode, dishcode, dpcode);

                    Utility.UnLoading();
                }
            }, {
                rmodel: rmodel,
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs,
                simCode: $("#SimulationCode").val()
            }, true);
        }
    });
    $("#btnPublish0").click(function () {

        HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {

            var dataSource = data;

            baserecipedata = [];
            baserecipedata.push({ "BaseRecipe_ID": "Select Base Recipe", "BaseRecipeName": "Select Base Recipe", "UOM_ID": "-1", "UOM_NAME": "Select", "CostPerUOM": 0, "CostPerKG": 0, "Quantity": 0, "TotalCost": 0, "BaseRecipe_Code": "" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].CostPerUOM == null) {
                    dataSource[i].CostPerUOM = 0;
                }
                if (dataSource[i].CostPerKG == null) {
                    dataSource[i].CostPerKG = 0;
                }
                if (dataSource[i].TotalCost == null) {
                    dataSource[i].TotalCost = 0;
                }
                if (dataSource[i].Quantity == null) {
                    dataSource[i].Quantity = 0;
                }
                if (dataSource[i].RecipeAlias != null && dataSource[i].RecipeAlias.trim() != "" && dataSource[i].RecipeAlias != dataSource[i].Name)
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name + " (" + dataSource[i].RecipeAlias + ") ", "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });
                else
                    baserecipedata.push({
                        "BaseRecipe_ID": dataSource[i].ID, "BaseRecipeName": dataSource[i].Name, "UOM_Code": dataSource[i].UOMCode, "UOM_ID": dataSource[i].UOM_ID, "UOM_NAME": dataSource[i].UOMName, "CostPerUOM": dataSource[i].CostPerUOM, "CostPerKG": dataSource[i].CostPerKG, "Quantity": dataSource[i].Quantity, "TotalCost": dataSource[i].TotalCost, "BaseRecipe_Code": dataSource[i].RecipeCode
                    });

            }
            baserecipedatafiltered = baserecipedata;
            basedataList = baserecipedata;
        }, { condition: "Base", DishCode: "0" }, true);
        isPublishBtnClick = true;
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie
        if (!validateDataBeforeSave("gridBaseRecipe0", 'gridMOG0')) {
            toastr.error("Invalid Data!");
            Utility.UnLoading();
            return false;
        }
        var brgrid = $("#gridBaseRecipe0").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= MajorPercentData) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode").val(),
                "Recipe_ID": $("#recipeid").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG0").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode0").val(),
                "Recipe_ID": $("#recipeid0").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1,
                "Status": isPublishBtnClick ? 3 : 2,
                "CreatedOn": kendo.parseDate(data[i].CreatedOn),
                "CreatedBy": data[i].CreatedBy
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputrecipename0").val() === "") {
            // $("#error").css("display", "flex");
            //    $("#error").find("p").text("Please provide valid input(s)");
            toastr.error("Please provide Recipe Name");
            $("#inputrecipename0").focus();
            Utility.UnLoading();
            return;
        }
        if (user.SectorNumber == "20" || user.SectorNumber == "10") {

            if ($("#inputrecipecum0").val() == null || $("#inputrecipecum0").val() == "" || $("#inputrecipecum0").val() == undefined || $("#inputrecipecum0").val() == "Select") {
                toastr.error("Please select Recipe UOM Category");
                Utility.UnLoading();
                return false;
            }
        }

        if ($("#inputuom0").val() == null || $("#inputuom0").val() == "" || $("#inputuom0").val() == undefined || $("#inputuom0").val() == "Select") {
            toastr.error("Please select UOM");
            Utility.UnLoading();
            return false;
        }
        if ($("#inputbase0").val() == "Yes")
            base = true
        else if ($("#inputbase0").val() == "No")
            base = false
        else {
            toastr.error("Select Recipe Type");
            Utility.UnLoading();
            return false;
        }
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom0").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode0").val(),
            "ID": $("#recipeid0").val(),
            "Name": titleCase($("#inputrecipename0").val()),
            "RecipeAlias": titleCase($("#inputrecipealiasname0").val()),
            "UOM_ID": $("#inputuom0").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity0").val(),
            "IsBase": base,
            "RecipeUOMCategory": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputrecipecum0").val() : "",
            "WeightPerUnit": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunit0").val() : "",
            "WeightPerUnitUOM": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputwtperunituom0").val() : "",
            "WeightPerUnitGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunit0").val() : "",
            "WeightPerUnitUOMGravy": user.SectorNumber == "20" || user.SectorNumber == "10" ? $("#inputgravywtperunituom0").val() : "",
            "TotalIngredientWeight": $("#ToTIngredients").text(),
            "CostPerKG": $("#grandCostPerKG0").text().substring(1).replace(",", ""),
            "CostPerUOM": $("#grandCostPerKG0").text().substring(1).replace(",", ""),
            "TotalCost": $('#grandTotal0').text().substring(1).replace(",", ""),
            "Quantity": $('#inputquantity0').val(),
            "Instructions": $("#inputinstruction0").val(),
            "IsActive": 1,
            "Status": isPublishBtnClick ? 3 : 2,
            "CreatedOn": (datamodel != undefined && datamodel != null && datamodel.CreatedOn != undefined) ? kendo.parseDate(datamodel.CreatedOn) : Utility.CurrentDate(),
            "CreatedBy": (datamodel != undefined && datamodel != null && datamodel.CreatedBy != undefined) ? datamodel.CreatedBy : user.UserId
        }

        if ($("#inputrecipename0").val() === "") {
            toastr.error("Please provide Recipe Name");
            $("#inputrecipename0").focus();
            return;
        }
        else {
            // $("#error").css("display", "none");
            var base;
            if ($("#inputbase0").val() == "Yes")
                base = true
            else if ($("#inputbase0").val() == "No")
                base = false
            else {
                toastr.error("Please select Recipe Type");
                $("#inputbase0").focus();
                return;
            }

            $("#btnPublish0").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result == false) {
                    $('#btnPublish0').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputrecipename0").focus();
                } else if (result == "Duplicate") {
                    $('#btnSubmit0').removeAttr("disabled");
                    toastr.error("Recipe Name already exits , kindly check");
                    $("#inputrecipename").focus();
                }
                else {
                    $(".k-overlay").hide();
                    $('#btnPublish0').removeAttr("disabled");
                    toastr.success("Recipe data published successfully");
                    $(".k-overlay").hide();
                    $("#emptybr0").css("display", "block");
                    $("#gridBaseRecipe0").css("display", "none");
                    $("#mainContent").show();
                    $("#windowEdit0").hide();
                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();




                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }


    });

    $("#btnPublish1").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        if (!validateDataBeforeSave("gridBaseRecipe1", 'gridMOG1')) {
            toastr.error("Invalid Data!");
            Utility.UnLoading();
            return false;
        }
        var brgrid = $("#gridBaseRecipe1").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode1").val(),
                "Recipe_ID": $("#recipeid1").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG1").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode1").val(),
                "Recipe_ID": $("#recipeid1").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase1").val() == "Yes")
            base = true
        else if ($("#inputbase1").val() == "No")
            base = false
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom1").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode1").val(),
            "ID": $("#recipeid1").val(),
            "Name": titleCase($("#inputrecipename1").val()),
            "UOM_ID": $("#inputuom1").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity1").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG1").text(),
            "CostPerUOM": $("#grandCostPerKG1").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#inputquantity1').val(),
            "Instructions": $("#inputinstruction1").val(),
            "IsActive": 1
        }

        if ($("#inputrecipename1").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename1").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase1").val() == "Yes")
                base = true
            else if ($("#inputbase1").val() == "No")
                base = false


            $("#btnPublish1").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result == false) {
                    $('#btnPublish1').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputrecipename1").focus();
                    Utility.UnLoading();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#windowEdit1").data("kendoWindow");
                    orderWindow.close();
                    $('#btnPublish1').removeAttr("disabled");

                    toastr.success("Recipe data saved");
                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();
                    $(".k-overlay").hide();

                    orderWindow.close();
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });

    $("#btnSubmit1").click(function () {
        //Model Creation
        //insertion or updation in  two tables
        //Base recipie

        if (!validateDataBeforeSave("gridBaseRecipe1", 'gridMOG1')) {
            toastr.error("Invalid Data!");
            Utility.UnLoading();
            return false;
        }
        var brgrid = $("#gridBaseRecipe1").data("kendoGrid").dataSource;
        var data = brgrid._data;
        var baseRecipes = [];

        for (var i = 0; i < brgrid._data.length; i++) {
            var obj = {
                "RecipeCode": $("#inputrecipecode1").val(),
                "Recipe_ID": $("#recipeid1").val(),
                "BaseRecipe_ID": data[i].BaseRecipe_ID,
                "BaseRecipeCode": data[i].BaseRecipeCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IsActive": 1
            }
            baseRecipes.push(obj);
        }
        //MOG
        var mogGrid = $("#gridMOG1").data("kendoGrid").dataSource;
        data = mogGrid._data;
        var mogs = [];
        for (var i = 0; i < mogGrid._data.length; i++) {
            var iMajor = false;
            if (data[i].IngredientPerc >= 5) {
                iMajor = true;
            }
            var obj = {
                "RecipeCode": $("#inputrecipecode1").val(),
                "Recipe_ID": $("#recipeid1").val(),
                "MOG_ID": data[i].MOG_ID,
                "MOGCode": data[i].MOGCode,
                "UOMCode": data[i].UOMCode,
                "UOM_ID": data[i].UOM_ID,
                "CostPerUOM": data[i].CostPerUOM,
                "CostPerKG": data[i].CostPerKG,
                "TotalCost": data[i].TotalCost,
                "Quantity": data[i].Quantity,
                "IngredientPerc": data[i].IngredientPerc,
                "IsMajor": iMajor,
                "IsActive": 1
            }
            mogs.push(obj);
        }

        //Top Level Actual Recipie
        var base;
        if ($("#inputbase1").val() == "Yes")
            base = true
        else if ($("#inputbase1").val() == "No")
            base = false
        var objUOM = uomdata.filter(function (uomObj) {
            return uomObj.value == $("#inputuom1").val();
        });
        var model = {
            "RecipeCode": $("#inputrecipecode1").val(),
            "ID": $("#recipeid1").val(),
            "Name": titleCase($("#inputrecipename1").val()),
            "UOM_ID": $("#inputuom1").val(),
            "UOMCode": objUOM[0].UOMCode,
            "Quantity": $("#inputquantity1").val(),
            "IsBase": base,
            "CostPerKG": $("#grandCostPerKG1").text(),
            "CostPerUOM": $("#grandCostPerKG1").text(),
            "TotalCost": $('#grandTotal1').text(),
            "Quantity": $('#inputquantity1').val(),
            "Instructions": $("#inputinstruction1").val(),
            "IsActive": 1,
            "CreatedOn": kendo.parseDate(datamodel.CreatedOn),
            "CreatedBy": datamodel.CreatedBy
        }

        if ($("#inputrecipename1").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputrecipename1").focus();
            Utility.UnLoading();
        }
        else {
            var base;
            if ($("#inputbase1").val() == "Yes")
                base = true
            else if ($("#inputbase1").val() == "No")
                base = false


            $("#btnSubmit1").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveRecipeData, function (result) {
                if (result == false) {
                    $('#btnSubmit1').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputrecipename1").focus();
                    Utility.UnLoading();
                }
                else {
                    $(".k-overlay").hide();
                    //var orderWindow = $("#windowEdit1").data("kendoWindow");
                    //orderWindow.close();
                    $('#btnSubmit1').removeAttr("disabled");
                    Toast.fire({
                        type: 'success',
                        title: 'Recipe data saved'
                    });
                    $("#gridRecipe").data("kendoGrid").dataSource.data([]);
                    $("#gridRecipe").data("kendoGrid").dataSource.read();
                }
            }, {
                model: model,
                baseRecipes: baseRecipes,
                mogs: mogs

            }, true);
        }
    });


    $("#btnSubmitRecipe").click(function () {
        isPublishBtnClick = false;
        saveRecipeOnBtnSubmit();
        //getRecipeDependenciesData();
        //checkIsBRNameChange();
        //showRecipeImpactedMessage();
        //if (isBRNameChange) {
        //    $("#btnAgree").css('display', 'block');
        //    $("#btnDoLater").text("No Shall do it later");
        //    $("#btnDoLater").removeClass("btn-info");
        //    $("#btnDoLater").addClass("btn-tool");
        //}
        //else {
        //    $("#btnAgree").css('display', 'none');
        //    $("#btnDoLater").text("Ok");
        //    $("#btnDoLater").addClass("btn-info");
        //    $("#btnDoLater").removeClass("btn-tool");
        //    checkIsBRConfigurationChange();
        //}

        //if (recipeImpactedData.IsMOGImpacted && (isBRConfigurationChange || isBRNameChange)) {
        //    showRecipeDependenciesPopUp();
        //}
        //else {
        //    saveRecipeOnBtnSubmit();
        //}
    });


    $("#btnPublish").click(function () {
        Utility.Loading();
        setTimeout(function () {
            isPublishBtnClick = true;
            // saveReceipeOnPublishBtn();
            getRecipeDependenciesData();
            checkIsBRNameChange();
            showRecipeImpactedMessage();


            if (isBRNameChange) {
                $("#btnAgree").css('display', 'block');
                $("#btnDoLater").text("No Shall do it later");
                $("#btnDoLater").removeClass("btn-info");
                $("#btnDoLater").addClass("btn-tool");
            }
            else {
                $("#btnAgree").css('display', 'none');
                $("#btnDoLater").text("Ok");
                $("#btnDoLater").addClass("btn-info");
                $("#btnDoLater").removeClass("btn-tool");
                checkIsBRConfigurationChange();
            }

            if (recipeImpactedData.IsMOGImpacted && (isBRConfigurationChange || isBRNameChange)) {
                showRecipeDependenciesPopUp();
            }
            else {
                saveReceipeOnPublishBtn();
            }
        }
            , 1000);
    });



    $("#btnDoLater").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditRecipeDependencies").data("kendoWindow");
        orderWindow.close();
        if (isStatusSave) {
            $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsActive);
        }
    });

    $("#btnAgree").on("click", function () {
        if (isPublishBtnClick) { saveReceipeOnPublishBtn(); }
        else { saveRecipeOnBtnSubmit(); }
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditRecipeDependencies").data("kendoWindow");
        orderWindow.close();
    });
});




function validateSubmit(noOfDays, region, multiselectdp, multiselectitem) {
    var msg = "";
    //var noOfDays = $("#NewSimNoOfDays").val();
    //var region = $("#NewSimRegion").val();
    //var daypart = [];
    //var selecteditem = [];
    //Region check
    if (region == 0) {
        msg += "Please Select Region<br>";
    }
    //Number of days check                              
    if (noOfDays.length == 0) {
        msg += "Please Enter Number of Days<br>";
    }
    //Service type check
    /*var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");*/
    var defName = "Please Select Service Type<br>";
    if (user.SectorNumber != "20")
        defName = "Please Select Mealtime<br>";
    if (multiselectdp != undefined) {
        var items = multiselectdp.value();

        if (items.length == 0)
            msg += defName;
        else {
            items = items.filter(a => a == 0);
            if (items.length > 0) {
                if (user.SectorNumber != "20")
                    msg += "Please select valid mealtime";
                else
                    msg += "Please select valid service type";
            }

            //for (var i = 0; i < items.length; i++) {
            //    daypart.push(items[i]);
            //}
            //daypart = $("#NewSimServiceType").data("kendoMultiSelect").dataItems();
        }
    }
    else
        msg += defName;
    //Items check
    /*var multiselectitem = $("#ItemsForSimulation").data("kendoMultiSelect");*/
    if (multiselectitem != undefined) {
        var items = multiselectitem.value();

        if (items.length == 0)
            msg += "Please Select Item<br>";
        else {
            items = items.filter(a => a == "Select");
            if (items.length > 0) {
                msg += "Please select valid item";
            }
            //selecteditem = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();
            //selecteditem = itemsid;
        }
    }
    else
        msg += "Please Select Item<br>";
    return msg;
}

function expandConfig(btnEle) {
    if ($(btnEle).text() == "Expand") {
        $(btnEle).text("Collapse");
    }
    else {
        $(btnEle).text("Expand");
    }
    $(".divConfigFcs").slideToggle(0, function () { });
}

function clearRecipePanel() {
    $("#windowEdit").hide();
}
//function backToSimulationList() {

//    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
//        function () {

//            $("#FCSimulatorAdd").hide();
//            $("#FCSimulator").show();
//            $("#topHeading").text("Food Cost Simulator");
//        },
//        function () {
//        }
//    );
//}
function populateExistingBoard(result) {

    Utility.Loading();
    var foodCostBoard = result.FoodCostSimulationBoard;
    //Data Object
    //itemdata.push({ "value": "Select", "text": "Select Items", "code": "Select", "dpcode": "", "DishCategories": [] });
    //for (var i = 0; i < dataSource.length; i++) {
    //    itemdata.push({ "value": dataSource[i].ItemID, "text": dataSource[i].ItemName + " - " + dataSource[i].DayPartName, "code": dataSource[i].ItemCode, "dpcode": dataSource[i].DayPartCode, "DishCategories": dataSource[i].DishCategoryList });
    //} 
    var idata = [];
    var dpartdata = [];

    var dishes = []
    var dataO = [];
    //for (var i = 0; i < foodCostBoard.length; i++) {
    //    //for (var jdp = 0; jdp < foodCostBoard.length; jdp++) {
    //    //    for (var kdc = 0; kdc < foodCostBoard.length; kdc++) {
    //    //        for (var lds = 0; lds < foodCostBoard.length; lds++) {
    //    //            for (var mdo = 0; mdo < foodCostBoard.length; mdo++) {

    //    //            }
    //    //        }
    //    //    }
    //    //}
    //    var chk = dishes.filter(it => it.DishID == foodCostBoard[i].DIshID && it.DayPartCode == foodCostBoard[i].DayPartCode);
    //    //console.log(chk)
    //    if (chk.length == 0) {
    //        dishes.push({ "DishID": foodCostBoard[i].DishID, "DishName": foodCostBoard[i].DishName });
    //    }
    //}
    var dishcats = []

    //for (var i = 0; i < foodCostBoard.length; i++) {
    //    var chk = dishcats.filter(it => it.DishCategoryID == foodCostBoard[i].DishCategoryID && it.DayPartCode == foodCostBoard[i].DayPartCode);
    //    //console.log(chk)
    //    if (chk.length == 0) {
    //        dishcats.push({
    //            "DishCategoryID": foodCostBoard[i].DishCategoryID, "DishCategoryName": foodCostBoard[i].DishCategoryName, "DishCategoryCode": foodCostBoard[i].DishCategoryCode, "ItemID": foodCostBoard[i].ItemID, "ItemName": foodCostBoard[i].ItemName,"Dishes":dishes });
    //    }
    //}
    //Data object

    //console.log("reassigned");
    //console.log(idata);
    //console.log("sitedishdata")
    //console.log(sitedata)

    //console.log("selItems")
    //console.log(selItems)

    if (result.RegionID > 0) {

        HttpClient.MakeSyncRequest(CookBookMasters.GetRegionMasterListByUserId, function (data) {
            //Utility.Loading();

            var dataSource = data;
            //sitemasterdata = dataSource;
            regionmasterdata = dataSource//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
            // regionmasterdata.sort(compare);
            if (data.length > 1) {
                regionmasterdata.unshift({ "RegionID": 0, "Name": "Select Region" })
                //$("#btnGo").css("display", "inline-block");
            }
            //Utility.UnLoading();

            populateRegionMasterDropdown();
            //populateItemsMultiDropdown();
            //if (regionid != null) {
            //    //$("#NewSimRegion").val(regionid);
            $("#NewSimRegion").data('kendoDropDownList').value(result.RegionID);
            var dropDownList = $("#NewSimRegion").getKendoDropDownList();
            dropDownList.trigger("change");


            //if (user.StateRegionId > 0) {
            //    var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
            //    //$("#NewSimRegion").val(user.StateRegionId);
            //    regDropdown.enable(false);
            //}
            //else {
            //    var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
            //    regDropdown.enable(true);
            //}
            if (regionmasterdata.length == 1) {
                var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
                regDropdown.enable(false);
            }
            else if (regionmasterdata.length == 0) {
                $("#NewSimRegion").hide();
            }
            else {
                var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
                regDropdown.enable(true);
            }

            showAddNewSection(null, null);

            //12th April
            populateSiteDishDropdown(result.SiteCode);

            //populateSiteDropdown();
            HttpClient.MakeSyncRequest(CookBookMasters.GetSiteByRegionList, function (data) {
                Utility.Loading();
                //Utility.UnLoading();
                var dataSource = data;
                sitedata = []

                sitedata.push({ "value": "Select", "text": "Select Site", "code": "" });
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i].IsActive) {
                        sitedata.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName + " - " + dataSource[i].SiteCode, "code": dataSource[i].SiteCode });
                    }
                }
                //Utility.UnLoading();
                populateSiteDropdown();
                //Multi select items
                //$("#ItemsForSimulation").html('');
                //itemdata = [];

                setTimeout(function () {
                    //Generate dishes
                    //console.log("result.SiteCode")
                    //console.log(result.SiteCode)
                    //12th April
                    //populateSiteDishDropdown(result.SiteCode);
                    //Create data object
                    for (var i = 0; i < foodCostBoard.length; i++) {
                        var chk = idata.filter(it => it.value == foodCostBoard[i].ItemID + "_" + foodCostBoard[i].DayPartCode && it.dpcode == foodCostBoard[i].DayPartCode);
                        //console.log(chk)
                        if (chk.length == 0) {
                            dishcats = [];
                            //Dish category
                            for (var j = 0; j < foodCostBoard.length; j++) {
                                var chk = dishcats.filter(it => it.DishCategoryID == foodCostBoard[j].DishCategoryID && it.ItemID == foodCostBoard[i].ItemID && it.DayPartCode == foodCostBoard[i].DayPartCode);
                                if (chk.length == 0) {
                                    //for (var k = 0; k < foodCostBoard.length; k++) {
                                    //    //Dishes
                                    //    var chk3 = dishes.filter(ds => ds.DishID == foodCostBoard[k].DishID && ds.DishCategoryID == foodCostBoard[j].DishCategoryID && ds.ItemID == foodCostBoard[i].ItemID && ds.DayPartCode == foodCostBoard[i].DayPartCode)
                                    //    if (chk3.length == 0) {
                                    //        dishes.push({
                                    //            "DishID": f444444444444444444444464444oodCostBoard[k].DishID, "DishName": foodCostBoard[k].DishName, "DishCategoryID": foodCostBoard[j].DishCategoryID, "DishCategoryName": foodCostBoard[j].DishCategoryName, "DishCategoryCode": foodCostBoard[j].DishCategoryCode, "ItemID": foodCostBoard[i].ItemID, "ItemName": foodCostBoard[i].ItemName, "DayPartCode": foodCostBoard[i].DayPartCode
                                    //        });
                                    //    }
                                    //}
                                    var filterDish = foodCostBoard.filter(ds => ds.DishCategoryID == foodCostBoard[j].DishCategoryID && ds.ItemID == foodCostBoard[i].ItemID && ds.DayPartCode == foodCostBoard[i].DayPartCode);
                                    //console.log("filterDish")
                                    //console.log(filterDish)
                                    dishes = [];
                                    for (var k = 0; k < filterDish.length; k++) {
                                        var chk3 = dishes.filter(ds => ds.DishID == filterDish[k].DishID);
                                        if (chk3.length == 0) {
                                            //dataO
                                            dataO = [];
                                            var noofdays = result.NoOfDays;
                                            var da = filterDish.filter(ds => ds.DishID == filterDish[k].DishID);
                                            for (var l = 0; l < da.length; l++) {

                                                dataO.push({ "index": l + 1, "paxTxt": da[l].Pax, "grmTxt": da[l].Gm, "costTxt": da[l].Cost, "boradId": da[l].ID });

                                                //console.log("da[l].ID")
                                                //console.log(da[l].ID)
                                            }
                                            var getcost = 0;
                                            //console.log("dishdata from loop")
                                            //console.log(dishdata)
                                            if (dishdata.length > 0) {
                                                var chkDish = dishdata.filter(ds => ds.DishID == filterDish[k].DishID)[0];
                                                if (chkDish != undefined)
                                                    getcost = chkDish.CostPerKG;
                                            }
                                            dishes.push({
                                                "DishID": filterDish[k].DishID, "DishName": filterDish[k].DishName, "DishCode": filterDish[k].DishCode, "CostPerKG": getcost, "dataO": dataO, "ServedPortion": filterDish[k].ServedPortion
                                            });

                                        }
                                    }

                                    var chkDishCatAvail = foodCostBoard.filter(it => it.DishCategoryID == foodCostBoard[j].DishCategoryID && it.ItemID == foodCostBoard[i].ItemID && it.DayPartCode == foodCostBoard[i].DayPartCode);

                                    //Fix issue if dish category  not available in the list for item and daypart dont show it.
                                    if (chkDishCatAvail.length > 0) {
                                        dishcats.push({
                                            "DishCategoryID": foodCostBoard[j].DishCategoryID, "DishCategoryName": foodCostBoard[j].DishCategoryName, "DishCategoryCode": foodCostBoard[j].DishCategoryCode, "ItemID": foodCostBoard[i].ItemID, "ItemName": foodCostBoard[i].ItemName, "DayPartCode": foodCostBoard[i].DayPartCode, "Dishes": dishes
                                        });
                                    }
                                }
                            }
                            idata.push({ "value": foodCostBoard[i].ItemID + "_" + foodCostBoard[i].DayPartCode, "text": foodCostBoard[i].ItemName + " - " + foodCostBoard[i].DayPartName, "code": foodCostBoard[i].ItemCode, "dpcode": foodCostBoard[i].DayPartCode, "DishCategories": dishcats });
                            //console.log("weird item id")
                            //console.log(foodCostBoard[i].ItemID)
                            selItems.push(foodCostBoard[i].ItemID + "_" + foodCostBoard[i].DayPartCode);
                            //console.log(foodCostBoard)
                        }
                        var chk2 = dpartdata.filter(dp => dp.ID == foodCostBoard[i].DayPartID);
                        if (chk2.length == 0) {
                            dpartdata.push({ "ID": foodCostBoard[i].DayPartID, "Name": foodCostBoard[i].DayPartName, "DayPartCode": foodCostBoard[i].DayPartCode });
                            selDparts.push(foodCostBoard[i].DayPartID);
                        }
                    }

                    //Bind after few seconds
                    if (result.SiteID != null) {
                        //$("#NewSimSiteByRegion").val(result.SiteID);
                        $("#NewSimSiteByRegion").data('kendoDropDownList').value(result.SiteID);
                        var dropDownList = $("#NewSimSiteByRegion").getKendoDropDownList();
                        dropDownList.trigger("change");
                    }
                    //14th April
                    //getItemsMultiDropdownData();
                    //14th April---------------------------

                    //12th April
                    //Utility.Loading();
                    var siteid = $("#NewSimSiteByRegion").val();
                    var regionid = $("#NewSimRegion").val();
                    //var daypartid = $("#NewSimServiceType").val();
                    var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");

                    var daypartid = [];
                    if (multiselectdp != undefined) {
                        var items = multiselectdp.value();
                        for (var i = 0; i < items.length; i++) {
                            daypartid.push(items[i]);
                        }
                    }
                    if (daypartid.length == 0)
                        daypartid.push(0);

                    //console.log(siteid);
                    //console.log(regionid);
                    //console.log(daypartid);
                    if (siteid == "Select" || siteid == "")
                        siteid = 0;
                    if (regionid == "Select")
                        regionid = 0;
                    //if (daypartid.length != 0) {
                    HttpClient.MakeRequest(CookBookMasters.GetSimulationItemList, function (data) {
                        //12th April
                        //Utility.UnLoading();
                        var dataSource = data;
                        itemdata = [];

                        itemdata.push({ "value": "Select", "text": "Select Items", "code": "Select", "dpcode": "", "DishCategories": [] });
                        for (var i = 0; i < dataSource.length; i++) {
                            itemdata.push({ "value": dataSource[i].ItemID + "_" + dataSource[i].DayPartCode, "text": dataSource[i].ItemName + " - " + dataSource[i].DayPartName, "code": dataSource[i].ItemCode, "dpcode": dataSource[i].DayPartCode, "DishCategories": dataSource[i].DishCategoryList });
                        }
                        console.log("itemdata")
                        console.log(itemdata)
                        Utility.UnLoading();
                        var multiselect = $("#ItemsForSimulation").data("kendoMultiSelect");
                        var ds = new kendo.data.DataSource({ data: itemdata });
                        multiselect.setDataSource(ds);
                        //multiselect.dataSource.filter({});
                        //multiselect.value(itemdata);
                        if (selItems.length > 0) {
                            console.log("selItems")
                            console.log(selItems)

                            var selectedp = multiselectdp.dataItems();
                            var selItemNew = [];
                            selItems.forEach(function (item, index) {
                                var dp = item.split('_')[1];

                                var chkindplist = selectedp.filter(a => a.DayPartCode == dp);
                                if (chkindplist.length > 0)
                                    selItemNew.push(item);
                            });
                            multiselect.value(selItemNew);
                        }

                        performSimulationBoardFill(result.NoOfDays, result.RegionID, dpartdata, idata);

                    }, { siteId: siteid, regionId: regionid, daypartId: daypartid }, false);
                    //}

                    //14th April End-----------------------
                    //daypart = multiselectdp.dataItems();
                    //selecteditem = multiselectitem.dataItems();
                    //Bind board
                    //performSimulationBoardFill(noOfDays, region, daypart, itemsid);

                    //14th April
                    //performSimulationBoardFill(result.NoOfDays, result.RegionID, dpartdata, idata);

                    //expandConfig($("#btnExpandConfig"));

                    Utility.UnLoading();
                }, 500)


            }, { regionId: result.RegionID }, false);

            //}
        }, null, true);
    }

    //showAddNewSection(result.RegionID, result.SiteID);
    //Bind board                                    
    //performSimulationBoardFill(noOfDays, region, daypart, selecteditem);
}
function populateRegionMasterDropdown() {
    //var multiselect = $("#NewSimRegion").data("kendoDropDownList");
    //if (multiselect == undefined) {

    //TODO
    //Pre Select  if list length is 1,
    //if 0 dont show region dropdown
    $("#NewSimRegion").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "RegionID",
        dataSource: regionmasterdata,
        index: 0,
        change: onRegionChange
    });

}
function testCheckGridHeight() {
    var root = document.getElementById("#gridSimulationBoard");

    document.addEventListener('resize', () => {
        //root.style.maxHeight.setProperty('--screen-x', window.screenX)
        //root.style.setProperty('--screen-y', window.screenY)
        console.log(window.screenY)
        root.style.maxHeight = window.screenY
    })
}

function populateSimulationGrid() {

    var gridVariable = $("#gridSimulationMaster");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "FoodCostSimulation.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: true,
        groupable: false,
        height: 485,
        width: 1022,
        //reorderable: true,
        //scrollable: true,
        columns: [
            //{
            //    field: "SimulationCode", title: "Simulation Code", width: "40px", attributes: {
            //        style: "text-align: left; font-weight:normal"
            //    }
            //},
            {
                field: "SimulationCode",
                title: "Simulation Code", width: "85px",
                attributes: {
                    style: "text-align: left; font-weight:normal;text-transform: uppercase;"
                },
                headerAttributes: {
                    style: "text-align: left;"
                }
            },
            {
                field: "Name", title: "Simulation Name", width: "150px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Status", title: "Status", width: "70px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "Version", title: "Version", width: "70px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "CreateOnFormatted", title: "Date & Time", width: "100px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
                //template: '#= kendo.toString(kendo.parseDate(CreateOn), "dd MMM yyyy hh:mm tt")#'
                //format: "{0:dd MMM yyyy hh:mm tt}"
            },
            {
                field: "IsActive",
                title: "Active", width: "75px",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center;"
                },
                template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
            },
            {
                field: "Edit", title: "Action", width: "135px",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center;"
                },
                command: [
                    {
                        name: 'Edit',
                        click: function (e) {
                            Utility.Loading();
                            setTimeout(function () {

                                var gridObj = $("#gridSimulationMaster").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                var simId = tr.ID;

                                resetResult = [];
                                HttpClient.MakeRequest(CookBookMasters.GetSimulationBoardData, function (result) {
                                    Utility.Loading();
                                    console.log("result");
                                    console.log(result);
                                    //reset board purpose
                                    resetResult = result;

                                    $("#SimulationID").val(result.ID);
                                    $("#SimulationCode").val(result.SimCode);
                                    $("#hdnStatus").val(result.Status);

                                    if (result.Status == "Incomplete") {
                                        $("#btnUpdateVer").attr('disabled', 'disabled');
                                        $("#btnFinalize").attr('disabled', 'disabled');
                                    }
                                    else {
                                        $("#btnUpdateVer").removeAttr('disabled');
                                        $("#btnFinalize").removeAttr('disabled');
                                    }

                                    $("#NewSimNoOfDays").val(result.NoOfDays);
                                    $("#txtSimulationName").val(result.SimulationName);
                                    var version = "";
                                    if (result.Version != null && result.Version != "null")
                                        version = result.Version;

                                    $("#divSimConfig input").attr("disabled", "disabled");
                                    $("#divSimConfig").addClass("divDisabled");

                                    //dropdownlist.enable(true);
                                    viewOnly = 0;

                                    $("#spanHeadertxt").html("Update Simulation " + result.SimulationName + " " + version);

                                    //get sitedish data if not generated
                                    //if(sitedish)

                                    populateExistingBoard(result);

                                    $("#topHeading").text("Food Cost Simulator");

                                    var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
                                    dropdownlist.enable(false);
                                    dropdownlist = $("#selectFromSite").data("kendoDropDownList");
                                    dropdownlist.enable(false);

                                    $(".card-tools").find("button").show();
                                    $("#btnCancelVO").hide();
                                    $("#btnResetBoard").show();

                                    //March 30th
                                    setTimeout(function () {
                                        var dropdownlist = $("#NewSimRegion").data("kendoDropDownList");
                                        dropdownlist.enable(false);
                                        dropdownlist = $("#NewSimSiteByRegion").data("kendoDropDownList");
                                        dropdownlist.enable(false);
                                    }, 100);

                                    //Utility.UnLoading();
                                }, { simId: simId }, false);

                                return true;
                            }, 50);

                        }
                    }
                    ,
                    {
                        name: 'View',
                        click: function (e) {
                            var gridObj = $("#gridSimulationMaster").data("kendoGrid");
                            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            datamodel = tr;
                            var simId = tr.ID;
                            Utility.Loading();
                            HttpClient.MakeRequest(CookBookMasters.GetSimulationBoardData, function (result) {

                                $("#divSimTopNav").show();
                                $("#SimulationID").val(result.ID);
                                $("#SimulationCode").val(result.SimCode);
                                $("#hdnStatus").val(result.Status);
                                $("#NewSimNoOfDays").val(result.NoOfDays);
                                $("#txtSimulationName").val(result.SimulationName);
                                var version = "";
                                if (result.Version != null && result.Version != "null")
                                    version = result.Version;

                                $("#divSimConfig input").attr("disabled", "disabled");
                                $("#divSimConfig").addClass("divDisabled");

                                //dropdownlist.enable(true);
                                viewOnly = 1;
                                $("#spanHeadertxt").html("View Simulation " + result.SimulationName + " " + version);
                                populateExistingBoard(result);

                                $("#btnGo").hide();
                                $("#NewSimNoOfDays").attr("disabled", "disabled");
                                $("#txtSimulationName").attr("disabled", "disabled");;
                                setTimeout(function () {
                                    var dropdownlist = $("#NewSimRegion").data("kendoDropDownList");
                                    dropdownlist.enable(false);
                                    dropdownlist = $("#NewSimSiteByRegion").data("kendoDropDownList");
                                    dropdownlist.enable(false);
                                    dropdownlist = $("#ItemsForSimulation").data("kendoMultiSelect");
                                    dropdownlist.enable(false);
                                    dropdownlist = $("#NewSimServiceType").data("kendoMultiSelect");
                                    dropdownlist.enable(false);
                                }, 100);
                                //$("#divSimConfig").hide();
                                //$("#divSimFilter").hide();
                                $(".card-tools").find("button").hide();
                                $("#btnCancelVO").show();
                                $("#btnResetBoard").hide();
                                var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
                                dropdownlist.enable(false);
                                dropdownlist = $("#selectFromSite").data("kendoDropDownList");
                                dropdownlist.enable(false);


                            }, { simId: simId }, false);
                            return true;
                        }
                    },
                    {
                        name: 'Export to Excel',
                        click: function (e) {
                        }
                    }
                ],
            }
        ],
        dataSource: {
            data: filteredSimulationDataSource,
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Name: { editable: false, type: "string" },
                        SimulationCode: { type: "string" },
                        Status: { type: "string" },
                        Version: { type: "string" },
                        IsActive: { editable: false },
                        CreateOn: { type: 'date' },
                        CreateOnFormatted: { type: "string" },
                        RegionID: { type: "int" },
                        SiteID: { type: "int" },
                        NoOfDays: { type: "int" },
                        CreatedBy: { type: "int" },
                        ModifiedBy: { type: "int" },
                        ModifiedOn: { type: "string" },
                    }
                },
                parse: function (response) {
                    for (var i = 0; i < response.length; i++) {
                        response[i].CreateOnFormatted = kendo.toString(kendo.parseDate(response[i].CreateOn), "dd MMM yyyy hh:mm tt")
                        //kendo.format("{0:dd MMM yyyy hh:mm tt}", response[i].CreateOn);
                        //console.log(response[i].CreateOnFormatted)
                    }

                    return response;
                }
            },
            pageSize: 100,
        },
        dataBound: function (e) {
            var items = e.sender.items();
            var grid = this;

            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);

                if (!model.IsActive || model.Status == "Final") {
                    //$(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    $(this).find(".k-grid-Edit").hide();
                }
                if (model.Status == "Final") {
                    $(this).find(".k-grid-View").show();
                }
                else {
                    $(this).find(".k-grid-View").hide();
                }
            });

            $(".chkbox").on("change", function () {
                // 
                $("#success").css("display", "none");
                $("#error").css("display", "none");
                var gridObj = $("#gridSimulationMaster").data("kendoGrid");
                var tr = gridObj.dataItem($(this).closest("tr"));
                datamodel = tr;
                var th = this;
                datamodel.IsActive = $(this)[0].checked;
                var active = "";
                if (datamodel.IsActive) {
                    active = "Active";
                } else {
                    active = "Inactive";
                }
                console.log("statusmodel")
                console.log(datamodel)
                Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Simulation " + active + "?", "Simulation Update Confirmation", "Yes", "No", function () {
                    $("#error").css("display", "none");
                    HttpClient.MakeRequest(CookBookMasters.SaveStatusSimulationData, function (result) {
                        if (result == false) {
                            //$("#error").css("display", "flex");
                            //$("#error").find("p").text("Some error occured, please try again");
                            toastr.error("Some error occured, please try again");
                        }
                        else {
                            //$("#error").css("display", "flex");
                            //$("#success").css("display", "flex");
                            //$("#success").find("p").text("Color updated successfully");
                            toastr.success("Simulation updated successfully");

                        }
                    }, {
                        model: datamodel
                    }, false);
                }, function () {
                    $(th)[0].checked = !datamodel.IsActive;
                    //$("#gridColor").data("kendoGrid").dataSource.data([]);
                    //$("#gridColor").data("kendoGrid").dataSource.read();
                });
                return true;
            });
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        excelExport: function onExcelExport(e) {
            //var sheet = e.workbook.sheets[0];
            //var data = e.data;
            //var cols = Object.keys(data[0])
            //var columns = cols.filter(function (col) {
            //    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
            //    else
            //        return col;
            //});
            //var columns1 = columns.map(function (col) {
            //    return {
            //        value: col,
            //        autoWidth: true,
            //        background: "#7a7a7a",
            //        color: "#fff"
            //    };
            //});
            //console.log(columns1);
            //var rows = [{ cells: columns1, type: "header" }];

            //for (var i = 0; i < data.length; i++) {
            //    var rowCells = [];
            //    for (var j = 0; j < columns.length; j++) {
            //        var cellValue = data[i][columns[j]];
            //        rowCells.push({ value: cellValue });
            //    }
            //    rows.push({ cells: rowCells, type: "data" });
            //}
            //sheet.rows = rows;
        }
        //,
        //search: {
        //    fields: ["SimulationCode", "Name", "Status", "Version", "CreateOnFormatted"]
        //},        
        //toolbar: ["search"],
    })
    //$(".k-label")[0].innerHTML.replace("items", "records");
    getDishCategoriesWithDishes();
    //Utility.UnLoading();

    $("#topHeading").text("My Simulations");
}
//populateExistingBoard

function populateSimulationDropdown() {
    $("#selectSimulation").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: simdata,
        enable: true,
        open: onSimulationDropdownClick,
        change: onExistingSimulationChange
    });
}
function populateSiteMasterDropdown() {
    $("#selectFromSite").kendoDropDownList({
        filter: "contains",
        dataTextField: "SiteName",
        dataValueField: "SiteID",
        dataSource: sitemasterdata,
        index: 0,
        open: onSiteMasterDropdownClick,
        change: onSiteMasterChange
    });
}
//Middle Row dropdowns
function populateSiteDropdown() {
    //TODO
    //Site dropdown if user is admin all site of the region will come
    //if user is not admin data will come from usersitemapping table
    $("#NewSimSiteByRegion").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: sitedata,
        enable: true,
        change: onSiteChange
    });
}


function populateSiteDishDropdown(SiteCode) {
    var sitedishdata = [];
    Utility.Loading();
    if (SiteCode != undefined && SiteCode.length > 0) {
        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteDishDataList, function (data) {
            sitedishdata = data;

        }, { siteCode: SiteCode }, true);

        HttpClient.MakeSyncRequest(CookBookMasters.GetDishList, function (data) {

            var dataSource = data;
            dishdata = [];
            //for (var i = 0; i < dataSource.length; i++) {
            //    sitedishdata.forEach(function (yi) {
            //        if (yi.DishCode == dataSource[i].DishCode) {
            //            //dataSource[i].CostPerKG = yi.CostPerKG;
            //        }
            //    });
            //}
            console.log(SiteCode)
            console.log("chkdishcateg")
            console.log(sitedishdata);

            dishdata.push({ "value": 0, "text": "Select", "price": 0 });
            for (var i = 0; i < dataSource.length; i++) {
                dishdata.push({
                    "DishID": dataSource[i].ID, "DishName": dataSource[i].Name, "DishCode": dataSource[i].DishCode,
                    "DishCategoryID": dataSource[i].DishCategory_ID, "DishCategoryName": dataSource[i].DishCategoryName,
                    "DishCategoryCode": dataSource[i].DishCategoryCode,
                    "CostPerKG": dataSource[i].CostPerKG, "tooltip": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
                    "DietCategoryCode": dataSource[i].DietCategoryCode,
                    "CuisineName": dataSource[i].CuisineName, "RecipeMapStatus": dataSource[i].RecipeMapStatus
                });
            }
            //for (var i = 0; i < sitedishdata.length; i++) {
            //    dishdata.push({
            //        "DishID": sitedishdata[i].ID, "DishName": sitedishdata[i].Name, "DishCode": sitedishdata[i].DishCode,
            //        "DishCategoryID": sitedishdata[i].DishCategory_ID, "DishCategoryName": sitedishdata[i].DishCategoryName,
            //        "DishCategoryCode": sitedishdata[i].DishCategoryCode,
            //        "CostPerKG": sitedishdata[i].CostPerKG, "tooltip": sitedishdata[i].Name, "UOMCode": sitedishdata[i].UOMCode,
            //        "DietCategoryCode": sitedishdata[i].DietCategoryCode,
            //        "CuisineName": sitedishdata[i].CuisineName, "RecipeMapStatus": sitedishdata[i].RecipeMapStatus
            //    });
            //}
            console.log("chkdishcateg")
            console.log(dishdata);
            console.log(data);

            dishcatwithdishData = [];
            for (var i = 0; i < sitedishcatlist.length; i++) {
                var filteredDishes = dishdata.filter(ds => ds.DishCategoryID == sitedishcatlist[i].ID);
                dishcatwithdishData.push({
                    "DishCategoryName": sitedishcatlist[i].Name,
                    "DishCategoryID": sitedishcatlist[i].ID,
                    "Dishes": filteredDishes,
                    "DishCategoryCode": sitedishcatlist[i].DishCategoryCode
                });
                //dcsData.push({ "DishCategoryName": filteredDc[j].DishCategoryName, "DishCategoryID": filteredDc[j].DishCategoryID, "Dishes": filteredDc[j].Dishes, "DishCategoryCode": filteredDc[j].DishCategoryCode })
            }
            console.log("dishcatwithdishData");
            console.log(dishcatwithdishData)
            //console.log("dishdata")
            //console.log(dishdata)
            Utility.UnLoading();
        }, { siteCode: SiteCode }, true);
    }
}

function getDishCategoriesWithDishes() {
    //Utility.Loading();
    HttpClient.MakeRequest(CookBookMasters.GetDishCategoryList, function (data) {
        //Utility.UnLoading();
        var dataSource = data;
        sitedishcatlist = []

        console.log("getDishCategoriesWithDishes")
        console.log(dataSource);
        sitedishcatlist = dataSource;
        //sitedishcatlist.push({ "value": "Select", "text": "Select Site" });
        //for (var i = 0; i < dataSource.length; i++) {
        //    if (dataSource[i].IsActive) {
        //        sitedishcatlist.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName });
        //    }
        //}
        ////Utility.UnLoading();
        //populateSiteDropdown();
        ////Multi select items
        ////$("#ItemsForSimulation").html('');
        ////itemdata = [];
        //getItemsMultiDropdownData();
        Utility.UnLoading();
    }, null, false);
}
function getDishes() {
    HttpClient.MakeRequest(CookBookMasters.GetDishList, function (data) {
        //Utility.UnLoading();
        var dataSource = data;
        sitedishlist = []

        console.log("getDishes")
        console.log(dataSource);

        sitedishlist = dataSource;
        //sitedishcatlist.push({ "value": "Select", "text": "Select Site" });
        //for (var i = 0; i < dataSource.length; i++) {
        //    if (dataSource[i].IsActive) {
        //        sitedishcatlist.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName });
        //    }
        //}
        ////Utility.UnLoading();
        //populateSiteDropdown();
        ////Multi select items
        ////$("#ItemsForSimulation").html('');
        ////itemdata = [];
        //getItemsMultiDropdownData();
    }, null, false);

}
function populateDayPartDropdown() {
    //$("#NewSimServiceType").kendoDropDownList({
    //    filter: "contains",
    //    dataTextField: "Name",
    //    dataValueField: "ID",
    //    dataSource: daypartdata,
    //    index: 0,
    //    change: onDayPartChange
    //});
    var multiselect = $("#NewSimServiceType").data("kendoMultiSelect");
    if (multiselect == undefined) {
        $("#NewSimServiceType").kendoMultiSelect({
            filter: "contains",
            dataTextField: "Name",
            dataValueField: "ID",
            dataSource: daypartdata,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
            change: onDayPartChange
        });
    }
}

function onExistingSimulationChange() {
    var simid = $("#selectSimulation").val();
    Utility.Loading();
    resetResult = [];
    HttpClient.MakeRequest(CookBookMasters.GetSimulationBoardData, function (result) {
        console.log("result");
        console.log(result);
        //14th April reset purpose
        resetResult = result;
        //$("#SimulationID").val(result.ID);
        $("#SimulationCode").val(result.SimCode);
        //$("#hdnStatus").val(result.Status);
        $("#NewSimNoOfDays").val(result.NoOfDays);
        $("#txtSimulationName").val(result.SimulationName);
        //$("#spanHeadertxt").html("Create From Simulation " + result.SimulationName + " " + result.Version);
        populateExistingBoard(result);

        $("#divSimConfig input").attr("disabled", "disabled");
        $("#divSimConfig").addClass("divDisabled");

        $("#btnUpdateVer").prop('disabled', 'disabled');
        $("#btnFinalize").prop('disabled', 'disabled');
    }, { simId: simid }, false);

}

function onSiteChange() {
    var getsiteid = $("#NewSimSiteByRegion").val();

    if (getsiteid != "" && getsiteid > 0) {
        getDayPartMultiData();

        var code = sitedata.filter(s => s.value == getsiteid)[0].code;
        $("#hdnSiteCode").val(code);
        //console.log("csitedataode")
        //console.log(sitedata)
        //Skip when in edit mode
        console.log("add mode")
        console.log($("#SimulationID").val())
        if ($("#SimulationID").val() == 0) {

            $("#NewSimNoOfDays").focus();
        }
        //populateSiteDishDropdown(code)
        SiteCode = code;
        //populateDayPartDropdown();
        setTimeout(function () { populateSiteDishDropdown(SiteCode) }, 100);
    }
}
function onDayPartChange() {
    getItemsMultiDropdownData();
}
function onItemChange() {
    var multiselectit = $("#ItemsForSimulation").data("kendoMultiSelect");

    var value = this.dataItems();
    console.log("selected value")
    console.log(value)
    //alert(multiselectit.value());
    itemsid = [];
    var dishcategs = [];
    var selecteditems = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();
    itemsid = selecteditems;
    console.log(selItems)
    console.log(selecteditems)

    if (selItems.length > 0) {
        selItems = [];
        selecteditems.forEach(function (item, index) {
            selItems.push(item.value);
        });
    }

    if (localStorage.getItem("contextitem") != undefined) {
        var noOfDays = $("#NewSimNoOfDays").val();
        var contextData = selecteditems;
        Utility.Loading();
        setTimeout(function () {
            //var getCurrentContext=contextData.filter(a=>a.value=)

            for (var i = 0; i < contextData.length; i++) {
                for (var j = 0; j < contextData[i].DishCategories.length; j++) {
                    for (var k = 0; k < contextData[i].DishCategories[j].Dishes.length; k++) {
                        var currentDishData = contextData[i].DishCategories[j].Dishes[k];
                        var currentDcData = contextData[i].DishCategories[j];
                        var currentItem = contextData[i];
                        var itemidvalue = currentItem.value.split('_')[0];
                        var dataO = [];
                        for (var l = 1; l <= noOfDays; l++) {
                            var paxTxt = "#txtPax_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
                            var grmTxt = ".txtGrmroot_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;
                            var costTxt = "#lblCost_" + l + "_" + itemidvalue + "_" + currentItem.dpcode + "_" + currentDishData.DishID;

                            dataO.push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });
                            //contextData[i].DishCategories[j].Dishes[k].push({ "index": l, "paxTxt": $(paxTxt).val(), "grmTxt": $(grmTxt).val(), "costTxt": $(costTxt).text() });

                        }
                        contextData[i].DishCategories[j].Dishes[k].dataO = dataO;
                    }
                }
                var itAvgCost = ".lblavgItemTotal_" + contextData[i].dpcode + "_" + contextData[i].value.split('_')[0];
                contextData[i].TotalAvgCost = $(itAvgCost).text();
                //if (removedDishContext.length>0) {
                //    contextData[i].DeletedDishes = removedDishContext;
                //}
            }
            Utility.UnLoading();


            //var contextData1 = JSON.parse(localStorage.getItem("contextitem"));
            ////contextData = contextData.filter(a => a.value != multiselectit.value());
            //contextData1.forEach(function (item, index) {
            //    var chk = selecteditems.filter(a => a.value == item.value);
            //    if (chk.length == 0)
            //        contextData1.splice(index, 1);
            //});
            //console.log("contextData")
            //console.log(contextData1)
            localStorage.setItem("contextitem", JSON.stringify(contextData));


        }, 50);
    }
    //console.log(localStorage.getItem("contextitem"))
    //if (localStorage.getItem("contextitem") != undefined) {
    //    var contextData1 = JSON.parse(localStorage.getItem("contextitem"));
    //    //contextData = contextData.filter(a => a.value != multiselectit.value());
    //    contextData1.forEach(function (item, index) {
    //        var chk = selecteditems.filter(a => a.value == item.value);
    //        if (chk.length == 0)
    //            contextData1.splice(index, 1);
    //    });
    //    console.log("contextData")
    //    console.log(contextData1)
    //    localStorage.setItem("contextitem", JSON.stringify(contextData1));
    //}




    //if (multiselectit != undefined && multiselectit.value().length>0) {
    //    var items = multiselectit.value();
    //    Utility.Loading();
    //    var selecteditems = $("#ItemsForSimulation").data("kendoMultiSelect").dataItems();

    //    /*console.log("items" + selecteditems);*/

    //    //for (var i = 0; i < selecteditems.length; i++) {
    //    //    console.log("items" + selecteditems[i].value);
    //    //    HttpClient.MakeRequest(CookBookMasters.GetSimulationRegionItemDishCategories, function (data) {
    //    //        //var jar = JSON.parse(data);
    //    //        for (var j = 0; j < data.length; j++) {

    //    //            dishcategs.push({ "DishCategoryName": data[j].DishCategoryName, "DishCategoryID": data[j].DishCategoryID,"Dishes":data[j].Dishes});
    //    //        }
    //    //        itemsid.push({ "value": selecteditems[i].value, "text": selecteditems[i].text, "DishCategories": dishcategs });
    //    //        Utility.UnLoading();
    //    //        //console.log("itemsid" + itemsid);
    //    //    }, { itemId: selecteditems[i].value }, false);
    //    //    //itemsid.push(items[i]);
    //    //}

    //    itemsid = [];
    //    var newitemid = [];
    //    for (var i = 0; i < selecteditems.length; i++) {
    //        console.log(selecteditems[i].DishCategories);
    //        newitemid.push(selecteditems[i].value);
    //        //Utility.Loading();
    //        //var val = selecteditems[i].value;
    //        //var tex = selecteditems[i].text;
    //        //var cod = selecteditems[i].code;
    //        //var dpcod = selecteditems[i].dpcode;
    //        ////console.log("items" + selecteditem[i].value);
    //        //HttpClient.MakeRequest(CookBookMasters.GetSimulationRegionItemDishCategories, function (data) {
    //        //    //var jar = JSON.parse(data);
    //        //    dishcategs = [];
    //        //    if (data.length > 0) {
    //        //        for (var j = 0; j < data.length; j++) {
    //        //            console.log(data[j])
    //        //            dishcategs.push({ "DishCategoryName": data[j].DishCategoryName, "DishCategoryID": data[j].DishCategoryID, "Dishes": data[j].Dishes });
    //        //        }
    //        //    }

    //        //    itemsid.push({ "value": val, "text": tex, "code": cod, "dpcode": dpcod, "DishCategories": dishcategs });


    //        //    Utility.UnLoading();
    //        //    //console.log("itemsid" + itemsid);
    //        //}, { itemId: selecteditems[i].value }, false);
    //        //itemsid.push(items[i]);
    //    }
    //    //HttpClient.MakeRequest(CookBookMasters.GetSimulationRegionItemDishCategories, function (data) {
    //    //        //var jar = JSON.parse(data);
    //    //        dishcategs = [];
    //    //        if (data.length > 0) {
    //    //            for (var j = 0; j < data.length; j++) {
    //    //                console.log(data[j])
    //    //                dishcategs.push({ "DishCategoryName": data[j].DishCategoryName, "DishCategoryID": data[j].DishCategoryID, "Dishes": data[j].Dishes });
    //    //            }
    //    //        }

    //    //        itemsid.push({ "value": val, "text": tex, "code": cod, "dpcode": dpcod, "DishCategories": dishcategs });


    //    //        Utility.UnLoading();
    //    //        //console.log("itemsid" + itemsid);
    //    //}, { itemIds: selecteditems }, false);
    //}
}
function onRegionChange() {
    Utility.Loading();
    var userRole = user.UserRoleId;
    var regId = $("#NewSimRegion").val();
    if (userRole == 1) {
        //Admin
        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteByRegionList, function (data) {
            Utility.UnLoading();
            var dataSource = data;
            sitedata = []

            console.log("user")
            console.log(user)
            sitedata.push({ "value": "Select", "text": "Select Site", "code": "" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].IsActive) {
                    //sitedata.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName });
                    sitedata.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName, "code": dataSource[i].SiteCode });
                }
            }
            //Utility.UnLoading();
            populateSiteDropdown();
            //Multi select items
            //$("#ItemsForSimulation").html('');
            //itemdata = [];
            getItemsMultiDropdownData();
        }, { regionId: regId }, false);
    }
    else {
        //Non-Admin
        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteByUserList, function (data) {
            Utility.UnLoading();
            var dataSource = data;
           // dataSource = dataSource.filter(ds => ds.RegionID == regId)[0];
            console.log("dataSource")
            console.log(dataSource)
            sitedata = []

            sitedata.push({ "value": "Select", "text": "Select Site", "code": "" });
            for (var i = 0; i < dataSource.length; i++) {
                //if (dataSource[i].IsActive) {
                //sitedata.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName });
                if (dataSource[i].RegionID == regId) {
                    sitedata.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName, "code": dataSource[i].SiteCode });
                }
                //}
            }
            //Utility.UnLoading();
            populateSiteDropdown();
            //Multi select items
            //$("#ItemsForSimulation").html('');
            //itemdata = [];
            getItemsMultiDropdownData();
        }, null, false);
    }
}
function onSiteMasterDropdownClick() {
    $("#radCopyFromSite").attr("checked", true);
    var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
    if (regDropdown != undefined)
        regDropdown.enable(false);
    regDropdown = $("#NewSimSiteByRegion").data("kendoDropDownList");
    if (regDropdown != undefined)
        regDropdown.enable(false);
}
function onSiteMasterChange() {

    Utility.Loading();
    var getsiteid = $("#selectFromSite").val();
    var result = sitemasterdata.filter(a => a.SiteID == getsiteid)[0];
    if (getsiteid != "" && getsiteid > 0 && result != undefined) {
        HttpClient.MakeSyncRequest(CookBookMasters.GetRegionMasterListByUserId, function (data) {
            // Utility.Loading();
            var dataSource = data;
            //sitemasterdata = dataSource;
            regionmasterdata = dataSource//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
            // regionmasterdata.sort(compare);
            if (data.length > 1) {
                regionmasterdata.unshift({ "RegionID": 0, "Name": "Select Region" })
                //$("#btnGo").css("display", "inline-block");
            }
            //Utility.UnLoading();

            populateRegionMasterDropdown();
            //populateItemsMultiDropdown();
            //if (regionid != null) {
            //    //$("#NewSimRegion").val(regionid);
            $("#NewSimRegion").data('kendoDropDownList').value(result.RegionID);
            var dropDownList = $("#NewSimRegion").getKendoDropDownList();
            dropDownList.trigger("change");


            //if (user.StateRegionId > 0) {
            //    var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
            //    //$("#NewSimRegion").val(user.StateRegionId);
            //    regDropdown.enable(false);
            //}
            //else {
            //    var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
            //    regDropdown.enable(true);
            //}
            if (regionmasterdata.length == 1) {
                var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
                regDropdown.enable(false);
            }
            else if (regionmasterdata.length == 0) {
                $("#NewSimRegion").hide();
            }
            else {
                var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
                regDropdown.enable(true);
            }

            showAddNewSection(null, null);


            //populateSiteDropdown();
            HttpClient.MakeSyncRequest(CookBookMasters.GetSiteByRegionList, function (data) {
                Utility.Loading();
                //Utility.UnLoading();
                var dataSource = data;
                sitedata = []

                sitedata.push({ "value": "Select", "text": "Select Site", "code": "" });
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i].IsActive) {
                        sitedata.push({ "value": dataSource[i].SiteID, "text": dataSource[i].SiteName, "code": dataSource[i].SiteCode });
                    }
                }
                //Utility.UnLoading();
                populateSiteDropdown();
                //Multi select items
                //$("#ItemsForSimulation").html('');
                //itemdata = [];

                setTimeout(function () {
                    //Generate dishes
                    populateSiteDishDropdown(result.SiteCode);

                    //Bind after few seconds
                    if (result.SiteID != null) {
                        //$("#NewSimSiteByRegion").val(result.SiteID);
                        $("#NewSimSiteByRegion").data('kendoDropDownList').value(result.SiteID);
                        var dropDownList = $("#NewSimSiteByRegion").getKendoDropDownList();
                        dropDownList.trigger("change");
                    }
                    getItemsMultiDropdownData();
                    //daypart = multiselectdp.dataItems();
                    //selecteditem = multiselectitem.dataItems();
                    //Bind board
                    //performSimulationBoardFill(noOfDays, region, daypart, itemsid);

                    var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
                    regDropdown.enable(false);
                    var regDropdown = $("#NewSimSiteByRegion").data("kendoDropDownList");
                    regDropdown.enable(false);

                    Utility.UnLoading();
                }, 100)


            }, { regionId: result.RegionID }, false);

            //}
        }, null, true);
    }
}
function onSimulationDropdownClick() {
    $("#radCopyFromSimulation").attr("checked", true);
}
function getDayPartMultiData() {
    var siteid = $("#NewSimSiteByRegion").val();
    HttpClient.MakeSyncRequest(CookBookMasters.GetDayPartBySiteList, function (data) {
        var dataSource = data;
        //sitemasterdata = dataSource;
        daypartdata = dataSource//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
        // regionmasterdata.sort(compare);
        var defName = "Select Service Type";
        if (user.SectorNumber != "20")
            defName = "Select Mealtime";
        if (data.length > 1) {
            daypartdata.unshift({ "ID": 0, "Name": defName })
            //$("#btnGo").css("display", "inline-block");
        }

        var multiselect = $("#NewSimServiceType").data("kendoMultiSelect");
        var ds = new kendo.data.DataSource({ data: daypartdata });
        multiselect.setDataSource(ds);
        if (selDparts.length > 0) {
            console.log("selDparts")
            console.log(selDparts)
            multiselect.value(selDparts);
        }
        Utility.UnLoading();
    }, { siteId: siteid }, true);
}
function getItemsMultiDropdownData() {
    //12th April
    //Utility.Loading();
    var siteid = $("#NewSimSiteByRegion").val();
    var regionid = $("#NewSimRegion").val();
    //var daypartid = $("#NewSimServiceType").val();
    var multiselectdp = $("#NewSimServiceType").data("kendoMultiSelect");

    var daypartid = [];
    if (multiselectdp != undefined) {
        var items = multiselectdp.value();
        for (var i = 0; i < items.length; i++) {
            daypartid.push(items[i]);
        }
    }
    if (daypartid.length == 0)
        daypartid.push(0);

    //console.log(siteid);
    //console.log(regionid);
    //console.log(daypartid);
    if (siteid == "Select" || siteid == "")
        siteid = 0;
    if (regionid == "Select")
        regionid = 0;
    //if (daypartid.length != 0) {
    HttpClient.MakeRequest(CookBookMasters.GetSimulationItemList, function (data) {
        //12th April
        //Utility.UnLoading();
        var dataSource = data;
        itemdata = [];

        itemdata.push({ "value": "Select", "text": "Select Items", "code": "Select", "dpcode": "", "DishCategories": [] });
        for (var i = 0; i < dataSource.length; i++) {
            itemdata.push({ "value": dataSource[i].ItemID + "_" + dataSource[i].DayPartCode, "text": dataSource[i].ItemName + " - " + dataSource[i].DayPartName, "code": dataSource[i].ItemCode, "dpcode": dataSource[i].DayPartCode, "DishCategories": dataSource[i].DishCategoryList });
        }
        console.log("itemdata")
        console.log(itemdata)
        Utility.UnLoading();
        var multiselect = $("#ItemsForSimulation").data("kendoMultiSelect");
        var ds = new kendo.data.DataSource({ data: itemdata });
        multiselect.setDataSource(ds);
        //multiselect.dataSource.filter({});
        //multiselect.value(itemdata);
        if (selItems.length > 0) {
            console.log("selItems")
            console.log(selItems)

            var selectedp = multiselectdp.dataItems();
            var selItemNew = [];
            selItems.forEach(function (item, index) {
                var dp = item.split('_')[1];

                var chkindplist = selectedp.filter(a => a.DayPartCode == dp);
                if (chkindplist.length > 0)
                    selItemNew.push(item);
            });
            multiselect.value(selItemNew);
        }
    }, { siteId: siteid, regionId: regionid, daypartId: daypartid }, false);
    //}
}
function populateItemsMultiDropdown() {
    var multiselect = $("#ItemsForSimulation").data("kendoMultiSelect");
    if (multiselect == undefined) {
        itemsMulti = $("#ItemsForSimulation").kendoMultiSelect({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: itemdata,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
            change: onItemChange
        }).data("kendoMultiSelect");
    }
}
function showAddNewSection(siteid, regionid) {
    Utility.Loading();
    $("#success").css("display", "none");
    $("#error").css("display", "none");
    //$("#topHeading").text("Create New Simulation");
    $("#FCSimulatorAdd").show();
    $("#FCSimulator").hide();
    //Sector based label for service type / mealtime
    if (user.SectorNumber == "20")
        $("#lblNewSimServiceType").text("Select Service Type");
    else
        $("#lblNewSimServiceType").text("Select Mealtime");

    populateSimulationDropdown();

    HttpClient.MakeSyncRequest(CookBookMasters.GetRegionMasterListByUserId, function (data) {
        Utility.Loading();
        var dataSource = data;
        //sitemasterdata = dataSource;
        regionmasterdata = dataSource//removeDuplicateObjectFromArray(sitemasterdata, 'RegionDesc');
        // regionmasterdata.sort(compare);
        if (data.length > 1) {
            regionmasterdata.unshift({ "RegionID": 0, "Name": "Select Region" })
            //$("#btnGo").css("display", "inline-block");
        }
        //Utility.UnLoading();

        populateRegionMasterDropdown();


        if (regionmasterdata.length == 1) {
            var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
            setTimeout(function () {
                regDropdown.value(user.StateRegionId);
                regDropdown.trigger("change");
                regDropdown.enable(false);
            }, 100);
        }
        else if (regionmasterdata.length == 0) {
            $("#NewSimRegion").hide();
        }
        else {
            var regDropdown = $("#NewSimRegion").data("kendoDropDownList");
            regDropdown.enable(true);
        }
        //if (regionid != null) {
        //    //$("#NewSimRegion").val(regionid);
        //    $("#NewSimRegion").data('kendoDropDownList').value(regionid);
        //    var dropDownList = $("#NewSimRegion").getKendoDropDownList();
        //    dropDownList.trigger("change");
        //}
    }, null, true);

    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteMasterList, function (data) {
        //console.log("Site Master Data");
        //console.log(data);
        sitemasterdata = data;
        sitemasterdata.forEach(function (item) {
            item.SiteName = item.SiteName + " - " + item.SiteCode;
            return item;
        })
        sitemasterdata = sitemasterdata.filter(item => item.IsActive != false);
        if (data.length > 1) {
            sitemasterdata.unshift({ "SiteID": 0, "SiteName": "Select Site" });
            /*$("#btnGo").css("display", "inline-block");*/
        }
        else {
            $("#btnGo").css("display", "inline-none");
        }
        populateSiteMasterDropdown();

        Utility.UnLoading();
        //if (siteid != null) {
        //    $("#NewSimSiteByRegion").val(siteid);
        //    var dropDownList = $("#NewSimSiteByRegion").getKendoDropDownList();
        //    dropDownList.trigger("change");
        //}
    }, null, false);

    //Get dish category master
    //getDishCategoriesWithDishes();
    //Get dishes master
    //getDishes();

    populateItemsMultiDropdown();
    populateDayPartDropdown();
}

function clearAndShowList() {
    Utility.Loading();

    HttpClient.MakeRequest(CookBookMasters.GetSimulationDataListWithPaging, function (result) {
        Utility.UnLoading();
        filteredSimulationDataSource = result;
        //For Simulation dropdown
        var dataSource = result;
        simdata = [];
        simdata.push({ "value": "Select", "text": "Select Simulation", "name": "Select Simulation" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].IsActive) {
                /*simdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name + " " + dataSource[i].Version });*/
                var version = "";
                if (dataSource[i].Version != null && dataSource[i].Version != "null")
                    version = dataSource[i].Version;
                simdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name + " " + version, "name": dataSource[i].Name });
            }
        }
        //if (user.UserRoleId === 1) {
        //    $("#InitiateBulkChanges").css("display", "inline");
        //    $("#AddNew").css("display", "inline");
        //}
        //if (user.UserRoleId === 2) {
        //    $("#InitiateBulkChanges").css("display", "none");
        //    $("#AddNew").css("display", "none");
        //}
        populateSimulationGrid();
        $("#FCSimulatorAdd").hide();
        $("#FCSimulator").show();
        $("#topHeading").text("Food Cost Simulator");

        $("#SimulationID").val("0");
        $("#SimulationCode").val("");
        $("#hdnStatus").val("");
        $("#NewSimNoOfDays").val("");
        $("#txtSimulationName").val("");

        $("#NewSimRegion").val("0");
        selDparts = [];
        selItems = [];
        sitedata = [];
        //
        removedDishCats = [];
        removedData = [];
        removedDishContext = [];
        //
        populateSiteDropdown();
        daypartdata = [];
        var multiselect = $("#NewSimServiceType").data("kendoMultiSelect");
        var ds = new kendo.data.DataSource({ data: daypartdata });
        multiselect.setDataSource(ds);

        itemdata = [];
        var multiselect = $("#ItemsForSimulation").data("kendoMultiSelect");
        var ds = new kendo.data.DataSource({ data: itemdata });
        multiselect.setDataSource(ds);
        //$("#NewSimSiteByRegion").html("");
        //$("#NewSimServiceType").html("");
        //$("#ItemsForSimulation").html("");
        $("#divBoard").hide("");
        $("#gridSimulationBoard").html("");

        $("#divSimConfig input").removeAttr("disabled");
        $("#divSimConfig").removeClass("divDisabled");
        var dropdownlist = $("#selectSimulation").data("kendoDropDownList");
        dropdownlist.enable(true);
        dropdownlist = $("#selectFromSite").data("kendoDropDownList");
        dropdownlist.enable(true);

        //View board these divs are hiding
        $("#divSimConfig").show();
        $("#divSimFilter").show();
        $(".card-tools").show();
        viewOnly = 0;

        clearRecipePanel();

        $("#topHeading").text("My Simulations");
    }, null, false);
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
//18th April
function validateU2D(e) {
    var t = e.value;
    e.value = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 3)) : t;
}