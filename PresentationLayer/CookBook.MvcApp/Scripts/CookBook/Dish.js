﻿var recipedata = [];
var basedataList = [];
var dishdata = [];
var baserecipedata = [];
var recipeCustomData = [];
var mappedData = [];
var colordata = [];
var mappedRecipeCode = [];
var DishCode;
var Dish_ID;
var multiarray = [];
var checkedRecipeCode = '';
var saveModel = [];
var isDishPage = true;
var cond = 'TALL';
var impactDishGrid;
var basedataList = [];
var uomdata = [];
var basedata = [];
var mogdataList = [];
var mogWiseAPLMasterdataSource = [];
var MOGCode = "MOG-00156";
var MOGName = "Test"
var dietcategorydata = [];
var healthTagMasterDataSource = [];
var lifeStyleTagMasterDataSource = [];
var diettypeMasterDataSource = [];
var uommodulemappingdata = [];
var selDishNutritionData = [];
var selTagData = '';
var DishName = "";
//Krish 07-10-2022
var rangeTypeData = [];

var kitchenSectionMasterDataSource = [];
var menuTypeMasterDataSource = [];
var patientBillingProfileMasterDataSource = [];


$("#windowEditNutritionInfo").kendoWindow({
    modal: true,
    width: "800px",
    height: "450px",
    left: '220px !important',
    top: '80px !important',
    title: "Dish Details",
    actions: ["Close"],
    visible: false,
    draggable: true,
    resizable: true,
    animation: false
});



$("#windowEditMOGWiseAPL").kendoWindow({
    modal: true,
    width: "900px",
    height: "300px",
    cssClass: 'aplPopup',
    top: '80px !important',
    title: "MOG APL Detials - " + MOGName,
    actions: ["Close"],
    visible: false,
    animation: false
});
$("#RangeTypeDishwindowEdit").kendoWindow({
    modal: true,
    width: "500px",
    height: "222px",
    title: DishName, //"Range Type Details - " + rangetypeName,
    actions: ["Close"],
    visible: false,
    animation: false,
    resizable: false
});
function customData(DishCode, cond) {
    //Krish 02-08-2022
    HttpClient.MakeRequest(CookBookMasters.GetSectorItemDataList, function (result) {
        console.log(result);
        //HttpClient.MakeSyncRequest(CookBookMasters.GetDishRecipeMappingDataList, function (data) {

        mappedData = result.DishRecipeMappingData;
        for (var d of mappedData) {
            mappedRecipeCode.push(d.RecipeCode);
        }

        //}, { DishCode: DishCode }, false);

        //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {
        recipeCustomData = result.RecipeData;

        recipedata = result.RecipeData;
        populateRecipeMultiSelect();
        recipeCustomData = recipeCustomData.filter(function (item) {
            for (var d of mappedData) {
                if (d.RecipeCode == item.RecipeCode) {
                    item.IsDefault = d.IsDefault;
                    item.DishCode = d.DishCode;

                    multiarray.push(item.RecipeCode);
                    return item;
                }
            }
        })
        //}, { condition: cond, DishCode: DishCode }, false);

        //Krish 12-08-2022
        var multiselect = $("#inputrecipelist").data("kendoMultiSelect");
        //clear filter
        multiselect.dataSource.filter({});
        //set value
        multiselect.value(multiarray);
        //Krish 12-08-2022
        populateRecipeGridCustom();

    }, { dishCode: DishCode, condition: cond }, false);
    //mappedRecipeCode = [];
    //HttpClient.MakeSyncRequest(CookBookMasters.GetDishRecipeMappingDataList, function (data) {

    //    mappedData = data;
    //    for (var d of mappedData) {
    //        mappedRecipeCode.push(d.RecipeCode);
    //    }

    //}, { DishCode: DishCode }, false);

    //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {
    //    recipeCustomData = data;

    //    recipedata = data;
    //    populateRecipeMultiSelect();
    //    recipeCustomData = recipeCustomData.filter(function (item) {
    //        for (var d of mappedData) {
    //            if (d.RecipeCode == item.RecipeCode) {
    //                item.IsDefault = d.IsDefault;
    //                item.DishCode = d.DishCode;

    //                multiarray.push(item.RecipeCode);
    //                return item;
    //            }
    //        }
    //    })
    //}, { condition: cond, DishCode: DishCode }, false);



}

function populateSubSectorDropdown() {
    $("#inputsubsector").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "SubSectorCode",
        dataSource: subsectordata,
        index: 0,
    });
}


function SetDishColor(e) {
    var clrdiv = $("#" + e);
    var clr = clrdiv.css("background-color");
    $(".colortagedit").css("background-color", clr);
    $(".colortagedit").val(e);
}

$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;

    var dishcategorydata = [];
    //var dishsubcategorydata = [];

    var dishtypedata = [];
    var texturedata = [];
    //var uomdata = [];
    var servingtemperaturedata = [];
    var cuisinedata = [];
    Utility.Loading();
    $("#windowEditDishsectorcate").kendoWindow({
        modal: true,
        width: "740px",
        height: "350px",
        title: "Dish Details - " + DishName,
        actions: ["Close"],
        visible: false,
        resizable: false
    });

    $("#windowEditDishImpactedItems").kendoWindow({
        modal: true,
        width: "1050px",
        height: "470px",
        left: '220px !important',
        top: '80px !important',
        title: "Item Detials - " + DishName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#windowEdit1").kendoWindow({
        modal: true,
        width: "1050px",
        height: "470px",
        left: "269.5px",
        top: "79px",
        // title: "Item Detials - " + DishName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $('#myInputDish').on('input', function (e) {
        var grid = $('#gridDish').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "DishCode" || x.field == "Name" || x.field == "DishAlias" || x.field == "UOMName" || x.field == "DishCategoryName"
                    || x.field == "DishTypeName" || x.field == "DietCategoryName" || x.field == "Status" || x.field == "CookingRangeTypeCodeNameFormatted" || x.field == "ServingRangeTypeCodeNameFormatted") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == "int") {
                        //Krish 25-10-2022
                        var targetValue = e.target.value;    
                        if (x.field == "RangeTypeMapped") {
                            var pendingString = "not mapped";
                            var mappedString = "mapped";
                            if (pendingString==e.target.value.toLowerCase()) {
                                targetValue = 0;
                            }
                            else if (mappedString==e.target.value.toLowerCase()) {
                                targetValue = 1;
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: targetValue
                        })
                    }
                    if (type == 'string') {
                        var targetValue = e.target.value;                        
                        if (x.field == "Status") {
                            var pendingString = "recipe not mapped";
                            var savedString = "saved";
                            var mappedString = "recipe mapped";
                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "3";
                            }
                        }
                       
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });



    $(document).ready(function () {

        $(".k-window").hide();
        $(".k-overlay").hide();
        Utility.Loading();
        onloadSetDishData();
        //setTimeout(function () {

        //   // Utility.UnLoading();
        //}, 5000);
    });

    function configureUISectorWise() {
        if (user.SectorNumber == '80') {
            $(".hcSectorHide").css('display', 'none');
        }
        else {
            $(".hcSectorHide").css('display', 'inline-block');
        }
    }

    function onloadSetDishData() {
        $("#btnExport").click(function (e) {

            setTimeout(Utility.Loading(), 5000)
            var grid = $("#gridDish").data("kendoGrid");

            setTimeout(function () {
                Utility.UnLoading();
                grid.saveAsExcel();
            }, 1000)
        });

        //Krish
        //13-07-2022
        //HttpClient.MakeRequest(CookBookMasters.GetApplicationSettingDataList, function (result) {

        //    if (result != null) {
        //        applicationSettingData = result;
        //        //for (item of applicationSettingData) {
        //        //    if (item.Code == "FLX-00004")//item.Name == "Major Identification Threshold" || 
        //        //        MajorPercentData = item.Value;
        //        //    $(".asPercent").text(MajorPercentData);
        //        //}

        //    }
        //    else {

        //    }
        //}, null

        //    , false);

        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNewDish").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNewDish").css("display", "none");

            //}
            
            //Krish 11-10-2022
            if (user.UserRoleId == 14) {
                //alert(user.UserRoleId);
                $("#InitiateBulkChanges").css("display", "none");      
                $("#AddNewDish").css("display", "none");              
                $("#btnExport").css("display", "none");      
                
            }
            if (user.SectorNumber == "20") {
                $(".mfgHide").hide();
            }
            else {
                $(".mfgHide").show();
            }
            //Krish 02-08-2022
            HttpClient.MakeRequest(CookBookMasters.GetSectorItemDataList, function (result) {
                console.log(result);
                //GetApplicationSettingDataList
                if (result.ApplicationSettingData != null) {
                    applicationSettingData = result.ApplicationSettingData;
                    //for (item of applicationSettingData) {
                    //    if (item.Code == "FLX-00004")//item.Name == "Major Identification Threshold" || 
                    //        MajorPercentData = item.Value;
                    //    $(".asPercent").text(MajorPercentData);
                    //}
                }

                //GetDietCategoryDataList
                var dataSource = result.DietCategoryData;
                dietcategorydata = [];
                dietcategorydata.push({ "value": "Select", "text": "Select Diet Category" });
                for (var i = 0; i < dataSource.length; i++) {
                    dietcategorydata.push({ "value": dataSource[i].DietCategoryCode, "text": dataSource[i].Name });
                }
                populateDietCategoryDropdown_Bulk();
                populateDietCategoryDropdown();
                populateDietCategoryDropdownFilter();

                //DishList
                dishdata = result.DishData;
                populateDishGrid();


                // Kitchen Section List
                kitchenSectionMasterDataSource = [];
                kitchenSectionMasterDataSource.push({ "value": "Select", "text": "Select Kitchen Section" });
                kitchenSectionMasterDataSource.push({ "value": "KTC-00001", "text": "Commissary & Salads" });
                kitchenSectionMasterDataSource.push({ "value": "KTC-00002", "text": "Hot Kitchen Veg" });
                populateKitchenSectionDropdown();


                if (user.SectorNumber == "80") {
                    // Menu Type List
                    $(".sectorhealthcareonly").show();
                    menuTypeMasterDataSource = [];
                     menuTypeMasterDataSource.push({ "value": "Select", "text": "Select Menu Type" });
                    menuTypeMasterDataSource.push({ "value": "MT1-001", "text": "IPD" });
                    menuTypeMasterDataSource.push({ "value": "MT1-002", "text": "RSO" });
                    menuTypeMasterDataSource.push({ "value": "MT1-003", "text": "Both" });
                    populateMenuTypeMasterDropdown();

                    // Patient Billing Profile List
                    patientBillingProfileMasterDataSource = [];
                    // patientBillingProfileMasterDataSource.push({ "value": "Select", "text": "Select Patient Billing Profile" });
                    patientBillingProfileMasterDataSource.push({ "value": "PBP-00001", "text": "IND" });
                    patientBillingProfileMasterDataSource.push({ "value": "PBP-00002", "text": "CGHS" });
                    patientBillingProfileMasterDataSource.push({ "value": "PBP-00003", "text": "INTL" });
                    populatePatientBillingProfileMasterDropdown();
                }
                else {
                    $(".sectorhealthcareonly").hide();
                }

                Utility.UnLoading();
            }, { onLoad: true }, false);

            if (user.SectorNumber == "10") {

                $(".googleonly").show();
                //Krish Comment 02-08-2022 
                //HttpClient.MakeRequest(CookBookMasters.GetCuisineDataList, function (data) {

                //    var dataSource = data;
                //    cuisinedata = [];
                //    cuisinedata.push({ "value": "Select", "text": "Select" });
                //    for (var i = 0; i < dataSource.length; i++) {
                //        cuisinedata.push({ "value": dataSource[i].CuisineCode, "text": dataSource[i].Name });
                //    }
                //    console.log(cuisinedata);
                //    populateCuisineDropdown();
                //}, null, false);

                //HttpClient.MakeSyncRequest(CookBookMasters.GetSubSectorMasterList, function (data) {

                //    if (data.length > 1) {
                //        isSubSectorApplicable = true;
                //        data.unshift({ "SubSectorCode": 'Select', "Name": "Select" })
                //        subsectordata = data;
                //        populateSubSectorDropdown();
                //    }
                //    else {
                //        isSubSectorApplicable = false;
                //    }
                //}, { isAllSubSector: true }, false);
            }
            else {
                $(".googleonly").hide();
            }



            if (user.UserRoleId == 13) {
                $(".hcSectorHide").hide();
                $(".healthcareonly").show();

                //Krish Comment 02-08-2022
                //HttpClient.MakeRequest(CookBookMasters.GetHealthTagMasterList, function (data) {
                //    var dataSource = data;
                //    healthTagMasterDataSource = [];
                //    for (var i = 0; i < dataSource.length; i++) {
                //        if (dataSource[i].IsActive) {
                //            healthTagMasterDataSource.push({
                //                "value": dataSource[i].HealthTagCode, "text": dataSource[i].Name, "HealthTagCode": dataSource[i].HealthTagCode
                //            });
                //        }
                //    }
                //    populateHealthTagMasterDropdown();

                //}, null, true);

                //HttpClient.MakeRequest(CookBookMasters.GetLifeStyleTagMasterList, function (data) {
                //    var dataSource = data;
                //    lifeStyleTagMasterDataSource = [];
                //    for (var i = 0; i < dataSource.length; i++) {
                //        if (dataSource[i].IsActive) {
                //            lifeStyleTagMasterDataSource.push({
                //                "value": dataSource[i].LifeStyleTagCode, "text": dataSource[i].Name, "LifeStyleTagCode": dataSource[i].LifeStyleTagCode
                //            });
                //        }
                //    }
                //    populateLifeStyleTagMasterDropdown();

                //}, null, true);

                //HttpClient.MakeRequest(CookBookMasters.GetDietTypeMasterList, function (data) {
                //    var dataSource = data;
                //    diettypeMasterDataSource = [];
                //    for (var i = 0; i < dataSource.length; i++) {
                //        if (dataSource[i].IsActive) {
                //            diettypeMasterDataSource.push({
                //                "value": dataSource[i].DietTypeCode, "text": dataSource[i].Name, "DietTypeCode": dataSource[i].DietTypeCode
                //            });
                //        }
                //    }
                //    populateDietTypeMasterDropdown();

                //}, null, true);
            }
            else {
                $(".healthcareonly").hide();
                $(".hcSectorHide").show();
            }
            //populateDishGrid();
        }, null, false);

        //Krish Comment 02-08-2022
        //HttpClient.MakeRequest(CookBookMasters.GetColorDataList, function (data) {

        //    var dataSource = data;
        //    colordata = [];

        //    for (var i = 0; i < dataSource.length; i++) {
        //        colordata.push({ "value": dataSource[i].ColorCode, "text": dataSource[i].HexCode, "title": dataSource[i].Name });
        //    }

        //}, null, false);

        //HttpClient.MakeRequest(CookBookMasters.GetDietCategoryDataList, function (data) {

        //    var dataSource = data;
        //    dietcategorydata = [];
        //    dietcategorydata.push({ "value": "Select", "text": "Select Diet Category" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        dietcategorydata.push({ "value": dataSource[i].DietCategoryCode, "text": dataSource[i].Name });
        //    }
        //    populateDietCategoryDropdown_Bulk();
        //    populateDietCategoryDropdown();
        //    populateDietCategoryDropdownFilter();
        //}, null, false);

        //HttpClient.MakeRequest(CookBookMasters.GetDishCategoryDataList, function (data) {

        //    var dataSource = data;
        //    dishcategorydata = [];
        //    dishcategorydata.push({ "value": "Select", "text": "Select" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        //if (!dataSource[i].IsActive)
        //        //    continue;
        //        dishcategorydata.push({ "value": dataSource[i].DishCategoryCode, "text": dataSource[i].Name, "DishCategoryID": dataSource[i].ID, "IsActive": dataSource[i].IsActive });
        //    }
        //    populateDishCategoryDropdown();
        //    populateDishCategoryDropdown_Bulk();
        //}, null, false);

        ////HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

        ////    var dataSource = data;
        ////    uomdata = [];
        ////    uomdata.push({ "value": "Select", "text": "Select" });
        ////    for (var i = 0; i < dataSource.length; i++) {
        ////        uomdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].Name });
        ////    }
        ////    populateUOMDropdown();

        ////    populateUOMDropdown_Bulk();
        ////}, null, false);

        //HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
        //    var dataSource = data;
        //    uommodulemappingdata = [];
        //    uommodulemappingdata.push({ "value": "Select", "text": "Select" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        if (dataSource[i].IsActive) {
        //            uommodulemappingdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].UOMName, "UOMModuleCode": dataSource[i].UOMModuleCode });
        //        }
        //    }
        //    uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00003' || m.text == "Select");
        //    populateUOMDropdown();
        //    populateUOMDropdown_Bulk();

        //}, null, false);

        //HttpClient.MakeRequest(CookBookMasters.GetDishTypeDataList, function (data) {

        //    var dataSource = data;
        //    dishtypedata = [];
        //    dishtypedata.push({ "value": "Select", "text": "Select" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        dishtypedata.push({ "value": dataSource[i].DishTypeCode, "text": dataSource[i].Name });
        //    }
        //    populateDishTypeDropdown();
        //    populateDishTypeDropdown_Bulk();
        //}, null, false);

        //HttpClient.MakeRequest(CookBookMasters.GetTextureMasterList, function (data) {

        //    var dataSource = data;
        //    texturedata = [];
        //    texturedata.push({ "value": "Select", "text": "Select" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        texturedata.push({ "value": dataSource[i].TextureCode, "text": dataSource[i].Name });
        //    }
        //    populateTextureDropdown();
        //}, null, false);

        //HttpClient.MakeRequest(CookBookMasters.GetServingTemperatureDataList, function (data) {

        //    var dataSource = data;
        //    servingtemperaturedata = [];
        //    servingtemperaturedata.push({ "value": "Select", "text": "Select" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        servingtemperaturedata.push({ "value": dataSource[i].ServingTemparatureCode, "text": dataSource[i].Name });
        //    }
        //    populateServingTemperatureDropdown();
        //}, null, false);




        $("#gridBulkChange").css("display", "none");
        populateBulkChangeControls();
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        changeControls.css("background-color", "#fff");
        changeControls.css("border", "none");
        changeControls.eq(2).text("");
        changeControls.eq(2).append("<div id='dietcategory' style='width:100%; font-size:10.5px!important'></div>");
        changeControls.eq(3).text("");
        changeControls.eq(3).append("<div id='uom' style='width:100%; font-size:10.5px!important'></div>");
        changeControls.eq(5).text("");
        changeControls.eq(5).append("<div id='dishcategory' style='width:100%; font-size:10.5px!important'></div>");
        //changeControls.eq(5).text("");
        //changeControls.eq(5).append("<div id='dishsubcategory' style='width:100%; font-size:10.5px!important'></div>");
        changeControls.eq(6).text("");
        changeControls.eq(6).append("<div id='dishtype' style='width:100%; font-size:10.5px!important'></div>");
    }




    $("#InitiateBulkChanges").on("click", function () {
        //populateBulkChangeControls();
        //var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        //changeControls.css("background-color", "#fff");
        //changeControls.css("border", "none");
        $("#gridBulkChange").css("display", "block");
        $("#gridBulkChange").children(".k-grid-header").css("border", "none");
        $("#InitiateBulkChanges").css("display", "none");
        $("#CancelBulkChanges").css("display", "inline");
        $("#SaveBulkChanges").css("display", "inline");

        //populateDietCategoryDropdown_Bulk();
        //populateDishCategoryDropdown_Bulk();
        //populateDishSubCategoryDropdown_Bulk();
        //populateUOMDropdown_Bulk();
        //populateDishTypeDropdown_Bulk();

    });

    function populateDietCategoryDropdown_Bulk() {
        $("#dietcategory").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: dietcategorydata,
            index: 0,
        });
    }

    function populateDishCategoryDropdown_Bulk() {
        $("#dishcategory").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: dishcategorydata,
            index: 0,
            change: function (e) {
                if (!e.sender.dataItem()?.IsActive) {
                    toastr.error("Selected Dish Category is In-active");
                    var dropdownlist = $("#inputdishcategory").data("kendoDropDownList");
                    dropdownlist.select("");
                }
            }

        });
    }


    function populateDishTypeDropdown_Bulk() {
        $("#dishtype").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: dishtypedata,
            index: 0,
        });
    }

    function populateDietCategoryDropdown() {
        if (user.UserRoleId != 13) {
            $("#sectordishcateinputdietcategory").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: dietcategorydata,
                index: 0,
            });
        }
    }

    function populateKitchenSectionDropdown() {
        if (user.UserRoleId != 13) {
            $("#inputkitchensection").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: kitchenSectionMasterDataSource,
                index: 0,
            });
        }
    }

    function populateDishCategoryDropdown() {
        if (user.UserRoleId != 13) {
            $("#inputdishcategory").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: dishcategorydata,
                index: 0,
                change: function (e) {
                    if (!e.sender.dataItem()?.IsActive) {
                        toastr.error("Selected Dish Category is In-active");
                        var dropdownlist = $("#inputdishcategory").data("kendoDropDownList");
                        dropdownlist.select("");
                    }

                }
            });
        }
    }



    function populateUOMDropdown() {
        $("#inputuomsectordish").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: uomdata,
            index: 0,
            //change: function (e) {
            //    //if (!e.sender.dataItem()?.IsActive) {
            //    //    toastr.error("Selected UOM Category");
            //        var dropdownlist = $("#inputuomsectordish").data("kendoDropDownList");
            //        dropdownlist.select("");
            //   // }

            //}
        });
    }

    function populateDishSubCategoryDropdown() {
        $("#inputdishsubcategory").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: dishsubcategorydata,
            index: 0,
        });
    }



    function populateCuisineDropdown() {
        $("#inputcuisine").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: cuisinedata,
            index: 0,
        });
    }

    function populateServingTemperatureDropdown() {
        $("#inputservingtemperature").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: servingtemperaturedata,
            index: 0,
        });
    }

    function populateDishTypeDropdown() {
        if (user.UserRoleId != 13) {
            $("#inputdishtype").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: dishtypedata,
                index: 0,
            });
        }
    }

    function populateTextureDropdown() {
        $("#inputtexture").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: texturedata,
            index: 0,
        });
    }

    //$("#inputcpu").data("kendoDropDownList").select(function (dataItem) {
    //    return dataItem.value === dataSource[0].value;
    //});
    //$("#inputcpu").data("kendoDropDownList").select(0);

    $("#CancelBulkChanges").on("click", function () {
        $("#InitiateBulkChanges").css("display", "inline");
        $("#CancelBulkChanges").css("display", "none");
        $("#SaveBulkChanges").css("display", "none");
        $("#gridBulkChange").css("display", "none");
        $("#bulkerror").css("display", "none");
        $("#success").css("display", "none");
        // populateDishGrid();
    });

    $(".colortable td").on("click", function () {

    });

    $("#SaveBulkChanges").on("click", function () {

        var neVal = $(this).val();
        var dataFiltered = $("#gridDish").data("kendoGrid").dataSource.dataFiltered();


        var model = dataFiltered;
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");

        var validChange = false;
        var dietcategory = "";
        var dishcategory = "";
        var dishsubcategory = "";
        var uom = "";
        var dishtype = "";

        //  changeControls.eq(4).children("span").children("span").children(".k-input");
        var dietcategoryspan = $("#dietcategory").data("kendoDropDownList").value();
        var dishcategoryspan = $("#dishcategory").data("kendoDropDownList").value();
        //var dishsubcategoryspan = $("#dishsubcategory").data("kendoDropDownList").text();
        var uomspan = $("#uom").data("kendoDropDownList").value();
        var dishtypespan = $("#dishtype").data("kendoDropDownList").value();

        if (dietcategoryspan != "Select") {
            validChange = true;
            dietcategory = dietcategoryspan;
        }
        if (dishcategoryspan != "Select") {
            validChange = true;
            dishcategory = dishcategoryspan;
        }
        //if (dishsubcategoryspan.val() != "Select") {
        //    validChange = true;
        //    dishsubcategory = dishsubcategoryspan.val();
        //}
        if (uomspan != "Select") {
            validChange = true;
            uom = uomspan;
        }
        if (dishtypespan != "Select") {
            validChange = true;
            dishtype = dishtypespan;
        }
        if (validChange == false) {
            toastr.error("Please change at least one attribute for Bulk Update");
        } else {
            Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                function () {

                    $.each(model, function () {
                        this.CreatedOn = kendo.parseDate(this.CreatedOn);
                        if (dietcategory != "")
                            this.DietCategoryCode = dietcategory;
                        if (dishcategory != "")
                            this.DishCategoryCode = dishcategory;
                        //if (dishsubcategory != "")
                        //    this.DishSubCategoryCode = dishsubcategory;
                        if (uom != "")
                            this.UOMCode = uom;
                        if (dishtype != "")
                            this.DishTypeCode = dishtype;
                    });

                    HttpClient.MakeSyncRequest(CookBookMasters.SaveDishDataList, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again.");
                        }
                        else {
                            $(".k-overlay").hide();
                            toastr.success("Records have been updated successfully");
                            //populateDishGrid('ref');
                            $("#gridDish").data("kendoGrid").dataSource.data([]);
                            $("#gridDish").data("kendoGrid").dataSource.read();
                            $("#InitiateBulkChanges").css("display", "inline");
                            $("#CancelBulkChanges").css("display", "none");
                            $("#SaveBulkChanges").css("display", "none");
                            $("#gridBulkChange").css("display", "none");
                        }
                    }, {
                        model: model

                    }, true);

                    // populateSiteGrid();
                    Utility.UnLoading();
                },
                function () {
                    //dietcategoryspan.focus();
                }
            );
        }

    });
    //Food Program Section Start

    function populateDishGrid(ref) {


        var gridVariable = $("#gridDish");
        gridVariable.html("");

        if (user.UserRoleId == 13) {
            gridVariable.kendoGrid({
                excel: {
                    fileName: "Dish.xlsx",
                    filterable: false,
                    allPages: true
                },
                //selectable: "cell",
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                groupable: false,
                pageable: false,
                //height:485,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    {
                        field: "DishCode", title: "Dish Code", width: "50px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "Name", title: "Dish Name", width: "80px", attributes: {
                            style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                        },
                        template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:Name#</span>',
                    },
                    //{
                    //    field: "DishAlias", title: "Dish Alias", width: "60px", attributes: {
                    //        style: "text-align: left; font-weight:normal"
                    //    }
                    //},

                    {
                        field: "UOMName", title: "UOM", width: "40px", attributes: {

                            style: "text-align: left; font-weight:normal; padding-left:35px"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                    },
                    {
                        field: "ColorCode", title: "Color", width: "30px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                        template: '<div class="colortag" style="background-color:#: RGBCode #"></div>',
                    },
                    //{
                    //    field: "DietCategoryName", title: "Diet Category", width: "35px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center"
                    //    },
                    //    template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}#',
                    //},
                    {
                        field: "DishCategoryName", title: "Dish Category", width: "70px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },

                    {
                        field: "DishTypeName", title: "Type", width: "40px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                        headerAttributes: {

                            style: "text-align: left"
                        },
                    },


                    {
                        field: "DietCategoryName", title: "", width: "1px", attributes: {

                            style: "text-align: center; font-weight:normal;"
                        },
                        headerAttributes: {
                            style: "text-align: center; width:1px; display:none;"
                        },
                        template: '<span class="itemname"></span>',
                    },
                    {
                        field: "Edit", title: "Action", width: "60px",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },

                        headerAttributes: {
                            style: "text-align: center"
                        },
                        command: [
                            {
                                name: 'Edit',
                                click: function (e) {
                                    //Krish 02-08-2022
                                    AddEdit();
                                    //Nutritionist
                                    var gridObj = $("#gridDish").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    DishName = tr.Name;
                                    setTimeout(function () {
                                        //Krish
                                        //21-06-2022
                                        editNutrionistDish(tr);
                                    }, 1000);
                                    //var gridObj = $("#gridDish").data("kendoGrid");
                                    //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    //datamodel = tr;
                                    //DishName = tr.Name;
                                    //var dialog = $("#windowEditDishsectorcate").data("kendoWindow");
                                    ////$("#windowEditDish").kendoWindow({
                                    ////  animation: false
                                    ////});
                                    //$(".k-overlay").css("display", "block");
                                    //$(".k-overlay").css("opacity", "0.5");
                                    //dialog.open();
                                    //dialog.center();
                                    //$("#inputdishname").prop("disabled", true);
                                    //$("#inputdishalias").prop("disabled", true);
                                    //var sectordishcateinputdietcategory = $("#sectordishcateinputdietcategory").data("kendoDropDownList");
                                    //sectordishcateinputdietcategory.enable(false);

                                    //var inputdishcategory = $("#inputdishcategory").data("kendoDropDownList");
                                    //inputdishcategory.enable(false);

                                    //var inputuomsectordish = $("#inputuomsectordish").data("kendoDropDownList");
                                    //inputuomsectordish.enable(false);

                                    //var inputdishtype = $("#inputdishtype").data("kendoDropDownList");
                                    //inputdishtype.enable(false);

                                    ////var inputtexture = $("#inputtexture").data("kendoDropDownList");
                                    ////inputtexture.enable(false);

                                    //$(".isHealthCare").css("display", "none");
                                    //$("#dishid").val(tr.ID);
                                    //$("#inputdishcode").val(tr.DishCode);
                                    //$("#inputdishname").val(tr.Name).focus();
                                    //$("#inputdishalias").val(tr.DishAlias);
                                    //$("#sectordishcateinputdietcategory").data('kendoDropDownList').value(tr.DietCategoryCode);
                                    //$("#inputdishcategory").data('kendoDropDownList').value(tr.DishCategoryCode);
                                    //$("#inputuomsectordish").data('kendoDropDownList').value(tr.UOMCode);
                                    //$("#inputdishtype").data('kendoDropDownList').value(tr.DishTypeCode);
                                    //$("#inputtexture").data('kendoDropDownList').value(tr.TextureCode);
                                    //$("#issensitive").prop('checked', tr.IsSensitive);
                                    //$("#allergenname").text(tr.AllergensName);
                                    //$("#ngallergenname").text(tr.NGAllergens);
                                    //if (tr.IsSensitive == null || tr.IsSensitive == false) {
                                    //    $("#inputsensitive").attr('disabled', 'disabled');
                                    //} else {
                                    //    $("#inputsensitive").removeAttr('disabled');
                                    //}

                                    //$("#inputsensitive").val(tr.SensitiveNote);


                                    //PopulateColorPalette(tr.ColorCode, tr.RGBCode);
                                    //debugger
                                    //dialog.setOptions({
                                    //    height: 380,
                                    //});

                                    //    var HealthTagdata = [];
                                    //    var multiselect = $("#inputHealthTag").data("kendoMultiSelect");
                                    //    multiselect.dataSource.filter({});
                                    //    multiselect.value([""]);
                                    //    if (tr.HealthTagCode != undefined && tr.HealthTagCode != null) {
                                    //        tr.HealthTagCode = tr.HealthTagCode.replaceAll(" ", "");
                                    //        var array = tr.HealthTagCode.split(',');
                                    //        if (tr.HealthTagCode != undefined && tr.HealthTagCode != null) {
                                    //            for (health of array) {
                                    //                HealthTagdata.push(health.toString());
                                    //            }
                                    //        }
                                    //        multiselect.value(HealthTagdata);
                                    //    }

                                    //    var LifeStyleTagdata = [];
                                    //    var multiselect = $("#inputLifeStyleTag").data("kendoMultiSelect");
                                    //    multiselect.dataSource.filter({});
                                    //    multiselect.value([""]);
                                    //if (tr.LifeStyleTagCode != undefined && tr.LifeStyleTagCode != null) {
                                    //    tr.LifeStyleTagCode = tr.LifeStyleTagCode.replaceAll(" ", "");
                                    //    var array = tr.LifeStyleTagCode.split(',');
                                    //    if (tr.LifeStyleTagCode != undefined && tr.LifeStyleTagCode != null) {
                                    //        for (lifestyle of array) {
                                    //            LifeStyleTagdata.push(lifestyle.toString());
                                    //        }
                                    //    }
                                    //    multiselect.value(LifeStyleTagdata);
                                    //}

                                    //var DietTypedata = [];
                                    //var multiselect = $("#inputDietType").data("kendoMultiSelect");
                                    //multiselect.dataSource.filter({});
                                    //multiselect.value([""]);
                                    //if (tr.DietTypeCode != undefined && tr.DietTypeCode != null) {
                                    //    tr.DietTypeCode = tr.DietTypeCode.replaceAll(" ", "");
                                    //    var array = tr.DietTypeCode.split(',');
                                    //    if (tr.DietTypeCode != undefined && tr.DietTypeCode != null) {
                                    //        for (diettype of array) {
                                    //            DietTypedata.push(diettype.toString());
                                    //        }
                                    //    }
                                    //    multiselect.value(DietTypedata);
                                    //}

                                    //dialog.title("Dish Details - " + DishName);
                                    return true;
                                }
                            },
                            //{
                            //    name: 'Nutrition Data',
                            //    click: function (e) {

                            //        var gridObj = $("#gridDish").data("kendoGrid");
                            //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            //        datamodel = tr;
                            //        DishName = tr.Name;
                            //        selTagData = tr.TagData;
                            //        if (tr.NutritionData != null && tr.NutritionData != "" && tr.NutritionData != undefined) {
                            //            selDishNutritionData = JSON.parse(tr.NutritionData);
                            //        }
                            //        $(".k-overlay").hide();
                            //        var orderWindow = $("#windowEditNutritionInfo").data("kendoWindow");
                            //        var ntrString = "";
                            //        $("#ndDishName").text(DishName);
                            //        if (selDishNutritionData != null && selDishNutritionData.length > 0) {
                            //            // selDishNutritionData = selDishNutritionData.filter(m => m.Quantity > 0);
                            //            if (selTagData == null || selTagData == "" || selTagData == undefined) {

                            //            }
                            //            else {
                            //                ntrString += '<p>' + selTagData + '</p>';
                            //            }

                            //            const key = 'TypeCode';
                            //            const ntrtype = [...new Map(selDishNutritionData.map(item =>
                            //                [item[key], item])).values()];

                            //            if (ntrtype != null && ntrtype.length > 0) {
                            //                ntrtype.forEach(function (item, index) {
                            //                    if (index == 0) {
                            //                        ntrString += "<h5>" + item.TypeName + ":</h5><table>";
                            //                    }
                            //                    else {
                            //                        ntrString += "<br><h5>" + item.TypeName + ":</h5><table>";
                            //                    }

                            //                    var twiseNtr = selDishNutritionData.filter(m => m.TypeCode == item.TypeCode);
                            //                    twiseNtr.forEach(function (nitem, index) {
                            //                        ntrString += " <tr><td width='250px'> " + nitem.ElementName + ":</td><td width='100px'> " + nitem.Quantity + " </td><td>" + nitem.UOMName + "</td></tr>";
                            //                    });
                            //                    ntrString += "</table>";

                            //                });
                            //            }
                            //            $("#nutritionDetail").html(ntrString);
                            //        }
                            //        else {
                            //            ntrString += '<h3 style="text-align: center;margin-top:20px;"> Nutrition Data not found for ' + DishName + ' Dish. </p>';
                            //            $("#nutritionDetail").html(ntrString);
                            //        }
                            //        orderWindow.open().element.closest(".k-window").css({
                            //            top: 167,

                            //        });
                            //        orderWindow.title("Dish  Details : " + DishName + "");
                            //        orderWindow.center();
                            //        return true;
                            //    }
                            //}
                        ],
                    }
                ],
                //selectable: true,
                pageable: {
                    pageSize: 150
                },
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";
                            //Krish 02-08-2022
                            HttpClient.MakeSyncRequest(CookBookMasters.GetDishDataList, function (result) {
                                if (result != null) {
                                    dishdata = result;
                                    //result = result.filter(m=> m.CreatedSector == user.SectorNumber);
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , true);
                            //if (ref != undefined) {
                            //    HttpClient.MakeSyncRequest(CookBookMasters.GetDishDataList, function (result) {
                            //        if (result != null) {
                            //            dishdata = [];
                            //            dishdata = result;
                            //            //result = result.filter(m=> m.CreatedSector == user.SectorNumber);
                            //            options.success(result);
                            //        }
                            //        else {
                            //            options.success("");
                            //        }
                            //    }, null
                            //        //{
                            //        //filter: mdl
                            //        //}
                            //        , true);
                            //}
                            //else
                            //    options.success(dishdata);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                DishCode: { type: "string", editable: false },
                                Name: { type: "string", editable: false },
                                DishAlias: { type: "string", editable: false },
                                UOMName: { type: "string", editable: false },
                                DishCategoryName: { type: "string", editable: false },
                                DishTypeName: { type: "string", editable: false },
                                DietCategoryName: { type: "string", editable: false },
                                IsActive: { type: "boolean", editable: false },
                                Status: { type: "string", editable: false },
                                ModifiedOn: { type: "date" },
                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if (view[i].Status == 1) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#ffe1e1");

                        }
                        else if (view[i].Status == 2) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#ffffbf");
                        }
                        else {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#d9ffb3");
                        }

                    }
                    var items = e.sender.items();

                    //items.each(function (e) {

                    //    if (user.UserRoleId == 1) {
                    //        $(this).find('.k-grid-Edit').text("Edit");
                    //    } else {
                    //        $(this).find('.k-grid-Edit').text("View");
                    //    }
                    //});
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
        }
        //Krish 07-10-2022
        else if (user.UserRoleId === 14) {
            gridVariable.kendoGrid({
                excel: {
                    fileName: "Dish.xlsx",
                    filterable: false,
                    allPages: true
                },
                //selectable: "cell",
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                groupable: false,
                pageable: false,
                //height:485,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    {
                        field: "DishCode", title: "Dish Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "Name", title: "Dish Name", width: "80px", attributes: {
                            style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                        },
                        template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:Name#</span>',
                    },
                    {
                        field: "DishAlias", title: "Dish Alias", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },

                    //{
                    //    field: "UOMName", title: "UOM", width: "30px", attributes: {

                    //        style: "text-align: left; font-weight:normal; padding-left:35px"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center"
                    //    },
                    //},
                    {
                        field: "ColorCode", title: "Color", width: "30px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                        template: '<div class="colortag" style="background-color:#: RGBCode #"></div>',
                    },
                    //{
                    //    field: "DietCategoryName", title: "Diet Category", width: "35px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center"
                    //    },
                    //    template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}#',
                    //},
                    {
                        field: "DishCategoryName", title: "Dish Category", width: "70px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },

                    {
                        field: "DishTypeName", title: "Type", width: "30px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {

                            style: "text-align: center"
                        },
                    },
                    //{
                    //    field: "ImpactedItems", title: "Impacts", width: "50px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {

                    //        style: "text-align: center"

                    //    },
                    //    template: function (dataItem) {
                    //        if (dataItem.ImpactedItems == "0") {
                    //            return '<div>' + kendo.htmlEncode(dataItem.ImpactedItems) + ' Item</div>';;

                    //        }
                    //        else if (dataItem.ImpactedItems == "1") {
                    //            return '<a href="#" onClick="getDishImpactedItems(this)" class="" title="Click for Item Details" style"color:#8f9090; margin-left:8px">' + kendo.htmlEncode(dataItem.ImpactedItems) + ' Item</a>';;

                    //        }
                    //        else {
                    //            return '<a href="#" onClick="getDishImpactedItems(this)" class="" title="Click for Item Details" style"color:#8f9090; margin-left:8px">' + kendo.htmlEncode(dataItem.ImpactedItems) + ' Item(s)</a>';;

                    //        }
                    //    }

                    //    //template: '##<a href = "#" onClick = "getDishImpactedItems(this)" title = "Click to Edit Item Details" style = 'color:#8f9090; margin-left:8px' > dataItem.ImpactedItems Item</ a >
                    //    //template: '##<a onclick="getDishImpactedItems()>"#:ImpactedItems# Item</a>##',
                    //},
                    //{
                    //    field: "BoughtFromOutSide", title: "OutSide", width: "25px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {

                    //        style: "text-align: center"
                    //    },
                    //},
                    //{
                    //    field: "ModifiedOn", title: "Last Updated", width: "40px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    template: '# if (ModifiedOn == null) {#<span>-</span>#} else{#<span>#: kendo.toString(ModifiedOn, "dd-MMM-yy")#</span>#}#',
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    }
                    //},
                    //{
                    //    field: "RangeTypeMapped", title: "Range Type Status", width: "80px", attributes: {
                    //        class: "mycustomstatus",
                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    template: '# if (RangeTypeMapped == 0) {#<span>Not Mapped</span>#} else {#Mapped#}#',
                    //    headerAttributes: {

                    //        style: "text-align: center"
                    //    },
                    //},
                    {
                        field: "CookingRangeTypeCodeNameFormatted", title: "Cooking Range Type", width: "80px", attributes: {
                            //class: "mycustomstatus",
                            style: "text-align: center; font-weight:normal"
                        },
                        //template: '# if (RangeTypeMapped == 0) {#<span>Not Mapped</span>#} else {#Mapped#}#',
                        headerAttributes: {

                            style: "text-align: center"
                        },
                    },
                    {
                        field: "ServingRangeTypeCodeNameFormatted", title: "Serving Range Type", width: "80px", attributes: {
                            //class: "mycustomstatus",
                            style: "text-align: center; font-weight:normal"
                        },
                       // template: '# if (RangeTypeMapped == 0) {#<span>Not Mapped</span>#} else {#Mapped#}#',
                        headerAttributes: {

                            style: "text-align: center"
                        },
                    },
                    {
                        field: "DietCategoryName", title: "", width: "1px", attributes: {

                            style: "text-align: center; font-weight:normal;"
                        },
                        headerAttributes: {
                            style: "text-align: center; width:1px; display:none;"
                        },
                        template: '<span class="itemname"></span>',
                    },
                    {
                        field: "Edit", title: "Action", width: "60px",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },

                        headerAttributes: {
                            style: "text-align: center"
                        },
                        command: [
                            {
                                name: 'Map Temp',
                                click: function (e) {
                                    Utility.Loading();
                                    //Krish 02-08-2022
                                    AddEdit();
                                    setTimeout(function () {
                                        var gridObj = $("#gridDish").data("kendoGrid");
                                        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                        datamodel = tr;
                                        DishName = tr.Name;
                                        $("#rtDishCode").text(tr.DishCode);
                                        $("#rtDishName").text(tr.Name);
                                        $("#rtDishAlias").text(tr.DishAlias);
                                        $("#rtDietCategory").text(tr.DietCategoryName);
                                        $("#rtUom").text(tr.UOMName);
                                        $("#rtDishCategory").text(tr.DishCategoryName);
                                        $("#rtRecipe").text();
                                        //alert(RecipeName);
                                        if ($("#rtRangeTypeC").data('kendoDropDownList') != undefined) {
                                            //console.log(tr)
                                            if (tr.CookingRangeTypeCode!=null)
                                                $("#rtRangeTypeC").data('kendoDropDownList').value(tr.CookingRangeTypeCode);
                                            else
                                                $("#rtRangeTypeC").data('kendoDropDownList').value("Select");
                                        }
                                        if ($("#rtRangeTypeS").data('kendoDropDownList') != undefined) {
                                            //console.log(tr)
                                            if (tr.ServingRangeTypeCode != null)
                                                $("#rtRangeTypeS").data('kendoDropDownList').value(tr.ServingRangeTypeCode);
                                            else
                                                $("#rtRangeTypeS").data('kendoDropDownList').value("Select");
                                        }
                                        var dialog = $("#RangeTypeDishwindowEdit").data("kendoWindow");

                                        $(".k-overlay").css("display", "block");
                                        $(".k-overlay").css("opacity", "0.5");
                                        dialog.open().element.closest(".k-window").css({
                                            top: 167,

                                        });
                                        //dialog.open();
                                        dialog.center();
                                        dialog.title(tr.Name);
                                        //var cuisineText = " Cuisine: " + tr.CuisineName + " | ";
                                        //$("#inputcuisinelabel").text(cuisineText);
                                        //if (tr.Name != tr.DishAlias && tr.DishAlias != "" && tr.DishAlias != null)
                                        //    $("#inputdishname1").text(tr.Name + "(" + tr.DishAlias + ")");
                                        //else
                                        //    $("#inputdishname1").text(tr.Name);
                                        //$("#inputdishtype1").text(tr.DishTypeName);
                                        //$("#inputdishcategory1").text(tr.DishCategoryName);
                                        //DishCode = tr.DishCode;
                                        //if (tr.DishTypeName == 'Base Recipe')
                                        //    cond = "TBASE"
                                        //else
                                        //    cond = 'TALL'
                                        //Dish_ID = tr.ID;
                                        //$("#ItemMaster").hide();
                                        //$("#mapRecipe").show();
                                        //if (tr.DietCategoryName == "Veg") {
                                        //    $("#itemDietCategoryStatus").removeClass("statusNonVeg statusEgg");
                                        //    $("#itemDietCategory").removeClass("squareNonVeg squareEgg");
                                        //    $("#itemDietCategoryStatus").addClass("statusVeg");
                                        //    $("#itemDietCategory").addClass("squareVeg")
                                        //} else if (tr.DietCategoryName == "Non-Veg") {
                                        //    $("#itemDietCategoryStatus").removeClass("statusVeg statusEgg");
                                        //    $("#itemDietCategory").removeClass("squareVeg squareEgg");
                                        //    $("#itemDietCategory").addClass("squareNonVeg")
                                        //    $("#itemDietCategoryStatus").addClass("statusNonVeg")
                                        //} else {
                                        //    $("#itemDietCategoryStatus").removeClass("statusNonVeg statusVeg");
                                        //    $("#itemDietCategory").removeClass("squareNonVeg squareVeg");
                                        //    $("#itemDietCategory").addClass("squareEgg")
                                        //    $("#itemDietCategoryStatus").addClass("statusEgg")
                                        //}
                                        //$("#iname").text(tr.Name + " (Dish Code: " + tr.DishCode + ")");
                                        //$("#topHeading").text("Sector Dish Configuration - Map Recipe");
                                        ////call and Grid genration
                                        //multiarray = [];
                                        ////Krish 12-08-2022
                                        ////populateRecipeGridCustom();
                                        //customData(DishCode, cond);
                                        Utility.UnLoading();
                                    }, 2000);
                                }
                            }

                        ],
                    }
                ],
                //selectable: true,
                pageable: {
                    pageSize: 150
                },
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";
                            //Krish 02-08-2022
                            HttpClient.MakeSyncRequest(CookBookMasters.GetDishDataList, function (result) {
                                console.log(result)
                                if (result != null) {
                                    dishdata = result;
                                    //result = result.filter(m => m.CreatedSector == user.SectorNumber);
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , true);
                            //if (ref != undefined) {
                            //    HttpClient.MakeSyncRequest(CookBookMasters.GetDishDataList, function (result) {

                            //        if (result != null) {
                            //            dishdata = [];
                            //            dishdata = result;
                            //            //result = result.filter(m => m.CreatedSector == user.SectorNumber);
                            //            options.success(result);
                            //        }
                            //        else {
                            //            options.success("");
                            //        }
                            //    }, null
                            //        //{
                            //        //filter: mdl
                            //        //}
                            //        , true);
                            //}
                            //else
                            //    options.success(dishdata);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                DishCode: { type: "string", editable: false },
                                Name: { type: "string", editable: false },
                                DishAlias: { type: "string", editable: false },
                                UOMName: { type: "string", editable: false },
                                DishCategoryName: { type: "string", editable: false },
                                DishTypeName: { type: "string", editable: false },
                                DietCategoryName: { type: "string", editable: false },
                                IsActive: { type: "boolean", editable: false },
                                //Status: { type: "string", editable: false },
                                ModifiedOn: { type: "date" },
                                //RangeTypeCode: { type: "string", editable: false },
                                //RangeTypeName: { type: "string", editable: false },
                                //RangeTypeMapped: { type: "int", editable: false }
                                CookingRangeTypeCode: { type: "string", editable: false },
                                ServingRangeTypeCode: { type: "string", editable: false },
                                CookingRangeTypeCodeNameFormatted: { type: "string", editable: false },
                                ServingRangeTypeCodeNameFormatted: { type: "string", editable: false },
                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if (view[i].RangeTypeMapped !=0) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#ffe1e1");

                        }
                        //else if (view[i].Status == 2) {
                        //    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        //        .find(".mycustomstatus").css("background-color", "#ffffbf");
                        //}
                        else {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#d9ffb3");
                        }

                    }
                    var items = e.sender.items();

                    //items.each(function (e) {

                    //    if (user.UserRoleId == 1) {
                    //        $(this).find('.k-grid-Edit').text("Edit");
                    //    } else {
                    //        $(this).find('.k-grid-Edit').text("View");
                    //    }
                    //});
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            });

            //Krish 11-10-2022
            if (user.UserRoleId == 14) {
                //alert(user.UserRoleId);
                $("#InitiateBulkChanges").css("display", "none");
                $("#AddNewDish").css("display", "none");
                $("#btnExport").css("display", "none");
                //$("#gridDish").data("kendoGrid").dataSource.sort({ field: "Code", dir: "asc" });
                $("#gridDish").data("kendoGrid").dataSource.sort({ field: "DishCode", dir: "asc" });
            }
        }
        else {
            gridVariable.kendoGrid({
                excel: {
                    fileName: "Dish.xlsx",
                    filterable: false,
                    allPages: true
                },
                //selectable: "cell",
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                groupable: false,
                pageable: false,
                //height:485,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    {
                        field: "DishCode", title: "Dish Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "Name", title: "Dish Name", width: "80px", attributes: {
                            style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                        },
                        template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:Name#</span>',
                    },
                    {
                        field: "DishAlias", title: "Dish Alias", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },

                    //{
                    //    field: "UOMName", title: "UOM", width: "30px", attributes: {

                    //        style: "text-align: left; font-weight:normal; padding-left:35px"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center"
                    //    },
                    //},
                    {
                        field: "ColorCode", title: "Color", width: "30px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                        template: '<div class="colortag" style="background-color:#: RGBCode #"></div>',
                    },
                    //{
                    //    field: "DietCategoryName", title: "Diet Category", width: "35px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center"
                    //    },
                    //    template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}#',
                    //},
                    {
                        field: "DishCategoryName", title: "Dish Category", width: "70px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },

                    {
                        field: "DishTypeName", title: "Type", width: "30px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {

                            style: "text-align: center"
                        },
                    },
                    {
                        field: "ImpactedItems", title: "Impacts", width: "50px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {

                            style: "text-align: center"

                        },
                        template: function (dataItem) {
                            if (dataItem.ImpactedItems == "0") {
                                return '<div>' + kendo.htmlEncode(dataItem.ImpactedItems) + ' Item</div>';;

                            }
                            else if (dataItem.ImpactedItems == "1") {
                                return '<a href="#" onClick="getDishImpactedItems(this)" class="" title="Click for Item Details" style"color:#8f9090; margin-left:8px">' + kendo.htmlEncode(dataItem.ImpactedItems) + ' Item</a>';;

                            }
                            else {
                                return '<a href="#" onClick="getDishImpactedItems(this)" class="" title="Click for Item Details" style"color:#8f9090; margin-left:8px">' + kendo.htmlEncode(dataItem.ImpactedItems) + ' Item(s)</a>';;

                            }
                        }

                        //template: '##<a href = "#" onClick = "getDishImpactedItems(this)" title = "Click to Edit Item Details" style = 'color:#8f9090; margin-left:8px' > dataItem.ImpactedItems Item</ a >
                        //template: '##<a onclick="getDishImpactedItems()>"#:ImpactedItems# Item</a>##',
                    },
                    //{
                    //    field: "BoughtFromOutSide", title: "OutSide", width: "25px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {

                    //        style: "text-align: center"
                    //    },
                    //},
                    {
                        field: "ModifiedOn", title: "Last Updated", width: "40px", attributes: {

                            style: "text-align: center; font-weight:normal"
                        },
                        template: '# if (ModifiedOn == null) {#<span>-</span>#} else{#<span>#: kendo.toString(ModifiedOn, "dd-MMM-yy")#</span>#}#',
                        headerAttributes: {
                            style: "text-align: center;"
                        }
                    },
                    {
                        field: "Status", title: "Status", width: "80px", attributes: {
                            class: "mycustomstatus",
                            style: "text-align: center; font-weight:normal"
                        },
                        template: '# if (Status == "1") {#<span>Recipe Not Mapped</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Recipe Mapped#}#',
                        headerAttributes: {

                            style: "text-align: center"
                        },
                    },
                    {
                        field: "DietCategoryName", title: "", width: "1px", attributes: {

                            style: "text-align: center; font-weight:normal;"
                        },
                        headerAttributes: {
                            style: "text-align: center; width:1px; display:none;"
                        },
                        template: '<span class="itemname"></span>',
                    },
                    {
                        field: "Edit", title: "Action", width: "60px",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },

                        headerAttributes: {
                            style: "text-align: center"
                        },
                        command: [
                            {
                                name: 'Edit',
                                click: function (e) {
                                    //Krish 02-08-2022
                                    AddEdit();
                                    setTimeout(function () {
                                        var gridObj = $("#gridDish").data("kendoGrid");
                                        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                        datamodel = tr;
                                        DishName = tr.Name;
                                        var dialog = $("#windowEditDishsectorcate").data("kendoWindow");
                                        //$("#windowEditDish").kendoWindow({
                                        //  animation: false
                                        //});
                                        $(".k-overlay").css("display", "block");
                                        $(".k-overlay").css("opacity", "0.5");
                                        dialog.open();
                                        dialog.center();



                                        $("#dishid").val(tr.ID);
                                        $("#inputdishcode").val(tr.DishCode);
                                        $("#inputdishname").val(tr.Name).focus();
                                        $("#inputdishalias").val(tr.DishAlias);
                                        $("#sectordishcateinputdietcategory").data('kendoDropDownList').value(tr.DietCategoryCode);
                                        $("#inputdishcategory").data('kendoDropDownList').value(tr.DishCategoryCode);
                                        //if (tr.DishSubCategoryCode!=null)
                                        // $("#inputdishsubcategory").data('kendoDropDownList').value(tr.DishSubCategoryCode);
                                        $("#inputuomsectordish").data('kendoDropDownList').value(tr.UOMCode);
                                        $("#inputdishtype").data('kendoDropDownList').value(tr.DishTypeCode);
                                        $("#inputtexture").data('kendoDropDownList').value(tr.TextureCode);
                                        //$("#inputservingtemperature").data('kendoDropDownList').value(tr.ServingTemperatureCode);
                                        if (user.SectorNumber == "10") {
                                            $("#inputcuisine").data('kendoDropDownList').value(tr.CuisineCode);
                                        }
                                        $("#issensitive").prop('checked', tr.IsSensitive);

                                        if (tr.IsSensitive == null || tr.IsSensitive == false) {
                                            $("#inputsensitive").attr('disabled', 'disabled');
                                        } else {
                                            $("#inputsensitive").removeAttr('disabled');
                                        }

                                        $("#inputsensitive").val(tr.SensitiveNote);

                                        PopulateColorPalette(tr.ColorCode, tr.RGBCode);
                                        // debugger;
                                        if (user.UserRoleId == 13) {
                                            dialog.setOptions({
                                                height: 440,
                                            });
                                            var HealthTagdata = [];
                                            var multiselect = $("#inputHealthTag").data("kendoMultiSelect");
                                            multiselect.dataSource.filter({});
                                            multiselect.value([""]);
                                            if (tr.HealthTagCode != undefined && tr.HealthTagCode != null) {
                                                tr.HealthTagCode = tr.HealthTagCode.replaceAll(" ", "");
                                                var array = tr.HealthTagCode.split(',');
                                                if (tr.HealthTagCode != undefined && tr.HealthTagCode != null) {
                                                    for (health of array) {
                                                        HealthTagdata.push(health.toString());
                                                    }
                                                }
                                                multiselect.value(HealthTagdata);
                                            }

                                            var LifeStyleTagdata = [];
                                            var multiselect = $("#inputLifeStyleTag").data("kendoMultiSelect");
                                            multiselect.dataSource.filter({});
                                            multiselect.value([""]);
                                            if (tr.LifeStyleTagCode != undefined && tr.LifeStyleTagCode != null) {
                                                tr.LifeStyleTagCode = tr.LifeStyleTagCode.replaceAll(" ", "");
                                                var array = tr.LifeStyleTagCode.split(',');
                                                if (tr.LifeStyleTagCode != undefined && tr.LifeStyleTagCode != null) {
                                                    for (lifestyle of array) {
                                                        LifeStyleTagdata.push(lifestyle.toString());
                                                    }
                                                }
                                                multiselect.value(LifeStyleTagdata);
                                            }

                                            var DietTypedata = [];
                                            var multiselect = $("#inputDietType").data("kendoMultiSelect");
                                            multiselect.dataSource.filter({});
                                            multiselect.value([""]);
                                            if (tr.DietTypeCode != undefined && tr.DietTypeCode != null) {
                                                tr.DietTypeCode = tr.DietTypeCode.replaceAll(" ", "");
                                                var array = tr.DietTypeCode.split(',');
                                                if (tr.DietTypeCode != undefined && tr.DietTypeCode != null) {
                                                    for (diettype of array) {
                                                        DietTypedata.push(diettype.toString());
                                                    }
                                                }
                                                multiselect.value(DietTypedata);
                                            }
                                        }
                                        else {
                                            dialog.setOptions({
                                                height: 230,
                                            });

                                        }
                                        if (user.SectorNumber == "10") {
                                            $("#inputsubsector").data('kendoDropDownList').value("Select");
                                            if (tr.SubSectorCode)
                                                $("#inputsubsector").data('kendoDropDownList').value(tr.SubSectorCode);
                                        }

                                        //$("#inputcalorie").val(tr.Calorie);

                                        //if (user.UserRoleId === 1) {
                                        //    $("#inputdishname").removeAttr('disabled');
                                        //    $("#sectordishcateinputdietcategory").removeAttr('disabled');
                                        //    $("#inputdishcategory").removeAttr('disabled');
                                        //    $("#inputdishsubcategory").removeAttr('disabled');
                                        //    $("#inputuom").removeAttr('disabled');
                                        //    $("#inputdishtype").removeAttr('disabled');
                                        //    $("#inputservingtemperature").removeAttr('disabled');
                                        //    //$("#inputcalorie").removeAttr('disabled');
                                        //    $("#btnSubmit").css('display', 'inline');
                                        //    $("#btnCancel").css('display', 'inline');
                                        //} else {
                                        //    $("#inputdishname").attr('disabled', 'disabled');
                                        //    $("#sectordishcateinputdietcategory").attr('disabled', 'disabled');
                                        //    $("#inputdishcategory").attr('disabled', 'disabled');
                                        //    $("#inputdishsubcategory").attr('disabled', 'disabled');
                                        //    $("#inputuom").attr('disabled', 'disabled');
                                        //    $("#inputdishtype").attr('disabled', 'disabled');
                                        //    $("#inputservingtemperature").attr('disabled', 'disabled');
                                        //    //$("#inputcalorie").attr('disabled', 'disabled');
                                        //    $("#btnSubmit").css('display', 'none');
                                        //    $("#btnCancel").css('display', 'none');
                                        //}

                                        dialog.title("Dish Details - " + DishName);
                                        return true;
                                    }, 1000);
                                }
                            },
                            {
                                name: 'Map Recipe',
                                click: function (e) {
                                    Utility.Loading();
                                    //Krish 02-08-2022
                                    AddEdit();
                                    setTimeout(function () {
                                        var gridObj = $("#gridDish").data("kendoGrid");
                                        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                        datamodel = tr;
                                        DishName = tr.Name;
                                        $("#dishid").val(tr.ID);
                                        $("#inputdishcode1").text(tr.DishCode);
                                        var cuisineText = " Cuisine: " + tr.CuisineName + " | ";
                                        $("#inputcuisinelabel").text(cuisineText);
                                        if (tr.Name != tr.DishAlias && tr.DishAlias != "" && tr.DishAlias != null)
                                            $("#inputdishname1").text(tr.Name + "(" + tr.DishAlias + ")");
                                        else
                                            $("#inputdishname1").text(tr.Name);
                                        $("#inputdishtype1").text(tr.DishTypeName);
                                        $("#inputdishcategory1").text(tr.DishCategoryName);
                                        DishCode = tr.DishCode;
                                        if (tr.DishTypeName == 'Base Recipe')
                                            cond = "TBASE"
                                        else
                                            cond = 'TALL'
                                        Dish_ID = tr.ID;
                                        $("#ItemMaster").hide();
                                        $("#mapRecipe").show();
                                        if (tr.DietCategoryName == "Veg") {
                                            $("#itemDietCategoryStatus").removeClass("statusNonVeg statusEgg");
                                            $("#itemDietCategory").removeClass("squareNonVeg squareEgg");
                                            $("#itemDietCategoryStatus").addClass("statusVeg");
                                            $("#itemDietCategory").addClass("squareVeg")
                                        } else if (tr.DietCategoryName == "Non-Veg") {
                                            $("#itemDietCategoryStatus").removeClass("statusVeg statusEgg");
                                            $("#itemDietCategory").removeClass("squareVeg squareEgg");
                                            $("#itemDietCategory").addClass("squareNonVeg")
                                            $("#itemDietCategoryStatus").addClass("statusNonVeg")
                                        } else {
                                            $("#itemDietCategoryStatus").removeClass("statusNonVeg statusVeg");
                                            $("#itemDietCategory").removeClass("squareNonVeg squareVeg");
                                            $("#itemDietCategory").addClass("squareEgg")
                                            $("#itemDietCategoryStatus").addClass("statusEgg")
                                        }
                                        $("#iname").text(tr.Name + " (Dish Code: " + tr.DishCode + ")");
                                        $("#topHeading").text("Sector Dish Configuration - Map Recipe");
                                        //call and Grid genration
                                        multiarray = [];
                                        //Krish 12-08-2022
                                        //populateRecipeGridCustom();
                                        customData(DishCode, cond);
                                        Utility.UnLoading();
                                    }, 500);
                                }
                            }
                        ],
                    }
                ],
                //selectable: true,
                pageable: {
                    pageSize: 150
                },
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";
                            //Krish 02-08-2022
                            HttpClient.MakeSyncRequest(CookBookMasters.GetDishDataList, function (result) {

                                if (result != null) {
                                    dishdata = result;
                                    //result = result.filter(m => m.CreatedSector == user.SectorNumber);
                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , true);
                            //if (ref != undefined) {
                            //    HttpClient.MakeSyncRequest(CookBookMasters.GetDishDataList, function (result) {

                            //        if (result != null) {
                            //            dishdata = [];
                            //            dishdata = result;
                            //            //result = result.filter(m => m.CreatedSector == user.SectorNumber);
                            //            options.success(result);
                            //        }
                            //        else {
                            //            options.success("");
                            //        }
                            //    }, null
                            //        //{
                            //        //filter: mdl
                            //        //}
                            //        , true);
                            //}
                            //else
                            //    options.success(dishdata);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                DishCode: { type: "string", editable: false },
                                Name: { type: "string", editable: false },
                                DishAlias: { type: "string", editable: false },
                                UOMName: { type: "string", editable: false },
                                DishCategoryName: { type: "string", editable: false },
                                DishTypeName: { type: "string", editable: false },
                                DietCategoryName: { type: "string", editable: false },
                                IsActive: { type: "boolean", editable: false },
                                Status: { type: "string", editable: false },
                                ModifiedOn: { type: "date" },
                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if (view[i].Status == 1) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#ffe1e1");

                        }
                        else if (view[i].Status == 2) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#ffffbf");
                        }
                        else {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#d9ffb3");
                        }

                    }
                    var items = e.sender.items();

                    //items.each(function (e) {

                    //    if (user.UserRoleId == 1) {
                    //        $(this).find('.k-grid-Edit').text("Edit");
                    //    } else {
                    //        $(this).find('.k-grid-Edit').text("View");
                    //    }
                    //});
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
        }
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function populateDietCategoryDropdownFilter() {

        $("#dietcategorysearch").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: dietcategorydata,
            index: 0,
            select: function (e) {
                if (e.item.data().offsetIndex == 0) {
                    //e.preventDefault();
                    var filter = { logic: 'or', filters: [] };
                    var grid = $('#gridDish').data('kendoGrid');
                    grid.dataSource.filter(filter);
                    return;
                }

                var grid = $('#gridDish').data('kendoGrid');
                var columns = grid.columns;

                var filter = { logic: 'or', filters: [] };
                columns.forEach(function (x) {
                    if (x.field) {
                        if (x.field == "DietCategoryName") {
                            var type = grid.dataSource.options.schema.model.fields[x.field].type;
                            var targetValue = e.sender.dataItem(e.item).text;

                            if (type == 'string') {
                                if (x.field == "Status") {
                                    var pendingString = "recipe not mapped";
                                    var savedString = "saved";
                                    var mappedString = "recipe mapped";
                                    if (pendingString.includes(targetValue.toLowerCase())) {
                                        targetValue = "1";
                                    }
                                    else if (savedString.includes(targetValue.toLowerCase())) {
                                        targetValue = "2";
                                    }
                                    else if (mappedString.includes(targetValue.toLowerCase())) {
                                        targetValue = "3";
                                    }
                                }
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: targetValue
                                })
                            }
                            else if (type == 'number') {

                                if (isNumeric(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: e.target.value
                                    });
                                }
                            } else if (type == 'date') {
                                var data = grid.dataSource.data();
                                for (var i = 0; i < data.length; i++) {
                                    var dateStr = kendo.format(x.format, data[i][x.field]);
                                    if (dateStr.startsWith(e.target.value)) {
                                        filter.filters.push({
                                            field: x.field,
                                            operator: 'eq',
                                            value: data[i][x.field]
                                        })
                                    }
                                }
                            } else if (type == 'boolean' && getBoolean(targetValue) !== null) {
                                var bool = getBoolean(e.target.value);
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: bool
                                });
                            }
                        }
                    }
                });
                grid.dataSource.filter(filter);

            }

        });
    }

    function PopulateColorPalette(colorcode, rgbcode) {

        var table = $(".colortable table");
        table.find("tr").remove();
        var tablehtml = "";
        tablehtml = "<tr>";
        for (var i = 1; i <= colordata.length; i++) {
            tablehtml += "<td><div title='" + colordata[i - 1].title + "' id='" + colordata[i - 1].value + "' style='border-radius:100%; height: 18px; width: 18px; background-color: " + colordata[i - 1].text + "' onclick='SetDishColor(\"" + colordata[i - 1].value + "\")'></div></td>";
            if (i % 9 == 0) {
                tablehtml += "</tr>";
                tablehtml += "<tr>";
            }
        }
        tablehtml += "</tr>";
        table.append(tablehtml);
        $(".colortagedit").css("background-color", rgbcode);
        $(".colortagedit").val(colorcode);
    }

    function populateBulkChangeControls() {

        var gridVariable = $("#gridBulkChange");
        gridVariable.html("");
        gridVariable.kendoGrid({
            //selectable: "cell",
            sortable: false,
            filterable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "", title: "", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "", title: "", width: "90px", attributes: {
                        style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                    },
                },
                {
                    field: "", title: "", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },

                {
                    field: "UOMName", title: "UOM", width: "40px", attributes: {

                        style: "text-align: left; font-weight:normal; padding-left:35px"
                    }
                },
                {
                    field: "", title: "", width: "35px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "50px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },

                {
                    field: "", title: "", width: "40px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "", title: "", width: "30px", attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "60px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                }
            ],
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
            },
            change: function (e) {
            },
        });
    }
    function populateRangeTypeDropdown() {
        
        $("#rtRangeTypeC").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: rangeTypeData,
            index: 0,
        });
        $("#rtRangeTypeS").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: rangeTypeData,
            index: 0,
        });
    }
    //Krish 02-08-2022
    function AddEdit() {
        //Krish 07-10-2022
        if (user.UserRoleId == 14) {
            //GetRangeTypeDataList
            HttpClient.MakeRequest(CookBookMasters.GetRangeTypeDataList, function (result) {
                rangeTypeData = [];
                rangeTypeData.push({ "value": "Select", "text": "Select" });
                //rangeTypeData = result;
                
                var dataSource = result.filter(a => a.IsActive == true);
                
                for (var i = 0; i < dataSource.length; i++) {
                    rangeTypeData.push({ "value": dataSource[i].Code, "text": dataSource[i].Name + " (" + dataSource[i].StdTempFrom + " - " + dataSource[i].StdTempTo + ")" });
                }
                
                populateRangeTypeDropdown();
            });
        }
        //GetSectorItemDataList
        HttpClient.MakeRequest(CookBookMasters.GetSectorItemDataList, function (result) {
            console.log(result);
            if (user.SectorNumber == "10") {

                //Krish  02-08-2022 
                //HttpClient.MakeRequest(CookBookMasters.GetCuisineDataList, function (data) {

                var dataSource = result.CuisineMasterData;
                cuisinedata = [];
                cuisinedata.push({ "value": "Select", "text": "Select" });
                for (var i = 0; i < dataSource.length; i++) {
                    cuisinedata.push({ "value": dataSource[i].CuisineCode, "text": dataSource[i].Name });
                }
                console.log(cuisinedata);
                populateCuisineDropdown();
                //}, null, false);

                //HttpClient.MakeSyncRequest(CookBookMasters.GetSubSectorMasterList, function (data) {
                var data = result.SubSectorData;
                if (data.length > 1) {
                    isSubSectorApplicable = true;
                    data.unshift({ "SubSectorCode": 'Select', "Name": "Select" })
                    subsectordata = data;
                    populateSubSectorDropdown();
                }
                else {
                    isSubSectorApplicable = false;
                }
                //}, { isAllSubSector: true }, false);
            }
            if (user.UserRoleId == 13) {

                //Krish  02-08-2022
                //HttpClient.MakeRequest(CookBookMasters.GetHealthTagMasterList, function (data) {
                var dataSource = result.HealthTagMasterData;
                healthTagMasterDataSource = [];
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i].IsActive) {
                        healthTagMasterDataSource.push({
                            "value": dataSource[i].HealthTagCode, "text": dataSource[i].Name, "HealthTagCode": dataSource[i].HealthTagCode
                        });
                    }
                }
                populateHealthTagMasterDropdown();

                //}, null, true);

                //HttpClient.MakeRequest(CookBookMasters.GetLifeStyleTagMasterList, function (data) {
                var dataSource = result.LifeStyleTagMasterData;
                lifeStyleTagMasterDataSource = [];
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i].IsActive) {
                        lifeStyleTagMasterDataSource.push({
                            "value": dataSource[i].LifeStyleTagCode, "text": dataSource[i].Name, "LifeStyleTagCode": dataSource[i].LifeStyleTagCode
                        });
                    }
                }
                populateLifeStyleTagMasterDropdown();

                //}, null, true);

                //HttpClient.MakeRequest(CookBookMasters.GetDietTypeMasterList, function (data) {
                var dataSource = result.DietTypeData;
                diettypeMasterDataSource = [];
                for (var i = 0; i < dataSource.length; i++) {
                    if (dataSource[i].IsActive) {
                        diettypeMasterDataSource.push({
                            "value": dataSource[i].DietTypeCode, "text": dataSource[i].Name, "DietTypeCode": dataSource[i].DietTypeCode
                        });
                    }
                }
                populateDietTypeMasterDropdown();

                //}, null, true);
            }

            //HttpClient.MakeRequest(CookBookMasters.GetColorDataList, function (data) {

            var dataSource = result.ColorData;
            colordata = [];

            for (var i = 0; i < dataSource.length; i++) {
                colordata.push({ "value": dataSource[i].ColorCode, "text": dataSource[i].HexCode, "title": dataSource[i].Name });
            }

            //}, null, false);

            //HttpClient.MakeRequest(CookBookMasters.GetDietCategoryDataList, function (data) {

            //    var dataSource = data;
            //    dietcategorydata = [];
            //    dietcategorydata.push({ "value": "Select", "text": "Select Diet Category" });
            //    for (var i = 0; i < dataSource.length; i++) {
            //        dietcategorydata.push({ "value": dataSource[i].DietCategoryCode, "text": dataSource[i].Name });
            //    }
            //    populateDietCategoryDropdown_Bulk();
            //    populateDietCategoryDropdown();
            //    populateDietCategoryDropdownFilter();
            //}, null, false);

            //HttpClient.MakeRequest(CookBookMasters.GetDishCategoryDataList, function (data) {

            var dataSource = result.DishCategoryData;
            dishcategorydata = [];
            dishcategorydata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                //if (!dataSource[i].IsActive)
                //    continue;
                dishcategorydata.push({ "value": dataSource[i].DishCategoryCode, "text": dataSource[i].Name, "DishCategoryID": dataSource[i].ID, "IsActive": dataSource[i].IsActive });
            }
            populateDishCategoryDropdown();
            populateDishCategoryDropdown_Bulk();
            //}, null, false);


            //HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
            var dataSource = result.UOMModuleMappingData;
            uommodulemappingdata = [];
            uommodulemappingdata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].IsActive) {
                    uommodulemappingdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].UOMName, "UOMModuleCode": dataSource[i].UOMModuleCode });
                }
            }
            uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00003' || m.text == "Select");
            populateUOMDropdown();
            populateUOMDropdown_Bulk();

            //}, null, false);

            //HttpClient.MakeRequest(CookBookMasters.GetDishTypeDataList, function (data) {

            var dataSource = result.DishTypeData;
            dishtypedata = [];
            dishtypedata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                dishtypedata.push({ "value": dataSource[i].DishTypeCode, "text": dataSource[i].Name });
            }
            populateDishTypeDropdown();
            populateDishTypeDropdown_Bulk();
            //}, null, false);

            //HttpClient.MakeRequest(CookBookMasters.GetTextureMasterList, function (data) {

            var dataSource = result.TextureData;
            texturedata = [];
            texturedata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                texturedata.push({ "value": dataSource[i].TextureCode, "text": dataSource[i].Name });
            }
            populateTextureDropdown();
            //}, null, false);

            //HttpClient.MakeRequest(CookBookMasters.GetServingTemperatureDataList, function (data) {

            var dataSource = result.ServingTemperatureData;
            servingtemperaturedata = [];
            servingtemperaturedata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                servingtemperaturedata.push({ "value": dataSource[i].ServingTemparatureCode, "text": dataSource[i].Name });
            }
            populateServingTemperatureDropdown();
            //}, null, false);


        }, { isAddEdit: true }, false);
    }
    $("#AddNewDish").on("click", function () {
        //Krish 02-08-2022
        AddEdit();
        setTimeout(function () {
            var dialog = $("#windowEditDishsectorcate").data("kendoWindow");

            $(".k-overlay").css("display", "block");
            $(".k-overlay").css("opacity", "0.5");
            dialog.open().element.closest(".k-window").css({
                top: 167,
            });
            dialog.center();
            datamodel = null;
            dialog.title("New Dish Creation");
            $("#dishid").val("");
            $("#inputdishcode").val("");
            $("#inputdishname").val("");
            $("#inputdishalias").val("");
            $("#sectordishcateinputdietcategory").data('kendoDropDownList').value("Select");
            $("#inputdishcategory").data('kendoDropDownList').value("Select");
            if (user.SectorNumber == "10") {
                $("#inputcuisine").data('kendoDropDownList').value("Select");
                $("#inputsubsector").data('kendoDropDownList').value("Select");
            }
            //$("#inputdishsubcategory").data('kendoDropDownList').value("Select");
            $("#inputuomsectordish").data('kendoDropDownList').value("Select");

            //$("#inputservingtemperature").val("Select");
            $(".colortagedit").css("background-color", "rgb(255,255,255,0)");
            $(".colortagedit").val("");
            $("#issensitive").prop('checked', false);
            $("#inputsensitive").attr('disabled', 'disabled');
            $("#inputsensitive").val("");
            PopulateColorPalette(null, "rgb(255,255,255)");
            //$("#inputcalorie").val("");
            //PopulateColorPalette(null, tr.RGBCode);
            $("#inputdishname").val("").focus();
            if (user.UserRoleId == 13) {
                dialog.setOptions({
                    height: 420,
                });
                $("#inputdishtype").data('kendoDropDownList').value("Select");
                $("#inputtexture").data('kendoDropDownList').value("Select");
                var multiSelect = $('#inputHealthTag').data("kendoMultiSelect");
                multiSelect.value([]);

                var multiSelectLifeStyleTag = $('#inputLifeStyleTag').data("kendoMultiSelect");
                multiSelectLifeStyleTag.value([]);

                var multiSelectDietType = $('#inputDietType').data("kendoMultiSelect");
                multiSelectDietType.value([]);
            }
        }, 1000);
    });

    $("#issensitive").on('change', function () {
        if ($("#issensitive")[0].checked == false) {
            $("#inputsensitive").attr('disabled', 'disabled');
        } else {
            $("#inputsensitive").removeAttr('disabled');
        }
    });

    $("#btnNutritionData").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditNutritionInfo").data("kendoWindow");
        var ntrString = "";
        $("#ndDishName").text(RecipeName);
        if (selDishNutritionData != null && selDishNutritionData.length > 0) {
            // selDishNutritionData = selDishNutritionData.filter(m => m.Quantity > 0);
            if (selTagData == null || selTagData == "" || selTagData == undefined) {

            }
            else {
                ntrString += '<p>' + selTagData + '</p>';
            }

            const key = 'TypeCode';
            const ntrtype = [...new Map(selDishNutritionData.map(item =>
                [item[key], item])).values()];

            if (ntrtype != null && ntrtype.length > 0) {
                ntrtype.forEach(function (item, index) {
                    if (index == 0) {
                        ntrString += "<h5>" + item.TypeName + ":</h5><table>";
                    }
                    else {
                        ntrString += "<br><h5>" + item.TypeName + ":</h5><table>";
                    }

                    var twiseNtr = selDishNutritionData.filter(m => m.TypeCode == item.TypeCode);
                    twiseNtr.forEach(function (nitem, index) {
                        ntrString += " <tr><td width='250px'> " + nitem.ElementName + ":</td><td width='100px'> " + nitem.Quantity + " </td><td>" + nitem.UOMName + "</td></tr>";
                    });
                    ntrString += "</table>";

                });
            }
            $("#nutritionDetail").html(ntrString);
        }
        else {
            ntrString += '<h3 style="text-align: center;margin-top:20px;"> Nutrition Data not found for ' + DishName + ' Dish. </p>';
            $("#nutritionDetail").html(ntrString);
        }
        orderWindow.open().element.closest(".k-window").css({
            top: 167,

        });
        orderWindow.center();

    });

    $("#btnCancelDish").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();

                var orderWindow = $("#windowEditDishsectorcate").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    //Krish
    //21-06-2022
    $("#btnCancelDishnutri").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
            function () {
                $("#divEditNutri").hide();
                $("#divMainContent").show();
            },
            function () {
            }
        );
    });
    //Krish 07-10-2022
    $("#rtbtnSubmit").on("click", function () {        
        var rangetypeC = $("#rtRangeTypeC").data("kendoDropDownList").value();
        var rangetypeS = $("#rtRangeTypeS").data("kendoDropDownList").value();
        if (rangetypeC == "Select") {
            toastr.error("Please select Cooking Range Type");
            return;
        }
        if (rangetypeS == "Select") {
            toastr.error("Please select Serving Range Type");
            return;
        }
        Utility.Loading();
        setTimeout(function () {
            $("#rtbtnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeSyncRequest(CookBookMasters.SaveRangeTypeDishData, function (result) {
                if (result == false) {
                    $('#rtbtnSubmit').removeAttr("disabled");
                    //$("#error").css("display", "flex");
                    toastr.error("Some error occured, please try again");
                    // $("#error").find("p").text("Some error occured, please try again");
                    $("#sitealias").focus();
                }
                else {
                    $(".k-overlay").hide();
                    //$("#error").css("display", "flex");
                    var orderWindow = $("#RangeTypeDishwindowEdit").data("kendoWindow");
                    orderWindow.close();
                    $('#rtbtnSubmit').removeAttr("disabled");
                    //$("#success").css("display", "flex");
                    //if (model.ID > 0) {
                    toastr.success("Updated successfully");
                    //}
                    //else {
                    //    toastr.success("New Range Type added successfully.");/
                    //}

                    // $("#success").find("p").text("Color configuration updated successfully");
                    //populateDishGrid();
                    $("#gridDish").data("kendoGrid").dataSource.data([]);
                    $("#gridDish").data("kendoGrid").dataSource.read();
                    $("#gridDish").data("kendoGrid").dataSource.sort({ field: "DishCode", dir: "asc" });
                }
                Utility.UnLoading();
            }, {
                dishCode: $("#rtDishCode").text(),
                rangeTypeCode: rangetypeC + ":" + rangetypeS

            }, true);
        }, 500);
        

    });
    $("#rtbtnCancel").on("click", function () {
        var orderWindow = $("#RangeTypeDishwindowEdit").data("kendoWindow");
        orderWindow.close();
        $("#rtRangeTypeC").data('kendoDropDownList').value("Select");
        $("#rtRangeTypeS").data('kendoDropDownList').value("Select");
    });

    //Krish
    //21-06-2022
    function editNutrionistDish(model) {
        var tr = model;
        $("#divEditNutri").show();
        $("#divMainContent").hide();

        HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDishDataByDish, function (data) {
            console.log(data);
            $("#recipeTab").html("");
            $("#recipeTab").kendoTabStrip();
            var tabstrip = $("#recipeTab").data("kendoTabStrip");
            if (data.length == 0) {
                var label = "<label class='form-control-label'>No Recipe Connected</label>";
                $("#recipeTab").html(label);
            }
            else {
                data.forEach(function (item, index) {
                    $("#gridMOGNutri").html("");
                    populateMOGGridNutri(item.RecipeMOG);
                    //Create HTML
                    if (item.IsDefault == true)
                        $("#recipeDetails .container").addClass('defaultRecipe');
                    else
                        $("#recipeDetails .container").removeClass('defaultRecipe');
                    var html = $("#recipeDetails").html();
                    html = html.replace("##RecipeCode##", item.RecipeCode);
                    html = html.replace("##RecipeName##", item.RecipeName);
                    html = html.replace("##Quantity##", item.Quantity);
                    html = html.replace("##RecipeType##", item.RecipeType);
                    html = html.replace("##Allergen##", item.Allergens);
                    html = html.replace("##AllergenNG##", item.NGAllergens);
                    //setTimeout(function () { 
                    //var gridHtml = $("#gridContent").html();
                    var gridHtml = $("#gridMOGNutri").html();
                    //html = html.replace("##RecipeGrid##", gridHtml);
                    html = html.replace("##RecipeGrid##", gridHtml);
                    //console.log(gridHtml)
                    var instr = item.Instructions;
                    if (item.Instructions == "null")
                        instr = "";
                    $("#instructionContent").html(instr);
                    html = html.replace("##Instructions##", $("#instructionContent").text());
                    //console.log(html)
                    tabstrip.append(
                        [{
                            text: item.RecipeName,
                            encoded: false,                             // Allows use of HTML for item text
                            content: html,                             // Content for the content element
                            //imageUrl: "https://demos.telerik.com/kendo-ui/content/shared/icons/sports/baseball.png" // Provides the image URL of the tab
                        }]
                    );
                    //}, 500);
                });
                tabstrip.select(0);
                //Krish 09-08-2022
                var defId = $(".defaultRecipe").parent().attr('id');
                $(".defaultRecipe").parent().addClass('defRecipeHighlighted');
                //console.log(defId);
                $(".k-tabstrip-items .k-item").each(function () {
                    var aria = $(this).attr("aria-controls");
                    //console.log(aria);
                    if (aria == defId)
                        $(this).find('.k-link').attr("style", "font-weight:bold");
                })
            }
            Utility.UnLoading();
        }, { dishCode: tr.DishCode }, true);

        //var dialog = $("#windowEditDishsectorcate").data("kendoWindow");
        //$("#windowEditDish").kendoWindow({
        //  animation: false
        //});
        //$(".k-overlay").css("display", "block");
        //$(".k-overlay").css("opacity", "0.5");
        //dialog.open();
        //dialog.center();
        $("#inputdishname").prop("disabled", true);
        $("#inputdishalias").prop("disabled", true);
        //var sectordishcateinputdietcategory = $("#sectordishcateinputdietcategory").data("kendoDropDownList");
        //sectordishcateinputdietcategory.enable(false);

        //var inputdishcategory = $("#inputdishcategory").data("kendoDropDownList");
        //inputdishcategory.enable(false);

        var inputuomsectordish = $("#inputuomsectordish").data("kendoDropDownList");
        inputuomsectordish.enable(false);

        //var inputdishtype = $("#inputdishtype").data("kendoDropDownList");
        //inputdishtype.enable(false);

        //var inputtexture = $("#inputtexture").data("kendoDropDownList");
        //inputtexture.enable(false);

        $(".isHealthCare").css("display", "none");
        $("#dishid").val(tr.ID);
        $("#inputdishcode").val(tr.DishCode);
        $("#txtinputdishcode").text(tr.DishCode);
        $("#inputdishname").val(tr.Name);
        $("#txtinputdishname").text(tr.Name);
        $("#inputdishalias").val(tr.DishAlias);
        $("#txtinputdishalias").text(tr.DishAlias);

        var getName = dietcategorydata.filter(a => a.value == tr.DietCategoryCode)[0].text;
        $("#sectordishcateinputdietcategory").val(tr.DietCategoryCode);
        $("#txtsectordishcateinputdietcategory").text(getName);
        getName = dishcategorydata.filter(a => a.value == tr.DishCategoryCode)[0].text;
        $("#inputdishcategory").val(tr.DishCategoryCode);
        $("#txtinputdishcategory").text(getName);
        getName = dishtypedata.filter(a => a.value == tr.DishTypeCode)[0].text;
        $("#inputdishtype").val(tr.DishTypeCode);
        $("#txtinputdishtype").text(getName);
        //$("#sectordishcateinputdietcategory").hide();
        //$("#inputdishcategory").hide();
        //$("#inputdishtype").hide();
        //$("#sectordishcateinputdietcategory").data('kendoDropDownList').value(tr.DietCategoryCode);
        //$("#inputdishcategory").data('kendoDropDownList').value(tr.DishCategoryCode);

        $("#inputuomsectordish").data('kendoDropDownList').value(tr.UOMCode);
        //$("#inputdishtype").data('kendoDropDownList').value(tr.DishTypeCode);
        $("#inputtexture").data('kendoDropDownList').value(tr.TextureCode);
        $("#issensitive").prop('checked', tr.IsSensitive);
        $("#allergenname").text(tr.AllergensName);
        $("#ngallergenname").text(tr.NGAllergens);
        if (tr.IsSensitive == null || tr.IsSensitive == false) {
            $("#inputsensitive").attr('disabled', 'disabled');
        } else {
            $("#inputsensitive").removeAttr('disabled');
        }

        $("#inputsensitive").val(tr.SensitiveNote);


        PopulateColorPalette(tr.ColorCode, tr.RGBCode);
        //debugger
        //dialog.setOptions({
        //    height: 380,
        //});

        var HealthTagdata = [];
        var multiselect = $("#inputHealthTag").data("kendoMultiSelect");

        multiselect.dataSource.filter({});
        multiselect.value([""]);
        if (tr.HealthTagCode != undefined && tr.HealthTagCode != null) {
            tr.HealthTagCode = tr.HealthTagCode.replaceAll(" ", "");
            var array = tr.HealthTagCode.split(',');
            if (tr.HealthTagCode != undefined && tr.HealthTagCode != null) {
                for (health of array) {
                    HealthTagdata.push(health.toString());
                }
            }
            multiselect.value(HealthTagdata);
        }

        var LifeStyleTagdata = [];
        var multiselect = $("#inputLifeStyleTag").data("kendoMultiSelect");
        multiselect.dataSource.filter({});
        multiselect.value([""]);
        if (tr.LifeStyleTagCode != undefined && tr.LifeStyleTagCode != null) {
            tr.LifeStyleTagCode = tr.LifeStyleTagCode.replaceAll(" ", "");
            var array = tr.LifeStyleTagCode.split(',');
            if (tr.LifeStyleTagCode != undefined && tr.LifeStyleTagCode != null) {
                for (lifestyle of array) {
                    LifeStyleTagdata.push(lifestyle.toString());
                }
            }
            multiselect.value(LifeStyleTagdata);
        }

        var DietTypedata = [];
        var multiselect = $("#inputDietType").data("kendoMultiSelect");
        multiselect.dataSource.filter({});
        multiselect.value([""]);
        if (tr.DietTypeCode != undefined && tr.DietTypeCode != null) {
            tr.DietTypeCode = tr.DietTypeCode.replaceAll(" ", "");
            var array = tr.DietTypeCode.split(',');
            if (tr.DietTypeCode != undefined && tr.DietTypeCode != null) {
                for (diettype of array) {
                    DietTypedata.push(diettype.toString());
                }
            }
            multiselect.value(DietTypedata);
        }

        //dialog.title("Dish Details - " + DishName);
        //$("#lblDishNutriTitle").text("Dish Details - " + DishName);
        $("#lblDishNutriTitle").text("Dish Details");
        //dialog.title("Dish Details - " + DishName);

        HttpClient.MakeRequest(CookBookMasters.GetCuisineDataList, function (data) {

            var dataSource = data;
            cuisinedata = [];
            cuisinedata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                cuisinedata.push({ "value": dataSource[i].CuisineCode, "text": dataSource[i].Name });
            }
            console.log(cuisinedata);
            populateCuisineDropdown();
        }, null, false);
    }
    function editNutrionistDish_BAK(model) {
        var tr = model;
        $("#divEditNutri").hide();
        $("#divMainContent").show();

        //var dialog = $("#windowEditDishsectorcate").data("kendoWindow");        
        //$(".k-overlay").css("display", "block");
        //$(".k-overlay").css("opacity", "0.5");
        //dialog.open();
        //dialog.center();
        $("#inputdishnamenutri").prop("disabled", true);
        $("#inputdishaliasnutri").prop("disabled", true);
        var sectordishcateinputdietcategory = $("#sectordishcateinputdietcategorynutri").data("kendoDropDownList");
        sectordishcateinputdietcategory.enable(false);

        var inputdishcategory = $("#inputdishcategorynutri").data("kendoDropDownList");
        inputdishcategory.enable(false);

        var inputuomsectordish = $("#inputuomsectordishnutri").data("kendoDropDownList");
        inputuomsectordish.enable(false);

        var inputdishtype = $("#inputdishtypenutri").data("kendoDropDownList");
        inputdishtype.enable(false);

        //var inputtexture = $("#inputtexture").data("kendoDropDownList");
        //inputtexture.enable(false);

        $(".isHealthCare").css("display", "none");
        $("#dishid").val(tr.ID);
        $("#inputdishcodenutri").val(tr.DishCode);
        $("#inputdishnamenutri").val(tr.Name).focus();
        $("#inputdishaliasnutri").val(tr.DishAlias);
        $("#sectordishcateinputdietcategorynutri").data('kendoDropDownList').value(tr.DietCategoryCode);
        $("#inputdishcategorynutri").data('kendoDropDownList').value(tr.DishCategoryCode);
        $("#inputuomsectordishnutri").data('kendoDropDownList').value(tr.UOMCode);
        $("#inputdishtypenutri").data('kendoDropDownList').value(tr.DishTypeCode);
        $("#inputtexturenutri").data('kendoDropDownList').value(tr.TextureCode);
        $("#issensitivenutri").prop('checked', tr.IsSensitive);
        $("#allergennamenutri").text(tr.AllergensName);
        $("#ngallergennamenutri").text(tr.NGAllergens);
        if (tr.IsSensitive == null || tr.IsSensitive == false) {
            $("#inputsensitivenutri").attr('disabled', 'disabled');
        } else {
            $("#inputsensitivenutri").removeAttr('disabled');
        }

        $("#inputsensitivenutri").val(tr.SensitiveNote);


        PopulateColorPalette(tr.ColorCode, tr.RGBCode);
        //debugger
        //dialog.setOptions({
        //    height: 380,
        //});

        var HealthTagdata = [];
        var multiselect = $("#inputHealthTagnutri").data("kendoMultiSelect");
        multiselect.dataSource.filter({});
        multiselect.value([""]);
        if (tr.HealthTagCode != undefined && tr.HealthTagCode != null) {
            tr.HealthTagCode = tr.HealthTagCode.replaceAll(" ", "");
            var array = tr.HealthTagCode.split(',');
            if (tr.HealthTagCode != undefined && tr.HealthTagCode != null) {
                for (health of array) {
                    HealthTagdata.push(health.toString());
                }
            }
            multiselect.value(HealthTagdata);
        }

        var LifeStyleTagdata = [];
        var multiselect = $("#inputLifeStyleTagnutri").data("kendoMultiSelect");
        multiselect.dataSource.filter({});
        multiselect.value([""]);
        if (tr.LifeStyleTagCode != undefined && tr.LifeStyleTagCode != null) {
            tr.LifeStyleTagCode = tr.LifeStyleTagCode.replaceAll(" ", "");
            var array = tr.LifeStyleTagCode.split(',');
            if (tr.LifeStyleTagCode != undefined && tr.LifeStyleTagCode != null) {
                for (lifestyle of array) {
                    LifeStyleTagdata.push(lifestyle.toString());
                }
            }
            multiselect.value(LifeStyleTagdata);
        }

        var DietTypedata = [];
        var multiselect = $("#inputDietTypenutri").data("kendoMultiSelect");
        multiselect.dataSource.filter({});
        multiselect.value([""]);
        if (tr.DietTypeCode != undefined && tr.DietTypeCode != null) {
            tr.DietTypeCode = tr.DietTypeCode.replaceAll(" ", "");
            var array = tr.DietTypeCode.split(',');
            if (tr.DietTypeCode != undefined && tr.DietTypeCode != null) {
                for (diettype of array) {
                    DietTypedata.push(diettype.toString());
                }
            }
            multiselect.value(DietTypedata);
        }
        $("#lblDishNutriTitle").text("Dish Details - " + DishName);
        //dialog.title("Dish Details - " + DishName);
    }
    //Krish
    //21-06-2022

    $("#btnSubmitDishnutri").click(function () {
        var dishName = $("#inputdishnamenutri").val();
        var dishAlias = $("#inputdishaliasnutri").val();
        if ($("#inputdishnamenutri").val() === "") {
            toastr.error("Please provide Dish Name");
            $("#inputdishnamenutri").focus();
            return;
        }
        //if (dishName === dishAlias) {
        //    toastr.error("Dish Name and Dish Alias should be different");
        //    return;
        //}
        if ($("#sectordishcateinputdietcategorynutri").val() === "Select") {
            toastr.error("Please select Diet Category");
            $("#sectordishcateinputdietcategorynutri").focus();
            return;
        }
        if ($("#inputuomsectordishnutri").val() === "Select") {
            toastr.error("Please select UOM");
            $("#inputuomsectordishnutri").focus();
            return;
        }
        if ($("#inputdishtypenutri").val() === "Select") {
            toastr.error("Please select Dish Type");
            $("#inputdishtypenutri").focus();
            return;
        }

        if ($("#inputdishcategorynutri").val() === "Select") {
            toastr.error("Please select Dish Category");
            $("#inputdishcategorynutri").focus();
            return;
        }
        if ($("#inputkitchensection").val() === "Select") {
            toastr.error("Please select Kitchen Section");
            $("#inputkitchensection").focus();
            return;
        }
        else {
            var localdishcategorydata = [];

            localdishcategorydata = dishcategorydata.filter(function (item) {
                return item.value == $("#inputdishcategorynutri").val();
            });

            var dishcategoryid = localdishcategorydata[0].DishCategoryID;
            if (user.UserRoleId == 13) {
                var healthTagdata = [];
                var healthTags = "";
                var dropdownlist = $("#inputHealthTagnutri").data("kendoMultiSelect");
                var dataItems = dropdownlist.dataItems();
                if (dataItems != undefined && dataItems != null) {
                    for (dataItem of dataItems) {
                        healthTagdata.push(dataItem.value);
                    }
                }

                var lifeStyleTagdata = [];
                var dropdownlistOther = $("#inputLifeStyleTagnutri").data("kendoMultiSelect");
                var dataItemsOther = dropdownlistOther.dataItems();
                if (dataItemsOther != undefined && dataItemsOther != null) {
                    for (dataItem of dataItemsOther) {
                        lifeStyleTagdata.push(dataItem.value);
                    }
                }

                var dietTypedata = [];
                var dropdownlistOther = $("#inputDietTypenutri").data("kendoMultiSelect");
                var dataItemsOther = dropdownlistOther.dataItems();
                if (dataItemsOther != undefined && dataItemsOther != null) {
                    for (dataItem of dataItemsOther) {
                        dietTypedata.push(dataItem.value);
                    }
                }

                
            }

            if (user.SectorNumber == "80") {
                var patientBillingTypedata = [];
                var dropdownlistOther = $("#inputPatientBilingProfile").data("kendoMultiSelect");
                var dataItemsOther = dropdownlistOther.dataItems();
                if (dataItemsOther != undefined && dataItemsOther != null) {
                    for (dataItem of dataItemsOther) {
                        patientBillingTypedata.push(dataItem.value);
                    }
                }
            }
            var model;
            if (datamodel != null) {
                model = datamodel;
                model.DishCode = $("#inputdishcodenutri").val();
                model.Name = $("#inputdishnamenutri").val();
                model.DishAlias = $("#inputdishaliasnutri").val();
                model.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
                model.CreatedBy = datamodel.CreatedBy;
                //model.DishSubCategoryCode= $("#inputdishsubcategory").val();
                model.DishTypeCode = $("#inputdishtypenutri").val();
                model.TextureCode = $("#inputtexturenutri").val();
                model.ColorCode = $(".colortagedit").val();
                model.IsSensitive = $("#issensitivenutri")[0].checked;
                model.SensitiveNote = $("#inputsensitivenutri").val();
                model.DietCategoryCode = $("#sectordishcateinputdietcategorynutri").val();
                model.DishCategoryCode = $("#inputdishcategorynutri").val();
              
                if (user.SectorNumber == "10") {
                    model.CuisineCode = $("#inputcuisinenutri").val();
                    model.SubSectorCode = $("#inputsubsector").val();
                }
                if (user.SectorNumber == "80") {
                    model.MenuType1Code = $("#inputDishMenuType").val();
                    model.PatientBillingProfileCode = patientBillingTypedata.toString();
                }
                model.KitchenSectionCode = $("#inputkitchensection").val();
                model.DishCategory_ID = dishcategoryid;
                model.UOMCode = $("#inputuomsectordishnutri").val();
                if (user.UserRoleId == 13) {
                    model.HealthTagCode = healthTagdata.toString();
                    model.LifeStyleTagCode = lifeStyleTagdata.toString();
                    model.DietTypeCode = dietTypedata.toString();
                }

                // model.ServingTemperatureCode = $("#inputservingtemperature").val();
            }
            else {
                model = {
                    "ID": $("#dishid").val(),
                    "DishCode": $("#inputdishcodenutri").val(),
                    "Name": $("#inputdishnamenutri").val(),
                    "DishAlias": $("#inputdishaliasnutri").val(),
                    "DietCategoryCode": $("#sectordishcateinputdietcategorynutri").val(),
                    "DishCategoryCode": $("#inputdishcategorynutri").val(),
                    "DishCategory_ID": dishcategoryid,
                    //"DishSubCategoryCode": $("#inputdishsubcategory").val(),
                    "UOMCode": $("#inputuomsectordishnutri").val(),
                    //"ServingTemperatureCode": $("#inputservingtemperature").val(),
                    "DishTypeCode": $("#inputdishtypenutri").val(),
                  
                    "TextureCode": $("#inputtexturenutri").val(),
                    "IsActive": true,
                    "ColorCode": $(".colortagedit").val(),
                    "IsSensitive": $("#issensitivenutri")[0].checked,
                    "SensitiveNote": $("#inputsensitivenutri").val(),
                    "CuisineCode": user.SectorNumber == "10" ? $("#inputcuisinenutri").val() : "",
                    "MenuType1Code": user.SectorNumber == "80" ? $("#inputDishMenuType").val() : "",
                    "PatientBillingProfileCode": user.SectorNumber == "80" ? patientBillingTypedata.toString() : null,
                    "KitchenSectionCode": $("#inputkitchensection").val(),
                    "HealthTagCode": user.UserRoleId == 13 ? healthTagdata.toString() : null,
                    "LifeStyleTagCode": user.UserRoleId == 13 ? lifeStyleTagdata.toString() : null,
                    "DietTypeCode": user.UserRoleId == 13 ? dietTypedata.toString() : null,
                   
                    "SubSectorCode": user.SectorNumber == "10" ? $("#inputsubsector").val() : "",
                }
            }
            if (!sanitizeAndSend(model)) {
                return;
            }

            $("#btnSubmitDishnutri").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveDishData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnSubmitDishnutri').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#btnSubmitDishnutri').removeAttr("disabled");
                        toastr.error(result.message);
                        $("#inputdishnamenutri").focus();
                        Utility.UnLoading();
                        return;
                    }
                    else {
                        if (result == false) {
                            $('#btnSubmitDishnutri').removeAttr("disabled");
                            toastr.error("Some error occured, please try again");
                            $("#inputdishnamenutri").focus();
                        } else if (result == "Duplicate") {
                            $('#btnSubmitDishnutri').removeAttr("disabled");
                            $("#inputdishnamenutri").focus();
                            toastr.error("Duplicate Dish Name, Please Correct it.");

                        }
                        else {
                            //$(".k-overlay").hide();
                            //var orderWindow = $("#windowEditDishsectorcate").data("kendoWindow");
                            //orderWindow.close();

                            $('#btnSubmitDishnutri').removeAttr("disabled");
                            if (model.ID > 0)
                                toastr.success("Dish configuration updated successfully");
                            else
                                toastr.success("New Dish added successfully");
                            // populateDishGrid('ref');
                            $("#gridDish").data("kendoGrid").dataSource.data([]);
                            $("#gridDish").data("kendoGrid").dataSource.read();
                            //});
                        }
                    }
                }
            }, {
                model: model

            }, true);
        }
    });
    //Krish
    //21-06-2022
    $("#inputdishaliasnutri").focusout(function () {
        var dishName = $("#inputdishnamenutri").val();
        var dishAlias = $("#inputdishaliasnutri").val();
        if (dishName === dishAlias) {
            toastr.error("Dish Name and Dish Alias should be different");
            return;
        }
    });


    $("#btnSubmitDish").click(function () {
        var dishName = $("#inputdishname").val();
        var dishAlias = $("#inputdishalias").val();
        if (user.UserRoleId !== 14) {
            if ($("#inputdishname").val() === "") {
                toastr.error("Please provide Dish Name");
                $("#inputdishname").focus();
                return;
            }
            //if (dishName === dishAlias) {
            //    toastr.error("Dish Name and Dish Alias should be different");
            //    return;
            //}
            if ($("#sectordishcateinputdietcategory").val() === "Select") {
                toastr.error("Please select Diet Category");
                $("#sectordishcateinputdietcategory").focus();
                return;
            }
            if ($("#inputuomsectordish").val() === "Select") {
                toastr.error("Please select UOM");
                $("#inputuomsectordish").focus();
                return;
            }
            if ($("#inputdishtype").val() === "Select") {
                toastr.error("Please select Dish Type");
                $("#inputdishtype").focus();
                return;
            }

            if ($("#inputkitchensection").val() === "Select") {
                toastr.error("Please select Kitchen Section");
                $("#inputkitchensection").focus();
                return;
            }

            if (datamodel == null) { //Duplicate Check
                for (item of dishdata) {
                    if (item.Name.toLowerCase().trim() == dishName.toLowerCase().trim()) {
                        toastr.error("Please check Duplicate Dish Name");
                        $("#inputdishname").focus();
                        return;
                    }
                }
            }
            else {
                for (item of dishdata) {
                    if (item.Name != null && item.Name.toLowerCase().trim() == dishName.toLowerCase().trim() && item.DishCode != datamodel.DishCode) {
                        toastr.error("Please check Duplicate DIsh Name is entered on editing");
                        $("#inputdishname").focus();
                        return;
                    }
                }
            }
            if (datamodel == null) { //Duplicate Check
                for (item of dishdata) {
                    if (dishAlias != "" && item.DishAlias != "" && item.DishAlias.toLowerCase().trim() == dishAlias.toLowerCase().trim()) {
                        toastr.error("Please check Duplicate Dish Alias");
                        $("#inputdishalias").focus();
                        return;
                    }
                }
            }
            else {
                for (item of dishdata) {
                    if (dishAlias != "" && item.DishAlias != "" && item.DishAlias != null && item.DishAlias.toLowerCase().trim() == dishAlias.toLowerCase().trim() && item.DishCode != datamodel.DishCode) {
                        toastr.error("Please check Duplicate DIsh Alias is entered on editing");
                        $("#inputdishalias").focus();
                        return;
                    }
                }
            }
            if ($("#inputdishcategory").val() === "Select") {
                toastr.error("Please select Dish Category");
                $("#inputdishcategory").focus();
                return;
            }
        }

        var localdishcategorydata = [];

        localdishcategorydata = dishcategorydata.filter(function (item) {
            return item.value == $("#inputdishcategory").val();
        });

        var dishcategoryid = localdishcategorydata[0].DishCategoryID;
        if (user.UserRoleId == 13) {
            var healthTagdata = [];
            var healthTags = "";
            var dropdownlist = $("#inputHealthTag").data("kendoMultiSelect");
            var dataItems = dropdownlist.dataItems();
            if (dataItems != undefined && dataItems != null) {
                for (dataItem of dataItems) {
                    healthTagdata.push(dataItem.value);
                }
            }

            var lifeStyleTagdata = [];
            var dropdownlistOther = $("#inputLifeStyleTag").data("kendoMultiSelect");
            var dataItemsOther = dropdownlistOther.dataItems();
            if (dataItemsOther != undefined && dataItemsOther != null) {
                for (dataItem of dataItemsOther) {
                    lifeStyleTagdata.push(dataItem.value);
                }
            }

            var dietTypedata = [];
            var dropdownlistOther = $("#inputDietType").data("kendoMultiSelect");
            var dataItemsOther = dropdownlistOther.dataItems();
            if (dataItemsOther != undefined && dataItemsOther != null) {
                for (dataItem of dataItemsOther) {
                    dietTypedata.push(dataItem.value);
                }
            }

          
        }

        if (user.SectorNumber == "80") {
            var patientBillingTypedata = [];
            var dropdownlistOther = $("#inputPatientBilingProfile").data("kendoMultiSelect");
            var dataItemsOther = dropdownlistOther.dataItems();
            if (dataItemsOther != undefined && dataItemsOther != null) {
                for (dataItem of dataItemsOther) {
                    patientBillingTypedata.push(dataItem.value);
                }
            }
        }
        var model;
        if (datamodel != null) {
            model = datamodel;
            model.DishCode = $("#inputdishcode").val();
            model.Name = $("#inputdishname").val();
            model.DishAlias = $("#inputdishalias").val();
            model.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
            model.CreatedBy = datamodel.CreatedBy;
            //model.DishSubCategoryCode= $("#inputdishsubcategory").val();
            model.DishTypeCode = $("#inputdishtype").val();
            model.TextureCode = $("#inputtexture").val();
            model.ColorCode = $(".colortagedit").val();
            model.IsSensitive = $("#issensitive")[0].checked;
            model.SensitiveNote = $("#inputsensitive").val();
            model.DietCategoryCode = $("#sectordishcateinputdietcategory").val();
            model.DishCategoryCode = $("#inputdishcategory").val();
            if (user.SectorNumber == "10") {
                model.CuisineCode = $("#inputcuisine").val();
                model.SubSectorCode = $("#inputsubsector").val();
            }
            if (user.SectorNumber == "80") {
                model.MenuType1Code = $("#inputDishMenuType").val();
                model.PatientBillingProfileCode = patientBillingTypedata.toString();
            }
            model.KitchenSectionCode = $("#inputkitchensection").val();
            model.DishCategory_ID = dishcategoryid;
            model.UOMCode = $("#inputuomsectordish").val();
            if (user.UserRoleId == 13) {
                model.HealthTagCode = healthTagdata.toString();
                model.LifeStyleTagCode = lifeStyleTagdata.toString();
                model.DietTypeCode = dietTypedata.toString();
            }

            // model.ServingTemperatureCode = $("#inputservingtemperature").val();
        }
        else {
            model = {
                "ID": $("#dishid").val(),
                "DishCode": $("#inputdishcode").val(),
                "Name": $("#inputdishname").val(),
                "DishAlias": $("#inputdishalias").val(),
                "DietCategoryCode": $("#sectordishcateinputdietcategory").val(),
                "DishCategoryCode": $("#inputdishcategory").val(),
                "DishCategory_ID": dishcategoryid,
                //"DishSubCategoryCode": $("#inputdishsubcategory").val(),
                "UOMCode": $("#inputuomsectordish").val(),
                //"ServingTemperatureCode": $("#inputservingtemperature").val(),
                "DishTypeCode": $("#inputdishtype").val(),
                "TextureCode": $("#inputtexture").val(),
                "IsActive": true,
                "ColorCode": $(".colortagedit").val(),
                "IsSensitive": $("#issensitive")[0].checked,
                "SensitiveNote": $("#inputsensitive").val(),
                "CuisineCode": user.SectorNumber == "10" ? $("#inputcuisine").val() : "",
                "HealthTagCode": user.UserRoleId == 13 ? healthTagdata.toString() : null,
                "LifeStyleTagCode": user.UserRoleId == 13 ? lifeStyleTagdata.toString() : null,
                "DietTypeCode": user.UserRoleId == 13 ? dietTypedata.toString() : null,
                "SubSectorCode": user.SectorNumber == "10" ? $("#inputsubsector").val() : "",
                "MenuType1Code": user.SectorNumber == "80" ? $("#inputDishMenuType").val() : "",
                "PatientBillingProfileCode": user.SectorNumber == "80" ? patientBillingTypedata.toString() : null,
                "KitchenSectionCode": $("#inputkitchensection").val(),
            }
        }
        if (!sanitizeAndSend(model)) {
            return;
        }
        $("#btnSubmitDish").attr('disabled', 'disabled');
        HttpClient.MakeRequest(CookBookMasters.SaveDishData, function (result) {
            if (result.xsssuccess !== undefined && !result.xsssuccess) {
                toastr.error(result.message);
                $('#btnSubmitDish').removeAttr("disabled");
                $("#inputdishnamenutri").focus();
                Utility.UnLoading();
            }
            else {
                if (result == false) {
                    $('#btnSubmitDishnutri').removeAttr("disabled");
                    toastr.error(result.message);
                    $("#inputdishnamenutri").focus();
                    Utility.UnLoading();
                    return;
                }
                else {
                    if (result == false) {
                        $('#btnSubmitDish').removeAttr("disabled");
                        toastr.error("Some error occured, please try again");
                        $("#inputdishname").focus();
                    } else if (result == "Duplicate") {
                        $('#btnSubmitDish').removeAttr("disabled");
                        $("#inputdishname").focus();
                        toastr.error("Duplicate Dish Name, Please Correct it.");

                    }
                    else {
                        $(".k-overlay").hide();
                        var orderWindow = $("#windowEditDishsectorcate").data("kendoWindow");
                        //orderWindow.close();
                        //Krish
                        if (orderWindow != undefined)
                            orderWindow.close();
                        else if (user.UserRoleId == 13) {
                            $("#divEditNutri").hide();
                            $("#divMainContent").show();
                        }
                        $('#btnSubmitDish').removeAttr("disabled");
                        if (model.ID > 0)
                            toastr.success("Dish configuration updated successfully");
                        else
                            toastr.success("New Dish added successfully");
                        //populateDishGrid('ref');
                        $("#gridDish").data("kendoGrid").dataSource.data([]);
                        $("#gridDish").data("kendoGrid").dataSource.read();
                        //});
                    }
                }
            }
        }, {
            model: model

        }, true);

    });

    $("#inputdishalias").focusout(function () {
        var dishName = $("#inputdishname").val();
        var dishAlias = $("#inputdishalias").val();
        if (dishName === dishAlias) {
            toastr.error("Dish Name and Dish Alias should be different");
            return;
        }
    });
});

function getDishImpactedItems(e) {
    var dialog = $("#windowEditDishImpactedItems").data("kendoWindow");
    dialog.open();
    if (user.SectorNumber == "20") {
        dialog.setOptions({ width: "600px" });
    }
    else {
        dialog.setOptions({ width: "1050px" });
    }
    dialog.center();

    var gridObj = $("#gridDish").data("kendoGrid");
    var tr = gridObj.dataItem($(e).closest("tr")); // 'e' is the HTML element <a>
    dialog.title("Item Details - " + tr.Name);
    var gridVariable = $("#gridDishImpactedItems");
    gridVariable.html("");

    if (user.SectorNumber == "10") {
        gridVariable.kendoGrid({
            //selectable: "cell",
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: true,
            //selectable: true,
            //reorderable: true,
            //scrollable: true,
            columns: [

                {
                    field: "ItemCode", title: "Item Code", width: "40px", attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    }
                },
                {
                    field: "ItemName", title: "Item Name", width: "120px", attributes: {

                        style: "text-align: left; font-weight:normal;float: left; display: flex;margin-top: 3px; "
                    },
                    template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
                },

                {
                    field: "ConceptType2Name", title: "Type", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "ApplicableTo", title: "Usability", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                }


            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetDishImpactedItemList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);


                                var dataSource = result;
                            }
                            else {
                                options.success("");
                            }
                        },
                            {
                                dishCode: tr.DishCode
                            }
                            , true);
                    }
                },
                schema: {

                    model: {
                        id: "ID",
                        fields: {
                            ItemCode: { type: "string" },
                            ItemName: { type: "string" },
                            DietCategoryName: { type: "string" },
                            VisualCategoryName: { type: "string" },
                            FoodProgramName: { type: "string" },
                            HSNCode: { type: "string" },
                            CGST: { type: "string" },
                            SGST: { type: "string" },
                            CESS: { type: "string" },
                            Status: { type: "string" },
                            ApplicableTo: { type: "string" },
                            ConceptType2Name: { type: "string" },
                            PublishedDate: { type: "date" },
                            CreatedOn: { type: "date" },
                        }

                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            height: 437,
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.ItemCode > 80000) {
                        $(this).find(".k-grid-Map").addClass("k-state-disabled");
                    }
                });

                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }
                    else {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    }

                }
                var items = e.sender.items();

                //items.each(function (e) {

                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //    }
                //});
            },
            change: function (e) {
            }

            //})
        });
        //dialog.setOptions({ width: "850px" });

    }
    else if (user.SectorNumber == "20") {
        gridVariable.kendoGrid({
            //selectable: "cell",
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: true,
            //selectable: true,
            //reorderable: true,
            //scrollable: true,
            columns: [

                {
                    field: "ItemCode", title: "Item Code", width: "40px", attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    }
                },
                {
                    field: "ItemName", title: "Item Name", width: "120px", attributes: {

                        style: "text-align: left; font-weight:normal;float: left; display: flex;margin-top: 3px; "
                    },
                    template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
                },

                //{
                //    field: "ConceptType2Name", title: "Type", width: "30px", attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    },
                //},
                //{
                //    field: "ApplicableTo", title: "Usability", width: "30px", attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    },
                //}


            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetDishImpactedItemList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);


                                var dataSource = result;
                            }
                            else {
                                options.success("");
                            }
                        },
                            {
                                dishCode: tr.DishCode
                            }
                            , true);
                    }
                },
                schema: {

                    model: {
                        id: "ID",
                        fields: {
                            ItemCode: { type: "string" },
                            ItemName: { type: "string" },
                            DietCategoryName: { type: "string" },
                            VisualCategoryName: { type: "string" },
                            FoodProgramName: { type: "string" },
                            HSNCode: { type: "string" },
                            CGST: { type: "string" },
                            SGST: { type: "string" },
                            CESS: { type: "string" },
                            Status: { type: "string" },
                            ApplicableTo: { type: "string" },
                            ConceptType2Name: { type: "string" },
                            PublishedDate: { type: "date" },
                            CreatedOn: { type: "date" },
                        }

                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            height: 437,
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.ItemCode > 80000) {
                        $(this).find(".k-grid-Map").addClass("k-state-disabled");
                    }
                });

                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }
                    else {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    }

                }
                var items = e.sender.items();

                //items.each(function (e) {

                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //    }
                //});
            },
            change: function (e) {
            }

            //})
        });
        //dialog.setOptions({ width: "850px" });

    }
    else {
        gridVariable.kendoGrid({
            //selectable: "cell",
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: true,
            //selectable: true,
            //reorderable: true,
            //scrollable: true,
            columns: [

                {
                    field: "ItemCode", title: "Item Code", width: "40px", attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    }
                },
                {
                    field: "ItemName", title: "Item Name", width: "120px", attributes: {

                        style: "text-align: left; font-weight:normal;float: left; display: flex;margin-top: 3px; "
                    },
                    template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
                },
                {
                    field: "FoodProgramName", title: "Food Program", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "ConceptType2Name", title: "Type", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "ApplicableTo", title: "Usability", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                }


            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetDishImpactedItemList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);


                                var dataSource = result;
                            }
                            else {
                                options.success("");
                            }
                        },
                            {
                                dishCode: tr.DishCode
                            }
                            , true);
                    }
                },
                schema: {

                    model: {
                        id: "ID",
                        fields: {
                            ItemCode: { type: "string" },
                            ItemName: { type: "string" },
                            DietCategoryName: { type: "string" },
                            VisualCategoryName: { type: "string" },
                            FoodProgramName: { type: "string" },
                            HSNCode: { type: "string" },
                            CGST: { type: "string" },
                            SGST: { type: "string" },
                            CESS: { type: "string" },
                            Status: { type: "string" },
                            ApplicableTo: { type: "string" },
                            ConceptType2Name: { type: "string" },
                            PublishedDate: { type: "date" },
                            CreatedOn: { type: "date" },
                        }

                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            height: 437,
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.ItemCode > 80000) {
                        $(this).find(".k-grid-Map").addClass("k-state-disabled");
                    }
                });

                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }
                    else {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    }

                }
                var items = e.sender.items();

                //items.each(function (e) {

                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //    }
                //});
            },
            change: function (e) {
            }

            //})
        });
        // dialog.setOptions({ width: "1050px" });
    }

    //$(".k-label")[0].innerHTML.replace("items", "records");
}

function back() {
    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
        function () {
            $("#ItemMaster").show();
            $("#mapRecipe").hide();
            $("#topHeading").text("Sector Dish Configuration");
        },
        function () {
        }
    );


}

function populateRecipeMultiSelect() {
    $("#rclear").empty();
    $("#rclear").append('<div id="inputrecipelist"></div>');
    $("#inputrecipelist").kendoMultiSelect({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "RecipeCode",
        dataSource: recipedata,
        index: -1,
        autoClose: false
    });
    // var multiselect = $("#inputrecipelist").data("kendoMultiSelect");
    // multiselect.bind("change", multiselect_select);
}

//function multiselect_select() {
//    // var multiselect = $("#inputrecipelist").data("kendoMultiSelect");
//    // var dataItems = multiselect.dataItems();
//    //add in list and refresh grid
//}

//HttpClient.MakeRequest(CookBookMasters.GetRecipeDataList, function (data) {

//    recipedata = data;
//    //  recipedata = [];

//    populateRecipeMultiSelect();
//}, { condition: "All" }, false);
function UpdateRecipe() {
    // alert("Update Grid MApper");
    var multiselect = $("#inputrecipelist").data("kendoMultiSelect");
    var flagDefault = 0;
    //get from multi select and bulid grid data
    var dataItems = multiselect.dataItems();
    console.log(dataItems);
    for (item of dataItems) {
        item.IsDefault = false;
        item.DishCode = DishCode;
        if (checkedRecipeCode == item.RecipeCode) {
            item.IsDefault = true;
            flagDefault = 1;
        }
        //condition for default and true
    }
    if (!flagDefault && dataItems.length > 0) {
        dataItems[0].IsDefault = true;
    }
    //populateRecipeGridCustomUpdate();
    recipeCustomData = dataItems;
    populateRecipeGridCustomUpdate()
}

function populateRecipeGridCustomUpdate() {


    var gridVariable = $("#gridRecipeCustom");//Hare Krishna!!
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "Recipe.xlsx",
            filterable: true,
            allPages: true
        },
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        //pageable: {
        //    pageSize: 15
        //},
        columns: [
            {
                field: "RecipeCode", title: "Recipe Code", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Recipe Name", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.Name) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetailsCustom(this)'></i>`;
                    return html;
                },
            },

            {
                field: "Quantity", title: "Quantity", width: "60px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: left"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "CostPerKG", title: "CostPerKG", format: Utility.Cost_Format, width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "IsBase", title: "Base Recipe", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: '# if (IsBase == true) {#<div>Yes</div>#} else {#<div>No</div>#}#',
            },


            {
                field: "IsDefault", title: "Default ", width: "80px", attributes: {

                    style: "text-align: center; font-weight:normal;display:list-item"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: templateFunction,
            }
        ],
        dataSource: {
            data: recipeCustomData,
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Name: { editable: false },
                        IsActive: { editable: false }
                    }
                }
            },
            pageSize: 15,
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        change: function (e) {
        },

    })
    function templateFunction(dataItem) {
        console.log(dataItem);
        var item = "<div class='icheck-success d-inline'>"
        if (dataItem.IsDefault) {
            item += "<input type='radio' style='margin-top: 7px;' name='" + dataItem.DishCode + "' id='" + dataItem.RecipeCode + "' onclick='setDataItem(this);'   checked=checked />";
            console.log(dataItem);
            checkedRecipeCode = dataItem.RecipeCode;
        } else {
            item += "<input  style='margin-top: 7px;' type='radio' onclick='setDataItem(this);'  id='" + dataItem.RecipeCode + "' name='" + dataItem.DishCode + "'/>";
        }
        item += "<label for='" + dataItem.RecipeCode + "' class='form-control-plaintext' style='display: inline!important'></label ></div>"
        return item;

    };



}



function populateRecipeGridCustom() {


    //customData(DishCode, cond);

    var gridVariable = $("#gridRecipeCustom");//Hare Krishna!!
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "Recipe.xlsx",
            filterable: true,
            allPages: true
        },
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        //pageable: {
        //    pageSize: 15
        //},
        columns: [
            {
                field: "RecipeCode", title: "Recipe Code", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Recipe Name", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.Name) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetailsCustom(this)'></i>`;
                    return html;
                },
            },

            {
                field: "Quantity", title: "Quantity", width: "60px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: left"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "CostPerKG", title: "CostPerKG", format: Utility.Cost_Format, width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },

            {
                field: "IsBase", title: "Base Recipe", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: '# if (IsBase == true) {#<div>Yes</div>#} else {#<div>No</div>#}#',
            },


            {
                field: "IsDefault", title: "Default ", width: "80px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: templateFunction,
            }
        ],
        dataSource: {
            data: recipeCustomData,
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Name: { editable: false },
                        IsActive: { editable: false }
                    }
                }
            },
            pageSize: 150,
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            //var items = e.sender.items();

            //items.each(function (e) {

            //    if (user.UserRoleId == 1) {
            //        $(this).find('.k-grid-Edit').text("Edit");
            //    } else {
            //        $(this).find('.k-grid-Edit').text("View");
            //    }
            //});
        },
        change: function (e) {
        },

    })
    function templateFunction(dataItem) {


        var item = "<div class='icheck-success d-inline'>"
        if (dataItem.IsDefault) {
            item += "<input type='radio' style='margin-top: 7px;' name='" + dataItem.DishCode + "' id='" + dataItem.RecipeCode + "' onclick='setDataItem(this);'   checked=checked />";
            console.log(dataItem);
            checkedRecipeCode = dataItem.RecipeCode;
        } else {
            item += "<input  style='margin-top: 7px;' type='radio' onclick='setDataItem(this);'  id='" + dataItem.RecipeCode + "' name='" + dataItem.DishCode + "'/>";
        }
        item += "<label for='" + dataItem.RecipeCode + "' class='form-control-plaintext' style='display: inline!important'></label ></div>"

        return item;
    };

    //Krish 12-08-2022
    //setTimeout(function () {
    //    var multiselect = $("#inputrecipelist").data("kendoMultiSelect");

    //    //clear filter
    //    multiselect.dataSource.filter({});

    //    //set value
    //    multiselect.value(multiarray);
    //}, 500);
}

function setDataItem(item) {
    var grid = $("#gridRecipeCustom").data("kendoGrid");
    var row = $(item).closest("tr");
    var dataItem = grid.dataItem(row);
    console.log(dataItem)
    dataItem.IsDefault = true;
    checkedRecipeCode = dataItem.RecipeCode;
    for (x of recipeCustomData) {
        if (x.RecipeCode == checkedRecipeCode) {
            x.IsDefault = true;
        }
        else {
            x.IsDefault = false;
        }
    }
}

function saveDB() {

    //make model;
    //grid data

    saveModel = [];
    for (item of recipeCustomData) {
        var x = {};
        x.RecipeCode = item.RecipeCode;
        x.DishCode = item.DishCode;
        x.Recipe_ID = item.ID;
        x.IsDefault = item.IsDefault;
        x.IsActive = 1;
        x.Dish_ID = Dish_ID;
        x.Status = 3;
        x.CreatedBy = user.UserId;
        x.CreatedOn = Utility.CurrentDate();
        saveModel.push(x);
    }
    //call and message display 
    if (saveModel.length > 0) {

        Utility.Loading();
        HttpClient.MakeRequest(CookBookMasters.SaveDishRecipeMappingList, function (result) {
            if (result == false) {
                Utility.UnLoading();
                toastr.error("Some error occured, please try again.");
            }
            else {
                Utility.UnLoading();
                $(".k-overlay").hide();
                toastr.success("The selected Recipe(s) has been mapped to this Dish");
                //populateDishGrid('ref');
                $("#gridDish").data("kendoGrid").dataSource.data([]);
                $("#gridDish").data("kendoGrid").dataSource.read();
                $("#ItemMaster").show();
                $("#mapRecipe").hide();
                $("#topHeading").text("Sector Dish Configuration");
            }
        }, {
            model: saveModel

        }, false);
    } else {
        toastr.error("Kindly add a Recipe.");
    }
}



function refreshBaseRecipeDropDownData(x) {
    // alert("call");
    if (basedataList != null && basedataList.length > 0) {
        var baseGridData = $("#gridBaseRecipe1").data('kendoGrid').dataSource._data;
        if (baseGridData != null && baseGridData.length > 0) {
            baseGridData = baseGridData.filter(m => m.BaseRecipeName != "" && m.BaseRecipeName != x);
            baserecipedata = basedataList;
            var basedataListtemp = basedataList;
            basedataListtemp = basedataListtemp.filter(m => m.BaseRecipe_Code != "");
            basedataListtemp.forEach(function (item, index) {
                for (var i = 0; i < baseGridData.length; i++) {
                    if (baseGridData[i].BaseRecipeCode !== "" && item.BaseRecipe_Code === baseGridData[i].BaseRecipeCode) {
                        baserecipedata.splice(index, 1);
                        baserecipedata = baserecipedata.filter(r => r.BaseRecipe_Code !== item.BaseRecipe_Code && r.BaseRecipe_Code != RecipeCode);
                        break;
                    }
                }
                return true;
            });
        }
    }
    baserecipedata = baserecipedata.filter(r => r.BaseRecipe_Code != RecipeCode);
    return baserecipedata;
};

function checkItemAlreadyExist(selectedId, gridID, isBR) {
    var dataExists = false;
    var gridObj = $("#" + gridID).data("kendoGrid");
    var data = gridObj.dataSource.data();
    var existingData = [];
    if (isBR) { existingData = data.filter(m => m.BaseRecipe_ID == selectedId); }
    else {
        existingData = data.filter(m => m.MOG_ID == selectedId);
        //if (!existingData[0]?.IsActive)
        //    return true;
    }
    if (existingData.length == 2) {
        dataExists = true;
    }
    return dataExists;
}
function onBRDDLChange(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridBaseRecipe', true)) {
        toastr.error("Base Recipe already exists.");
        var dropdownlist = $("#ddlBaseRecipe").data("kendoDropDownList");
        dropdownlist.select(0);
        dropdownlist.trigger("change");
        return false;
    }
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var data = row.find(".k-input")[0].innerText;
    row.find(".brspan").val(data);

    var Obj = baserecipedata.filter(function (recipeObj) {
        return recipeObj.BaseRecipe_ID == e.sender.value();
    });
    //baserecipedatafiltered = baserecipedata.filter(function (recipeObj) {
    //    return recipeObj.BaseRecipe_ID != e.sender.value();
    //});

    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.CostPerUOM = UnitCost;
    dataItem.CostPerKG = KGCost;
    var BRCode = Obj[0].BaseRecipe_Code;
    dataItem.BaseRecipeCode = BRCode;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    //$(element).closest('tr').find('.kgcost')[0].innerHTML = KGCost;
    // $(element).closest('tr').find('.uid')[0].innerHTML = UID;
};

function populateUOMDropdown1() {
    $("#inputuom1").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateBaseDropdown1() {
    $("#inputbase1").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}

function populateBaseRecipeGrid1(recipeCode) {

    //if (recipeID == null)
    //    recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe1");
    gridVariable.html("");
    gridVariable.kendoGrid({


        columns: [
            {
                field: "BaseRecipe_ID", title: "Base Recipe", width: "90px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.BaseRecipeName) + '</span>'
                        + `  <i title="Click to View Details" style='float:right;' class='fas fa-external-link-alt' onclick='showDetails2(this)'></i>`;
                    return html;
                },
                //editor: function (container, options) {
                //    $('<input tabindex="0" id="ddlBaseRecipe1" class="brTemplate" text="' + options.model.BaseRecipeName + '" data-text-field="BaseRecipeName" data-value-field="BaseRecipe_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 1,
                //            filter: "contains",
                //            dataTextField: "BaseRecipeName",
                //            dataValueField: "BaseRecipe_ID",
                //            autoBind: true,
                //            dataSource: baserecipedata,
                //            value: options.field,
                //            change: onBRDDLChange1,
                //            //open: function (e) { e.preventDefault();}
                //        });
                // }
            },

            {
                field: "UOMName", title: "UOM", width: "40px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "30px",
                //  template: '<input type="number" class="inputbrqty"  name="Quantity" oninput="calculateItemTotal(this,1)" readonly/>',

                headerAttributes: {
                    style: "text-align: left;"
                },
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },

            {
                field: "IngredientPerc", title: "Major %", width: "40px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "40px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },

            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;"
            //    },
            //    command: [
            //        {
            //            name: 'Delete',
            //            text: "Delete",

            //            click: function (e) {
            //                if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //                    return false;
            //                }
            //                var gridObj = $("#gridBaseRecipe1").data("kendoGrid");
            //                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                gridObj.dataSource._data.remove(selectedRow);
            //                CalculateGrandTotal(1);
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe1").css("display", "block");
                            $("#emptybr1").css("display", "none");
                        } else {
                            $("#gridBaseRecipe1").css("display", "none");
                            $("#emptybr1").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.brTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange
                });
                ddt.find(".k-input").val(dataItem.BaseRecipeName);

                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridBaseRecipe1');
    var toolbar = $("#gridBaseRecipe1").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe1 .k-grid-content").addClass("gridInside");

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");
        $("#windowEditx").kendoWindow({
            animation: false
        });
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
    });
}



function onMOGDDLChange(e) {
    if (checkItemAlreadyExist(e.sender.value(), 'gridMOG', false)) {
        toastr.error("Mog is already selected");
        var dropdownlist = $("#ddlMOG").data("kendoDropDownList");
        dropdownlist.select("");
        //dropdownlist.trigger("change");
        return false;
    }

    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#gridMOG").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    console.log("Data Item ")
    console.log(dataItem)

    var data = row.find(".k-input")[0].innerText;
    dataItem.set("MOG_ID", e.sender.value());
    dataItem.set("MOGName", data);

    var Obj = mogdata.filter(function (mogObj) {
        return mogObj.MOG_ID == e.sender.value();
    });
    var UID = Obj[0].UOM_ID;
    var UOMNAME = Obj[0].UOM_NAME;
    var UnitCost = Obj[0].CostPerUOM;
    // var KGCost = Obj[0].CostPerKG;
    dataItem.set("BaseRecipe_ID", e.sender.value());
    dataItem.set("BaseRecipeName", data);
    dataItem.UOMName = UOMNAME;
    dataItem.UOM_ID = UID;
    dataItem.UOMCode = Obj[0].UOM_Code;
    dataItem.CostPerUOM = UnitCost;
    dataItem.MOGCode = Obj[0].MOG_Code;
    console.log(dataItem)
    //  dataItem.CostPerKG = KGCost;
    $(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    $(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    $(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
    CalculateGrandTotal('');
};


function populateMOGGrid1(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG1");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" }, //{ name: "create", text: "Add" }
        //],
        groupable: false,
        editable: true,

        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                editor: function (container, options) {

                    $('<input class="mogTemplate" id="ddlMOG1" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            index: 0,
                            filter: "contains",
                            dataTextField: "MOGName",
                            dataValueField: "MOG_ID",
                            autoBind: true,
                            dataSource: mogdata,
                            value: options.field,
                            change: onMOGDDLChange1,
                        });

                }
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {

                    class: 'uomname',
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;"
                },

                // template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    oninput="calculateItemTotalMOG(this,1)" readonly/>',
                attributes: {

                    style: "text-align: center; font-weight:normal"
                },
            },

            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            },
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "uomcost",

                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                headerAttributes: {
                    style: "text-align: right;"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                title: "Delete",
                width: "60px",
                headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;"
                },
                command: [
                    //{
                    //    name: 'Delete',
                    //    text: "Delete",

                    //    click: function (e) {
                    //        if ($(e.currentTarget).hasClass("k-state-disabled")) {
                    //            return false;
                    //        }
                    //        var gridObj = $("#gridMOG1").data("kendoGrid");
                    //        var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                    //        gridObj.dataSource._data.remove(selectedRow);
                    //        CalculateGrandTotal(1);
                    //        return true;
                    //    }
                    //},
                    {
                        name: 'View APL',
                        click: function (e) {
                            var gridObj = $("#gridMOG1").data("kendoGrid");
                            var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                            datamodel = tr;
                            MOGName = tr.MOGName;
                            var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
                            $("#windowEditMOGWiseAPL").kendoWindow({
                                animation: false
                            });
                            $(".k-overlay").css("display", "block");
                            $(".k-overlay").css("opacity", "0.5");
                            dialog.open();
                            dialog.center();
                            dialog.title("MOG APL Details - " + MOGName);
                            MOGCode = tr.MOGCode;
                            mogWiseAPLMasterdataSource = [];
                            getMOGWiseAPLMasterData();
                            populateAPLMasterGrid();
                            return true;
                        }
                    }
                ],
            }
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridMOG1").css("display", "block");
                            $("#emptymog1").css("display", "none");
                        } else {
                            $("#gridMOG1").css("display", "none");
                            $("#emptymog1").css("display", "block");
                        }
                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        MOG_ID: { type: "number", validation: { required: true } },
                        MOGName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            items.each(function (e) {

                var dataItem = grid.dataItem(this);

                var ddt = $(this).find('.mogTemplate');

                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "MOGName",
                    dataValueField: "MOG_ID",
                    change: onMOGDDLChange1
                });
                //ddt.find(".k-input").val(dataItem.MOGName);

                $(this).find('.inputmogqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    cancelChangesConfirmation('gridMOG1');
    var toolbar = $("#gridMOG1").find(".k-grid-toolbar");
    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();

    toolbar.addClass("gridToolbarShift");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    toolbar.find(".k-grid-add").addClass("gridButton")
    $("#gridMOG1 .k-grid-content").addClass("gridInside");
}

//Krish
//22-06-2022
function populateMOGGridNutri(recipeResult) {
    console.log(recipeResult)
    Utility.Loading();
    //$("#gridContent").html("");
    //$("#gridContent").html("<div id='gridMOGNutri' style='overflow: visible!important;'></div>")
    var gridVariable = $("#gridMOGNutri");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //toolbar: [{ name: "cancel", text: "Reset" }, //{ name: "create", text: "Add" }
        //],
        groupable: false,
        editable: false,

        columns: [
            {
                field: "MOG_ID", title: "MOG", width: "100px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="mogspan">' + kendo.htmlEncode(dataItem.MOGName) + '</span>';
                    return html;
                },
                //editor: function (container, options) {

                //    $('<input class="mogTemplate" id="ddlMOG1" text="' + options.model.MOGName + '" data-text-field="MOGName" data-value-field="MOG_ID" data-bind="value:' + options.field + '"/>')
                //        .appendTo(container)
                //        .kendoDropDownList({
                //            index: 0,
                //            filter: "contains",
                //            dataTextField: "MOGName",
                //            dataValueField: "MOG_ID",
                //            autoBind: true,
                //            dataSource: mogdata,
                //            value: options.field,
                //            change: onMOGDDLChange1,
                //        });

                //}
            },
            {
                field: "ColorCode", title: "Color", width: "30px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                },
                template: '<div class="colortag"></div>',
            },
            {
                field: "AllergensName", title: "Contains", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "AllergensName",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "NGAllergen", title: "May Contains (NG)", width: "70px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "AllergensName",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "MOGQty", title: "Quantity", width: "35px",

                headerAttributes: {
                    style: "text-align: center;width:35px;"
                },
                attributes: {
                    class: 'gridmogqty',
                    style: "text-align: center; font-weight:normal"
                },
            },
            //{
            //    field: "UOMName", title: "UOM", width: "50px",
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {

            //        class: 'uomname',
            //        style: "text-align: center; font-weight:normal"
            //    },
            //},
            //{
            //    field: "Quantity", title: "Quantity", width: "35px",
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },

            //    // template: '<input type="number" min="0" name="Quantity" class="inputmogqty"    oninput="calculateItemTotalMOG(this,1)" readonly/>',
            //    attributes: {

            //        style: "text-align: center; font-weight:normal"
            //    },
            //},

            //{
            //    field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "major",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    template: '# if (IsMajor == true) {#<div>#= IngredientPerc#</div>#} else {#<div>-</div>#}#',
            //},
            //{
            //    field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        class: "minor",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc#</div>#}#',
            //},
            //{
            //    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
            //    headerAttributes: {
            //        style: "text-align: right;"
            //    },
            //    attributes: {
            //        class: "uomcost",

            //        style: "text-align: right; font-weight:normal"
            //    },
            //},
            //{
            //    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
            //    headerAttributes: {
            //        style: "text-align: right;"
            //    },
            //    attributes: {
            //        class: "totalcost",
            //        style: "text-align: right; font-weight:normal"
            //    },
            //},
            //{
            //    title: "Delete",
            //    width: "60px",
            //    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    attributes: {
            //        style: "text-align: center; font-weight:normal;"
            //    },
            //    command: [
            //        //{
            //        //    name: 'Delete',
            //        //    text: "Delete",

            //        //    click: function (e) {
            //        //        if ($(e.currentTarget).hasClass("k-state-disabled")) {
            //        //            return false;
            //        //        }
            //        //        var gridObj = $("#gridMOG1").data("kendoGrid");
            //        //        var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //        //        gridObj.dataSource._data.remove(selectedRow);
            //        //        CalculateGrandTotal(1);
            //        //        return true;
            //        //    }
            //        //},
            //        {
            //            name: 'View APL',
            //            click: function (e) {
            //                var gridObj = $("#gridMOG1").data("kendoGrid");
            //                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
            //                datamodel = tr;
            //                MOGName = tr.MOGName;
            //                var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
            //                $("#windowEditMOGWiseAPL").kendoWindow({
            //                    animation: false
            //                });
            //                $(".k-overlay").css("display", "block");
            //                $(".k-overlay").css("opacity", "0.5");
            //                dialog.open();
            //                dialog.center();
            //                dialog.title("MOG APL Details - " + MOGName);
            //                MOGCode = tr.MOGCode;
            //                mogWiseAPLMasterdataSource = [];
            //                getMOGWiseAPLMasterData();
            //                populateAPLMasterGrid();
            //                return true;
            //            }
            //        }
            //    ],
            //}
        ],
        dataSource: recipeResult,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            //var grid = e.sender;
            //var items = e.sender.items();

            //items.each(function (e) {

            //    var dataItem = grid.dataItem(this);

            //    var ddt = $(this).find('.mogTemplate');

            //    $(ddt).kendoDropDownList({
            //        index: 0,
            //        filter: "contains",
            //        value: dataItem.value,
            //        dataSource: baserecipedata,
            //        dataTextField: "MOGName",
            //        dataValueField: "MOG_ID",
            //        change: onMOGDDLChange1
            //    });
            //    //ddt.find(".k-input").val(dataItem.MOGName);

            //    $(this).find('.inputmogqty').val(dataItem.Quantity);
            //});

            //Krish
            //13-07-2022
            var grid = this;
            console.log(applicationSettingData)
            $(".gridmogqty").each(function () {
                var qty = parseFloat($(this).html()).toFixed(2);
                // var newqty = qty.toFixed(2);
                if (qty == 0)
                    qty = parseFloat(qty).toFixed(3);
                $(this).html(qty);

            });
            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);
                //Krish 06-08-2022
                //gridmogqty

                if (model.AllergensName != "None" && model.NGAllergen != "None") {
                    var code = "FLX-00012";
                    if (user.SectorNumber == "80")  //Healthcare
                        code = "FLX-00012";
                    else if (user.SectorNumber == "30")  //Core
                        code = "FLX-00015";
                    else if (user.SectorNumber == "20")  //Manufac
                        code = "FLX-00009";

                    var getBothColor = applicationSettingData.filter(a => a.Code == code && a.IsActive == true)[0];
                    if (getBothColor != undefined)
                        $(this).find(".colortag").css('background-color', getBothColor.Value);
                }
                else if (model.AllergensName == "None" && model.NGAllergen != "None") {
                    var code = "FLX-00013";
                    if (user.SectorNumber == "80")  //Healthcare
                        code = "FLX-00013";
                    else if (user.SectorNumber == "30")  //Core
                        code = "FLX-00016";
                    else if (user.SectorNumber == "20")  //Manufac
                        code = "FLX-00010";
                    var getBothColor = applicationSettingData.filter(a => a.Code == code && a.IsActive == true)[0];
                    if (getBothColor != undefined)
                        $(this).find(".colortag").css('background-color', getBothColor.Value);
                }
                else if (model.AllergensName != "None" && model.NGAllergen == "None") {
                    var code = "FLX-00014";
                    if (user.SectorNumber == "80")  //Healthcare
                        code = "FLX-00014";
                    else if (user.SectorNumber == "30")  //Core
                        code = "FLX-00017";
                    else if (user.SectorNumber == "20")  //Manufac
                        code = "FLX-00011";
                    var getBothColor = applicationSettingData.filter(a => a.Code == code && a.IsActive == true)[0];
                    if (getBothColor != undefined)
                        $(this).find(".colortag").css('background-color', getBothColor.Value);
                }
                else if (model.AllergensName == "None" && model.NGAllergen == "None") {
                    var code = "FLX-00015";
                    if (user.SectorNumber == "80")  //Healthcare
                        code = "FLX-00015";
                    else if (user.SectorNumber == "30")  //Core
                        code = "FLX-00018";
                    else if (user.SectorNumber == "20")  //Manufac
                        code = "FLX-00012";
                    var getBothColor = applicationSettingData.filter(a => a.Code == code && a.IsActive == true)[0];
                    if (getBothColor != undefined)
                        $(this).find(".colortag").css('background-color', getBothColor.Value);
                }
            });
        },
        change: function (e) {
        },
    });
    //cancelChangesConfirmation('gridMOG1');
    //var toolbar = $("#gridMOG1").find(".k-grid-toolbar");
    //toolbar.find(".k-add").remove();
    //toolbar.find(".k-cancel").remove();

    //toolbar.addClass("gridToolbarShift");
    //toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
    //toolbar.find(".k-grid-add").addClass("gridButton")
    //$("#gridMOG1 .k-grid-content").addClass("gridInside");

    //var grid = $("#gridMOGNutri").data("kendoGrid");
    //var dataItem = grid.dataItem(row);
    //console.log("Data Item ")
    //console.log(dataItem)

    ////var data = row.find(".k-input")[0].innerText;
    ////dataItem.set("MOG_ID", e.sender.value());
    ////dataItem.set("MOGName", data);

    ////var Obj = mogdata.filter(function (mogObj) {
    ////    return mogObj.MOG_ID == e.sender.value();
    ////});
    ////var UID = Obj[0].UOM_ID;
    ////var UOMNAME = Obj[0].UOM_NAME;
    ////var UnitCost = Obj[0].CostPerUOM;
    ////// var KGCost = Obj[0].CostPerKG;
    ////dataItem.set("BaseRecipe_ID", e.sender.value());
    ////dataItem.set("BaseRecipeName", data);
    ////dataItem.UOMName = UOMNAME;
    ////dataItem.UOM_ID = UID;
    ////dataItem.UOMCode = Obj[0].UOM_Code;
    ////dataItem.CostPerUOM = UnitCost;
    ////dataItem.MOGCode = Obj[0].MOG_Code;
    ////console.log(dataItem)
    //////  dataItem.CostPerKG = KGCost;
    ////$(element).closest('tr').find('.uomname')[0].innerHTML = UOMNAME;
    ////$(element).closest('tr').find('.uomcost')[0].innerHTML = Utility.AddCurrencySymbol(UnitCost, 2);
    //dataItem.TotalCost = dataItem.CostPerUOM * dataItem.Quantity;
    //$(element).closest('tr').find('.totalcost')[0].innerHTML = Utility.AddCurrencySymbol(dataItem.TotalCost, 2);
}

$("#btnCancel1").on("click", function () {
    $(".k-overlay").hide();
    var orderWindow = $("#windowEdit1").data("kendoWindow");
    $("#emptybr1").css("display", "block");
    $("#gridBaseRecipe1").css("display", "none");
    orderWindow.close();


});
function showDetails(e) {

    // alert(e);
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridBaseRecipe1").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    $("#success").css("display", "none");
    $("#error1").css("display", "none");
    var gridObj = $("#gridRecipe").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.Name.trim() == dataItem.BaseRecipeName.split("(")[0].trim())
            return x;
    })



    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;
    $("#hiddenRecipeID").val(tr.Recipe_ID);
    var dialog = $("#windowEdit1").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    //populateUOMDropdown1();
    //populateBaseDropdown1();
    //populateBaseRecipeGrid1(tr.ID);
    //populateMOGGrid1(tr.ID);

    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputrecipealiasname1").val(tr.RecipeAlias);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");
    $('#grandTotal1').html(Utility.AddCurrencySymbol(tr.TotalCost, 2));
    $('#grandCostPerKG1').html(Utility.AddCurrencySymbol(tr.CostPerKG, 2));
    $('#inputinstruction1').val(tr.Instructions);
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename1").removeAttr('disabled');
    //    $("#inputuom1").removeAttr('disabled');
    //    $("#inputquantity1").removeAttr('disabled');
    //    $("#inputbase1").removeAttr('disabled');
    //   // $("#btnSubmit1").css('display', '');
    //    $("#btnCancel1").css('display', '');
    //} else {
    //    $("#inputrecipename1").attr('disabled', 'disabled');
    //    $("#inputuom1").attr('disabled', 'disabled');
    //    $("#inputquantity1").attr('disabled', 'disabled');
    //    $("#inputbase1").attr('disabled', 'disabled');
    //   //$("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details  - " + RecipeName);
    return true;

}
function populateBaseRecipeGrid(recipeCode) {

    //if (recipeID == null)
    //    recipeID = 0
    Utility.Loading();
    var gridVariable = $("#gridBaseRecipe1");
    gridVariable.html("");
    gridVariable.kendoGrid({
        columns: [
            {
                field: "BaseRecipeName", title: "Base Recipe", width: "120px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },

            {
                field: "UOMName", title: "UOM", width: "50px",
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "uomname",
                    style: "text-align: center; font-weight:normal"
                },
            },
            {
                field: "Quantity", title: "Quantity", width: "35px",
                headerAttributes: {
                    style: "text-align: center;width:35px;"
                },
                attributes: {
                    style: "text-align: center; font-weight:normal;width:35px;"
                },
            },
            {
                field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "major",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
            },
            {
                field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {
                    style: "text-align: center;"
                },
                attributes: {
                    class: "minor",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
            },
            {
                field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format, editable: false,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "uomcost",
                    style: "text-align: right; font-weight:normal"
                },
            },
            {
                field: "TotalCost", title: "Total Cost", width: "50px", editable: false, format: Utility.Cost_Format,
                headerAttributes: {

                    style: "text-align: right"
                },
                attributes: {
                    class: "totalcost",
                    style: "text-align: right; font-weight:normal"
                },
            }

        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetBaseRecipesByRecipeID, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }

                        if (result.length > 0) {
                            $("#gridBaseRecipe1").css("display", "block");
                            $("#emptybr1").css("display", "none");
                        } else {
                            $("#gridBaseRecipe1").css("display", "none");
                            $("#emptybr1").css("display", "block");
                        }

                    },
                        {
                            recipeCode: recipeCode
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ID: { editable: false },
                        BaseRecipe_ID: { type: "number", validation: { required: true } },
                        BaseRecipeName: { type: "string", validation: { required: true } },
                        Quantity: { editable: false },
                        UOMName: { editable: false },
                        IngredientPerc: { editable: false },
                        CostPerUOM: { editable: false },
                        CostPerKG: { editable: false },
                        TotalCost: { editable: false }
                    }
                }
            }
        },

        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },

        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();
            items.each(function (e) {
                var dataItem = grid.dataItem(this);
                var ddt = $(this).find('.brTemplate');
                $(ddt).kendoDropDownList({
                    index: 0,
                    filter: "contains",
                    value: dataItem.value,
                    dataSource: baserecipedata,
                    dataTextField: "BaseRecipeName",
                    dataValueField: "BaseRecipe_ID",
                    change: onBRDDLChange,
                    //  autoBind: true,
                });
                //  ddt.find(".k-input").val(dataItem.BaseRecipeName);
                $(this).find('.inputbrqty').val(dataItem.Quantity);
            });
        },
        change: function (e) {
        },
    });
    //  cancelChangesConfirmation('gridBaseRecipe');
    var toolbar = $("#gridBaseRecipe").find(".k-grid-toolbar");


    toolbar.find(".k-add").remove();
    toolbar.find(".k-cancel").remove();
    toolbar.find(".k-grid-new").addClass("gridButton");
    toolbar.find(".k-grid-clearall").addClass("gridButton");

    toolbar.addClass("gridToolbarShift");//("style", "margin-top :-32px!important;background-color:#76B4B0!important;");
    toolbar.find(".k-grid-cancel-changes").addClass("gridButton");

    toolbar.find(".k-grid-add").addClass("gridButton");
    $("#gridBaseRecipe .k-grid-content").addClass("gridInside");
    toolbar.find(".k-grid-clearall").addClass("gridButton");
    $('#gridBaseRecipe a.k-grid-clearall').click(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                $("#gridBaseRecipe").data('kendoGrid').dataSource.data([]);
                gridIdin = 'gridBaseRecipe';
                // showHideGrid('gridBaseRecipe', 'emptybr', 'bup_gridBaseRecipe');
                hideGrid('emptybr', 'gridBaseRecipe');
            },
            function () {
            }
        );

    });
    $(".k-grid-cancel-changes").click(function (e) {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                // var grid = $("#" + gridIdin + "").data("kendoGrid");
                if (gridIdin == 'gridBaseRecipe') {
                    populateBaseRecipeGrid(resetID);
                    return;
                } else if (gridIdin == 'gridMOG') {
                    populateMOGGrid(resetID)
                    return;
                }
                // grid.cancelChanges();
            },
            function () {
            }
        );

    });

    $('a.k-grid-new').click(function (e) {
        var model;
        var dialog = $("#windowEditx").data("kendoWindow");
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        populateUOMDropdownx();
        populateBaseDropdownx();
        populateBaseRecipeGridx();
        populateMOGGridx();

        datamodel = model;
        $("#recipeidx").text("");
        $("#inputrecipenamex").val("");
        // $("#inputuom0").data('kendoDropDownList').value("Select");
        $("#inputquantityx").val("10");
        $("#inputbasex").data('kendoDropDownList').value("Select");
        dialog.title("New Base Recipe Creation");
        Utility.UnLoading();
    });
}
function refreshMOGDropDownData(x) {

    if (mogdataList != null && mogdataList.length > 0) {
        var mogGridData = $("#gridMOG").data('kendoGrid').dataSource._data;

        if (mogGridData != null && mogGridData.length > 0) {
            mogGridData = mogGridData.filter(m => m.MOGName != "" && m.MOGName != x);
            mogdata = mogdataList;
            var mogdataListtemp = mogdataList;
            mogdataListtemp = mogdataListtemp.filter(m => m.MOG_Code != "");
            mogdataListtemp.forEach(function (item, index) {
                for (var i = 0; i < mogGridData.length; i++) {
                    if (mogGridData[i].MOGCode !== "" && item.MOG_Code === mogGridData[i].MOGCode) {
                        // mogdata.splice(index, 1);
                        mogdata = mogdata.filter(mog => mog.MOG_Code !== item.MOG_Code);
                        break;
                    }
                }
                return true;
            });
        }
    }
    return mogdata.filter(m => m.IsActive == true);
};

function populateAPLMasterGrid() {
    Utility.Loading();


    var gridVariable = $("#gridMOGWiseAPL");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "APLMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        //reorderable: true,
        scrollable: true,
        columns: [
            {
                field: "ArticleNumber", title: "Article Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ArticleDescription", title: "Article Name", width: "150px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOM", title: "UOM", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "SiteCode", title: "SiteCode", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            },
            {
                field: "SiteName", title: "SiteName", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            }
            ,
            {
                field: "ArticleCost", title: "Standard Cost", width: "50px", format: Utility.Cost_Format,
                attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            },
            {
                field: "StandardCostPerKg", title: "Cost Per KG", width: "50px", format: Utility.Cost_Format,
                attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            }

        ],
        dataSource: {
            data: mogWiseAPLMasterdataSource,
            schema: {
                model: {
                    id: "ArticleID",
                    fields: {
                        ArticleNumber: { type: "string" },
                        ArticleDescription: { type: "string" },
                        UOM: { type: "string" },
                        ArticleCost: { type: "string" },
                        StandardCostPerKg: { type: "string" },
                        MerchandizeCategoryDesc: { type: "string" },
                        MOGName: { type: "string" },
                        StandardCostPerKg: { editable: false },
                        ArticleCost: { editable: false },
                        SiteCode: { type: "string" },
                        SiteName: { type: "string" }

                    }
                }
            },
            // pageSize: 5,
        },
        // height: 404,
        noRecords: {
            template: "No Records Available"
        },

        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {


        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
    //$(".k-label")[0].innerHTML.replace("items", "records");
}
function populateMOGGrid(recipeCode) {

    Utility.Loading();
    var gridVariable = $("#gridMOG1");
    gridVariable.html("");
    if (user.SectorNumber == '20') {
        gridVariable.kendoGrid({
            // toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,
            //navigatable: true,
            columns: [
                {
                    field: "MOGName", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {

                        class: 'uomname',
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: left;width:35px;"
                    },
                },
                {
                    field: "DKgTotal", title: "DKG", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: left;width:35px;"
                    },
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "uomcost",

                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG1").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG1").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {
                                recipeCode: recipeCode
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "MOG_ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            Quantity: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        index: 0,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });


            },
            change: function (e) {
                if (e.action == "itemchange") {
                    e.items[0].dirtyFields = e.items[0].dirtyFields || {};
                    e.items[0].dirtyFields[e.field] = true;
                }
            },
        });
    }
    else {
        gridVariable.kendoGrid({
            // toolbar: [{ name: "clearall", text: "Clear All" }, { name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,
            //navigatable: true,
            columns: [
                {
                    field: "MOGName", title: "MOG", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "UOMName", title: "UOM", width: "50px",
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {

                        class: 'uomname',
                        style: "text-align: center; font-weight:normal"
                    },
                },

                {
                    field: "Quantity", title: "Quantity", width: "35px", editable: false,
                    headerAttributes: {
                        style: "text-align: center;width:35px;"
                    },
                },
                {
                    field: "IngredientPerc", title: "Major %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "major",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>#= IngredientPerc# </div>#} else {#<div>-</div>#}#',
                },
                {
                    field: "IngredientPerc", title: "Minor %", width: "50px", format: Utility.Cost_Format, editable: false,
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        class: "minor",
                        style: "text-align: center; font-weight:normal"
                    },
                    template: '# if (IsMajor == true) {#<div>-</div>#} else {#<div>#= IngredientPerc# </div>#}#',
                },
                {
                    field: "CostPerUOM", title: "Cost/UOM", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "uomcost",

                        style: "text-align: right; font-weight:normal"
                    },
                },
                {
                    field: "TotalCost", title: "Total Cost", width: "50px", format: Utility.Cost_Format,
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    attributes: {
                        class: "totalcost",
                        style: "text-align: right; font-weight:normal"
                    },
                },

            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGsByRecipeID, function (result) {
                            Utility.UnLoading();

                            if (result != null) {

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }

                            if (result.length > 0) {
                                $("#gridMOG1").css("display", "block");
                                $("#emptymog").css("display", "none");
                            } else {
                                $("#gridMOG1").css("display", "none");
                                $("#emptymog").css("display", "block");
                            }
                        },
                            {
                                recipeCode: recipeCode
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "MOG_ID",
                        fields: {
                            MOG_ID: { type: "number", validation: { required: true } },
                            MOGName: { type: "string", validation: { required: true } },
                            Quantity: { editable: false },
                            UOMName: { editable: false },
                            Quantity: { editable: false },
                            IngredientPerc: { editable: false },
                            CostPerUOM: { editable: false },
                            CostPerKG: { editable: false },
                            TotalCost: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = e.sender;
                var items = e.sender.items();
                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.mogTemplate');
                    $(ddt).kendoDropDownList({
                        index: 0,
                        value: dataItem.value,
                        dataSource: baserecipedata,
                        dataTextField: "MOGName",
                        dataValueField: "MOG_ID",
                        change: onMOGDDLChange
                    });
                    //ddt.find(".k-input").val(dataItem.MOGName);

                    $(this).find('.inputmogqty').val(dataItem.Quantity);
                });


            },
            change: function (e) {
                if (e.action == "itemchange") {
                    e.items[0].dirtyFields = e.items[0].dirtyFields || {};
                    e.items[0].dirtyFields[e.field] = true;
                }
            },
        });
    }
    Utility.UnLoading();
}
function showHideGrid(gridId, emptyGrid, uid) {

    var gridLength = $("#" + gridId).data().kendoGrid.dataSource.data().length;

    if (gridLength == 0) {
        $("#" + emptyGrid).toggle();
        $("#" + gridId).hide();
    }
    else {
        $("#" + emptyGrid).hide();
        $("#" + gridId).toggle();
    }

    if (!($("#" + gridId).is(":visible") || $("#" + emptyGrid).is(":visible"))) {
        $("#" + uid).addClass("bottomCurve");

    } else {
        $("#" + uid).removeClass("bottomCurve");

    }

}
function populateUOMDropdown() {

    $("#inputuom1").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}
function populateUOMDropdown_Bulk() {
    $("#uom").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
}

function populateBaseDropdown() {
    $("#inputbase1").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: basedata,
        index: 0,
    });
}
function getMOGWiseAPLMasterData() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG, function (result) {
        Utility.UnLoading();

        if (result != null) {
            mogWiseAPLMasterdataSource = result;
            console.log(result);
        }
        else {
        }
    }, { mogCode: MOGCode }
        , true);
}
function showDetailsCustom(e) {
    var element = e
    var row = element.closest("tr");
    var grid = $("#gridRecipeCustom").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    var gridObj = $("#gridRecipeCustom").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.Name == dataItem.Name)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    RecipeName = tr.Name;

    var dialog = $("#windowEdit1").data("kendoWindow");
    $("#windowEdit1").kendoWindow({
        animation: false
    });
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open().element.closest(".k-window").css({
        top: 167
    });
    //dialog.open();
    dialog.center();
    basedata = [{ "value": "Select", "text": "Select" }, { "value": "No", "text": "Final Recipe" },
    { "value": "Yes", "text": "Base Recipe" }];
    populateUOMDropdown();
    populateBaseDropdown();
    //getMOGWiseAPLMasterData();
    if (user.SectorNumber !== '20') {
        populateBaseRecipeGrid(tr.RecipeCode);
    }
    populateMOGGrid(tr.RecipeCode);
    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOMCode);
    $("#inputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");
    $('#grandTotal1').html(tr.TotalCost);
    $('#grandCostPerKG1').html(tr.CostPerKG);
    $('#inputinstruction1').val(tr.Instructions);
    dialog.title("Recipe Details Inline - " + RecipeName);
    return true;

}

function populateHealthTagMasterDropdown() {
    var autocomplete = $("#inputHealthTag").data("kendoMultiSelect");
    // Check the returned value.
    if (typeof autocomplete === "undefined") {
        $("#inputHealthTag").kendoMultiSelect({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: healthTagMasterDataSource,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
        });
    }
    //var multiselect = $("#inputHealthTag").data("kendoMultiSelect");

    //clear filter
    //multiselect.dataSource.filter({});

    //set value
    // multiselect.value(mogWiseAPLArray);

}


function populateMenuTypeMasterDropdown() {
    $("#inputDishMenuType").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: menuTypeMasterDataSource,
        index: 0,
    });
}


function populatePatientBillingProfileMasterDropdown() {
    var autocomplete = $("#inputPatientBilingProfile").data("kendoMultiSelect");
    // Check the returned value.
    if (typeof autocomplete === "undefined") {
        $("#inputPatientBilingProfile").kendoMultiSelect({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: patientBillingProfileMasterDataSource,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
        });
    }
}

function populateLifeStyleTagMasterDropdown() {
    //if ($('#inputLifeStyleTag').data('kendoMultiSelect')) {
    //    $('#inputLifeStyleTag').data('kendoMultiSelect').destroy();
    //    $('#inputLifeStyleTag').unwrap('.k-multiselect').show().empty();
    //    $(".k-multiselect-wrap").remove();
    //}
    var autocomplete = $("#inputLifeStyleTag").data("kendoMultiSelect");
    // Check the returned value.
    if (typeof autocomplete === "undefined") {
        $("#inputLifeStyleTag").kendoMultiSelect({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: lifeStyleTagMasterDataSource,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
        });
    }
    //var multiselect = $("#inputLifeStyleTag").data("kendoMultiSelect");

    //clear filter
    //multiselect.dataSource.filter({});

    //set value
    // multiselect.value(mogWiseAPLArray);

}

function populateDietTypeMasterDropdown() {
    //if ($('#inputDietType').data('kendoMultiSelect')) {
    //    $('#inputDietType').data('kendoMultiSelect').destroy();
    //    $('#inputDietType').unwrap('.k-multiselect').show().empty();
    //    $(".k-multiselect-wrap").remove();
    //}
    var autocomplete = $("#inputDietType").data("kendoMultiSelect");
    // Check the returned value.
    if (typeof autocomplete === "undefined") {
        $("#inputDietType").kendoMultiSelect({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: diettypeMasterDataSource,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
        });
    }
    //var multiselect = $("#inputDietType").data("kendoMultiSelect");

    //clear filter
    //multiselect.dataSource.filter({});

    //set value
    // multiselect.value(mogWiseAPLArray);

}




function addNewRecipe() {
    $("#mapRecipe").hide();
}
//function backCreate() {
//    $("#mapRecipe").show();
//    $("#windowEdit0").hide();
//    var multiselect = $("#inputrecipelist").data("kendoMultiSelect");

//    HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {
//        recipedata = data;
//        multiselect.setDataSource(data);
//    }, { condition: "All" }, true);



//}
function backCreate() {
    $("#mapRecipe").show();
    $("#windowEdit0").hide();
    //var multiselect = $("#inputrecipelist").data("kendoMultiSelect");
    //HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {
    //    recipedata = data;
    //    multiselect.setDataSource(data);
    //}, { condition: "All" }, true);

}