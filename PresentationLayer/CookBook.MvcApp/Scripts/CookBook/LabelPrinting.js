﻿var user;
var datamodel;
var ShelfLifeUOMData = [];
var ExpiryCategorydata = [];
var ExpiryCategoryCodeIntial = null;

//function populateShelfLifedrpodown() {
//    $("#dpShelfLifeUOM").kendoDropDownList({
//        filter: "contains",
//        dataTextField: "text",
//        dataValueField: "ShelfLifeUOMCode",
//        dataSource: ShelfLifeUOMData,
//        index: 0,

//        change: function (e) {
//            if (!e.sender.dataItem()?.IsActive) {
//                toastr.error("Selected Item is In-Active State");
//                var dropdownlist = $("#dpShelfLifeUOM").data("kendoDropDownList");
//                dropdownlist.select("");
//            }

//        }
//    });
//}
Date.prototype.toShortFormat = function () {

    let monthNames = ["Jan", "Feb", "Mar", "Apr",
        "May", "Jun", "Jul", "Aug",
        "Sep", "Oct", "Nov", "Dec"];

    let day = this.getDate();

    let monthIndex = this.getMonth();
    let monthName = monthNames[monthIndex];

    let year = this.getFullYear();

    return `${day}-${monthName}-${year}`;
}

Date.prototype.toShortFormatDDMMYY = function () {

    let monthNames = ["Jan", "Feb", "Mar", "Apr",
        "May", "Jun", "Jul", "Aug",
        "Sep", "Oct", "Nov", "Dec"];

    let day = String("0" + this.getDate()).slice(-2);;

    let monthIndex = this.getMonth();
    let monthName = monthNames[monthIndex];

    let year = this.getFullYear().toString().substr(-2);

    return `${day} ${monthName} ${year}`;
}

function parseDate(s) {
    var months = {
        jan: 0, feb: 1, mar: 2, apr: 3, may: 4, jun: 5,
        jul: 6, aug: 7, sep: 8, oct: 9, nov: 10, dec: 11
    };
    var p = s.split('-');
    var t = p[2].split(' ')[1];
    return new Date(p[2].split(' ')[0], months[p[1].toLowerCase()], p[0], t.split(':')[0], t.split(':')[1]);
}

$(document).ready(function () {
    $(".k-window").hide();
    $(".k-overlay").hide();
    //$("#ReaswindowEdit").kendoWindow({
    //    modal: true,
    //    width: "300px",

    //    title: "ExpiryCategory Details  ",
    //    actions: ["Close"],
    //    visible: false,
    //    animation: false
    //});
    $("#doprint").click(function () {
        var grid = $('#gridExpiryCategoryMapping').data('kendoGrid');
        var isSelected = false;
        grid.tbody.find("tr[role='row']").each(function () {
            var objNoofLabel = $(this).find("input.labelno");
            if ($(objNoofLabel).val() > 0) {
                isSelected = true;
            }
        });
        if (isSelected)
            DownloadLablePrintingFile();
        else
            toastr.error("Please enter No Of Labels for at least one product.");
    });

    function DownloadLablePrintingFile() {
        var labelData = [];
        var grid = $("#gridExpiryCategoryMapping").data("kendoGrid");
        grid.tbody.find("tr[role='row']").each(function () {
          //  debugger;
            var model = grid.dataItem(this);
            var objLabel = $(this).find("label.labelexpdate");
            var objDate = $(this).find("input.datetimepicker6");
            var objNoofLabel = $(this).find("input.labelno");
            if ($(objNoofLabel).val() > 0) {
                var obj = {
                    "ProductName": "",
                    "ScannedDate": "",
                    "ScannedTime": "",
                    "PermitDay": "",
                    "PermitHour": "",
                    "ExpiryDate": "",
                    "ExpiryTime": "",
                    "LabelCount": "",
                }
                obj.ProductName = model.ProductName;

                var today = new Date(objDate[0].value);
                //var yyyy = today.getFullYear().toString().substr(-2);
                //let mm = monthNames[today.getMonth()]; // Months start at 0!
                //let dd = today.getDate();
                //today = dd + '-' + mm + '-' + yyyy;
                obj.ScannedDate = today.toShortFormatDDMMYY();

                var dt = new Date(objDate[0].value);
                var time = dt.getHours().toString().padStart(2, '0') + ":" + dt.getMinutes().toString().padStart(2, '0') + ":" + dt.getSeconds().toString().padStart(2, '0');
                obj.ScannedTime = time;

                var cellValue = "";
                if (model.ShelfLifeUOMCode == "SUM-00001") {//Days
                    cellValue = model.ShelfLife + " " + model.ShelfLifeUOMName;
                }
                obj.PermitDay = cellValue;
                var cellValue = "";
                if (model.ShelfLifeUOMCode == "SUM-00002") {//Hours
                    cellValue = model.ShelfLife + " " + model.ShelfLifeUOMName;
                }
                obj.PermitHour = cellValue;

                var str = $(objLabel).text();
                if (str.length > 0) {
                    var result = parseDate(str);
                    //var yyyy = result.getFullYear().toString().substr(-2);
                    //let mm = monthNames[result.getMonth()]; // Months start at 0!
                    //let dd = result.getDate();
                    //var cellValue = dd + '-' + mm + '-' + yyyy;
                    obj.ExpiryDate = result.toShortFormatDDMMYY();
                }
                else
                    obj.ExpiryDate = "";

                var str = $(objLabel).text();
                if (str.length > 0) {
                    var result = parseDate(str);
                    var dateStr =
                        ("00" + result.getHours()).slice(-2) + ":" +
                        ("00" + result.getMinutes()).slice(-2) + ":" +
                        ("00" + result.getSeconds()).slice(-2);

                    var cellValue = dateStr;
                    obj.ExpiryTime = cellValue;
                }
                else
                    obj.ExpiryTime = "";

                var str = $(objNoofLabel).val();
                obj.LabelCount = str;
                labelData.push(obj);
            }
        });
        HttpClient.MakeRequest(CookBookMasters.ExportLabelPrintingData, function (result) {
            if (result != null) {
                var bytes = new Uint8Array(result.FileContents);
                var blob = new Blob([bytes], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = "ExpiryCategoryData.xlsx";
                link.click();
            }
        },
            {
                labelPrintingData: labelData
            }
            , true);
    }

    $('#myInput').on('input', function (e) {
        var grid = $('#gridExpiryCategoryMapping').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ProductCode" || x.field == "ProductName" || x.field == "ProductType" || x.field == "ProductExpiryName" || x.field == "ShelfLife" || x.field == "ShelfLifeUOMName") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        if (x.field == "Status") {
                            var pendingString = "pending";
                            var savedString = "saved";
                            var mappedString = "mapped";
                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "3";
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    //HttpClient.MakeSyncRequest(CookBookMasters.GetShelfLifeUOMDataList, function (result) {
    //    dataSource = result;
    //    ShelfLifeUOMData.push({ "value": null, "text": "Select", "ShelfLifeUOMCode": null, "IsActive": 1 });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        ShelfLifeUOMData.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "ShelfLifeUOMCode": dataSource[i].ShelfLifeUOMCode, "IsActive": dataSource[i].IsActive });
    //    }
    //    //populateShelfLifedrpodown();

    //    populateExpiryCategoryGrid();
    //}, null, false);
    populateExpiryProductGrid();
});

function populateExpiryProductGrid() {


    var gridVariable = $("#gridExpiryCategoryMapping");
    gridVariable.html("");
    gridVariable.kendoGrid({
        //excel: {
        //    fileName: "ExpiryCategoryData.xlsx",
        //    filterable: false,
        //    allPages: true
        //},
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,
        pageable: {
            pageSize: 150,
            messages: {
                display: "{0}-{1} of {2} records"
            }
        },
        //reorderable: true,
        scrollable: true,
        columns: [
            {
                field: "ProductCode", title: "Product Code", attributes: {
                    style: "text-align: left; font-weight:normal;vertical-align:top"
                },
                headerAttributes: {
                    style: "text-align: left;"
                }
            },
            {
                field: "ProductName", title: "Product Name", attributes: {
                    style: "text-align: left; font-weight:normal;vertical-align:top"
                },
            },
            {
                field: "ProductType", title: "Product Type", attributes: {
                    style: "text-align: left; font-weight:normal;vertical-align:top"
                }
            },
            {
                field: "ProductExpiryName", title: "Expiry Category", attributes: {
                    style: "text-align: left; font-weight:normal;vertical-align:top"
                },
            },
            {
                field: "ShelfLife", title: "Shelf Life", attributes: {
                    style: "text-align: left; font-weight:normal;vertical-align:top"
                },
            },
            {
                field: "ShelfLifeUOMName", title: "Shelf Life UOM", attributes: {
                    style: "text-align: left; font-weight:normal;vertical-align:top"
                },
            },
            {
                title: 'Prep/Receiving Date',
                //headerTemplate: "<input type='checkbox' id='header-chb' class='k-checkbox-md k-rounded-md header-checkbox'>",
                template: function (dataItem) {
                    //return "<input type='checkbox' id='" + dataItem.Id + "' data-code='"+dataItem.RecipeCode+"' class='k-checkbox-md k-rounded-md row-checkbox'>";
                    //return "<label class='checkbox-container' style='display: inline-block;padding-top:5px' ><input type='checkbox' class='checkbox' id='" + dataItem.Id + "' data-code='" + dataItem.RecipeCode + "' ><span class='checkmarkgrid'></span></label>"
                    return "<input type='text' id='" + dataItem.ProductCode + "' class='form-control datetimepicker6'/>"
                },
                width: "160px",
                attributes: { style: "text-align: center; font-weight:normal" },
                headerAttributes: { style: "text-align: center" },
            },
            //{
            //    field: "PrepDate", title: "Prep Date", width: "60px", attributes: {
            //        style: "text-align: left; font-weight:normal"
            //    },
            //},
            {
                title: 'Expiry Date',
                template: function (dataItem) {
                    //return "<input type='checkbox' id='" + dataItem.Id + "' data-code='"+dataItem.RecipeCode+"' class='k-checkbox-md k-rounded-md row-checkbox'>";
                    //return "<label class='checkbox-container' style='display: inline-block;padding-top:5px' ><input type='checkbox' class='checkbox' id='" + dataItem.Id + "' data-code='" + dataItem.RecipeCode + "' ><span class='checkmarkgrid'></span></label>"
                    return "<label class='labelexpdate'/>"
                },
                //width: "80px",
                attributes: { style: "text-align: center; font-weight:normal" },
                headerAttributes: { style: "text-align: center" },

            },
            {
                title: 'No of Labels',
                headerTemplate: "No Of Labels<input type='text' id='headerNol' class='form-control labelno'/>",
                template: function (dataItem) {
                    //return "<input type='checkbox' id='" + dataItem.Id + "' data-code='"+dataItem.RecipeCode+"' class='k-checkbox-md k-rounded-md row-checkbox'>";
                    //return "<label class='checkbox-container' style='display: inline-block;padding-top:5px' ><input type='checkbox' class='checkbox' id='" + dataItem.Id + "' data-code='" + dataItem.RecipeCode + "' ><span class='checkmarkgrid'></span></label>"
                    return "<input type='text' id='" + dataItem.ID + "' class='form-control labelno'/>"
                },
                width: "80px",
                attributes: { style: "text-align: center; font-weight:normal" },
                headerAttributes: { style: "text-align: center" },
            },

        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(CookBookMasters.GetExpiryProductDataList, function (result) {
                        Utility.UnLoading();
                        if (result != null) {
                            console.log(result)
                            options.success(result);
                        }
                        else {
                            options.success("");
                        }
                    }, null
                        //{
                        //filter: mdl
                        //}
                        , false);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ProductCode: { type: "string" },
                        ProductName: { type: "string" },
                        ProductType: { type: "string" },
                        ProductExpiryCode: { type: "string" },
                        ProductExpiryName: { type: "string" },
                        ShelfLifeUOMCode: { type: "string" },
                        ShelfLife: { type: "int" },
                        ShelfLifeUOMName: { type: "string" },
                        //PrepDate: { editable: true },

                    }
                }
            },
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {

            var items = e.sender.items();
            var grid = this;

            //grid.tbody.find("tr[role='row'] .datetimepicker6").each(function () {
            //    //$(this).datetimepicker();
            //    var objLabel = $(this).find("label.labelexpdate");
            //    $(this).kendoDateTimePicker({
            //        format: "dd/MM/yyyy hh:mm",
            //        value: new Date(),
            //        change: onchangeExpDate(this, objLabel, model.ShelfLifeUOMCode)
            //    });
            //});

            grid.tbody.find("tr[role='row']").each(function () {
                var model = grid.dataItem(this);

                var objLabel = $(this).find("label.labelexpdate");
                var objDate = $(this).find("input.datetimepicker6");
                $(objDate).kendoDateTimePicker({
                    format: "dd-MMM-yyyy HH:mm",
                    value: new Date(),
                    change: function () {
                        onchangeExpDate(objDate, objLabel, model.ShelfLifeUOMCode, model.ShelfLife);
                    }
                    //change: onchangeExpDate(this, objLabel, model.ShelfLifeUOMCode)
                });

                if (model.ProductExpiryCode == "" || model.ProductExpiryCode == null) {
                    $(this).find(".k-grid-PrintLabel").addClass("k-state-disabled");
                }

                onchangeExpDate(objDate, objLabel, model.ShelfLifeUOMCode, model.ShelfLife);

            });


            $('#headerNol').on('input', function (ev) {
                var val = $(this).val();
                $('.labelno').each(function (idx, item) {
                    $(this).val(val);
                });
            });
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (view[i].Status == 1) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffe1e1");

                }
                else if (view[i].Status == 2) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffffbf");
                }
                else {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#d9ffb3");
                }

            }
            var items = e.sender.items();
        },
        change: function (e) {
        },
        excelExport: function onExcelExport(e) {
            var data = e.data;
            var labelData = [];
            debugger;
           

            //if (data != null && data.length > 0) {
            //    for (var i = 0; i < data.length; i++) {
            //        var obj = {
            //            "ProductName": $("#inputrecipecode0").val(),
            //            "ScannedDate": $("#recipeid0").val(),
            //            "ScannedTime": data[i].MOG_ID,
            //            "PermitDay": data[i].MOGCode,
            //            "PermitHour": data[i].UOMCode,
            //            "ExpiryDate": data[i].UOM_ID,
            //            "ExpiryTime": data[i].CostPerUOM,
            //            "LabelCount": data[i].CostPerKG,
            //        }
            //        labelData.push(obj);
            //    }
            //}


          
        }
    });
}

    //        var sheet = e.workbook.sheets[0];
    //        var data = e.data;
    //        var cols = Object.keys(data[0])
    //        var columns = cols.filter(function (col) {
    //            if (col == "ProductName" || col == "ProductType" || col == "ProductExpiryName" || col == "ShelfLife" || col == "ShelfLifeUOMName") {
    //                return col;
    //            }
    //        });
    //        var columns1 = columns.map(function (col) {
    //            return {
    //                value: col,
    //                autoWidth: true,
    //                background: "#7a7a7a",
    //                color: "#fff"
    //            };
    //        });
    //        console.log(columns1);
    //        columns1 = [];
    //        columns1.push({
    //            value: "Product Name",
    //            autoWidth: true,
    //            background: "#7a7a7a",
    //            color: "#fff"
    //        });
    //        columns1.push({
    //            value: "Scanned Date",
    //            autoWidth: true,
    //            background: "#7a7a7a",
    //            color: "#fff"
    //        });
    //        columns1.push({
    //            value: "Scanned Time",
    //            autoWidth: true,
    //            background: "#7a7a7a",
    //            color: "#fff"
    //        });
    //        columns1.push({
    //            value: "Permit Day",
    //            autoWidth: true,
    //            background: "#7a7a7a",
    //            color: "#fff"
    //        });
    //        columns1.push({
    //            value: "Permit Hours",
    //            autoWidth: true,
    //            background: "#7a7a7a",
    //            color: "#fff"
    //        });
    //        columns1.push({
    //            value: "Expiry Date",
    //            autoWidth: true,
    //            background: "#7a7a7a",
    //            color: "#fff"
    //        });
    //        columns1.push({
    //            value: "Expiry Time",
    //            autoWidth: true,
    //            background: "#7a7a7a",
    //            color: "#fff"
    //        });
           
    //        var rows = [{ cells: columns1, type: "header" }];
    //        var grid = $("#gridExpiryCategoryMapping").data("kendoGrid");
    //        grid.tbody.find("tr[role='row']").each(function () {
    //            var model = grid.dataItem(this);
    //            var objLabel = $(this).find("label.labelexpdate");
    //            var objDate = $(this).find("input.datetimepicker6");
    //            var objNoofLabel = $(this).find("input.labelno");
    //            if ($(objNoofLabel).val() > 0) {
    //                var rowCells = [];
    //                for (var j = 0; j < columns1.length; j++) {
    //                    if (j == 0) {
    //                        var cellValue = model.ProductName;
    //                        rowCells.push({ value: cellValue });
    //                    }
    //                    if (j == 1) {
    //                        var today = new Date();
    //                        var yyyy = today.getFullYear();
    //                        let mm = today.getMonth() + 1; // Months start at 0!
    //                        let dd = today.getDate();
    //                        today = dd + '/' + mm + '/' + yyyy;
    //                        var cellValue = today;
    //                        rowCells.push({ value: cellValue });
    //                    }
    //                    if (j == 2) {
    //                        var dt = new Date();
    //                        var time = dt.getHours().toString().padStart(2, '0') + ":" + dt.getMinutes().toString().padStart(2, '0') + ":" + dt.getSeconds().toString().padStart(2, '0');
    //                        var cellValue = time;
    //                        rowCells.push({ value: cellValue });
    //                    }
    //                    if (j == 3) {
    //                        var cellValue = "";
    //                        if (model.ShelfLifeUOMCode == "SUM-001") {//Days
    //                            cellValue = model.ShelfLife + " " + model.ShelfLifeUOMName;
    //                        }
    //                        rowCells.push({ value: cellValue });
    //                    }
    //                    if (j == 4) {
    //                        var cellValue = "";
    //                        if (model.ShelfLifeUOMCode == "SUM-002") {//Hours
    //                            cellValue = model.ShelfLife + " " + model.ShelfLifeUOMName;
    //                        }
    //                        rowCells.push({ value: cellValue });
    //                    }
    //                    if (j == 5) {
    //                        var str = $(objLabel).text();
    //                        if (str.length > 0) {
    //                            var result = parseDate(str);
    //                            var yyyy = result.getFullYear();
    //                            let mm = result.getMonth() + 1; // Months start at 0!
    //                            let dd = result.getDate();
    //                            var cellValue = dd + '/' + mm + '/' + yyyy;
    //                            rowCells.push({ value: cellValue });
    //                        }
    //                        else
    //                            rowCells.push({ value: "" });
    //                    }
    //                    if (j == 6) {
    //                        var str = $(objLabel).text();
    //                        if (str.length > 0) {
    //                            var result = parseDate(str);
    //                            var dateStr =
    //                                ("00" + result.getHours()).slice(-2) + ":" +
    //                                ("00" + result.getMinutes()).slice(-2) + ":" +
    //                                ("00" + result.getSeconds()).slice(-2);

    //                            var cellValue = dateStr;
    //                            rowCells.push({ value: cellValue });
    //                        }
    //                        else
    //                            rowCells.push({ value: "" });
    //                    }
    //                    if (j == 7) {
    //                        var str = $(objNoofLabel).val();
    //                        console.log(str)
    //                        var cellValue = str;
    //                        rowCells.push({ value: cellValue });
    //                    }
    //                }
    //                rows.push({ cells: rowCells, type: "data" });
    //            }
    //        });
           
    //        sheet.rows = rows;
    //    }
//    })
//}
function onchangeExpDate(dateObj, labelObj, shellLifeCode, ShelfLife) {
    if (shellLifeCode == "SUM-00001") {//Days
        var str = $(dateObj).val();
        var result = parseDate(str);
        //console.log(str);
        //const [dateComponents, timeComponents] = str.split(' ');

        //const [day, month, year] = dateComponents.split('/');
        //const [hours, minutes] = timeComponents.split(':');

        //var result = new Date(+year, month - 1, +day, +hours, +minutes);

        result.setDate(result.getDate() + ShelfLife);

        var timeStr =
            //("00" + result.getDate()).slice(-2) + "/" +
            //("00" + (result.getMonth() + 1)).slice(-2) + "/" +            
            //result.getFullYear() + " " +
            ("00" + result.getHours()).slice(-2) + ":" +
            ("00" + result.getMinutes()).slice(-2);
        //+ ":" +
        //("00" + result.getSeconds()).slice(-2);

        var dateStr = result.toShortFormat() + " " + timeStr;
        $(labelObj).text(dateStr);
    }
    if (shellLifeCode == "SUM-00002") {//Hours
        var str = $(dateObj).val();
        var result = parseDate(str);
        //const [dateComponents, timeComponents] = str.split(' ');

        //const [day, month, year] = dateComponents.split('/');
        //const [hours, minutes] = timeComponents.split(':');

        //var result = new Date(+year, month - 1, +day, +hours, +minutes);
        //result.setDate(result.getDate() + model.ShelfLife);
        result.setTime(result.getTime() + (ShelfLife * 60 * 60 * 1000));
        var timeStr =
            //("00" + result.getDate()).slice(-2) + "/" +
            //("00" + (result.getMonth() + 1)).slice(-2) + "/" +            
            //result.getFullYear() + " " +
            ("00" + result.getHours()).slice(-2) + ":" +
            ("00" + result.getMinutes()).slice(-2);
        //+ ":" +
        //("00" + result.getSeconds()).slice(-2);
        var dateStr = result.toShortFormat() + " " + timeStr;
        $(labelObj).text(dateStr);
    }
}
//function dpvalidate() {
//    var valid = true;

//    if ($("#dpname").val() === "") {
//        toastr.error("Please provide input");
//        $("#dpname").addClass("is-invalid");
//        valid = false;
//        $("#dpname").focus();
//    }
//    if (($.trim($("#dpname").val())).length > 59) {
//        toastr.error("ExpiryCategory Description can't accepts upto 60 charactre");

//        $("#dpname").addClass("is-invalid");
//        valid = false;
//        $("#dpname").focus();
//    }
//    return valid;
//}
//$(document).ready(function () {
//    $("#dpaddnew").on("click", function () {
//        var model;
//        var dialog = $("#ReaswindowEdit").data("kendoWindow");

//        dialog.open().element.closest(".k-window").css({
//            top: 167,
//            left: 558

//        });

//        datamodel = model;

//        dialog.title("New ExpiryCategory Details Creation");




//        $("#dpname").removeClass("is-invalid");
//        $('#secredpsubmit').removeAttr("disabled");
//        $("#dpid").val("");
//        $("#dpname").val("");
//        $("#dpcode").val("");
//        $("#dpshelflife").val("");
//        $("#dpname").focus();
//        $("#dpShelfLifeUOM").data('kendoDropDownList').value(null)

//        ExpiryCategoryCodeIntial = null;
//    });

//    $("#secredpcancel").on("click", function () {
//        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
//            function () {
//                $(".k-overlay").hide();
//                var orderWindow = $("#ReaswindowEdit").data("kendoWindow");
//                orderWindow.close();
//            },
//            function () {
//            }
//        );
//    });
//    $("#secredpsubmit").click(function () {
//        var msg = "";
//        // if (user.SectorNumber == "20") {
//        msg = "ExpiryCategory";

//        var namedp = $("#dpname").val().toLowerCase().trim();
//        if (ExpiryCategoryCodeIntial == null) { //Duplicate Check
//            for (item of ExpiryCategorydata) {
//                if (item.Name.toLowerCase().trim() == namedp) {
//                    toastr.error("Kindly check Duplicate " + msg + " Description");
//                    $("#dpname").focus();
//                    return;
//                }
//            }
//        } else {
//            for (item of ExpiryCategorydata) {
//                if (item.Name?.toLowerCase().trim() == namedp && item.ExpiryCategoryCode != ExpiryCategoryCodeIntial) {
//                    toastr.error("Kindly check Duplicate  " + msg + " Description is entered on editing");
//                    $("#dpname").focus();
//                    return;
//                }
//            }
//        }
//        if ($("#dpname").val() === "" || $("#dpname").val() == null) {
//            toastr.error("Please provide " + msg + " Name");
//            $("#dpname").focus();
//            return;
//        }
//        // if (user.SectorNumber == "20") {
//        if ($("#dpShelfLifeUOM").val() == "" || $("#dpShelfLifeUOM").val() == null) {
//            toastr.error("Please provide " + msg + " Shelf Life UOM");
//            $("#dpShelfLifeUOM").focus();
//            return;
//        }
//        // }
//        if (dpvalidate() === true) {
//            $("#dpname").removeClass("is-invalid");

//            var model = {
//                "ID": $("#dpid").val(),
//                "Name": $("#dpname").val(),
//                "ExpiryCategoryCode": ExpiryCategoryCodeIntial,
//                "ShelfLifeUOMCode": $("#dpShelfLifeUOM").val(),
//                "ShelfLife": $("#dpshelflife").val(),
//            }
//            if (ExpiryCategoryCodeIntial == null) {
//                model.IsActive = 1;
//            } else {
//                model.IsActive = 1;
//                model.CreatedBy = datamodel.CreatedBy;
//                model.CreatedOn = datamodel.CreatedOn;

//            }
//            model.ShelfLifeUOMCode = $("#dpShelfLifeUOM").data('kendoDropDownList').value()

//            $("#secredpsubmit").attr('disabled', 'disabled');
//            HttpClient.MakeRequest(CookBookMasters.SaveExpiryCategory, function (result) {
//                if (result == false) {
//                    $('#secredpsubmit').removeAttr("disabled");
//                    toastr.success("There was some error, the record cannot be saved");

//                }
//                else {
//                    $(".k-overlay").hide();
//                    $('#secredpsubmit').removeAttr("disabled");

//                    if (model.ID > 0)
//                        toastr.success(msg + " record updated successfully");
//                    else
//                        toastr.success("New" + msg + " record added successfully");
//                    $(".k-overlay").hide();
//                    var orderWindow = $("#ReaswindowEdit").data("kendoWindow");
//                    orderWindow.close();
//                    $("#gridExpiryCategory").data("kendoGrid").dataSource.data([]);
//                    $("#gridExpiryCategory").data("kendoGrid").dataSource.read();
//                    $("#gridExpiryCategory").data("kendoGrid").dataSource.sort({ field: "ExpiryCategoryCode", dir: "asc" });



//                }
//            }, {
//                model: model

//            }, true);
//        }
//    });


//});
