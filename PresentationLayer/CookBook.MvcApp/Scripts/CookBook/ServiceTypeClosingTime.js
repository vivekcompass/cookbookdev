﻿var sitemasterdata = [];
var savedatalist = [];
var siteCode;
var isClosingTimeChange = false;
kendo.data.DataSource.prototype.dataFiltered = function () {
    // Gets the filter from the dataSource
    var filters = this.filter();

    // Gets the full set of data from the data source
    var allData = this.data();

    // Applies the filter to the data
    var query = new kendo.data.Query(allData);

    // Returns the filtered data
    return query.filter(filters).data;
}
HttpClient.MakeRequest(CookBookMasters.GetSiteDataListByUserId, function (data) {
  
    var dataSource = data;
    var dataSource = data;
    sitemasterdata = dataSource;
    sitemasterdata.forEach(function (item) {

        item.SiteName = item.SiteName + " - " + item.SiteCode;
        return item;
    })
    sitemasterdata = sitemasterdata.filter(item => item.IsActive != false);
    if (data.length > 1) {
        sitemasterdata.unshift({ "SiteCode": 0, "SiteName": "Select" });
        $("#btnGo").css("display", "inline-block");
    }
    else {
        $("#btnGo").css("display", "inline-none");


    }
    populateSiteMasterDropdown();
  
}, null, false);

function populateSiteMasterDropdown() {
    $("#inputsite").kendoDropDownList({
        filter: "contains",
        dataTextField: "SiteName",
        dataValueField: "SiteCode",
        dataSource: sitemasterdata,
        index: 0,
    });
    if (sitemasterdata.length == 1) {
        $("#btnGo").click();
     //   var dropdownlist = $("#inputsite").data("kendoDropDownList");
       // dropdownlist.wrapper.hide();
    }

}
function populateItemGrid() {

    Utility.Loading();
    var gridVariable = $("#gridItem");
    gridVariable.html("");
    grid = gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,
        editable: true,
        columns: [


            {
                field: "DayPart", title: "Service Type", width: "300px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: left"
                }
            },
            {
                field: "ClosingTime",
                title: "<left>Closing Time</left>",
                width: "150px",
                type: "number",
                template: "#= kendo.toString(new Date(ClosingTime), 'HH:mm') #",
                editor: dateTimeEditor2,
                attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: left"
                }
            },
            {
                field: "CreatedByName", title: "Last Modified By", width: "250px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ModifiedOnString", title: "Last Modified On", width: "150",
                attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ClosingStatusLevel", title: "Status", width: "100px", attributes: {
                    class: "mycustomstatus",
                    style: "text-align: center; font-weight:normal"
                },headerAttributes: {
               
                style: "text-align: center; font-weight:normal"
            },
            },


        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteDayPartClossingMappingDataList, function (result) {
                        Utility.UnLoading();

                        if (result != null) {
                            result.forEach(m => {
                                if (m.ClosingTime == null || m.ClosingTime == '') {
                                    var d = new Date();
                                    d.setHours(0, 0, 0, 0);
                                    m.ClosingTime = d;
                                }
                            });
                            options.success(result);
                            if (result.length > 0) {
                               // savedatalist = result;//filter
                                $("#myInput").show();
                                $("#btnSaveAll").show();
                                $("#btnPricePublish").show();
                                
                                savedatalist = [];

                                for (x of result) {
                                    var model = {};
                                    if (x.ClosingStatus != 3) {
                                        model.SiteCode = siteCode;
                                        model.ClosingTime = x.ClosingTime;
                                        model.DayPart = x.DayPart;
                                        model.DayPartCode = x.DayPartCode;
                                        model.ClosingStatus = x.ClosingStatus;
                                        if (model.ClosingStatus == null)
                                            model.ClosingStatus = 0;
                                        savedatalist.push(model);
                                    }
                                   
                                   
                                }
                               
                            } else {
                                $("#myInput").hide();
                                $("#btnSaveAll").hide();
                                $("#btnPricePublish").hide();
                            }
                           

                        }
                        else {
                            options.success("");
                        }
                    }, {

                        SiteCode: $("#inputsite").val()
                    }
                        , true);
                }
            },

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        DayPart: { type: "string", editable: false },
                        DayPartCode: { type: "string" },
                        SiteCode: { type: "string" },
                        ModifiedBy: { type: "string", editable: false },
                        CreatedByName: { type: "string", editable: false },
                        ModifiedOnString: { type: "string", editable: false  },
                        UserName: { type: "string", editable: false  },
                        ModifiedOn: { type: "string", editable: false  },
                        ClosingTime: { type: "date", editable:true },
                        ClosingStatus: { type: "string" },
                        ClosingStatusLevel: { type: "string" }
                        
                    }

                }
            },
            pageSize: 150
        },
        columnResize: function (e) {
            //var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        //  height: 440,
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var grid = e.sender;
            var items = e.sender.items();

            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (view[i].ClosingStatus == 0 || view[i].ClosingStatus == null || view[i].ClosingStatus == 1) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffe1e1");
                   

                }
                else if (view[i].ClosingStatus == 2) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffffbf");
                  
                }
                else {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#d9ffb3");
                   
                }

            }

            },
        change: function (e) {
            
        },
        // filter: { field: "ItemCode", operator: "gt", value: "69999" }
    });
    
    Utility.UnLoading();
   
}
var lastValid = 0;
var validNumber = new RegExp(/^\d*\.?\d*$/);
function validateNumber(elem) {

    if (validNumber.test(elem.value)) {
        lastValid = elem.value;
    } else {
        elem.value = lastValid;
    }
}

function dateTimeEditor2(container, options) {
    debugger;
    $('<input type="text" />')
        .appendTo(container)
        .kendoTimePicker({
            format: "HH:mm",
            value: kendo.toString(new Date(options.model.ClosingTime), 'HH:mm'),
            change: function (e) {
                debugger;

                var value = this.value();
                var element = e.sender.element;
                var row = element.closest("tr");
                var grid = $("#gridItem").data("kendoGrid");
                var dataItem = grid.dataItem(row);
                if (dataItem.ClosingTime == value) {
                    return;
                }
                dataItem.ClosingTime = value;
                var editItemModelId = dataItem.DayPartCode;
                var grid = $("#grid").data("kendoGrid");
                dataItem.set("ClosingTime", this.value());

                var model = {};
                model.ClosingTime = dataItem.ClosingTime;
                model.SiteCode = siteCode;
                model.DayPartCode = dataItem.DayPartCode;
                model.ClosingStatus = dataItem.ClosingStatus;
                model.IsChangedByUser = true;
                isClosingTimeChange = true;
                for (x of savedatalist) {
                    if (x.DayPartCode == model.DayPartCode) {
                        x.ClosingTime = model.ClosingTime;
                        x.IsChangedByUser = model.IsChangedByUser;
                        return;
                    }
                }
                savedatalist.push(model);

            }
        });
}





$(document).ready(function () {
    $("#windowEditMOGWiseAPL").kendoWindow({
        modal: true,
        width: "600px",
        height: "300px",
        cssClass: 'aplPopup',
        top: '80px !important',
        title: "Item Price Detials - ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $('#myInput').on('input',
        function (e) {
            var grid = $('#gridItem').data('kendoGrid');
            var columns = grid.columns;

            var filter = { logic: 'or', filters: [] };
            columns.forEach(function (x) {
                if (x.field) {
                    if (x.field == "ModifiedOn" || x.field== "ClosingStatus" || x.field=="ClosingTime"
                         || x.field == "UserName" || x.field == "DayPart" ) {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;

                        if (type == 'string') {
                           
                            filter.filters.push({
                                field: x.field,
                                operator: 'contains',
                                value: e.target.value
                            })
                        }
                        else if (type == 'number') {

                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        } else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format('dd-MMM-yy', data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                }
            });
            grid.dataSource.filter(filter);
        }
    );
    $("#btnGo").click(function () {
        isClosingTimeChange = false;
        siteCode = $("#inputsite").val();
        populateItemGrid();
    })

    $("#btnSaveAll").click(function () {
        if (savedatalist.length == 0 || !isClosingTimeChange) {
            toastr.error("Kindly, update at least one Closing Time")

            return;
        }
        savedatalist.forEach(function (item) {
            if (item.ClosingStatus == null || item.ClosingStatus == 0 || item.ClosingStatus == 1) {
                item.ClosingStatus = 1;
            }
            else {
                item.ClosingStatus = 2;
            }
            if (item.IsChangedByUser) {
                item.ClosingStatus = 2;
            };
        })
        Utility.Page_Alert_Save("Closing Time configuration saved. Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                HttpClient.MakeSyncRequest(CookBookMasters.SaveSiteDayPartMappingDataList, function (result) {
                    if (result == false) {
                        toastr.error("Some error occured, please try again.");
                    }
                    else {
                            $(".k-overlay").hide();

                        var msg = "Closing Time changes updated. Do not forget to Publish the same";
                            Toast.fire({
                                type: 'success',
                                title: msg
                            });
                        savedatalist = [];
                        isClosingTimeChange = false;
                        populateItemGrid();
                    }
                }, {
                        model: savedatalist, isClosingTime:true
                }, true);

                Utility.UnLoading();
            },
            function () {
                Utility.UnLoading();
            }
        );
    })

    $("#btnPricePublish").click(function () {
        if (savedatalist.length == 0) {
            toastr.error("Kindly, update at least one Item Price")

            return;
        }
        savedatalist.forEach(function (item) {
            item.ClosingStatus = 3;
        })
        Utility.Page_Alert_Save("Closing Time configuration has been Published. Continue?", "Reset Confirmation", "Yes", "No",
            function () {
                HttpClient.MakeSyncRequest(CookBookMasters.SaveSiteDayPartMappingDataList, function (result) {
                    if (result == false) {
                        toastr.error("Some error occured, please try again.");
                    }
                    else {
                        $(".k-overlay").hide();
                        var msg = "Closing Time changes have been Publised";
                        toastr.success(msg);
                        savedatalist = [];
                        populateItemGrid();
                    }
                }, {
                        model: savedatalist,
                         isClosingTime: true
                }, true);

                Utility.UnLoading();
            },
            function () {
                Utility.UnLoading();
            }
        );
    })

});

function showDetails(iCode,iName) {
  
    var dialog = $("#windowEditMOGWiseAPL").data("kendoWindow");
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
    dialog.title("Item Price History Details of Item Name "+iName+" ("+iCode.trim()+")");
    populateAPLMasterGrid(iCode.trim());
}

function populateAPLMasterGrid(iCode) {
    Utility.Loading();


    var gridVariable = $("#gridMOGWiseAPL");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "APLMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        scrollable: true,
        noRecords: {
            template: "No Records Available"
        },

        columns: [
           
            {
                field: "ItemPrice", title: "Price", width: "50px", format: Utility.Cost_Format, attributes: {

                    style: "text-align: left; font-weight:normal;"
                },
            },
            {
                field: "ModifiedOn", title: "Modified On", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal;"
                },
            },
            {
                field: "UserName", title: "Modified By", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            }
           
        ],
        dataSource: {
            sort: { field: "ModifiedOn", dir: "desc" },
            transport: {
                read: function (options) {
                    HttpClient.MakeSyncRequest(CookBookMasters.GetSiteItemPriceHistory, function (result) {


                        if (result != null) {
                            options.success(result);
                            Utility.UnLoading();

                        }
                        else {
                            options.success("");
                        }
                    }, {

                        siteCode: $("#inputsite").val(),
                        itemCode: iCode
                    }
                        , true);
                  
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Name: { type: "string" },
                        ItemCode: { type: "string" },
                        ItemPrice: { type: "decimal" },
                         SiteCode: { type: "string" },
                      
                    }
                }
            },
            // pageSize: 100,
        },

        //maxHeight: 250,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {


        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })
    //$(".k-label")[0].innerHTML.replace("items", "records");
}