﻿var user;
var conceptype2data;
var ImageIcon = null;
var item = null;
var daypartdata = [];
var allergenCode;
var status = "";
var varname = "";
var datamodel;
var Name = "";
var sitedata = [];
var dkdata = [];
$(function () {




    $('#myInput').on('input', function (e) {
        var grid = $('#gridAllergen').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "AllergenCode" || x.field == "Name" || x.field == "Description" || x.field == "ConceptType2Name") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        if (user.SectorNumber != "20") {
            $("#mtype").text("Mealtime");
        }
        $("#allergenWindowEdit").kendoWindow({
            modal: true,
            width: "600px",
            //height: "350px",
            title: "Allergen Details  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });

        $("#btnExport").click(function (e) {
            var grid = $("#gridAllergen").data("kendoGrid");
            grid.saveAsExcel();
        });
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;

            populateAllergenGrid();
        }, null, false);

    });

    //Allergen Section Start

    function populateAllergenGrid() {

        Utility.Loading();
        var gridVariable = $("#gridAllergen");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [

                {
                    field: "", title: "Image", width: "35px", attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    template: "<div class='item-photo'" +
                        "style='background-image: url(./AllergenImages/#:data.ImageIcon#);'></div>", width: "35px"
                },
                {
                    field: "AllergenCode", title: "Code", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Name", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: left;"
                    },
                   // template: '# if (Top8 == "true") {#<div>#:data.Name#</div>#} else {#<div></div>#}#',
                },
                //{
                //    field: "Name", title: "Other Allergens", width: "80px", attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: left;"
                //    },
                //    template: '# if (Top8 == "true") {#<div></div>#} else {#<div>#:data.Name#</div>#}#',
                //},
                //{
                //    field: "Name", title: "Other Allergens", width: "80px", attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: left;"
                //    },
                //    template: '# if (Top8 == "true") {#<div></div>#} else {#<div>#:data.Name#</div>#}#',
                //},
                {
                    field: "Top8", title: "Top 8", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: left;"
                    },
                    template: '# if (Top8 == "true") {#<div>Yes</div>#} else {#<div>No</div>#}#',
                },
                {
                    field: "AllergenInfo", title: "Allergen Information", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive",
                //    title: "Status", width: "40px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal;"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {

                                var tr = $(e.target).closest("tr");    // get the current table row (tr)

                                item = this.dataItem(tr);          // get the date of this row

                                if (!item.IsActive) {
                                    return;
                                }
                                var model = item;
                                Name = item.Name;
                                AllergenCodeInitial = item.AllergenCode;
                                ImageIcon = item.ImageIcon;
                                var dialog = $("#allergenWindowEdit").data("kendoWindow");
                                allergenCode = item.AllergenCode;
                                dialog.open().element.closest(".k-window").css({
                                    top: 167,
                                });
                                dialog.center();
                                $("#allergenid").val(model.ID);
                                $("#allergencode").val(model.AllergenCode);
                                $("#allergenname").val(model.Name);
                                $("#allergeninfo").val(model.AllergenInfo);
                                console.log(model.Top8)
                                if (model.Top8 == "true") {
                                    console.log(model.Top8)
                                    $("#top8").attr("checked", "checked");
                                }
                                else {
                                    $("#top8").removeAttr("checked");
                                }
                                $("#file").val("");
                                $("#image").attr('src', './AllergenImages/' + model.ImageIcon);
                                $("#allergenname").focus();
                                dialog.title("Details - " + Name);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetAllergenDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                console.log(result);
                                options.success(result);
                                // allergenCode = result.sort((a, b) => (a.AllergenCode > b.AllergenCode) ? 1 : -1)[result.length - 1].AllergenCode;

                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { type: "string" },
                            IsActive: { editable: false },
                            Top8: { type: "string" },
                            AllergenInfo: { type: "string" },
                            AllergenCode: { type: "string" },
                            ImageIcon: { type: "string" }

                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
                
                $(".chkbox").on("change", function () {
                    var gridObj = $("#gridAllergen").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    var trEdit = $(this).closest("tr");
                    var th = this;
                    datamodel = tr;
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Allergen " + active + "?", " Allergen Update Confirmation", "Yes", "No", function () {
                        HttpClient.MakeRequest(CookBookMasters.SaveAllergen, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                toastr.success("Allergen configuration updated successfully");
                                if ($(th)[0].checked) {
                                    $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                } else {
                                    $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                }
                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {

                        $(th)[0].checked = !datamodel.IsActive;
                        if ($(th)[0].checked) {
                            $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                        } else {
                            $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                        }
                    });
                    return true;
                });

            },
            change: function (e) {
            },
        })

        //  var gridVariable = $("#gridAllergen").data("kendoGrid");
        //sort Grid's dataSource
        gridVariable.data("kendoGrid").dataSource.sort({ field: "AllergenCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }


    $("#allergencancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#allergenWindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#allergenaddnew").on("click", function () {
        var model;
        var dialog = $("#allergenWindowEdit").data("kendoWindow");

        dialog.open().element.closest(".k-window").css({
            top: 167,

        });
        dialog.center();

        datamodel = model;

        dialog.title("New Allergen Creation");
        allergenCode = null;
        $('#allergensubmit').removeAttr("disabled");
        $("#allergenid").val("");
        $("#allergencode").val("");
        $("#allergenname").val("");
        $("#allergeninfo").val("");
        $("#top8").prop('checked', false);
        $("#file").val("");
        $("#image").attr('src', '../AllergenImages/default.png');
        $("#allergenname").removeClass("is-invalid")
        $("#allergenname").focus();
        item = null;
        AllergenCodeInitial = null;
    });

    function allergenValidate() {
        var valid = true;

        if ($("#allergenname").val() === "") {
            toastr.error("Please provide input");
            $("#allergenname").addClass("is-invalid");
            valid = false;
            $("#allergenname").focus();
        }
        else if (($.trim($("#allergenname").val())).length > 30) {
            toastr.error("Please provide input not exceeding 30 charcters");
            $("#allergenname").addClass("is-invalid");
            valid = false;
            $("#allergenname").focus();
        }
       
        return valid;
    }

    $('#btnUpload').click(function () {

        $('#txtFilePath').removeClass('validationError');
        $('#file').val('').trigger('click');
    });

    function validateImportFile(filename) {

        if (filename != '') {
            var fileExtension = ['png', 'jpg', 'jpeg'];
            if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == -1) {
                Utility.Page_Alert(ExtensionAllowedMessage);
                return false;
            }
        }
        return true;
    };


    $('#file').change(function () {

        var that = this;
        if (validateImportFile(that.value)) {
            //$('#txtFilePath').val(that.value);
            //  $('#txtFilePath').val(that.files[0].name);
            if (that.files[0]) {
                var uploadimg = new FileReader();
                uploadimg.onload = function (displayimg) {
                    $("#image").attr('src', displayimg.target.result);
                }
                uploadimg.readAsDataURL(that.files[0]);
            }
        }
        else {
            //ImportReport.refreshPage();
        }
    });

    $("#allergensubmit").click(function () {
        var editFlag = 0;
        if ($("#allergenname").val() === "") {
            toastr.error("Please provide Allergen Name");
            $("#allergenname").focus();
            return;
        }
        var allergencode = "";
        if (allergenValidate() === true) {
            $("#allergenname").removeClass("is-invalid");
            var model = item;
            if (model == null) {
                model = {
                    "ID": $("#allergenid").val(),
                    "Name": $("#allergenname").val(),
                    "IsActive": 1,
                    "AllergenCode": AllergenCodeInitial,
                    "AllergenInfo": $("#allergeninfo").val(),
                    "Top8": $("#top8").val(),
                    "ImageIcon": ImageIcon,
                }
                editFlag = 0;
            } else {
                editFlag = 1;
                model.Name = $("#allergenname").val();
                model.AllergenCode = AllergenCodeInitial;
                model.AllergenInfo = $("#allergeninfo").val();
                model.Top8 = $('#top8').is(':checked');
                model.ImageIcon = ImageIcon;
            }
            allergenCode = model.AllergenCode;
            var file = document.getElementById("file").files[0];
            if (file != undefined) {
                var fileName = document.getElementById("file").value;
                var idxDot = fileName.lastIndexOf(".") + 1;
                var extn = fileName.substr(idxDot, fileName.length).toLowerCase();
                //var extn = file.type.split("/")[1];
            }
            if (document.getElementById("file").files.length > 0 && model.ImageIcon != null) {
                model.ImageIcon = model.Name.replace(/[^\w]/gi, '').substring(0, 11) + '.' + extn;
            } else if (document.getElementById("file").files.length > 0 && model.ImageIcon == null) {
                model.ImageIcon = model.Name.replace(/[^\w]/gi, '').substring(0, 11) + '.' + extn;//replace(/\s/g, '')
            }
            if (document.getElementById("file").files.length == 0 && ImageIcon == null) {
                model.ImageIcon = "default.png";
            }
            if (model.AllergenCode != null) {
                allergencode = model.AllergenCode;

            }

            else
                allergencode = model.Name.replace(/[^\w]/gi, '').substring(0, 11);
            $("#fpsubmit").attr('disabled', 'disabled');

            if (document.getElementById("file").files.length > 0) {
                var formData = new FormData();
                model.ImageIcon = allergencode + '.' + extn;
                formData.append('AllergenCode', allergencode);
                formData.append('folderName', "AllergenImages");
                formData.append("file", file);
                $.ajax({
                    type: "POST",
                    //url: './Item/UploadFile',
                    url: './Item/UploadFile',
                    data: formData,
                    contentType: false,
                    processData: false,
                    async: false,
                    success: function (response) {

                    },
                    error: function (error) {
                        Utility.UnLoading();
                        Utility.Page_Alert("Problem while uploading the Allergen Image on Server. Please contact Administrator", "Success", "OK", function () {
                        });
                    }
                });
            }
            console.log(model);
            if (!sanitizeAndSend(model)) {
                return;
            }
            HttpClient.MakeSyncRequest(CookBookMasters.SaveAllergen, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#allergensubmit').removeAttr("disabled");
                    $("#allergenname").focus();
                    Utility.UnLoading();
                    return;
                }
                else {
                    if (result == false) {
                        $('#allergensubmit').removeAttr("disabled");
                        $("#allergenname").focus();
                        toastr.error(result.message);
                        Utility.UnLoading();
                        return;
                    }
                    else {

                        if (result == "Duplicate") {
                            $('#allergensubmit').removeAttr("disabled");
                            toastr.error("Duplicate Name, Kindly change");
                            $("#allergenname").focus();
                            Utility.UnLoading();
                            return;
                        } else {
                            $(".k-overlay").hide();
                            var orderWindow = $("#allergenWindowEdit").data("kendoWindow");
                            orderWindow.close();
                            $('#allergensubmit').removeAttr("disabled");
                            if (model.AllergenCode != null)
                                toastr.success("Allergen configuration updated successfully");
                            else
                                toastr.success("New Allergen added successfully");

                            $("#gridAllergen").data("kendoGrid").dataSource.data([]);
                            $("#gridAllergen").data("kendoGrid").dataSource.read();
                            $("#gridAllergen").data("kendoGrid").dataSource.sort({ field: "AllergenCode", dir: "asc" });
                        }
                    }
                }
            }, {
                model: model
            }, true);
        }
    });


});