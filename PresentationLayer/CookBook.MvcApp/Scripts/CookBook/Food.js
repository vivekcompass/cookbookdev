﻿var recipedata = [];
var recipeCustomData = [];
var mappedData = [];
var mappedRecipeCode = [];
var DishCode;
var Dish_ID;
var multiarray = [];
var checkedRecipeCode = '';
var siteCode = '';
var saveModel = [];

var sitemasterdata = [];
function customData(DishCode) {
    mappedRecipeCode = [];
    HttpClient.MakeSyncRequest(CookBookMasters.GetDishRecipeMappingDataList, function (data) {

        mappedData = data;
        for (var d of mappedData) {
            mappedRecipeCode.push(d.RecipeCode);
        }
        
    }, { DishCode: DishCode  }, false);
    HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {
        recipeCustomData = data;
        recipeCustomData = recipeCustomData.filter(function (item) {
            for (var d of mappedData) {
                if (d.RecipeCode == item.RecipeCode) {
                    item.IsDefault = d.IsDefault;
                    item.DishCode = d.DishCode;
                    
                    multiarray.push(item.RecipeCode);
                     return item;
                }
            }   
        })
    }, { condition: "All" }, false);

   
}

    
            
$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var DishName = "";
    var foodPanTypedata = [];
    //var dishsubcategorydata = [];
    var dietcategorydata = [];
    var dishtypedata = [];
    var uomdata = [];
    var servingtemperaturedata = [];

    $("#windowEditDish").kendoWindow({
        modal: true,
        width: "500px",
        height: "185px",
        title: "Dish Details - " + DishName,
        actions: ["Close"],
        visible: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }


    $(document).ready(function () {
        $("#btnExport").click(function (e) {
            var grid = $("#gridDish").data("kendoGrid");
            grid.saveAsExcel();
        });

        HttpClient.MakeRequest(CookBookMasters.GetSiteMasterList, function (data) {
            console.log("Site Master Data");
            console.log(data);
            var dataSource = data;
            sitemasterdata = dataSource;
            sitemasterdata.unshift({ "SiteCode": 0, "SiteName": "Select Site" });
            populateSiteMasterDropdown();
        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNewDish").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNewDish").css("display", "none");
            //}

        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetDietCategoryDataList, function (data) {

            var dataSource = data;
            dietcategorydata = [];
            dietcategorydata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                dietcategorydata.push({ "value": dataSource[i].DietCategoryCode, "text": dataSource[i].Name });
            }
            populateDietCategoryDropdown_Bulk();
            populateDietCategoryDropdown();
        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetFoodPanDataList, function (data) {

            var dataSource = data;
            foodPanTypedata = [];
            foodPanTypedata.push({ "value": "Select", "text": "Select Food Pan Type" });
            for (var i = 0; i < dataSource.length; i++) {
                foodPanTypedata.push({ "value": dataSource[i].FoodPanCode, "text": dataSource[i].Name, "FoodPanTypeID": dataSource[i].FoodPanCode });
            }
            populateFoodPanTypeDropdown();
            populateFoodPanTypeDropdown_Bulk();
        }, null, false);


        HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

            var dataSource = data;
            uomdata = [];
            uomdata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                uomdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].Name });
            }
            populateUOMDropdown();
            populateUOMDropdown_Bulk();
        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetDishTypeDataList, function (data) {

            var dataSource = data;
            dishtypedata = [];
            dishtypedata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                dishtypedata.push({ "value": dataSource[i].DishTypeCode, "text": dataSource[i].Name });
            }
            populateDishTypeDropdown();
            populateDishTypeDropdown_Bulk();
        }, null, false);

        HttpClient.MakeRequest(CookBookMasters.GetServingTemperatureDataList, function (data) {

            var dataSource = data;
            servingtemperaturedata = [];
            servingtemperaturedata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                servingtemperaturedata.push({ "value": dataSource[i].ServingTemparatureCode, "text": dataSource[i].Name });
            }
            populateServingTemperatureDropdown();
        }, null, false);

        $("#gridBulkChange").css("display", "none");
        populateBulkChangeControls();
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        changeControls.css("background-color", "#fff");
        changeControls.css("border", "none");
        changeControls.eq(4).text("");
        changeControls.eq(4).append("<div id='foodpan' style='width:100%; font-size:10.5px!important'></div>");
        changeControls.eq(5).text("");
        changeControls.eq(5).append("<input id='capacity' class='form-control' style='width:90%; margin-left:3px; padding-right: 5px!important;font-size: 12px!important;'></div>");
    });

   
    function populateSiteMasterDropdown() {
        $("#ddlSiteMaster").kendoDropDownList({
            filter: "contains",
            dataTextField: "SiteName",
            dataValueField: "SiteCode",
            dataSource: sitemasterdata,
            index: 0,
        });
        $("#inputsite").css("display", "block")
        $("#btnGo").css("display", "inline-block")
    }

    $("#InitiateBulkChanges").on("click", function () {
        //populateBulkChangeControls();
        //var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        //changeControls.css("background-color", "#fff");
        //changeControls.css("border", "none");
        $("#gridBulkChange").css("display", "block");
        $("#gridBulkChange").children(".k-grid-header").css("border", "none");
        $("#InitiateBulkChanges").css("display", "none");
        $("#CancelBulkChanges").css("display", "inline");
        $("#SaveBulkChanges").css("display", "inline");
        //populateDietCategoryDropdown_Bulk();
        //populateDishCategoryDropdown_Bulk();
        //populateDishSubCategoryDropdown_Bulk();
        //populateUOMDropdown_Bulk();
        //populateDishTypeDropdown_Bulk();

    });

    function populateDietCategoryDropdown_Bulk() {
        $("#dietcategory").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: dietcategorydata,
            index: 0,
        });
    }

    function populateFoodPanTypeDropdown_Bulk() {
        $("#foodpan").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: foodPanTypedata,
            index: 0,
        });
    }
    //function populateDishSubCategoryDropdown_Bulk() {
    //    $("#dishsubcategory").kendoDropDownList({
    //        filter: "contains",
    //        dataTextField: "text",
    //        dataValueField: "value",
    //        dataSource: dishsubcategorydata,
    //        index: 0,
    //    });
    //}
    function populateUOMDropdown_Bulk() {
        $("#uom").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: uomdata,
            index: 0,
        });
    }
    function populateDishTypeDropdown_Bulk() {
        $("#dishtype").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: dishtypedata,
            index: 0,
        });
    }

    function populateDietCategoryDropdown() {
        $("#inputdietcategory").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: dietcategorydata,
            index: 0,
        });
    }

    function populateFoodPanTypeDropdown() {
        $("#inputFoodPanType").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: foodPanTypedata,
            index: 0,
        });
    }

    function populateDishSubCategoryDropdown() {
        $("#inputdishsubcategory").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: dishsubcategorydata,
            index: 0,
        });
    }

    function populateUOMDropdown() {
        $("#inputuom").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: uomdata,
            index: 0,
        });
    }

    function populateServingTemperatureDropdown() {
        $("#inputservingtemperature").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: servingtemperaturedata,
            index: 0,
        });
    }
    function populateDishTypeDropdown() {
        $("#inputdishtype").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: dishtypedata,
            index: 0,
        });
    }

    //$("#inputcpu").data("kendoDropDownList").select(function (dataItem) {
    //    return dataItem.value === dataSource[0].value;
    //});
    //$("#inputcpu").data("kendoDropDownList").select(0);

    $("#CancelBulkChanges").on("click", function () {
        $("#InitiateBulkChanges").css("display", "inline");
        $("#CancelBulkChanges").css("display", "none");
        $("#SaveBulkChanges").css("display", "none");
        $("#gridBulkChange").css("display", "none");
        populateDishGrid();
    });

    

    $("#btnGo").on("click", function () {
        var dropdownlist = $("#ddlSiteMaster").data("kendoDropDownList");
        siteCode = dropdownlist.value();
        populateDishGrid();
        $("#fpbody").css("display", "block");
        $("#gridDish").css("display", "block");
       // $("#gridBulkChange").css("display", "block");
        $("#bulkChangeToolBar").css("display", "block");
        $("#gridLabel").css("display", "block");
    });

    $("#SaveBulkChanges").on("click", function () {
        
        var neVal = $(this).val();
        var dataFiltered = $("#gridDish").data("kendoGrid").dataSource.dataFiltered();
        
        
        var model = dataFiltered;
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        var validChange = false;
        var foodpan = "";
        var capacity = "";

        var foodpanspan = $("#foodpan");
        var capacityspan = $("#capacity");
        
        if (capacityspan.val() != "") {
            validChange = true;
            capacity = capacityspan.val();
        }
        if (foodpanspan.val() != "Select") {
            validChange = true;
            foodpan = foodpanspan.val();
        }
        
        if (validChange == false) {
            toastr.error("Please change at least one attribute for Bulk Update");
        } else {
            Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                function () {
                    $.each(model, function () {
                        this.SiteCode = siteCode
                        if (foodpan != "")
                            this.FoodPanCode = foodpan;
                        if (capacity != "")
                            this.Capacity = capacity;
                        this.CapacityUOMCode = this.UOMCode;
                    });
                    
                    HttpClient.MakeRequest(CookBookMasters.SaveSiteDishFoodPanDataList, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again.");
                        }
                        else {
                            $(".k-overlay").hide();
                            toastr.success("Records have been updated successfully");
                            $("#gridDish").data("kendoGrid").dataSource.data([]);
                            $("#gridDish").data("kendoGrid").dataSource.read();
                            $("#InitiateBulkChanges").css("display", "inline");
                            $("#CancelBulkChanges").css("display", "none");
                            $("#SaveBulkChanges").css("display", "none");
                            $("#gridBulkChange").css("display", "none");
                        }
                    }, {
                        model: model

                    }, false);

                    //populateSiteGrid();
                },
                function () {
                    foodpanspan.focus();
                }
            );
        }
    });
    //Food Program Section Start

    function populateDishGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridDish");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "Dish.xlsx",
                filterable: true,
                allPages: true
            },
            //selectable: "cell",
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: false,
            height: 480,
            minHeight:200,
            //reorderable: true,
            //scrollable: true,
            columns: [
                
                {
                    field: "DishCode", title: "Dish Code", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "DishName", title: "Dish Name", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "DishCategory", title: "Dish Category", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOM", title: "UOM", width: "25px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    },
                },
                {
                    field: "FoodPanName", title: "Food Pan", width: "60px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    },
                },
                {
                    field: "Capacity", title: "Capacity (UOM)", width: "35px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },
                {
                    field: "Edit", title: "Action", width: "30px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = $("#gridDish").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                               
                                var dialog = $("#windowEditDish").data("kendoWindow");
                                $("#windowEditDish").kendoWindow({
                                    animation: false
                                });
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open();
                                dialog.center();

                                $("#foodpanid").val(tr.ID);
                                $("#inputdishcode").text(tr.DishCode);
                                $("#inputdishname").text(tr.DishName);
                                $("#lbluom").text(tr.UOM);
                                $("#inputFoodPanCapacity").val(tr.Capacity);
                                $("#foodPanCapcitylbl").text('Capacity (' + tr.UOM + ')');
                                
                                DishName = tr.DishName;
                                DishCode = tr.DishCode;
                                $("#inputFoodPanType").data('kendoDropDownList').value(tr.FoodPanCode);
                               
                                //if (user.UserRoleId === 1) {
                                //    $("#inputdishname").removeAttr('disabled');
                                //   ;
                                //    $("#inputFoodPanType").removeAttr('disabled');
                                   
                                //    $("#btnSubmit").css('display', 'inline');
                                //    $("#btnCancel").css('display', 'inline');
                                //} else {
                                //    $("#inputdishname").attr('disabled', 'disabled');
                                   
                                //    $("#inputFoodPanType").attr('disabled', 'disabled');
                                   
                                //    $("#btnSubmit").css('display', 'none');
                                //    $("#btnCancel").css('display', 'none');
                                //}

                                dialog.title("Dish Details - " + DishName);
                                return true;
                            }
                        }
                    ],
                }
            ],
            pageable: {
                pageSize: 15
            },
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetDishDataListForFood, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, 
                            {
                                siteCode: siteCode
                        }
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                //items.each(function (e) {
                    
                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //    }
                //});
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })

    }

    function populateBulkChangeControls() {
        
        Utility.Loading();
        var gridVariable = $("#gridBulkChange");
        gridVariable.html("");
        gridVariable.kendoGrid({
            //selectable: "cell",
            sortable: false,
            filterable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "", title: "", width: "30px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "", title: "", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "", title: "", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "", title: "", width: "25px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "60px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "35px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {

                        style: "text-align: center"
                    },
                },
            ],
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
            },
            change: function (e) {
            },
        });
    }

    

    $("#btnCancelDish").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    
    $("#btnSubmitDish").click(function () {
        var dropdownlist = $("#inputFoodPanType").data("kendoDropDownList");
        var foodPanType = dropdownlist.value();
        if (foodPanType === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputFoodPanType").focus();
        }
        if ($("#inputFoodPanCapacity").val() === "") {
            toastr.error("Please provide valid input(s)");
            $("#inputFoodPanCapacity").focus();
        }
        else {
            var model;
            if (datamodel != null) {
                model = datamodel;
                model.SiteCode = siteCode;
                model.FoodPanCode = foodPanType;
                model.Capacity = $("#inputFoodPanCapacity").val();
                model.CapacityUOMCode = datamodel.UOMCode;
                model.IsActive = true;
            }
            else {
                model = {
                    "ID": $("#foodpanid").val(),
                    "SiteCode": siteCode,
                    "DishCode": datamodel.DishCode,
                    "FoodPanCode": foodPanType,
                    "Capacity": $("#inputFoodPanCapacity").val(),
                    "CapacityUOMCode": datamodel.UOMCode,
                    "IsActive" : true,
                }
            }

            $("#btnSubmitDish").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveSiteDishFoodPanData, function (result) {
                if (result == false) {
                    $('#btnSubmitDish').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputFoodPanType").focus();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#windowEditDish").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmitDish').removeAttr("disabled");
                    if (model.ID > 0)
                        toastr.success("Dish mapping updated ");
                    else
                        toastr.success("Food Pan mapped to the Dish ");
                    $("#gridDish").data("kendoGrid").dataSource.data([]);
                    $("#gridDish").data("kendoGrid").dataSource.read();
                    //});
                }
            }, {
                model: model

            }, false);
        }
    });
});

function back() {
    $("#ItemMaster").show();
    $("#mapRecipe").hide();
    $("#topHeading").text("Dish Master");
}

function populateRecipeMultiSelect() {
    $("#inputrecipelist").kendoMultiSelect({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "RecipeCode",
        dataSource: recipedata,
        index: -1,
        autoClose: false
    });
    var multiselect = $("#inputrecipelist").data("kendoMultiSelect");
   // multiselect.bind("change", multiselect_select);
}

function multiselect_select() {
   // var multiselect = $("#inputrecipelist").data("kendoMultiSelect");
   // var dataItems = multiselect.dataItems();
    //add in list and refresh grid
}

HttpClient.MakeRequest(CookBookMasters.GetRecipeDataList, function (data) {

    recipedata = data;
  //  recipedata = [];
   
    populateRecipeMultiSelect();
}, { condition: "All" }, false);
function UpdateRecipe() {
   // alert("Update Grid MApper");
    var multiselect = $("#inputrecipelist").data("kendoMultiSelect");
    var flagDefault = 0;
//get from multi select and bulid grid data
    var dataItems = multiselect.dataItems();
    console.log(dataItems);
    for (item of dataItems) {
        item.IsDefault = false;
        item.DishCode = DishCode;
        if (checkedRecipeCode == item.RecipeCode) {
            item.IsDefault = true;
            flagDefault = 1;
        }
        //condition for default and true
    }
    if (!flagDefault && dataItems.length>0) {
        dataItems[0].IsDefault = true;
    }
    //populateRecipeGridCustomUpdate();
    recipeCustomData = dataItems;
    populateRecipeGridCustomUpdate()
}

function populateRecipeGridCustomUpdate() {


    var gridVariable = $("#gridRecipeCustom");//Hare Krishna!!
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "Recipe.xlsx",
            filterable: true,
            allPages: true
        },
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        //pageable: {
        //    pageSize: 15
        //},
        columns: [
            {
                field: "RecipeCode", title: "Recipe Code", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Recipe Name", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.Name)+ '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetailsCustom(this)'></i>`;
                    return html;
                },
            },
           
            {
                field: "Quantity", title: "Quantity", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },

            {
                field: "IsBase", title: "Base Recipe", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: '# if (IsBase == true) {#<div>Yes</div>#} else {#<div>No</div>#}#',
            },


            {
                field: "IsDefault", title: "Default ", width: "80px", attributes: {

                    style: "text-align: center; font-weight:normal;display:list-item"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: templateFunction,
            }
        ],
        dataSource: {
            data: recipeCustomData,
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Name: { editable: false },
                        IsActive: { editable: false }
                    }
                }
            },
            pageSize: 15,
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        change: function (e) {
        },

    })
    function templateFunction(dataItem) {
        console.log(dataItem);

        var item = "<span>"
        if (dataItem.IsDefault) {
            item += "<input type='radio' style='margin-top: 7px;'  onclick='setDataItem(this);'  name='" + dataItem.DishCode + "' checked=checked />";
            console.log(dataItem);
            checkedRecipeCode = dataItem.RecipeCode;
        } else {
            item += "<input type='radio' style='margin-top: 7px;' onclick='setDataItem(this);'  name='" + dataItem.DishCode + "'/>";
        }
        item += "</span>"

        return item;
    };
    Utility.UnLoading();

   

}



function populateRecipeGridCustom() {

   
        customData(DishCode);
   
    var gridVariable = $("#gridRecipeCustom");//Hare Krishna!!
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "Recipe.xlsx",
            filterable: true,
            allPages: true
        },
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        //pageable: {
        //    pageSize: 15
        //},
        columns: [
            {
                field: "RecipeCode", title: "Recipe Code", width: "60px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "Name", title: "Recipe Name", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                template: function (dataItem) {
                    var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.Name) + '</span>'
                        + `  <i style='float:right;' class='fas fa-external-link-alt' onclick='showDetailsCustom(this)'></i>`;
                    return html;
                },
            },
            //{
            //    field: "ID", title: "View", width: "30px",
            //    template: "<i class='fas fa-external-link-alt' onclick='showDetailsCustom(this)'></i>",
            //    attributes: {
            //        style: "text-align: left; font-weight:normal"
            //    },
            //}, 
            {
                field: "Quantity", title: "Quantity", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "UOMName", title: "UOM", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },

            {
                field: "IsBase", title: "Base Recipe", width: "60px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: '# if (IsBase == true) {#<div>Yes</div>#} else {#<div>No</div>#}#',
            },
            

            {
                field: "IsDefault", title: "Default ", width: "80px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
                template: templateFunction,
            }
        ],
        dataSource: {
            data:recipeCustomData  ,
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Name: { editable: false },
                        IsActive: { editable: false }
                    }
                }
            },
            pageSize: 15,
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            //var items = e.sender.items();

            //items.each(function (e) {

            //    if (user.UserRoleId == 1) {
            //        $(this).find('.k-grid-Edit').text("Edit");
            //    } else {
            //        $(this).find('.k-grid-Edit').text("View");
            //    }
            //});
        },
        change: function (e) {
        },
       
    })
    function templateFunction(dataItem) {
       

        var item = "<span>"
        if (dataItem.IsDefault) {
            item += "<input type='radio' style='margin-top: 7px;' name='" + dataItem.DishCode + "' onclick='setDataItem(this);'   checked=checked />";
            console.log(dataItem);
            checkedRecipeCode = dataItem.RecipeCode;
        } else {
            item += "<input  style='margin-top: 7px;' type='radio' onclick='setDataItem(this);' name='" + dataItem.DishCode + "'/>";
            }
        item += "</span>"
       
        return item;
    };
    Utility.UnLoading();

    var multiselect = $("#inputrecipelist").data("kendoMultiSelect");

    //clear filter
    multiselect.dataSource.filter({});

    //set value
    multiselect.value(multiarray);

}

function setDataItem(item) {
    var grid = $("#gridRecipeCustom").data("kendoGrid");
    var row = $(item).closest("tr");
    var dataItem = grid.dataItem(row);
    console.log(dataItem)
    dataItem.IsDefault = true;
    checkedRecipeCode = dataItem.RecipeCode;
    for (x of recipeCustomData) {
        if (x.RecipeCode == checkedRecipeCode) {
            x.IsDefault = true;
        }
        else {
            x.IsDefault = false;
        }
    }
}

function saveDB() {

    //make model;
    //grid data

    saveModel = [];
    for (item of recipeCustomData) {
        var x = {};
        x.RecipeCode = item.RecipeCode;
        x.DishCode = item.DishCode;
        x.Recipe_ID = item.ID;
        x.IsDefault = item.IsDefault;
        x.IsActive = 1;
        x.Dish_ID = Dish_ID;
        saveModel.push(x);
    }
    //call and message display 
    if (saveModel.length > 0) {


        HttpClient.MakeRequest(CookBookMasters.SaveDishRecipeMappingList, function (result) {
            if (result == false) {
                toastr.error("Some error occured, please try again.");
            }
            else {
                $(".k-overlay").hide();
                toastr.success("Records have been updated successfully");
            }
        }, {
                model: saveModel

        });
    }
}

function showDetailsCustom(e) {

    var element = e
    var row = element.closest("tr");
    var grid = $("#gridRecipeCustom").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    var gridObj = $("#gridRecipeCustom").data("kendoGrid");
    var trArray = gridObj.dataSource._data.filter(function (x) {
        if (x.Name == dataItem.Name)
            return x;
    })

    tr = trArray[0];
    datamodel = tr;
    datamodel.SiteCode = siteCode;
    RecipeName = tr.Name;
    var dialog = $("#windowEdit1").data("kendoWindow");
    $("#windowEdit1").kendoWindow({
        animation: false
    });
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();

    populateUOMDropdown1();
    populateBaseDropdown1();
    populateBaseRecipeGrid1(tr.ID);
    populateMOGGrid1(tr.ID);

    $("#recipeid1").val(tr.ID);
    $("#inputrecipecode1").val(tr.RecipeCode);
    $("#inputrecipename1").val(tr.Name);
    $("#inputuom1").data('kendoDropDownList').value(tr.UOM_ID);
    $("#inputquantity1").val(tr.Quantity);
    if (tr.IsBase == true)
        $("#inputbase1").data('kendoDropDownList').value("Yes");
    else
        $("#inputbase1").data('kendoDropDownList').value("No");
    $('#grandTotal1').html(tr.TotalCost);
    $('#grandCostPerKG1').html(tr.CostPerKG);
    $('#inputinstruction1').val(tr.Instructions);
    //if (user.UserRoleId === 1) {
    //    $("#inputrecipename1").removeAttr('disabled');
    //    $("#inputuom1").removeAttr('disabled');
    //    $("#inputquantity1").removeAttr('disabled');
    //    $("#inputbase1").removeAttr('disabled');
    //    $("#btnSubmit1").css('display', 'inline');
    //    $("#btnCancel1").css('display', 'inline');
    //} else {
    //    $("#inputrecipename1").attr('disabled', 'disabled');
    //    $("#inputuom1").attr('disabled', 'disabled');
    //    $("#inputquantity1").attr('disabled', 'disabled');
    //    $("#inputbase1").attr('disabled', 'disabled');
    //    $("#btnSubmit1").css('display', 'none');
    //    $("#btnCancel1").css('display', 'none');
    //}

    dialog.title("Recipe Details Inline - " + RecipeName);
    return true;

}

$(document).ready(function () {
  
});

function addNewRecipe() {
    $("#mapRecipe").hide();
}
function backCreate() {
    $("#mapRecipe").show();
    $("#windowEdit0").hide();
    var multiselect = $("#inputrecipelist").data("kendoMultiSelect");

    HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {
        recipedata = data;
        multiselect.setDataSource(data);
    }, { condition: "All" }, false);

 
    
}
function backCreate() {
    $("#mapRecipe").show();
    $("#windowEdit0").hide();
    var multiselect = $("#inputrecipelist").data("kendoMultiSelect");
    HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeDataList, function (data) {
        recipedata = data;
        multiselect.setDataSource(data);
    }, { condition: "All" }, false);

}