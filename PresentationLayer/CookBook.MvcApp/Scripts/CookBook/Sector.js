﻿
var foodprogramdata = [];
var menuitemdata = [];
var localdata = [];
var itemG = {};
HttpClient.MakeRequest(CookBookMasters.GetFoodProgramDataList, function (data) {
    console.log("foodprogramdata");
    console.log(data);
    var dataSource = data;
    foodprogramdata = [];
    foodprogramdata.push({ "value": 0, "text": "All" });
    for (var i = 0; i < dataSource.length; i++) {
        foodprogramdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name });
    }
    populateFoodProgramDropdown();
 
}, null, false);



HttpClient.MakeRequest(CookBookMasters.GetItemDataList, function (result) {
    Utility.UnLoading();

    if (result != null) {
        var dataSource = result;
        console.log("menuitemdata");
        console.log(result);
        menuitemdata = [];
        menuitemdata = result;
        menuitemdata.unshift({ "ID": 0, "ItemName": "All Category" })
        localdata = menuitemdata;
        populateMenuItemDropdown();
    }
    else {
   
    }
}, null, false);
                    





function populateFoodProgramDropdown() {
    $("#inputfoodprogram").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: foodprogramdata,
        index: 0,
    });
    var dropdownlist = $("#inputfoodprogram").data("kendoDropDownList");
    dropdownlist.bind("change", dropdownlist_selectMain);

}


function dropdownlist_selectMain(eid) {

    var ID = eid.sender.dataItem(eid.item).value;
    if (ID == 0) {
        localdata = menuitemdata;
    }
    else {
        localdata = menuitemdata.filter(function (item) {

            return item.FoodProgram_ID == ID;
        });
    }
   

    localdata.unshift({ "ID": 0, "ItemName": "All Sub Category" });
    populateMenuItemDropdown();
  
}

function populateMenuItemDropdown() {
    $("#inputmenuitem").kendoDropDownList({
        filter: "contains",
        dataTextField: "ItemName",
        dataValueField: "ID",
        dataSource: localdata,
        index: 0,
        
    });
    $("#applyArea").empty();
    for (var item of localdata) {
       
        if (item.ID == 0) {
            continue;
        }
        

        $("#applyArea").prepend('<div class="tilesCustom"><div class="item-photo img" style="background-image: url(../ItemImages/' + item.ImageName + '" ></div>' +
           ' <div class="combine"><div class="itemName"><div class="squareVeg"><div class="statusVeg"></div></div>'
            + item.ItemName +  '</div ><div class="itemCode">ItemCode'
            + item.ItemCode + '</div></div><div class="price">Price Rs 60.</div><div class="configure">Configure</div></div>');

    }
    var dropdownlist2 = $("#inputmenuitem").data("kendoDropDownList");
    dropdownlist2.bind("change", dropdownSelectSub);
}

function dropdownSelectSub(eid) {

    
    itemG = eid.sender.dataItem(eid.item);
    console.log(itemG);


}  

function apply() {
   // alert("hare Krishna")
    $("#applyArea").empty();
    var item = itemG;
    if (itemG.ID == 0) {
        for (var item of localdata) {

            if (item.ID == 0) {
                continue;
            }

            $("#applyArea").prepend('<div class="tilesCustom"><div class="item-photo img" style="background-image: url(../ItemImages/' + item.ImageName + '" ></div>' +
                ' <div class="combine"><div class="itemName"><div class="squareVeg"><div class="statusVeg"></div></div>'
                + item.ItemName + '</div ><div class="itemCode">ItemCode'
                + item.ItemCode + '</div></div><div class="price">Price Rs 60.</div><div class="configure">Configure</div></div>');

        }
    }
    else {

        $("#applyArea").prepend('<div class="tilesCustom"><div class="item-photo img" style="background-image: url(../ItemImages/' + item.ImageName + '" ></div>' +
            ' <div class="combine"><div class="itemName"><div class="squareVeg"><div class="statusVeg"></div></div>'
            + item.ItemName + '</div ><div class="itemCode">ItemCode'
            + item.ItemCode + '</div></div><div class="price">Price Rs 60.</div><div class="configure">Configure</div></div>');

    }
}