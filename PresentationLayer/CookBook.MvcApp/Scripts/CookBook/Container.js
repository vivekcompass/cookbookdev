﻿var uomdata = [];
var contypedata = [];
/*var aliasdata = [];*/
var modifiedContainerData = [];

var res;
function modifiedDataSource() {
    

    HttpClient.MakeSyncRequest(CookBookMasters.GetContainerDataList, function (result) {
       

        if (result != null) {
            modifiedContainerData = result;
        }
        else {
          
        }
    }, null

        , true);

    modifiedContainerData.forEach(function (item) {
        item.ContainerDetail = "";
       
        for (itemX of contypedata) {
            if (item.ContainerTypeCode == null) {
                item.ContainerDetail = null
                break;
            }
            else if (item.ContainerTypeCode == itemX.value) {
                item.ContainerDetail = itemX.text;
                break;
            }
        }
        for (itemX of uomdata) {
            if (item.ContainerUOM == itemX.value) {
                item.UOM = itemX.text;

                break;
            }
        }
        return item;
    });
    
}

//$(function () {

    var user;
    var datamodel = null;

function validateImportFile(filename) {

    if (filename != '') {
        var fileExtension = ['png', 'jpg','jpeg'];
        if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == -1) {
            toastr.error("Extension not allowed,Kindly check");
            return false;
        }
    }
    return true;
};
$(document).ready(function () {
    $(".k-window").hide();
    $(".k-overlay").hide();
    $('#file').change(function () {

        var that = this;
        if (validateImportFile(that.value)) {
            //$('#txtFilePath').val(that.value);
            $('#txtFilePath').val(that.files[0].name);
            if (that.files[0]) {
                var uploadimg = new FileReader();
                uploadimg.onload = function (displayimg) {
                    $("#image").attr('src', displayimg.target.result);
                }
                uploadimg.readAsDataURL(that.files[0]);
            }
        }
        else {
            //ImportReport.refreshPage();
        }
    });
    $('#myInput').on('input', function (e) {
        var grid = $('#gridContainer').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
               
                if (x.field == "ContainerCode" || x.field == "Name" || x.field == "ContainerDetail" || x.field == "aliasName" || x.field == "ContainerWeight"
                    || x.field == "UOM") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $("#ContainerswindowEdit").kendoWindow({
        modal: true,
        width: "740px",
        height: "440px",
        title: "Container Details",
        actions: ["Close"],
        visible: false,
        animation: false,
        resizable: false
    });
        HttpClient.MakeSyncRequest(CookBookMasters.GetUOMDataList, function (data) {
        var dataSource = data;
        uomdata = [];
        uomdata.push({ "value": "UOM", "text": "UOM" });
        for (var i = 0; i < dataSource.length; i++) {
            uomdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].Name });
        }
    }, null, false);

       
        HttpClient.MakeSyncRequest(CookBookMasters.GetContainerTypeDataList, function (data) {
            var dataSource = data;
            contypedata = [];
            contypedata.push({ "value": null, "text": "Select","IsActive":0});
            for (var i = 0; i < dataSource.length; i++) {
                
                contypedata.push({ "value": dataSource[i].ContainerTypeCode, "text": dataSource[i].Name, "IsActive": dataSource[i].IsActive });
            }
        }, null, false);
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
        });
      
   
    populateContainerGrid();
   

    $("#CCbtnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                //$(".k-overlay").hide();
                $(".k-window").hide();
                $(".k-overlay").hide();
                var orderWindow = $("#ContainerswindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#cnaddnew").on("click", function () {
        datamodel = null;
        $('#cnsubmit').removeAttr("disabled");
        // $("#cnaddnew").css("display", "none");
        $("#cnname").css("display", "block");
        var dialog = $("#ContainerswindowEdit").data("kendoWindow");
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open().element.closest(".k-window").css({
            top: 167,
           
        });
        //dialog.open();
        dialog.center();
        dialog.title("Create Container Detail");
        $("#cnid").val("");
        $("#cnname").val("");
        $("#cnaliasname").val("");
   
        $("#file").val("");
        //$("#image").attr('src', '../ContainerImages/default.png');
        $("#image").attr('src', './ContainerImages/default.png');

        populateUOMDropdown('#inputconuom');
        populateCTDropdown("#inputctuom");
        $("#inputctuom").data('kendoDropDownList').value(null);
        $("#cnwt").val("");
        $("#cndepth").val("");
        $("#cnbreadth").val("");
        $("#cnlength").val("");
        $("#cnname").removeClass("is-invalid");
        $("#cnactive").prop('checked', true);
        $("#cnname").focus();
        item = null;
    });

    $("#CCbtnSubmit").click(function () {
       
        if ($("#cnname").val() === "") {
            toastr.error("Please provide Container Detail Name");
            $("#cnname").focus();
            return;
        }
        if ($("#inputctuom").val() == "Select" || $("#inputctuom").val().trim() == "" || $("#inputctuom").val() ==null) {
            toastr.error("Please select Container Name");
            $("#inputctuom").focus();
            return;
        }
        if ($("#cnwt").val() === "") {
            toastr.error("Please provide Container Weight");
            $("#cnwt").focus();
            return;
        }
        if ($("#inputconuom").val() === "Select") {
            toastr.error("Please select Weight UOM");
            $("#inputconuom").focus();
            return;
        }
        else {
            Utility.Loading();
            var formData = new FormData();
            var file = document.getElementById("file").files[0];// File Properties
            if (file != undefined) {
                var fileName = document.getElementById("file").value;
                var idxDot = fileName.lastIndexOf(".") + 1;
                var extn = fileName.substr(idxDot, fileName.length).toLowerCase();
                //var extn = file.type.split("/")[1];
                if (!file.name.match(/.(jpg|jpeg|png)$/i)) {
                    toastr.error("File type is not appropraite");
                    Utility.UnLoading();
                    return;
                }
                formData.append('name', $("#cnname").val().replaceAll(" ", ""));
                formData.append('folderName', "ContainerImages");
                formData.append("file", file);
                $.ajax({
                    type: "POST",
                    //url: '../Item/UploadFile',
                    url: './Item/UploadFile',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (response) {

                    },
                    error: function (error) {
                        Utility.UnLoading();
                        toastr.error("Problem while uploading the Container Image on Server. Please contact Administrator");
                    }
                });
            }

            var model;
            var ContainerAliasText = $("#cnaliasname").val();
            var ContainerTypeCode = $("#inputctuom").data('kendoDropDownList').value();
            var ContainerUOMCode = $("#inputconuom").data('kendoDropDownList').value();

            if (datamodel != null) {
                model = datamodel;
                model.Name = $("#cnname").val();
                model.Length = $("#cnlength").val();
                model.Breadth = $("#cnbreadth").val();
                model.Depth = $("#cndepth").val();
                model.ContainerAliasText = ContainerAliasText;
                model.ContainerUOM = ContainerUOMCode;
                model.ContainerTypeCode = ContainerTypeCode;
                model.ContainerWeight=$("#cnwt").val(),
                model.IsActive = 1;
                model.ContainerImage = $("#cnname").val().replaceAll(" ", "") + "." + extn;
            }
            else {
                model = {
                    "ID": $("#cnid").val(),
                    "Name": $("#cnname").val(),
                    "Length": $("#cnlength").val(),
                    "Breadth": $("#cnbreadth").val(),
                    "Depth": $("#cndepth").val(),
                    "ContainerAliasText": ContainerAliasText,
                    "ContainerTypeCode": ContainerTypeCode,
                    "ContainerUOM": ContainerUOMCode,
                    "ContainerWeight": $("#cnwt").val(),
                    "IsActive": 1,
                    "ContainerImage": $("#cnname").val().replaceAll(" ", "") + "." + extn
                }
            }
            if (!sanitizeAndSend(model)) {
                return;
            }
            $("#CCbtnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveContainerData, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#btnSubmit').removeAttr("disabled");
                    $("#sitealias").focus();
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#CCbtnSubmit').removeAttr("disabled");
                        $("#cnname").focus();
                        toastr.error(result.message);
                        Utility.UnLoading();
                        return;
                    }
                    else {
                        if (result == false) {
                            $('#CCbtnSubmit').removeAttr("disabled");
                            toastr.error("Duplicate Container Details name");
                            $("#cnname").focus();
                            Utility.UnLoading();
                        }
                        else {
                            if (result == "Duplicate") {
                                $('#CCbtnSubmit').removeAttr("disabled");
                                toastr.error(result.message);
                                $("#cnname").focus();
                                Utility.UnLoading();
                                return;
                            }
                            $(".k-overlay").hide();
                            var orderWindow = $("#ContainerswindowEdit").data("kendoWindow");
                            orderWindow.close();
                            $('#CCbtnSubmit').removeAttr("disabled");
                            if (model.ID > 0) {
                                toastr.success("Container Detail Updated Successfully");
                            }
                            else {
                                toastr.success("Container Detail Added Successfully");
                                // toastr.success("Container added successfully");
                            }
                            Utility.UnLoading();
                            populateContainerGrid();
                        }
                    }
                }
            }, {
                model: model

            }, false);
        }
    });
    });
 
function populateContainerGrid() {
       modifiedDataSource();
        Utility.Loading();
        var gridVariable = $("#gridContainer");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "ContainerCode", title: "Code", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Container Name", width: "110px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ContainerDetail", title: "Container Detail", width: "110px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "aliasName", title: "Alias ", width: "110px", attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    }
                //},
                {
                    field: "ContainerWeight", title: "Empty Container Weight (in Kg)", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive",
                //    title: "Status", width: "50px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal;"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                 var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                item = this.dataItem(tr);          // get the date of this row
                                var model = item;
                                datamodel = item;
                                var dialog = $("#ContainerswindowEdit").data("kendoWindow");
                                
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialog.open().element.closest(".k-window").css({
                                    top: 167,
                                    left: 558

                                });
                                dialog.center();
                                dialog.title(model.Name + " - Edit Details");
                                ContainerCodeInitial = item.ContainerCode;
                                $('#cnsubmit').removeAttr("disabled");
                                populateCTDropdown("#inputctuom",model.ContainerTypeCode);
                                $("#cnid").val(model.ID);
                                $("#cnname").val(model.Name);
                                $("#file").val("");
                                //$("#image").attr('src', '../ContainerImages/' + model.ContainerImage);
                                $("#image").attr('src', './ContainerImages/' + model.ContainerImage);
                                //populateUOMDropdown('#inputdimuom',model.DimensionUOM);
                                populateUOMDropdown('#inputconuom',model.ContainerUOM);
                                $("#cnwt").val(model.ContainerWeight);
                                $("#cndepth").val(model.Depth);
                                $("#cnbreadth").val(model.Breadth);
                                $("#cnlength").val(model.Length);
                                $("#cnname").removeClass("is-invalid");
                                //$("#cnactive").prop('checked', true);
                                $("#cnactive").prop('checked', model.IsActive);
                                $("#cnname").focus();
                                $("#cnname").removeClass("is-invalid");
                                $("#cnaliasname").val(model.ContainerAliasText);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                data: modifiedContainerData,
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ContainerCode: { editable: false, type: "string" },
                            Name: { type: "string" },
                            ContainerDetail: { type: "string" },
                            ContainerWeight: { type: "string" },
                            UOM: { type: "string" },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {

                    $(this).find('.k-grid-Edit').css('display', 'inline');
                });
                $(".chkbox").on("change", function () {
                    var gridObj = $("#gridContainer").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    var th = this;
                    datamodel = tr;
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Container " + active + "?", "Container Update Confirmation", "Yes", "No", function () {
                        HttpClient.MakeRequest(CookBookMasters.ChangeStatus, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                toastr.success("container  updated successfully");
                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {

                        $(th)[0].checked = !datamodel.IsActive;
                    });
                    return true;
                });
            },
            change: function (e) {
            },
        })
        //$(".k-label")[0].innerHTML.replace("items", "records"); 
    }


//});

//function populateAliasDropdown(uomid, uomvalue) {

//    //$(uomid).kendoDropDownList({
//    //    dataTextField: "text",
//    //    dataValueField: "value",
//    //    dataSource: aliasdata,
//    //    index: 0,
//    //});
//    //var dropdownlist = $(uomid).data("kendoDropDownList");
//    //if (uomvalue != null)
//    //    dropdownlist.value(uomvalue);
//}

function populateCTDropdown(uomid, uomvalue) {

    $(uomid).kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: contypedata,
        index: 0,
        change: function (e) {
            if (!e.sender.dataItem()?.IsActive) {
                toastr.error("Selected Item is In-Active");
                var dropdownlist = $(uomid).data("kendoDropDownList");
                dropdownlist.select("");
            }

        }
    });
    var dropdownlist = $(uomid).data("kendoDropDownList");
    if (uomvalue != null)
        dropdownlist.value(uomvalue);
}

function populateUOMDropdown(uomid, uomvalue) {

    $(uomid).kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
    var dropdownlist = $(uomid).data("kendoDropDownList");
    if (uomvalue == -1 || uomvalue == null)
        dropdownlist.select(1);//value("Select");
    else
        dropdownlist.value(uomvalue);

    //dropdownlist.bind("change", uomdropdownlist_select);
}