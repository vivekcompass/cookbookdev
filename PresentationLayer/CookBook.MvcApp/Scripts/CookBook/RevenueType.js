﻿var user;
var conceptype2data;
var tileImage = null;
var item = null;
var RevenueTypeCodeIntial = null;
var datamodel;
$(function () {

    $('#myInput').on('input', function (e) {
        var grid = $('#gridRevenueType').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "RevenueTypeCode" || x.field == "Name") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $(document).ready(function () {
        $("#windowEdit").kendoWindow({
            modal: true,
            width: "250px",
            height: "170px",
            title: "RevenueType Details  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });


        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;

            populateRevenueTypeGrid();
        }, null, false);


    });



    function dpvalidate() {
        var valid = true;

        if ($("#dpname").val() === "") {
            toastr.error("Please provide input");
            $("#dpname").addClass("is-invalid");
            valid = false;
            $("#dpname").focus();
        }
        if (($.trim($("#dpname").val())).length > 30) {
            toastr.error("RevenueType accepts upto 30 charactre");

            $("#dpname").addClass("is-invalid");
            valid = false;
            $("#dpname").focus();
        }
        return valid;
    }
    //RevenueType Section Start

    function populateRevenueTypeGrid() {

        Utility.Loading();
        var gridVariable = $("#gridRevenueType");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "RevenueTypeCode", title: "Code", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive", title: "Active", width: "75px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                               
                                var item = this.dataItem(tr);
                                if (!item.IsActive) {
                                    return;
                                }// get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive,
                                    "RevenueTypeCode": item.RevenueTypeCode,
                                    "CreatedBy": item.CreatedBy,
                                    "CreatedOn": kendo.parseDate(item.CreatedOn)
                                };
                               RevenueTypeCodeIntial = item.RevenueTypeCode;
                                var dialog = $("#windowEdit").data("kendoWindow");

                                dialog.open();
                                dialog.center();

                                datamodel = model;

                                $("#dpid").val(model.ID);
                                $("#dpcode").val(model.RevenueTypeCode);
                                $("#dpname").val(model.Name);

                                $("#dpname").focus();
                                dialog.title("Revenue Type Details - " + model.Name);
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetRevenueTypeDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { type: "string" },
                           RevenueTypeCode: { type: "string" },
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
               
                $(".chkbox").on("change", function () {
                    var gridObj = $("#gridRevenueType").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    var trEdit = $(this).closest("tr");
                    var th = this;
                    datamodel = tr;
                    datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b>RevenueType " + active + "?", "RevenueType Update Confirmation", "Yes", "No", function () {
                        HttpClient.MakeRequest(CookBookMasters.SaveRevenueType, function (result) {
                            if (result == false) {
                                toastr.error("Some error occured, please try again");
                            }
                            else {
                                toastr.success("RevenueType updated successfully");
                                $(th)[0].checked = datamodel.IsActive;
                                if ($(th)[0].checked) {
                                    $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                } else {
                                    $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                }
                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {

                            $(th)[0].checked = !datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                    });
                    return true;
                });

            },
            change: function (e) {
            },
        })
        gridVariable.data("kendoGrid").dataSource.sort({ field: "RevenueTypeCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function populateDietCategoryDropdown_Bulk() {
        $("#dietcategorysearch").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: dietcategorydataOut,
            index: 0,
            select: function (e) {
                if (e.item.data().offsetIndex == 0) {
                    //e.preventDefault();
                    var filter = { logic: 'or', filters: [] };
                    var grid = $('#gridItem').data('kendoGrid');
                    grid.dataSource.filter(filter);
                    return;
                }

                var grid = $('#gridItem').data('kendoGrid');
                var columns = grid.columns;

                var filter = { logic: 'or', filters: [] };
                columns.forEach(function (x) {
                    if (x.field) {
                        if (x.field == "DietCategoryName") {
                            var type = grid.dataSource.options.schema.model.fields[x.field].type;
                            var targetValue = e.sender.dataItem(e.item).text;

                            if (type == 'string') {
                                if (x.field == "Status") {
                                    var pendingString = "pending";
                                    var savedString = "saved";
                                    var mappedString = "mapped";
                                    if (pendingString.includes(targetValue.toLowerCase())) {
                                        targetValue = "1";
                                    }
                                    else if (savedString.includes(targetValue.toLowerCase())) {
                                        targetValue = "2";
                                    }
                                    else if (mappedString.includes(targetValue.toLowerCase())) {
                                        targetValue = "3";
                                    }
                                }
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: targetValue
                                })
                            }
                            else if (type == 'number') {

                                if (isNumeric(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: e.target.value
                                    });
                                }
                            } else if (type == 'date') {
                                var data = grid.dataSource.data();
                                for (var i = 0; i < data.length; i++) {
                                    var dateStr = kendo.format(x.format, data[i][x.field]);
                                    if (dateStr.startsWith(e.target.value)) {
                                        filter.filters.push({
                                            field: x.field,
                                            operator: 'eq',
                                            value: data[i][x.field]
                                        })
                                    }
                                }
                            } else if (type == 'boolean' && getBoolean(targetValue) !== null) {
                                var bool = getBoolean(e.target.value);
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: bool
                                });
                            }
                        }
                    }
                }); grid.dataSource.filter(filter);

            }
        });
    };


    $("#dpcancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#dpaddnew").on("click", function () {
        var model;
        var dialog = $("#windowEdit").data("kendoWindow");

        dialog.open();
        dialog.center();

        datamodel = model;

        dialog.title("New Revenue Type Creation");

        $("#dpname").removeClass("is-invalid");
        $('#dpsubmit').removeAttr("disabled");
        $("#dpid").val("");
        $("#dpname").val("");
        $("#dpcode").val("");
        $("#dpname").focus();
       RevenueTypeCodeIntial = null;
    });

    $("#dpsubmit").click(function () {
        if ($("#dpname").val() === "") {
            toastr.error("Please provide Revenue Type Name");
            $("#dpname").focus();
            return;
        }
        if (dpvalidate() === true) {
            $("#dpname").removeClass("is-invalid");

            var model = {
                "ID": $("#dpid").val(),
                "Name": $("#dpname").val(),
                "RevenueTypeCode": RevenueTypeCodeIntial,
                "IsActive":1
            }
            if (RevenueTypeCodeIntial == null) {
                model.IsActive = 1;
            } else {
                model.CreatedBy = datamodel.CreatedBy;
                model.CreatedOn = datamodel.CreatedOn;
            }

            $("#dpsubmit").attr('disabled', 'disabled');
            if (!sanitizeAndSend(model)) {
                return;
            }
            HttpClient.MakeRequest(CookBookMasters.SaveRevenueType, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#dpsubmit').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#dpsubmit').removeAttr("disabled");
                        toastr.success("There was some error, the record cannot be saved");

                    }
                    else {
                        $(".k-overlay").hide();
                        $('#dpsubmit').removeAttr("disabled");

                        if (model.ID > 0)
                            toastr.success("RevenueType record updated successfully");
                        else
                            toastr.success("New Revenue Type record added successfully");
                        $(".k-overlay").hide();
                        var orderWindow = $("#windowEdit").data("kendoWindow");
                        orderWindow.close();
                        $("#gridRevenueType").data("kendoGrid").dataSource.data([]);
                        $("#gridRevenueType").data("kendoGrid").dataSource.read();
                        $("#gridRevenueType").data("kendoGrid").dataSource.sort({ field: "RevenueTypeCode", dir: "asc" });

                    }


                }
            }, {
                model: model

            }, true);
        }
    });



});