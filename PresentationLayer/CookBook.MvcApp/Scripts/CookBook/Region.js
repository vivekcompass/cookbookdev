﻿//import { forEach } from "angular";

//const { checked } = require("modernizr");
var DishItemCategory = [];
var fmodel = [];
var foodprogramdata = [];
var menuitemdata = [];
var localdata = [];
var itemG = {};
var sitemasterdata = [];
var regionmasterdata = [];
var subsectorMasterData = [];
var regionNonInheritedMasterDataSource = [];
var user;
var status = "";
var varname = "";
var datamodel;
var ItemName = "";
var dishcategorydata = [];
var dietcategorydata = [];
var dietcategorydataOut = [];
var itemObject = {};
var ingredientObject = [];
var dishdata = [];
var masterGroupList = [];
var masterQty = [];
var currentMasterQtyList = [];
var currentMasterList = [];
var masterItems = [];
var finalModel = [];
var finalModelExits = [];
var eid = 0;
var allMenuData = [];
var confingMenuData = [];
var unconfigMasterData = [];
var itemsids = [];
var tr = 0;
var multiarray = [];
var flagAlert = 0;
var flagDishStandardQtyAlert = 0;
var finalModelExitsDishCategory = [];
var flagCurated = 0;
var dataItems = [];
var dataItems1 = [];
var flagSectorIsActive = 0;
var filterDropdownData = [
    { text: "All", value: "All" },
    { text: "Sector", value: "Sector" },
    { text: "Region", value: "Region" },

];
var regionDishCategory = [];
var uomdata = [];
var uommodulemappingdata = [];
var finalModelDishCategory = [];
var flagRegion = 0;
var flagSector = 0;
var user;
var grid;
var InheritedItem = [];
var regionDataSource = [];
var regionMasterDataSource = [];
var flagToggle = 1;
var UniqueItemCodes = [];
var checkedIds = {};
var checkedItemCodes = [];
var Pub_checkedItemCodes = [];
var RegionID = 0;
var selItemDietCategory = 0;
var isPublishBtnClick = false;
var topText = '';
var controlCall = 1;
var globalChecked = false;


var subSectorCode = "";
var subSectorCodeDD = "";
var subSectorNameDD = "";
var isRegionInheritedCase = false;
var lastValid = 0;
var validNumber = new RegExp(/^\d*\.?\d*$/);
var isSubSectorApplicable = false;

function validateNumber(elem) {

    if (validNumber.test(elem.value)) {
        lastValid = elem.value;
    } else {
        elem.value = lastValid;
    }
}
kendo.data.DataSource.prototype.dataFiltered = function () {
    // Gets the filter from the dataSource
    var filters = this.filter();

    // Gets the full set of data from the data source
    var allData = this.data();

    // Applies the filter to the data
    var query = new kendo.data.Query(allData);

    // Returns the filtered data
    return query.filter(filters).data;
}
function compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const bandA = a.RegionDesc.toUpperCase();
    const bandB = b.RegionDesc.toUpperCase();

    let comparison = 0;
    if (bandA > bandB) {
        comparison = 1;
    } else if (bandA < bandB) {
        comparison = -1;
    }
    return comparison;
}

function removeDuplicateObjectFromArray(array, key) {
    let check = {};
    let res = [];
    for (let i = 0; i < array.length; i++) {
        if (!check[array[i][key]]) {
            check[array[i][key]] = true;
            res.push(array[i]);
        }
    }
    return res;
}

//Krish 30-07-2022
//HttpClient.MakeRequest(CookBookMasters.GetSubSectorMasterList, function (data) {

//    subsectorMasterData = data;

//    if (data.length > 1) {

//        isSubSectorApplicable = true;
//        subsectorMasterData.unshift({ "SubSectorCode": '0', "Name": "Select Sub Sector" })
//        $("#inputSubSector").css("display", "block");
//        $(".googleonly").css("display", "block");
//        populateSubSectorMasterDropdown();
//    }
//    else {
//        isSubSectorApplicable = false;
//        $(".googleonly").css("display", "none");
//    }

//}, { isAllSubSector: false }, false);

//HttpClient.MakeRequest(CookBookMasters.GetRegionMasterListByUserId, function (data) {

//    var dataSource = data;
//    sitemasterdata = dataSource;
//    regionmasterdata = sitemasterdata
//    if (data.length > 1) {
//        regionmasterdata.unshift({ "RegionID": 0, "Name": "Select Region" })
//        $("#btnGo").css("display", "inline-block");
//    }
//    else {
//        $("#btnGo").css("display", "inline-none");
//    }
//    populateRegionMasterDropdown();


//}, null, false);

function loadFormData() {
    
    HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
        user = result;
        //console.log(user)
        //user.IsSectorUser
        //if (user.UserRoleId === 1) {
        //    $("#InitiateBulkChanges").css("display", "inline");
        //    $("#AddNew").css("display", "inline");
        //}
        //if (user.UserRoleId === 2) {
        //    $("#InitiateBulkChanges").css("display", "none");
        //    $("#AddNew").css("display", "none");
        //}


    }, null, false);

    //Krish 30-07-2022
    HttpClient.MakeSyncRequest(CookBookMasters.GetRegionItemDataList, function (result) {
        //console.log(result);
        //GetSubSectorMasterList
        subsectorMasterData = result.SubSectorData;
        
        if (subsectorMasterData.length > 1) {
            subsectorMasterData = result.SubSectorData.filter(s => s.SubSectorCode != "SSR-001")[0];
            isSubSectorApplicable = true;
            subsectorMasterData.unshift({ "SubSectorCode": '0', "Name": "Select Sub Sector" })
            $("#inputSubSector").css("display", "block");
            $(".googleonly").css("display", "block");
            populateSubSectorMasterDropdown();
        }
        else {
            isSubSectorApplicable = false;
            $(".googleonly").css("display", "none");
        }

        //GetRegionMasterListByUserId
        var dataSource =[];
        if (user.IsSectorUser)
            dataSource = result.RegionMasterData;
        else
            dataSource = result.RegionsDetailsByUserIdData;
        sitemasterdata = dataSource;
        regionmasterdata = sitemasterdata
        if (dataSource.length > 1) {
            regionmasterdata.unshift({ "RegionID": 0, "Name": "Select Region" })
            $("#btnGo").css("display", "inline-block");
        }
        else {
            $("#btnGo").css("display", "inline-none");
        }
        populateRegionMasterDropdown();

        //GetUOMModuleMappingDataList
        var dataSource = result.UOMModuleMappingData;
        uommodulemappingdata = [];
        uommodulemappingdata.push({ "value": "Select", "text": "Select" });
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].IsActive) {
                uommodulemappingdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].UOMName, "UOMModuleCode": dataSource[i].UOMModuleCode });
            }
        }
        uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");

        //GetDishCategoryList
        var dataSource = result.DishCategoryData;
        dishcategorydata = [];

        for (var i = 0; i < dataSource.length; i++) {
            dishcategorydata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "alias": dataSource[i].Name, "code": dataSource[i].DishCategoryCode });
        }

        //GetFoodProgramDataList
        var dataSource = result.FoodProgramData;
        foodprogramdata = [];
        foodprogramdata.push({ "value": 0, "text": "Select" });
        for (var i = 0; i < dataSource.length; i++) {
            foodprogramdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name });
        }
        populateFoodProgramDropdown();

        //GetItemDataList
        if (result != null) {
            menuitemdata = [];
            menuitemdata = result.ItemData;
            menuitemdata.unshift({ "ID": 0, "ItemName": "Select" })
            localdata = menuitemdata;
            allMenuData = menuitemdata;
        }
        else {
        }

        //GetDietCategoryDataList
        var dataSource = result.DietCategoryData;
        dietcategorydata = [];
        dietcategorydataOut = [];
        dietcategorydataOut.push({ "value": "Select", "text": "Select Diet Category" });
        dietcategorydata.push({ "value": "Select", "text": "Select" });
        for (var i = 0; i < dataSource.length; i++) {
            dietcategorydata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "DietCategoryCode": dataSource[i].DietCategoryCode });
            dietcategorydataOut.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "DietCategoryCode": dataSource[i].DietCategoryCode });

        }
        populateDietCategoryDropdown_Bulk();

    }, { isSectorUser: user.IsSectorUser}, false);

    //HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
    //    var dataSource = data;
    //    uommodulemappingdata = [];
    //    uommodulemappingdata.push({ "value": "Select", "text": "Select" });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        if (dataSource[i].IsActive) {
    //            uommodulemappingdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].UOMName, "UOMModuleCode": dataSource[i].UOMModuleCode });
    //        }
    //    }
    //    uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00002' || m.text == "Select");
    //}, null, false);

    //HttpClient.MakeSyncRequest(CookBookMasters.GetDishCategoryList, function (data) {

    //    var dataSource = data;
    //    dishcategorydata = [];

    //    for (var i = 0; i < dataSource.length; i++) {
    //        dishcategorydata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "alias": dataSource[i].Name, "code": dataSource[i].DishCategoryCode });
    //    }

    //}, null, false);
    //HttpClient.MakeSyncRequest(CookBookMasters.GetFoodProgramDataList, function (data) {
    //    var dataSource = data;
    //    foodprogramdata = [];
    //    foodprogramdata.push({ "value": 0, "text": "Select" });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        foodprogramdata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name });
    //    }
    //    populateFoodProgramDropdown();

    //}, null, false);

    //HttpClient.MakeSyncRequest(CookBookMasters.GetItemDataList, function (result) {
    //    if (result != null) {
    //        menuitemdata = [];
    //        menuitemdata = result;
    //        menuitemdata.unshift({ "ID": 0, "ItemName": "Select" })
    //        localdata = menuitemdata;
    //        allMenuData = menuitemdata;

    //    }
    //    else {

    //    }
    //}, null, false);

    //HttpClient.MakeSyncRequest(CookBookMasters.GetDietCategoryDataList, function (data) {
    //    var dataSource = data;
    //    dietcategorydata = [];
    //    dietcategorydataOut = [];
    //    dietcategorydataOut.push({ "value": "Select", "text": "Select Diet Category" });
    //    dietcategorydata.push({ "value": "Select", "text": "Select" });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        dietcategorydata.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "DietCategoryCode": dataSource[i].DietCategoryCode });
    //        dietcategorydataOut.push({ "value": dataSource[i].ID, "text": dataSource[i].Name, "DietCategoryCode": dataSource[i].DietCategoryCode });

    //    }
    //    populateDietCategoryDropdown_Bulk();
    //}, null, false);
}

function populateDietCategoryDropdown_Bulk() {
    $("#dietcategorysearch").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: dietcategorydataOut,
        index: 0,
        select: function (e) {
            if (e.item.data().offsetIndex == 0) {
                //e.preventDefault();
                var filter = { logic: 'or', filters: [] };
                var grid = $('#gridItem').data('kendoGrid');
                grid.dataSource.filter(filter);
                return;
            }

            var grid = $('#gridItem').data('kendoGrid');
            var columns = grid.columns;

            var filter = { logic: 'or', filters: [] };
            columns.forEach(function (x) {
                if (x.field) {
                    if (x.field == "DietCategoryName") {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;
                        var targetValue = e.sender.dataItem(e.item).text;

                        if (type == 'string') {
                            if (x.field == "Status") {
                                var pendingString = "pending";
                                var savedString = "saved";
                                var mappedString = "mapped";
                                if (pendingString.includes(targetValue.toLowerCase())) {
                                    targetValue = "1";
                                }
                                else if (savedString.includes(targetValue.toLowerCase())) {
                                    targetValue = "2";
                                }
                                else if (mappedString.includes(targetValue.toLowerCase())) {
                                    targetValue = "3";
                                }
                            }
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: targetValue
                            })
                        }
                        else if (type == 'number') {

                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        } else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format(x.format, data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(targetValue) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                }
            }); grid.dataSource.filter(filter);

        }

    });
}

function InheritedRegionItem(regionID, flag) {
    HttpClient.MakeSyncRequest(CookBookMasters.GetRegionItemInheritanceMappingDataList, function (result) {
        if (result != null) {

            InheritedItem = result;
            modifiedDataSource();
            if (flag) {
                populateItemGrid();
                return;
            }
            populateItemGrid_IN();
        }
        else {

        }
    }, { regionID: regionID }, false);

}

function modifiedDataSource() {

    checkedItemCodes = [];
    checkedIds = {};

    HttpClient.MakeSyncRequest(CookBookMasters.GetUniqueMappedItemCodesFromItem, function (data) {
        var dataSource = data;
        UniqueItemCodes = dataSource;
    }, null, false);
    HttpClient.MakeSyncRequest(CookBookMasters.GetItemDataList, function (data) {

        if (data != null) {
            data = data.filter(function (item) {
                if (UniqueItemCodes.indexOf(item.ItemCode) != -1 && item.Status == 3)
                    return item;
            });
            if (InheritedItem.length > 0) {
                for (var x of data) {
                    var fStatus = 1;
                    for (y of InheritedItem) {
                        x["RecipeMapStatus"] = y.RecipeMapStatus;
                        if (user.SectorNumber == 10 && isSubSectorApplicable) {
                            if (x.ItemCode == y.ItemCode && y.SubSectorCode == subSectorCodeDD) {
                                x.Status = y.Status;
                                if (y.IsActive == 1) {
                                    x["MenuLevel"] = "Region";
                                }
                                else {
                                    x["MenuLevel"] = "Draft";
                                }
                                fStatus = 0;
                                x["RegionSubSectorCode"] = y.SubSectorCode;
                                x["SubSectorName"] = subSectorNameDD;
                                break;
                            }

                        }
                        else {
                            if (x.ItemCode == y.ItemCode) {
                                x.Status = y.Status;
                                if (y.IsActive == 1) {
                                    x["MenuLevel"] = "Region";
                                }
                                else {
                                    x["MenuLevel"] = "Draft";
                                }
                                fStatus = 0;
                                x["SubSectorName"] = "";
                                break;
                            }

                        }


                    }
                    if (fStatus) {
                        x["MenuLevel"] = "Sector";
                    }

                }
            } else {
                for (var x of data) {
                    x["MenuLevel"] = "Sector";
                }
            }


            regionMasterDataSource = data.filter(
                function (x) {
                    if (user.SectorID == 3)
                        return x.ItemCode > 69999 && x.ItemCode < 80000;
                    else
                        return x.ItemCode = x.ItemCode
                });

            regionNonInheritedMasterDataSource = regionMasterDataSource;

            if (user.SectorNumber == 10 && isSubSectorApplicable) {
                regionDataSource = regionMasterDataSource.filter(function (item) {
                    return item.MenuLevel == "Region" && item.RegionSubSectorCode == subSectorCodeDD;
                });
            }
            else {
                regionDataSource = regionMasterDataSource.filter(function (item) {
                    return item.MenuLevel == "Region";
                });
            }

        } else {

        }

    }, null, true);

}

var itemcodes = "";

function saveProcedure(IsPublished) {
    if (globalChecked) {
        toastr.error("Have patience ,this process might takes few minutes");
    }
    if (IsPublished) {
        toastr.warning("The Items shall not be visible to Unit if not individually Configured and Published");
    }
    Utility.Loading();
    setTimeout(function () {

        var dropdownlist = $("#inputregion").data("kendoDropDownList");
        var regionID = dropdownlist.value();

        itemcodes = "";
        Pub_checkedItemCodes = checkedItemCodes;
        if (IsPublished) {
            $('#gridItem').data("kendoGrid").dataSource.dataFiltered().filter(function (item) {

                if ((checkedItemCodes.indexOf("1_" + item.ItemCode) != -1) || (checkedItemCodes.indexOf("0_" + item.ItemCode) != -1)) {
                    return item;
                }
                else if (item.MenuLevel == "Draft") {
                    Pub_checkedItemCodes.push("1_" + item.ItemCode);
                }
            });
            checkedItemCodes = Pub_checkedItemCodes;
            $("#btnPreview").attr('checked', false);;
        }

        itemcodes = checkedItemCodes.toString();
        if (itemcodes.length == 0) {
            alert("Please select at least one Item to continue");
            Utility.UnLoading();
            return;
        }
        var subsectorcode = "";
        if (user.SectorNumber == 10 && isSubSectorApplicable) {
            subsectorcode = $("#inputSubSector").val();
            if (subsectorcode == "" || subsectorcode == null) {
                toastr.success("Please select the Sub Sector to proceed.");
                Utility.UnLoading();
                return;
            }
        }

        var saveModel = {
            "Region_ID": regionID,
            "ItemCode": itemcodes,
            "IsActive": IsPublished ? 1 : 0,
            "Status": IsPublished ? 3 : 2,
            "SubSectorCode": subsectorcode
        }
        HttpClient.MakeSyncRequest(CookBookMasters.SaveSectorToRegionInheritance, function (result) {
            if (result == false) {

            }
            else {
                $(".k-overlay").hide();

                if (IsPublished) {
                    toastr.success("Selected Menu Items have been inherited. Do not forget to Publish the same");

                    Utility.UnLoading();
                    // updateSiteCost(saveModel);
                    checkedItemCodes = [];
                    checkedIds = {};
                    InheritedMap();

                } else {
                    Toast.fire({
                        type: 'success',
                        title: 'Draft saved successfully',
                        timer: 5000
                    });

                    var dropdownlist = $("#inputregion").data("kendoDropDownList");
                    var regionID = dropdownlist.value();
                    if (regionID == 0 || regionID == 'undefined') {
                        return;
                    }
                    InheritedRegionItem(regionID, 1);


                }

            }
        }, {
            model: saveModel
        });

        Utility.UnLoading();


    }, 2000);

}

function populateRegionMasterDropdown() {
    $("#inputregion").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "RegionID",
        dataSource: regionmasterdata,
        index: 0,
    });
    if (regionmasterdata.length == 1) {
        $("#btnGo").click();

    }
}

function populateSubSectorMasterDropdown() {
    $("#inputSubSector").kendoDropDownList({
        filter: "contains",
        dataTextField: "Name",
        dataValueField: "SubSectorCode",
        dataSource: subsectorMasterData,
        index: 0,
    });
}

function populateFoodProgramDropdown() {
    $("#inputfoodprogram").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: foodprogramdata,
        index: 0,
    });
}


function dropdownlist_selectMain(eid) {
    var ID = eid.sender.dataItem(eid.item).value;
    if (ID == 0) {
        localdata = menuitemdata;
    }
    else {
        localdata = menuitemdata.filter(function (item) {
            return item.FoodProgram_ID == ID;
        });
        localdata.unshift({ "ID": 0, "ItemName": "Select" });
    }
}

function dropdownlist_selectMain2(e) {
    eid = e;

}

function closeBelow(belowid, sameid, dishesid) {

    $("#" + belowid.id).toggle();
    if ($("#" + belowid.id).is(":visible")) {
        $("#" + sameid.id).find(".hideG").html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        $("#" + sameid.id).find(".displayG").hide();
        $("#" + sameid.id).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" });
        if (!flagCurated)
            $("#" + sameid.id).find(".k-widget").show();
    } else {
        $("#" + sameid.id).find(".hideG").html(" Show <i class= 'fas fa-angle-down' ></i>");
        var count = $("#" + belowid.id).find(".smallIngredient").length;
        $("#" + sameid.id).find(".displayG").show().html(" " + count + " Dish(s) Added");
        $("#" + sameid.id).find(".k-widget:last-child").hide();
        $("#" + sameid.id).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
    }
}

function bigRemove(val) {

    var sid = "inputqty" + val;
    var did = "inputdish" + val;
    var si = "subinputdish" + val;
    var di = "dish" + val;
    var ind = "inputdish" + val;
    var dishesid = "#inputdish" + val;
    if ($("#chk" + did).is(':checked')) {
        $("#" + sid).prop("disabled", false);
        $("#" + di).find(".hideG").show();//html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        $("#" + di).find(".displayG").hide();
        $("#" + si).show();
        $("#" + di).find(".k-widget").show();
        if (flagCurated) {
            $("#" + di).find(".k-widget:last-child").hide();
        }
        $("#" + di).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" });
    } else {
        $("#" + sid).prop("disabled", true);
        $("#" + di).find(".hideG").hide();//.html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        var count = $("#" + si).find(".smallIngredient").length;
        $("#" + di).find(".displayG").show().html(" " + count + " Dish(s)    ");
        $("#" + si).hide();
        $("#" + di).find(".k-widget:last-child").hide();
        $("#" + di).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
    }

}

function bigRemoveDefault(val) {

    var sid = "inputqty" + val;
    var did = "inputdish" + val;
    var si = "subinputdish" + val;
    var di = "dish" + val;
    var ind = "inputdish" + val;
    var dishesid = "#inputdish" + val;
    $("#chk" + did).prop("checked", false);
    $("#" + sid).prop("disabled", true);
    $("#" + di).find(".hideG").hide();
    var count = $("#" + si).find(".smallIngredient").length;
    $("#" + di).find(".displayG").show().html(" " + count + " Dish(s)    ");
    $("#" + si).hide();
    $("#" + di).find(".k-widget:last-child").hide();
    $("#" + di).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });


}

function smallRemove(sid, ingID) {

    if (!flagCurated) {
        $("#" + sid.id).remove();
        masterGroupList = masterGroupList.filter(function (item) {
            return item.ID !== ingID

        });
        ingredientObject = ingredientObject.filter(function (item) {
            return item.ID !== ingID

        });
        return;
    }
    if ($("#chksmall" + ingID).is(':checked')) {
        $("#inputsubqtysmall" + ingID).prop("disabled", false);
    } else {
        $("#inputsubqtysmall" + ingID).prop("disabled", true);
    }
}

function preSet() {
    var ID = eid.sender.dataItem(eid.item).ID;
    if (ID == 0 || ID == 'undefined') {
        return;
    } else {
        //  Utility.Page_Alert_Save("The current configuration will be overwritten with the Sector Configuraiton. Click 'Ok' to proceed", "Confirmation", "Ok", "Cancel",
        //    function () {
        masterGroupList = [];
        ingredientObject = [];
        $("#dishCategory").empty();
        mainSource(ID);
        //   },
        ///  function () {

        // }
        //  );

    }

}

function populateDishDropdown() {
    //var regiondishdata = [];
    //HttpClient.MakeSyncRequest(CookBookMasters.GetRegionDishMasterData, function (data) {
    //    regiondishdata = data;
    //}, { regionID: RegionID }, true);

    //HttpClient.MakeSyncRequest(CookBookMasters.GetDishList, function (data) {

    //    var dataSource = data;
    //    dishdata = [];
    //    for (var i = 0; i < dataSource.length; i++) {
    //        regiondishdata.forEach(function (yi) {
    //            if (yi.DishCode == dataSource[i].DishCode) {
    //                dataSource[i].CostPerKG = yi.CostPerKG;
    //            }
    //        });
    //    }
    //    dishdata.push({ "value": 0, "text": "Select", "price": 0 });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        dishdata = [];
    //        dishdata.push({ "value": 0, "text": "Select Dish(s)", "price": 0 });
    //        for (var i = 0; i < dataSource.length; i++) {
    //            dishdata.push({
    //                "value": dataSource[i].ID, "text": dataSource[i].Name, "DishCode": dataSource[i].DishCode, "DishCategoryID": dataSource[i].DishCategory_ID,
    //                "DishCategoryName": dataSource[i].DishCategoryName, "price": dataSource[i].CostPerKG, "alias": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
    //                "DietCategoryCode": dataSource[i].DietCategoryCode, "CuisineName": dataSource[i].CuisineName, "RecipeMapStatus": dataSource[i].RecipeMapStatus
    //            });
    //        }
    //    }
    //}, { regionID: RegionID }, true);
}


function populateDishDropdown1() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetRegionDishMasterData, function (data) {
        //  HttpClient.MakeSyncRequest(CookBookMasters.GetDishList, function (data) {
        var dataSource = data;

        dishdata = [];
        dishdata.push({ "value": 0, "text": "Select Dish(s)", "price": 0 });
        for (var i = 0; i < dataSource.length; i++) {
            dishdata.push({
                "value": dataSource[i].ID, "text": dataSource[i].Name, "DishCode": dataSource[i].DishCode, "DishCategoryID": dataSource[i].DishCategory_ID,
                "DishCategoryName": dataSource[i].DishCategoryName, "price": dataSource[i].CostPerKG, "alias": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
                "DietCategoryCode": dataSource[i].DietCategoryCode, "CuisineName": dataSource[i].CuisineName
            });
        }

    }, { regionID: RegionID }, true);
}


function InheritedMap() {
    Utility.Loading();
    if (user.SectorNumber == 10 && isSubSectorApplicable) {
        var $radios = $('input:radio[name=subsector]');
        $radios.filter('[value=SSR-001]').prop('checked', true);
        subSectorCodeDD = $("#inputSubSector").val();
        subSectorNameDD = $("#inputSubSector").data("kendoDropDownList").text();
        subSectorCode = $("input[name='subsector']:checked").val();
        if (subSectorCodeDD == 'SSR-002') {
            $("#inputGovoDiv").removeClass('d-inline');
            $("#inputGogoDiv").addClass('d-inline');
            $("#inputGogoDiv").show()
            $("#inputGovoDiv").hide();


        }
        else if (subSectorCodeDD == 'SSR-003') {
            $("#inputGogoDiv").removeClass('d-inline');
            $("#inputGovoDiv").addClass('d-inline');
            $("#inputGovoDiv").show();
            $("#inputGogoDiv").hide();
        }

    }
    var dropdownlist = $("#inputregion").data("kendoDropDownList");
    var regionID = dropdownlist.value();
    RegionID = regionID;
    if (regionID == 0 || regionID == 'undefined') {

        return;
    }
    var regionName = dropdownlist.text();

    $("#location").text(" | " + regionName);
    //populateDishDropdown();
    //InheritedRegionItem(regionID);

    //Krish 30-07-2022
    HttpClient.MakeSyncRequest(CookBookMasters.GetRegionItemDataList, function (result) {
        console.log(result);

        //populateDishDropdown
        //var regiondishdata = [];
        /*HttpClient.MakeSyncRequest(CookBookMasters.GetRegionDishMasterData, function (data) {*/
        //regiondishdata = result.RegionDishMasterData;
        /*}, { regionID: RegionID }, true);*/

        /*HttpClient.MakeSyncRequest(CookBookMasters.GetDishList, function (data) {*/

        //var dataSource = result.DishData;
        //    dishdata = [];
        //    for (var i = 0; i < dataSource.length; i++) {
        //        regiondishdata.forEach(function (yi) {
        //            if (yi.DishCode == dataSource[i].DishCode) {
        //                dataSource[i].CostPerKG = yi.CostPerKG;
        //            }
        //        });
        //    }
        //    dishdata.push({ "value": 0, "text": "Select", "price": 0 });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        dishdata = [];
        //        dishdata.push({ "value": 0, "text": "Select Dish(s)", "price": 0 });
        //        for (var i = 0; i < dataSource.length; i++) {
        //            dishdata.push({
        //                "value": dataSource[i].ID, "text": dataSource[i].Name, "DishCode": dataSource[i].DishCode, "DishCategoryID": dataSource[i].DishCategory_ID,
        //                "DishCategoryName": dataSource[i].DishCategoryName, "price": dataSource[i].CostPerKG, "alias": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
        //                "DietCategoryCode": dataSource[i].DietCategoryCode, "CuisineName": dataSource[i].CuisineName, "RecipeMapStatus": dataSource[i].RecipeMapStatus
        //            });
        //        }
        //    }
    /*}, { regionID: RegionID }, true);*/

        //InheritedRegionItem
        /*HttpClient.MakeSyncRequest(CookBookMasters.GetRegionItemInheritanceMappingDataList, function (result) {*/
        if (result.RegionItemInheritanceMappingData != null) {

            InheritedItem = result.RegionItemInheritanceMappingData;
                //modifiedDataSource();
            //modifiedDataSource
            checkedItemCodes = [];
            checkedIds = {};

            /*HttpClient.MakeSyncRequest(CookBookMasters.GetUniqueMappedItemCodesFromItem, function (data) {*/
            var dataSource = result.UniqueMappedItemCodesFromRegion;
                UniqueItemCodes = dataSource;
            /*}, null, false);*/
            /*HttpClient.MakeSyncRequest(CookBookMasters.GetItemDataList, function (data) {*/

            if (result.ItemData != null) {
                var data = result.ItemData;
                    data = data.filter(function (item) {
                        if (UniqueItemCodes.indexOf(item.ItemCode) != -1 && item.Status == 3)
                            return item;
                    });
                    if (InheritedItem.length > 0) {
                        for (var x of data) {
                            var fStatus = 1;
                            for (y of InheritedItem) {
                                x["RecipeMapStatus"] = y.RecipeMapStatus;
                                if (user.SectorNumber == 10 && isSubSectorApplicable) {
                                    if (x.ItemCode == y.ItemCode && y.SubSectorCode == subSectorCodeDD) {
                                        x.Status = y.Status;
                                        if (y.IsActive == 1) {
                                            x["MenuLevel"] = "Region";
                                        }
                                        else {
                                            x["MenuLevel"] = "Draft";
                                        }
                                        fStatus = 0;
                                        x["RegionSubSectorCode"] = y.SubSectorCode;
                                        x["SubSectorName"] = subSectorNameDD;
                                        break;
                                    }

                                }
                                else {
                                    if (x.ItemCode == y.ItemCode) {
                                        x.Status = y.Status;
                                        if (y.IsActive == 1) {
                                            x["MenuLevel"] = "Region";
                                        }
                                        else {
                                            x["MenuLevel"] = "Draft";
                                        }
                                        fStatus = 0;
                                        x["SubSectorName"] = "";
                                        break;
                                    }

                                }


                            }
                            if (fStatus) {
                                x["MenuLevel"] = "Sector";
                            }

                        }
                    } else {
                        for (var x of data) {
                            x["MenuLevel"] = "Sector";
                        }
                    }


                    regionMasterDataSource = data.filter(
                        function (x) {
                            if (user.SectorID == 3)
                                return x.ItemCode > 69999 && x.ItemCode < 80000;
                            else
                                return x.ItemCode = x.ItemCode
                        });

                    regionNonInheritedMasterDataSource = regionMasterDataSource;

                    if (user.SectorNumber == 10 && isSubSectorApplicable) {
                        regionDataSource = regionMasterDataSource.filter(function (item) {
                            return item.MenuLevel == "Region" && item.RegionSubSectorCode == subSectorCodeDD;
                        });
                    }
                    else {
                        regionDataSource = regionMasterDataSource.filter(function (item) {
                            return item.MenuLevel == "Region";
                        });
                    }

            } 

            /*}, null, true);*/

                //if (flag) {
                //    populateItemGrid();
                //    return;
                //}

                //populateItemGrid_IN();
            }
            else {

            }
        /*}, { regionID: regionID }, false);*/

    }, { isSectorUser: user.IsSectorUser, regionId: regionID,gridOnly:true }, false);

    $("#actionRegion").show();

    $("#btnSaveChk").hide();
    $("#btnDrft").hide();
    $("#previewchk").hide();
    $("#inheritItemChanges").hide();
    flagToggle = 0;
    toggleInherited();
    if (user.SectorNumber == 10 && isSubSectorApplicable) {
        $("#subSectorRadioBtn").hide();
    }

    Utility.UnLoading();
    //Krish 30-07-2022
    HttpClient.MakeSyncRequest(CookBookMasters.GetRegionItemDataList, function (result) {
        console.log(result);

        //populateDishDropdown
        var regiondishdata = [];
        /*HttpClient.MakeSyncRequest(CookBookMasters.GetRegionDishMasterData, function (data) {*/
        regiondishdata = result.RegionDishMasterData;
        /*}, { regionID: RegionID }, true);*/

        /*HttpClient.MakeSyncRequest(CookBookMasters.GetDishList, function (data) {*/

        var dataSource = result.DishData;
            dishdata = [];
            for (var i = 0; i < dataSource.length; i++) {
                regiondishdata.forEach(function (yi) {
                    if (yi.DishCode == dataSource[i].DishCode) {
                        dataSource[i].CostPerKG = yi.CostPerKG;
                    }
                });
            }
            dishdata.push({ "value": 0, "text": "Select", "price": 0 });
            for (var i = 0; i < dataSource.length; i++) {
                dishdata = [];
                dishdata.push({ "value": 0, "text": "Select Dish(s)", "price": 0 });
                for (var i = 0; i < dataSource.length; i++) {
                    dishdata.push({
                        "value": dataSource[i].ID, "text": dataSource[i].Name, "DishCode": dataSource[i].DishCode, "DishCategoryID": dataSource[i].DishCategory_ID,
                        "DishCategoryName": dataSource[i].DishCategoryName, "price": dataSource[i].CostPerKG, "alias": dataSource[i].Name, "UOMCode": dataSource[i].UOMCode,
                        "DietCategoryCode": dataSource[i].DietCategoryCode, "CuisineName": dataSource[i].CuisineName, "RecipeMapStatus": dataSource[i].RecipeMapStatus
                    });
                }
            }
        /*}, { regionID: RegionID }, true);*/

        //InheritedRegionItem
        /*HttpClient.MakeSyncRequest(CookBookMasters.GetRegionItemInheritanceMappingDataList, function (result) {*/
        //if (result.RegionItemInheritanceMappingData != null) {

        //    //InheritedItem = result.RegionItemInheritanceMappingData;
        //    //    //modifiedDataSource();
        //    ////modifiedDataSource
        //    //checkedItemCodes = [];
        //    //checkedIds = {};

        //    ///*HttpClient.MakeSyncRequest(CookBookMasters.GetUniqueMappedItemCodesFromItem, function (data) {*/
        //    //var dataSource = result.UniqueMappedItemCodesFromRegion;
        //    //    UniqueItemCodes = dataSource;
        //    ///*}, null, false);*/
            
        //}
        //else {

        //}
        /*}, { regionID: regionID }, false);*/

    }, { isSectorUser: user.IsSectorUser, regionId: regionID }, false);
    
}

// Building from main Source Start
function CreateDishCategoryOutline(ID, itemcode) {
    HttpClient.MakeSyncRequest(CookBookMasters.GetItemDishCategoryMappingDataList, function (data) {
        finalModelExitsDishCategory = data;
        var dataItems = [];
        var multiarray = [];
        currentMasterQtyList = [];
        dataItems1 = finalModelExitsDishCategory.filter(function (x) {

            if (multiarray.indexOf(parseInt(x.DishCategory_ID)) == -1) {
                multiarray.push(x.DishCategory_ID);
                return x;
            }

        });

        for (xi of dataItems1) {
            dishcategorydata.forEach(function (yi) {
                if (yi.code == xi.DishCategoryCode) {
                    dataItems.push({
                        "value": xi.DishCategory_ID, "text": yi.text, "stdqty": xi.StandardPortion, "ucode": xi.UOMCode, "wtuom": xi.WeightPerUOM,
                        "IsActive": xi.IsActive
                    })
                }
            });
        }

        for (item of dataItems) {
            var divid = "dish" + item.value;
            var dishesid = "inputdish" + item.value;
            var inputqty = "inputqty" + item.value;
            var subdishid = "subinputdish" + item.value;
            var uomid = "uom" + item.value;
            var wtqty = item.wtuom;//radha
            if (wtqty == null)
                wtqty = 0;
            var wtspanid = "wtspan" + uomid;
            var uomcode = item.ucode;
            var wtuom = "wtuom" + uomid;
            var dblock = item.IsActive ? "none" : "block";
            var dnone = item.IsActive ? "block" : "none";
            var isChecked = item.IsActive ? "checked" : "";

           // if (user.SectorNumber == "20") {
                $('#dishCategory').append('<div class="dish" ><div class="upper" id=' + divid + '><div class="upperText"  onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')">'
                    + item.text + '</div>'
                    + '<span onclick = "bigRemove(' + item.value + ')" ><span class="hideSt"><label style = "margin-top:7px;" class= "switch" >' +
                    '<input id="chk' + dishesid + '" type="checkbox" ' + isChecked + '><span class="slider round"></span></label></span ></span > <span class="hideSt">' +
                    '</span > <span class="hideG" style="display:' + dnone + '" onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')"> Hide <i class="fas fa-angle-up" ></i ></span >' +
                    '<span class="displayG" style="display:' + dblock + '"></span><span class="dllrgndish" style="float:right;margin-top:2px" id=' + dishesid + '></span></div >' +
                    '<div class="lower" style="display:' + dnone + '" id=' + subdishid + ' > </div ></div> ');
            //}
            //else {
            //    $('#dishCategory').append('<div class="dish" ><div class="upper" id=' + divid + '><div class="upperText"  onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')">'
            //        + item.text + '</div> <input id="' + inputqty +
            //        '"  class="qty"  type=" " value="0" min="0" onchange="updateQuantity(' + inputqty + ')" /> <div id = "' + uomid + '" class="colorG"></div ><span class="serv">Per Serving</span>'
            //        + '          <span id="' + wtspanid + '">' +
            //        '                <input id="' + wtuom + '" class="qtyT" type="" value="' + wtqty + '" min="0" onchange="updateWtPerUOMQuantity(' + wtuom + ')"/>' +
            //        '                <span class="wtlabel">wt. /pc (kg)</span>' +
            //        '          </span>'
            //        + '<span onclick = "bigRemove(' + item.value + ')" ><span class="hideSt"><label style = "margin-top:7px;" class= "switch" >' +
            //        '<input id="chk' + dishesid + '" type="checkbox" ' + isChecked + '><span class="slider round"></span></label></span ></span > <span class="hideSt">' +
            //        '</span > <span class="hideG" style="display:' + dnone + '" onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')"> Hide <i class="fas fa-angle-up" ></i ></span >' +
            //        '<span class="displayG" style="display:' + dblock + '"></span><span class="dllrgndish" style="float:right;margin-top:2px" id=' + dishesid + '></span></div >' +
            //        '<div class="lower" style="display:' + dnone + '" id=' + subdishid + ' > </div ></div> ');
            //}



            populateDishDataDropdown('#' + dishesid, item.text, "#" + subdishid);
            populateUOMDropdown('#' + uomid, item.ucode);
            //comeback
            currentMasterList.push({ "dropdownid": dishesid, "id": inputqty, "uomid": uomid, "uomcode": item.ucode });
            if (uomcode != "UOM-00003") {
                $("#" + wtspanid).hide();
            }
            if (uomcode == "UOM-00003") {

                $("#wtuom" + uomid).val(wtqty);
            }
            $("#" + inputqty).val(item.stdqty);
            currentMasterQtyList.push({ "id": inputqty, "qty": item.stdqty, "uomid": uomid, "uomcode": item.ucode });
            if (flagCurated) {
                $('#' + divid).find(".k-widget:last-child").hide();

            }
        }
        masterQty = currentMasterQtyList;
    }, { itemCode: itemcode }, false);
    CreateDishTiles(ID)
}

function CreateDishTiles(itemID) {
    HttpClient.MakeSyncRequest(CookBookMasters.GetItemDishMappingDataList, function (data) {
        ingredientObject = [];
        // var dataSource = data;
        var dataItems = [];
        finalModelExits = data;

        masterGroupList = [];
        //   currentMasterQtyList = [];

        //finalModelExist = ItemDisCategoryDish Mapping
        //build dataset
        for (itemRec of finalModelExits) {
            var localdishdata = [];

            localdishdata = dishdata.filter(function (item) {

                return item.DishCode == itemRec.DishCode;
            });
            if (localdishdata != null && localdishdata.length > 0) {
                var text = localdishdata[0].text;   //Dish Name
                var tooltip = localdishdata[0].alias;
                var dcat = localdishdata[0].DietCategoryCode;
                var idr = localdishdata[0].DishCategoryID;
                var smid = localdishdata[0].value;   //Dish ID
                var ingPrice = localdishdata[0].price; // Dishlevel Cost to be compued basis portion given
                var wtPerUOM = itemRec.WeightPerUOM;
                var dropdownid = "inputdish" + idr;   //Dish dropdown at DC level
                var subid = "#subinputdish" + idr;    //Container for all Dishes to keep the Dish tiles for a category
                var sid = "small" + smid;             // Dish Tile ID
                //  var inputqty = "inputqty" + idr;      // Dish Category Qty ID
                var ingID = smid;                     // DishID
                var qty = itemRec.StandardPortion;    //StandardPortion
                var subqty = itemRec.ContractPortion; //ContractPortion
                var costPerKg = localdishdata[0].price;
                var cuisine = localdishdata[0].CuisineName;
                var recipeMapStatus = localdishdata[0].RecipeMapStatus;
                if (qty == null) {
                    qty = 0;
                }
                ingredientObject.push({
                    "ID": ingID,
                    "Name": text,
                    "Price": 0
                })
                //pura scelaton of Dish Category Section + Dish Tile Section ka information
                masterGroupList.push({
                    "dropdownid": dropdownid,
                    "subid": subid,
                    "sid": sid,
                    "ingPrice": ingPrice,
                    "ingID": ingID,
                    "text": text,
                    "smallinput": "inputsubqty" + sid,
                    "ContractPortion": subqty,
                    "StandardPortion": itemRec.StandardPortion,
                    "UOMCode": itemRec.UOMCode,    //Dish's UOM Code
                    "DietCategoryCode": dcat,
                    "WeigthPerUOM": wtPerUOM,
                    "IsActive": itemRec.IsActive,
                    "CostPerKg": costPerKg,
                    "tooltip": tooltip,
                    "CuisineName": cuisine,
                    "RecipeMapStatus": recipeMapStatus,
                });

            }
            else {
                // bigRemove(ID);
            }
        }
        RecreateWholeExits();
    }, { itemID: itemID }, false);
    flagSectorIsActive = 1;
}


// Building from main Sources End
//Building From Region same source start

function CreateRegionDishCategoryOutline(ID, itemcode, regionID) {

    finalModelExitsDishCategory = [];
    HttpClient.MakeSyncRequest(CookBookMasters.GetRegionDishCategoryMappingDataList, function (data) {
        finalModelExitsDishCategory = data;
        // -- For Populate updated Dish Cost --//
        populateDishDropdown();
        if (finalModelExitsDishCategory.length < 1) {
            flagSector = 1;
            return;
        }
        var dataItems = [];
        var multiarray = [];
        currentMasterQtyList = [];
        dataItems1 = [];
        dataItems1 = finalModelExitsDishCategory.filter(function (x) {

            if (multiarray.indexOf(parseInt(x.DishCategory_ID)) == -1) {
                multiarray.push(x.DishCategory_ID);
                return x;
            }

        });

        for (xi of dataItems1) {
            dishcategorydata.forEach(function (yi) {
                if (yi.code == xi.DishCategoryCode) {
                    dataItems.push({ "value": xi.DishCategory_ID, "text": yi.text, "stdqty": xi.StandardPortion, "ucode": xi.UOMCode, "wtuom": xi.WeightPerUOM, "IsActive": xi.IsActive })
                }
            });
        }

        for (item of dataItems) {
            var divid = "dish" + item.value;
            var dishesid = "inputdish" + item.value;
            var inputqty = "inputqty" + item.value;
            var subdishid = "subinputdish" + item.value;
            var uomid = "uom" + item.value;
            var wtqty = item.wtuom;//radha
            if (wtqty == null)
                wtqty = 0;
            var wtspanid = "wtspan" + uomid;
            var uomcode = item.ucode;
            var wtuom = "wtuom" + uomid;
            var dblock = item.IsActive ? "none" : "block";
            var dnone = item.IsActive ? "block" : "none";
            var isChecked = item.IsActive ? "checked" : "";

           // if (user.SectorNumber == "20") {
                $('#dishCategory').append('<div class="dish" ><div class="upper"  id=' + divid + '><div class="upperText"  onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')">'
                    + item.text + '</div>' +
                    ' <span onclick="bigRemove(' + item.value + ')"><span class="hideSt"><label style = "margin-top:7px;" class= "switch" ><input id="chk' + dishesid + '" type="checkbox" ' + isChecked + '><span class="slider round"></span></label></span></span><span class="hideSt"></span >' +
                    '<span class="hideG" style="display:' + dnone + '" onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')"> Hide <i class="fas fa-angle-up" ></i ></span >' +
                    '<span style="display:' + dblock + '" class="displayG"></span><span class="dllrgndish" style="float:right;margin-top:2px" id=' + dishesid + '></span></div >' +
                    '<div style="display:' + dnone + '" class="lower" id=' + subdishid + ' > </div ></div > ');
            //}
            //else {
            //    $('#dishCategory').append('<div class="dish" ><div class="upper"  id=' + divid + '><div class="upperText"  onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')">'
            //        + item.text + '</div> <input id="' + inputqty +
            //        '"  class="qty"  type=" " value="0" min="0" onchange="updateQuantity(' + inputqty + ')" oninput="validateNumber(this);"/> <div id = "' + uomid + '" class="colorG"></div ><span class="serv">Per Serving</span>'
            //        + '          <span id="' + wtspanid + '">' +
            //        '                <input id="' + wtuom + '" class="qtyT" oninput="validateNumber(this);" type="Number" value="' + wtqty + '" min="0" onchange="updateWtPerUOMQuantity(' + wtuom + ')"/>' +
            //        '                <span class="wtlabel">wt. /pc (kg)</span>' +
            //        '          </span>' +
            //        ' <span onclick="bigRemove(' + item.value + ')"><span class="hideSt"><label style = "margin-top:7px;" class= "switch" ><input id="chk' + dishesid + '" type="checkbox" ' + isChecked + '><span class="slider round"></span></label></span></span><span class="hideSt"></span >' +
            //        '<span class="hideG" style="display:' + dnone + '" onclick="closeBelow(' + subdishid + ',' + divid + ',' + dishesid + ')"> Hide <i class="fas fa-angle-up" ></i ></span >' +
            //        '<span style="display:' + dblock + '" class="displayG"></span><span class="dllrgndish" style="float:right;margin-top:2px" id=' + dishesid + '></span></div >' +
            //        '<div style="display:' + dnone + '" class="lower" id=' + subdishid + ' > </div ></div > ');
            //}



            populateDishDataDropdown('#' + dishesid, item.text, "#" + subdishid);
            populateUOMDropdown('#' + uomid, item.ucode);
            //comeback
            currentMasterList.push({ "dropdownid": dishesid, "id": inputqty, "uomid": uomid, "uomcode": item.ucode });
            if (uomcode != "UOM-00003") {
                $("#" + wtspanid).hide();
            }
            if (uomcode == "UOM-00003") {

                $("#wtuom" + uomid).val(wtqty);
            }
            $("#" + inputqty).val(item.stdqty);
            currentMasterQtyList.push({ "id": inputqty, "qty": item.stdqty, "uomid": uomid, "uomcode": item.ucode });
            if (flagCurated) {
                $(".dllrgndish").hide();

            }
        }
        masterQty = currentMasterQtyList;
        CreateRegionDishTiles(itemcode, regionID);
    }, { regionID: regionID, itemCode: itemcode }, false);

}

function CreateRegionDishTiles(itemID, regionID) {
    HttpClient.MakeSyncRequest(CookBookMasters.GetRegionItemDishMappingDataList, function (data) {
        ingredientObject = [];
        // var dataSource = data;
        var dataItems = [];
        finalModelExits = data;

        masterGroupList = [];
        //   currentMasterQtyList = [];

        //finalModelExist = ItemDisCategoryDish Mapping
        //build dataset
        for (itemRec of finalModelExits) {
            var localdishdata = [];

            localdishdata = dishdata.filter(function (item) {

                return item.DishCode == itemRec.DishCode;
            });
            if (localdishdata != null && localdishdata.length > 0) {
                var text = localdishdata[0].text;   //Dish Name
                var tooltip = localdishdata[0].alias;
                var dcat = localdishdata[0].DietCategoryCode;
                var idr = localdishdata[0].DishCategoryID;
                var dcatCode = localdishdata[0].DishCategoryCode;
                var smid = localdishdata[0].value;   //Dish ID
                var ingPrice = localdishdata[0].price; // Dishlevel Cost to be compued basis portion given
                var wtPerUOM = itemRec.WeightPerUOM;
                var dropdownid = "inputdish" + idr;   //Dish dropdown at DC level
                var subid = "#subinputdish" + idr;    //Container for all Dishes to keep the Dish tiles for a category
                var sid = "small" + smid;             // Dish Tile ID
                //  var inputqty = "inputqty" + idr;      // Dish Category Qty ID
                var ingID = smid;                     // DishID
                var qty = itemRec.StandardPortion;    //StandardPortion
                var subqty = itemRec.ContractPortion; //ContractPortion
                var costPerKg = localdishdata[0].price;
                var cuisine = localdishdata[0].CuisineName;
                var recipeMapStatus = localdishdata[0].RecipeMapStatus;
                if (qty == null) {
                    qty = 0;
                }
                ingredientObject.push({
                    "ID": ingID,
                    "Name": text,
                    "Price": ingPrice,
                    "CostPerKg": costPerKg
                });
                //pura scelaton of Dish Category Section + Dish Tile Section ka information
                masterGroupList.push({
                    "dropdownid": dropdownid,
                    "subid": subid,
                    "sid": sid,
                    "ingPrice": ingPrice,
                    "ingID": ingID,
                    "text": text,
                    "smallinput": "inputsubqty" + sid,
                    "ContractPortion": subqty,
                    "StandardPortion": itemRec.StandardPortion,
                    "UOMCode": itemRec.UOMCode,    //Dish's UOM Code
                    "DietCategoryCode": dcat,
                    "WeigthPerUOM": wtPerUOM,
                    "IsActive": itemRec.IsActive,
                    "CostPerKg": costPerKg,
                    "tooltip": tooltip,
                    "CuisineName": cuisine,
                    "DishCategory_ID": idr,
                    "RecipeMapStatus": recipeMapStatus
                });
            }
        }
        RecreateWholeExits();
    }, { itemID: itemID, regionID: regionID, subSectorCode: subSectorCodeDD }, false);
    flagSectorIsActive = 0;
}



//Building from same source End

// Added Populate UOM
function populateUOMDropdown(uomid, uomvalue) {

    $(uomid).kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: uomdata,
        index: 0,
    });
    var dropdownlist = $(uomid).data("kendoDropDownList");
    if (dropdownlist != undefined) {
        if (uomvalue == -1)
            dropdownlist.select(1);//value("Select");
        else
            dropdownlist.value(uomvalue);

        dropdownlist.bind("change", uomdropdownlist_select);
    }

}



function uomdropdownlist_select(e) {

    var ID = e.sender.dataItem(e.item).value;
    if (ID == 0) {
        return;
    }


    if (ID == "UOM-00003")
        $("#wtspan" + e.sender.element.context.id).show();
    else
        $("#wtspan" + e.sender.element.context.id).hide();
    console.log(e.sender.element.context.id);
    var idnum = e.sender.element.context.id.substring(3);
    var dishFlag = e.sender.element.context.id.search("small");
    var x = e.sender.element.context.id;
    var i = 0;
    if (dishFlag > 0) {


        var smallId = x.substring(7);
        calculateCostPerServing(smallId);
        for (item of masterGroupList) {
            if (item.sid == smallId) {
                item.UOMCode = ID;//$("#" + x).val();
                //  item.WeigthPerUOM = $("#wtspan" + e.sender.element.context.id).val();
                masterGroupList[i].UOMCode = ID;
                //   masterGroupList[i].WeigthPerUOM = $("#wtspan" + e.sender.element.context.id).val();
            }

            i++;
        }
        return
    }

    for (item of finalModelExitsDishCategory) {

        if (idnum == item.DishCategory_ID) {
            finalModelExitsDishCategory[i].UOMCode = ID;
            item.UOMCode = ID;
        }
        i++;
    }

    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {

        var sm = subItem[i].id;

        $("#dishuom" + sm).data('kendoDropDownList').value(ID);

        if (ID == "UOM-00003")
            $("#wtspandishuom" + sm).show();
        else
            $("#wtspandishuom" + sm).hide();
        calculateCostPerServing(sm);
    }
    i = 0;
    for (item of masterGroupList) {

        if (item.dropdownid == "inputdish" + idnum) {
            item.UOMCode = ID;
            item.WeigthPerUOM = $("#wtuom" + e.sender.element.context.id).val();
        }
        i++;
    }

}

//function uomdropdownlist_select(e) {

//    var ID = e.sender.dataItem(e.item).value;
//    if (ID == 0) {
//        return;
//    }

//    var idnum = e.sender.element.context.id.substring(3);
//    var dishFlag = e.sender.element.context.id.search("small");
//    if (ID == "UOM-00003")
//        $("#wtspan" + e.sender.element.context.id).show();
//    else
//        $("#wtspan" + e.sender.element.context.id).hide();

//    if (dishFlag > 0) {


//        var sm = e.sender.element.context.id.substring(7);
//        calculateCostPerServing(sm);
//        return
//    }
//    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
//    var len = subItem.length;
//    for (var i = 0; i < len; i++) {

//        var sm = subItem[i].id;

//        $("#dishuom" + sm).data('kendoDropDownList').value(ID);

//        if (ID == "UOM-00003")
//            $("#wtspandishuom" + sm).show();
//        else
//            $("#wtspandishuom" + sm).hide();

//        calculateCostPerServing(sm);

//    }
//    ////Master to be updated
//    //for (x of idstobeupdated) {
//    //    var subinID = x;

//    //    for (item of masterGroupList) {
//    //        if (item.smallinput == subinID) {
//    //            item.ContractPortion = $("#" + subinID).val();
//    //        }
//    //    }
//    //}

//}

//Ended


function back() {

    Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
        function () {
            $("#configMaster").show();
            $("#mapMaster").hide();
            $("#topMsg").show();
            $("#topHeading").text("Regional Menu Configuration");
        },
        function () {
        }
    );
}
function apply() {
     Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Reset Confirmation", "Yes", "No",
        function () {
            $("#dishCategory").empty();
            var dropdownlist = $("#inputregion").data("kendoDropDownList");
            var regionID = dropdownlist.value();
            $("#configMaster").hide();
            $("#mapMaster").show();
            sameSource(ID, regionID);
            Utility.UnLoading();
        },
        function () {
            Utility.UnLoading();
        }
    );
}



function populateDishDataDropdown(dishid, dishcategory, subdishid) {
    //Filter for data based on category
    var localdishdata = [];

    if (selItemDietCategory == "DTC-00003") {
        localdishdata = dishdata.filter(item => item.DishCategoryName == dishcategory && item.DietCategoryCode == selItemDietCategory);
    }
    else if (selItemDietCategory == "DTC-00001") {
        localdishdata = dishdata.filter(item => item.DishCategoryName == dishcategory && (item.DietCategoryCode == selItemDietCategory || item.DietCategoryCode == "DTC-00003"));
    }
    else {
        localdishdata = dishdata.filter(item => item.DishCategoryName == dishcategory);
    }
    localdishdata.unshift({
        "value": 0, "text": "Select All", "DishCategoryName": dishcategory, "price": 0, "DietCategoryCode": selItemDietCategory
    });
    localdishdata.unshift({
        "value": 0, "text": "Select Dish(s)", "DishCategoryName": "Select", "price": 0, "tooltip": ""
    });
    $(dishid).kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: localdishdata,
        autoClose: false,
        preventCloseCnt: 0,
        index: 0,
        close: function (e) {
            if (this.preventCloseCnt > 0) {//now the next time something tries to close it will work ( unless it's a change )
                e.preventDefault();
                this.preventCloseCnt--;
            } else {
                var dropdownlist = $(dishid).data("kendoDropDownList");
                dropdownlist.select(0);

            }
        }
    });
    var dropdownlist = $(dishid).data("kendoDropDownList");
    dropdownlist.bind("change", dropdownlist_select);
    dropdownlist.bind("open", dropdownlist_open);

    if (localdishdata == null || localdishdata == "" || localdishdata.length == 0) {

    }
}

function dropdownlist_open(e) {
    if (e.sender.dataSource._pristineTotal == 1) {
        toastr.error("No Dish exists for this dish category.");
    }
}

function dropdownlist_select(e) {
    if (e.sender.dataItem(e.item).text == "Select All") {
        selectAllDish(e);
    }
    else {

        this.preventCloseCnt = 1;
        if (e.sender.dataItem(e.item).value == 0) {
            return;
        }
        var item = e.sender.dataItem(e.item)
        var mid = e.sender.element[0].id;
        var subid = "#sub" + mid;
        var dropdownid = e.sender.element[0].id;
        var text = item.text;
        var tooltip = item.alias;
        var ingID = item.value;
        var uomcode = $("#uom" + dropdownid.substring(9)).val();
        var ingPrice = item.price;
        var costPerKg = item.price;
        var len = ingredientObject.length;
        var flag = 1;
        var subqty = $("#inputqty" + dropdownid.substring(9)).val();
        var dtcat = item.DietCategoryCode;
        for (var i = 0; i < len; i++) {
            if (ingredientObject[i].ID == ingID) {
                flag = 0;
                break;
            }
        }
        if (flag == 1) {
            ingredientObject.push({
                "ID": ingID,
                "Name": text,
                "Price": ingPrice,
                "CostPerKg": costPerKg
            })
        }
        else {
            return;
        }
        var wtqty = 0;
        var sid = "small" + ingID;
        var subInput = 'inputsubqty' + sid;
        var uomid = 'dishuom' + sid;
        var costpsid = 'costpsid' + sid;
        var costpkid = 'costpkid' + sid;
        var wtspanid = 'wtspan' + uomid;
        var cuisinestring = item.CuisineName == null ? "" : '<span class="cuisine-heading">' + item.CuisineName + '</span>';
        var recipeMapString = '<i style="color: green;font-size: 10px;" class="fas fa-check"></i>';
        if (item.RecipeMapStatus == "Recipe Not Mapped") {
            recipeMapString = '<i style="color: red;font-size: 10px;" class="fas fa-times"></i>';
        }
        //if (user.SectorNumber == "20") {
            $(subid)
                .prepend(
                    '<div class="smallIngredient" id=' + sid + '>' +
                    '     <div class="supper" title="' + tooltip + '">' + text + '</div>' +
                    '      <div class="slower"><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 3px!important;">' + recipeMapString + '</span>' + cuisinestring + '</div><input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
                    '     <div class="dishtile">' +
                    '          <span style="float:right;margin-top:-1px" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
                    '                 <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
                    '          </span >' +
                    '     </div>' +
                    '</div>');
        //}
        //else {
        //    $(subid)
        //        .prepend(
        //            '<div class="smallIngredient" id=' + sid + '>' +
        //            '     <div class="supper" title="' + tooltip + '">' + text + '</div>' +
        //            '      <div class="slower"><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 3px!important;">' + recipeMapString + '</span>' + cuisinestring + '<span  id="' + costpsid + '">Cost / Serving:  ' + Utility.AddCurrencySymbol(ingPrice, 2) + '</span></div><input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
        //            '     <div class="dishtile">' +
        //            '          <input id="inputsubqty' + sid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + subqty + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
        //            '          <span id="' + uomid + '" class="colorGS"></span>' +
        //            '          <span id="' + wtspanid + '">' +
        //            '                <input id="wtuom' + uomid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtqty + '" min="0" onchange="updateSubUOMInput(' + subInput + ',' + sid + ')" />' +
        //            '                <span class="wtlabel">wt. /pc (kg)</span>' +
        //            '          </span>' +
        //            '          <span style="float:right;margin-top:-1px" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
        //            '                 <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
        //            '          </span >' +
        //            '     </div>' +
        //            '</div>');
        //}

        if (uomcode != "UOM-00003") {
            $("#" + wtspanid).hide();
        }

        if (dtcat == "DTC-00001") {
            $('#' + sid).addClass("yellowborder");
        } else if (dtcat == "DTC-00002") {
            $('#' + sid).addClass("redborder");
        } else if (dtcat == "DTC-00003") {
            $('#' + sid).addClass("greenborder");
        }
        populateUOMDropdown('#' + uomid, uomcode);
        calculateCostPerServing(sid);
        //List containg
        masterGroupList.push({
            "dropdownid": dropdownid,
            "subid": subid,
            "sid": sid,
            "ingPrice": ingPrice,
            "costPerKg": costPerKg,
            "ingID": ingID,
            "text": text,
            "tooltip": tooltip,
            "smallinput": "inputsubqty" + sid,
            "ContractPortion": subqty,
            "UOMCode": uomcode,
            "DietCategoryCode": dtcat,
            "IsActive": true
        });

    }

}


function selectAllDish(e) {
    var selItem = e.sender.dataItem(e.item)
    var localdishdata = [];

    if (selItem.DietCategoryCode == "DTC-00003") {
        localdishdata = dishdata.filter(item => item.DishCategoryName == selItem.DishCategoryName && item.DietCategoryCode == selItem.DietCategoryCode);
    }
    else if (selItem.DietCategoryCode == "DTC-00001") {
        localdishdata = dishdata.filter(item => item.DishCategoryName == selItem.DishCategoryName && (item.DietCategoryCode == selItem.DietCategoryCode || item.DietCategoryCode == "DTC-00003"));
    }
    else {
        localdishdata = dishdata.filter(item => item.DishCategoryName == selItem.DishCategoryName);
    }
    localdishdata.unshift({
        "value": 0, "text": "Select All", "DishCategoryName": "Select", "price": 0
    });
    localdishdata.unshift({
        "value": 0, "text": "Select Dish(s)", "DishCategoryName": "Select", "price": 0
    });

    if (localdishdata != null && localdishdata.length > 0) {
        localdishdata.forEach(function (item, index) {
            if (item.value > 0) {
                // var item = e.sender.dataItem(e.item)
                var mid = e.sender.element[0].id;
                var subid = "#sub" + mid;
                var dropdownid = e.sender.element[0].id;
                var text = item.text;
                var tooltip = item.alias;
                var ingID = item.value;
                var uomcode = $("#uom" + dropdownid.substring(9)).val();
                var ingPrice = item.price;
                var costPerKg = item.price;
                var len = ingredientObject.length;
                var flag = 1;
                var subqty = $("#inputqty" + dropdownid.substring(9)).val();
                var dtcat = item.DietCategoryCode;
                for (var i = 0; i < len; i++) {
                    if (ingredientObject[i].ID == ingID) {
                        flag = 0;
                        break;
                    }
                }
                if (flag == 1) {
                    ingredientObject.push({
                        "ID": ingID,
                        "Name": text,
                        "Price": ingPrice,
                        "CostPerKg": costPerKg
                    })
                }
                else {
                    return;
                }
                var wtqty = 0;
                var sid = "small" + ingID;
                var subInput = 'inputsubqty' + sid;
                var uomid = 'dishuom' + sid;
                var costpsid = 'costpsid' + sid;
                var costpkid = 'costpkid' + sid;
                var wtspanid = 'wtspan' + uomid;
                var cuisinestring = item.CuisineName == null ? "" : '<span class="cuisine-heading">' + item.CuisineName + '</span>';
                var recipeMapString = '<i style="color: green;font-size: 10px;" class="fas fa-check"></i>';
                if (item.RecipeMapStatus == "Recipe Not Mapped") {
                    recipeMapString = '<i style="color: red;font-size: 10px;" class="fas fa-times"></i>';
                }
                //if (user.SectorNumber == "20") {
                    $(subid)
                        .prepend(
                            '<div class="smallIngredient" id=' + sid + '>' +
                            '     <div class="supper" title="' + tooltip + '">' + text + '</div>' +
                            '      <div class="slower"><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 3px!important;">' + recipeMapString + '</span>' + cuisinestring + '</div><input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
                            '     <div class="dishtile">' +
                            '          <span style="float:right;margin-top:-1px" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
                            '                 <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
                            '          </span >' +
                            '     </div>' +
                            '</div>');
                //}
                //else {
                //    $(subid)
                //        .prepend(
                //            '<div class="smallIngredient" id=' + sid + '>' +
                //            '     <div class="supper" title="' + tooltip + '">' + text + '</div>' +
                //            '      <div class="slower"><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 3px!important;">' + recipeMapString + '</span>' + cuisinestring + '<span  id="' + costpsid + '">Cost / Serving:  ' + Utility.AddCurrencySymbol(ingPrice, 2) + '</span></div><input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
                //            '     <div class="dishtile">' +
                //            '          <input id="inputsubqty' + sid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + subqty + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
                //            '          <span id="' + uomid + '" class="colorGS"></span>' +
                //            '          <span id="' + wtspanid + '">' +
                //            '                <input id="wtuom' + uomid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtqty + '" min="0" onchange="updateSubUOMInput(' + subInput + ',' + sid + ')" />' +
                //            '                <span class="wtlabel">wt. /pc (kg)</span>' +
                //            '          </span>' +
                //            '          <span style="float:right;margin-top:-1px" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
                //            '                 <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
                //            '          </span >' +
                //            '     </div>' +
                //            '</div>');
                //}

                if (uomcode != "UOM-00003") {
                    $("#" + wtspanid).hide();
                }

                if (dtcat == "DTC-00001") {
                    $('#' + sid).addClass("yellowborder");
                } else if (dtcat == "DTC-00002") {
                    $('#' + sid).addClass("redborder");
                } else if (dtcat == "DTC-00003") {
                    $('#' + sid).addClass("greenborder");
                }
                populateUOMDropdown('#' + uomid, uomcode);
                calculateCostPerServing(sid);
                //List containg
                masterGroupList.push({
                    "dropdownid": dropdownid,
                    "subid": subid,
                    "sid": sid,
                    "ingPrice": ingPrice,
                    "costPerKg": costPerKg,
                    "ingID": ingID,
                    "text": text,
                    "tooltip": tooltip,
                    "smallinput": "inputsubqty" + sid,
                    "ContractPortion": subqty,
                    "UOMCode": uomcode,
                    "DietCategoryCode": dtcat,
                    "IsActive": true
                });
            }
        });
    }
}

function sameSource(ID, regionID) {
    multiarray = [];
    var flagMS = 0;
    flagSectorIsActive = 0;
    currentMasterList = [];
    if (regionID == 0) {
        mainSource(ID);
        return;
    }
    flagSector = 0;
    CreateRegionDishCategoryOutline(ID, tr.ItemCode, regionID);

    if (flagSector) {
        mainSource(ID);//sector item
        flagSector = 0;
        return;
    }

    $("#sourceName").text("Regional Menu Configuration");

}


function mainSource(ID) {
    $("#sourceName").text("Sector Configuration");
    multiarray = [];
    //console.log("Calling Main Source");
    currentMasterList = [];
    CreateDishCategoryOutline(ID, tr.ItemCode);

}

function updateQuantity(iqty) {
    var qty = $("#" + iqty.id).val();
    var flag = 1;
    for (sqty of masterQty) {
        if (sqty.id == iqty.id) {
            sqty.qty = qty;
            ////console.log(sqty);
            flag = 0;
        }
    }
    if (flag) {
        masterQty.push({
            "id": iqty.id,
            "qty": qty
        });
    }

    //reflection update small input
    //loop array
    var idnum = iqty.id.substring(8);
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {
        var sm = subItem[i].id;
        $("#inputsubqty" + sm).val(qty);
    }

}



function updateQuantitySub(iqty) {
    //Lower Updation

}

function RecreateQuantity() {
}
function SaveModel() {
    var dropdownlist = $("#inputregion").data("kendoDropDownList");
    var regionID = dropdownlist.value();
    var wtPerUOM = 0;
    var UOMCode = "";
    flagAlert = 0;
    flagDishStandardQtyAlert = 0;
    finalModel = [];
    var ContractPortion = 0;
    var uniqueArray = [...new Set(currentMasterQtyList)]
    currentMasterList = removeDuplicateObjectFromArray(currentMasterList, 'id');
    var dishcategory_id_coll = [];

    for (m1 of uniqueArray) {
        var idnumeric = m1.id.substr(8);
        dishcategory_id_coll.push(idnumeric);
    }

    var uniqueDishCategoryID = [...new Set(dishcategory_id_coll)]
    finalModelDishCategory = [];
    for (m1 of uniqueDishCategoryID) {
        var idnumeric = m1;
        var StandardPortion = $("#inputqty" + idnumeric).val();
        var wtPerUOMMain = $("#wtuomuom" + idnumeric).val();
        //loop array
        var len = $("#subinputdish" + idnumeric).find(".smallIngredient").length;
        var ismainActive = $("#chkinputdish" + idnumeric).is(':checked');
        if (len == 0 && ismainActive) {
            flagAlert = 1;
            $("#inputqty" + idnumeric).addClass("redHighlight");
            return;
        } else {
            $("#inputqty" + idnumeric).removeClass("redHighlight");
        }
        var InnerTile = StandardPortion;//Check for Inside Tile non zero

        var coll = $("#subinputdish" + idnumeric).find(".smallIngredient");
        var idAlready = 0;
        for (i = 0; i < len; i++) {
            dishid = coll[i].id.substr(5);

            var localdishdata = [];

            localdishdata = dishdata.filter(function (item) {
                return item.value == dishid;
            });

            var dishcode = localdishdata[0].DishCode;
            //alert(dishcode);

            ContractPortion = $("#inputsubqtysmall" + dishid).val();
            if (ContractPortion > 0) {
                InnerTile = ContractPortion;
            }
            //if (user.SectorNumber !== "20") {
            //    UOMCode = $("#dishuomsmall" + dishid).data("kendoDropDownList").value();
            //    if (UOMCode == "UOM-00003") {
            //        wtPerUOM = $("#wtuomdishuomsmall" + dishid).val();
            //    }
            //}
            for (item of finalModelExits) {
                idAlready = 0;
                if (dishcode == item.DishCode && regionID == item.Region_ID) {
                    idAlready = item.ID;
                    break;
                }

            }

            var isActive = false;
            if (ismainActive) {
                isActive = 1;// $("#chksmall" + dishid).is(':checked')
                if (flagCurated) {
                    isActive = $("#chksmall" + dishid).is(':checked');
                }
            }
            //if (flagSectorIsActive) {
            //    isActive = 1;
            //}
            finalModel.push({
                "ID": idAlready,
                "Item_ID": itemObject.ID,
                "ItemCode": itemObject.Code,
                "Dish_ID": dishid,
                "DishCode": dishcode,
                "StandardPortion": StandardPortion,
                "ContractPortion": ContractPortion,
                "IsActive": isActive,
                "CreatedBy": user.UserId,
                "CreatedOn": Utility.CurrentDate(),
                "Region_ID": regionID,
                "UOMCode": UOMCode,
                "WeightPerUOM": wtPerUOM,
                "Status": isPublishBtnClick ? 3 : 2,
                "SubSectorCode": $("#inputSubSector").val()
            })
        }
        if (ismainActive && StandardPortion == 0 && InnerTile == 0) {
            flagDishStandardQtyAlert = 1;
            $("#inputqty" + idnumeric).addClass("redHighlight");
            return;
        } else {
            $("#inputqty" + idnumeric).removeClass("redHighlight");
        }
        var StandardPortion = $("#inputqty" + m1).val();
        var DCUOMCode = "";
        //if (user.SectorNumber !== "20") {
        //    DCUOMCode = $("#uom" + m1).data("kendoDropDownList").value();
        //}
        //console.log(m1);

        var dcatCode = dishcategorydata.filter(function (x) {

            return x.value == m1;
        });
        //active Inactive check
        var isActive = $("#chkinputdish" + m1).is(':checked');
        finalModelDishCategory.push({
            "ID": 0,
            "Item_ID": itemObject.ID,
            "Region_ID": regionID,
            "ItemCode": itemObject.Code,
            "DishCategory_ID": m1,
            "DishCategoryCode": dcatCode[0].code,
            "StandardPortion": StandardPortion,
            "IsActive": isActive,
            "CreatedBy": user.UserId,
            "CreatedOn": Utility.CurrentDate(),
            "UOMCode": DCUOMCode,
            "WeightPerUOM": wtPerUOMMain,
            "SubSectorCode": $("#inputSubSector").val()
        })

    }


    SaveRegionDishCategory(finalModelDishCategory);
    ///  flagSectorIsActive = 0;
}

function SaveRegionDishCategory(finalModelDishCategory) {
    //first check then remove return;
    HttpClient.MakeRequest(CookBookMasters.SaveRegionDishCategoryMappingDataList, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again.");
        }
        else {
            if (!finalModel.length) {
                $(".k-overlay").hide();
                toastr.success("Records have been updated successfully");
            }
        }
    }, {
        model: finalModelDishCategory
    }, true);

}

function RecreateWholeExits() {
    for (item1 of masterGroupList) {
        var wtqty = 0;
        var dtcat = item1.DietCategoryCode;//"DTC-00001";
        var subid = item1.subid;
        var sid = item1.sid;
        var ingPrice = item1.ingPrice;
        var costPerKg = item1.CostPerKg;
        var ingID = item1.ingID;
        var text = item1.text;
        var uomcode = item1.UOMCode;
        var tooltip = item1.tooltip;
        var subqty = item1.ContractPortion;
        var subInput = "inputsubqty" + sid;
        var uomid = 'dishuom' + item1.sid;
        var costpsid = 'costpsid' + sid;
        var costpkid = 'costpkid' + sid;
        var wtspanid = 'wtspan' + uomid;
        var wtPerUOM = item1.WeigthPerUOM;
        var cuisinestring = item1.CuisineName == null ? "" : '<span class="cuisine-heading">' + item1.CuisineName + '</span>';
        var recipeMapString = '<i style="color: green;font-size: 10px;" class="fas fa-check"></i>';
        if (item1.RecipeMapStatus == "Recipe Not Mapped") {
            recipeMapString = '<i style="color: red;font-size: 10px;" class="fas fa-times"></i>';
        }
        //if (user.SectorNumber == "20") {
            if (!flagCurated) {
                $(subid)
                    .prepend(
                        '<div class="smallIngredient" id=' + sid + ' >' +
                        '     <div class="supper" title="' + tooltip + '">' + text + '</div>' +
                        '      <div class="slower"><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 3px!important;">' + recipeMapString + '</span>' + cuisinestring + '</div><input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
                        '     <div class="dishtile">' +
                        '          <span style="float:right;margin-top:-1px" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
                        '                 <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
                        '          </span >' +
                        '     </div>' +
                        '</div>');
            } else {
                $(subid)
                    .prepend(
                        '<div class="smallIngredient" id=' + sid + '>' +
                        '     <div class="supper" title="' + tooltip + '">' + text + '</div>' +
                        '      <div class="slower"><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 3px!important;">' + recipeMapString + '</span>' + cuisinestring + '</span></div><input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
                        '     <div class="dishtile">' +
                        '          <span style="float:right;margin-top:-1px" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
                        '                  <span class="smallGreen"><label style = "margin-top:5px;" class= "switch" ><input  type="checkbox" id="chk' + sid + '"checked="checked"><span class="slider round"></span></label></span></span>' +
                        '          </span >' +
                        '     </div>' +
                        '</div>');
            }
     //   }
        //else {
        //    if (!flagCurated) {
        //        $(subid)
        //            .prepend(
        //                '<div class="smallIngredient" id=' + sid + ' >' +
        //                '     <div class="supper" title="' + tooltip + '">' + text + '</div>' +
        //                '      <div class="slower"><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 3px!important;">' + recipeMapString + '</span>' + cuisinestring + '<span  id="' + costpsid + '">Cost / Serving:  ' + Utility.AddCurrencySymbol(ingPrice, 2) + '</span></div><input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
        //                '     <div class="dishtile">' +
        //                '          <input id="inputsubqty' + sid + '" class="qtyS" type="number" oninput="validateNumber(this);" value="' + subqty + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
        //                '          <span id="' + uomid + '" class="colorGS"></span>' +
        //                '          <span id="' + wtspanid + '">' +
        //                '                <input id="wtuom' + uomid + '" class="qtyS" type="Number" oninput="validateNumber(this);" value="' + wtqty + '" min="0" onchange="updateSubUOMInput(' + subInput + ',' + sid + ')" />' +
        //                '                <span class="wtlabel">wt./pc(kg)</span>' +
        //                '          </span>' +
        //                '          <span style="float:right;margin-top:-1px" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
        //                '                 <span class="smallGreen"><i class="far fa-times-circle"></i></span>' +
        //                '          </span >' +
        //                '     </div>' +
        //                '</div>');
        //    } else {
        //        $(subid)
        //            .prepend(
        //                '<div class="smallIngredient" id=' + sid + '>' +
        //                '     <div class="supper" title="' + tooltip + '">' + text + '</div>' +
        //                '      <div class="slower"><span>Recipe:</span><span  style="margin-left: 3px;margin-right: 3px!important;">' + recipeMapString + '</span>' + cuisinestring + '</span><span  id="' + costpsid + '">Cost / Serving:  ' + Utility.AddCurrencySymbol(ingPrice, 2) + '</span></div><input type="hidden" id="' + costpkid + '" value="' + costPerKg + '"/>' +
        //                '     <div class="dishtile">' +
        //                '          <input id="inputsubqty' + sid + '" class="qtyS" type="" value="' + subqty + '" min="0" onchange="updateSubInput(' + subInput + ',' + sid + ')" />' +
        //                '          <span id="' + uomid + '" class="colorGS"></span>' +
        //                '          <span id="' + wtspanid + '">' +
        //                '                <input id="wtuom' + uomid + '" class="qtyS" type="" value="' + wtqty + '" min="0" onchange="updateSubUOMInput(' + subInput + ',' + sid + ')" />' +
        //                '                <span class="wtlabel">wt./pc(kg)</span>' +
        //                '          </span>' +
        //                '          <span style="float:right;margin-top:-1px" onclick="smallRemove(' + sid + ',' + ingID + ')">' +
        //                '                  <span class="smallGreen"><label style = "margin-top:5px;" class= "switch" ><input  type="checkbox" id="chk' + sid + '"checked="checked"><span class="slider round"></span></label></span></span>' +
        //                '          </span >' +
        //                '     </div>' +
        //                '</div>');
        //    }
        //}



        if (uomcode != "UOM-00003") {
            $("#" + wtspanid).hide();
        }
        if (uomcode == "UOM-00003") {

            $("#wtuom" + uomid).val(wtPerUOM);
        }

        if (dtcat == "DTC-00001") {
            $('#' + sid).addClass("yellowborder");
        } else if (dtcat == "DTC-00002") {
            $('#' + sid).addClass("redborder");
        } else if (dtcat == "DTC-00003") {
            $('#' + sid).addClass("greenborder");
        }
        populateUOMDropdown('#' + uomid, uomcode);
        $('#' + uomid).val(uomcode);

        calculateCostPerServing(sid);
    }

    RecreateQuantity();

    disabledDishCategory();
}

function disabledDishCategory() {
    if (dataItems1 != null && dataItems1.length > 0) {
        for (xi of dataItems1) {
            var isDishExists = masterGroupList.filter(m => m.DishCategory_ID == xi.DishCategory_ID);
            if (isDishExists == null || isDishExists == "" || isDishExists.length == 0) {
                bigRemoveDefault(xi.DishCategory_ID);
            }
        }

    }
}

function updateSubInput(inID, sid) {
    //Master to be updated
    var subinID = inID.id;
    for (item of masterGroupList) {
        if (item.smallinput == subinID) {
            item.ContractPortion = $("#" + subinID).val();
            calculateCostPerServing(sid.id);
        }
    }
}

function updateSubUOMInput(inID, sid) {
    //Master to be updated
    var subinID = inID.id;
    for (item of masterGroupList) {
        if (item.smallinput == subinID) {
            item.ContractPortion = $("#" + subinID).val();
            calculateCostPerServing(sid.id);
        }
    }
}

function updateQuantity(iqty) {
    var qty = $("#" + iqty.id).val();
    var flag = 1;
    for (sqty of masterQty) {
        if (sqty.id == iqty.id) {
            sqty.qty = qty;
            ////console.log(sqty);
            flag = 0;
        }
    }
    if (flag) {
        masterQty.push({
            "id": iqty.id,
            "qty": qty
        });
    }

    //reflection update small input
    //loop array
    var idnum = iqty.id.substring(8);
    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {
        var sm = subItem[i].id;
        $("#inputsubqty" + sm).val(qty);
        calculateCostPerServing(sm);
    }

}


function calculateCostPerServing(sid) {
    var costPerKg = $("#costpkid" + sid).val();
    var qty = $("#inputsubqty" + sid).val();
    var uom = $("#dishuom" + sid).val();
    var uomQty = $("#wtuom" + sid).val();
    if (uom == 'UOM-00001') {
        $("#costpsid" + sid).text("Cost / Serving: " + Utility.AddCurrencySymbol(costPerKg * qty, 2));
    }
    else if (uom == 'UOM-00003') {
        var uomQty = $("#wtuomdishuom" + sid).val();
        $("#costpsid" + sid).text("Cost / Serving: " + Utility.AddCurrencySymbol(costPerKg * qty * uomQty, 2));
    }
}
function UpdateDishCateInactiveUpdate() {
    fmodel = [];
    var Dishfinalmodel = finalModelExitsDishCategory;
    for (let x of Dishfinalmodel) {
        if (!$("#chkinputdish" + x.DishCategory_ID).prop("checked")) {
            x.IsActive = 0;
            fmodel.push(x);
        }

    }

    if (fmodel.length > 0) {
        Utility.Loading();
        HttpClient.MakeSyncRequest(CookBookMasters.mydishcateinactiveupdate, function (result) {

        }, {
            model: fmodel

        }, true);

    }
}
function onSave() {
    Utility.Loading();
    setTimeout(function () {
        isPublishBtnClick = false;
        var subsectorcode = "";
        if (user.SectorNumber == 10 && isSubSectorApplicable) {
            subsectorcode = $("#inputSubSector").val();
            if (!subsectorcode) {
                toastr.success("Please select the Sub Sector to proceed.");
                Utility.UnLoading();
                return;
            }
        }
        var dropdownlist = $("#inputregion").data("kendoDropDownList");
        var regionID = dropdownlist.value();
        if (!regionID) {
            toastr.error("Please select the Region to proceed");
            Utility.UnLoading();
            finalModel = [];
            return;
        }

        RecreateQuantity();
        SaveModel();
        //if (flagDishStandardQtyAlert) {
        //    toastr.error("While publishing a Regional Menu, Portion Size under Dish Categories cannot be left blank");
        //    finalModel = [];
        //    return;
        //}
        //if (flagAlert) {
        //    toastr.error("While publishing a Regional Menu, Dish Configuration under Dish Categories cannot be left blank");
        //    finalModel = [];
        //    return;
        //}
        if (finalModel.length > 0) {
            Utility.Loading();
            HttpClient.MakeSyncRequest(CookBookMasters.SaveRegionItemDishMappingDataList, function (result) {
                if (result == false) {
                    toastr.error("Some error occured, please try again.");
                    Utility.UnLoading();
                }
                else {
                    $(".k-overlay").hide();

                    // toastr.success("Changes have been saved. Click Publish to make the Item available for Units");

                    Toast.fire({
                        type: 'success',
                        title: 'Changes have been saved. Click Publish to make the Item available for Units',
                        timer: 5000
                    });

                    $("#sourceName").text("Regional Menu Configuration");
                    Utility.UnLoading();
                    InheritedRegionItem(regionID);

                }
            }, {
                model: finalModel

            }, true);

        }
        $("#topMsg").hide();

    }, 1000);
}

function onPublish() {
    Utility.Loading();
    setTimeout(function () {
        isPublishBtnClick = true;

        //isPublishBtnClick = false;
        var subsectorcode = "";
        if (user.SectorNumber == 10 && isSubSectorApplicable) {
            subsectorcode = $("#inputSubSector").val();
            if (!subsectorcode) {
                toastr.success("Please select the Sub Sector to proceed.");
                Utility.UnLoading();
                return;
            }
        }
        var dropdownlist = $("#inputregion").data("kendoDropDownList");
        var regionID = dropdownlist.value();
        if (!regionID) {
            toastr.error("Please select the Region to proceed");
            Utility.UnLoading();
            finalModel = [];
            return;
        }

        RecreateQuantity();
        SaveModel();
        UpdateDishCateInactiveUpdate();
        if (flagDishStandardQtyAlert) {
            toastr.error("While publishing a Regional Menu, Portion Size under Dish Categories cannot be left blank");
            finalModel = [];
            Utility.UnLoading();
            return;
        }
        if (flagAlert) {
            toastr.error("While publishing a Regional Menu, Dish Configuration under Dish Categories cannot be left blank");
            finalModel = [];
            Utility.UnLoading();
            return;
        }
        console.log("final Model");
        console.log(finalModel);
        // return;
        if (finalModel.length > 0) {
            HttpClient.MakeSyncRequest(CookBookMasters.SaveRegionItemDishMappingDataList, function (result) {
                if (result.result == false) {
                    toastr.error("Some error occured, please try again.");
                }
                else {
                    $(".k-overlay").hide();
                    //Toast.fire({
                    //    type: 'success',
                    //    title: 'Menu Item-Dish mapping complete',
                    //    timer: 5000
                    //});
                    toastr.success("Menu Item-Dish mapping complete");
                    //if (result.isCostUpdate && isPublishBtnClick) {
                    //    setTimeout(function () {
                    //        toastr.success("Listed items cost updation process started successfully");
                    //    }, 2000);
                    //}
                    $("#sourceName").text("Regional Menu Configuration");
                    InheritedRegionItem(regionID);

                }
            }, {
                model: finalModel

            }, true);
        }

        $("#configMaster").show();
        $("#mapMaster").hide();
        $("#topHeading").text("Regional Menu Configuration");

    }, 1000);
}

function updateSiteCost(saveModel) {
    toastr.success("Item - Dish cost updating process is starting");
    HttpClient.MakeRequest(CookBookMasters.UpdateCostSectorToRegionInheritance, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again.");
        }
        else {
        }
    }, {
        model: saveModel
    }, false);
}

function filterdropdownlist_select(e) {
    if (e.sender.dataItem(e.item).value == 0) {
        return;
    }
    var item = e.sender.dataItem(e.item)
    if (item.value == "Sector") {
        $("#btnRecipeCancel").css("display", "none");
        $("#btnReset").text('Reset')
    } else if (item.value == "Region") {
        $("#btnRecipeCancel").text("Reset");
        $("#btnRecipeCancel").css("display", "inline");
        $("#btnReset").text('Restore Sector Configuration')
    } else {
        $("#btnRecipeCancel").text("Reset");
        $("#btnRecipeCancel").css("display", "inline");
        $("#btnReset").text('Restore Sector Configuration')
    }
    handleClick(item);
}

function handleClick(rad) {

    if (rad.value == "All") {
        HttpClient.MakeRequest(CookBookMasters.GetItemDataList, function (result) {
            if (result != null) {
                var dataSource = result;
                //console.log("menuitemdata");
                //console.log(result);
                menuitemdata = [];
                menuitemdata = result;
                menuitemdata.unshift({ "ID": 0, "ItemName": "Select" })
                localdata = menuitemdata;

            }
            else {
                //console.log("Some Issue");
            }
        }, null, true);
    } else if (rad.value == "Region") {
        var dropdownlist = $("#inputregion").data("kendoDropDownList");
        var regionID = dropdownlist.value();
        if (regionID == 0) {
            // alert("Select Region First");
            Utility.Page_Alert_Warning("Please select the Region to proceed", "Validation", "Ok",
                function () {

                }
            );

            return;
        }
        HttpClient.MakeRequest(CookBookMasters.GetMasterRegionItemDishMappingDataList, function (data) {


            itemsids = [];
            for (m1 of data) {
                itemsids.push(m1.ItemCode)
            }

            menuitemdata = allMenuData.filter(function (x) {

                if (itemsids.indexOf(x.ItemCode) != -1)
                    return x;
            });
            menuitemdata.unshift({ "ID": 0, "ItemName": "Select" })
            configmasterdata = menuitemdata;
            localdata = menuitemdata;


        }, { regionID: regionID }, true);

    } else {

        HttpClient.MakeRequest(CookBookMasters.GetItemDataList, function (result) {
            if (result != null) {
                var dataSource = result;

                menuitemdata = [];
                menuitemdata = result;
                menuitemdata.unshift({ "ID": 0, "ItemName": "Select" })
                localdata = menuitemdata;


            }
            else {
                //console.log("Some Issue");
            }
        }, null, true);
        var dropdownlist = $("#inputregion").data("kendoDropDownList");
        var regionID = dropdownlist.value();
        if (regionID == 0) {
            //  alert("Select Region First");
            Utility.Page_Alert_Warning("Please select the Region to proceed", "Validation", "Ok",
                function () {

                }
            );
            return;
        }
        HttpClient.MakeRequest(CookBookMasters.GetMasterRegionItemDishMappingDataList, function (data) {


            itemsids = [];
            for (m1 of data) {
                itemsids.push(m1.ItemCode)
            }

            menuitemdata = allMenuData.filter(function (x) {

                if (itemsids.indexOf(x.ItemCode) != -1)
                    return x;
            });
            configmasterdata = menuitemdata;
            localdata = menuitemdata;
            //populateMenuItemDropdown();

        }, { regionID: regionID }, true);


        //Sub A-B
        menuitemdata = allMenuData.filter(function (x) {
            // checking second array does not contain element "x"
            if (itemsids.indexOf(x.ItemCode) == -1)
                return x;

        });
        localdata = menuitemdata;
        unconfigMasterData = localdata;


    }
    //var dropdownlistfp = $("#inputfoodprogram").data("kendoDropDownList");
    //dropdownlistfp.select(0);
}


$(document).ready(function () {
     // inheritChanges();
    if (user.SectorNumber != "30") {
        $("#fpname").hide();
        $("#fptype").hide();
        $("#pipeSpan").hide();
    }

    topText = $("#location").text();
    $("#btnExport").click(function (e) {
        var grid = $("#gridItem").data("kendoGrid");
        grid.saveAsExcel();
    });
    $("#collapseIcon").click(function () {
        collapseAll();
    });

    $("#expandIcon").click(function () {
        expandAll();
    });

    $('#myInput').on('input', function (e) {
        if (globalChecked) {
            $('#header-chb').attr("checked", false).triggerHandler('change');
        }
        var grid = $('#gridItem').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "MenuLevel" || x.field == "ItemCode" || x.field == "ItemName" || x.field == "VisualCategoryName" || x.field == "FoodProgramName" || x.field == "DietCategoryName"
                    || x.field == "HSNCode" || x.field == "CGST" || x.field == "SGST" || x.field == "CESS" || x.field == "Status" || x.field == "SubSectorName") {
                    // || x.field == "RecipeMapStatus") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        if (x.field == "Status") {
                            var pendingString = "pending";
                            var savedString = "saved";
                            var mappedString = "mapped";
                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "3";
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });


    $('#myInputPreview').on('input', function (e) {
        var grid = $('#gridPreview').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "ItemCode" || x.field == "ItemName" || x.field == "VisualCategoryName" || x.field == "FoodProgramName" || x.field == "DietCategoryName" || x.field == "HSNCode" || x.field == "CGST" || x.field == "SGST" || x.field == "CESS") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
    $("#windowEdit").kendoWindow({
        modal: true,
        width: "620px",
        height: "560px",
        title: "Preview Changes",
        actions: ["Close"],
        visible: false,
        animation: false
    });
    $("#prevDiv").hide();
    loadFormData();

    $('input[type=radio][name=subsector]').change(function () {
        subSectorCode = this.value;
        populateItemGridByRadioButton();
    });
    Utility.UnLoading();
});

function populateItemGridByRadioButton() {
    if (subSectorCode == 'SSR-001') {
        if (!isRegionInheritedCase) {
            regionDataSource = regionMasterDataSource.filter(function (item) {
                return item.MenuLevel == "Region";
            });
            populateItemGrid_IN();
        }
        else {
            regionNonInheritedMasterDataSource = regionMasterDataSource.filter(function (item) {
                return item.SubSectorCode == "SSR-001";
            });
            populateItemGrid();
        }
    }
    else {
        if (!isRegionInheritedCase) {
            regionDataSource = regionMasterDataSource.filter(function (item) {
                return item.MenuLevel == "Region" && item.SubSectorCode == subSectorCode;
            });
            populateItemGrid_IN();
        }
        else {
            regionNonInheritedMasterDataSource = regionMasterDataSource.filter(function (item) {
                return item.SubSectorCode == subSectorCodeDD;
            });
            populateItemGrid();
        }
    }
}

function expandAll() {
    var rows = $(".upper");
    $.each(rows, function () {
        var upperid = (this).id;
        var lowerid = (this).nextElementSibling.id;
        $("#" + upperid).find(".hideG").html(" Hide <i class= 'fas fa-angle-up' ></i> ");
        $("#" + lowerid).show();
        $("#" + upperid).find(".k-widget").show();
        $("#" + upperid).find(".displayG").hide()
        $("#" + upperid).css({ "border-bottom-left-radius": "0px", "border-bottom-right-radius": "0px" })
        $("#expandIcon").css({ "color": "#3f7990" });
        $("#collapseIcon").css({ "color": "#76b4b0" });
        if (flagCurated) {
            $(".dllrgndish").hide();

        }
    });
}

function collapseAll() {
    var rows = $(".upper");
    rows.each(function () {
        var upperid = (this).id;
        var lowerid = (this).nextElementSibling.id;
        $("#" + upperid).find(".hideG").html(" Show <i class= 'fas fa-angle-down' ></i>");
        var count = $("#" + lowerid).find(".smallIngredient").length;
        $("#" + lowerid).hide();
        $("#" + upperid).find(".displayG").show().html(" " + count + " Dish(s) Added");
        $("#" + upperid).find(".k-widget:last-child").hide();
        $("#" + upperid).css({ "border-bottom-left-radius": "5px", "border-bottom-right-radius": "5px" });
        $("#collapseIcon").css({ "color": "#3f7990" });
        $("#expandIcon").css({ "color": "#76b4b0" });
    });
}

function populateItemGrid() {

    //regionNonInheritedMasterDataSource = regionMasterDataSource.filter(function (item) {
    //    return item.SubSectorCode == subSectorCodeDD;
    //});

    var gridVariable = $("#gridItem");
    gridVariable.html("");
    grid = gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,
        columns: [

            {
                field: "ItemCode", title: "Item Code", width: "140px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                }
            },
            {
                field: "ItemName", title: "Item Name", attributes: {
                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                },
                template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
            },

            {
                field: "FoodProgramName", title: "Food Program", width: "160px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "VisualCategoryName", title: "Visual Category", width: "155px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MenuLevel", title: "Menu Level", width: "135px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {

                template: "<label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px' ><input type='checkbox' class='checkbox'><span class='checkmarkgrid'></span></label>",
                headerTemplate: "<span style='margin-right : 5px;'>Include All</span><label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px' ><input type='checkbox' id='header-chb' class='checkbox1'><span class='checkmarkgrid'></span></label>",

                width: "135px", title: "Include",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center; vertical-align:middle"
                }
            },
            {
                field: "DietCategoryName", title: "", width: "1px", attributes: {

                    style: "text-align: center; font-weight:normal;"
                },
                headerAttributes: {
                    style: "text-align: center; width:1px; display:none;"
                },
                template: '<span class="itemname"></span>',
            }
        ],
        dataSource: {
            data: regionNonInheritedMasterDataSource,

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ItemCode: { type: "string" },
                        ItemName: { type: "string" },
                        DietCategoryName: { type: "string" },
                        VisualCategoryName: { type: "string" },
                        FoodProgramName: { type: "string" },
                        SubSectorName: { type: "string" },
                        HSNCode: { type: "string" },
                        CGST: { type: "string" },
                        SGST: { type: "string" },
                        CESS: { type: "string" },
                        MenuLevel: { type: "string" },


                    }

                }
            },
            pageSize: 150
        },
        columnResize: function (e) {
            //var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (globalChecked) {
                    $('#header-chb').attr("checked", "checked");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");
                    if (checkedIds[view[i].id] == false) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".checkbox")
                            .removeAttr("checked");
                        continue;

                    }
                    continue;
                }
                if (checkedIds[view[i].id]) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");
                    //.addClass("k-state-selected")
                }
                if (checkedIds[view[i].id] == false) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .removeAttr("checked");
                    continue;
                    //.addClass("k-state-selected")
                }

                if (view[i].MenuLevel == "Region") {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");//.attr("disabled", "disabled");
                } else {
                    if (view[i].MenuLevel == "Draft") {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".checkbox")
                            .attr("checked", "checked");//.attr("disabled", "disabled");
                    }
                    //k-grid-MapDishes
                    //this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                    //    .find(".k-grid-MapDishes")//color grey
                    //    .removeClass("k-grid-MapDishes");
                    // $(this).removeClass("k-grid-MapDishes");
                }
            }
            //var grid = this;
            //var rows = grid.items();

            //$(rows).each(function (e) {
            //    var row = this;
            //    var dataItem = grid.dataItem(row);

            //    if (dataItem.Status ==  "Region") {
            //        grid.select(row);
            //    }
            //});

        },

    });
    //bind click event to the checkbox
    grid = grid.data("kendoGrid")
    if (user.SectorNumber != "30") {

        grid.hideColumn("FoodProgramName");
        grid.hideColumn("VisualCategoryName");
    }

    grid.table.on("click", ".checkbox", selectRow);
    $('#header-chb').change(function (ev) {
        var checked = ev.target.checked;
        globalChecked = checked;
        if (checked) {
            checkedItemCodes = $('#gridItem').data("kendoGrid").dataSource.dataFiltered().filter(item => item.MenuLevel == "Sector").map(item => "1_" + item.ItemCode);//Ram

        } else {

            checkedItemCodes = [];
            checkedIds = {};
            populateItemGrid();
            return;
        }
        $('.checkbox').each(function (idx, item) {
            if (checked) {

                if (!($('.checkbox')[idx].checked)) {
                    //  $('.checkbox').find(".checkbox").attr("checked", "checked");
                    $(item).click();
                }
            }
        });

    });

}
//bind click event to the checkbox
$("#showSelection").bind("click", function () {
    var checked = [];
    if (globalChecked)
        return;
    for (var i in checkedIds) {
        if (checkedIds[i]) {
            checked.push(i);
        }
    }


});

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        td = $(this).closest("td"),
        row = $(this).closest("tr"),
        grid = $("#gridItem").data("kendoGrid"),
        dataItem = grid.dataItem(row);
    // if (globalChecked)
    checkedIds[dataItem.id] = checked;
    if (checked) {
        var filteredAry = checkedItemCodes.filter(function (e) { return e != "0_" + dataItem.ItemCode && e != "1_" + dataItem.ItemCode });
        checkedItemCodes = filteredAry;
        if (dataItem.MenuLevel == "Region")
            return;
        checkedItemCodes.push("1_" + dataItem.ItemCode);
    }
    else {
        var filteredAry = checkedItemCodes.filter(function (e) { return e != "1_" + dataItem.ItemCode && e != "0_" + dataItem.ItemCode });
        checkedItemCodes = filteredAry;
        if (dataItem.MenuLevel == "Sector")
            return;
        checkedItemCodes.push("0_" + dataItem.ItemCode);
        $('#header-chb').attr("checked", false);
    }


}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                .addClass("k-state-selected")
                .find(".checkbox")
                .attr("checked", "checked");
        }
    }
}

function toggleInherited() {

    if (!flagToggle) {
        isRegionInheritedCase = false;
        $("#toggleText").text("Add More");
        $("#btnSaveChk").hide();
        $("#btnDrft").hide();
        $("#previewchk").hide();
        $("#btnInherited").addClass("btn-info");
        $("#btnInherited").removeClass("btn-tool");
        console.log('isRegionInheritedCase')
        populateItemGrid_IN()

        ////for google case
        //if (subSectorCode == "") {
        //    populateItemGrid_IN()
        //}
        //else {
        //    populateItemGridByRadioButton();
        //}

        flagToggle = 1;
        // $("#btnExport").show();
        //  $("#topMsg").show();
        if (user.SectorNumber == 10 && isSubSectorApplicable) {
            $("#subSectorRadioBtn").hide();
            $("#subSectorRadioBtn").removeClass('d-inline');
        }
    }
    else {
        
        isRegionInheritedCase = true;
        $("#topMsg").hide();
        $("#btnExport").hide();
        $("#myInput").show();
        $("#previewchk").show();
        $("#toggleText").text("Back");
        $("#btnInherited").removeClass("btn-info");
        $("#btnInherited").addClass("btn-tool");
        //if (user.UserRoleId === 1) {
        //    $("#btnSaveChk").show();
        //    $("#btnDrft").show();
        //} else {
        //    $("#btnSaveChk").hide();
        //    $("#btnDrft").hide();
        //}
        $("#btnSaveChk").show();
        $("#btnDrft").show();
        //for google case

        if (subSectorCode == "") {
            populateItemGrid();
        }
        else {
            populateItemGridByRadioButton();
        }
        flagToggle = 0;
        $('#header-chb').attr("checked", false).triggerHandler('change');
        checkedItemCodes = [];
        if (user.SectorNumber == 10 && isSubSectorApplicable) {
            $("#subSectorRadioBtn").show();
            $("#subSectorRadioBtn").addClass('d-inline');
        }
    }
}
function populateItemGrid_IN() {

    //if (user.SectorNumber == 10 && isSubSectorApplicable) {
    //    regionDataSource = regionMasterDataSource.filter(function (item) {
    //        return item.MenuLevel == "Region" && item.SubSectorCode == subSectorCode;
    //    });
    //}

    if (regionDataSource.length == 0) {
        $("#toggleText").text("Build Menu");
        $("#myInput").hide();
        $("#btnSaveChk").hide();
        $("#btnDrft").hide();
        $("#previewchk").hide();
        $("#topMsg").hide();
        $("#btnExport").hide();
    }
    else {
        $("#myInput").show();
        $("#toggleText").text("Add More");
        $("#topMsg").show();
        $("#btnExport").show();

    }

    var gridVariable = $("#gridItem");
    gridVariable.html("");
    grid = gridVariable.kendoGrid({
        //selectable: "cell",
        noRecords: {
            template: "No Records Available"
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,
        columns: [

            {
                field: "ItemCode", title: "Item Code", width: "95px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                }
            },
            {
                field: "ItemName", title: "Item Name", attributes: {

                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                },
                template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
            },
            {
                field: "SubSectorName", title: "Sub Sector", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "FoodProgramName", title: "Food Program", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "VisualCategoryName", title: "Visual Category", width: "155px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MenuLevel", title: "Menu Level", width: "135px", attributes: {

                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            //{
            //    field: "RecipeMapStatus", title: "Recipe Status", width: "130px", attributes: {
            //        class: "mycustomstatusr",
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    headerAttributes: {

            //        style: "text-align: center"
            //    },
            //},
            {
                field: "Status", title: "Status", width: "100px", attributes: {
                    class: "mycustomstatus",
                    style: "text-align: center; font-weight:normal"
                },
                template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Mapped#}#',
                headerAttributes: {

                    style: "text-align: center"
                },
            },
            {
                field: "DietCategoryName", title: "", width: "1px", attributes: {

                    style: "text-align: center; font-weight:normal;"
                },
                headerAttributes: {
                    style: "text-align: center; width:1px; display:none;"
                },
                template: '<span class="itemname"></span>',
            },
            {
                field: "Edit", title: "Action", width: "190px",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center; vertical-align:middle"
                },
                command: [

                    {
                        title: 'Configure for Region',
                        name: 'Map Dishes',

                        click: function (e) {
                            // console.log("hare");
                            //console.log("Hare Krishna");
                            //alert("Ram Ram");
                            //controlCall = 1;
                            $("#topMsg").hide();
                            controlCall++;
                            if (controlCall % 2 == 0) {
                                Utility.Loading();
                                setTimeout(function () {
                                    var gridObj = $("#gridItem").data("kendoGrid");
                                    tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    console.log("tr");
                                    console.log(tr);
                                    itemObject.ItemName = tr.ItemName;
                                    itemObject.ID = tr.ID;
                                    ID = tr.ID;
                                    itemObject.Code = tr.ItemCode;
                                    itemObject.ItemPrice = 100;
                                    itemObject.DietCategorID = tr.DietCategory_ID;
                                    selItemDietCategory = tr.DietCategoryCode;
                                    itemObject.ImagePath = '../ItemImages/' + '/' + tr.ImageName;
                                    $("#itemCode").text(tr.ItemCode);
                                    $("#itemName").text(tr.ItemName);
                                    $("#fpname").text(tr.FoodProgramName);
                                    $("#fptype").text(tr.Foo);
                                    $("#vcat").text(tr.VisualCategoryName);

                                    $("#iname").text(tr.ItemName + " (Item Code: " + tr.ItemCode + ")");
                                    if (tr.ConceptType2Code == 'CT2-00001' || tr.ConceptType2Code == 'CT2-00003') {
                                        $("#fptype").text("Curated");
                                        flagCurated = 1;
                                    } else {
                                        $("#fptype").text("Non-Curated");
                                        flagCurated = 0;
                                    }
                                    if (user.SectorNumber == "10") {
                                        var ssubsectorText = " Subsector: " + $("#inputSubSector").data("kendoDropDownList").text() + " | ";
                                        $("#ssector").text(ssubsectorText);
                                        $("#ssector").show();
                                    }
                                    else {
                                        $("#ssector").hide();
                                    }
                                    if (tr.DietCategory_ID == 3) {
                                        $("#itemDietCategoryStatus").removeClass("statusNonVeg statusEgg");
                                        $("#itemDietCategory").removeClass("squareNonVeg squareEgg");
                                        $("#itemDietCategoryStatus").addClass("statusVeg");
                                        $("#itemDietCategory").addClass("squareVeg")
                                    } else if (tr.DietCategory_ID == 2) {
                                        $("#itemDietCategoryStatus").removeClass("statusVeg statusEgg");
                                        $("#itemDietCategory").removeClass("squareVeg squareEgg");
                                        $("#itemDietCategory").addClass("squareNonVeg")
                                        $("#itemDietCategoryStatus").addClass("statusNonVeg")
                                    } else {
                                        $("#itemDietCategoryStatus").removeClass("statusNonVeg statusVeg");
                                        $("#itemDietCategory").removeClass("squareNonVeg squareVeg");
                                        $("#itemDietCategory").addClass("squareEgg")
                                        $("#itemDietCategoryStatus").addClass("statusEgg")

                                    }
                                    $("#itemPrice").text(itemObject.ItemPrice);
                                    $("#ItemMaster").hide();
                                    $("#ConfigMaster").show();
                                    $("#imageMenuItem").attr('src', itemObject.ImagePath);
                                    $("#dishCategory").empty();
                                    var dropdownlist = $("#inputregion").data("kendoDropDownList");
                                    var regionID = dropdownlist.value();
                                    $("#configMaster").hide();
                                    $("#mapMaster").show();

                                    sameSource(ID, regionID);//un
                                    $("#btnSubmitRecipe").css('display', 'inline');
                                    $("#btnPublishRecipe").css('display', 'inline');

                                    //if (user.UserRoleId === 1) {
                                    //    $("#btnSubmitRecipe").css('display', 'inline');
                                    //    $("#btnPublishRecipe").css('display', 'inline');
                                    //} else {
                                    //    $("#btnSubmitRecipe").css('display', 'none');
                                    //    $("#btnPublishRecipe").css("display", "none");
                                    //}
                                    Utility.UnLoading();
                                }, 20)

                            }

                        }
                    }
                ]

            }
        ],
        dataSource: {
            data: regionDataSource,

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ItemCode: { type: "string" },
                        ItemName: { type: "string" },
                        DietCategoryName: { type: "string" },
                        VisualCategoryName: { type: "string" },
                        FoodProgramName: { type: "string" },
                        HSNCode: { type: "string" },
                        CGST: { type: "string" },
                        SGST: { type: "string" },
                        CESS: { type: "string" },
                        Status: { type: "string" },
                        MenuLevel: { type: "string" },
                        SubSectorName: { type: "string" },
                        RecipeMapStatus: { type: "string" },
                    }

                }
            },
            pageSize: 150
        },
        ////columnResize: function (e) {
        ////    //var grid = gridVariable.data("kendoGrid");
        ////    e.preventDefault();
        ////},
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {

            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (view[i].RecipeMapStatus == "Recipe Not Mapped") {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatusr").css("background-color", "#ffe1e1");

                }
                else {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatusr").css("background-color", "#d9ffb3");
                }

                if (view[i].Status == 1) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffe1e1");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".k-grid-MapDishes").text("Configure for Region");

                }
                else if (view[i].Status == 2) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffffbf");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".k-grid-MapDishes").text("Configure for Region");
                }
                else {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".k-grid-MapDishes").text("Edit & Re-publish");
                }

             
            }
            //for (var i = 0; i < view.length; i++) {
            //    if (checkedIds[view[i].id]) {
            //        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
            //            .find(".checkbox")
            //            .attr("checked", "checked");
            //        //.addClass("k-state-selected")
            //    }
            //    if (view[i].MenuLevel == "Region" || view[i].MenuLevel == "Draft") {
            //        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
            //            .find(".checkbox")
            //            .attr("checked", "checked");//.attr("disabled", "disabled");
            //    }
            //}


        },

        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var grid = $("#gridItem").data("kendoGrid");
            var data = grid.dataSource._data

            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }
    });
    grid = grid.data("kendoGrid")
    if (user.SectorNumber != "30") {
        grid.hideColumn("FoodProgramName");
        grid.hideColumn("VisualCategoryName");

    }
    if (user.SectorNumber != "10") {
        grid.hideColumn("SubSectorName");
    }
}

function populateItemGridPreview() {


    var previewDataSource = $('#gridItem').data("kendoGrid").dataSource.dataFiltered().filter(function (item) {

        if ((checkedItemCodes.indexOf("1_" + item.ItemCode) != -1) || (checkedItemCodes.indexOf("0_" + item.ItemCode) != -1) || item.MenuLevel == "Draft") {


            return item;
        }

    });
    var gridVariable = $("#gridItem");
    gridVariable.html("");
    var grid1 = gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,
        // height: "430px",
        columns: [
            {
                field: "ItemCode", title: "Item Code", width: "80px", attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center"
                }
            },
            {
                field: "ItemName", title: "Item Name", attributes: {

                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                },
                template: '# if (DietCategoryName == "Veg") {#<div class="squareVeg"><div class= "statusVeg"></div></div>#} else if (DietCategoryName == "Non-Veg") {#<div class="squareNonVeg"><div class= "statusNonVeg"></div></div>#} else if (DietCategoryName == "Egg") {#<div class="squareEgg"><div class= "statusEgg"></div></div>#}# <span class="itemname">#:ItemName#</span>',
            },
            {
                field: "FoodProgramName", title: "Food Program", width: "160px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "VisualCategoryName", title: "Visual Category", width: "155px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "Status", title: "Menu Level", width: "135px",
                attributes: {
                    class: "mycustomstatus",
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: center"
                },
            },
        ],
        dataSource: {
            data: previewDataSource,

            schema: {
                model: {
                    id: "ID",
                    fields: {
                        ItemCode: { type: "string" },
                        ItemName: { type: "string" },
                        DietCategoryName: { type: "string" },
                        VisualCategoryName: { type: "string" },
                        FoodProgramName: { type: "string" },
                        HSNCode: { type: "string" },
                        CGST: { type: "string" },
                        SGST: { type: "string" },
                        CESS: { type: "string" },
                        MenuLevel: { type: "string" }
                    }

                }
            },
            pageSize: 150
        },
        columnResize: function (e) {
            //var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (checkedIds[view[i].id] == false) {
                    var textValue = this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text() + " (Removed)";
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffe1e1");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text("Removed");

                } else {
                    var textValue = this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text() + " (Added)";
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").text("Added");
                }
            }


        },
        change: function (e) {
        },
        //  filter: { field: "ItemCode", operator: "gt", value: "69999" }
    });
    var grid = grid1.data("kendoGrid")
    if (user.SectorNumber != "30") {
        grid.hideColumn("FoodProgramName");
        grid.hideColumn("VisualCategoryName");

    }
}


function preview(e) {

    if (e.checked)
        populateItemGridPreview();
    else
        populateItemGrid();

}


function updateWtPerUOMQuantity(iqty) {
    var qty = $("#" + iqty.id).val();
    var idstobeupdated = [];
    var idnum = iqty.id.substring(8);
    console.log(idnum);
    for (item of finalModelExitsDishCategory) {
        if (idnum == item.DishCategory_ID) {
            item.WeightPerUOM = qty;
        }
    }

    var subItem = $("#subinputdish" + idnum).find(".smallIngredient");
    var len = subItem.length;
    for (var i = 0; i < len; i++) {
        var sm = subItem[i].id;
        $("#wtuomdishuom" + sm).val(qty);
        var ins = "wtuomdishuom" + sm
        calculateCostPerServing(sm);
        idstobeupdated.push(ins);
    }
    //Master to be updated


    for (item of masterGroupList) {
        if (item.dropdownid == "inputdish" + idnum) {
            item.WeigthPerUOM = qty;

        }
    }

}

function inheritChanges() {
    $("#inheritItemChanges").show();
    $("#mapMaster").hide();
    populateItemInheritChangesGrid();
}

function backInheritChanges() {
    $("#inheritItemChanges").hide();
    $("#mapMaster").show();
}

function saveInheritChanges() {
    var grid = $("#gridInheritChanges").data("kendoGrid");
    // Get selected rows
    var sel = $("input:checked", grid.tbody).closest("tr");
    // Get data item for each
    var items = [];
    $.each(sel, function (idx, row) {
        var item = grid.dataItem(row);
        items.push(item);
    });
    if (items == undefined || items.length == 0) {
        toastr.error("Please select at least one change.");
        return false;
    }
    else {
        items[0].RegionID = RegionID,
            items[0].ItemCode = "70807"
        //regionID: RegionID,
        //itemCode: itemObject.Code
    }
    HttpClient.MakeSyncRequest(CookBookMasters.SaveChangesSectorToRegion, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again.");
            Utility.UnLoading();
        }
        else {
            toastr.success("Changes have been saved. Click Publish to make the Item available for Units");
            Utility.UnLoading();
        }
    }, {
        model: items
    }, true);
};

function populateItemInheritChangesGrid() {

    var gridVariable = $("#gridInheritChanges");
    gridVariable.html("");
    grid = gridVariable.kendoGrid({
        //selectable: "cell",
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        groupable: false,
        pageable: true,
        columns: [

            {
                field: "ChangeType", title: "Change", width: "250px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: left"
                }
            },
            {
                field: "ChangeMadeBy", title: "Change Made By", width: "100px", attributes: {
                    style: "text-align: left; font-weight: normal; float: left; display: flex; margin-top: 3px;"
                }
            },

            {
                field: "ActionType", title: "Action Type", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ChangeFrom", title: "Change From", width: "155px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ChangeTo", title: "Change To", width: "135px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
                headerAttributes: {

                    style: "text-align: left"
                },
            },
            {
                template: "<label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px' ><input type='checkbox' class='inheritcheckbox'><span class='checkmarkgrid'></span></label>",
                headerTemplate: "<span style='margin-right : 5px;'>Include All</span><label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px' ><input type='checkbox' id='header-chb-inherit' class='inheritcheckbox1'><span class='checkmarkgrid'></span></label>",
                width: "135px", title: "Include",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center; vertical-align:middle"
                }
            },
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeSyncRequest(CookBookMasters.GetInheritChangesSectorToRegion, function (result) {
                        Utility.UnLoading();
                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }
                    },
                        {
                            //regionID: RegionID,
                            //itemCode: itemObject.Code
                            regionID: 10,
                            itemCode: "70807"
                        }
                        , true);
                }
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        Code: { type: "string" },
                        ChangeType: { type: "string" },
                        ChangeMadeBy: { type: "string" },
                        ActionType: { type: "string" },
                        ChangeFrom: { type: "string" },
                        ChangeTo: { type: "string" },
                        IsChecked: { type: "boolean" }
                    }
                }
            },
            pageSize: 150
        },
        columnResize: function (e) {
            //var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        dataBound: function (e) {

        },

    });
    //bind click event to the checkbox
    grid = grid.data("kendoGrid")

    // grid.table.on("click", ".inheritcheckbox", inheritSelectRow);
    $('#header-chb-inherit').change(function (ev) {
        debugger;
        var checked = ev.target.checked;
        globalChecked = checked;
        $('.inheritcheckbox').each(function (idx, item) {
            // if (checked) {
            // if (($('.inheritcheckbox')[idx].checked)) {
            $(item).click();
            // }
            // }
        });

    });



}

