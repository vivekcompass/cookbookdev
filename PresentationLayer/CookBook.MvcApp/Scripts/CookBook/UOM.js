﻿var user;
var conceptype2data;
var tileImage = null;
var item = null;
var UOMCodeIntial = null;
var datamodel;
var completedata = [];
$(function () {
    $('#myInput').on('input', function (e) {
        var grid = $('#gridUOM').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "UOMCode" || x.field == "Name"  ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $(document).ready(function () {
      

        $(".k-window").hide();
        $(".k-overlay").hide();
        $("#UOMwindowEdit").kendoWindow({
            modal: true,
            width: "250px",
            height: "170px",
            title: "UOM Details  ",
            actions: ["Close"],
            visible: false,
            animation: false
        });

      
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.SectorNumber == "20") {
            //    //$("#videoPlayDiv").attr("disabled", "disabled");
            //    //$("#videoPlayDiv").removeAttr("disabled");
            //    //$('#videoPlayDiv').css('display', 'none');
            //    $('#videoPlayDiv').css('display', 'block');
            //    document.getElementById("videoCodeDisplayName").value = "UOM Details";
            //    document.getElementById("videoCodeURL").value = "/VideoFile/CBK0003.mp4";
            //}
            populateUOMGrid();
        }, null, false);

    
    });

 

    function uomvalidate() {
        var valid = true;

        if ($("#uomname").val() === "") {        
            toastr.error("Please provide input");
            $("#uomname").addClass("is-invalid");
            valid = false;
            $("#uomname").focus();
        }
        if (($.trim($("#uomname").val())).length > 30) {
            toastr.error("UOM accepts upto 30 character");
           
            $("#uomname").addClass("is-invalid");
            valid = false;
            $("#uomname").focus();
        }
        return valid;
    }
    //UOM Section Start

    function populateUOMGrid() {

        Utility.Loading();
        var gridVariable = $("#gridUOM");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "UOMCode", title: "UOM Code", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "IsActive", title: "Active", width: "75px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                               
                                var item = this.dataItem(tr);          // get the date of this row

                                if (!item.IsActive) {
                                    return;
                                }
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive,
                                    "UOMCode": item.UOMCode,
                                    "CreatedBy": item.CreatedBy,
                                    "CreatedOn": kendo.parseDate(item.CreatedOn)
                                };
                                UOMCodeIntial = item.UOMCode;
                                var dialog = $("#UOMwindowEdit").data("kendoWindow");

                                dialog.open();
                                dialog.center();

                                datamodel = model;
                               
                                $("#uomid").val(model.ID);
                                $("#uomcode").val(model.UOMCode);
                                $("#uomname").val(model.Name);
                              
                                $("#uomname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetUOMDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                if (user.SectorNumber !== "20") {
                                    result = result.filter(m => m.UOMCode != "UOM-00004"
                                        && m.UOMCode != "UOM-00005" && m.UOMCode != "UOM-00006" && m.UOMCode != "UOM-00007" && m.UOMCode != "UOM-00008" && m.UOMCode != "UOM-00009");
                                }
                                options.success(result);
                                completedata = result;
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { type: "string" },
                            UOMCode: { type: "string" },
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (!model.IsActive) {
                        $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                    
                });
                   
                    $(".chkbox").on("change", function () {
                        var gridObj = $("#gridUOM").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        var trEdit = $(this).closest("tr");
                        var th = this;
                        datamodel = tr;
                        datamodel.CreatedOn = kendo.parseDate(datamodel.CreatedOn);
                        datamodel.IsActive = $(this)[0].checked;
                        var active = "";
                        if (datamodel.IsActive) {
                            active = "Active";
                        } else {
                            active = "Inactive";
                        }

                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> UOM " + active + "?", " UOM Update Confirmation", "Yes", "No", function () {
                            HttpClient.MakeRequest(CookBookMasters.SaveUOM, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again");
                                }
                                else {
                                    toastr.success("UOM configuration updated successfully");
                                    if ($(th)[0].checked) {
                                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                    } else {
                                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                    }
                                }
                            }, {
                                model: datamodel
                            }, false);
                        }, function () {

                                $(th)[0].checked = !datamodel.IsActive;
                                if ($(th)[0].checked) {
                                    $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                } else {
                                    $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                }
                        });
                        return true;
                    });
               
            },
            change: function (e) {
            },
        })

      //  var gridVariable = $("#gridUOM").data("kendoGrid");
        //sort Grid's dataSource
        gridVariable.data("kendoGrid").dataSource.sort({ field: "UOMCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }


    $("#uomcancel").on("click", function () {
        var orderWindow = $("#UOMwindowEdit").data("kendoWindow");
        orderWindow.close();
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","UOM Cancel Confirmation", "Yes", "No", function () {
            $(".k-overlay").hide();
        }, function () {

              
                var orderWindow = $("#UOMwindowEdit").data("kendoWindow");
                orderWindow.open();
        });
       
       
    });

    $("#uomaddnew").on("click", function () {
        var model;
        var dialog = $("#UOMwindowEdit").data("kendoWindow");

        dialog.open();
        dialog.center();

        datamodel = model;
      
        dialog.title("New UOM Creation");

        $("#uomname").removeClass("is-invalid");
        $('#uomsubmit').removeAttr("disabled");
        $("#uomid").val("");
        $("#uomname").val("");
        $("#uomcode").val("");
        $("#uomname").focus();
        UOMCodeIntial = null;
    });
    
    $("#uomsubmit").click(function () {
        if ($("#uomname").val() === "") {
            toastr.error("Please provide UOM Name");
            $("#uomname").focus();
            return;
        }
        var name = $("#uomname").val().toLowerCase().trim();
        if (UOMCodeIntial == null) { //Duplicate Check
            for (item of completedata) {
                if (item.Name.toLowerCase().trim() == name) {
                    toastr.error("Please check Duplicate UOM Name");
                    $("#uomname").focus();
                    return;
                }
            }
        } else {
            for (item of completedata) {
                if (item.Name.toLowerCase().trim() == name && item.UOMCode != UOMCodeIntial) {
                    toastr.error("Please check Duplicate  UOM Name is entered on editing");
                    $("#uomname").focus();
                    return;
                }
            }
        }

        if (uomvalidate() === true) {
            $("#uomname").removeClass("is-invalid");
          
            var model = {
                "ID": $("#uomid").val(),
                "Name": $("#uomname").val(),
                "UOMCode": UOMCodeIntial,
                "IsActive":1
            }
            if (UOMCodeIntial== null) {
                model.IsActive = 1;
            } else {
                model.CreatedBy = datamodel.CreatedBy;
                model.CreatedOn = datamodel.CreatedOn;
            }
            if (!sanitizeAndSend(model)) {
                return;
            }

            //$("#uomsubmit").attr('disabled', 'disabled');
            HttpClient.MakeSyncRequest(CookBookMasters.SaveUOM, function (result) {
                if (result.xsssuccess !== undefined && !result.xsssuccess) {
                    toastr.error(result.message);
                    $('#uomsubmit').removeAttr("disabled");
                    Utility.UnLoading();
                }
                else {
                    if (result == false) {
                        $('#uomsubmit').removeAttr("disabled");
                        toastr.success("There was some error, the record cannot be saved");

                    }
                    else {
                        $(".k-overlay").hide();
                        $('#uomsubmit').removeAttr("disabled");

                        if (model.ID > 0)
                            toastr.success("UOM configuration updated successfully");
                        else
                            toastr.success("New UOM added successfully");
                        $(".k-overlay").hide();
                        var orderWindow = $("#UOMwindowEdit").data("kendoWindow");
                        orderWindow.close();
                        $("#gridUOM").data("kendoGrid").dataSource.data([]);
                        $("#gridUOM").data("kendoGrid").dataSource.read();
                        $("#gridUOM").data("kendoGrid").dataSource.sort({ field: "UOMCode", dir: "asc" });

                    }
                   
                }
            }, {
                model: model

            }, true);
        }
    });
 


});