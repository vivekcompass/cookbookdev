﻿var recipeImpactedData = [];

$("document").ready(function () {
    //populateRecipeDependencies();

    $("#btnDoLater").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditRecipeDependencies").data("kendoWindow");
        orderWindow.close();
    });

    $("#btnAgree").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditRecipeDependencies").data("kendoWindow");
        orderWindow.close();
    });
});

function getRecipeDependenciesData() {
    var recipeID = $("#hiddenRecipeID").val();
    HttpClient.MakeSyncRequest(CookBookMasters.GetRecipeImpactedDataList, function (data) {
        recipeImpactedData = data;
    }, { recipeId: recipeID, isBaseRecipe: isBaseRecipe }, false);
}

function populateRecipeDependencies() {
    if (isBaseRecipe) {
        $(".baseRecipeDiv").css('display', 'block');
        $(".finalRecipeDiv").css('display', 'none');
    }
    else {
        $(".baseRecipeDiv").css('display', 'none');
        $(".finalRecipeDiv").css('display', 'block');
    }
    getRecipeDependenciesData();
    $('.recipeName').text(RecipeName);
    $("#totalImapactedBaseRecipes").text(recipeImpactedData.TotalImapactedBaseRecipes);
    $("#totalImapactedFinalRecipes").text(recipeImpactedData.TotalImapactedFinalRecipes);
    $("#totalImapactedDishes").text(recipeImpactedData.TotalImapactedDishes);
    $("#totalImapactedItems").text(recipeImpactedData.TotalImapactedItems);
    $("#totalImapactedSites").text(recipeImpactedData.TotalImapactedSites);
    $("#totalImapactedRegion").text(recipeImpactedData.TotalImapactedRegions);
    populateBaseRecipesGrid();
    populateFinalRecipesGrid();
    populateDishGrid();
    populateItemGrid();
    populateSiteGrid();
    populateRegionGrid();
    Utility.UnLoading();
}

function populateBaseRecipesGrid() {
    Utility.Loading();
    var gridVariable = $("#baseRecipeGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        columns: [
            {
                field: "BaseRecipeCode", title: "Base Recipe Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "BaseRecipeName", title: "Base Recipe Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedFinalRecipeName", title: "Impacted Final Recipes (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: recipeImpactedData.MOGImpactedBaseRecipesData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        BaseRecipeCode: { type: "string" },
                        BaseRecipeName: { type: "string" },
                        ImpactedFinalRecipeName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateFinalRecipesGrid() {
    Utility.Loading();
    var gridVariable = $("#finalRecipeGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        columns: [
            {
                field: "FinalRecipeCode", title: "Final Recipe Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "FinalRecipeName", title: "Final Recipe Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedDishName", title: "Impacted Dishes (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: recipeImpactedData.MOGImpactedFinalRecipesData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        BaseFinalCode: { type: "string" },
                        BaseFinalName: { type: "string" },
                        ImpactedDishCode: { type: "string" },
                        ImpactedDishName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateDishGrid() {
    Utility.Loading();
    var gridVariable = $("#dishGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        columns: [
            {
                field: "DishCode", title: "Dish Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishName", title: "Dish Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedItemName", title: "Impacted Item (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: recipeImpactedData.MOGImpactedDishesData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        DishCode: { type: "string" },
                        DishName: { type: "string" },
                        ImpactedItemCode: { type: "string" },
                        ImpactedItemName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateItemGrid() {
    Utility.Loading();
    var gridVariable = $("#itemGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        columns: [
            {
                field: "ItemCode", title: "Item Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ItemName", title: "Item Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedSiteName", title: "Impacted Site (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: recipeImpactedData.MOGImpactedItemsData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        ItemCode: { type: "string" },
                        ItemName: { type: "string" },
                        ImpactedSiteCode: { type: "string" },
                        ImpactedSiteName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateSiteGrid() {
    Utility.Loading();
    var gridVariable = $("#siteGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        columns: [
            {
                field: "SiteCode", title: "Site Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "SiteName", title: "Site Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "RegionName", title: "Region", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: recipeImpactedData.MOGImpactedSitesData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        ItemCode: { type: "string" },
                        ItemName: { type: "string" },
                        ImpactedSiteCode: { type: "string" },
                        ImpactedSiteName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateRegionGrid() {
    Utility.Loading();
    var gridVariable = $("#regionGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        columns: [
            //{
            //    field: "RegionID", title: "Region Code", width: "50px", attributes: {
            //        style: "text-align: left; font-weight:normal"
            //    }
            //},
            {
                field: "RegionName", title: "Region Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
        ],
        dataSource: {
            data: recipeImpactedData.MOGImpactedRegionsData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        ItemCode: { type: "string" },
                        ItemName: { type: "string" },
                        ImpactedSiteCode: { type: "string" },
                        ImpactedSiteName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

