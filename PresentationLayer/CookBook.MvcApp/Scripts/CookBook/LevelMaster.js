﻿//import { Util } from "../bootstrap/js/bootstrap.bundle";

var mogWiseAPLMasterdataSource = [];
var checkedIds = {};
var checkedAPLCodes = [];
var user;
var status = "";
var varname = "";
var datamodel;
var MOGName = "";
var BATCHNUMBER = "";
var NewMOGName = "";
var MOGCode = "";
var MOGID = 0;
var uomdata = [];
var aplMasterdataSource = [];
var sectorMasterdataSource = [];
var isShowPreview = false;
var CheckedTrueAPLCodes = [];
var mogWiseAPLArray = [];
var mogImpactedData = [];
var isStatusSave = false;
var mogStatus = false;
var isRIEnabled = false;
var isMOGAPLMappingSave = false;
var aplFilterDataSource = [];

$(function () {

    


    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
        Utility.Loading();
        setTimeout(function () {
            populateMOGGrid();
            Utility.UnLoading();
        }, 2000);
    });


   


    $("#LevelwindowEdit").kendoWindow({
        modal: true,
        width: "475px",
        height: "99px",
        title: "Level Details Add ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#LevelwindowEdit1").kendoWindow({
        modal: true,
        width: "475px",
        height: "99px",
        title: "Level Details Update ",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    $("#LevelAddNew").on("click", function () {
        //
        // var jq = jQuery.noConflict();
        // alert("calick");
        var model;

        var dialog = $("#LevelwindowEdit").data("kendoWindow");

        //dialog.open();
        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });
        // dialog.center();

        datamodel = model;

        dialog.title("New Level Details");

        DishCategoryCodeIntial = null;

    });

    $("#Levelcancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#LevelwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#LevelcancelEdit").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#LevelwindowEdit1").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });
    
    $('#myInput').on('input', function (e) {
        var grid = $('#gridLevelList').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "Name" || x.field == "Description" || x.field == "Code" ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        if (x.field == "Status") {
                            var pendingString = "pending";
                            var savedString = "saved";
                            var mappedString = "mapped";
                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "3";
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
    $("#btnExport").click(function (e) {
        var grid = $("#gridLevelList").data("kendoGrid");
        grid.saveAsExcel();
    });


    

    $("#LevelSubmit").click(function () {
        debugger;
        if ($("#txt_Levelname").val() === "") {
            toastr.error("Please Provide Level Name");
            $("#txt_Levelname").focus();
            return false;
        }
        else if ($("#txt_Leveldesc").val() === "") {
            toastr.error("Please Provide Level Description");
            $("#txt_Leveldesc").focus();
            return false;
        }
        

        var model = datamodel;

        model = {

            "Name": $("#txt_Levelname").val(),
            "Description": $("#txt_Leveldesc").val()           
        }

        $("#LevelSubmit").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.SaveManageLevelInfo, function (result) {

            if (result.Data == 'Success') {
                $('#LevelSubmit').removeAttr("disabled");
                toastr.success("New Level record added successfully");
                $(".k-window").hide();
                $(".k-overlay").hide();


                //return true;

                var orderWindow = $("#LevelwindowEdit").data("kendoWindow");
                orderWindow.close();
                $("#gridLevelList").data("kendoGrid").dataSource.data([]);
                $("#gridLevelList").data("kendoGrid").dataSource.read();
                Utility.UnLoading();
                $("#txt_Levelname").val('');
                $("#txt_Leveldesc").val('');               
                return;
            }
            else {
                $(".k-overlay").hide();

                toastr.warning("Something is wrong");
                $(".k-window").hide();
                $(".k-overlay").hide();
                return;
            }
        }, {
            model: model

        }, true);

        Utility.UnLoading();
        //populateCafeGrid();
    });

    $("#LevelSubmitEdit").click(function () {
        
        if ($("#txt_LevelnameEdit").val() === "") {
            toastr.error("Please Provide Level Name");
            $("#txt_LevelnameEdit").focus();
            return false;
        }
        else if ($("#txt_LeveldescEdit").val() === "") {
            toastr.error("Please Provide Level Description");
            $("#txt_LeveldescEdit").focus();
            return false;
        }


        var model = datamodel;

        model = {
            "id": $("#sectorid").val(),
            "Name": $("#txt_LevelnameEdit").val(),
            "Description": $("#txt_LeveldescEdit").val()
        }

        $("#LevelSubmitEdit").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.UpdateManageLevelInfo, function (result) {

            if (result.Data == 'Success') {
                $('#LevelSubmitEdit').removeAttr("disabled");
                toastr.success("Level record Update successfully");
                $(".k-window").hide();
                $(".k-overlay").hide();


                //return true;

                var orderWindow = $("#LevelwindowEdit1").data("kendoWindow");
                orderWindow.close();
                $("#gridLevelList").data("kendoGrid").dataSource.data([]);
                $("#gridLevelList").data("kendoGrid").dataSource.read();
                Utility.UnLoading();
                $("#txt_LevelnameEdit").val('');
                $("#txt_LeveldescEdit").val('');
                return;
            }
            else {
                $(".k-overlay").hide();

                toastr.warning("Something want is wrong");
                $(".k-window").hide();
                $(".k-overlay").hide();
                return;
            }
        }, {
            model: model

        }, true);

        Utility.UnLoading();
        //populateCafeGrid();
    });

        function populateMOGGrid() {

            debugger;
            var gridVariable = $("#gridLevelList");
            gridVariable.html("");
            gridVariable.kendoGrid({
                excel: {
                    fileName: "MOG.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: {
                    extra: true,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            doesnotcontain: "Does not contain",
                            endswith: "Ends with"
                        }
                    }
                },
                groupable: false,

                //pageable: true,
                //reorderable: true,
                //scrollable: true,
                columns: [
                    {
                        field: "Code", title: "Level Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: left;"
                        }
                    },
                    {
                        field: "Name", title: "Level Name", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        },

                    },
                    {
                        field: "Description", title: "Description", width: "60px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },

                    //{
                    //    field: "IsActive", title: "Active", width: "75px", attributes: {

                    //        style: "text-align: center; font-weight:normal"
                    //    },
                    //    headerAttributes: {
                    //        style: "text-align: center;"
                    //    },
                    //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                    //},


                    {
                        field: "Edit", title: "Action", width: "35px",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center"
                        },
                        command: [
                            {
                                name: 'Edit',
                                click: function (e) {

                                    var dialog = $("#LevelwindowEdit1").data("kendoWindow");
                                    var tr = $(e.target).closest("tr");
                                    var gridObj = $("#gridLevelList").data("kendoGrid");
                                    tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    // get the date of this row
                                    $("#sectorid").val(tr.id);



                                    var model = {
                                        "id": tr.id,
                                        "Name": tr.Name,
                                        "Description": tr.Description
                                        
                                    };
                                    dialog.open();
                                    dialog.center();

                                    datamodel = model;


                                    $("#txt_LevelnameEdit").val(model.Name);
                                    $("#txt_LeveldescEdit").val(model.Description);
                                    
                                    return true;
                                }
                            }
                            ,
                            
                        ],
                    },


                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(CookBookMasters.GetLevelMasterList, function (result) {
                                Utility.UnLoading();
                                if (result != null) {

                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , false);
                        }
                    },
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                id: { type: "string" },
                                Code: { type: "string" },
                                Name: { type: "string" },
                                Description: { type: "string" },
                                IsActive: { type: "string" },
                                
                            }
                        }
                    },
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },
                dataBound: function (e) {

                   
                    var items = e.sender.items();
                    var grid = this;
                    grid.tbody.find("tr[role='gridcell']").each(function () {
                        var model = grid.dataItem(this);

                        if (!model.IsActive) {
                            $(this).find(".k-grid-Edit").addClass("k-state-disabled");
                        }
                    });
                    var items = e.sender.items();
                    $(".chkbox").on("change", function () {
                        
                        var gridObj = $("#gridLevelList").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        var trEdit = $(this).closest("tr");
                        var th = this;
                        datamodel = tr;

                        if ($(this)[0].checked == false) {
                            datamodel.IsActive = 0;
                        }
                        else {
                            datamodel.IsActive = 1;
                        }

                        var active = "";
                        if (datamodel.IsActive) {
                            active = 1;
                        } else {
                            active = 0;
                        }

                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> Sector " + active + "?", " Sector Update Confirmation", "Yes", "No", function () {
                            HttpClient.MakeRequest(CookBookMasters.UpdateLevelInactiveActive, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again");
                                }
                                else {
                                    toastr.success("Level updated successfully");
                                    $(th)[0].checked = datamodel.IsActive;
                                    if ($(th)[0].checked) {
                                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                    } else {
                                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                    }
                                }
                            }, {
                                model: datamodel
                            }, false);
                        }, function () {

                            $(th)[0].checked = !datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        });
                        return true;
                    });
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
            //    if ($(".k-label")[0]!= null)
            //$(".k-label")[0].innerHTML.replace("items", "records");
        }
        

       
 

});



//on click of the checkbox:










$("document").ready(function () {
    // showMOGAPLDependencies()
    $(".k-window").hide();
    $(".k-overlay").hide();
    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();

    });
});
