﻿//import { Util } from "../bootstrap/js/bootstrap.bundle";

var mogWiseAPLMasterdataSource = [];
var checkedIds = {};
var checkedAPLCodes = [];
var user;
var status = "";
var varname = "";
var datamodel;
var MOGName = "";
var NewMOGName = "";
var MOGCode = "";
var MOGID = 0;
var uomdata = [];
var aplMasterdataSource = [];
var isShowPreview = false;
var CheckedTrueAPLCodes = [];
var mogWiseAPLArray = [];
var mogImpactedData = [];
var isStatusSave = false;
var mogStatus = false;
var isRIEnabled = false;
var isMOGAPLMappingSave = false;
var uommodulemappingdata = [];

$(function () {

    $("#MOGwindowEdit").kendoWindow({
        modal: true,
        width: "250px",
        height: "300px",
        title: "MOG Details - " + MOGName,
        actions: ["Close"],
        visible: false,
        animation: false,
        resizable:false
    });

    $("#MOGwindowEditMOGChangeConfirm").kendoWindow({
        modal: true,
        width: "600px",
        height: "220px",
        title: "Alert",
        // actions: ["Close"],
        visible: false,
        animation: false
    });


    $("#windowEditMOGDependencies").kendoWindow({
        modal: true,
        width: "1050px",
        height: "570px",
        left: '220px !important',
        top: '20px !important',
        title: "MOG - " + MOGName + " Dependencies",
        actions: ["Close"],
        visible: false,
        animation: false,
        close: function (e) {
            if (isStatusSave) {
                $("#chkbox-" + MOGID + "").prop('checked', !datamodel.IsActive);
            }
            else if (isMOGAPLMappingSave) {
                $("#MOGwindowEditMOGWiseAPL").parent().show();
            }
        }
    });

    $("#MOGwindowEditMOGWiseAPL").kendoWindow({
        modal: true,
        width: "1150px",
        height: "470px",
        left: '220px !important',
        top: '80px !important',
        title: "MOG APL Detials - " + MOGName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $('#myInput').on('input', function (e) {
        var grid = $('#gridMOG').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "MOGCode" || x.field == "Name" || x.field == "UOMName" || x.field == "Alias" || x.field == "Status") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        if (x.field == "Status") {
                            var pendingString = "pending";
                            var savedString = "saved";
                            var mappedString = "mapped";
                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "3";
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $(document).ready(function () {
        isRIEnabled = $("#isRIEnabled").val();

        $("#MOGwindowEditMOGChangeConfirm").parent().find(".k-window-action").css("visibility", "hidden");

        $("#btnExport").click(function (e) {
            Utility.Loading();
            var grid = $("#gridMOG").data("kendoGrid");
            grid.saveAsExcel();
            Utility.UnLoading();
        });

        //aplMasterdataSource.push({ "ArticleID": 0, "ArticleDescription": "Select APL" })
        //populateAPLMasterDropdown();

        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
                $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNew").css("display", "none");
            //}
            populateMOGGrid();
            HttpClient.MakeRequest(CookBookMasters.GetAPLMasterDataList, function (data) {
                aplMasterdataSource = data.filter(m => m.ArticleType == 'ZFOD' || m.ArticleType == 'ZPRP');
                aplMasterdataSource.unshift({ "ArticleID": 0, "ArticleDescription": "Select APL" })

                // populateAPLMasterDropdown();
                //$("#btnGo").css("display", "inline-block");

            }, null , true);
        }, null , true);

        //HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

        //    var dataSource = data;
        //    uomdata = [];
        //    uomdata.push({ "value": "Select", "text": "Select" });
        //    for (var i = 0; i < dataSource.length; i++) {
        //        uomdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].Name });
        //    }
        //    console.log(uomdata)
        //    populateUOMDropdown();
        //    populateUOMDropdown_Bulk();
        //}, null, true);

        HttpClient.MakeRequest(CookBookMasters.GetUOMModuleMappingDataList, function (data) {
            var dataSource = data;
            debugger;
            uommodulemappingdata = [];
            uommodulemappingdata.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                if (dataSource[i].IsActive) {
                    uommodulemappingdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].UOMName, "UOMModuleCode": dataSource[i].UOMModuleCode });
                }
            }
            uomdata = uommodulemappingdata.filter(m => m.UOMModuleCode == 'UMM-00001' || m.text == "Select");
            populateUOMDropdown();
            populateUOMDropdown_Bulk();
            GetUOMModuleMappingDataList
        }, null, false);

        $("#gridBulkChange").css("display", "none");
        populateBulkChangeControls();
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        changeControls.css("background-color", "#fff");
        changeControls.css("border", "none");
        changeControls.eq(3).text("");
        changeControls.eq(3).append("<div id='uom' style='width:100%; font-size:10.5px!important'></div>");

        //HttpClient.MakeRequest(CookBookMasters.GetAPLMasterDataList, function (data) {
        //    aplMasterdataSource = data.filter(m => m.ArticleType == 'ZFOD' && m.ArticleType == 'ZPRP');
        //    aplMasterdataSource.unshift({ "ArticleID": 0, "ArticleDescription": "Select APL" })

        //    //populateRegionMasterDropdown();
        //    //$("#btnGo").css("display", "inline-block");

        //}, null , true);
    });


    $("#InitiateBulkChanges").on("click", function () {
        $("#gridBulkChange").css("display", "block");
        $("#gridBulkChange").children(".k-grid-header").css("border", "none");
        $("#InitiateBulkChanges").css("display", "none");
        $("#CancelBulkChanges").css("display", "inline");
        $("#SaveBulkChanges").css("display", "inline");
    });

    function populateUOMDropdown_Bulk() {
        $("#uom").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: uomdata,
            index: 0,
        });
    }
    function populateUOMDropdown() {
        $("#inputuom").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: uomdata,
            index: 0,
        });
    }

    //$("#inputcpu").data("kendoDropDownList").select(function (dataItem) {
    //    return dataItem.value === dataSource[0].value;
    //});
    //$("#inputcpu").data("kendoDropDownList").select(0);

    $("#CancelBulkChanges").on("click", function () {
        $("#InitiateBulkChanges").css("display", "inline");
        $("#CancelBulkChanges").css("display", "none");
        $("#SaveBulkChanges").css("display", "none");
        $("#gridBulkChange").css("display", "none");
        populateDishGrid();
    });

    $("#SaveBulkChanges").on("click", function () {

        var neVal = $(this).val();
        var dataFiltered = $("#gridMOG").data("kendoGrid").dataSource.dataFiltered();


        var model = dataFiltered;
        var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
        var validChange = false;
        var uom = "";

        var uomspan = $("#uom");

        if (uomspan.val() != "Select") {
            validChange = true;
            uom = uomspan.val();
        }
        if (validChange == false) {
            toastr.error("Please change at least one attribute for Bulk Update");
        } else {
            Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                function () {
                    $.each(model, function () {
                        this.CreatedOn = kendo.parseDate(this.CreatedOn);
                        this.ModifiedOn = Utility.CurrentDate();
                        this.ModifiedBy = user.UserId;
                        if (uom != "")
                            this.UOM_ID = uom;
                    });

                    HttpClient.MakeRequest(CookBookMasters.SaveMOGDataList, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again.");
                        }
                        else {
                            $(".k-overlay").hide();
                            toastr.success("Records have been updated successfully");
                            $("#gridMOG").data("kendoGrid").dataSource.data([]);
                            $("#gridMOG").data("kendoGrid").dataSource.read();
                            $("#InitiateBulkChanges").css("display", "inline");
                            $("#CancelBulkChanges").css("display", "none");
                            $("#SaveBulkChanges").css("display", "none");
                            $("#gridBulkChange").css("display", "none");
                        }
                    }, {
                        model: model

                    } , true);
                },
                function () {
                    maptype.focus();
                }
            );
        }
    });
    //Food Program Section Start

    function populateMOGGrid() {

        Utility.Loading();
        var gridVariable = $("#gridMOG");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "MOG.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,

            //pageable: true,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "MOGCode", title: "MOG Code", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: left;"
                    }
                },
                {
                    field: "Name", title: "MOG Name", width: "70px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: '# if(isRIEnabled){#<span sytle="cursor: pointer !important;" onClick="showMOGImpacts(this)"><i title="View Dependencies" class="fas fa-link m-link"></i></span>#}#<span class="mogname"> #:Name#</span>',
                },
                {
                    field: "Alias", title: "MOG Alias", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "UOMName", title: "UOM", width: "40px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    }
                },
                //{
                //    field: "PublishedDate", title: "Last Updated", width: "40px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    template: '# if (PublishedDate == null) {#<span>-</span>#} else{#<span>#: kendo.toString(PublishedDate, "dd-MMM-yy")#</span>#}#',
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    }
                //},
                {
                    field: "IsActive",
                    title: "Active", width: "40px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    //template: '<label class= "switch"><input type="checkbox"  disabled  id="chkbox-#:ID#" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                    template: '# if (IsActive == "1") {#<span>Yes</span>#} else  {#<span>No</span>#}  #',
                },
                //{
                //    field: "Status", title: "Status", width: "30px", attributes: {
                //        class: "mycustomstatus",
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    template: '# if (Status == "1") {#<span>Pending</span>#} else if (Status == "2") {#<span>Saved</span>#} else if (Status == "3") {#Mapped#}#',
                //    headerAttributes: {

                //        style: "text-align: center"
                //    },
                //},
                {
                    field: "Edit", title: "Action", width: "35px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    },
                    command: [
                        //{
                        //    name: 'Edit',
                        //    click: function (e) {
                        //        isStatusSave = false;
                        //        var gridObj = $("#gridMOG").data("kendoGrid");
                        //        var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                        //        datamodel = tr;
                        //        MOGName = tr.Name;
                        //        MOGCode = tr.MOGCode;
                        //        MOGID = tr.ID;
                        //        var dialog = $("#MOGwindowEdit").data("kendoWindow");
                               
                        //        $(".k-overlay").css("display", "block");
                        //        $(".k-overlay").css("opacity", "0.5");
                        //        dialog.open();
                        //        dialog.center();
                        //        console.log(tr)

                        //        $("#mogCodeDiv").css('display', 'block')
                        //        //$("#mogUOMDiv").removeClass('col-3');
                        //        //$("#mogUOMDiv").addClass('col-2');
                        //        //$("#mogNameDiv").removeClass('col-8');
                        //        //$("#mogNameDiv").addClass('col-6');

                        //        $("#mogcode").attr('disabled', 'disabled');
                        //        $("#mogid").val(tr.ID);
                        //        $("#inputmogname").val(tr.Name);
                        //        $("#inputmogalias").val(tr.Alias);
                        //        $("#mogcode").val(tr.MOGCode);
                        //        $("#inputuom").data('kendoDropDownList').value(tr.UOMCode);


                        //        //if (user.UserRoleId === 1) {
                        //        //    $("#inputmogname").removeAttr('disabled');
                        //        //    $("#inputmogalias").removeAttr('disabled');
                        //        //    $("#inputuom").removeAttr('disabled');
                        //        //    $("#btnSubmit").css('display', 'inline');
                        //        //    $("#btnCancel").css('display', 'inline');
                        //        //} else {
                        //        //    $("#inputmogname").attr('disabled', 'disabled');
                        //        //    $("#inputuom").attr('disabled', 'disabled');
                        //        //    $("#inputmogalias").attr('disabled', 'disabled');
                        //        //    $("#btnSubmit").css('display', 'none');
                        //        //    $("#btnCancel").css('display', 'none');
                        //        //}

                        //        dialog.title("MOG Details - " + MOGName);
                        //        return true;
                        //    }
                        //}
                        //,
                        {
                            name: 'View APL',
                            click: function (e) {
                                Utility.Loading();
                                setTimeout(function () {
                                    $("#inputAPLClear").empty();
                                    $("#inputAPLClear").append('<div style="display: none; text - align: left; " id="inputAPL" ></div>');
                                    checkedAPLCodes = [];
                                    checkedIds = [];
                                    CheckedTrueAPLCodes = [];
                                    isShowPreview = false;
                                    $("#btnShowChecked").prop('checked', false);
                                    $("#btnPreview").prop('checked', false);
                                    var gridObj = $("#gridMOG").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    MOGName = tr.Name;
                                    MOGCode = tr.MOGCode;
                                    MOGID = tr.ID;
                                    var dialog = $("#MOGwindowEditMOGWiseAPL").data("kendoWindow");

                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open().element.closest(".k-window").css({
                                        //top: 167,
                                        //left: 558

                                    });

                                    
                                    dialog.center();
                                    $("#mogid").val(tr.ID);
                                    dialog.title("MOG APL Details - " + MOGName);
                                    Utility.Loading();

                                    Utility.UnLoading();
                                    $("#btnGo").css("display", "inline-block");
                                    MOGCode = tr.MOGCode;
                                    mogWiseAPLMasterdataSource = [];
                                    getMOGWiseAPLMasterData();
                                    populateAPLMasterDropdown();
                                    var multiselect = $("#inputAPL").data("kendoMultiSelect");

                                    //clear filter
                                    multiselect.dataSource.filter({});
                                    populateAPLMasterGrid();
                                    return true;
                                    Utility.UnLoading();
                                }, 2000);
                            }
                        }
                    ],
                },


            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetMOGDataList, function (result) {
                            Utility.UnLoading();
                            if (result != null) {

                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                             , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            MOGCode: { type: "string" },
                            Name: { type: "string" },
                            UOMName: { type: "string" },
                            Alias: { type: "string" },
                            Status: { type: "string" },
                          
                            ModifiedOnAsString: { type: "string" },
                            CreatedOnAsString: { type: "string"},
                        }
                    }
                },
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var grid = this;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    
                    $(this).find(".switch").addClass("k-state-disabled");
                    
                });

                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (view[i].Status == 1) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffe1e1");

                    }
                    else if (view[i].Status == 2) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#ffffbf");
                    }
                    else {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#d9ffb3");
                    }

                }
                var items = e.sender.items();

                //items.each(function (e) {

                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //        $(this).find('.chkbox').removeAttr('disabled');
                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //        $(this).find('.chkbox').attr('disabled', 'disabled');
                //    }
                //});

                $(".chkbox").on("change", function (e) {
                    // 
                    isStatusSave = true;
                    isMOGAPLMappingSave = false;
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridMOG").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    MOGName = tr.Name;
                    MOGCode = tr.MOGCode;
                    MOGID = tr.ID;
                    datamodel.IsActive = $(this)[0].checked;
                    mogStatus = datamodel.IsActive;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }
                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.Name + "</b> MOG " + active + "?", "MOG Update Confirmation", "Yes", "No", function () {
                        getMOGDependenciesData();
                        if (mogImpactedData.IsMOGImpacted) {
                            $(".mogDepStatus").text(datamodel.IsActive ? "active" : "inactive");
                            showMOGDependencies();
                        }
                        else {
                            SaveMOGStatus();
                        }
                    }, function () {
                        $("#chkbox-" + datamodel.ID + "").prop('checked', !datamodel.IsActive);
                        // $("#chkbox-" + datamodel.ID + "").checked = !datamodel.IsActive;
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                //var grid = e.sender;
                //var fields = grid.dataSource.options.fields;
                //var fieldsModels = grid.dataSource.options.schema.model.fields;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid" || col == "CreatedOn" || col=="ModifiedOn") { }
                    else
                        return col;
                });


                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });

                //var sheet = e.workbook.sheets[0];
                //var grid = e.sender;
                //var fields = grid.dataSource.options.fields;
                //var fieldsModels = grid.dataSource.options.schema.model.fields;
                //var columns = grid.columns;
                //var dateCells = [];

                //for (var i = 0; i < fields.length; i++) {
                //    var currentField = fields[i].field;
                //    var currentModel = fieldsModels[currentField];

                //    if (currentModel.type === "date") {
                //        for (var j = 0; j < columns.length; j++) {
                //            if (currentField === columns[j].field) {
                //                dateCells.push(j);
                //                break;
                //            };
                //        };
                //    };
                //};
                //for (var rowIndex = 1; rowIndex < sheet.rows.length; rowIndex++) {
                //    var row = sheet.rows[rowIndex];

                //    for (var q = 0; q < dateCells.length; q++) {
                //        var cellIndex = dateCells[q];
                //        var value = row.cells[cellIndex].value;
                //        var newValue = new Date(value.getFullYear(), value.getMonth(), value.getDay());

                //        row.cells[cellIndex].value = newValue;
                //        row.cells[cellIndex].format = "yyyy-MMM-dd";
                //    };
                //};



                //console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
    //    if ($(".k-label")[0]!= null)
           //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function populateBulkChangeControls() {

        Utility.Loading();
        var gridVariable = $("#gridBulkChange");
        gridVariable.html("");
        gridVariable.kendoGrid({
            selectable: "cell",
            sortable: false,
            filterable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "MOGCode", title: "", width: "50px", attributes: {
                        style: "text-align: center; font-weight:normal"
                    }
                },
                {
                    field: "Name", title: "MOG Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "", title: "MOG Alias", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
               
                {
                    field: "", title: "", width: "40px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "40px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "30px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                 {
                    field: "", title: "", width: "35px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                }
            ],
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
            },
            change: function (e) {
            },
        });
    }

    $("#AddNew").on("click", function () {
        var model;
        var dialog = $("#MOGwindowEdit").data("kendoWindow");
        
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open().element.closest(".k-window").css({
            top: 167,
            left: 558

        });
        dialog.center();

        datamodel = model;
        //$("#mogCodeDiv").css('display', 'none')
        //$("#mogUOMDiv").removeClass('col-2');
        //$("#mogUOMDiv").addClass('col-3');
        //$("#mogNameDiv").removeClass('col-6');
        //$("#mogNameDiv").addClass('col-8');

        $("#mogid").text("");
        $("#mogcode").val("");
        $("#inputmogname").val("");
        $("#inputmogalias").val("");
        $("#inputuom").data('kendoDropDownList').value("Select");
        dialog.title("New MOG Creation");
    })

    $("#btnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?","Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#MOGwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
            function () {
            }
        );
    });

    $("#btnDoLater").on("click", function () {
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditMOGDependencies").data("kendoWindow");
        orderWindow.close();
        if (isStatusSave) {
            $("#chkbox-" + MOGID + "").prop('checked', !datamodel.IsActive);
        }
        else if (isMOGAPLMappingSave) {
            $("#MOGwindowEditMOGWiseAPL").parent().show();
        }
    });

    $("#btnAgree").on("click", function () {
        if (isStatusSave) {
            if (mogImpactedData.IsMOGImpacted) {
                return;
            }
            SaveMOGStatus();
        }
        else
            saveMOGData();
        $(".k-overlay").hide();
        var orderWindow = $("#windowEditMOGDependencies").data("kendoWindow");
        orderWindow.close();
    });

    $("#btnViewDependencies").click(function () {

        showMOGDependencies();
    });

   

    function sendMOGEmail() {
        Utility.Loading();
        HttpClient.MakeRequest(CookBookMasters.SendMOGEmail, function (result) {
            if (result == false) {
                // toastr.error("Some error occured, please try again");
            }
            else {
                if (isStatusSave) {
                    //toastr.success("MOG status updated successfully");
                }
                else {
                    //   toastr.success("MOG updated successfully");
                }
            }
        }, {
            model: datamodel
        } , true);
    }


    $("#btnSubmit").click(function () {
        if ($("#inputmogname").val() === "") {
            toastr.error("Please provide MOG Name");
            $("#inputmogname").focus();
            return;
        }
        if ($("#inputuom").val() === "") {
            toastr.error("Please select UOM");
            $("#inputuom").focus();
            return;
        }
        else {
            NewMOGName = $("#inputmogname").val();

            if ($("#inputmogname").val() !== MOGName) {
                $(".oldmogname").text(MOGName);
                $(".oldmogcode").text(MOGCode);
                $(".newmogname").text(NewMOGName);
                $("#MOGwindowEdit").parent().hide();
                getMOGDependenciesData();
                if (mogImpactedData.IsMOGImpacted) {
                    showMOGDependencies();
                }
                else {
                    saveMOGData();
                }
            }
            else {
                saveMOGData();
            }
        }
    });

    function SaveMOGStatus() {

        HttpClient.MakeSyncRequest(CookBookMasters.SaveMOGData, function (result) {
            if (result == false) {
                toastr.error("Some error occured, please try again");
            }
            else {
                toastr.success("MOG status updated successfully");
                $("#gridMOG").data("kendoGrid").dataSource.data([]);
                $("#gridMOG").data("kendoGrid").dataSource.read();
                if (mogImpactedData.IsMOGImpacted) {
                    sendMOGEmail();
                }
            }
        }, {
            model: datamodel
        } , true);
    }

    function saveMOGData() {
        var model;
        if (datamodel != null) {
            model = datamodel;
            model.MOGCode = $("#mogcode").val();
            model.Name = $("#inputmogname").val();
            model.Alias = $("#inputmogalias").val();
            model.UOMCode = $("#inputuom").val();
            model.OldMOGName = MOGName;
            model.CreatedOn = kendo.parseDate(model.CreatedOn);
        }
        else {
            model = {
                "ID": $("#dishid").val(),
                "MOGCode": $("#mogcode").val(),
                "Name": $("#inputmogname").val(),
                "Alias": $("#inputmogalias").val(),
                "UOMCode": $("#inputuom").val(),
                "IsActive": true,
                "OldMOGName": MOGName
            }
        }
        if (!sanitizeAndSend(model)) {
            return;
        }
        $("#btnSubmit").attr('disabled', 'disabled');
        HttpClient.MakeRequest(CookBookMasters.SaveMOGData, function (result) {
            if (result.xsssuccess !== undefined && !result.xsssuccess) {
                toastr.error(result.message);
                $('#btnSubmit').removeAttr("disabled");
                toastr.error("Some error occured, please try again");
                $("#inputmogname").focus();
            }
            else {
                if (result == false) {
                    $('#btnSubmit').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    $("#inputmogname").focus();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#MOGwindowEdit").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit').removeAttr("disabled");
                    if (model.ID > 0) {


                        toastr.success("MOG updated successfully");
                    }
                    else
                        toastr.success("New MOG created successfully");

                    $("#gridMOG").data("kendoGrid").dataSource.data([]);
                    $("#gridMOG").data("kendoGrid").dataSource.read();
                    if (mogImpactedData.IsMOGImpacted) {
                        datamodel = model;
                        sendMOGEmail();
                    }
                }
            }
        }, {
            model: model
        } , true);
    }


    function populateAPLMasterDropdown() {

        var aplFilterDataSource = aplMasterdataSource.filter(function (item) {
            if (mogWiseAPLArray.indexOf(parseInt(item.ArticleID)) == -1) {
                return item;
            }
        });

        $("#inputAPL").kendoMultiSelect({
            filter: "contains",
            dataTextField: "ArticleDescription",
            dataValueField: "ArticleID",
            dataSource: aplFilterDataSource,
            placeholder: " ",
            index: 0,
            autoBind: true,
            autoClose: false,
        });
        var multiselect = $("#inputAPL").data("kendoMultiSelect");

        //clear filter
        multiselect.dataSource.filter({});

        //set value
        // multiselect.value(mogWiseAPLArray);

    }

    $("#showSelection").bind("click", function () {
        var checked = [];
        for (var i in checkedIds) {
            if (checkedIds[i]) {
                checked.push(i);
            }
        }
    });

});

function populateAPLMasterGrid() {
    Utility.Loading();


    var gridVariable = $("#gridMOGWiseAPL");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "APLMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: true,
        groupable: false,
        //reorderable: true,
        //scrollable: true,
        columns: [
            {
                field: "ArticleNumber", title: "Article Number", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ArticleDescription", title: "Article Description", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOM", title: "UOM", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ArticleType", title: "Article Type", width: "40px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "Hierlevel3", title: "Category", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MerchandizeCategoryDesc", title: "Merchandize Category", width: "80px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MOGName", title: "MOG Name", width: "80px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ArticleCost", title: "Standard Cost", width: "50px", format: Utility.Cost_Format,
                attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            },
            {
                field: "StandardCostPerKg", title: "Cost Per KG", width: "50px", format: Utility.Cost_Format,
                attributes: {

                    style: "text-align: left; font-weight:normal;text-align:center;"
                },
            }
        ],
        dataSource: {
            data: mogWiseAPLMasterdataSource,
            schema: {
                model: {
                    id: "ArticleID",
                    fields: {
                        ArticleNumber: { type: "string" },
                        ArticleDescription: { type: "string" },
                        UOM: { type: "string" },
                        ArticleType: { type: "string" },
                        Hierlevel3: { type: "string" },
                        MerchandizeCategoryDesc: { type: "string" },
                        MOGName: { type: "string" },
                        StandardCostPerKg: { editable: false },
                        ArticleCost: { editable: false }
                    }
                }
            },
            pageSize: 100,
        },
        height: 450,
        minHeight: 200,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {
            CheckedTrueAPLCodes = [];
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (checkedIds[view[i].id]) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");
                }
                if (checkedIds[view[i].id] == false) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .removeAttr("checked");
                    continue;
                }

                if (view[i].MOGName != "") {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");
                    CheckedTrueAPLCodes.push(view[i].ArticleID);
                }
            }
        },
        excelExport: function onExcelExport(e) {
            var sheet = e.workbook.sheets[0];
            var data = e.data;
            var cols = Object.keys(data[0])
            var columns = cols.filter(function (col) {
                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                else
                    return col;
            });
            var columns1 = columns.map(function (col) {
                return {
                    value: col,
                    autoWidth: true,
                    background: "#7a7a7a",
                    color: "#fff"
                };
            });
            console.log(columns1);
            var rows = [{ cells: columns1, type: "header" }];

            for (var i = 0; i < data.length; i++) {
                var rowCells = [];
                for (var j = 0; j < columns.length; j++) {
                    var cellValue = data[i][columns[j]];
                    rowCells.push({ value: cellValue });
                }
                rows.push({ cells: rowCells, type: "data" });
            }
            sheet.rows = rows;
        }

    })

    //bind click event to the checkbox
    var grid = gridVariable.data("kendoGrid")
    grid.table.on("click", ".checkbox", selectRow);
    //$(".k-label")[0].innerHTML.replace("items", "records");
}

function getMOGWiseAPLMasterData() {
    mogWiseAPLArray = [];
    HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataListForMOG, function (result) {
        Utility.UnLoading();

        if (result != null) {
            mogWiseAPLMasterdataSource = result;
            mogWiseAPLArray = mogWiseAPLMasterdataSource.map(
                item => {
                    return item.ArticleID;
                });
        }
        else {
        }
    }, { mogCode: MOGCode }
         , true);
}

function populateAPLMasterGridPreview() {
    var previewDataSource = [];
    Utility.Loading();
    if (isShowPreview) {

        previewDataSource = mogWiseAPLMasterdataSource.filter(function (item) {
            if (CheckedTrueAPLCodes.indexOf(item.ArticleID) != -1) {
                return item;
            }
        });
    }
    else {
        previewDataSource = mogWiseAPLMasterdataSource.filter(function (item) {
            if ((checkedAPLCodes.indexOf("1_" + item.ArticleID) != -1) || (checkedAPLCodes.indexOf("0_" + item.ArticleID) != -1)) {
                return item;
            }
        });
    }

    var gridVariable = $("#gridMOGWiseAPL");
    gridVariable.html("");
    gridVariable.kendoGrid({
        excel: {
            fileName: "APLMaster.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: true,
        groupable: false,
        //reorderable: true,
        //scrollable: true,
        columns: [
            {
                field: "ArticleNumber", title: "Article Number", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ArticleDescription", title: "Article Description", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOM", title: "UOM", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ArticleType", title: "Article Type", width: "40px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "Hierlevel3", title: "Category", width: "50px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MerchandizeCategoryDesc", title: "Merchandize Category", width: "80px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "MOGName", title: "MOG Name", width: "80px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
            //{
            //    template: "<label class= 'checkbox-container' style = 'display: inline-block;padding-top:5px' ><input  type='checkbox' class='checkbox'><span class='checkmarkgrid'></span></label>",
            //    width: "35px", title: "Include",
            //    attributes: {
            //        style: "text-align: center; font-weight:normal",
            //        class: "mycustomstatus"
            //    },
            //    headerAttributes: {
            //        style: "text-align: center; vertical-align:middle"
            //    }
            //}
        ],
        dataSource: {
            data: previewDataSource,
            schema: {
                model: {
                    id: "ArticleID",
                    fields: {
                        ArticleNumber: { type: "string" },
                        ArticleDescription: { type: "string" },
                        UOM: { type: "string" },
                        ArticleType: { type: "string" },
                        Hierlevel3: { type: "string" },
                        MerchandizeCategoryDesc: { type: "string" },
                        MOGName: { type: "string" }
                    }
                }
            },
            pageSize: 100,
        },
        height: 435,
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        dataBound: function (e) {

            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (checkedIds[view[i].id]) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .attr("checked", "checked");
                }
                if (checkedIds[view[i].id] == false) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".checkbox")
                        .removeAttr("checked");
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .find(".mycustomstatus").css("background-color", "#ffe1e1");
                    continue;
                }
                else {
                    if (isShowPreview) {
                        if ((checkedAPLCodes.indexOf("1_" + view[i].ArticleID) != -1) || (checkedAPLCodes.indexOf("0_" + view[i].ArticleID) != -1)) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".mycustomstatus").css("background-color", "#dcf5ff");
                        }
                    }
                    else {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".mycustomstatus").css("background-color", "#dcf5ff");
                    }
                    if (view[i].MOGName != "") {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .find(".checkbox")
                            .attr("checked", "checked");

                    }
                }
            }
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        }
    });
    var grid = gridVariable.data("kendoGrid")
    grid.table.on("click", ".checkbox", selectRow);
}

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        td = $(this).closest("td"),
        row = $(this).closest("tr"),
        grid = $("#gridMOGWiseAPL").data("kendoGrid"),
        dataItem = grid.dataItem(row);
    checkedIds[dataItem.id] = checked;
    if (checked) {
        var filteredAry = checkedAPLCodes.filter(function (e) { return e !== "0_" + dataItem.ArticleID });
        checkedAPLCodes = filteredAry;
        checkedAPLCodes.push("1_" + dataItem.ArticleID);
        CheckedTrueAPLCodes.push(view[i].ArticleID);
    }
    else {
        var filteredAry = checkedAPLCodes.filter(function (e) { return e !== "1_" + dataItem.ArticleID });
        checkedAPLCodes = filteredAry;
        checkedAPLCodes.push("0_" + dataItem.ArticleID);
        //CheckedTrueAPLCodes.pop(dataItem.ArticleID);
        CheckedTrueAPLCodes = CheckedTrueAPLCodes.filter(m => m != dataItem.ArticleID);
    }


}

function preview(e) {
    isShowPreview = false;
    if (e.checked)
        populateAPLMasterGridPreview();
    else
        populateAPLMasterGrid();

    Utility.UnLoading();
}

function showChecked(e) {
    isShowPreview = true;
    if (e.checked)
        populateAPLMasterGridPreview();
    else
        populateAPLMasterGrid();
    Utility.UnLoading();
}

function showMOGImpacts(e) {
    var gridObj = $("#gridMOG").data("kendoGrid");
    var tr = gridObj.dataItem($(e).closest("tr")); // 'e' is the HTML element <a>
    datamodel = tr;
    MOGName = datamodel.Name;
    MOGCode = datamodel.MOGCode;
    MOGID = datamodel.ID;
    getMOGDependenciesData();
    if (mogImpactedData.IsMOGImpacted) {
        showMOGDependencies();
        $("#mogMsg").css('display', 'none');
        $("#btnAgree").css('display', 'none');
        $("#btnDoLater").text("Ok");
        $("#btnDoLater").addClass("btn-info");
        $("#btnDoLater").removeClass("btn-tool");
    }
    else {
        Utility.Page_Alert_Save_Info("No Impact Found!");
        $("#confirmSave_No").css('display', 'none');
        $("#confirmSave_Ok").addClass('colored-btn');
        $("#confirmSave_Ok").removeClass('white-btn');
        Utility.UnLoading();

    }
}

function getMOGDependenciesData() {
    HttpClient.MakeSyncRequest(CookBookMasters.GetMOGImpactedDataList, function (data) {
        mogImpactedData = data;
    }, { mogID: MOGID }, true);
}

function showMOGDependencies() {
    $(".oldmogname").text(MOGName);
    $(".oldmogcode").text(MOGCode);
    $(".newmogname").text(NewMOGName);
    $("#mogMsg").css('display', 'block');
    var dialog = $("#windowEditMOGDependencies").data("kendoWindow");
    dialog.title("MOG Dependencies");
    if (isStatusSave) {
        $("#mogNameChangeMsgDiv").css('display', 'none');
        $("#mogInactiveMsgDiv").css('display', 'block');
        $("#mogAPLMappingMsgDiv").css('display', 'none');
        $("#btnAgree").css('display', 'none');
        $("#btnDoLater").text("Ok");
        $("#btnDoLater").addClass("btn-info");
        $("#btnDoLater").removeClass("btn-tool");

    }
    else if (isMOGAPLMappingSave) {
        $("#mogNameChangeMsgDiv").css('display', 'none');
        $("#mogInactiveMsgDiv").css('display', 'none');
        $("#mogAPLMappingMsgDiv").css('display', 'block');
        $("#btnAgree").css('display', 'none');
        $("#btnDoLater").text("Ok");
        $("#btnDoLater").addClass("btn-info");
        $("#btnDoLater").removeClass("btn-tool");
        $("#MOGwindowEditMOGWiseAPL").parent().hide();
        dialog.title("MOG-APL Dependencies");
    }
    else {
        $("#mogNameChangeMsgDiv").css('display', 'block');
        $("#mogInactiveMsgDiv").css('display', 'none');
        $("#mogAPLMappingMsgDiv").css('display', 'none');
        $("#btnAgree").css('display', 'block');
        $("#btnDoLater").text("I understand the impact, Proceed");
        $("#btnDoLater").removeClass("btn-info");
        $("#btnDoLater").addClass("btn-tool");
    }
   
    $(".k-overlay").css("display", "block");
    $(".k-overlay").css("opacity", "0.5");
    dialog.open();
    dialog.center();
    
    $("#MOGwindowEditMOGChangeConfirm").parent().hide();
    populateMOGDependencies();
}


//$("#showPreview").on("click", function () {
//    isShowPreview = true;
//    populateAPLMasterGridPreview();
//});



function addAPL() {
    var dropdownlist = $("#inputAPL").data("kendoMultiSelect");

    var dataItems = dropdownlist.dataItems();
    for (dataItem of dataItems) {
        dataItem.MOGCode = MOGCode;
        dataItem.MOGName = MOGName;
        ///Multi select array loop and play
        //mogWiseAPLMasterdataSource.push(dataItem)
        mogWiseAPLMasterdataSource.unshift(dataItem);
        checkedAPLCodes.push("1_" + dataItem.ArticleID);
        var grid = $("#gridMOGWiseAPL").data("kendoGrid");
        grid.dataSource.insert(0, dropdownlist.dataItem());
        CheckedTrueAPLCodes.push(dataItem.ArticleID);
        mogWiseAPLArray.push(dataItem.ArticleID);
    }
    var multiselect = $("#inputAPL").data("kendoMultiSelect");
    var modifiedDataSource = aplMasterdataSource.filter(function (item) {
        if (mogWiseAPLArray.indexOf(parseInt(item.ArticleID)) == -1) {
            return item;
        }
    });
    multiselect.value([]);
    multiselect.setDataSource(modifiedDataSource);
}

function checkMOGAplChange() {
    var isChanged = false;
    if (checkedAPLCodes != null && checkedAPLCodes.length > 0) {
        isChanged = true;
    }
    return isChanged;
}

function saveAll() {
    isStatusSave = false;
    isMOGAPLMappingSave = true;
    if (checkMOGAplChange()) {
        $("#confirmSave_No").css('display', 'block');
        $("#confirmSave_Ok").addClass('white-btn');
        $("#confirmSave_Ok").addClass('white-btn');
        $("#confirmSave_Ok").removeClass('colored-btn');
        $("#confirmSave_Box").css('z-index', '99999');
        Utility.Page_Alert_Save("Proceeding will perform change MOG APL mapping. Are you sure to proceed?", "MOG-APL Mapping Change Confirmation", "Yes", "No",
            function () {
                getMOGDependenciesData();
                if (mogImpactedData.IsMOGImpacted) {
                    showMOGDependencies();
                }
                else {
                    saveMOGAPLMapping();
                }
            },
            function () {

            }
        );
    }
    else {
        saveMOGAPLMapping();
    }
}



function saveMOGAPLMapping() {
    var filterData = [];
    mogWiseAPLMasterdataSource.forEach(function (item) {
        if (checkedAPLCodes.indexOf("0_" + item.ArticleID) != -1) {
            item.MOGCode = "";
            item.MOGName = "";
            item.ModifiedDate = Utility.CurrentDate();
            item.CreatedDate = kendo.parseDate(item.CreatedDate);
            item.ModifiedBy = user.UserId;
            filterData.push(item);
        }
        else if (checkedAPLCodes.indexOf("1_" + item.ArticleID) != -1) {
            item.MOGStatus = 3;
            item.ModifiedDate = Utility.CurrentDate();
            item.CreatedDate = kendo.parseDate(item.CreatedDate);
            item.ModifiedBy = user.UserId;
            item.CreatedDate
            filterData.push(item);
        }
    });

    if (filterData == null || filterData.length == 0) {
        toastr.error("Please update or map APL data First.");
        return;
    }
    else {

        $("#btnSubmit").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.SaveAPLMasterDataList, function (result) {
            if (result == false) {
                $('#btnSubmit').removeAttr("disabled");
                toastr.error("Some error occured, please try again");
                $("#inputmog").focus();
            }
            else {
                $(".k-overlay").hide();
                var orderWindow = $("#MOGwindowEditMOGWiseAPL").data("kendoWindow");
                orderWindow.close();
                $('#btnSubmit').removeAttr("disabled");
                HttpClient.MakeSyncRequest(CookBookMasters.SaveMOGData, function (result) {
                    if (result == false) {
                        toastr.error("Some error occured, please try again");
                    }
                    else {
                       // toastr.success("MOG status updated successfully");
                        
                    }
                }, {
                    model: datamodel
                }, true);

                toastr.success("APL updated successfully");
                $("#gridMOG").data("kendoGrid").dataSource.data([]);
                $("#gridMOG").data("kendoGrid").dataSource.read();
            }
        }, {
            model: filterData

        }, true);
    }
}

function populateMOGDependencies() {

    $("#totalImapactedBaseRecipes").text(mogImpactedData.TotalImapactedBaseRecipes);
    $("#totalImapactedFinalRecipes").text(mogImpactedData.TotalImapactedFinalRecipes);
    $("#totalImapactedDishes").text(mogImpactedData.TotalImapactedDishes);
    $("#totalImapactedItems").text(mogImpactedData.TotalImapactedItems);
    $("#totalImapactedSites").text(mogImpactedData.TotalImapactedSites);
    populateBaseRecipesGrid();
    populateFinalRecipesGrid();
    populateDishGrid();
    populateItemGrid();
    populateSiteGrid();
    Utility.UnLoading();
}

function populateBaseRecipesGrid() {
    Utility.Loading();
    var gridVariable = $("#baseRecipeGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        noRecords: true,
        columns: [
            {
                field: "BaseRecipeCode", title: "Base Recipe Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "BaseRecipeName", title: "Base Recipe Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedFinalRecipeName", title: "Impacted Final Recipes (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogImpactedData.MOGImpactedBaseRecipesData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        BaseRecipeCode: { type: "string" },
                        BaseRecipeName: { type: "string" },
                        ImpactedFinalRecipeName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateFinalRecipesGrid() {
    Utility.Loading();
    var gridVariable = $("#finalRecipeGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        noRecords: true,
        columns: [
            {
                field: "FinalRecipeCode", title: "Final Recipe Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "FinalRecipeName", title: "Final Recipe Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedDishName", title: "Impacted Dishes (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogImpactedData.MOGImpactedFinalRecipesData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        BaseFinalCode: { type: "string" },
                        BaseFinalName: { type: "string" },
                        ImpactedDishCode: { type: "string" },
                        ImpactedDishName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateDishGrid() {
    Utility.Loading();
    var gridVariable = $("#dishGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        noRecords: true,
        columns: [
            {
                field: "DishCode", title: "Dish Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishName", title: "Dish Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "DishType", title: "Dish Type", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedItemName", title: "Impacted Item (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogImpactedData.MOGImpactedDishesData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        DishCode: { type: "string" },
                        DishName: { type: "string" },
                        DishType: { type: "string" },
                        ImpactedItemCode: { type: "string" },
                        ImpactedItemName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateItemGrid() {
    Utility.Loading();
    var gridVariable = $("#itemGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        noRecords: true,
        scrollable: true,
        columns: [
            {
                field: "ItemCode", title: "Item Code", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ItemName", title: "Item Name", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ImpactedSiteName", title: "Impacted Site (Names)", width: "80px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogImpactedData.MOGImpactedItemsData,
            schema: {
                model: {
                    // id: "ArticleID",
                    fields: {
                        ItemCode: { type: "string" },
                        ItemName: { type: "string" },
                        ImpactedSiteCode: { type: "string" },
                        ImpactedSiteName: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
    })
}

function populateSiteGrid() {
    Utility.Loading();
    var gridVariable = $("#siteGrid");
    gridVariable.html("");
    gridVariable.kendoGrid({
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        pageable: false,
        groupable: false,
        reorderable: true,
        scrollable: true,
        noRecords: true,
        columns: [
            {
                field: "ArticleNumber", title: "Article Number", width: "50px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "ArticleDescription", title: "Article Description", width: "100px", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "UOM", title: "UOM", width: "30px", attributes: {
                    style: "text-align: left; font-weight:normal"
                },
            },
            {
                field: "ArticleType", title: "Article Type", width: "40px", attributes: {

                    style: "text-align: left; font-weight:normal"
                },
            },
        ],
        dataSource: {
            data: mogWiseAPLMasterdataSource,
            schema: {
                model: {
                    id: "ArticleID",
                    fields: {
                        ArticleNumber: { type: "string" },
                        ArticleDescription: { type: "string" },
                        UOM: { type: "string" },
                        ArticleType: { type: "string" },
                        Hierlevel3: { type: "string" },
                        MerchandizeCategoryDesc: { type: "string" },
                        MOGName: { type: "string" }
                    }
                }
            },
            pageSize: 100,
        },
    })
}

$("document").ready(function () {
    $(".k-i-close").on("click", function () {
        $(".k-overlay").hide();

    });
});
