alter table SiteRecipeMOGMapping
add ArticleNumber varchar(50)

Go


ALTER table MOG 
add AllergensName nvarchar(500)

Go

ALTER table RecipeMOGMapping 
add AllergensName nvarchar(500)

GO

ALTER table Recipe 
add AllergensName nvarchar(500)

Go

ALTER table Dish 
add AllergensName nvarchar(500)

Go

ALTER table Item 
add AllergensName nvarchar(500)