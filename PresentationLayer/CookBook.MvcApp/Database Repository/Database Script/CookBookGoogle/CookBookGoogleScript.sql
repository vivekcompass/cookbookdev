USE [CookBookGoogle]
GO
/****** Object:  StoredProcedure [dbo].[procGetAPLMasterDataForSite]    Script Date: 7/6/2021 10:34:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  procedure [dbo].[procGetAPLMasterDataForSite] @ArticleNumber nvarchar(20) = null, @MOGCode nvarchar(10) = null,@SiteCode  nvarchar(10) = null    
as     
begin    
     select a.ArticleID,
			a.ArticleNumber,
			a.ArticleDescription,a.UOM,
			a.MerchandizeCategory,
			a.MerchandizeCategoryDesc,
			a.IsActive,
			a.CreatedBy,
			a.CreatedDate,
			a.ModifiedBy,
			a.ModifiedDate,
			a.ArticleType,
			a.Hierlevel3, 
			a.MOGCode,
			m.Name as MOGName, 
			apl.StandardCostPerKg,
			apl.ArticleCost,
			STUFF((SELECT ', ' + al.Name
				   FROM cookbookcommon..AllergenAPLMapping aam
				   left join cookbookcommon..Allergen al on al.AllergenCode = aam.AllergenCode
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensName,
			STUFF((SELECT ', ' + aam.AllergenCode
				   FROM cookbookcommon..AllergenAPLMapping aam
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensCode
     from   [CookBookCommon].[dbo].UniqueAPLMaster a    
            left join    
            MOG m    
            on a.MOGCode = m.MOGCode   
	        inner Join CookBookCommon.dbo.APLMaster apl 
			on a.ArticleNumber = apl.ArticleNumber and  apl.Site = @SiteCode 
     where  (@ArticleNumber is null or (a.ArticleNumber = @ArticleNumber)) and    
            (@MOGCode is null or (a.MOGCode = @MOGCode))  and  
            a.ArticleType in ('ZFOD', 'ZPRP') and apl.IsActive = 1
   order by apl.StandardCostPerKg desc
end    
    
Go

USE [CookBookCore]
GO
/****** Object:  StoredProcedure [dbo].[procGetSiteMOGsByRecipeID]    Script Date: 7/6/2021 11:47:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[procGetSiteMOGsByRecipeID] @RecipeID int,@RecipeCode nvarchar(10),@siteCode nvarchar(10)
as
begin
select rm.ID,
       rm.Recipe_ID,
       r.Name as RecipeName,
	   rm.MOG_ID,
	   rm.MOGCode,
	 --  m.Name as MOGName,
	    CASE
      WHEN m.Alias is NULL or m.Alias='' or m.Alias=m.Name then m.Name
    ELSE m.Name+' ('+m.Alias+') 'end  as MOGName,
	   rm.Quantity,
	   rm.Yield,
	   rm.UOM_ID,
	   u.Name as UOMName,
	   rm.IsActive,
	   rm.CreatedBy,
	   rm.CreatedOn,
	   rm.ModifiedOn,
	   rm.ModifiedBy,
	   dbo.fnMathRoundOff(rm.CostPerUOM,2)  as CostPerUOM,
	   dbo.fnMathRoundOff(rm.CostPerKG,2)  as CostPerKG,
	   dbo.fnMathRoundOff(rm.TotalCost,2)  as TotalCost,
	   rm.UOMCode,
	   rm.IngredientPerc,
	   rm.RegionIngredientPerc,
	   rm.Site_ID,
	   rm.SiteCode,
	   rm.Region_ID,
	    rm.IsMajor,
	   rm.RegionTotalCost,
	   rm.RegionQuantity,
	   rm.ArticleNumber
from   SiteRecipeMOGMapping rm
       left join
	   SiteRecipe r
	   on rm.RecipeCode = r.RecipeCode and r.SiteCode = @siteCode
	   left join
	   UOM u
	   on rm.UOMCode = u.UOMCode
	   inner join
	   MOG m
	   on rm.MOGCode = m.MOGCode
where  rm.Recipe_ID = @RecipeID or rm.RecipeCode = @RecipeCode
       and rm.SiteCode = @siteCode order by rm.ID
end


GO
/****** Object:  StoredProcedure [dbo].[SaveRegionToSiteInheritance]    Script Date: 7/8/2021 5:39:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[SaveRegionToSiteInheritance] 
@SiteCode nvarchar(10),
@ItemCode nvarchar(max),
@UserID int,
@IsActive bit,
@SubSectorCode nvarchar(10)
as
begin

BEGIN TRY
   IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#TempSiteItem]') AND type in (N'U'))
      DROP TABLE [dbo].[#TempSiteItem]

    CREATE TABLE #TempSiteItem (
	   ItemCode nvarchar(20) ,IsActive bit
	)

	declare @Site_ID as int
	declare @Region_ID as int

	select @Site_ID = SiteID, @Region_ID = RegionID, @SubSectorCode = SubSectorCode from CookBookCommon.dbo.SiteMaster where SiteCode = @SiteCode

    insert into #TempSiteItem
	SELECT SUBSTRING(tuple,3,LEN(tuple)) as ItemCode,Cast(SUBSTRING(tuple,1,1) as bit) as IsActive
	FROM   split_string(@ItemCode, ',')

	declare @cntitem as int = 0
	declare @cntdc as int = 0
	declare @cntdsh as int = 0

	set @cntitem = isnull((select isnull(COUNT(1),0) from SiteItemInheritanceMapping where ItemCode in (select ItemCode from #TempSiteItem) and SiteCode = @SiteCode),0)
	set @cntdc = isnull((select isnull(COUNT(1),0) from SiteDishCategoryMapping where ItemCode in (select ItemCode from #TempSiteItem) and SiteCode = @SiteCode),0)
	set @cntdsh = isnull((select isnull(COUNT(1),0) from SiteItemDishMapping where ItemCode in (select ItemCode from #TempSiteItem)  and SiteCode = @SiteCode),0)
	
	if (@cntitem > 0) begin
	   delete from SiteItemInheritanceMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
	end

		IF EXISTS( select 1 from SiteItemInheritanceMapping where Site_ID=@Site_ID  and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RIM
			set RIM.Site_ID=@Site_ID,RIM.Item_ID=i.ID ,RIM.IsActive = 1, RIM.ModifiedBy = @UserID, RIM.ModifiedOn = getdate(), RIM.Status=1                 
			from SiteItemInheritanceMapping RIM 
			join 
			(select ItemCode, Site_ID from SiteItemInheritanceMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RIM2
			on  RIM2.ItemCode = RIM.ItemCode and RIM2.Site_ID = RIM.Site_ID
		    join Item i 
			on  RIM2.ItemCode = I.ItemCode
		end
		else
		begin
		    insert into SiteItemInheritanceMapping(
			Site_ID,SiteCode,Item_ID,ItemCode,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,Status) 
			select @Site_ID,@SiteCode ,ID, ItemCode, @IsActive, @UserID, GETDATE(), null, null,1 -- 1 for pending
			from   Item i where ItemCode 
			IN (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteItemInheritanceMapping where Site_ID = @Site_ID  )) and i.IsActive =1
		end

		select @IsActive

	if(@IsActive=1)
	begin

		if (@cntdc > 0) begin
		   delete from SiteDishCategoryMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
		end
		
		IF EXISTS( select 1 from SiteDishCategoryMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RDCM
			set RDCM.IsActive = IDCM.IsActive, RDCM.StandardPortion = IDCM.StandardPortion, RDCM.UOMCode = IDCM.UOMCode, RDCM.ModifiedBy =@UserID, RDCM.ModifiedOn = getdate()                 
			from SiteDishCategoryMapping RDCM
			join 
			(select DishCategoryCode, ItemCode, Site_ID from SiteDishCategoryMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RDCM2
			on RDCM2.DishCategoryCode = RDCM.DishCategoryCode and RDCM2.ItemCode = RDCM.ItemCode and RDCM2.Site_ID = RDCM.Site_ID
		    join RegionDishCategoryMapping IDCM 
			on RDCM2.DishCategoryCode = IDCM.DishCategoryCode and RDCM2.ItemCode = IDCM.ItemCode and Region_ID = @Region_ID
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end
		else
		begin
		    insert into SiteDishCategoryMapping 
			select @Site_ID, @SiteCode, Item_ID, DishCategory_ID, @IsActive, @UserID, getdate(), null,null, ItemCode, DishCategoryCode, StandardPortion,UOMCode,WeightPerUOM
			from   RegionDishCategoryMapping IDCM where ItemCode 
			in (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteDishCategoryMapping where Site_ID =@Site_ID ))
			and Region_ID= @Region_ID and IDCM.IsActive=1 
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end

		insert into SiteDishCategory
		select rdcm.SiteCode, rdcm.DishCategoryCode, 0,0,0,null,1,@UserID,getdate(),null,null
		from
		       (
			    select distinct SiteCode, DishCategoryCode
			    from   SiteDishCategoryMapping
			   ) rdcm
		       left join 
			   SiteDishCategory rdc
			   on rdcm.SiteCode = rdc.SiteCode and rdcm.DishCategoryCode = rdc.DishCategoryCode
        where  rdc.SiteCode is null and rdc.DishCategoryCode is null
	
		if (@cntdsh > 0) begin
		   delete from SiteItemDishMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
		end
		
		IF EXISTS( select 1 from SiteItemDishMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RIM
			set RIM.Site_ID=@Site_ID,RIM.Item_ID=i.Item_ID ,RIM.Dish_ID=i.Dish_ID, RIM.IsActive = i.IsActive, RIM.ModifiedBy = @UserID, RIM.ModifiedOn = getdate(), RIM.SiteCode=@SiteCode,
			RIM.DishCode =i.DishCode, RIM.StandardPortion=I.StandardPortion ,RIM.ContractPortion = i.ContractPortion,
			RIM.ContractUOMCode = i.UOMCode, RIM.ContractWeightPerUOM= i.WeightPerUOM, RIM.ServedUOMCode =i.UOMCode, RIM.ServedWeightPerUOM =i.WeightPerUOM
			,RIM.StandardUOMCode = i.UOMCode, RIM.StandardWeightPerUOM = i.WeightPerUOM
			from SiteItemDishMapping RIM 
			join 
			(select ItemCode, Site_ID,DishCode from SiteItemDishMapping where Site_ID=@Site_ID 
			and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RIM2
			on  RIM2.ItemCode = RIM.ItemCode and RIM2.Site_ID = RIM.Site_ID and RIM2.DishCode = RIM.DishCode
		    join RegionItemDishMapping i 
			on  RIM2.ItemCode = i.ItemCode and I.DishCode =RIM2.DishCode and i.Region_ID = @Region_ID and i.IsActive=1
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end
		else
		begin
		 
		    insert into SiteItemDishMapping 
			select @Site_ID, Item_ID, Dish_ID, IsActive, @UserID,getdate(), null,null, @SiteCode, ItemCode, DishCode,ContractPortion, StandardPortion, ContractPortion,null, WeightPerUOM, UOMCode,WeightPerUOM, UOMCode,WeightPerUOM, UOMCode,null,null
			from   RegionItemDishMapping i where ItemCode 
			IN (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteItemDishMapping where Site_ID = @Site_ID ))
			and Region_ID = @Region_ID and i.IsActive =1
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end

		insert into SiteDish
		select rdcm.SiteCode, rdcm.DishCode, null,0,null,1,@UserID,getdate(),null,null
		from
		       (
			    select distinct SiteCode, DishCode
			    from   SiteItemDishMapping
			   ) rdcm
		       left join 
			   SiteDish rdc
			   on rdcm.SiteCode = rdc.SiteCode and rdcm.DishCode = rdc.DishCode
        where  rdc.SiteCode is null and rdc.DishCode is null
		
		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#TempItemDish]') AND type in (N'U'))
		  DROP TABLE [dbo].[#TempItemDish]

		CREATE TABLE #TempItemDish (
		   DishCode nvarchar(20) 
		)

		insert into #TempItemDish
		select  DishCode
		from   SiteItemDishMapping where ItemCode in (select ItemCode from #TempSiteItem ti where ti.IsActive=1)

		While ((select count(1) from #TempItemDish) > 0)
		begin
			 DECLARE @DishCode varchar(20)
			 SELECT TOP 1 @DishCode = DishCode FROM   #TempItemDish
				exec procSaveSiteItemDishRecipeMapping @Site_ID, @SiteCode, @DishCode, @UserID
			 Delete from #TempItemDish where  DishCode = @DishCode 
		end
    end
	DROP TABLE [dbo].[#TempSiteItem]

	select 'True' as Result
END TRY  
BEGIN CATCH  
    SELECT ERROR_MESSAGE() as Result
END CATCH  
end


Go

GO
/****** Object:  StoredProcedure [dbo].[procSaveSiteItemDishRecipeMapping]    Script Date: 7/8/2021 6:32:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procSaveSiteItemDishRecipeMapping] (
  @Site_ID int
 ,@SiteCode  nvarchar(10)
 ,@DishCode  nvarchar(10)
 ,@CreatedBy int
)
as
begin
    declare @Region_ID as int = 0
    set @Region_ID = (select RegionID from CookBookCommon.dbo.SiteMaster where SiteID= @Site_ID)

	If not exists (select * from SiteDishRecipeMapping where  Site_ID = @Site_ID and DishCode = @DishCode)
	begin
	    insert into SiteDishRecipeMapping
		select @Site_ID, @SiteCode, Dish_ID, Recipe_ID, IsActive, @CreatedBy, GETDATE(),null,null, DishCode, RecipeCode, IsDefault from DishRecipeMapping where DishCode = @DishCode
	end

	IF OBJECT_ID('TempDB..#TEMPSiteRecipeList') IS NOT NULL DROP TABLE #TEMPSiteRecipeList
		SELECT  a.Site_ID,a.SiteCode,a.RecipeCode
		into    #TEMPSiteRecipeList
		FROM    (
				 select distinct rdrm.Site_ID,rdrm.SiteCode, rdrm.RecipeCode
				 from   SiteDishRecipeMapping rdrm left join SiteRecipe rr 
				 	    on rdrm.Site_ID = rr.Site_ID and rdrm.RecipeCode = rr.RecipeCode
				 where  rdrm.Site_ID = @Site_ID and rdrm.DishCode = @DishCode and rr.Site_ID is null and rr.RecipeCode is null
				) as a


	While ((select count(1) from #TEMPSiteRecipeList) > 0)
	begin
	     DECLARE @SiteID int
	     DECLARE @Site_Code varchar(10)
		 DECLARE @RecipeCode varchar(20)
		 SELECT TOP 1 @SiteID = Site_ID,@Site_Code= SiteCode ,@RecipeCode = RecipeCode 
		 FROM   #TEMPSiteRecipeList

		 If not exists (select * from   SiteRecipe where  Site_ID = @SiteID and RecipeCode = @RecipeCode)
	     begin
		     declare @RecipeCost decimal(18,3)
		     exec procGetRecipeCostAtSite @RecipeCode, @Site_ID, @RecipeCost output
			 insert into SiteRecipe
	   	     select @SiteID,@Site_Code,@Region_ID ,Name, UOM_ID, Quantity, Instructions, IsBase, IsActive, @CreatedBy, GETDATE(), null,null, @RecipeCode, UOMCode,@RecipeCost/10, @RecipeCost/10,@RecipeCost,@RecipeCost/10, @RecipeCost/10,@RecipeCost,Status
			 from   Recipe 
			 where  RecipeCode = @RecipeCode
			   exec procSaveUnitBaseRecipeData @SiteID,@Site_Code,@RecipeCode,@CreatedBy
			 if not exists (select * from SiteRecipeMOGMapping where Site_ID = @Site_ID and RecipeCode = @RecipeCode)
			  
			begin
			     insert into SiteRecipeMOGMapping
				 select  @Site_ID,@SiteCode,@Region_ID ,Recipe_ID, MOG_ID, UOM_ID, BaseRecipe_ID, Quantity, 1, @CreatedBy, getdate(), null,null, Yield,
					     @RecipeCode, MOGCode, BaseRecipeCode, UOMCode, CostPerUOM, CostPerKG, TotalCost, CostAsOfDate,IngredientPerc
						 ,IsMajor,  IngredientPerc,TotalCost, Quantity,NULL
				 from    RecipeMOGMapping
				 where   RecipeCode = @RecipeCode
			 end
	     end
		 else 
		 begin
			if not exists (select * from SiteRecipeMOGMapping where Site_ID = @Site_ID and RecipeCode = @RecipeCode)
			 begin
			 exec procSaveUnitBaseRecipeData @SiteID,@Site_Code,@RecipeCode,@CreatedBy
			     insert into SiteRecipeMOGMapping
				 select  @Site_ID,@SiteCode,@Region_ID ,Recipe_ID, MOG_ID, UOM_ID, BaseRecipe_ID, Quantity, 1, @CreatedBy, getdate(), null,null, Yield,
					     @RecipeCode, MOGCode, BaseRecipeCode, UOMCode, CostPerUOM, CostPerKG, TotalCost, CostAsOfDate,IngredientPerc,IsMajor, IngredientPerc ,
						 TotalCost, Quantity,null
				 from    RecipeMOGMapping
				 where   RecipeCode = @RecipeCode
			 end
		 end
	  Delete from #TEMPSiteRecipeList where @SiteID = Site_ID and  @RecipeCode = RecipeCode 
	end

end


Go

USE [CookBookGoogle]
GO
/****** Object:  StoredProcedure [dbo].[SaveRegionToSiteInheritance]    Script Date: 7/8/2021 6:42:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[SaveRegionToSiteInheritance] 
@SiteCode nvarchar(10),
@ItemCode nvarchar(max),
@UserID int,
@IsActive bit,
@SubSectorCode nvarchar(10) = NULL
as
begin

BEGIN TRY
   IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#TempSiteItem]') AND type in (N'U'))
      DROP TABLE [dbo].[#TempSiteItem]

    CREATE TABLE #TempSiteItem (
	   ItemCode nvarchar(20) ,IsActive bit
	)

	declare @Site_ID as int
	declare @Region_ID as int

	select @Site_ID = SiteID, @Region_ID = RegionID, @SubSectorCode = SubSectorCode from CookBookCommon.dbo.SiteMaster where SiteCode = @SiteCode

    insert into #TempSiteItem
	SELECT SUBSTRING(tuple,3,LEN(tuple)) as ItemCode,Cast(SUBSTRING(tuple,1,1) as bit) as IsActive
	FROM   split_string(@ItemCode, ',')

	declare @cntitem as int = 0
	declare @cntdc as int = 0
	declare @cntdsh as int = 0

	set @cntitem = isnull((select isnull(COUNT(1),0) from SiteItemInheritanceMapping where ItemCode in (select ItemCode from #TempSiteItem) and SiteCode = @SiteCode),0)
	set @cntdc = isnull((select isnull(COUNT(1),0) from SiteDishCategoryMapping where ItemCode in (select ItemCode from #TempSiteItem) and SiteCode = @SiteCode),0)
	set @cntdsh = isnull((select isnull(COUNT(1),0) from SiteItemDishMapping where ItemCode in (select ItemCode from #TempSiteItem)  and SiteCode = @SiteCode),0)
	
	if (@cntitem > 0) begin
	   delete from SiteItemInheritanceMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
	end

		IF EXISTS( select 1 from SiteItemInheritanceMapping where Site_ID=@Site_ID  and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RIM
			set RIM.Site_ID=@Site_ID,RIM.Item_ID=i.ID ,RIM.IsActive = 1, RIM.ModifiedBy = @UserID, RIM.ModifiedOn = getdate(), RIM.Status=1                 
			from SiteItemInheritanceMapping RIM 
			join 
			(select ItemCode, Site_ID from SiteItemInheritanceMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RIM2
			on  RIM2.ItemCode = RIM.ItemCode and RIM2.Site_ID = RIM.Site_ID
		    join Item i 
			on  RIM2.ItemCode = I.ItemCode
		end
		else
		begin
		    insert into SiteItemInheritanceMapping(
			Site_ID,SiteCode,Item_ID,ItemCode,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,Status) 
			select @Site_ID,@SiteCode ,ID, ItemCode, @IsActive, @UserID, GETDATE(), null, null,1 -- 1 for pending
			from   Item i where ItemCode 
			IN (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteItemInheritanceMapping where Site_ID = @Site_ID  )) and i.IsActive =1
		end

		select @IsActive

	if(@IsActive=1)
	begin

		if (@cntdc > 0) begin
		   delete from SiteDishCategoryMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
		end
		
		IF EXISTS( select 1 from SiteDishCategoryMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RDCM
			set RDCM.IsActive = IDCM.IsActive, RDCM.StandardPortion = IDCM.StandardPortion, RDCM.UOMCode = IDCM.UOMCode, RDCM.ModifiedBy =@UserID, RDCM.ModifiedOn = getdate()                 
			from SiteDishCategoryMapping RDCM
			join 
			(select DishCategoryCode, ItemCode, Site_ID from SiteDishCategoryMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RDCM2
			on RDCM2.DishCategoryCode = RDCM.DishCategoryCode and RDCM2.ItemCode = RDCM.ItemCode and RDCM2.Site_ID = RDCM.Site_ID
		    join RegionDishCategoryMapping IDCM 
			on RDCM2.DishCategoryCode = IDCM.DishCategoryCode and RDCM2.ItemCode = IDCM.ItemCode and Region_ID = @Region_ID
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end
		else
		begin
		    insert into SiteDishCategoryMapping 
			select @Site_ID, @SiteCode, Item_ID, DishCategory_ID, @IsActive, @UserID, getdate(), null,null, ItemCode, DishCategoryCode, StandardPortion,UOMCode,WeightPerUOM
			from   RegionDishCategoryMapping IDCM where ItemCode 
			in (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteDishCategoryMapping where Site_ID =@Site_ID ))
			and Region_ID= @Region_ID and IDCM.IsActive=1 
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end

		insert into SiteDishCategory
		select rdcm.SiteCode, rdcm.DishCategoryCode, 0,0,0,null,1,@UserID,getdate(),null,null
		from
		       (
			    select distinct SiteCode, DishCategoryCode
			    from   SiteDishCategoryMapping
			   ) rdcm
		       left join 
			   SiteDishCategory rdc
			   on rdcm.SiteCode = rdc.SiteCode and rdcm.DishCategoryCode = rdc.DishCategoryCode
        where  rdc.SiteCode is null and rdc.DishCategoryCode is null
	
		if (@cntdsh > 0) begin
		   delete from SiteItemDishMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
		end
		
		IF EXISTS( select 1 from SiteItemDishMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RIM
			set RIM.Site_ID=@Site_ID,RIM.Item_ID=i.Item_ID ,RIM.Dish_ID=i.Dish_ID, RIM.IsActive = i.IsActive, RIM.ModifiedBy = @UserID, RIM.ModifiedOn = getdate(), RIM.SiteCode=@SiteCode,
			RIM.DishCode =i.DishCode, RIM.StandardPortion=I.StandardPortion ,RIM.ContractPortion = i.ContractPortion,
			RIM.ContractUOMCode = i.UOMCode, RIM.ContractWeightPerUOM= i.WeightPerUOM, RIM.ServedUOMCode =i.UOMCode, RIM.ServedWeightPerUOM =i.WeightPerUOM
			,RIM.StandardUOMCode = i.UOMCode, RIM.StandardWeightPerUOM = i.WeightPerUOM
			from SiteItemDishMapping RIM 
			join 
			(select ItemCode, Site_ID,DishCode from SiteItemDishMapping where Site_ID=@Site_ID 
			and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RIM2
			on  RIM2.ItemCode = RIM.ItemCode and RIM2.Site_ID = RIM.Site_ID and RIM2.DishCode = RIM.DishCode
		    join RegionItemDishMapping i 
			on  RIM2.ItemCode = i.ItemCode and I.DishCode =RIM2.DishCode and i.Region_ID = @Region_ID and i.IsActive=1
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end
		else
		begin
		 
		    insert into SiteItemDishMapping 
			select @Site_ID, Item_ID, Dish_ID, IsActive, @UserID,getdate(), null,null, @SiteCode, ItemCode, DishCode,ContractPortion, StandardPortion, ContractPortion,null, WeightPerUOM, UOMCode,WeightPerUOM, UOMCode,WeightPerUOM, UOMCode,null,null
			from   RegionItemDishMapping i where ItemCode 
			IN (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteItemDishMapping where Site_ID = @Site_ID ))
			and Region_ID = @Region_ID and i.IsActive =1
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end

		insert into SiteDish
		select rdcm.SiteCode, rdcm.DishCode, null,0,null,1,@UserID,getdate(),null,null
		from
		       (
			    select distinct SiteCode, DishCode
			    from   SiteItemDishMapping
			   ) rdcm
		       left join 
			   SiteDish rdc
			   on rdcm.SiteCode = rdc.SiteCode and rdcm.DishCode = rdc.DishCode
        where  rdc.SiteCode is null and rdc.DishCode is null
		
		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#TempItemDish]') AND type in (N'U'))
		  DROP TABLE [dbo].[#TempItemDish]

		CREATE TABLE #TempItemDish (
		   DishCode nvarchar(20) 
		)

		insert into #TempItemDish
		select  DishCode
		from   SiteItemDishMapping where ItemCode in (select ItemCode from #TempSiteItem ti where ti.IsActive=1)

		While ((select count(1) from #TempItemDish) > 0)
		begin
			 DECLARE @DishCode varchar(20)
			 SELECT TOP 1 @DishCode = DishCode FROM   #TempItemDish
				exec procSaveSiteItemDishRecipeMapping @Site_ID, @SiteCode, @DishCode, @UserID
			 Delete from #TempItemDish where  DishCode = @DishCode 
		end
    end
	DROP TABLE [dbo].[#TempSiteItem]

	select 'True' as Result
END TRY  
BEGIN CATCH  
    SELECT ERROR_MESSAGE() as Result
END CATCH  
end


Go