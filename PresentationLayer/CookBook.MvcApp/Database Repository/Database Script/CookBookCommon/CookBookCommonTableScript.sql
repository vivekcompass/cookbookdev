GO

CREATE TABLE [dbo].[AllergenAPLMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NULL,
	[AllergenCode] [nvarchar](20) NULL,
	[ArticleNumber] [nvarchar](20) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_AllergenAPLMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO




USE [CookBookCommon]
GO

/****** Object:  Table [dbo].[CapringMaster]    Script Date: 7/30/2021 5:53:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CapringMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](10) NULL,
	[GroupCode] [nvarchar](10) NULL,
	[IsActive] [bit] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[Remarks] [nvarchar](100) NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [varchar](500) NULL,
	[SectorCode] [int] NULL,
	[LevelCode] [int] NULL,
	[PermissionCode] [int] NULL,
	[FunctionCode] [nvarchar](5) NULL,
 CONSTRAINT [PK_CapringMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

go
create proc procGetCapringMasterData  
as  
begin   
select cm.ID,cm.Code as CAPRINGCODE,cm.Description,cm.SectorCode,sm.SectorName,lm.Code as LevelCode,lm.Name as LevelName,
fm.Code as FunctionCode,fm.Name as FunctionName,pm.Code as PermissionCode,pm.Type as PermissionName,
cm.IsActive,cm.CreatedOn,cm.CreatedBy,cm.ModifiedOn,cm.ModifiedBy
,sm.SectorCode as SCODE,  
lm.Name,pm.Type as typ from CapringMaster cm  
left join SectorMaster sm on cm.SectorCode=sm.SectorNumber  
left join LevelMaster lm on cm.LevelCode=lm.Code  
left join PermissionMaster pm on cm.PermissionCode=pm.Code  
left join FunctionalityMaster fm on cm.FunctionCode=fm.Code  

end 



go
CREATE proc ProcUpdateCapringDescriptionInfo    
(    
 @ID int,    
 @code nvarchar(20),    
 @Description nvarchar(500),    
 @ModifiedBy int,    
 @SectorCode int,    
 @LevelCode int,    
 @FunctionCode nvarchar(5),    
 @PermissionCode int,    
 @Result varchar(30)output    
)    
as    
begin    
    
       update CapringMaster set Description=@Description,ModifiedBy=@ModifiedBy,ModifiedOn=GETDATE()   
       where ID=@ID    
        
      select @Result='Success'    
      
end



go

CREATE proc ProcUpdateCapringInfo  
(  
 @ID int,  
 @code nvarchar(20),  
 @Description nvarchar(500),  
 @ModifiedBy int,  
 @SectorCode int,  
 @LevelCode int,  
 @FunctionCode nvarchar(5),  
 @PermissionCode int,  
 @Result varchar(30)output  
)  
as  
begin  
 if not exists (select Code from CapringMaster where Code=@code)  
    begin   
       update CapringMaster set Code=@code,Description=@Description,ModifiedBy=@ModifiedBy,ModifiedOn=GETDATE(),  
    SectorCode=@SectorCode,LevelCode=@LevelCode,FunctionCode=@FunctionCode,PermissionCode=@PermissionCode where ID=@ID  
      
      select @Result='Success'  
    end  
    else begin  
      select @Result='Already Exist'  
    end  
end

go
CREATE proc ProcSaveCapringInfo  
(  
 @code nvarchar(20),  
 @Description nvarchar(500),  
 @CreatedBy int,  
 @SectorCode int,  
 @LevelCode int,  
 @FunctionCode nvarchar(5),  
 @PermissionCode int,  
 @Result varchar(30)output  
)  
as  
begin  
 if not exists (select Code from CapringMaster where Code=@code)  
    begin   
       insert into CapringMaster(Code,Description,IsActive,CreatedBy,CreatedOn,SectorCode,LevelCode,FunctionCode,PermissionCode)   
    Values (@code,@Description,1,@CreatedBy,GETDATE(),@SectorCode,@LevelCode,@FunctionCode,@PermissionCode)  
      select @Result='Success'  
    end  
    else begin  
      select @Result='Already Exist'  
    end  
end

go
create proc procUpdateCapringActiveInactiveData    
(    
 @ID int,    
 @IsActive bit,    
 @ModifiedBy int,    
 @Result varchar(30)output    
)    
as    
begin    
     
    
      update CapringMaster set IsActive=@IsActive,ModifiedBy=@ModifiedBy,ModifiedOn=GETDATE() where ID=@ID    
      select @Result='Success'    
       
end

go
CREATE proc procGetCapringSectorMaster    
as    
begin    
select distinct SectorCode,SectorName from SectorMaster where IsActive=1    
end  

go
CREATE proc procGetCapringLevelMaster  
as  
begin  
select distinct Code,Name from LevelMaster where IsActive=1  
end

go
CREATE proc procGetCapringFunctionMaster  
as  
begin  
select distinct Code,Name from FunctionalityMaster where IsActive=1  
end

go
CREATE proc procGetCapringPermissionMaster  
as  
begin  
select distinct Code,Type as typ from PermissionMaster where IsActive=1  
end  