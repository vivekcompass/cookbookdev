GO
/****** Object:  StoredProcedure [dbo].[procGetAPLMasterData]    Script Date: 7/5/2021 10:16:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetAPLMasterData] @ArticleNumber nvarchar(20) = null, @MOGCode nvarchar(10) = null          
as           
begin          
     select ArticleID, a.ArticleNumber,      
            ArticleDescription,UOM,MerchandizeCategory,      
            MerchandizeCategoryDesc,a.IsActive,      
            a.CreatedBy,a.CreatedDate,      
            a.ModifiedBy,a.ModifiedDate,      
            ArticleType,Hierlevel3, 
			a.MOGCode,  m.Name as MOGName, 
			apl.StandardCostPerKg,
			apl.ArticleCost,
			apl.SiteName,
			apl.SiteAlias,
			apl.SiteCode,
			STUFF((SELECT ', ' + al.Name
				   FROM AllergenAPLMapping aam
				   left join Allergen al on al.AllergenCode = aam.AllergenCode
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensName,
			STUFF((SELECT ', ' + aam.AllergenCode
				   FROM AllergenAPLMapping aam
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensCode
     from   [CookBookCommon].[dbo].UniqueAPLMaster a          
            left join  NationalMOG m  on a.MOGCode = m.MOGCode        
			inner join  (Select   SiteName,SiteCode,SiteAlias,ArticleNumber,Max(StandardCostPerKg) StandardCostPerKg 
								  ,max(ArticleCost) ArticleCost      
								  from   CookBookCommon.dbo.APLMaster am      
								  inner join  CookBookCommon.dbo.SiteMaster sm      
								  on am.Site = sm.SiteCode and sm.SectorCode = '30'      
								  where  StandardCostPerKg > 0 and ArticleType in ('ZFOD', 'ZPRP') and am.IsActive = 1      
								  group by ArticleNumber ,SiteName,SiteCode,SiteAlias      
						) as apl       
			on  a.ArticleNumber = apl.ArticleNumber       
		    where  (@ArticleNumber is null or (a.ArticleNumber = @ArticleNumber)) and          
				   (@MOGCode is null or (a.MOGCode = @MOGCode))  and  ArticleType in ('ZFOD', 'ZPRP')      
			order by apl.StandardCostPerKg desc      
end 

Go

USE [CookBookCommon]
GO
/****** Object:  StoredProcedure [dbo].[procGetAPLMasterData]    Script Date: 7/12/2021 4:42:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetAPLMasterData] @ArticleNumber nvarchar(20) = null, @MOGCode nvarchar(10) = null          
as           
begin          
     select ArticleID, a.ArticleNumber,      
            ArticleDescription,UOM,MerchandizeCategory,      
            MerchandizeCategoryDesc,a.IsActive,      
            a.CreatedBy,a.CreatedDate,      
            a.ModifiedBy,a.ModifiedDate,      
            ArticleType,Hierlevel3, 
			a.MOGCode,  m.Name as MOGName, 
			apl.StandardCostPerKg,
			apl.ArticleCost,
			apl.SiteName,
			apl.SiteAlias,
			apl.SiteCode,
			STUFF((SELECT ', ' + al.Name
				   FROM AllergenAPLMapping aam
				   left join Allergen al on al.AllergenCode = aam.AllergenCode
				   WHERE aam.ArticleNumber = a.ArticleNumber and aam.IsActive=1
				  FOR XML PATH('')), 1, 2, '') as AllergensName,
			STUFF((SELECT ', ' + aam.AllergenCode
				   FROM AllergenAPLMapping aam
				   WHERE aam.ArticleNumber = a.ArticleNumber and aam.IsActive=1
				  FOR XML PATH('')), 1, 2, '') as AllergensCode
     from   [CookBookCommon].[dbo].UniqueAPLMaster a          
            left join  NationalMOG m  on a.MOGCode = m.MOGCode        
			inner join  (Select   SiteName,SiteCode,SiteAlias,ArticleNumber,Max(StandardCostPerKg) StandardCostPerKg 
								  ,max(ArticleCost) ArticleCost      
								  from   CookBookCommon.dbo.APLMaster am      
								  inner join  CookBookCommon.dbo.SiteMaster sm      
								  on am.Site = sm.SiteCode and sm.SectorCode = '30'      
								  where  StandardCostPerKg > 0 and ArticleType in ('ZFOD', 'ZPRP') and am.IsActive = 1      
								  group by ArticleNumber ,SiteName,SiteCode,SiteAlias      
						) as apl       
			on  a.ArticleNumber = apl.ArticleNumber       
			--left join AllergenAPLMapping aam on aam.ArticleNumber = a.ArticleNumber
		    where  (@ArticleNumber is null or (a.ArticleNumber = @ArticleNumber)) and          
				   (@MOGCode is null or (a.MOGCode = @MOGCode))  and  ArticleType in ('ZFOD', 'ZPRP')      
			order by apl.StandardCostPerKg desc      
end 


Go

USE [CookBookCommon]
GO
/****** Object:  StoredProcedure [dbo].[procGetAPLMasterData]    Script Date: 7/12/2021 4:42:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetAPLMasterData] @ArticleNumber nvarchar(20) = null, @MOGCode nvarchar(10) = null          
as           
begin          
     select ArticleID, a.ArticleNumber,      
            ArticleDescription,UOM,MerchandizeCategory,      
            MerchandizeCategoryDesc,a.IsActive,      
            a.CreatedBy,a.CreatedDate,      
            a.ModifiedBy,a.ModifiedDate,      
            ArticleType,Hierlevel3, 
			a.MOGCode,  m.Name as MOGName, 
			apl.StandardCostPerKg,
			apl.ArticleCost,
			apl.SiteName,
			apl.SiteAlias,
			apl.SiteCode,
			STUFF((SELECT ', ' + al.Name
				   FROM AllergenAPLMapping aam
				   left join Allergen al on al.AllergenCode = aam.AllergenCode
				   WHERE aam.ArticleNumber = a.ArticleNumber and aam.IsActive=1 and al.Top8 = 1
				  FOR XML PATH('')), 1, 2, '') as AllergensName,
			STUFF((SELECT ', ' + aam.AllergenCode
				   FROM AllergenAPLMapping aam
				    left join Allergen al on al.AllergenCode = aam.AllergenCode
				   WHERE aam.ArticleNumber = a.ArticleNumber and aam.IsActive=1 and al.Top8 = 1
				  FOR XML PATH('')), 1, 2, '') as AllergensCode,
			STUFF((SELECT ', ' + al.Name
				   FROM AllergenAPLMapping aam
				   left join Allergen al on al.AllergenCode = aam.AllergenCode
				   WHERE aam.ArticleNumber = a.ArticleNumber and aam.IsActive=1 and al.Top8 = 0
				  FOR XML PATH('')), 1, 2, '') as OtherAllergensName,
			STUFF((SELECT ', ' + aam.AllergenCode
				   FROM AllergenAPLMapping aam
				    left join Allergen al on al.AllergenCode = aam.AllergenCode
				   WHERE aam.ArticleNumber = a.ArticleNumber and aam.IsActive=1 and al.Top8 = 0
				  FOR XML PATH('')), 1, 2, '') as OtherAllergensCode
     from   [CookBookCommon].[dbo].UniqueAPLMaster a          
            left join  NationalMOG m  on a.MOGCode = m.MOGCode        
			inner join  (Select   SiteName,SiteCode,SiteAlias,ArticleNumber,Max(StandardCostPerKg) StandardCostPerKg 
								  ,max(ArticleCost) ArticleCost      
								  from   CookBookCommon.dbo.APLMaster am      
								  inner join  CookBookCommon.dbo.SiteMaster sm      
								  on am.Site = sm.SiteCode and sm.SectorCode = '30'      
								  where  StandardCostPerKg > 0 and ArticleType in ('ZFOD', 'ZPRP') and am.IsActive = 1      
								  group by ArticleNumber ,SiteName,SiteCode,SiteAlias      
						) as apl       
			on  a.ArticleNumber = apl.ArticleNumber       
			--left join AllergenAPLMapping aam on aam.ArticleNumber = a.ArticleNumber
		    where  (@ArticleNumber is null or (a.ArticleNumber = @ArticleNumber)) and          
				   (@MOGCode is null or (a.MOGCode = @MOGCode))  and  ArticleType in ('ZFOD', 'ZPRP')      
			order by apl.StandardCostPerKg desc      
end 


go

alter procedure [dbo].[procGetSiteList] @SiteCode nvarchar(20), @SectorNumber nvarchar(5)      
as      
Begin      
 Select  SiteID, SiteCode, ISNULL(SiteALias, SiteName) AS SiteName, SiteOrg, Name1, Name2, Street1, Street2, Street3, CityName as City, District,       
         RegionID, RegionDesc, PostalCode, CountryKey, Country, IsActive, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,       
         Division, DistributedChannel, SOrg, SiteAlias, SiteType, Optimus, OptimusDate, MapType, Retail, IsCPU, CPUCode,      
         (select SiteAlias from SiteMaster sm where sm.SiteCode = s.CPUCode) as CPUName,     
         IsDK, s.DKCode, (select SiteAlias from SiteMaster sm1 where sm1.SiteCode = s.DKCode) as DKName,SectorCode,(select Name from CookBookGoogle.dbo.SubSectorMaster csm where csm.SubSectorCode=s.SubSectorCode)as SubSectorCode    
   ,s.IsCustomBillingEndDate,s.BillingEndDate    
 from    SiteMaster s      
 where   SiteCode = (case when LEN(isnull(@SiteCode,'')) = 0 then SiteCode else @SiteCode end) and      
         convert(nvarchar(5),s.SectorCode) = @SectorNumber and SiteOrg = 'IN10'    
order by SiteAlias    
End    


go
alter view viewAPLLIST    
as    
select am.ArticleID,am.ArticleNumber,am.ArticleDescription as ArticleDescription, am.ArticleType,am.Hierlevel3,am.UOM,am.MerchandizeCategory,am.IsActive,s.SectorName,    
am.ArticleCost,am.StandardCostPerKg,sm.SiteName,sm.SiteCode    
    
from APLMaster am       
inner join SiteMaster sm on am.site=sm.SiteCode      
inner join SectorMaster s on sm.SectorCode=s.SectorNumber where  ArticleType in ('ZFOD', 'ZPRP') 


go

alter proc procGetAPLsPageWiseDataList        
      @PageIndex INT =null        
      --,@PageSize INT=100        
             
AS        
BEGIN        
      SET NOCOUNT ON;        
        
  --  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#Results]') AND type in (N'U'))        
  --DROP TABLE [dbo].[#Results]        
        
    with cte as      
 (      
      SELECT ROW_NUMBER() OVER        
      (        
            ORDER BY [ArticleID] ASC        
      )AS RowNumber        
     ,ArticleID        
     ,ArticleNumber,ArticleDescription,ArticleType,Hierlevel3,UOM,MerchandizeCategory,IsActive,SectorName,        
     ArticleCost,StandardCostPerKg,SiteName,Sitecode,(select count(*)       
        
from APLMaster am           
inner join SiteMaster sm on am.site=sm.SiteCode          
inner join SectorMaster s on sm.SectorCode=s.SectorNumber where  ArticleType in ('ZFOD', 'ZPRP') )  as NumberOfRows         
        
     -- INTO #Results        
      FROM viewAPLLIST        
       )      
      
      SELECT ArticleID        
  ,ArticleNumber,ArticleDescription,ArticleType,Hierlevel3,UOM,MerchandizeCategory,IsActive,SectorName,        
     ArticleCost,StandardCostPerKg,SiteName,NumberOfRows ,Sitecode FROM cte        
      WHERE RowNumber BETWEEN(@PageIndex -1) * 150 + 1 AND(((@PageIndex -1) * 150 + 1) + 150) - 1        
             
             
END        




go
alter procedure [dbo].[NationalprocGetSiteList] @SiteCode nvarchar(20)      
as        
Begin        
 Select  SiteID, SiteCode, ISNULL(SiteALias, SiteName) AS SiteName, SiteOrg, Name1, Name2, Street1, Street2, Street3, CityName as City, District,         
         RegionID, RegionDesc, PostalCode, CountryKey, Country, s.IsActive, s.CreatedBy, CreatedDate, s.ModifiedBy, ModifiedDate,         
         Division, DistributedChannel, SOrg, SiteAlias, st.Name, Optimus, OptimusDate, MapType, Retail, IsCPU, CPUCode,        
         (select SiteAlias from SiteMaster sm where sm.SiteCode = s.CPUCode) as CPUName,       
         IsDK, s.DKCode, (select SiteAlias from SiteMaster sm1 where sm1.SiteCode = s.DKCode) as DKName,s.SectorCode,SubSectorCode      
   ,s.IsCustomBillingEndDate,s.BillingEndDate,sm.SectorName    
 from    SiteMaster s        
 inner join SectorMaster sm on s.SectorCode=sm.SectorNumber
 inner join SiteType st on s.SiteType=st.SiteTypeCode
    
 where   SiteCode = (case when LEN(isnull(@SiteCode,'')) = 0 then SiteCode else @SiteCode end) and SiteOrg = 'IN10'      
order by SiteAlias      
End 





