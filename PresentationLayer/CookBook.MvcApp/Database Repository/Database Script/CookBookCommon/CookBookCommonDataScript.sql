Go
Insert into Allergen values('Egg', 'ALG-00001','', 1,1,1,getdate(),null,null,'')
Insert into Allergen values('Fish', 'ALG-00002','', 1,1,1,getdate(),null,null,'')
Insert into Allergen values('Milk', 'ALG-00003','', 1,1,1,getdate(),null,null,'')
Insert into Allergen values('Peanut', 'ALG-00004','', 1,1,1,getdate(),null,null,'')
Insert into Allergen values('Soy', 'ALG-00005','', 1,1,1,getdate(),null,null,'')
Insert into Allergen values('Tree Nuts', 'ALG-00006','', 1,1,1,getdate(),null,null,'')
Insert into Allergen values('Wheat', 'ALG-00007','', 1,1,1,getdate(),null,null,'')

Go


Insert into AllergenAPLMapping (AllergenCode,ArticleNumber,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn)
select 'ALG-00001',[Article Number],1,1,GETDATE(),NULL,NULL from AllergenAPLMappings where Egg ='Y'

Insert into AllergenAPLMapping (AllergenCode,ArticleNumber,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn)
select 'ALG-00002',[Article Number],1,1,GETDATE(),NULL,NULL from AllergenAPLMappings where Fish ='Y'

Insert into AllergenAPLMapping (AllergenCode,ArticleNumber,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn)
select 'ALG-00003',[Article Number],1,1,GETDATE(),NULL,NULL from AllergenAPLMappings where Milk ='Y'

Insert into AllergenAPLMapping (AllergenCode,ArticleNumber,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn)
select 'ALG-00004',[Article Number],1,1,GETDATE(),NULL,NULL from AllergenAPLMappings where Peanut ='Y'


Insert into AllergenAPLMapping (AllergenCode,ArticleNumber,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn)
select 'ALG-00005',[Article Number],1,1,GETDATE(),NULL,NULL from AllergenAPLMappings where Soy ='Y'


Insert into AllergenAPLMapping (AllergenCode,ArticleNumber,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn)
select 'ALG-00006',[Article Number],1,1,GETDATE(),NULL,NULL from AllergenAPLMappings where [Tree Nuts] ='Y'


Insert into AllergenAPLMapping (AllergenCode,ArticleNumber,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn)
select 'ALG-00007',[Article Number],1,1,GETDATE(),NULL,NULL from AllergenAPLMappings where Wheat ='Y'

Go