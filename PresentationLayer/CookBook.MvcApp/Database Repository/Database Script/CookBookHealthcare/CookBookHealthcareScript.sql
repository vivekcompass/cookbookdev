USE [CookBookGoogle]
GO
/****** Object:  StoredProcedure [dbo].[procGetAPLMasterDataForSite]    Script Date: 7/6/2021 10:34:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  procedure [dbo].[procGetAPLMasterDataForSite] @ArticleNumber nvarchar(20) = null, @MOGCode nvarchar(10) = null,@SiteCode  nvarchar(10) = null    
as     
begin    
     select a.ArticleID,
			a.ArticleNumber,
			a.ArticleDescription,a.UOM,
			a.MerchandizeCategory,
			a.MerchandizeCategoryDesc,
			a.IsActive,
			a.CreatedBy,
			a.CreatedDate,
			a.ModifiedBy,
			a.ModifiedDate,
			a.ArticleType,
			a.Hierlevel3, 
			a.MOGCode,
			m.Name as MOGName, 
			apl.StandardCostPerKg,
			apl.ArticleCost,
			STUFF((SELECT ', ' + al.Name
				   FROM cookbookcommon..AllergenAPLMapping aam
				   left join cookbookcommon..Allergen al on al.AllergenCode = aam.AllergenCode
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensName,
			STUFF((SELECT ', ' + aam.AllergenCode
				   FROM cookbookcommon..AllergenAPLMapping aam
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensCode
     from   [CookBookCommon].[dbo].UniqueAPLMaster a    
            left join    
            MOG m    
            on a.MOGCode = m.MOGCode   
	        inner Join CookBookCommon.dbo.APLMaster apl 
			on a.ArticleNumber = apl.ArticleNumber and  apl.Site = @SiteCode 
     where  (@ArticleNumber is null or (a.ArticleNumber = @ArticleNumber)) and    
            (@MOGCode is null or (a.MOGCode = @MOGCode))  and  
            a.ArticleType in ('ZFOD', 'ZPRP') and apl.IsActive = 1
   order by apl.StandardCostPerKg desc
end    
    
Go

USE [CookBookCore]
GO
/****** Object:  StoredProcedure [dbo].[procGetSiteMOGsByRecipeID]    Script Date: 7/6/2021 11:47:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[procGetSiteMOGsByRecipeID] @RecipeID int,@RecipeCode nvarchar(10),@siteCode nvarchar(10)
as
begin
select rm.ID,
       rm.Recipe_ID,
       r.Name as RecipeName,
	   rm.MOG_ID,
	   rm.MOGCode,
	 --  m.Name as MOGName,
	    CASE
      WHEN m.Alias is NULL or m.Alias='' or m.Alias=m.Name then m.Name
    ELSE m.Name+' ('+m.Alias+') 'end  as MOGName,
	   rm.Quantity,
	   rm.Yield,
	   rm.UOM_ID,
	   u.Name as UOMName,
	   rm.IsActive,
	   rm.CreatedBy,
	   rm.CreatedOn,
	   rm.ModifiedOn,
	   rm.ModifiedBy,
	   dbo.fnMathRoundOff(rm.CostPerUOM,2)  as CostPerUOM,
	   dbo.fnMathRoundOff(rm.CostPerKG,2)  as CostPerKG,
	   dbo.fnMathRoundOff(rm.TotalCost,2)  as TotalCost,
	   rm.UOMCode,
	   rm.IngredientPerc,
	   rm.RegionIngredientPerc,
	   rm.Site_ID,
	   rm.SiteCode,
	   rm.Region_ID,
	    rm.IsMajor,
	   rm.RegionTotalCost,
	   rm.RegionQuantity,
	   rm.ArticleNumber
from   SiteRecipeMOGMapping rm
       left join
	   SiteRecipe r
	   on rm.RecipeCode = r.RecipeCode and r.SiteCode = @siteCode
	   left join
	   UOM u
	   on rm.UOMCode = u.UOMCode
	   inner join
	   MOG m
	   on rm.MOGCode = m.MOGCode
where  rm.Recipe_ID = @RecipeID or rm.RecipeCode = @RecipeCode
       and rm.SiteCode = @siteCode order by rm.ID
end
