alter table SiteRecipeMOGMapping
add ArticleNumber varchar(50)

Go

ALTER table MOG 
add AllergensName nvarchar(500)

Go

ALTER table RecipeMOGMapping 
add AllergensName nvarchar(500)

GO

ALTER table Recipe 
add AllergensName nvarchar(500)

Go

ALTER table Dish 
add AllergensName nvarchar(500)

Go

ALTER table Item 
add AllergensName nvarchar(500)


Go


ALTER table RegionRecipeMOGMapping 
add AllergensName nvarchar(500)

GO

ALTER table RegionRecipe 
add AllergensName nvarchar(500)

Go

ALTER table RegionDish 
add AllergensName nvarchar(500)

Go

ALTER table RegionItemInheritanceMapping 
add AllergensName nvarchar(500)

Go



ALTER table SiteRecipeMOGMapping 
add AllergensName nvarchar(500)

GO

ALTER table SiteRecipe 
add AllergensName nvarchar(500)

Go

ALTER table SiteDish 
add AllergensName nvarchar(500)

Go

ALTER table SiteItemInheritanceMapping 
add AllergensName nvarchar(500)