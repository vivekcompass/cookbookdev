GO
/****** Object:  StoredProcedure [dbo].[procGetAPLMasterDataForSite]    Script Date: 7/6/2021 10:34:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  procedure [dbo].[procGetAPLMasterDataForSite] @ArticleNumber nvarchar(20) = null, @MOGCode nvarchar(10) = null,@SiteCode  nvarchar(10) = null    
as     
begin    
     select a.ArticleID,
			a.ArticleNumber,
			a.ArticleDescription,a.UOM,
			a.MerchandizeCategory,
			a.MerchandizeCategoryDesc,
			a.IsActive,
			a.CreatedBy,
			a.CreatedDate,
			a.ModifiedBy,
			a.ModifiedDate,
			a.ArticleType,
			a.Hierlevel3, 
			a.MOGCode,
			m.Name as MOGName, 
			apl.StandardCostPerKg,
			apl.ArticleCost,
			STUFF((SELECT ', ' + al.Name
				   FROM cookbookcommon..AllergenAPLMapping aam
				   left join cookbookcommon..Allergen al on al.AllergenCode = aam.AllergenCode
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensName,
			STUFF((SELECT ', ' + aam.AllergenCode
				   FROM cookbookcommon..AllergenAPLMapping aam
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensCode
     from   [CookBookCommon].[dbo].UniqueAPLMaster a    
            left join    
            MOG m    
            on a.MOGCode = m.MOGCode   
	        inner Join CookBookCommon.dbo.APLMaster apl 
			on a.ArticleNumber = apl.ArticleNumber and  apl.Site = @SiteCode 
     where  (@ArticleNumber is null or (a.ArticleNumber = @ArticleNumber)) and    
            (@MOGCode is null or (a.MOGCode = @MOGCode))  and  
            a.ArticleType in ('ZFOD', 'ZPRP') and apl.IsActive = 1
   order by apl.StandardCostPerKg desc
end    
    
Go

USE [CookBookCore]
GO
/****** Object:  StoredProcedure [dbo].[procGetSiteMOGsByRecipeID]    Script Date: 7/6/2021 11:47:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[procGetSiteMOGsByRecipeID] @RecipeID int,@RecipeCode nvarchar(10),@siteCode nvarchar(10)
as
begin
select rm.ID,
       rm.Recipe_ID,
       r.Name as RecipeName,
	   rm.MOG_ID,
	   rm.MOGCode,
	 --  m.Name as MOGName,
	    CASE
      WHEN m.Alias is NULL or m.Alias='' or m.Alias=m.Name then m.Name
    ELSE m.Name+' ('+m.Alias+') 'end  as MOGName,
	   rm.Quantity,
	   rm.Yield,
	   rm.UOM_ID,
	   u.Name as UOMName,
	   rm.IsActive,
	   rm.CreatedBy,
	   rm.CreatedOn,
	   rm.ModifiedOn,
	   rm.ModifiedBy,
	   dbo.fnMathRoundOff(rm.CostPerUOM,2)  as CostPerUOM,
	   dbo.fnMathRoundOff(rm.CostPerKG,2)  as CostPerKG,
	   dbo.fnMathRoundOff(rm.TotalCost,2)  as TotalCost,
	   rm.UOMCode,
	   rm.IngredientPerc,
	   rm.RegionIngredientPerc,
	   rm.Site_ID,
	   rm.SiteCode,
	   rm.Region_ID,
	    rm.IsMajor,
	   rm.RegionTotalCost,
	   rm.RegionQuantity,
	   rm.ArticleNumber
from   SiteRecipeMOGMapping rm
       left join
	   SiteRecipe r
	   on rm.RecipeCode = r.RecipeCode and r.SiteCode = @siteCode
	   left join
	   UOM u
	   on rm.UOMCode = u.UOMCode
	   inner join
	   MOG m
	   on rm.MOGCode = m.MOGCode
where  rm.Recipe_ID = @RecipeID or rm.RecipeCode = @RecipeCode
       and rm.SiteCode = @siteCode order by rm.ID
end



GO
/****** Object:  StoredProcedure [dbo].[SaveRegionToSiteInheritance]    Script Date: 7/8/2021 5:39:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[SaveRegionToSiteInheritance] 
@SiteCode nvarchar(10),
@ItemCode nvarchar(max),
@UserID int,
@IsActive bit,
@SubSectorCode nvarchar(10)
as
begin

BEGIN TRY
   IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#TempSiteItem]') AND type in (N'U'))
      DROP TABLE [dbo].[#TempSiteItem]

    CREATE TABLE #TempSiteItem (
	   ItemCode nvarchar(20) ,IsActive bit
	)

	declare @Site_ID as int
	declare @Region_ID as int

	select @Site_ID = SiteID, @Region_ID = RegionID, @SubSectorCode = SubSectorCode from CookBookCommon.dbo.SiteMaster where SiteCode = @SiteCode

    insert into #TempSiteItem
	SELECT SUBSTRING(tuple,3,LEN(tuple)) as ItemCode,Cast(SUBSTRING(tuple,1,1) as bit) as IsActive
	FROM   split_string(@ItemCode, ',')

	declare @cntitem as int = 0
	declare @cntdc as int = 0
	declare @cntdsh as int = 0

	set @cntitem = isnull((select isnull(COUNT(1),0) from SiteItemInheritanceMapping where ItemCode in (select ItemCode from #TempSiteItem) and SiteCode = @SiteCode),0)
	set @cntdc = isnull((select isnull(COUNT(1),0) from SiteDishCategoryMapping where ItemCode in (select ItemCode from #TempSiteItem) and SiteCode = @SiteCode),0)
	set @cntdsh = isnull((select isnull(COUNT(1),0) from SiteItemDishMapping where ItemCode in (select ItemCode from #TempSiteItem)  and SiteCode = @SiteCode),0)
	
	if (@cntitem > 0) begin
	   delete from SiteItemInheritanceMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
	end

		IF EXISTS( select 1 from SiteItemInheritanceMapping where Site_ID=@Site_ID  and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RIM
			set RIM.Site_ID=@Site_ID,RIM.Item_ID=i.ID ,RIM.IsActive = 1, RIM.ModifiedBy = @UserID, RIM.ModifiedOn = getdate(), RIM.Status=1                 
			from SiteItemInheritanceMapping RIM 
			join 
			(select ItemCode, Site_ID from SiteItemInheritanceMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RIM2
			on  RIM2.ItemCode = RIM.ItemCode and RIM2.Site_ID = RIM.Site_ID
		    join Item i 
			on  RIM2.ItemCode = I.ItemCode
		end
		else
		begin
		    insert into SiteItemInheritanceMapping(
			Site_ID,SiteCode,Item_ID,ItemCode,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,Status) 
			select @Site_ID,@SiteCode ,ID, ItemCode, @IsActive, @UserID, GETDATE(), null, null,1 -- 1 for pending
			from   Item i where ItemCode 
			IN (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteItemInheritanceMapping where Site_ID = @Site_ID  )) and i.IsActive =1
		end

		select @IsActive

	if(@IsActive=1)
	begin

		if (@cntdc > 0) begin
		   delete from SiteDishCategoryMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
		end
		
		IF EXISTS( select 1 from SiteDishCategoryMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RDCM
			set RDCM.IsActive = IDCM.IsActive, RDCM.StandardPortion = IDCM.StandardPortion, RDCM.UOMCode = IDCM.UOMCode, RDCM.ModifiedBy =@UserID, RDCM.ModifiedOn = getdate()                 
			from SiteDishCategoryMapping RDCM
			join 
			(select DishCategoryCode, ItemCode, Site_ID from SiteDishCategoryMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RDCM2
			on RDCM2.DishCategoryCode = RDCM.DishCategoryCode and RDCM2.ItemCode = RDCM.ItemCode and RDCM2.Site_ID = RDCM.Site_ID
		    join RegionDishCategoryMapping IDCM 
			on RDCM2.DishCategoryCode = IDCM.DishCategoryCode and RDCM2.ItemCode = IDCM.ItemCode and Region_ID = @Region_ID
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end
		else
		begin
		    insert into SiteDishCategoryMapping 
			select @Site_ID, @SiteCode, Item_ID, DishCategory_ID, @IsActive, @UserID, getdate(), null,null, ItemCode, DishCategoryCode, StandardPortion,UOMCode,WeightPerUOM
			from   RegionDishCategoryMapping IDCM where ItemCode 
			in (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteDishCategoryMapping where Site_ID =@Site_ID ))
			and Region_ID= @Region_ID and IDCM.IsActive=1 
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end

		insert into SiteDishCategory
		select rdcm.SiteCode, rdcm.DishCategoryCode, 0,0,0,null,1,@UserID,getdate(),null,null
		from
		       (
			    select distinct SiteCode, DishCategoryCode
			    from   SiteDishCategoryMapping
			   ) rdcm
		       left join 
			   SiteDishCategory rdc
			   on rdcm.SiteCode = rdc.SiteCode and rdcm.DishCategoryCode = rdc.DishCategoryCode
        where  rdc.SiteCode is null and rdc.DishCategoryCode is null
	
		if (@cntdsh > 0) begin
		   delete from SiteItemDishMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
		end
		
		IF EXISTS( select 1 from SiteItemDishMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RIM
			set RIM.Site_ID=@Site_ID,RIM.Item_ID=i.Item_ID ,RIM.Dish_ID=i.Dish_ID, RIM.IsActive = i.IsActive, RIM.ModifiedBy = @UserID, RIM.ModifiedOn = getdate(), RIM.SiteCode=@SiteCode,
			RIM.DishCode =i.DishCode, RIM.StandardPortion=I.StandardPortion ,RIM.ContractPortion = i.ContractPortion,
			RIM.ContractUOMCode = i.UOMCode, RIM.ContractWeightPerUOM= i.WeightPerUOM, RIM.ServedUOMCode =i.UOMCode, RIM.ServedWeightPerUOM =i.WeightPerUOM
			,RIM.StandardUOMCode = i.UOMCode, RIM.StandardWeightPerUOM = i.WeightPerUOM
			from SiteItemDishMapping RIM 
			join 
			(select ItemCode, Site_ID,DishCode from SiteItemDishMapping where Site_ID=@Site_ID 
			and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RIM2
			on  RIM2.ItemCode = RIM.ItemCode and RIM2.Site_ID = RIM.Site_ID and RIM2.DishCode = RIM.DishCode
		    join RegionItemDishMapping i 
			on  RIM2.ItemCode = i.ItemCode and I.DishCode =RIM2.DishCode and i.Region_ID = @Region_ID and i.IsActive=1
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end
		else
		begin
		 
		    insert into SiteItemDishMapping 
			select @Site_ID, Item_ID, Dish_ID, IsActive, @UserID,getdate(), null,null, @SiteCode, ItemCode, DishCode,ContractPortion, StandardPortion, ContractPortion,null, WeightPerUOM, UOMCode,WeightPerUOM, UOMCode,WeightPerUOM, UOMCode,null,null
			from   RegionItemDishMapping i where ItemCode 
			IN (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteItemDishMapping where Site_ID = @Site_ID ))
			and Region_ID = @Region_ID and i.IsActive =1
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end

		insert into SiteDish
		select rdcm.SiteCode, rdcm.DishCode, null,0,null,1,@UserID,getdate(),null,null
		from
		       (
			    select distinct SiteCode, DishCode
			    from   SiteItemDishMapping
			   ) rdcm
		       left join 
			   SiteDish rdc
			   on rdcm.SiteCode = rdc.SiteCode and rdcm.DishCode = rdc.DishCode
        where  rdc.SiteCode is null and rdc.DishCode is null
		
		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#TempItemDish]') AND type in (N'U'))
		  DROP TABLE [dbo].[#TempItemDish]

		CREATE TABLE #TempItemDish (
		   DishCode nvarchar(20) 
		)

		insert into #TempItemDish
		select  DishCode
		from   SiteItemDishMapping where ItemCode in (select ItemCode from #TempSiteItem ti where ti.IsActive=1)

		While ((select count(1) from #TempItemDish) > 0)
		begin
			 DECLARE @DishCode varchar(20)
			 SELECT TOP 1 @DishCode = DishCode FROM   #TempItemDish
				exec procSaveSiteItemDishRecipeMapping @Site_ID, @SiteCode, @DishCode, @UserID
			 Delete from #TempItemDish where  DishCode = @DishCode 
		end
    end
	DROP TABLE [dbo].[#TempSiteItem]

	select 'True' as Result
END TRY  
BEGIN CATCH  
    SELECT ERROR_MESSAGE() as Result
END CATCH  
end



GO
/****** Object:  StoredProcedure [dbo].[procSaveSiteItemDishRecipeMapping]    Script Date: 7/8/2021 6:32:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procSaveSiteItemDishRecipeMapping] (
  @Site_ID int
 ,@SiteCode  nvarchar(10)
 ,@DishCode  nvarchar(10)
 ,@CreatedBy int
)
as
begin
    declare @Region_ID as int = 0
    set @Region_ID = (select RegionID from CookBookCommon.dbo.SiteMaster where SiteID= @Site_ID)

	If not exists (select * from SiteDishRecipeMapping where  Site_ID = @Site_ID and DishCode = @DishCode)
	begin
	    insert into SiteDishRecipeMapping
		select @Site_ID, @SiteCode, Dish_ID, Recipe_ID, IsActive, @CreatedBy, GETDATE(),null,null, DishCode, RecipeCode, IsDefault from DishRecipeMapping where DishCode = @DishCode
	end

	IF OBJECT_ID('TempDB..#TEMPSiteRecipeList') IS NOT NULL DROP TABLE #TEMPSiteRecipeList
		SELECT  a.Site_ID,a.SiteCode,a.RecipeCode
		into    #TEMPSiteRecipeList
		FROM    (
				 select distinct rdrm.Site_ID,rdrm.SiteCode, rdrm.RecipeCode
				 from   SiteDishRecipeMapping rdrm left join SiteRecipe rr 
				 	    on rdrm.Site_ID = rr.Site_ID and rdrm.RecipeCode = rr.RecipeCode
				 where  rdrm.Site_ID = @Site_ID and rdrm.DishCode = @DishCode and rr.Site_ID is null and rr.RecipeCode is null
				) as a


	While ((select count(1) from #TEMPSiteRecipeList) > 0)
	begin
	     DECLARE @SiteID int
	     DECLARE @Site_Code varchar(10)
		 DECLARE @RecipeCode varchar(20)
		 SELECT TOP 1 @SiteID = Site_ID,@Site_Code= SiteCode ,@RecipeCode = RecipeCode 
		 FROM   #TEMPSiteRecipeList

		 If not exists (select * from   SiteRecipe where  Site_ID = @SiteID and RecipeCode = @RecipeCode)
	     begin
		     declare @RecipeCost decimal(18,3)
		     exec procGetRecipeCostAtSite @RecipeCode, @Site_ID, @RecipeCost output
			 insert into SiteRecipe
	   	     select @SiteID,@Site_Code,@Region_ID ,Name, UOM_ID, Quantity, Instructions, IsBase, IsActive, @CreatedBy, GETDATE(), null,null, @RecipeCode, UOMCode,@RecipeCost/10, @RecipeCost/10,@RecipeCost,@RecipeCost/10, @RecipeCost/10,@RecipeCost,Status
			 from   Recipe 
			 where  RecipeCode = @RecipeCode
			   exec procSaveUnitBaseRecipeData @SiteID,@Site_Code,@RecipeCode,@CreatedBy
			 if not exists (select * from SiteRecipeMOGMapping where Site_ID = @Site_ID and RecipeCode = @RecipeCode)
			  
			begin
			     insert into SiteRecipeMOGMapping
				 select  @Site_ID,@SiteCode,@Region_ID ,Recipe_ID, MOG_ID, UOM_ID, BaseRecipe_ID, Quantity, 1, @CreatedBy, getdate(), null,null, Yield,
					     @RecipeCode, MOGCode, BaseRecipeCode, UOMCode, CostPerUOM, CostPerKG, TotalCost, CostAsOfDate,IngredientPerc
						 ,IsMajor,  IngredientPerc,TotalCost, Quantity,NULL
				 from    RecipeMOGMapping
				 where   RecipeCode = @RecipeCode
			 end
	     end
		 else 
		 begin
			if not exists (select * from SiteRecipeMOGMapping where Site_ID = @Site_ID and RecipeCode = @RecipeCode)
			 begin
			 exec procSaveUnitBaseRecipeData @SiteID,@Site_Code,@RecipeCode,@CreatedBy
			     insert into SiteRecipeMOGMapping
				 select  @Site_ID,@SiteCode,@Region_ID ,Recipe_ID, MOG_ID, UOM_ID, BaseRecipe_ID, Quantity, 1, @CreatedBy, getdate(), null,null, Yield,
					     @RecipeCode, MOGCode, BaseRecipeCode, UOMCode, CostPerUOM, CostPerKG, TotalCost, CostAsOfDate,IngredientPerc,IsMajor, IngredientPerc ,
						 TotalCost, Quantity,null
				 from    RecipeMOGMapping
				 where   RecipeCode = @RecipeCode
			 end
		 end
	  Delete from #TEMPSiteRecipeList where @SiteID = Site_ID and  @RecipeCode = RecipeCode 
	end

end


Go

USE [CookBookGoogle]
GO
/****** Object:  StoredProcedure [dbo].[SaveRegionToSiteInheritance]    Script Date: 7/8/2021 6:42:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[SaveRegionToSiteInheritance] 
@SiteCode nvarchar(10),
@ItemCode nvarchar(max),
@UserID int,
@IsActive bit,
@SubSectorCode nvarchar(10) = NULL
as
begin

BEGIN TRY
   IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#TempSiteItem]') AND type in (N'U'))
      DROP TABLE [dbo].[#TempSiteItem]

    CREATE TABLE #TempSiteItem (
	   ItemCode nvarchar(20) ,IsActive bit
	)

	declare @Site_ID as int
	declare @Region_ID as int

	select @Site_ID = SiteID, @Region_ID = RegionID, @SubSectorCode = SubSectorCode from CookBookCommon.dbo.SiteMaster where SiteCode = @SiteCode

    insert into #TempSiteItem
	SELECT SUBSTRING(tuple,3,LEN(tuple)) as ItemCode,Cast(SUBSTRING(tuple,1,1) as bit) as IsActive
	FROM   split_string(@ItemCode, ',')

	declare @cntitem as int = 0
	declare @cntdc as int = 0
	declare @cntdsh as int = 0

	set @cntitem = isnull((select isnull(COUNT(1),0) from SiteItemInheritanceMapping where ItemCode in (select ItemCode from #TempSiteItem) and SiteCode = @SiteCode),0)
	set @cntdc = isnull((select isnull(COUNT(1),0) from SiteDishCategoryMapping where ItemCode in (select ItemCode from #TempSiteItem) and SiteCode = @SiteCode),0)
	set @cntdsh = isnull((select isnull(COUNT(1),0) from SiteItemDishMapping where ItemCode in (select ItemCode from #TempSiteItem)  and SiteCode = @SiteCode),0)
	
	if (@cntitem > 0) begin
	   delete from SiteItemInheritanceMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
	end

		IF EXISTS( select 1 from SiteItemInheritanceMapping where Site_ID=@Site_ID  and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RIM
			set RIM.Site_ID=@Site_ID,RIM.Item_ID=i.ID ,RIM.IsActive = 1, RIM.ModifiedBy = @UserID, RIM.ModifiedOn = getdate(), RIM.Status=1                 
			from SiteItemInheritanceMapping RIM 
			join 
			(select ItemCode, Site_ID from SiteItemInheritanceMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RIM2
			on  RIM2.ItemCode = RIM.ItemCode and RIM2.Site_ID = RIM.Site_ID
		    join Item i 
			on  RIM2.ItemCode = I.ItemCode
		end
		else
		begin
		    insert into SiteItemInheritanceMapping(
			Site_ID,SiteCode,Item_ID,ItemCode,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,Status) 
			select @Site_ID,@SiteCode ,ID, ItemCode, @IsActive, @UserID, GETDATE(), null, null,1 -- 1 for pending
			from   Item i where ItemCode 
			IN (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteItemInheritanceMapping where Site_ID = @Site_ID  )) and i.IsActive =1
		end

		select @IsActive

	if(@IsActive=1)
	begin

		if (@cntdc > 0) begin
		   delete from SiteDishCategoryMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
		end
		
		IF EXISTS( select 1 from SiteDishCategoryMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RDCM
			set RDCM.IsActive = IDCM.IsActive, RDCM.StandardPortion = IDCM.StandardPortion, RDCM.UOMCode = IDCM.UOMCode, RDCM.ModifiedBy =@UserID, RDCM.ModifiedOn = getdate()                 
			from SiteDishCategoryMapping RDCM
			join 
			(select DishCategoryCode, ItemCode, Site_ID from SiteDishCategoryMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RDCM2
			on RDCM2.DishCategoryCode = RDCM.DishCategoryCode and RDCM2.ItemCode = RDCM.ItemCode and RDCM2.Site_ID = RDCM.Site_ID
		    join RegionDishCategoryMapping IDCM 
			on RDCM2.DishCategoryCode = IDCM.DishCategoryCode and RDCM2.ItemCode = IDCM.ItemCode and Region_ID = @Region_ID
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end
		else
		begin
		    insert into SiteDishCategoryMapping 
			select @Site_ID, @SiteCode, Item_ID, DishCategory_ID, @IsActive, @UserID, getdate(), null,null, ItemCode, DishCategoryCode, StandardPortion,UOMCode,WeightPerUOM
			from   RegionDishCategoryMapping IDCM where ItemCode 
			in (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteDishCategoryMapping where Site_ID =@Site_ID ))
			and Region_ID= @Region_ID and IDCM.IsActive=1 
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end

		insert into SiteDishCategory
		select rdcm.SiteCode, rdcm.DishCategoryCode, 0,0,0,null,1,@UserID,getdate(),null,null
		from
		       (
			    select distinct SiteCode, DishCategoryCode
			    from   SiteDishCategoryMapping
			   ) rdcm
		       left join 
			   SiteDishCategory rdc
			   on rdcm.SiteCode = rdc.SiteCode and rdcm.DishCategoryCode = rdc.DishCategoryCode
        where  rdc.SiteCode is null and rdc.DishCategoryCode is null
	
		if (@cntdsh > 0) begin
		   delete from SiteItemDishMapping where SiteCode = @SiteCode and ItemCode in (select ItemCode from #TempSiteItem t1 where  t1.IsActive=0)
		end
		
		IF EXISTS( select 1 from SiteItemDishMapping where Site_ID=@Site_ID and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1))
		begin
			update RIM
			set RIM.Site_ID=@Site_ID,RIM.Item_ID=i.Item_ID ,RIM.Dish_ID=i.Dish_ID, RIM.IsActive = i.IsActive, RIM.ModifiedBy = @UserID, RIM.ModifiedOn = getdate(), RIM.SiteCode=@SiteCode,
			RIM.DishCode =i.DishCode, RIM.StandardPortion=I.StandardPortion ,RIM.ContractPortion = i.ContractPortion,
			RIM.ContractUOMCode = i.UOMCode, RIM.ContractWeightPerUOM= i.WeightPerUOM, RIM.ServedUOMCode =i.UOMCode, RIM.ServedWeightPerUOM =i.WeightPerUOM
			,RIM.StandardUOMCode = i.UOMCode, RIM.StandardWeightPerUOM = i.WeightPerUOM
			from SiteItemDishMapping RIM 
			join 
			(select ItemCode, Site_ID,DishCode from SiteItemDishMapping where Site_ID=@Site_ID 
			and ItemCode IN(select ItemCode from #TempSiteItem ti where ti.IsActive=1)) RIM2
			on  RIM2.ItemCode = RIM.ItemCode and RIM2.Site_ID = RIM.Site_ID and RIM2.DishCode = RIM.DishCode
		    join RegionItemDishMapping i 
			on  RIM2.ItemCode = i.ItemCode and I.DishCode =RIM2.DishCode and i.Region_ID = @Region_ID and i.IsActive=1
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end
		else
		begin
		 
		    insert into SiteItemDishMapping 
			select @Site_ID, Item_ID, Dish_ID, IsActive, @UserID,getdate(), null,null, @SiteCode, ItemCode, DishCode,ContractPortion, StandardPortion, ContractPortion,null, WeightPerUOM, UOMCode,WeightPerUOM, UOMCode,WeightPerUOM, UOMCode,null,null
			from   RegionItemDishMapping i where ItemCode 
			IN (select ItemCode from #TempSiteItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from SiteItemDishMapping where Site_ID = @Site_ID ))
			and Region_ID = @Region_ID and i.IsActive =1
			and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end)
		end

		insert into SiteDish
		select rdcm.SiteCode, rdcm.DishCode, null,0,null,1,@UserID,getdate(),null,null
		from
		       (
			    select distinct SiteCode, DishCode
			    from   SiteItemDishMapping
			   ) rdcm
		       left join 
			   SiteDish rdc
			   on rdcm.SiteCode = rdc.SiteCode and rdcm.DishCode = rdc.DishCode
        where  rdc.SiteCode is null and rdc.DishCode is null
		
		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#TempItemDish]') AND type in (N'U'))
		  DROP TABLE [dbo].[#TempItemDish]

		CREATE TABLE #TempItemDish (
		   DishCode nvarchar(20) 
		)

		insert into #TempItemDish
		select  DishCode
		from   SiteItemDishMapping where ItemCode in (select ItemCode from #TempSiteItem ti where ti.IsActive=1)

		While ((select count(1) from #TempItemDish) > 0)
		begin
			 DECLARE @DishCode varchar(20)
			 SELECT TOP 1 @DishCode = DishCode FROM   #TempItemDish
				exec procSaveSiteItemDishRecipeMapping @Site_ID, @SiteCode, @DishCode, @UserID
			 Delete from #TempItemDish where  DishCode = @DishCode 
		end
    end
	DROP TABLE [dbo].[#TempSiteItem]

	select 'True' as Result
END TRY  
BEGIN CATCH  
    SELECT ERROR_MESSAGE() as Result
END CATCH  
end

GO
ALTER function [dbo].[DistinctList](
    @List varchar(max),
    @Delim char)
returns varchar(max)
as
    begin
        declare @ParsedList table(
            Item varchar(max)
        );
 
        declare @list1 varchar(max);
        declare @Pos int;
        declare @rList varchar(max);
 
        set @list = LTRIM(RTRIM(@list)) + @Delim;
        set @pos = CHARINDEX(@delim, @list, 1);
 
        while @pos > 0
        begin
            set @list1 = LTRIM(RTRIM(LEFT(@list, @pos - 1)));
            if @list1 <> ''
                begin
                    insert into @ParsedList values (CAST(@list1 as varchar(max)))
                end;
            set @list = SUBSTRING(@list, @pos + 1, LEN(@list));
            set @pos = CHARINDEX(@delim, @list, 1);
        end;
 
        select @rlist = COALESCE(@rlist + ',', '') + item
        from (select distinct Item from @ParsedList) as t;
 
        return @rlist;
    end;


GO
/****** Object:  StoredProcedure [dbo].[procUpdateCostForAllDishesAtSector]    Script Date: 7/9/2021 10:47:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[procUpdateAllergensAtSector] @SectorCode as varchar(20)
as begin

-- Update MOG Table Allergsns

update mog
	set		mog.AllergensName = uniqueApl.AllergensName
			from MOG mog
					left join(
							SELECT uapl.ArticleNumber,uapl.MOGCode
							,STUFF((SELECT ', ' + al.Name
							 FROM cookbookcommon.dbo.AllergenAPLMapping aam
							 left join cookbookcommon.dbo.Allergen al on al.AllergenCode = aam.AllergenCode
							 WHERE aam.ArticleNumber = uapl.ArticleNumber 
							 FOR XML PATH('')), 1, 2, '') as AllergensName
										from  [cookbookcommon].[dbo].UniqueAPLMaster  uapl
										inner join 
											  (  Select ArticleNumber 
													   from cookbookcommon.dbo.APLMaster am
														  inner join
														  cookbookcommon.dbo.SiteMaster sm
														 on am.Site = case when sm.SiteType = 'STP-001' then sm.CPUCode else sm.SiteCode end and sm.SectorCode = @SectorCode
												 where    ArticleType in ('ZFOD', 'ZPRP') and am.IsActive = 1
												 group by ArticleNumber 
											  ) as apl 
										  on  uapl.ArticleNumber = apl.ArticleNumber
										 ) as uniqueApl
										  on uniqueApl.MOGCode = MOG.MOGCode 
				WHERE  uniqueApl.AllergensName is not null    
end

Go


/****** Object:  StoredProcedure [dbo].[procUpdateAllergensAtSector]    Script Date: 7/12/2021 10:33:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Alter procedure [dbo].[procUpdateAllergensAtRegion] @SectorCode as varchar(20)
as begin

   
-- Update RecipeMOGMapping Table Allergsns

update mog
	set		mog.AllergensName = uniqueApl.AllergensName
			from RegionRecipeMOGMapping mog
					left join(
							SELECT uapl.ArticleNumber,uapl.MOGCode, 
							 STUFF((SELECT ', ' + al.Name
							 FROM cookbookcommon.dbo.AllergenAPLMapping aam
							 left join cookbookcommon.dbo.Allergen al on al.AllergenCode = aam.AllergenCode
							 WHERE aam.ArticleNumber = uapl.ArticleNumber 
							 FOR XML PATH('')), 1, 2, '') as AllergensName
							, apl.RegionID
										from  [cookbookcommon].[dbo].UniqueAPLMaster  uapl
										inner join 
											  (  Select ArticleNumber, sm.RegionID
													   from cookbookcommon.dbo.APLMaster am
														  inner join
														  cookbookcommon.dbo.SiteMaster sm
														 on am.Site = case when sm.SiteType = 'STP-001' then sm.CPUCode else sm.SiteCode end and sm.SectorCode = 30
												 where    ArticleType in ('ZFOD', 'ZPRP') and am.IsActive = 1
												 group by ArticleNumber, sm.RegionID
											  ) as apl 
										  on  uapl.ArticleNumber = apl.ArticleNumber
										 ) as uniqueApl
										  on uniqueApl.MOGCode = MOG.MOGCode 
				WHERE mog.Region_ID = uniqueApl.RegionID and  uniqueApl.AllergensName is not null 
				

-- update Recipe Allergens --


  
  IF OBJECT_ID('TempDB..#TEMPRecipe') IS NOT NULL DROP TABLE #TEMPRecipe

 ;WITH rCTE AS(
    SELECT  Region_ID ,BaseRecipeCode,MOGCode, cast(Quantity as decimal(18,3)) as MOGQty,RecipeCode ,RecipeCode as FinalRecipeCode
	FROM    RegionRecipeMOGMapping where IsActive = 1 
	
	UNION ALL
    
	SELECT t.Region_ID, t.BaseRecipeCode,t.MOGCode, cast((cast(r.MOGQty as decimal(18,3)) * cast(t.Quantity as decimal(18,3))/10) as decimal(18,3)) as MOGQty
	,t.RecipeCode ,r.FinalRecipeCode
    FROM    RegionRecipeMOGMapping t
            INNER JOIN rCTE r
            ON t.RecipeCode = r.BaseRecipeCode and t.Region_ID= r.Region_ID
	where t.IsActive = 1
   ), 
   rCTE2 AS(
      select MOGCode,FinalRecipeCode,RecipeCode,BaseRecipeCode,sum(rCTE.MOGQty) as MOGQty, Region_ID
	 
	  from   rCTE
	  --where  MOGCode is not null
	 group by  MOGCode,FinalRecipeCode,RecipeCode,BaseRecipeCode,Region_ID
   )

    SELECT  FinalRecipeCode,RecipeCode,BaseRecipeCode,MOGCode,MOGQty, Region_ID
	into    #TEMPRecipe
	FROM    rCTE2

	update r
		 set  r.AllergensName   =  RecipeAllergen.NewField
    from RegionRecipe r
	Join (SELECT 
	 Region_ID
	,FinalRecipeCode
	,STRING_AGG(x.AllergensName, ',') WITHIN GROUP (ORDER BY x.AllergensName) AS 
	NewField
	from (
		SELECT  distinct uniqueApl.AllergensName, FinalRecipeCode, Region_ID
   		 From   #TEMPRecipe trs  
		   left join(
		  SELECT uapl.ArticleNumber,uapl.MOGCode, alg.Name as AllergensName, apl.RegionID
										from  [cookbookcommon].[dbo].UniqueAPLMaster  uapl
										inner join 
											  (  Select ArticleNumber, sm.RegionID
													   from cookbookcommon.dbo.APLMaster am
														  inner join
														  cookbookcommon.dbo.SiteMaster sm
														 on am.Site = case when sm.SiteType = 'STP-001' then sm.CPUCode else sm.SiteCode end and sm.SectorCode = 30
												 where    ArticleType in ('ZFOD', 'ZPRP') and am.IsActive = 1
												 group by ArticleNumber, sm.RegionID
											  ) as apl 
										  on  uapl.ArticleNumber = apl.ArticleNumber
										  left join cookbookcommon.dbo.AllergenAPLMapping aam on aam.ArticleNumber = uapl.ArticleNumber
										  left join cookbookcommon.dbo.Allergen alg on alg.AllergenCode = aam.AllergenCode
										 ) as uniqueApl
						      on uniqueApl.MOGCode = trs.MOGCode and uniqueApl.RegionID = trs.Region_ID
							  where uniqueApl.AllergensName is not null
	) x
	GROUP BY Region_ID,FinalRecipeCode
	)RecipeAllergen
  on r.RecipeCode = RecipeAllergen.FinalRecipeCode and r.Region_ID = RecipeAllergen.Region_ID

  ---- Update Dishes Allergens
  
update d
		 set   AllergensName = r.AllergensName
		 From RegionDish d 
		 inner join RegionDishRecipeMapping drm on drm.DishCode = d.DishCode and drm.IsDefault = 1 and drm.Region_ID = d.Region_ID
		 inner join RegionRecipe r  on r.RecipeCode = drm.RecipeCode  
		 where  r.Region_ID = drm.Region_ID

 ---- Update Items Allergens


update it 
set AllergensName = al.Allergens
from RegionItemInheritanceMapping it
JOIN (select i.ItemCode, i.Region_ID,
   dbo.DistinctList(STRING_AGG(cast(d.AllergensName as NVARCHAR(MAX)), ',') WITHIN GROUP (ORDER BY d.AllergensName),',') AS Allergens
from RegionItemInheritanceMapping i
left join RegionItemDishMapping idm on idm.ItemCode = i.ItemCode and idm.Region_ID = i.Region_ID
left join  RegionDish  d on d.DishCode = idm.DishCode  
where d.DishCode is not null and d.AllergensName is not null and d.Region_ID = idm.Region_ID 
group by i.ItemCode, i.Region_ID
) al
on al.ItemCode = it.ItemCode
where it.Region_ID = al.Region_ID

end


Go


/****** Object:  StoredProcedure [dbo].[procUpdateAllergensAtSector]    Script Date: 7/12/2021 10:33:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[procUpdateAllergensAtSite] @SectorCode as varchar(20)
as begin

   
-- Update RecipeMOGMapping Table Allergsns

update mog
	set		mog.AllergensName = uniqueApl.AllergensName
			from SiteRecipeMOGMapping mog
					left join(
							SELECT uapl.ArticleNumber,uapl.MOGCode, 
							 STUFF((SELECT ', ' + al.Name
							 FROM cookbookcommon.dbo.AllergenAPLMapping aam
							 left join cookbookcommon.dbo.Allergen al on al.AllergenCode = aam.AllergenCode
							 WHERE aam.ArticleNumber = uapl.ArticleNumber 
							 FOR XML PATH('')), 1, 2, '') as AllergensName
							, apl.SiteCode
										from  [cookbookcommon].[dbo].UniqueAPLMaster  uapl
										inner join 
											  (  Select ArticleNumber, sm.SiteCode
													   from cookbookcommon.dbo.APLMaster am
														  inner join
														  cookbookcommon.dbo.SiteMaster sm
														 on am.Site = case when sm.SiteType = 'STP-001' then sm.CPUCode else sm.SiteCode end and sm.SectorCode = @SectorCode
												 where    ArticleType in ('ZFOD', 'ZPRP') and am.IsActive = 1
												 group by ArticleNumber, sm.SiteCode
											  ) as apl 
										  on  uapl.ArticleNumber = apl.ArticleNumber
										 ) as uniqueApl
										  on uniqueApl.MOGCode = MOG.MOGCode 
				WHERE mog.SiteCode = uniqueApl.SiteCode and  uniqueApl.AllergensName is not null 
				

-- update Recipe Allergens --


  
  IF OBJECT_ID('TempDB..#TEMPSiteRecipe') IS NOT NULL DROP TABLE #TEMPSiteRecipe

 ;WITH rCTE AS(
    SELECT  SiteCode ,BaseRecipeCode,MOGCode, cast(Quantity as decimal(18,3)) as MOGQty,RecipeCode ,RecipeCode as FinalRecipeCode
	FROM    SiteRecipeMOGMapping where IsActive = 1 
	
	UNION ALL
    
	SELECT t.SiteCode, t.BaseRecipeCode,t.MOGCode, cast((cast(r.MOGQty as decimal(18,3)) * cast(t.Quantity as decimal(18,3))/10) as decimal(18,3)) as MOGQty
	,t.RecipeCode ,r.FinalRecipeCode
    FROM    SiteRecipeMOGMapping t
            INNER JOIN rCTE r
            ON t.RecipeCode = r.BaseRecipeCode and t.SiteCode= r.SiteCode
	where t.IsActive = 1
   ), 
   rCTE2 AS(
      select MOGCode,FinalRecipeCode,RecipeCode,BaseRecipeCode,sum(rCTE.MOGQty) as MOGQty, SiteCode
	 
	  from   rCTE
	  --where  MOGCode is not null
	 group by  MOGCode,FinalRecipeCode,RecipeCode,BaseRecipeCode,SiteCode
   )

    SELECT  FinalRecipeCode,RecipeCode,BaseRecipeCode,MOGCode,MOGQty, SiteCode
	into    #TEMPSiteRecipe
	FROM    rCTE2


	update r
		 set  r.AllergensName   =  RecipeAllergen.NewField
    from SiteRecipe r
	Join (SELECT 
	 SiteCode
	,FinalRecipeCode
	,STRING_AGG(x.AllergensName, ',') WITHIN GROUP (ORDER BY x.AllergensName) AS 
	NewField
	from (
		SELECT  distinct uniqueApl.AllergensName, FinalRecipeCode, uniqueApl.SiteCode
   		 From   #TEMPSiteRecipe trs  
		   left join(
				 SELECT uapl.ArticleNumber,uapl.MOGCode, alg.Name as AllergensName, apl.SiteCode
										from  [cookbookcommon].[dbo].UniqueAPLMaster  uapl
										inner join 
											  (  Select ArticleNumber, sm.SiteCode
													   from cookbookcommon.dbo.APLMaster am
														  inner join
														  cookbookcommon.dbo.SiteMaster sm
														 on am.Site = case when sm.SiteType = 'STP-001' then sm.CPUCode else sm.SiteCode end and sm.SectorCode = @SectorCode
												 where ArticleType in ('ZFOD', 'ZPRP') and am.IsActive = 1
												 group by ArticleNumber, sm.SiteCode
											  ) as apl 
										  on  uapl.ArticleNumber = apl.ArticleNumber
										  left join cookbookcommon.dbo.AllergenAPLMapping aam on aam.ArticleNumber = uapl.ArticleNumber
										  left join cookbookcommon.dbo.Allergen alg on alg.AllergenCode = aam.AllergenCode
										 ) as uniqueApl
						      on uniqueApl.MOGCode = trs.MOGCode and uniqueApl.SiteCode = trs.SiteCode
				 where uniqueApl.AllergensName is not null
	) x
	GROUP BY SiteCode ,FinalRecipeCode
	)RecipeAllergen
  on r.RecipeCode = RecipeAllergen.FinalRecipeCode and r.SiteCode = RecipeAllergen.SiteCode

  ---- Update Dishes Allergens
  
update d
		 set   AllergensName = r.AllergensName
		 From SiteDish d 
		 inner join SiteDishRecipeMapping drm on drm.DishCode = d.DishCode and drm.IsDefault = 1 and drm.SiteCode = d.SiteCode
		 inner join SiteRecipe r  on r.RecipeCode = drm.RecipeCode  
		 where  r.SiteCode = drm.SiteCode

 ---- Update Items Allergens


update it 
set AllergensName = al.Allergens
from SiteItemInheritanceMapping it
JOIN (select i.ItemCode, i.SiteCode,
   dbo.DistinctList(STRING_AGG(cast(d.AllergensName as NVARCHAR(MAX)), ',') WITHIN GROUP (ORDER BY d.AllergensName),',') AS Allergens
from SiteItemInheritanceMapping i
left join SiteItemDishMapping idm on idm.ItemCode = i.ItemCode and idm.SiteCode = i.SiteCode
left join  SiteDish  d on d.DishCode = idm.DishCode  
where d.DishCode is not null and d.AllergensName is not null and d.SiteCode = idm.SiteCode 
group by i.ItemCode, i.SiteCode
) al
on al.ItemCode = it.ItemCode
where it.SiteCode = al.SiteCode

end


Go

GO


  
ALTER procedure [dbo].[procGetSiteRecipeList] @siteCode nvarchar(10) ,@condition nvarchar(4)  
as  
begin  
select r.ID,
       r.Site_ID,
	   r.SiteCode,
       rm.Name,
	   rm.RecipeAlias,
	   rm.ID as Recipe_ID,
	   r.RecipeCode,
       r.UOM_ID,
	   r.UOMCode,
       u.Name as UOMName,  
       r.Quantity,  
       r.Instructions,  
       r.IsBase,  
	    (case when RecipeCount.RecipeCount > 0
	       then 2
		   else 1
	  end) as Status, 
       rm.ServingTemperature_ID,
       st.Name as ServingTemperatureName,  
       r.IsActive  ,
	    r.CostPerKG,
	   r.CostPerUOM,
	   r.TotalCost,
	    r.CreatedBy,
	   r.CreatedOn,
	  (case when r.IsBase = 1
	       then 'Base Recipe'
		   else 'Final Recipe'
	  end) as RecipeType,
	  r.AllergensName
from   dbo.SiteRecipe r
       left join Recipe rm
	   on rm.RecipeCode=r.RecipeCode
       left join UOM u  
       on r.UOM_ID = u.ID  
       left join CookBookCommon..ServingTemperature st  
       on rm.ServingTemperature_ID = st.ID  
	     left join (
		     select  RecipeCode ,count(1) as RecipeCount 
			 from SiteRecipeMOGMapping 
			 group by RecipeCode
		) as RecipeCount on r.RecipeCode=RecipeCount.RecipeCode
where  r.IsBase = (case @condition when 'All' then r.IsBase  
                                   when 'Base' then 1  
                                   when 'Main' then 0  
                    end)  and r.SiteCode = @siteCode
end

Go


GO
/****** Object:  StoredProcedure [dbo].[procGetRegionRecipeList]    Script Date: 7/12/2021 7:38:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
ALTER procedure [dbo].[procGetRegionRecipeList] @Region_ID int,@condition nvarchar(4)  , @SubSectorCode nvarchar(20)
as  
begin  
select r.ID,
       r.Region_ID,
       rm.Name,
	   rm.RecipeAlias,
	   rm.ID as Recipe_ID,
	   r.RecipeCode,
       r.UOM_ID,
	   r.UOMCode,
       u.Name as UOMName,  
       r.Quantity,  
       r.Instructions,  
       r.IsBase,  
	   (case when RecipeCount.RecipeCount > 0
	       then 2
		   else 1
	  end) as Status, 
       rm.ServingTemperature_ID,
       st.Name as ServingTemperatureName,  
       r.IsActive  ,
	    r.CostPerKG,
	   r.CostPerUOM,
	   r.TotalCost,
	   r.SectorCostPerKG,
	   r.SectorCostPerUOM,
	   r.SectorTotalCost,
	   r.CreatedBy,
	   r.CreatedOn,
	  (case when r.IsBase = 1
	       then 'Base Recipe'
		   else 'Final Recipe'
	  end) as RecipeType,
	  r.SubSectorCode,
	  r.AllergensName
from   dbo.RegionRecipe r
       left join Recipe rm
	   on rm.RecipeCode=r.RecipeCode
       left join UOM u  
       on r.UOM_ID = u.ID  
       left join CookBookCommon..ServingTemperature st  
       on rm.ServingTemperature_ID = st.ID  
	   left join (
		     select  RecipeCode ,count(1) as RecipeCount 
			 from RegionRecipeMOGMapping 
			 group by RecipeCode
		) as RecipeCount on r.RecipeCode=RecipeCount.RecipeCode
where  r.IsBase = (case @condition when 'All' then r.IsBase  
                                   when 'Base' then 1  
                                   when 'Main' then 0  
                    end)  and r.[Region_ID] = @Region_ID
	and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)
end

Go


GO
/****** Object:  StoredProcedure [dbo].[procGetRecipeList]    Script Date: 7/12/2021 7:41:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
ALTER procedure [dbo].[procGetRecipeList] @condition nvarchar(10)='TALL'  , @Dishcode nvarchar(10)=N'0'
as  
begin  
if @condition = 'TALL' or  @condition = 'TBASE' or  @condition = 'TMAIN'
Begin
select r.ID,
       r.Name,
	   r.RecipeAlias,
	   r.RecipeCode,
       r.UOM_ID,
	   r.UOMCode,
       u.Name as UOMName,  
       r.Quantity,  
       r.Yield,  
       r.Instructions,  
       r.IsBase,  
       r.Type,
       r.ServingTemperature_ID,
       st.Name as ServingTemperatureName,  
       r.IsActive  ,
	   r.CostPerKG,
	   r.CostPerUOM,
	   r.TotalCost,
	   isnull(r.Status,1) as Status, 
	   r.CreatedOn,
	   r.CreatedBy,
	   r.ModifiedOn as PublishedDate,
	  (case when IsBase = 1
	       then ISNULL(RBRCount.BRCount,0) 
		   else ISNULL( DBRCount.BRCount,0)
	  end) as Impact,
	  (case when IsBase = 1
	       then 'Base Recipe'
		   else 'Final Recipe'
	  end) as RecipeType,
	  r.AllergensName

from   Recipe r  
       left join UOM u  
       on r.UOM_ID = u.ID  
       left join CookBookCommon..ServingTemperature st  
       on r.ServingTemperature_ID = st.ID  
	   left join (
		     select BaseRecipeCode, count(1) as BRCount 
			 from RecipeMOGMapping 
			 group by BaseRecipeCode
		) as RBRCount
		on r.RecipeCode = RBRCount.BaseRecipeCode
		left join (
		     select RecipeCode, count(1) as BRCount 
			 from DishRecipeMapping 
			 group by RecipeCode
		) as DBRCount
		on r.RecipeCode = DBRCount.RecipeCode
       
where  r.IsBase = (case @condition when 'TAll' then r.IsBase  
                                   when 'TBASE' then 1  
                                   when 'TMAIN' then 0  
                    end)  and  r.RecipeCode Not in(
		select distinct RecipeCode from DishRecipeMapping where DishCode<>@Dishcode
		) 

end
else
begin
select r.ID,
       r.Name,
	      r.RecipeAlias,
	   r.RecipeCode,
       r.UOM_ID,
	   r.UOMCode,
       u.Name as UOMName,  
       r.Quantity,  
       r.Yield,  
       r.Instructions,  
       r.IsBase,  
       r.Type,
       r.ServingTemperature_ID,
       st.Name as ServingTemperatureName,  
       r.IsActive  ,
	   r.CostPerKG,
	   r.CostPerUOM,
	   r.TotalCost,
	   isnull(r.Status,1) as Status, 
	   r.CreatedOn,
	   r.CreatedBy,
	   r.ModifiedOn as PublishedDate,
	  (case when IsBase = 1
	       then ISNULL(RBRCount.BRCount,0) 
		   else ISNULL( DBRCount.BRCount,0)
	  end) as Impact,
	  (case when IsBase = 1
	       then 'Base Recipe'
		   else 'Final Recipe'
	  end) as RecipeType

from   Recipe r  
       left join UOM u  
       on r.UOM_ID = u.ID  
       left join CookBookCommon..ServingTemperature st  
       on r.ServingTemperature_ID = st.ID  
	   left join (
		     select BaseRecipeCode, count(1) as BRCount 
			 from RecipeMOGMapping 
			 group by BaseRecipeCode
		) as RBRCount
		on r.RecipeCode = RBRCount.BaseRecipeCode
		left join (
		     select RecipeCode, count(1) as BRCount 
			 from DishRecipeMapping 
			 group by RecipeCode
		) as DBRCount
		on r.RecipeCode = DBRCount.RecipeCode

where  r.IsBase = (case @condition when 'All' then r.IsBase  
                                   when 'Base' then 1  
                                   when 'Main' then 0  
                    end)  
end
end


Go


GO
/****** Object:  StoredProcedure [dbo].[procGetDishList]    Script Date: 7/12/2021 7:43:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetDishList] @FoodProgramID int = null, @DietCategoryID int = null      
as      
Begin      
select  d.ID,      
        d.DishCode,    
        d.Name,  
		isnull(d.DishAlias,d.Name) as DishAlias,
        d.DishCategory_ID,  
        d.DishCategoryCode,  
		dc.Name as DishCategoryName,      
		d.DietCategory_ID,      
		d.DietCategoryCode,      
		dt.Name as DietCategoryName,      
		Calorie,      
		d.DishType_ID,      
		d.DishTypeCode,      
		dy.Name as DishTypeName,      
		d.DishSubCategory_ID,      
		d.DishSubCategoryCode,      
		ds.Name as DishSubCategoryName,      
		d.IsActive,      
		d.UOM_ID,      
		d.UOMCode,      
		um.Name as UOMName,      
		d.FoodProgram_ID,      
		d.FoodProgramCode,      
		fp.Name as FoodProgramName,      
		d.ServingTemperature_ID,      
		d.ServingTemperatureCode,      
		st.Name as ServingTemperatureName,  
		d.CreatedBy,  
		d.CreatedOn,  
		d.ModifiedBy,  
		d.ModifiedOn,
		d.ColorCode,
		isnull(c.RGBCode,'rgb(255,255,255,0)') as RGBCode,
		c.HexCode,
		c.FontColor,
		ISNULL( d.CostPerKG,0)  as CostPerKG,
		d.IsSensitive,
		d.SensitiveNote,
		d.Status,
		d.BoughtFromOutSide,
		ISNULL(ItemCount.ItemCount,0) as ImpactedItems,
		d.CuisineCode,
		cm.Name CuisineName,
		d.AllergensName
from    Dish d      
		left join DishCategory dc      
		on d.DishCategoryCode = dc.DishCategoryCode      
		left join CookBookCommon..DietCategory dt      
		on d.DietCategoryCode = dt.DietCategoryCode      
		left join DishSubCategory ds      
		on d.DishSubCategoryCode = ds.DishSubCategoryCode      
		left join FoodProgram fp      
		on d.FoodProgramCode = fp.FoodProgramCode      
		left join DishType dy      
		on d.DishTypeCode = dy.DishTypeCode      
		left join UOM um      
		on d.UOMCode = um.UOMCOde      
		left join CookBookCommon..ServingTemperature st      
		on d.ServingTemperatureCode = st.ServingTemperatureCode      
		left join Color c
		on d.ColorCode = c.ColorCode
		left join (
		     select dishcode, count(1) as ItemCount 
			 from ItemDishMapping 
			 group by DishCode
		) as ItemCount
		on d.DishCode = ItemCount.DishCode
		left join CookBookCommon..CuisineMaster cm      
		on d.CuisineCode = cm.CuisineCode 
where   (FoodProgram_ID = @FoodProgramID or @FoodProgramID is null)      
  and (DietCategory_ID = @DietCategoryID or @DietCategoryID is null) 
End

Go


GO
/****** Object:  StoredProcedure [dbo].[procGetRegionDishList]    Script Date: 7/12/2021 7:44:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetRegionDishList] @RegionID int , @FoodProgramID int = null,
@DietCategoryID int = null, @SubSectorCode nvarchar(20)      
as      
Begin      
select        
        d.DishCode,    

	   d.ID,
        d.Name,  
		isnull(d.DishAlias,d.Name) as DishAlias,    
        d.DishCategory_ID,  
        d.DishCategoryCode,  
		dc.Name as DishCategoryName,      
		d.DietCategory_ID,      
		d.DietCategoryCode,      
		dt.Name as DietCategoryName,      
		Calorie,      
		d.DishType_ID,      
		d.DishTypeCode,      
		dy.Name as DishTypeName,      
		d.DishSubCategory_ID,      
		d.DishSubCategoryCode,      
		ds.Name as DishSubCategoryName,      
		d.IsActive,      
		d.UOM_ID,      
		d.UOMCode,      
		um.Name as UOMName,      
		d.FoodProgram_ID,      
		d.FoodProgramCode,      
		fp.Name as FoodProgramName,      
		d.ServingTemperature_ID,      
		d.ServingTemperatureCode,      
		st.Name as ServingTemperatureName,  
		d.CreatedBy,  
		d.CreatedOn,  
		d.ModifiedBy,  
		isnull(rd.ModifiedOn,rd.CreatedOn) as ModifiedOn,
		d.ColorCode,
		isnull(c.RGBCode,'rgb(255,255,255,0)') as RGBCode,
		c.HexCode,
		c.FontColor,
		rd.SubSectorCode,
		cm.CuisineCode,
		cm.Name as CuisineName,
		rd.AllergensName
from   RegionDish rd 
        Left Join Dish d
		on d.DishCode = rd.DishCode
		left join DishCategory dc      
		on d.DishCategoryCode = dc.DishCategoryCode      
		left join CookBookCommon..DietCategory dt      
		on d.DietCategoryCode = dt.DietCategoryCode      
		left join DishSubCategory ds      
		on d.DishSubCategoryCode = ds.DishSubCategoryCode      
		left join FoodProgram fp      
		on d.FoodProgramCode = fp.FoodProgramCode      
		left join DishType dy      
		on d.DishTypeCode = dy.DishTypeCode      
		left join UOM um      
		on d.UOMCode = um.UOMCOde      
		left join CookBookCommon..ServingTemperature st      
		on d.ServingTemperatureCode = st.ServingTemperatureCode      
		left join Color c
		on d.ColorCode = c.ColorCode
		left join CookBookCommon..CuisineMaster cm
		on d.CuisineCode = cm.CuisineCode
		
where  

   (FoodProgram_ID = @FoodProgramID or @FoodProgramID is null)      
  and (DietCategory_ID = @DietCategoryID or @DietCategoryID is null) 
  and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)

  and rd.Region_ID = @RegionID and rd.IsActive=1

End


Go


GO
/****** Object:  StoredProcedure [dbo].[procGetSiteDishList]    Script Date: 7/12/2021 7:45:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetSiteDishList]@SiteCode nvarchar(10), @FoodProgramID int = null, @DietCategoryID int = null      
as      
Begin      
select  distinct d.ID,      
        d.DishCode,    
        d.Name,  
			isnull(d.DishAlias,d.Name) as DishAlias,  
        d.DishCategory_ID,  
        d.DishCategoryCode,  
		dc.Name as DishCategoryName,      
		d.DietCategory_ID,      
		d.DietCategoryCode,      
		dt.Name as DietCategoryName,      
		Calorie,      
		d.DishType_ID,      
		d.DishTypeCode,      
		dy.Name as DishTypeName,      
		d.DishSubCategory_ID,      
		d.DishSubCategoryCode,      
		ds.Name as DishSubCategoryName,      
		d.IsActive,      
		d.UOM_ID,      
		d.UOMCode,      
		um.Name as UOMName,      
		d.FoodProgram_ID,      
		d.FoodProgramCode,      
		fp.Name as FoodProgramName,      
		d.ServingTemperature_ID,      
		d.ServingTemperatureCode,      
		st.Name as ServingTemperatureName,  
		d.CreatedBy,  
		d.CreatedOn,  
		d.ModifiedBy,  
		d.ModifiedOn,
		d.ColorCode,
		isnull(c.RGBCode,'rgb(255,255,255,0)') as RGBCode,
		c.HexCode,
		c.FontColor,
		ISNULL(sd.CostPerKG,0) as CostPerKG,
		isnull(d.Status,1) as Status,
		d.CuisineCode,
		cm.Name as CuisineName,
		sd.AllergensName
from    Dish d      
        inner join SiteDish sd on sd.DishCode = d.DishCode
		left join DishCategory dc      
		on d.DishCategoryCode = dc.DishCategoryCode      
		left join CookBookCommon..DietCategory dt      
		on d.DietCategoryCode = dt.DietCategoryCode      
		left join DishSubCategory ds      
		on d.DishSubCategoryCode = ds.DishSubCategoryCode      
		left join FoodProgram fp      
		on d.FoodProgramCode = fp.FoodProgramCode      
		left join DishType dy      
		on d.DishTypeCode = dy.DishTypeCode      
		left join UOM um      
		on d.UOMCode = um.UOMCOde      
		left join CookBookCommon..ServingTemperature st      
		on d.ServingTemperatureCode = st.ServingTemperatureCode      
		left join Color c
		on d.ColorCode = c.ColorCode
			left join CookBookCommon..CuisineMaster cm
		on d.CuisineCode = cm.CuisineCode
		--left join (select * from fnGetAllDishCostsAtSite(@SiteCode)) AS dcr
		--on dcr.DishCode = d.DishCode
where 
	sd.SiteCode = @SiteCode and
    (FoodProgram_ID = @FoodProgramID or @FoodProgramID is null)      
    and (DietCategory_ID = @DietCategoryID or @DietCategoryID is null) 
End


Go


GO
/****** Object:  StoredProcedure [dbo].[procGetItemList]    Script Date: 7/12/2021 7:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetItemList] @ItemCode nvarchar(10), @FoodProgramID int = null, @DietCategoryID int = null  
as  
Begin  
select Item.ID,  
    item.ItemCode,   
    Item.Name as ItemName,   
    VisualCategory_ID,  
    Item.VisualCategoryCode,  
    vc.Name as VisualCategoryName,  
    FoodProgram_ID,  
    Item.FoodProgramCode,  
    fp.Name as FoodProgramName,  
    DietCategory_ID,  
    Item.DietCategoryCode,  
    dc.Name as DietCategoryName,  
    ConceptType1_ID,  
    Item.ConceptType1Code,  
    ct1.Name as ConceptType1Name,  
    fp.ConceptType2_ID,  
    fp.ConceptType2Code,  
    ct2.Name as ConceptType2Name,  
    ConceptType3_ID,  
    Item.ConceptType3Code,  
    ct3.Name as ConceptType3Name,  
    ConceptType4_ID,  
    Item.ConceptType4Code,  
    ct4.Name as ConceptType4Name,  
    ConceptType5_ID,  
    Item.ConceptType5Code,  
    ct5.Name as ConceptType5Name,  
    ItemType1_ID,  
    Item.ItemType1Code,  
    it1.Name as ItemType1Name,  
    ItemType2_ID,  
    Item.ItemType2Code,  
    it1.Name as ItemType2Name,  
    Serveware_ID,  
    Item.ServewareCode,  
    sw.Name as ServewareName,  
    HSNCode,  
    CGST,  
    SGST,  
    UGST,  
    CESS,  
    isnull(ImageName,'default.png') as ImageName,  
    ApplicableTo,  
    isnull(Status,1) as Status,  
     Item.ModifiedOn as PublishedDate, --(select top 1  ModifiedOn  from ItemDishMapping idm where idm.ItemCode = item.ItemCode and item.Status =3) as PublishedDate  ,
	Item.IsActive,
	Item.CreatedOn,
	Item.CreatedBy,
	Item.SubSectorCode,
	ssm.Name as SubSectorName,
	Item.AllergensName
from Item  
     left join cookbookcommon..VisualCategory vc  
  on Item.VisualCategoryCode = vc.VisualCategoryCode  
  left join FoodProgram fp  
  on Item.FoodProgramCode = fp.FoodProgramCode  
  left join cookbookcommon..DietCategory dc  
  on Item.DietCategoryCode = dc.DietCategoryCode  
  left join cookbookcommon..ConceptType1 ct1  
  on Item.ConceptType1Code = ct1.ConceptType1Code  
  left join cookbookcommon..ConceptType2 ct2  
  on fp.ConceptType2Code = ct2.ConceptType2Code  
  left join cookbookcommon..ConceptType3 ct3  
  on Item.ConceptType3Code = ct3.ConceptType3Code  
  left join cookbookcommon..ConceptType4 ct4  
  on Item.ConceptType4Code = ct4.ConceptType4Code  
  left join cookbookcommon..ConceptType5 ct5  
  on Item.ConceptType5Code = ct5.ConceptType5Code  
  left join cookbookcommon..ItemType1 it1  
  on Item.ItemType1Code = it1.ItemType1Code  
  left join cookbookcommon..ItemType2 it2  
  on Item.ItemType2Code = it2.ItemType2Code  
  left join cookbookcommon..Serveware sw  
  on Item.ServewareCode = sw.ServewareCode  
  left join SubSectorMaster ssm
  on ssm.SubSectorCode = Item.SubSectorCode  
  --right join ItemDishMapping idm  
  --on Item.ItemCode = idm.ItemCode  
where Item.ItemCode = (case when len(isnull(@ItemCode,'')) = 0  then Item.ItemCode else @ItemCode end)  
  and (FoodProgram_ID = @FoodProgramID or @FoodProgramID is null)  
  and (DietCategory_ID = @DietCategoryID or @DietCategoryID is null)  
End

Go


GO
/****** Object:  StoredProcedure [dbo].[procGetBaseRecipesByRecipeID]    Script Date: 7/12/2021 7:50:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetBaseRecipesByRecipeID] @RecipeID int
as
begin
select rm.Recipe_ID,
     --   r.Name as RecipeName,
		  CASE
      WHEN r.RecipeAlias is NULL or r.RecipeAlias='' or r.RecipeAlias=r.Name  THEN r.Name
    ELSE r.Name+' ('+r.RecipeAlias+') 'end  as RecipeName,
		r.RecipeAlias,
	   rm.BaseRecipe_ID,
	   rm.BaseRecipeCode,
	   rm.UOMCode,
	 --  br.Name as BaseRecipeName,
	   CASE
      WHEN br.RecipeAlias is NULL or br.RecipeAlias=''THEN br.Name
    ELSE br.Name+' ('+br.RecipeAlias+') 'end  as BaseRecipeName,
	   rm.Quantity,
	   rm.Yield,
	   rm.UOM_ID,
	    u.Name as UOMName,
	   rm.IsActive,
	   rm.CreatedBy,
	   rm.CreatedOn,
	   rm.ModifiedOn,
	   rm.ModifiedBy,
	   dbo.fnMathRoundOff(rm.CostPerUOM,2)  as CostPerUOM,
	   dbo.fnMathRoundOff(rm.CostPerKG,2)  as CostPerKG,
	   dbo.fnMathRoundOff(rm.TotalCost,2)  as TotalCost,
	   rm.IngredientPerc,
	   rm.IsMajor,
	   rm.AllergensName
from   RecipeMOGMapping rm
       left join
	   Recipe r
	   on rm.RecipeCode= r.RecipeCode
	   left join
	   UOM u
	   on rm.UOMCode = u.UOMCode
	   inner join
	   Recipe br
	   on rm.BaseRecipeCode= br.RecipeCode
where  (@RecipeID is null or rm.Recipe_ID = @RecipeID) and rm.IsActive =1 order by rm.ID 
end



Go

--USE [CookBookCore]
GO
/****** Object:  StoredProcedure [dbo].[procGetMOGsByRecipeID]    Script Date: 7/12/2021 7:52:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetMOGsByRecipeID] @RecipeID int
as
begin
select rm.Recipe_ID,
       r.Name as RecipeName,
	   rm.MOG_ID,
	   rm.MOGCode,
	   CASE
      WHEN m.Alias is NULL or m.Alias='' or m.Alias=m.Name THEN m.Name
    ELSE m.Name+' ('+m.Alias+') 'end  as MOGName,
	 --  m.Name as MOGName,
	   m.Alias as MogAlias,
	   rm.Quantity,
	   rm.Yield,
	   rm.UOM_ID,
	   u.Name as UOMName,
	   rm.IsActive,
	   rm.CreatedBy,
	   rm.CreatedOn,
	   rm.ModifiedOn,
	   rm.ModifiedBy, 
	   dbo.fnMathRoundOff(rm.CostPerUOM,2)  as CostPerUOM,
	   dbo.fnMathRoundOff(rm.CostPerKG,2)  as CostPerKG,
	   dbo.fnMathRoundOff(rm.TotalCost,2)  as TotalCost,
	   rm.UOMCode,
	   isnull(rm.IngredientPerc,0) as IngredientPerc,
	   rm.IsMajor,
	   rm.AllergensName
from   RecipeMOGMapping rm
       left join
	   Recipe r
	   on rm.RecipeCode = r.RecipeCode
	   left join
	   UOM u
	   on rm.UOMCode = u.UOMCode
	   inner join
	   MOG m
	   on rm.MOGCode = m.MOGCode
where  rm.Recipe_ID = @RecipeID and rm.IsActive = 1 order by rm.ID 
end

Go

--USE [CookBookCore]
GO
/****** Object:  StoredProcedure [dbo].[procGetRecipesByRegionID]    Script Date: 7/12/2021 7:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetRecipesByRegionID] @RegionID int
as
begin
select rm.Recipe_ID,
       r.Name as RecipeName,
	   rm.MOG_ID,
	   m.Name as MOGName,
	   rm.Quantity,
	   rm.Yield,
	   rm.UOM_ID,
	   u.Name as UOMName,
	   rm.IsActive,
	   rm.CreatedBy,
	   rm.CreatedOn,
	   rm.ModifiedOn,
	   rm.ModifiedBy,
	   rm.CostPerUOM,
	   rm.CostPerKG,
	   rm.TotalCost,
	   rm.AllergensName
from   RegionRecipeMOGMapping rm
       left join
	   Recipe r
	   on rm.Recipe_ID = r.ID
	   left join
	   UOM u
	   on rm.UOM_ID = u.ID
	   inner join
	   MOG m
	   on rm.MOG_ID = m.ID
where  rm.Region_ID = @RegionID 
end

Go

--USE [CookBookCore]
GO
/****** Object:  StoredProcedure [dbo].[procGetRegionBaseRecipesByRecipeID]    Script Date: 7/12/2021 7:53:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetRegionBaseRecipesByRecipeID]  @RecipeID int,  @RegionID int, @SubSectorCode nvarchar(20)
as
begin
select 
       rm.ID,
	    rm.Recipe_ID,
		rm.Region_ID,
        rc.Name as RecipeName,
		rb.RecipeAlias,
	   rm.BaseRecipe_ID,
	   --rb.Name as BaseRecipeName,
	      CASE
      WHEN rb.RecipeAlias is NULL or rb.RecipeAlias=''or rb.RecipeAlias=rb.Name THEN rb.Name
    ELSE rb.Name+' ('+rb.RecipeAlias+') 'end  as BaseRecipeName,
	   rm.BaseRecipeCode,
	   rm.UOMCode,
	   rm.Quantity,
	   rm.Yield,
	   rm.UOM_ID,
	    u.Name as UOMName,
	   rm.IsActive,
	   rm.CreatedBy,
	   rm.CreatedOn,
	   rm.ModifiedOn,
	   rm.ModifiedBy,
	  dbo.fnMathRoundOff(rm.CostPerUOM,2)  as CostPerUOM,
	   dbo.fnMathRoundOff(rm.CostPerKG,2)  as CostPerKG,
	   dbo.fnMathRoundOff(rm.TotalCost,2)  as TotalCost,
	   rm.IsMajor,
	   rm.IngredientPerc,
	   rm.SectorIngredientPerc,
	   rm.SectorTotalCost,
	   rm.SectorQuantity,
	   rm.SubSectorCode,
	   rm.AllergensName
from   RegionRecipeMOGMapping rm
       right join RegionRecipe r on rm.RecipeCode = r.RecipeCode and rm.Region_ID = r.Region_ID
	   left join UOM u on rm.UOMCode = u.UOMCode 
	   inner join RegionRecipe br on rm.BaseRecipeCode = br.RecipeCode and br.Region_ID =rm.Region_ID
	   left join Recipe rc on rc.RecipeCode = r.RecipeCode
	   left join Recipe rb on rb.RecipeCode =br.RecipeCode
where rm.Region_ID = @RegionID and r.Region_ID =@RegionID
	   and ISNULL(rm.SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(rm.SubSectorCode,'') else @subsectorcode end)
       and rm.Recipe_ID = @RecipeID order by rm.id
	  
end

Go

--USE [CookBookCore]
GO
/****** Object:  StoredProcedure [dbo].[procGetRegionMOGsByRecipeID]    Script Date: 7/12/2021 7:54:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procGetRegionMOGsByRecipeID] @RecipeID int,@RegionID int, @SubSectorCode nvarchar(20)
as
begin
select  rm.ID,rm.Recipe_ID,
       r.Name as RecipeName,
	   rm.MOG_ID,
	   rm.MOGCode,
	  -- m.Name as MOGName,
	     CASE
      WHEN m.Alias is NULL or m.Alias=''or m.Alias=m.Name THEN m.Name
    ELSE m.Name+' ('+m.Alias+') 'end  as MOGName,
	   rm.Quantity,
	   rm.Yield,
	   rm.UOM_ID,
	   u.Name as UOMName,
	   rm.IsActive,
	   rm.CreatedBy,
	   rm.CreatedOn,
	   rm.ModifiedOn,
	   rm.ModifiedBy,
	   dbo.fnMathRoundOff(rm.CostPerUOM,2)  as CostPerUOM,
	   dbo.fnMathRoundOff(rm.CostPerKG,2)  as CostPerKG,
	   dbo.fnMathRoundOff(rm.TotalCost,2)  as TotalCost,
	   rm.UOMCode,
	   rm.IsMajor,
	   rm.IngredientPerc,
	    rm.SectorIngredientPerc,
	   rm.SectorTotalCost,
	   rm.SectorQuantity,
	    rm.SubSectorCode,
		rm.AllergensName
from   RegionRecipeMOGMapping rm
       left join
	   RegionRecipe r
	   on rm.RecipeCode = r.RecipeCode and rm.Region_ID = r.Region_ID
	   left join
	   UOM u
	   on rm.UOMCode = u.UOMCode
	   inner join
	   MOG m
	   on rm.MOGCode = m.MOGCode
where  rm.Recipe_ID = @RecipeID
	   and ISNULL(rm.SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(rm.SubSectorCode,'') else @subsectorcode end) 
       and rm.Region_ID = @RegionID order by rm.ID

end


Go

--USE [CookBookCore]
GO
/****** Object:  StoredProcedure [dbo].[procGetSiteMOGsByRecipeID]    Script Date: 7/12/2021 7:55:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[procGetSiteMOGsByRecipeID] @RecipeID int,@RecipeCode nvarchar(10),@siteCode nvarchar(10)
as
begin
select rm.ID,
       rm.Recipe_ID,
       r.Name as RecipeName,
	   rm.MOG_ID,
	   rm.MOGCode,
	 --  m.Name as MOGName,
	    CASE
      WHEN m.Alias is NULL or m.Alias='' or m.Alias=m.Name then m.Name
    ELSE m.Name+' ('+m.Alias+') 'end  as MOGName,
	   rm.Quantity,
	   rm.Yield,
	   rm.UOM_ID,
	   u.Name as UOMName,
	   rm.IsActive,
	   rm.CreatedBy,
	   rm.CreatedOn,
	   rm.ModifiedOn,
	   rm.ModifiedBy,
	   dbo.fnMathRoundOff(rm.CostPerUOM,2)  as CostPerUOM,
	   dbo.fnMathRoundOff(rm.CostPerKG,2)  as CostPerKG,
	   dbo.fnMathRoundOff(rm.TotalCost,2)  as TotalCost,
	   rm.UOMCode,
	   rm.IngredientPerc,
	   rm.RegionIngredientPerc,
	   rm.Site_ID,
	   rm.SiteCode,
	   rm.Region_ID,
	    rm.IsMajor,
	   rm.RegionTotalCost,
	   rm.RegionQuantity,
	   rm.ArticleNumber,
	   rm.AllergensName
from   SiteRecipeMOGMapping rm
       left join
	   SiteRecipe r
	   on rm.RecipeCode = r.RecipeCode and r.SiteCode = @siteCode
	   left join
	   UOM u
	   on rm.UOMCode = u.UOMCode
	   inner join
	   MOG m
	   on rm.MOGCode = m.MOGCode
where  rm.Recipe_ID = @RecipeID or rm.RecipeCode = @RecipeCode
       and rm.SiteCode = @siteCode order by rm.ID
end

gO

USE [CookBookCore]
GO
/****** Object:  StoredProcedure [dbo].[procGetAPLMasterDataForSite]    Script Date: 7/13/2021 10:46:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  procedure [dbo].[procGetAPLMasterDataForSite] @ArticleNumber nvarchar(20) = null, @MOGCode nvarchar(10) = null,@SiteCode  nvarchar(10) = null    
as     
begin    
     select a.ArticleID,
			a.ArticleNumber,
			a.ArticleDescription,a.UOM,
			a.MerchandizeCategory,
			a.MerchandizeCategoryDesc,
			a.IsActive,
			a.CreatedBy,
			a.CreatedDate,
			a.ModifiedBy,
			a.ModifiedDate,
			a.ArticleType,
			a.Hierlevel3, 
			a.MOGCode,
			m.Name as MOGName, 
			apl.StandardCostPerKg,
			apl.ArticleCost,
			STUFF((SELECT ', ' + al.Name
				   FROM cookbookcommon..AllergenAPLMapping aam
				   left join cookbookcommon..Allergen al on al.AllergenCode = aam.AllergenCode
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensName,
			STUFF((SELECT ', ' + aam.AllergenCode
				   FROM cookbookcommon..AllergenAPLMapping aam
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensCode,
					case when (
						SELECT  max(StandardCostPerKg) FROM  [CookBookCommon].[dbo].UniqueAPLMaster a 
						  left join MOG m  on a.MOGCode = m.MOGCode   
						  inner Join CookBookCommon.dbo.APLMaster apl 
						  on a.ArticleNumber = apl.ArticleNumber and  apl.Site = @SiteCode 
						  where  (@ArticleNumber is null or (a.ArticleNumber = @ArticleNumber)) and    
						(@MOGCode is null or (a.MOGCode = @MOGCode))  and  
						a.ArticleType in ('ZFOD', 'ZPRP') and apl.IsActive = 1
					) = apl.StandardCostPerKg then 1 else 0 end as IsMaxAPLCost
     from   [CookBookCommon].[dbo].UniqueAPLMaster a    
            left join    
            MOG m    
            on a.MOGCode = m.MOGCode   
	        inner Join CookBookCommon.dbo.APLMaster apl 
			on a.ArticleNumber = apl.ArticleNumber and  apl.Site = @SiteCode 
			
     where  (@ArticleNumber is null or (a.ArticleNumber = @ArticleNumber)) and    
            (@MOGCode is null or (a.MOGCode = @MOGCode))  and  
            a.ArticleType in ('ZFOD', 'ZPRP') and apl.IsActive = 1
   order by apl.StandardCostPerKg desc
end    
   
   Go


GO
/****** Object:  StoredProcedure [dbo].[procGetAPLMasterDataForSite]    Script Date: 7/13/2021 10:46:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  procedure [dbo].[procGetAPLMasterDataForSite] @ArticleNumber nvarchar(20) = null, @MOGCode nvarchar(10) = null,@SiteCode  nvarchar(10) = null, @SectorCode  nvarchar(10) = null
as     
begin    
     select a.ArticleID,
			a.ArticleNumber,
			a.ArticleDescription,a.UOM,
			a.MerchandizeCategory,
			a.MerchandizeCategoryDesc,
			a.IsActive,
			a.CreatedBy,
			a.CreatedDate,
			a.ModifiedBy,
			a.ModifiedDate,
			a.ArticleType,
			a.Hierlevel3, 
			a.MOGCode,
			m.Name as MOGName, 
			apl.StandardCostPerKg,
			apl.ArticleCost,
			STUFF((SELECT ', ' + al.Name
				   FROM cookbookcommon..AllergenAPLMapping aam
				   left join cookbookcommon..Allergen al on al.AllergenCode = aam.AllergenCode
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensName,
			STUFF((SELECT ', ' + aam.AllergenCode
				   FROM cookbookcommon..AllergenAPLMapping aam
				   WHERE aam.ArticleNumber = a.ArticleNumber 
				  FOR XML PATH('')), 1, 2, '') as AllergensCode,
					case when (
						SELECT  max(StandardCostPerKg) FROM  [CookBookCommon].[dbo].UniqueAPLMaster a 
						  left join MOG m  on a.MOGCode = m.MOGCode   
						  inner Join CookBookCommon.dbo.APLMaster apl 
						  on a.ArticleNumber = apl.ArticleNumber and  apl.Site = @SiteCode 
						  where  (@ArticleNumber is null or (a.ArticleNumber = @ArticleNumber)) and    
						(@MOGCode is null or (a.MOGCode = @MOGCode))  and  
						a.ArticleType in ('ZFOD', 'ZPRP') and apl.IsActive = 1
					) = apl.StandardCostPerKg then 1 else 0 end as IsMaxAPLCost
     from   [CookBookCommon].[dbo].UniqueAPLMaster a    
            left join MOG m    
            on a.MOGCode = m.MOGCode   
	        inner Join CookBookCommon.dbo.APLMaster apl 
			on a.ArticleNumber = apl.ArticleNumber
			inner join cookbookcommon.dbo.SiteMaster sm
			on apl.Site = case when sm.SiteType = 'STP-001' then sm.CPUCode else sm.SiteCode end and sm.SectorCode = @SectorCode
     where  (@ArticleNumber is null or (a.ArticleNumber = @ArticleNumber)) and    
            (@MOGCode is null or (a.MOGCode = @MOGCode))  and  
            a.ArticleType in ('ZFOD', 'ZPRP') and apl.IsActive = 1  and  apl.Site = @SiteCode 
   order by apl.StandardCostPerKg desc
end    
    
Go

GO
/****** Object:  StoredProcedure [dbo].[SaveSectorToRegionInheritance]    Script Date: 7/13/2021 4:20:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[SaveSectorToRegionInheritance]
@RegionID int,
@ItemCode nvarchar(max),
@UserID int ,
@IsActive bit,
@SubSectorCode nvarchar(10),
@SectorNumber varchar(50)
as
begin

BEGIN TRY

   IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#TempItem]') AND type in (N'U'))
      DROP TABLE [dbo].[#TempItem]

    CREATE TABLE #TempItem (
	   ItemCode nvarchar(20) ,IsActive bit
	)

    insert into #TempItem
	SELECT SUBSTRING(tuple,3,LEN(tuple)) as ItemCode,Cast(SUBSTRING(tuple,1,1) as bit) as IsActive
	FROM   split_string(@ItemCode, ',')


	declare @cntitem as int = 0
	declare @cntdc as int = 0
	declare @cntdsh as int = 0

	set @cntitem = isnull((select isnull(COUNT(1),0) from RegionItemInheritanceMapping where ItemCode in (select ItemCode from #TempItem) and Region_ID = @RegionID
									and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)),0)
	set @cntdc = isnull((select isnull(COUNT(1),0) from RegionDishCategoryMapping where ItemCode in (select ItemCode from #TempItem) and Region_ID = @RegionID
									and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)),0)
	set @cntdsh = isnull((select isnull(COUNT(1),0) from RegionItemDishMapping where ItemCode in (select ItemCode from #TempItem) and Region_ID = @RegionID
									and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)),0)

	if (@cntitem > 0)
	begin
	   delete from RegionItemInheritanceMapping where Region_ID = @RegionID and ItemCode in (select ItemCode from #TempItem t1 where  t1.IsActive=0)
									and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)
	end
	IF EXISTS( select 1 from RegionItemInheritanceMapping where Region_ID=@RegionID and ItemCode IN(select ItemCode from #TempItem ti where ti.IsActive=1)
									and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end))
		begin
			update RIM
			set RIM.Region_ID=@RegionID,RIM.Item_ID=i.ID ,RIM.IsActive = 1, RIM.ModifiedBy = @UserID, RIM.ModifiedOn = getdate(), RIM.Status=1                 
			,SubSectorCode = @SubSectorCode from RegionItemInheritanceMapping RIM 
			join 
			(select ItemCode, Region_ID from RegionItemInheritanceMapping where Region_ID=@RegionID and ItemCode IN(select ItemCode from #TempItem ti where ti.IsActive=1)
									and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)) RIM2
			on  RIM2.ItemCode = RIM.ItemCode and RIM2.Region_ID = RIM.Region_ID
		    join Item i 
			on  RIM2.ItemCode = I.ItemCode
			where ISNULL(RIM.SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(RIM.SubSectorCode,'') else @subsectorcode end)
		end
		else
		begin
		    insert into RegionItemInheritanceMapping(Region_ID,Item_ID,ItemCode,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,Status
			,SubSectorCode)
			select @RegionID, ID, ItemCode, @IsActive, @UserID, GETDATE(), null, null,1,@SubSectorCode
			from   Item i where ItemCode 
			IN (select ItemCode from #TempItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from RegionItemInheritanceMapping where Region_ID = @RegionID 
			                 and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end))) and i.IsActive =1
		end

	if(@IsActive=1)
	begin
		if (@cntdc > 0) begin
		   delete from RegionDishCategoryMapping where Region_ID = @RegionID and ItemCode in (select ItemCode from #TempItem t1 where  t1.IsActive=0)
							and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)
		end

		IF EXISTS( select 1 from RegionDishCategoryMapping where Region_ID=@RegionID and ItemCode IN(select ItemCode from #TempItem ti where ti.IsActive=1)
							and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end))
		begin
			update RDCM
			set RDCM.IsActive = IDCM.IsActive, RDCM.StandardPortion = IDCM.StandardPortion, RDCM.UOMCode = IDCM.UOMCode, 
			RDCM.ModifiedBy =@UserID, RDCM.ModifiedOn = getdate(), RDCM.SubSectorCode = @SubSectorCode         
			from RegionDishCategoryMapping RDCM
			join 
			(select DishCategoryCode, ItemCode, Region_ID from RegionDishCategoryMapping where Region_ID=@RegionID 
			and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end) 
			and  ItemCode IN(select ItemCode from #TempItem ti where ti.IsActive=1)) RDCM2
			on RDCM2.DishCategoryCode = RDCM.DishCategoryCode and RDCM2.ItemCode = RDCM.ItemCode and RDCM2.Region_ID = RDCM.Region_ID
		    join ItemDishCategoryMapping IDCM 
			on RDCM2.DishCategoryCode = IDCM.DishCategoryCode and RDCM2.ItemCode = IDCM.ItemCode
			where  ISNULL(RDCM.SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(RDCM.SubSectorCode,'') else @subsectorcode end)
		end
		else
		begin
		    insert into RegionDishCategoryMapping (Region_ID,Item_ID,DishCategory_ID,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn
				,ItemCode,DishCategoryCode,StandardPortion,UOMCode,WeightPerUOM,SubSectorCode)
			select @RegionID, Item_ID, DishCategory_ID, IDCM.IsActive, @UserID, GETDATE(), null, null, ItemCode, DishCategoryCode,
			StandardPortion,UOMCode,WeightPerUOM,@SubSectorCode
			from   ItemDishCategoryMapping IDCM where ItemCode 
			in (select ItemCode from #TempItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from regiondishcategorymapping where Region_ID = @RegionID 
			and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end))) and IDCM.IsActive =1
		end
	  
		if (@cntdsh > 0) begin
		   delete from RegionItemDishMapping where Region_ID = @RegionID and ItemCode in (select ItemCode from #TempItem t1 where  t1.IsActive=0 )
								and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)
		end

		insert into RegionDishCategory
		select rdcm.Region_ID, rdcm.DishCategoryCode, 0,0,0,null,1,@UserID,getdate(),null,null
		from
		       (
			    select distinct Region_ID, DishCategoryCode
			    from   RegionDishCategoryMapping
			   ) rdcm
		       left join 
			   RegionDishCategory rdc
			   on rdcm.Region_ID = rdc.Region_ID and rdcm.DishCategoryCode = rdc.DishCategoryCode
        where  rdc.Region_ID is null and rdc.DishCategoryCode is null 


		IF EXISTS( select 1 from RegionItemDishMapping where Region_ID=@RegionID and ItemCode IN(select ItemCode from #TempItem ti where ti.IsActive=1)
									and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end))
		begin
			update RIM
			set RIM.Region_ID=@RegionID,RIM.Item_ID=i.Item_ID ,RIM.IsActive = I.IsActive, RIM.ModifiedBy = @UserID, RIM.ModifiedOn = getdate(), RIM.StandardPortion=I.StandardPortion                
			,RIM.ContractPortion = i.ContractPortion, RIM.DishCode= i.DishCode, RIM.UOMCode = i.UOMCode, RIM.WeightPerUOM= i.WeightPerUOM
			,SubSectorCode = @SubSectorCode
			from RegionItemDishMapping RIM 
			join 
			(select ItemCode, Region_ID, DishCode from RegionItemDishMapping where Region_ID=@RegionID 
			and ItemCode IN(select ItemCode from #TempItem ti where ti.IsActive=1)
			and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)) RIM2
			on  RIM2.ItemCode = RIM.ItemCode and RIM2.Region_ID = RIM.Region_ID and RIM.DishCode = RIM2.DishCode
		    join ItemDishMapping i 
			on  RIM2.ItemCode = i.ItemCode and i.DishCode = RIM2.DishCode
			where  ISNULL(RIM.SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(RIM.SubSectorCode,'') else @subsectorcode end)
		end
		else
		begin
		    insert into RegionItemDishMapping (Item_ID,Dish_ID,ContractPortion,StandardPortion,IsActive,CreatedBy,CreatedOn,ModifiedBy
			,ModifiedOn,ItemCode,DishCode,Region_ID,UOMCode,WeightPerUOM,SubSectorCode)
			select Item_ID, Dish_ID, ContractPortion, StandardPortion, IsActive, @UserID,getdate(), null,null, ItemCode, DishCode,
			@RegionID, UOMCode, WeightPerUOM, @SubSectorCode
			from   ItemDishMapping i where ItemCode 
			IN (select ItemCode from #TempItem ti where ti.IsActive=1 and ti.ItemCode 
			NOT IN(select itemcode from RegionItemDishMapping where Region_ID = @RegionID
					and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end))) and i.IsActive =1
		end
		
		insert into RegionDish(Region_ID,DishCode,CostColor,CostPerKG,CostAsOfDate,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,SubSectorCode,AllergensName)
		select rdcm.Region_ID, rdcm.DishCode, null,0,null,1,@UserID,getdate(),null,null,rdcm.SubSectorCode,AllergensName
		from
		       (
			    select distinct Region_ID, SubSectorCode,DishCode
			    from   RegionItemDishMapping
			   ) rdcm
		       left join 
			   RegionDish rdc
			   on rdcm.Region_ID = rdc.Region_ID
			   and rdcm.SubSectorCode = rdc.SubSectorCode
			   and rdcm.DishCode = rdc.DishCode
        where  rdc.Region_ID is null and rdc.DishCode is null

		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#TempItemDish]') AND type in (N'U'))
		  DROP TABLE [dbo].[#TempItemDish]

		CREATE TABLE #TempItemDish (
		   DishCode nvarchar(20) 
		)

		insert into #TempItemDish
		select  DishCode
		from   RegionItemDishMapping where
		ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)
		and ItemCode in (select ItemCode from #TempItem ti where ti.IsActive=1)

		While ((select count(1) from #TempItemDish) > 0)
		begin
			 DECLARE @DishCode varchar(20)
			 SELECT TOP 1 @DishCode = DishCode FROM   #TempItemDish
				exec procSaveRegionItemDishRecipeMapping  @RegionID,@DishCode,@UserID,@SubSectorCode
			 Delete from #TempItemDish where  DishCode = @DishCode 
		end

    end
	DROP TABLE [dbo].[#TempItem]

	select 'True' as Result
END TRY  
BEGIN CATCH  
    SELECT ERROR_MESSAGE() as Result
END CATCH  
end


Go


--USE [CookBookGoogle]
GO
/****** Object:  StoredProcedure [dbo].[procSaveRegionBaseRecipeData]    Script Date: 7/13/2021 4:26:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[procSaveRegionBaseRecipeData] (
  @Region_ID int
 ,@RecipeCode  nvarchar(10)
 ,@CreatedBy int
 ,@SubSectorCode nvarchar(20)
)
as
begin

	;WITH rCTE AS(
		SELECT  *
		FROM    RecipeMOGMapping 
		WHERE   RecipeCode = @RecipeCode and BaseRecipeCode is not null
    
		UNION ALL
    
		SELECT  t.*
		FROM    RecipeMOGMapping t
				INNER JOIN rCTE r
				ON t.RecipeCode = r.BaseRecipeCode and t.BaseRecipeCode is not null
	   )
	  insert into RegionRecipe(Region_ID,Name,UOM_ID,Quantity,Instructions,IsBase,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,RecipeCode,UOMCode,CostPerUOM,CostPerKG
			,TotalCost,SectorCostPerUOM,SectorCostPerKG,SectorTotalCost,Status,SubSectorCode,AllergensName)
	   	     select @Region_ID, Name, UOM_ID, Quantity, Instructions, IsBase, IsActive, @CreatedBy, GETDATE(), null,null
			 , RecipeCode, UOMCode,CostPerUOM, CostPerKG,TotalCost ,CostPerUOM,CostPerKG,TotalCost,Status,@SubSectorCode,AllergensName
			 from   Recipe 
			 where  RecipeCode In (
								  select distinct r.RecipeCode as BaseRecipeCode 
									from rCTE rm 
									left join Recipe r on r.RecipeCode = rm.RecipeCode
									where rm.RecipeCode  NOT IN (select RecipeCode from RegionRecipe where Region_ID=@Region_ID
									and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)))
								
end


Go

--USE [CookBookGoogle]
GO
/****** Object:  StoredProcedure [dbo].[procSaveRegionItemDishRecipeMapping]    Script Date: 7/13/2021 4:26:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procSaveRegionItemDishRecipeMapping] (
  @Region_ID int
 ,@DishCode  nvarchar(10)
 ,@CreatedBy int
 ,@SubSectorCode nvarchar(10)
)
as  
begin
 If not exists (select * from  RegionDishRecipeMapping where  Region_ID = @Region_ID and DishCode = @DishCode
							 and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end))
	begin
	    insert into RegionDishRecipeMapping
		select @Region_ID, Dish_ID, Recipe_ID, IsActive, @CreatedBy, GETDATE(),null,null, DishCode, RecipeCode, IsDefault,@SubSectorCode  from DishRecipeMapping where DishCode = @DishCode
	end

	IF OBJECT_ID('TempDB..#TEMPRegionRecipeList') IS NOT NULL DROP TABLE #TEMPRegionRecipeList
		SELECT  a.Region_ID, a.RecipeCode
		into    #TEMPRegionRecipeList
		FROM    (
				 select distinct rdrm.Region_ID, rdrm.RecipeCode
				 from   RegionDishRecipeMapping rdrm left join RegionRecipe rr 
				 	    on rdrm.Region_ID = rr.Region_ID and rdrm.RecipeCode = rr.RecipeCode
				 where  rdrm.Region_ID = @Region_ID and rdrm.DishCode = @DishCode 
				-- and rr.Region_ID is null  and rr.RecipeCode is null
				 and ISNULL(rdrm.SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(rdrm.SubSectorCode,'') else @SubSectorCode end)
				) as a

SELECT * FROM #TEMPRegionRecipeList
	While ((select count(1) from #TEMPRegionRecipeList) > 0)
	begin
	     DECLARE @RegionID int
		 DECLARE @RecipeCode varchar(20)
		 SELECT TOP 1 @RegionID = Region_ID, @RecipeCode = RecipeCode 
		 FROM   #TEMPRegionRecipeList
		 SELECT * FROM #TEMPRegionRecipeList
		 If not exists (select * from   RegionRecipe where  Region_ID = @RegionID and RecipeCode = @RecipeCode 
													and ISNULL(SubSectorCode,'') = ( case when @SubSectorCode is null then ISNULL(SubSectorCode,'') else @SubSectorCode end))
	     begin
		     declare @RecipeCost decimal(18,3)
		     --exec  procGetRecipeCostAtRegion @RecipeCode, @Region_ID, @RecipeCost output
	         SELECT 'INSERT'
			 insert into RegionRecipe(Region_ID,Name,UOM_ID,Quantity,Instructions,IsBase,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,RecipeCode,UOMCode,CostPerUOM,CostPerKG
			,TotalCost,SectorCostPerUOM,SectorCostPerKG,SectorTotalCost,Status,SubSectorCode,AllergensName)
	   	     select @RegionID, Name, UOM_ID, Quantity, Instructions, IsBase, IsActive, @CreatedBy, GETDATE(), null,null
			 , @RecipeCode, UOMCode,CostPerKG, CostPerKG,CostPerKG ,CostPerKG,
			   CostPerKG,CostPerKG,Status,@SubSectorCode,AllergensName
			 from   Recipe 
			 where  RecipeCode = @RecipeCode

			 exec procSaveRegionBaseRecipeData @Region_ID,@RecipeCode,@CreatedBy,@SubSectorCode

			 
			 if not exists (select * from RegionRecipeMOGMapping where Region_ID = @Region_ID 
			 and RecipeCode = @RecipeCode 
			 and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end))
			 begin
			 exec procSaveRegionBaseRecipeData @Region_ID,@RecipeCode,@CreatedBy,@SubSectorCode

			     insert into RegionRecipeMOGMapping(Region_ID,Recipe_ID,MOG_ID,UOM_ID,BaseRecipe_ID,Quantity,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,Yield,RecipeCode
					,MOGCode,BaseRecipeCode,UOMCode,CostPerUOM,CostPerKG,TotalCost,CostAsOfDate,IngredientPerc,IsMajor,SectorIngredientPerc,SectorTotalCost,SectorQuantity,
					SubSectorCode,AllergensName)
				 select  @Region_ID, Recipe_ID, MOG_ID, UOM_ID, BaseRecipe_ID, Quantity, 1,@CreatedBy, getdate(), null,null
				 , Yield,@RecipeCode, MOGCode, BaseRecipeCode, UOMCode, CostPerUOM, CostPerKG, TotalCost, 
				 CostAsOfDate,IngredientPerc,IsMajor, IngredientPerc,TotalCost,Quantity,@SubSectorCode,AllergensName
				 from    RecipeMOGMapping
				 where   RecipeCode = @RecipeCode
				 
			 end
	     end
		 else 
		 begin
			if not exists (select * from RegionRecipeMOGMapping where Region_ID = @Region_ID 
			and ISNULL(SubSectorCode,'') = ( case when @subsectorcode is null then ISNULL(SubSectorCode,'') else @subsectorcode end)
			and RecipeCode = @RecipeCode)
			 begin
			  exec procSaveRegionBaseRecipeData @Region_ID,@RecipeCode,@CreatedBy,@SubSectorCode
			     insert into RegionRecipeMOGMapping(Region_ID,Recipe_ID,MOG_ID,UOM_ID,BaseRecipe_ID,Quantity,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,Yield,RecipeCode
					,MOGCode,BaseRecipeCode,UOMCode,CostPerUOM,CostPerKG,TotalCost,CostAsOfDate,IngredientPerc,IsMajor,SectorIngredientPerc,SectorTotalCost,SectorQuantity,
					SubSectorCode,AllergensName)
				 select  @Region_ID, Recipe_ID, MOG_ID, UOM_ID, BaseRecipe_ID, Quantity, 1, @CreatedBy, getdate(), null,null, Yield,
					     @RecipeCode, MOGCode, BaseRecipeCode, UOMCode, CostPerUOM, CostPerKG, TotalCost, CostAsOfDate
						 ,IngredientPerc,IsMajor, IngredientPerc,TotalCost,Quantity,@SubSectorCode,AllergensName
				 from    RecipeMOGMapping
				 where   RecipeCode = @RecipeCode
			 end
		 end
	  Delete from #TEMPRegionRecipeList where @RegionID = Region_ID and  @RecipeCode = RecipeCode 
	end

end

Go


GO
/****** Object:  StoredProcedure [dbo].[procSaveSiteItemDishRecipeMapping]    Script Date: 7/13/2021 4:36:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procSaveSiteItemDishRecipeMapping] (
  @Site_ID int
 ,@SiteCode  nvarchar(10)
 ,@DishCode  nvarchar(10)
 ,@CreatedBy int
)
as
begin
    declare @Region_ID as int = 0
    set @Region_ID = (select RegionID from CookBookCommon.dbo.SiteMaster where SiteID= @Site_ID)

	If not exists (select * from SiteDishRecipeMapping where  Site_ID = @Site_ID and DishCode = @DishCode)
	begin
	    insert into SiteDishRecipeMapping
		select @Site_ID, @SiteCode, Dish_ID, Recipe_ID, IsActive, @CreatedBy, GETDATE(),null,null, DishCode, RecipeCode, IsDefault from DishRecipeMapping where DishCode = @DishCode
	end

	IF OBJECT_ID('TempDB..#TEMPSiteRecipeList') IS NOT NULL DROP TABLE #TEMPSiteRecipeList
		SELECT  a.Site_ID,a.SiteCode,a.RecipeCode
		into    #TEMPSiteRecipeList
		FROM    (
				 select distinct rdrm.Site_ID,rdrm.SiteCode, rdrm.RecipeCode
				 from   SiteDishRecipeMapping rdrm left join SiteRecipe rr 
				 	    on rdrm.Site_ID = rr.Site_ID and rdrm.RecipeCode = rr.RecipeCode
				 where  rdrm.Site_ID = @Site_ID and rdrm.DishCode = @DishCode and rr.Site_ID is null and rr.RecipeCode is null
				) as a


	While ((select count(1) from #TEMPSiteRecipeList) > 0)
	begin
	     DECLARE @SiteID int
	     DECLARE @Site_Code varchar(10)
		 DECLARE @RecipeCode varchar(20)
		 SELECT TOP 1 @SiteID = Site_ID,@Site_Code= SiteCode ,@RecipeCode = RecipeCode 
		 FROM   #TEMPSiteRecipeList

		 If not exists (select * from   SiteRecipe where  Site_ID = @SiteID and RecipeCode = @RecipeCode)
	     begin
		     declare @RecipeCost decimal(18,3)
		     exec procGetRecipeCostAtSite @RecipeCode, @Site_ID, @RecipeCost output
			 insert into SiteRecipe(Site_ID,SiteCode,Region_ID,Name,UOM_ID,Quantity,Instructions,IsBase,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,RecipeCode,UOMCode,CostPerUOM
			,CostPerKG,TotalCost,RegionCostPerUOM,RegionCostPerKG,RegionTotalCost,Status,AllergensName)
	   	     select @SiteID,@Site_Code,@Region_ID ,Name, UOM_ID, Quantity, Instructions, IsBase, IsActive, @CreatedBy, GETDATE(), null,null, @RecipeCode, UOMCode
			 ,@RecipeCost/10, @RecipeCost/10,@RecipeCost,@RecipeCost/10, @RecipeCost/10,@RecipeCost,Status,AllergensName
			 from   Recipe 
			 where  RecipeCode = @RecipeCode
			   exec procSaveUnitBaseRecipeData @SiteID,@Site_Code,@RecipeCode,@CreatedBy
			 if not exists (select * from SiteRecipeMOGMapping where Site_ID = @Site_ID and RecipeCode = @RecipeCode)
			  
			begin
			     insert into SiteRecipeMOGMapping(Site_ID,SiteCode,Region_ID,Recipe_ID,MOG_ID,UOM_ID,BaseRecipe_ID,Quantity,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,Yield
				,RecipeCode,MOGCode,BaseRecipeCode,UOMCode,CostPerUOM,CostPerKG,TotalCost,CostAsOfDate,IngredientPerc,IsMajor,RegionIngredientPerc,RegionTotalCost
				,RegionQuantity,ArticleNumber,AllergensName)
				 select  @Site_ID,@SiteCode,@Region_ID ,Recipe_ID, MOG_ID, UOM_ID, BaseRecipe_ID, Quantity, 1, @CreatedBy, getdate(), null,null, Yield,
					     @RecipeCode, MOGCode, BaseRecipeCode, UOMCode, CostPerUOM, CostPerKG, TotalCost, CostAsOfDate,IngredientPerc
						 ,IsMajor,  IngredientPerc,TotalCost, Quantity,null,AllergensName
				 from    RecipeMOGMapping
				 where   RecipeCode = @RecipeCode
			 end
	     end
		 else 
		 begin
			if not exists (select * from SiteRecipeMOGMapping where Site_ID = @Site_ID and RecipeCode = @RecipeCode)
			 begin
			 exec procSaveUnitBaseRecipeData @SiteID,@Site_Code,@RecipeCode,@CreatedBy
			     insert into SiteRecipeMOGMapping(Site_ID,SiteCode,Region_ID,Recipe_ID,MOG_ID,UOM_ID,BaseRecipe_ID,Quantity,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,Yield
				,RecipeCode,MOGCode,BaseRecipeCode,UOMCode,CostPerUOM,CostPerKG,TotalCost,CostAsOfDate,IngredientPerc,IsMajor,RegionIngredientPerc,RegionTotalCost
				,RegionQuantity,ArticleNumber,AllergensName)
				 select  @Site_ID,@SiteCode,@Region_ID ,Recipe_ID, MOG_ID, UOM_ID, BaseRecipe_ID, Quantity, 1, @CreatedBy, getdate(), null,null, Yield,
					     @RecipeCode, MOGCode, BaseRecipeCode, UOMCode, CostPerUOM, CostPerKG, TotalCost, CostAsOfDate,IngredientPerc,IsMajor, IngredientPerc ,TotalCost,
						 Quantity,null,AllergensName
				 from    RecipeMOGMapping
				 where   RecipeCode = @RecipeCode
			 end
		 end
	  Delete from #TEMPSiteRecipeList where @SiteID = Site_ID and  @RecipeCode = RecipeCode 
	end

end


Go

--USE [CookBookGoogle]
GO
/****** Object:  StoredProcedure [dbo].[procSaveUnitBaseRecipeData]    Script Date: 7/13/2021 4:42:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procSaveUnitBaseRecipeData] (
  @SiteID int,
  @SiteCode nvarchar(10)
 ,@RecipeCode  nvarchar(10)
 ,@CreatedBy int
)
as
begin

	declare @Region_ID as int
	select  @Region_ID = RegionID from CookBookCommon.dbo.SiteMaster where SiteCode = @SiteCode

	 ;WITH rCTE AS(
		SELECT  *
		FROM    RegionRecipeMOGMapping 
		WHERE   RecipeCode = @RecipeCode and BaseRecipeCode is not null and Region_ID = @Region_ID
    
		UNION ALL
    
		SELECT  t.*
		FROM    RegionRecipeMOGMapping t
				INNER JOIN rCTE r
				ON t.RecipeCode = r.BaseRecipeCode and t.BaseRecipeCode is not null and t.Region_ID = @Region_ID
	   )
	  insert into SiteRecipe(Site_ID,SiteCode,Region_ID,Name,UOM_ID,Quantity,Instructions,IsBase,IsActive,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,RecipeCode,UOMCode,CostPerUOM
			,CostPerKG,TotalCost,RegionCostPerUOM,RegionCostPerKG,RegionTotalCost,Status,AllergensName)
	   	     select @SiteID, @SiteCode,Region_ID, Name, UOM_ID, Quantity, Instructions, IsBase, IsActive, @CreatedBy, GETDATE(), null,null
			 , RecipeCode, UOMCode,CostPerUOM, CostPerKG,TotalCost ,CostPerUOM, CostPerKG,TotalCost,Status,AllergensName
			 from   RegionRecipe 
			 where  RecipeCode In (
								  select distinct r.RecipeCode as BaseRecipeCode 
									from rCTE rm 
									left join RegionRecipe r on r.RecipeCode = rm.RecipeCode and  rm.Region_ID = @Region_ID
									where rm.RecipeCode  NOT IN (select RecipeCode from SiteRecipe where SiteCode=@SiteCode)
									)
					and Region_ID = @Region_ID
end


Go


GO
/****** Object:  StoredProcedure [dbo].[procUpdateCost]    Script Date: 7/13/2021 4:57:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[procUpdateAllergens] @SectorCode nvarchar(10)
as      
Begin      
	begin try 
		 exec procUpdateAllergensAtSector @SectorCode
     end try

     begin catch
          select ERROR_PROCEDURE() AS ErrorProcedure
     end catch   
	 
	  begin try 
		 exec procUpdateAllergensAtRegion @SectorCode
     end try

     begin catch
          select ERROR_PROCEDURE() AS ErrorProcedure
     end catch  

	  begin try 
		 exec procUpdateAllergensAtSite @SectorCode
     end try

     begin catch
          select ERROR_PROCEDURE() AS ErrorProcedure
     end catch 

End

Go



GO
/****** Object:  StoredProcedure [dbo].[procUpdateAllergensAtSite]    Script Date: 7/13/2021 6:49:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procUpdateAllergensAtSite] @SectorCode as varchar(20)
as begin

   
-- Update RecipeMOGMapping Table Allergsns

update mog
	set		mog.AllergensName = uniqueApl.Allergens
			from SiteRecipeMOGMapping mog
					left join(
							select SiteCode, RecipeCode,MOGCode, STRING_AGG(al.Name, ',') WITHIN GROUP (ORDER BY al.Name) AS Allergens
							from SiteRecipeMOGMapping srm
							left join CookBookCommon..AllergenAPLMapping aam on aam.ArticleNumber = srm.ArticleNumber
							left join CookBookCommon..Allergen al on al.AllergenCode = aam.AllergenCode
							where MOGCode is not null and aam.AllergenCode is not null 
							group by SiteCode, RecipeCode,MOGCode
					     ) as uniqueApl 
					 on uniqueApl.MOGCode = MOG.MOGCode 
			where uniqueApl.SiteCode= mog.SiteCode
							

-- update Recipe Allergens --


  
  IF OBJECT_ID('TempDB..#TEMPSiteRecipe') IS NOT NULL DROP TABLE #TEMPSiteRecipe

 ;WITH rCTE AS(
    SELECT  SiteCode ,BaseRecipeCode,MOGCode, cast(Quantity as decimal(18,3)) as MOGQty,RecipeCode ,RecipeCode as FinalRecipeCode
	FROM    SiteRecipeMOGMapping where IsActive = 1 
	
	UNION ALL
    
	SELECT t.SiteCode, t.BaseRecipeCode,t.MOGCode, cast((cast(r.MOGQty as decimal(18,3)) * cast(t.Quantity as decimal(18,3))/10) as decimal(18,3)) as MOGQty
	,t.RecipeCode ,r.FinalRecipeCode
    FROM    SiteRecipeMOGMapping t
            INNER JOIN rCTE r
            ON t.RecipeCode = r.BaseRecipeCode and t.SiteCode= r.SiteCode
	where t.IsActive = 1
   ), 
   rCTE2 AS(
      select MOGCode,FinalRecipeCode,RecipeCode,BaseRecipeCode,sum(rCTE.MOGQty) as MOGQty, SiteCode
	 
	  from   rCTE
	  --where  MOGCode is not null
	 group by  MOGCode,FinalRecipeCode,RecipeCode,BaseRecipeCode,SiteCode
   )

    SELECT  FinalRecipeCode,RecipeCode,BaseRecipeCode,MOGCode,MOGQty, SiteCode
	into    #TEMPSiteRecipe
	FROM    rCTE2


	select * from #TEMPSiteRecipe

	update r
		 set  r.AllergensName   =  RecipeAllergen.NewField
    from SiteRecipe r
	Join (SELECT 
	 SiteCode
	,FinalRecipeCode
	,STRING_AGG(x.AllergensName, ',') WITHIN GROUP (ORDER BY x.AllergensName) AS 
	NewField
	from (
		SELECT  distinct uniqueApl.AllergensName, FinalRecipeCode, uniqueApl.SiteCode
   		 From   #TEMPSiteRecipe trs  
		   left join(
						select SiteCode, RecipeCode,MOGCode, STRING_AGG(al.Name, ',') WITHIN GROUP (ORDER BY al.Name) AS AllergensName
							from SiteRecipeMOGMapping srm
							left join CookBookCommon..AllergenAPLMapping aam on aam.ArticleNumber = srm.ArticleNumber
							left join CookBookCommon..Allergen al on al.AllergenCode = aam.AllergenCode
							where MOGCode is not null and aam.AllergenCode is not null 
							group by SiteCode, RecipeCode,MOGCode
										 ) as uniqueApl
						      on uniqueApl.MOGCode = trs.MOGCode and uniqueApl.SiteCode = trs.SiteCode
				 where uniqueApl.AllergensName is not null
	) x
	GROUP BY SiteCode ,FinalRecipeCode
	)RecipeAllergen
  on r.RecipeCode = RecipeAllergen.FinalRecipeCode and r.SiteCode = RecipeAllergen.SiteCode

  ---- Update Dishes Allergens
  
update d
		 set   AllergensName = r.AllergensName
		 From SiteDish d 
		 inner join SiteDishRecipeMapping drm on drm.DishCode = d.DishCode and drm.IsDefault = 1 and drm.SiteCode = d.SiteCode
		 inner join SiteRecipe r  on r.RecipeCode = drm.RecipeCode  
		 where  r.SiteCode = drm.SiteCode

 ---- Update Items Allergens


update it 
set AllergensName = al.Allergens
from SiteItemInheritanceMapping it
JOIN (select i.ItemCode, i.SiteCode,
   dbo.DistinctList(STRING_AGG(cast(d.AllergensName as NVARCHAR(MAX)), ',') WITHIN GROUP (ORDER BY d.AllergensName),',') AS Allergens
from SiteItemInheritanceMapping i
left join SiteItemDishMapping idm on idm.ItemCode = i.ItemCode and idm.SiteCode = i.SiteCode
left join  SiteDish  d on d.DishCode = idm.DishCode  
where d.DishCode is not null and d.AllergensName is not null and d.SiteCode = idm.SiteCode 
group by i.ItemCode, i.SiteCode
) al
on al.ItemCode = it.ItemCode
where it.SiteCode = al.SiteCode

end


Go

GO
/****** Object:  StoredProcedure [dbo].[procUpdateAllergensAtSite]    Script Date: 7/13/2021 6:49:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[procUpdateAllergensAtSiteByRecipeCode] @RecipeCode as varchar(20)
as begin

   
-- Update RecipeMOGMapping Table Allergsns

update mog
	set		mog.AllergensName = uniqueApl.Allergens
			from SiteRecipeMOGMapping mog
					left join(
							select SiteCode, RecipeCode,MOGCode, STRING_AGG(al.Name, ',') WITHIN GROUP (ORDER BY al.Name) AS Allergens
							from SiteRecipeMOGMapping srm
							left join CookBookCommon..AllergenAPLMapping aam on aam.ArticleNumber = srm.ArticleNumber
							left join CookBookCommon..Allergen al on al.AllergenCode = aam.AllergenCode
							where MOGCode is not null and aam.AllergenCode is not null and srm.RecipeCode = @RecipeCode
							group by SiteCode, RecipeCode,MOGCode
					     ) as uniqueApl 
					 on uniqueApl.MOGCode = MOG.MOGCode 
			where uniqueApl.SiteCode= mog.SiteCode and mog.RecipeCode = @RecipeCode
							

-- update Recipe Allergens --


  
  IF OBJECT_ID('TempDB..#TEMPSiteRecipe') IS NOT NULL DROP TABLE #TEMPSiteRecipe

 ;WITH rCTE AS(
    SELECT  SiteCode ,BaseRecipeCode,MOGCode, cast(Quantity as decimal(18,3)) as MOGQty,RecipeCode ,RecipeCode as FinalRecipeCode
	FROM    SiteRecipeMOGMapping where RecipeCode = @RecipeCode and IsActive = 1 and ArticleNumber is not null
	
	UNION ALL
    
	SELECT t.SiteCode, t.BaseRecipeCode,t.MOGCode, cast((cast(r.MOGQty as decimal(18,3)) * cast(t.Quantity as decimal(18,3))/10) as decimal(18,3)) as MOGQty
	,t.RecipeCode ,r.FinalRecipeCode
    FROM    SiteRecipeMOGMapping t
            INNER JOIN rCTE r
            ON t.RecipeCode = r.BaseRecipeCode and t.SiteCode= r.SiteCode and t.ArticleNumber is not null
	where t.IsActive = 1
   ), 
   rCTE2 AS(
      select MOGCode,FinalRecipeCode,RecipeCode,BaseRecipeCode,sum(rCTE.MOGQty) as MOGQty, SiteCode
	 
	  from   rCTE
	  --where  MOGCode is not null
	 group by  MOGCode,FinalRecipeCode,RecipeCode,BaseRecipeCode,SiteCode
   )

    SELECT  FinalRecipeCode,RecipeCode,BaseRecipeCode,MOGCode,MOGQty, SiteCode
	into    #TEMPSiteRecipe
	FROM    rCTE2


	--select * from #TEMPSiteRecipe

	update r
		 set  r.AllergensName   =  RecipeAllergen.NewField
    from SiteRecipe r
	Join (SELECT 
	 SiteCode
	,FinalRecipeCode
	,STRING_AGG(x.AllergensName, ',') WITHIN GROUP (ORDER BY x.AllergensName) AS 
	NewField
	from (
		SELECT  distinct uniqueApl.AllergensName, FinalRecipeCode, uniqueApl.SiteCode
   		 From   #TEMPSiteRecipe trs  
		   left join(
						select SiteCode, RecipeCode,MOGCode, STRING_AGG(al.Name, ',') WITHIN GROUP (ORDER BY al.Name) AS AllergensName
							from SiteRecipeMOGMapping srm
							left join CookBookCommon..AllergenAPLMapping aam on aam.ArticleNumber = srm.ArticleNumber
							left join CookBookCommon..Allergen al on al.AllergenCode = aam.AllergenCode
							where MOGCode is not null and aam.AllergenCode is not null 
							group by SiteCode, RecipeCode, MOGCode
										 ) as uniqueApl
						      on uniqueApl.MOGCode = trs.MOGCode and uniqueApl.SiteCode = trs.SiteCode
				 where uniqueApl.AllergensName is not null
	) x
	GROUP BY SiteCode ,FinalRecipeCode
	)RecipeAllergen
  on r.RecipeCode = RecipeAllergen.FinalRecipeCode and r.SiteCode = RecipeAllergen.SiteCode

  ---- Update Dishes Allergens
  
update d
		 set   AllergensName = r.AllergensName
		 From SiteDish d 
		 inner join SiteDishRecipeMapping drm on drm.DishCode = d.DishCode and drm.IsDefault = 1 and drm.SiteCode = d.SiteCode
		 inner join SiteRecipe r  on r.RecipeCode = drm.RecipeCode  
		 where  r.SiteCode = drm.SiteCode and drm.RecipeCode = @RecipeCode

 ---- Update Items Allergens


update sit 
		set AllergensName = al.Allergens
		from SiteItemInheritanceMapping sit
		JOIN (select i.ItemCode, i.SiteCode,
		   dbo.DistinctList(STRING_AGG(cast(d.AllergensName as NVARCHAR(MAX)), ',') WITHIN GROUP (ORDER BY d.AllergensName),',') AS Allergens
		from SiteItemInheritanceMapping i
		inner join SiteItemDishMapping idm on idm.ItemCode = i.ItemCode and idm.SiteCode = i.SiteCode
		inner join SiteDishRecipeMapping srm on srm.DishCode = idm.DishCode and idm.SiteCode = srm.SiteCode
		inner join  SiteDish  d on d.DishCode = idm.DishCode  
		where d.DishCode is not null and d.AllergensName is not null and d.SiteCode = idm.SiteCode and srm.RecipeCode = @RecipeCode
		group by i.ItemCode, i.SiteCode
		) al
		on al.ItemCode = sit.ItemCode
		where sit.SiteCode = al.SiteCode

end


Go


GO
/****** Object:  StoredProcedure [dbo].[procUpdateAllergensAtSiteByRecipeCode]    Script Date: 7/14/2021 12:15:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procUpdateAllergensAtSiteByRecipeCode] @RecipeCode as varchar(20), @SiteCode as varchar(20)
as begin

   
-- Update RecipeMOGMapping Table Allergsns

update mog
	set		mog.AllergensName = uniqueApl.Allergens
			from SiteRecipeMOGMapping mog
					left join(
							select SiteCode, RecipeCode,MOGCode, STRING_AGG(al.Name, ',') WITHIN GROUP (ORDER BY al.Name) AS Allergens
							from SiteRecipeMOGMapping srm
							left join CookBookCommon..AllergenAPLMapping aam on aam.ArticleNumber = srm.ArticleNumber
							left join CookBookCommon..Allergen al on al.AllergenCode = aam.AllergenCode
							where MOGCode is not null and aam.AllergenCode is not null and srm.RecipeCode = @RecipeCode and SiteCode = @SiteCode
							group by SiteCode, RecipeCode,MOGCode
					     ) as uniqueApl 
					 on uniqueApl.MOGCode = MOG.MOGCode 
			where uniqueApl.SiteCode= mog.SiteCode and mog.RecipeCode = @RecipeCode and mog.SiteCode= @SiteCode
							

-- update Recipe Allergens --


  
  IF OBJECT_ID('TempDB..#TEMPSiteRecipe') IS NOT NULL DROP TABLE #TEMPSiteRecipe

 ;WITH rCTE AS(
    SELECT  SiteCode ,BaseRecipeCode,MOGCode, cast(Quantity as decimal(18,3)) as MOGQty,RecipeCode ,RecipeCode as FinalRecipeCode
	FROM    SiteRecipeMOGMapping where RecipeCode = @RecipeCode and SiteCode = @SiteCode and IsActive = 1 and ArticleNumber is not null
	
	UNION ALL
    
	SELECT t.SiteCode, t.BaseRecipeCode,t.MOGCode, cast((cast(r.MOGQty as decimal(18,3)) * cast(t.Quantity as decimal(18,3))/10) as decimal(18,3)) as MOGQty
	,t.RecipeCode ,r.FinalRecipeCode
    FROM    SiteRecipeMOGMapping t
            INNER JOIN rCTE r
            ON t.RecipeCode = r.BaseRecipeCode and t.SiteCode= r.SiteCode and t.ArticleNumber is not null
	where t.IsActive = 1
   ), 
   rCTE2 AS(
      select MOGCode,FinalRecipeCode,RecipeCode,BaseRecipeCode,sum(rCTE.MOGQty) as MOGQty, SiteCode
	 
	  from   rCTE
	  --where  MOGCode is not null
	 group by  MOGCode,FinalRecipeCode,RecipeCode,BaseRecipeCode,SiteCode
   )

    SELECT  FinalRecipeCode,RecipeCode,BaseRecipeCode,MOGCode,MOGQty, SiteCode
	into    #TEMPSiteRecipe
	FROM    rCTE2


	--select * from #TEMPSiteRecipe

	update r
		 set  r.AllergensName   =  RecipeAllergen.NewField
    from SiteRecipe r
	Join (SELECT 
	 SiteCode
	,FinalRecipeCode
	,STRING_AGG(x.AllergensName, ',') WITHIN GROUP (ORDER BY x.AllergensName) AS 
	NewField
	from (
		SELECT  distinct uniqueApl.AllergensName, FinalRecipeCode, uniqueApl.SiteCode
   		 From   #TEMPSiteRecipe trs  
		   left join(
						select SiteCode, RecipeCode,MOGCode, STRING_AGG(al.Name, ',') WITHIN GROUP (ORDER BY al.Name) AS AllergensName
							from SiteRecipeMOGMapping srm
							left join CookBookCommon..AllergenAPLMapping aam on aam.ArticleNumber = srm.ArticleNumber
							left join CookBookCommon..Allergen al on al.AllergenCode = aam.AllergenCode
							where MOGCode is not null and aam.AllergenCode is not null
							group by SiteCode, RecipeCode, MOGCode
										 ) as uniqueApl
						      on uniqueApl.MOGCode = trs.MOGCode and uniqueApl.SiteCode = trs.SiteCode
				 where uniqueApl.AllergensName is not null
	) x
	GROUP BY SiteCode ,FinalRecipeCode
	)RecipeAllergen
  on r.RecipeCode = RecipeAllergen.FinalRecipeCode and r.SiteCode = RecipeAllergen.SiteCode and r.SiteCode= @SiteCode

  ---- Update Dishes Allergens
  
update d
		 set   AllergensName = r.AllergensName
		 From SiteDish d 
		 inner join SiteDishRecipeMapping drm on drm.DishCode = d.DishCode and drm.IsDefault = 1 and drm.SiteCode = d.SiteCode
		 inner join SiteRecipe r  on r.RecipeCode = drm.RecipeCode  
		 where  r.SiteCode = drm.SiteCode and drm.RecipeCode = @RecipeCode and r.SiteCode = @SiteCode

 ---- Update Items Allergens


update sit 
		set AllergensName = al.Allergens
		from SiteItemInheritanceMapping sit
		JOIN (select i.ItemCode, i.SiteCode,
		   dbo.DistinctList(STRING_AGG(cast(d.AllergensName as NVARCHAR(MAX)), ',') WITHIN GROUP (ORDER BY d.AllergensName),',') AS Allergens
		from SiteItemInheritanceMapping i
		inner join SiteItemDishMapping idm on idm.ItemCode = i.ItemCode and idm.SiteCode = i.SiteCode
		inner join SiteDishRecipeMapping srm on srm.DishCode = idm.DishCode and idm.SiteCode = srm.SiteCode
		inner join  SiteDish  d on d.DishCode = idm.DishCode  
		where d.DishCode is not null and d.AllergensName is not null and d.SiteCode = idm.SiteCode and srm.RecipeCode = @RecipeCode
		group by i.ItemCode, i.SiteCode
		) al
		on al.ItemCode = sit.ItemCode
		where sit.SiteCode = al.SiteCode and sit.SiteCode= @SiteCode

end


Go

--USE [CookBookCore]
GO
/****** Object:  StoredProcedure [dbo].[procGetItemList]    Script Date: 7/20/2021 4:32:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[procGetItemListExport]  
as  
Begin  
select 
	i.ItemCode,
	i.Name as ItemName,
	dtc.Name as DietCategory,
	vs.Name as VisualCateogry,
	fp.Name as FoodProgram ,
	dcat.Name as DishCategory,
	d.Name as DishName, 
	dt.Name as DishType, 
	i.HSNCode, 
	i.CGST, 
	i.SGST, 
	i.CESS 
from item i 
left join ItemDishCategoryMapping dc on dc.ItemCode = i.ItemCode
left join DishCategory dcat on dcat.DishCategoryCode = dc.DishCategoryCode
left join ItemDishMapping idish on idish.ItemCode = i.ItemCode
left join Dish d on d.DishCode = idish.DishCode
left join DishType dt on dt.DishTypeCode = d.DishTypeCode
left join CookBookCommon..VisualCategory vs on vs.VisualCategoryCode = i.VisualCategoryCode
left join FoodProgram fp on fp.FoodProgramCode = i.FoodProgramCode
left join CookBookCommon..DietCategory dtc on dtc.DietCategoryCode = i.DietCategoryCode

End
 
Go


-------- 10-08-2021--------------------------------------------

--USE [CookBookCore]
GO
/****** Object:  StoredProcedure [dbo].[procGetSiteWiseItemConfiguration]    Script Date: 8/10/2021 9:34:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[procGetSiteItemInheritanceMappingData] @SiteCode nvarchar(20) = null, @ItemCode nvarchar(20) = null
as
begin
select
	  sim.ID
	  Site_ID,
	  sm.SiteCode,
	  sm.SiteName, 
	  sim.Item_ID,
	  sim.ItemCode,
	  sim.ItemPrice,
	  sim.Status,
	  sim.CreatedBy,
	  sim.CreatedOn,
	  sim.ModifiedBy,
	  sim.ModifiedOn,
	  sim.PriceStatus,
	  sim.MaxMealCount,
	  sim.AllergensName,
	  (STUFF((SELECT ', ' + dp.Name
				   FROM SiteItemDayPartMapping idm WITH (NOLOCK)
				   left join DayPart dp WITH (NOLOCK) on dp.DayPartCode = idm.DayPartCode and dp.IsActive = 1
				   WHERE idm.ItemCode = sim.ItemCode and idm.SiteCode = sim.SiteCode  and idm.IsActive = 1
				  FOR XML PATH('')), 1, 2, '')) as DayPartNames
from   SiteItemInheritanceMapping sim WITH (NOLOCK)
       left join CookBookCommon..SiteMaster sm WITH (NOLOCK)
	   on sim.SiteCode = sm.SiteCode
where  sim.IsActive = 1 and
       (@SiteCode is null or @SiteCode = @SiteCode) and
	   (@ItemCode is null or @ItemCode = @ItemCode)

end
