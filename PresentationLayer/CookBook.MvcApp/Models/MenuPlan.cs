﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CookBook.MvcApp.Models
{
    public class MenuPlan
    {
        public int id { get; set; }
        public List<KeyValueObject> selectedmealTimedata { get; set; }
        public List<KeyValueObject> selectedbusinessTypedata { get; set; }
        public SiteData selectedsiteDatat { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public List<PlannedDays> weekdays { get; set; }
    }


    public class PlannedDays
    {
        public List<DayMealTime> DayMealTime { get; set; }
        public DateTime weekDay { get; set; }
        public string uniqid { get; set; }
    }

    public class DayMealTime
    {
        public string text { get; set; }
        public int value { get; set; }
        public List<Service> selectedServices { get; set; }
    }
    public class DishItem
    {
        public string DishName { get; set; }
        public int DishId { get; set; }
        public int UomId { get; set; }
    }
    public class SiteData
    {
        public int SiteId { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
    }

    public class Service 
    {
        public string ServiceTypeName { get; set; }
        public string ServiceTypeId { get; set; }
        public List<DishItem> dishItems { get; set; }
    }
}