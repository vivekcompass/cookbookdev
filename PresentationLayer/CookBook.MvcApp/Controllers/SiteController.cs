﻿using ClosedXML.Excel;
using CookBook.Aspects.Factory;
using CookBook.Data;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CookBook.Data.MasterData;

namespace CookBook.MvcApp.Controllers
{
    public class SiteController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        public ActionResult GetSiteDataList(string SiteCode)
        {
            var requestData = GetRequestData();
            requestData.SiteCode = SiteCode;
            var siteList = ServiceClient.GetSiteMasterDataList(requestData);
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        //Krish        
        [CustAuthFilter]
        public ActionResult GetSiteByRegionDataList(int regionId)
        {
            var siteList = ServiceClient.GetSiteByRegionDataList(regionId, GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        //Krish        
        [CustAuthFilter]
        public ActionResult GetSiteByUserDataList()
        {
            var siteList = ServiceClient.GetSiteDetailsByUserIdDataList(GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteDataListByUserId()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            if (userData.IsSectorUser)
            {
                var siteList = ServiceClient.GetSiteMasterDataList(GetRequestData());
                var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            else
            {
                var siteList = ServiceClient.GetSiteDetailsByUserIdDataList(GetRequestData());
                var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveSiteData(SiteMasterData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            model.ModifiedBy = userData.UserId.ToString();
            model.ModifiedDate = DateTime.Now;

            var result = ServiceClient.SaveSiteData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult ChangeStatus(SiteMasterData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.ModifiedBy = userData.UserId.ToString();
            model.ModifiedDate = DateTime.Now;
            var result = ServiceClient.SaveSiteData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveSiteDataList(SiteMasterData[] model)
        {
            var result = ServiceClient.SaveSiteDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetCPUDataList()
        {
            var siteList = ServiceClient.GetCPUDataList(GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetDKDataList()
        {
            var siteList = ServiceClient.GetDKDataList(GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
    }
}