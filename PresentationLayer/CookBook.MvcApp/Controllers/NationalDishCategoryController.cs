﻿using System;
using System.Web.Mvc;
using CookBook.Data.MasterData;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;

namespace CookBook.MvcApp.Controllers
{
    public class NationalDishCategoryController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();


        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult NationalGetDishCategoryDataList()
        {
            var dishCategoryList = ServiceClient.NationalGetDishCategoryDataList(GetRequestData());
            var jsonResult = Json(dishCategoryList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }


        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult NationalDish()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult NationalGetDisDataList()
        {
            var dishCategoryList = ServiceClient.NationalGetDishDataList(GetRequestData());
            var jsonResult = Json(dishCategoryList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
    }
}