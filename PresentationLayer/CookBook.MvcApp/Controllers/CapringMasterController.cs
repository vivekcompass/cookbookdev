﻿using CookBook.Data.MasterData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using Microsoft.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp.Controllers
{
    public class CapringMasterController : BaseController
    {
        // GET: CapringMaster
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetSectorMasterData()
        {
           
                var regionList = ServiceClient.GetSectorListInfo(GetRequestData());
                var jsonResult = Json(regionList, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            

        }
        [CustAuthFilter]
        public ActionResult GetLevelMasterData()
        {

            var regionList = ServiceClient.GetLevelMasterData(GetRequestData());
            var jsonResult = Json(regionList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;


        }

        [CustAuthFilter]
        public ActionResult GetFunctionMasterData()
        {

            var regionList = ServiceClient.GetFunctionMasterData(GetRequestData());
            var jsonResult = Json(regionList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;


        }
        [CustAuthFilter]
        public ActionResult GetPermissionMasterData()
        {

            var regionList = ServiceClient.GetPermissionMasterData(GetRequestData());
            var jsonResult = Json(regionList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;


        }


        [CustAuthFilter]
        public ActionResult SaveCapringInfo(CapringMastre model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.SaveCapringInfo(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }
        [CustAuthFilter]
        public ActionResult UpdateCapringInfo(CapringMastre model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.UpdateCapringInfo(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        public ActionResult UpdateCapringDescriptionInfo(CapringMastre model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.UpdateCapringDescriptionInfo(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        public ActionResult UpdateCapringInactiveActive(CapringMastre model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.UpdateCapringInactiveActive(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        public ActionResult GetCapringMasterList()
        {
            var mogList = ServiceClient.GetCapringMasterList(GetRequestData());
            return Json(mogList, JsonRequestBehavior.AllowGet);
        }

    }
}