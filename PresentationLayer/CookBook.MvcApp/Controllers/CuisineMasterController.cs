﻿using CookBook.Data.MasterData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp.Controllers
{
    public class CuisineMasterController : BaseController
    {
        // GET: CuisineMaster
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult CuisineMaster()
        {
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult CuisineMasterList()
        {
            var regionRecipeList = ServiceClient.GetCuisineDataList(GetRequestData());
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
           
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveCuisine(CuisineMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var  regionRecipeList = ServiceClient.SaveCuisine(GetRequestData(),model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
           
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
            
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult UpdateCuisine(CuisineMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.UpdateCuisine(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }
    }
}