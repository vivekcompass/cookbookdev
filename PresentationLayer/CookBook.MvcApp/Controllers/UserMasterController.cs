﻿using System;
using System.Web.Mvc;
using CookBook.Data.MasterData;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;

namespace CookBook.MvcApp.Controllers
{
    public class UserMasterController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
        // GET: UserMaster
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return PartialView(GetRequestData());
        }
       // [CustAuthFilter]
        //public ActionResult SaveAddNewUser(AddNewUser model)
        //{
        //    var result = ServiceClient.SaveNewUser(model, GetRequestData());
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

    }
}