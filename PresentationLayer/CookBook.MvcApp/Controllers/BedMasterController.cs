﻿using ClosedXML.Excel;
using CookBook.Data.MasterData;
using CookBook.Encryption.EncryptImpl;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp.Controllers
{
    public class BedMasterController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            // return View();
            return PartialView();
        }
        [CustAuthFilter]
        public ActionResult GetBedMasterDataList()
        {
            var Beddata = ServiceClient.GetBedMaster(GetRequestData());
            CompassEncryption enc = new CompassEncryption();
            foreach (var item in Beddata)
            {
                var decPId = enc.DecryptData(item.BedNumber, ConfigurationManager.AppSettings["key"]);
                item.BedNumber = decPId;

            }
            return Json(Beddata, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult DownloadExcTemplate()
        {
            //var mogList = ServiceClient.GetNOTMappedMOGDataList(GetRequestData());
            DataTable dt = new DataTable();
            dt.Columns.Add("BedNumber");
            dt.Columns.Add("Occupied");
            //dt.Columns.Add("Alias");
            //dt.Columns.Add("ArticleNumber");
            //foreach (MOGNotMapped p in mogList)
            //{
            //    dt.Rows.Add(p.MOGCode, p.Name, p.Alias, p.ArticalNumber);
            //}
            using (XLWorkbook wb = new XLWorkbook())
            {
                dt.TableName = "BedMasterTemplate.xlsx";
                wb.Worksheets.Add(dt);

                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "BedMasterUploadTemplate.xlsx");
                }
            }
        }
        [CustAuthFilter]
        public ActionResult GetBedMasterHISTORY()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();

            var regionRecipeList = ServiceClient.GetBedMasterHISTORY(GetRequestData(), username);
            CompassEncryption enc = new CompassEncryption();
            foreach (var item in regionRecipeList)
            {
                var decPId = enc.DecryptData(item.BedNumber, ConfigurationManager.AppSettings["key"]);
                item.BedNumber = decPId;

            }
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;

            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetBedMasterHISTORYDetails()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();
            //  DETAILS
            var regionRecipeList_det = ServiceClient.GetBedMasterHISTORYDet(GetRequestData(), username);
            var jsonResult = Json(regionRecipeList_det, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }
        [CustAuthFilter]
        public ActionResult GetshowGetALLBMHISTORYBATCHWISE(string BATCHNUMBER)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();
            //  DETAILS
            var regionRecipeList_det = ServiceClient.GetshowGetALLBedMasterHISTORYBATCHWISE(GetRequestData(),  BATCHNUMBER);
            CompassEncryption enc = new CompassEncryption();
            foreach (var item in regionRecipeList_det)
            {
                var decPId = enc.DecryptData(item.BedNumber, ConfigurationManager.AppSettings["key"]);
                item.BedNumber = decPId;

            }
            var jsonResult = Json(regionRecipeList_det, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }
        [CustAuthFilter]
        public ActionResult GetALLHISTORYDetailsBedMaster()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();

            var regionRecipeList = ServiceClient.GetALLHISTORYDetailsBedMaster(GetRequestData());
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;

            return jsonResult;
        }
        [CustAuthFilter]
        [HttpPost]
        
        public string UploadExcelsheetBedMaster()
        {
            string Res = string.Empty;
            DataTable dt = new DataTable();
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                // List<ProductModel> _lstProductMaster = new List<ProductModel>();
                string filePath = string.Empty;
                if (Request.Files != null)
                {
                    string extension = Path.GetExtension(file.FileName);

                    string path = Path.Combine(Server.MapPath("~/Content/Upload/"), Path.GetFileName(file.FileName));
                    //Saving the file
                    file.SaveAs(path);
                    CompassEncryption enc = new CompassEncryption();
                    var Beddata = ServiceClient.GetBedMaster(GetRequestData());
                    if (extension.ToLower().Contains("json"))
                    {
                        using (StreamReader r = new StreamReader(path))
                        {
                            string json = r.ReadToEnd();
                            List<BedMasterData> items = JsonConvert.DeserializeObject<List<BedMasterData>>(json);
                            //var xml = XDocument.Load(JsonReaderWriterFactory.CreateJsonReader(Encoding.ASCII.GetBytes(json), new XmlDictionaryReaderQuotas()));

                            dt = ToDataTable(items, enc, Beddata);
                        }

                    }
                    else if (extension.ToLower().Contains("csv"))
                    {

                        dt = ConvertCSVtoDataTable(path, enc, Beddata);
                    }
                    else
                    {
                        using (XLWorkbook workbook = new XLWorkbook(path))
                        {
                            IXLWorksheet worksheet = workbook.Worksheet(1);
                            bool FirstRow = true;
                            //Range for reading the cells based on the last cell used.
                            string readRange = "1:1";

                            var count = Beddata.Count() > 0 ? Beddata.Max(d => d.ID) : 1;
                            foreach (IXLRow row in worksheet.RowsUsed())
                            {

                                if (FirstRow)
                                {
                                    //dt.Columns.Add("BedCode");
                                    readRange = string.Format("{0}:{1}", 1, row.LastCellUsed().Address.ColumnNumber);
                                    foreach (IXLCell cell in row.Cells(readRange))
                                    {
                                        dt.Columns.Add(cell.Value.ToString());
                                    }
                                    FirstRow = false;
                                }
                                else
                                {

                                    dt.Rows.Add();
                                    int cellIndex = 0;
                                    
                                    foreach (IXLCell cell in row.Cells(readRange))
                                    {
                                        var value = cell.Value.ToString();
                                        if (cellIndex == 0)
                                            value = enc.EncryptData(cell.Value.ToString(), ConfigurationManager.AppSettings["key"]);
                                        //if (cellIndex == 1)
                                        //    value = enc.EncryptData(cell.Value.ToString(), ConfigurationManager.AppSettings["key"]);
                                        dt.Rows[dt.Rows.Count - 1][cellIndex] = value;
                                        cellIndex++;
                                    }
                                }

                            }
                            dt.Columns.Add("BedCode");
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                var chkExist = Beddata.FirstOrDefault(a => a.BedNumber == dt.Rows[i][0].ToString());
                                if (chkExist != null)
                                    dt.Rows[i][2] = chkExist.BedCode;
                                else
                                {
                                    var ourRef = "BDC-" + count.ToString().PadLeft(5, '0');
                                    dt.Rows[i][2] = ourRef;
                                    count++;
                                }                                
                            }
                        }
                    }
                    dt.TableName = "BedMaster";
                    MemoryStream str = new MemoryStream();
                    dt.WriteXml(str, true);
                    str.Seek(0, SeekOrigin.Begin);
                    StreamReader sr = new StreamReader(str);
                    string xmlstr;
                    xmlstr = sr.ReadToEnd();
                    if (dt.Rows.Count > 0)
                    {
                        string UserName = string.Empty;
                        var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
                        if (UserInstance.IsAuthenticated)
                        {
                            UserName = UserInstance.FindFirst("preferred_username").Value;
                        }
                        var res = ServiceClient.GetUserData(UserName, null);
                        string username = res.UserId.ToString();
                        Res = ServiceClient.AddUpdateBedMasterList(GetRequestData(), xmlstr, username);
                    }
                    else
                        Res = "The uploaded file is empty";
                }


            }
            return Res;

        }
        public static DataTable ToDataTable<T>(List<T> items, CompassEncryption enc, BedMasterData[] beddata)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            var count = beddata.Count() > 0 ? beddata.Max(d => d.ID) : 1;
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows

                    values[i] = Props[i].GetValue(item, null);
                    if (i == 1)
                        values[i] = enc.EncryptData(values[i].ToString(), ConfigurationManager.AppSettings["key"]);
                    if (i == 2)
                    {
                        //Check Existing Bed Number and fetch the Code.
                        var chkExist = beddata.FirstOrDefault(a => a.BedNumber == values[1].ToString());
                        if (chkExist != null)
                            values[i] = chkExist.BedCode;
                        else
                        {
                            //Generate Bed Code

                            var ourRef = "BDC-" + count.ToString().PadLeft(5, '0');
                            values[i] = ourRef;
                            count++;
                        }
                    }
                }
                
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        public static DataTable ConvertCSVtoDataTable(string strFilePath, CompassEncryption enc, BedMasterData[] beddata)
        {
            DataTable dt = new DataTable();

            using (StreamReader sr = new StreamReader(strFilePath))
            {
                var getHeader = sr.ReadLine();
                getHeader = "BedCode," + getHeader;
                string[] headers = getHeader.Split(',');
                //dt.Columns.Add("BedCode");
                var count = beddata.Count() > 0 ? beddata.Max(d => d.ID) : 1;
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    //Generate Bed Code
                    //count = beddata.Count() > 0 ? beddata.Max(d => d.ID) : count;
                    var ourRef = "BDC-" + count.ToString().PadLeft(5, '0');
                    var getLine = sr.ReadLine();
                    getLine = ourRef + "," + getLine;

                    string[] rows = getLine.Split(',');
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        if(i==1)
                        rows[1] = enc.EncryptData(rows[1], ConfigurationManager.AppSettings["key"]);

                        if (i == 0)
                        {
                            //Check Existing Bed Number and fetch the Code.
                            var chkExist = beddata.FirstOrDefault(a => a.BedNumber == rows[1]);
                            if (chkExist != null)
                            {
                                rows[0] = chkExist.BedCode;
                            }
                            else
                                count++;
                        }

                        //rows[1] = enc.EncryptData(rows[1], ConfigurationManager.AppSettings["key"]);
                        dr[i] = rows[i];
                    }
                    
                    dt.Rows.Add(dr);
                }

            }
            return dt;
        }

        [CustAuthFilter]
        public ActionResult SaveStatusBedMaster(int id)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            var username = res.UserId;
            var saved = ServiceClient.SaveBedMasterStatus(GetRequestData(), id, username);
            var jsonResult = Json(saved, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            //return saved;
            return jsonResult;
        }
    }
}