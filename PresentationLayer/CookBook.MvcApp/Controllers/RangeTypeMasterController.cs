﻿using CookBook.Data.MasterData;
using CookBook.Data.UserData;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp.Controllers
{
    public class RangeTypeMasterController : BaseController
    {
        private CookBookService.ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
        
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return PartialView(GetRequestData());
        }
       
        [CustAuthFilter]
        public ActionResult GetRangeTypeDataList()
        {
            var aplList = ServiceClient.GetRangeTypeMasterData(GetRequestData());
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveRangeTypeData(RangeTypeMasterData model)
        {            
            var result = ServiceClient.SaveRangeTypeMasterData(GetRequestData(), model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}