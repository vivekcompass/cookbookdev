﻿using System.Configuration;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;

namespace CookBook.MvcApp.Controllers
{
    public class BaseController : Controller
    {
        ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (Session["UserLoginDetails"] == null || (Session["UserLoginDetails"] != null && ((UserMasterResponseData)Session["UserLoginDetails"]).UserId <= 0))
            {
                if (!Request.IsAuthenticated)
                {
                    HttpContext.GetOwinContext().Authentication.Challenge(
                        new AuthenticationProperties { RedirectUri = ConfigurationManager.AppSettings["redirectUri"].ToString() },
                        OpenIdConnectAuthenticationDefaults.AuthenticationType);
                }
                else
                {
                    //string UserName = User.Identity.Name;
                    var UserInstance = User.Identity as System.Security.Claims.ClaimsIdentity;
                    string UserName = UserInstance.FindFirst("preferred_username").Value;

                    if (string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.TEST_USER]))
                    {
                        UserName = ConfigurationManager.AppSettings[Constants.TEST_USER];
                    }
                    var res = ServiceClient.GetUserData(UserName,null);
                    res.ProfileName = @System.Security.Claims.ClaimsPrincipal.Current.FindFirst("name").Value;
                    Session["UserLoginDetails"] = res;

                  

                    if (res == null && (res != null && res.UserId <= 0))
                    { 
                        filterContext.Result = new RedirectResult("~/Home/NotAuthenticated");
                    }
                }
            }
        }

        public RequestData GetRequestData()
        {
            RequestData requestData = new RequestData();
            if (Session["UserLoginDetails"] != null)
            {
                requestData.SessionSectorName = ((UserMasterResponseData)Session["UserLoginDetails"]).SectorName;
                requestData.SessionSectorNumber = ((UserMasterResponseData)Session["UserLoginDetails"]).SectorNumber;
                requestData.SessionUserId = ((UserMasterResponseData)Session["UserLoginDetails"]).UserId;
            }
            else
            {
                var UserInstance = User.Identity as System.Security.Claims.ClaimsIdentity;
                string UserName = UserInstance.FindFirst("preferred_username").Value;
                var res = ServiceClient.GetUserData(UserName,null);
                res.ProfileName = @System.Security.Claims.ClaimsPrincipal.Current.FindFirst("name").Value;
                Session["UserLoginDetails"] = res;
                requestData.SessionSectorName = res.SectorName;
            }
            return requestData;
        }
    }
}