﻿using CookBook.Data.MasterData;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp.Controllers
{
    public class ManageSectorController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetManageSectorList()
        {
            var mogList = ServiceClient.GetSectorDataList(GetRequestData());
            return Json(mogList, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveManageSectorInfo(ManageSectorData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.SaveManageSectorInfo(GetRequestData(), model, userData.UserId);
            var jsonResult = Json(result, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }
        [CustAuthFilter]
        public ActionResult UpdateManageSectorInfo(ManageSectorData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.UpdateManageSectorInfo(GetRequestData(), model, userData.UserId);
            var jsonResult = Json(result, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }


        [CustAuthFilter]
        public ActionResult UpdateInactiveActive(ManageSectorData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.UpdateInactiveActive(GetRequestData(), model, userData.UserId);
            var jsonResult = Json(result, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }
    }
}