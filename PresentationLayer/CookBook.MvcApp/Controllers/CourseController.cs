﻿using System.Collections.Generic;
using System.Web.Mvc;
using HSEQ.Business.Contract;
using Microsoft.Practices.Unity;
using HSEQ.Aspects.Factory;
using HSEQ.Aspects.Constants;
using HSEQ.Data.Course;
using Microsoft.Practices.ServiceLocation;

namespace HSEQ.MvcApp.Controllers
{
    public class CourseController : BaseController
    {

        [Dependency]
        public ICourseCategory CourseCategoryManager { get; set; }

        
        public ICourseCategory DryCourseCategoryManager { get; set; }

        public ActionResult Index()
        {
            IList<CourseCategoryMasterData> courses = null;
            //var ctrl = ServiceLocator.Current.GetInstance<ICourse>();
            courses = CourseCategoryManager.GetCourseCategories();


            ViewBag.Message = "Your application description page.";

            return View(courses);
        }

        public JsonResult ChangeCourse(CourseCategoryMasterData model)
        {
            string message = "Fail";
            bool isSuccess = CourseCategoryManager.SaveCourse(model);
            if(isSuccess)
            {
                message = "Success";
            }
            
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public void LogCourseDetails(CourseData course)
        {
            LogTraceFactory.WriteLogWithCategory(course.CourseName, LogTraceCategoryNames.General);
        }
    }
}