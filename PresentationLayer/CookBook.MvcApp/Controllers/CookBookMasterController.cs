﻿using ClosedXML.Excel;
using CookBook.Aspects.Factory;
using CookBook.Data;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CookBook.Data.MasterData;

namespace CookBook.MvcApp.Controllers
{
    public class CookBookMasterController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult VisualCategory()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult UOMModule()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult UOMModuleMapping()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveUOMModule(UOMModuleData model)
        {
            var result = ServiceClient.SaveUOMModuleMasterData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveUOMModuleMapping(UOMModuleMappingData model)
        {
            var result = ServiceClient.SaveUOMModuleMappingData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult DietCategory()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }


        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Allergen()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        public ActionResult GetAllergenDataList()
        {
            var allergenList = ServiceClient.GetAllergenDataList(GetRequestData());
            var jsonResult = Json(allergenList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveAllergen(AllergenData model)
        {
            var result = ServiceClient.SaveAllergenData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult GetSiteTypeDataList()
        {
            var SiteTypeList = ServiceClient.GetSiteTypeDataList(GetRequestData());
            var jsonResult = Json(SiteTypeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetFoodProgramDataList()
        {
            var foodProgramList = ServiceClient.GetFoodProgramDataList(GetRequestData());
            var jsonResult = Json(foodProgramList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        //[CustAuthFilter]
        //public ActionResult GetFoodPanDataList()
        //{
        //    var foodPanList = ServiceClient.GetFoodPanDataList(GetRequestData());
        //    var jsonResult = Json(foodPanList, JsonRequestBehavior.AllowGet);
        //    jsonResult.MaxJsonLength = Int32.MaxValue;
        //    return jsonResult;
        //}

        [CustAuthFilter]
        public ActionResult GetVisualCategoryDataList()
        {
            var visualCategoryList = ServiceClient.GetVisualCategoryDataList(GetRequestData());
            var jsonResult = Json(visualCategoryList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetDishCategoryDataList()
        {
            var dishCategoryList = ServiceClient.GetDishCategoryDataList(GetRequestData());
            var jsonResult = Json(dishCategoryList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetDishSubCategoryDataList()
        {
            var subdishCategoryList = ServiceClient.GetDishSubCategoryDataList(GetRequestData());
            var jsonResult = Json(subdishCategoryList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetDishTypeDataList()
        {
            var dishTypeyList = ServiceClient.GetDishTypeDataList(GetRequestData());
            var jsonResult = Json(dishTypeyList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetDietCategoryDataList()
        {
            var dietCategoryList = ServiceClient.GetDietCategoryDataList(GetRequestData());
            var jsonResult = Json(dietCategoryList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetDayPartDataList()
        {
            var dayPartList = ServiceClient.GetDayPartDataList(GetRequestData());
            var jsonResult = Json(dayPartList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetRevenueTypeDataList()
        {
            var dayPartList = ServiceClient.GetRevenueTypeDataList(GetRequestData());
            var jsonResult = Json(dayPartList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetConceptType1DataList()
        {
            var conceptType1List = ServiceClient.GetConceptType1DataList(GetRequestData());
            var jsonResult = Json(conceptType1List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetConceptType2DataList()
        {
            var conceptType2List = ServiceClient.GetConceptType2DataList(GetRequestData());
            var jsonResult = Json(conceptType2List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetConceptType3DataList()
        {
            var conceptType3List = ServiceClient.GetConceptType3DataList(GetRequestData());
            var jsonResult = Json(conceptType3List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetConceptType4DataList()
        {
            var conceptType4List = ServiceClient.GetConceptType4DataList(GetRequestData());
            var jsonResult = Json(conceptType4List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetConceptType5DataList()
        {
            var conceptType5List = ServiceClient.GetConceptType5DataList(GetRequestData());
            var jsonResult = Json(conceptType5List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetItemType1DataList()
        {
            var itemType1List = ServiceClient.GetItemType1DataList(GetRequestData());
            var jsonResult = Json(itemType1List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetItemType2DataList()
        {
            var itemType2List = ServiceClient.GetItemType2DataList(GetRequestData());
            var jsonResult = Json(itemType2List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetServewareDataList()
        {
            var servewareList = ServiceClient.GetServewareDataList(GetRequestData());
            var jsonResult = Json(servewareList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteProfile1DataList()
        {
            var siteProfile1List = ServiceClient.GetSiteProfile1DataList(GetRequestData());
            var jsonResult = Json(siteProfile1List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteProfile2DataList()
        {
            var siteProfile2List = ServiceClient.GetSiteProfile2DataList(GetRequestData());
            var jsonResult = Json(siteProfile2List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteProfile3DataList()
        {
            var siteProfile3List = ServiceClient.GetSiteProfile3DataList(GetRequestData());
            var jsonResult = Json(siteProfile3List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetServingTemperatureDataList()
        {
            var siteProfile3List = ServiceClient.GetServingTemperatureDataList(GetRequestData());
            var jsonResult = Json(siteProfile3List, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetUOMDataList()
        {
            var uomList = ServiceClient.GetUOMDataList(GetRequestData());
            var jsonResult = Json(uomList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetUOMModuleDataList()
        {
            var uomList = ServiceClient.GetUOMModuleDataList(GetRequestData());
            var jsonResult = Json(uomList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetUOMModuleMappingDataList()
        {
            var uomList = ServiceClient.GetUOMModuleMappingDataList(GetRequestData());
            var jsonResult = Json(uomList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetUOMModuleMappingGridData()
        {
            var uomList = ServiceClient.procGetUOMModuleMappingGridData(GetRequestData());
            var jsonResult = Json(uomList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetReasonTypeDataList()
        {
            var ReasonTypeList = ServiceClient.GetReasonTypeDataList(GetRequestData());
            var jsonResult = Json(ReasonTypeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetExpiryCategoryDataList()
        {
            var list = ServiceClient.GetExpiryCategoryDataList(GetRequestData());
            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

       

        [CustAuthFilter]
        public ActionResult GetShelfLifeUOMDataList()
        {
            var list = ServiceClient.GetShelfLifeUOMDataList(GetRequestData());
            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveFoodProgram(FoodProgramData model)
        {
            var result = ServiceClient.SaveFoodProgramData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //[CustAuthFilter]
        //public ActionResult SaveFoodPan(FoodPanData model)
        //{
        //    var result = ServiceClient.SaveFoodPanData(model,GetRequestData());
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveVisualCategory(VisualCategoryData model)
        {
            var result = ServiceClient.SaveVisualCategoryData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveDishCategory(DishCategoryData model)
        {
            var result = ServiceClient.SaveDishCategoryData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveDishSubCategory(DishSubCategoryData model)
        {
            var result = ServiceClient.SaveDishSubCategoryData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveConceptType1(ConceptType1Data model)
        {
            var result = ServiceClient.SaveConceptType1Data(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveConceptType2(ConceptType2Data model)
        {
            var result = ServiceClient.SaveConceptType2Data(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveConceptType3(ConceptType3Data model)
        {
            var result = ServiceClient.SaveConceptType3Data(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveConceptType4(ConceptType4Data model)
        {
            var result = ServiceClient.SaveConceptType4Data(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveConceptType5(ConceptType5Data model)
        {
            var result = ServiceClient.SaveConceptType5Data(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveSiteProfile1(SiteProfile1Data model)
        {
            var result = ServiceClient.SaveSiteProfile1Data(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveSiteProfile2(SiteProfile2Data model)
        {
            var result = ServiceClient.SaveSiteProfile2Data(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveSiteProfile3(SiteProfile3Data model)
        {
            var result = ServiceClient.SaveSiteProfile3Data(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveItemType1(ItemType1Data model)
        {
            var result = ServiceClient.SaveItemType1Data(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveItemType2(ItemType2Data model)
        {
            var result = ServiceClient.SaveItemType2Data(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveDietCategory(DietCategoryData model)
        {
            var result = ServiceClient.SaveDietCategoryData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveDayPart(DayPartData model)
        {
            var result = ServiceClient.SaveDayPartData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveReason(ReasonData model)
        {
            var result = ServiceClient.SaveReasonData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveRevenueType(RevenueTypeData model)
        {
            var result = ServiceClient.SaveRevenueTypeData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveUOM(UOMData model)
        {
            var result = ServiceClient.SaveUOMData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveReasonType(ReasonTypeData model)
        {
            var result = ServiceClient.SaveReasonTypeData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveExpiryCategory(ExpiryCategoryMasterData model)
        {
            var result = ServiceClient.SaveExpiryCategory(GetRequestData(), model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveNutritionMaster(NutritionElementMasterData model)
        {
            var result = ServiceClient.SaveNutritionMaster(GetRequestData(), model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetNutritionMasterDataList()
        {
            var list = ServiceClient.GetNutritionMasterDataList(GetRequestData());
            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetNutritionElementTypeList()
        {
            var list = ServiceClient.GetNutritionElementTypeDataList(GetRequestData());
            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveServeware(ServewareData model)
        {
            var result = ServiceClient.SaveServewareData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveServingTemperature(ServingTemperatureData model)
        {
            var result = ServiceClient.SaveServingTemperatureData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        

        public ActionResult GetLoginUserDetails()
        {
            //string UserName = User.Identity.Name;
            //string uname = "";
            //if (UserName.IndexOf("\\") > 0)
            //{
            //    uname = UserName.Split('\\')[1];
            //}
            //else
            //{
            //    uname = UserName;
            //}
            //if (string.IsNullOrEmpty(uname) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.TEST_USER]))
            //{
            //    uname = ConfigurationManager.AppSettings[Constants.TEST_USER];
            //}
            var res = Session["UserLoginDetails"];
             

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult CommonMasterList(CommonMasterData model)
        {
            var data = ServiceClient.GetCommonMasterDataList(GetRequestData(), model);
            var jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }
    }
}