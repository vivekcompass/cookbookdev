﻿using System;
using System.Web.Mvc;
using CookBook.Data.MasterData;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;

namespace CookBook.MvcApp.Controllers
{
    public class ContainerController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
      
        [CustAuthFilter]
        public ActionResult SaveContainer(ContainerData model)
        {
            var result = ServiceClient.SaveContainerData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult GetContainerDataList()
        {
            var foodPanList = ServiceClient.GetContainerDataList( GetRequestData());
            var jsonResult = Json(foodPanList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult Container()
        {
            //return View( GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            //return View( GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult FoodPanAlias()
        {
            return View( GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult SiteContainer()
        {
            //return View( GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetContainerTypeDataList()
        {
            var containerList = ServiceClient.GetContainerTypeDataList( GetRequestData());
            var jsonResult = Json(containerList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        //[CustAuthFilter]
        //public ActionResult GetFoodPanAliasDataList()
        //{
        //    var foodList = ServiceClient.GetFoodPanAliasDataList( GetRequestData());
        //    var jsonResult = Json(foodList, JsonRequestBehavior.AllowGet);
        //    jsonResult.MaxJsonLength = Int32.MaxValue;
        //    return jsonResult;
        //}
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveContainerTypeData(ContainerTypeData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }
            var result = ServiceClient.SaveContainerTypeData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult ChangeStatus(ContainerTypeData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.ModifiedBy = userData.UserId;
            model.ModifiedOn = DateTime.Now;
            var result = ServiceClient.SaveContainerTypeData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult ChangeContainerStatus(ContainerData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.ModifiedBy = userData.UserId;
            model.ModifiedOn = DateTime.Now;
            var result = ServiceClient.SaveContainerData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveContainerTypeDataList(ContainerTypeData[] model)
        {
            var result = ServiceClient.SaveContainerTypeDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //[CustAuthFilter]
        //public ActionResult SaveFoodPanAliasData(FoodPanAliasData model)
        //{
        //    UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

        //    if (model.ID == 0)
        //    {
        //        model.CreatedBy = userData.UserId;
        //        model.CreatedOn = DateTime.Now;
        //    }
        //    else
        //    {
        //        model.ModifiedBy = userData.UserId;
        //        model.ModifiedOn = DateTime.Now;
        //    }
        //    var result = ServiceClient.SaveFoodPanAliasData(model, GetRequestData());
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //[CustAuthFilter]
        //public ActionResult ChangeStatusFoodPanAlias(FoodPanAliasData model)
        //{
        //    UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
        //    model.ModifiedBy = userData.UserId;
        //    model.ModifiedOn = DateTime.Now;
        //    var result = ServiceClient.SaveFoodPanAliasData(model, GetRequestData());
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //[CustAuthFilter]
        //public ActionResult SaveFoodPanAliasDataList(FoodPanAliasData[] model)
        //{
        //    var result = ServiceClient.SaveFoodPanAliasDataList(model, GetRequestData());
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        [CustAuthFilter]
        public ActionResult GetDishDataListForContainer(string siteCode)
        {
            var aplList = ServiceClient.GetSiteDishContainerMappingDataList(siteCode, GetRequestData());
            var jsonResult = Json(aplList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetDishCategoryDataListForContainer(string siteCode)
        {
            var aplList = ServiceClient.GetSiteDishCategoryContainerMappingDataList(siteCode, GetRequestData());
            var jsonResult = Json(aplList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult SaveDishContainerData(SiteDishContainerMappingData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.ModifiedBy = userData.UserId;
            model.ModifiedOn = DateTime.Now;
            var result = ServiceClient.SaveSiteDishContainerMappingData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveContainerDataList(SiteDishContainerMappingData[] model)
        {
            var result = ServiceClient.SaveSiteDishContainerMappingDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveContainerCategoryDataList(SiteDishCategoryContainerMappingData[] model)
        {
            var result = ServiceClient.SaveSiteDishCategoryContainerMappingDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }




    }
}