﻿using CookBook.Data.UserData;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.CookBookService;
using System.Configuration;
using System.Web.Mvc;
using CookBook.MvcApp.Filters;
using System.Web;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.Owin.Security.Cookies;
using CookBook.Data.Request;
using System.Collections.Generic;
using CookBook.Data.MasterData;
using System.Net.Http;
using System;
using System.Net.Http.Headers;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using CookBook.Aspects.Factory;
using CookBook.Aspects.Constants;
using System.Web.Security;

namespace CookBook.MvcApp.Controllers
{
    public class HomeController : Controller
    {
        ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        public ActionResult Index()
        {

            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                string UserName = UserInstance.FindFirst("preferred_username").Value;

                if (string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.TEST_USER]))
                {
                    UserName = ConfigurationManager.AppSettings[Constants.TEST_USER];
                }
                var res = ServiceClient.GetUserData(UserName, null);
                res.ProfileName = @System.Security.Claims.ClaimsPrincipal.Current.FindFirst("name").Value;
                HttpContext.Session["UserLoginDetails"] = res;

            }

            try
            {
                // ChartwellDataPush();
                //HospitalBedMasterDataPushtoDBAsync();
             //  HospitalDataPushtoDBAsync();

              //  HospitalUserMasterDataPushtoDBAsync();
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }

            return View();
        }

        private void HospitalDataPushtoDBAsync()
        {
            try
            {
                var httpClient = new HttpClient();
                // Set basic authentication credentials
                var apiUrl = "https://medirest.rainbowhospitals.in:5017/thirdparty/GetIPPatientDetails";
                var username = "hisuser";
                var password = "620a299e05f5a047fb8f829c";
                //var orgCode = "40HN";
                //var siteCode = "150F";
                //var dbName = "DTRWA_150F";

                var orgCode = "40HI";
                var siteCode = "144S";
                var dbName = "DTRWA_144S";
                var endDate = DateTime.Now.ToShortDateString();
                 var startDate = DateTime.Now.AddDays(-1).ToShortDateString();
                //var startDate = DateTime.Now.AddDays(-30).ToShortDateString();
                var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

                var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Deserialize the JSON response
                    var jsonResponse = response.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<List<HISPatientProfileData>>(jsonResponse);
                    RequestData requestData = new RequestData();
                    requestData.SiteCode = siteCode;
                    requestData.SessionSectorName = dbName;
                    ServiceHelper.GetCookBookService().SaveHISPatientProfileMaster(requestData, data.ToArray());
                    LogTraceFactory.WriteLogWithCategory("HospitalDataPushtoDBAsync:Data saved successfully", LogTraceCategoryNames.Tracing);
                }
                else
                {
                    LogTraceFactory.WriteLogWithCategory("HospitalDataPushtoDBAsync:" + response.StatusCode + "", LogTraceCategoryNames.Tracing);
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("HospitalDataPushtoDBAsync: " + ex.ToString() + "", LogTraceCategoryNames.Tracing);
            }
        }

       

        public void HospitalBedMasterDataPushtoDBAsync()
        {

            var httpClient = new HttpClient();
            // Set basic authentication credentials
            var apiUrl = "https://medirest.rainbowhospitals.in:5017/thirdparty/GetBedDetails";
            var username = "hisuser";
            var password = "620a299e05f5a047fb8f829c";
            var endDate = DateTime.Now.ToShortDateString();
            var startDate = DateTime.Now.AddDays(-10).ToShortDateString();
            //var orgCode = "40KO";
            //var siteCode = "106B";
            //var dbName = "DTRWA_144O";

            var orgCode = "40HN";
            var siteCode = "150F";
            var dbName = "DTRWA_150F";

            //var orgCode = "40HI";
            //var siteCode = "144S";
            //var dbName = "DTRWA_144S";
            var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

            var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the JSON response
                var jsonResponse = response.Content.ReadAsStringAsync().Result;
                var data = JsonConvert.DeserializeObject<List<HISBedMasterData>>(jsonResponse);
                RequestData requestData = new RequestData();
                requestData.SiteCode = siteCode;
                requestData.SessionSectorName = dbName;
                ServiceClient.SaveHISBedMasterData(requestData, data.ToArray());
                Console.WriteLine("Data saved successfully.");
            }
            else
            {
                Console.WriteLine($"Failed to call API. Status code: {response.StatusCode}");
            }
        }

        public void HospitalUserMasterDataPushtoDBAsync()
        {

            var httpClient = new HttpClient();
            // Set basic authentication credentials
            var apiUrl = "https://medirest.rainbowhospitals.in:5017/thirdparty/GetUserDetails";
            var username = "hisuser";
            var password = "620a299e05f5a047fb8f829c";
            var endDate = DateTime.Now.ToShortDateString();
            var startDate = DateTime.Now.AddDays(-10).ToShortDateString();
            //var orgCode = "40KO";
            //var siteCode = "106B";
            //var dbName = "DTRWA_144O";

            //var orgCode = "40HN";
            //var siteCode = "150F";
            //var dbName = "DTRWA_150F";

            var orgCode = "40HI";
            var siteCode = "144S";
            var dbName = "DTRWA_144S";
            var parameters = new Dictionary<string, string>
            {
                { "StartDttm", startDate },
                { "EndDttm", endDate },
                { "OrgCode", orgCode }
            };

            var authHeaderValue = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{username}:{password}"));
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = httpClient.PostAsync(apiUrl, content).Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the JSON response
                var jsonResponse = response.Content.ReadAsStringAsync().Result;
                var data = JsonConvert.DeserializeObject<List<HISUserData>>(jsonResponse);
                RequestData requestData = new RequestData();
                requestData.SiteCode = siteCode;
                requestData.SessionSectorName = dbName;
                ServiceClient.SaveHISUserData(requestData, data.ToArray());
                Console.WriteLine("Data saved successfully.");
            }
            else
            {
                Console.WriteLine($"Failed to call API. Status code: {response.StatusCode}");
            }
        }

        public static string ToQueryString(Dictionary<string, string> parameters)
        {
            var keyValuePairs = new List<string>();
            foreach (var kvp in parameters)
            {
                keyValuePairs.Add($"{kvp.Key}={Uri.EscapeDataString(kvp.Value)}");
            }
            return string.Join("&", keyValuePairs);
        }

        public void ChartwellDataPush()
        {
            var token = "8b5be523-0db2-4984-a363-9c292205baed";
            string URL = "https://nutrition-info-smartqdemo-pnbahkgobq-uc.a.run.app/V2/Nutrition";
            var data = ServiceHelper.GetCookBookService().GetFoodBookNutritionDataList("SEC-0010");
            List<FoodBookNutritionDataRead> _output = new List<FoodBookNutritionDataRead>();
            if (data != null && data.Count() > 0)
            {
                foreach (var food in data)
                {
                    var fobj = new FoodBookNutritionDataRead();
                    fobj.Allergens = food.Allergens;
                    fobj.ArticleNumber = food.ArticleNumber;
                    fobj.HealthTags = food.HealthTags;
                    fobj.Ingredients = food.Ingredients.Replace(",", "");
                    // fobj.Nutrition = food.Nutrition;
                    fobj.Nutrition = food.Nutrition.Replace("ElementName", "name");
                    fobj.Nutrition = fobj.Nutrition.Replace("UOMName", "uom");
                    fobj.Nutrition = fobj.Nutrition.Replace("Quantity", "value");
                    fobj.PortionSize = food.PortionSize;
                    fobj.PortionUOM = food.PortionUOM;
                    fobj.SiteCode = food.SiteCode;
                    fobj.Details = new List<DishDetailsArray>();
                    var dArray = JsonConvert.DeserializeObject<List<DishDetails>>(food.Details);
                    if (dArray != null && dArray.Count() > 0)
                    {
                        foreach (var dobj in dArray)
                        {
                            DishDetailsArray dd = new DishDetailsArray();
                            dd.DishName = dobj.DishName;
                            dd.DishCode = dobj.DishCode;
                            dd.HealthTags = dobj.HealthTags;
                            //  dd.Ingredients = dobj.Ingredients;
                            dd.Allergens = JsonConvert.DeserializeObject<List<AllergenJson>>(dobj.Allergens);
                            dd.Nutrition = JsonConvert.DeserializeObject<List<NutritionJson>>(dobj.Nutrition);
                            fobj.Details.Add(dd);
                        }
                    }
                    _output.Add(fobj);
                }
            }
            var total = _output.Count();
            var pageSize = 50;
            var loopCount = 1;
            var pageLeft = total - pageSize * loopCount;
            while (pageLeft > 0 || pageSize < 50)
            {
                var skip = pageSize * (loopCount - 1);
                if (loopCount > 1)
                {
                    pageLeft = total - pageSize * loopCount;
                }
                var pdata = _output.Skip(skip).Take(pageSize).ToArray();
                var output = JsonConvert.SerializeObject(pdata);
                string DATA = @"" + output + "";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("token", token);
                request.ContentLength = DATA.Length;
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
                requestWriter.Write(DATA);
                requestWriter.Close();
                try
                {
                    WebResponse webResponse = request.GetResponse();
                    using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                    using (StreamReader responseReader = new StreamReader(webStream))
                    {
                        string response = responseReader.ReadToEnd();
                        Console.Out.WriteLine(response);
                        loopCount++;
                    }
                }
                catch (Exception ex)
                {
                    LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
                }
            }
        }
       
        [CustAuthFilter]
        public ActionResult GetSectorDataList()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            var sectorList = ServiceClient.GetSectorDataByUserID(userData.UserId, GetRequestData());
            return Json(sectorList, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetModulesByUserID()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            var moduleList = ServiceClient.GetModulesByUserID(userData.UserId, GetRequestData());
            return Json(moduleList, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetUserPermissionsByModuleCode()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            var userPermissionList = ServiceClient.GetUserPermissionsByModuleCode(userData.UserId, "01", GetRequestData());
            return Json(userPermissionList, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult ChangeSectorData(string sectorNumber)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.ChangeSectorData(userData.UserId, sectorNumber, GetRequestData());
            //var result = ServiceClient.ChangeSectorData(userData.UserId, 10, GetRequestData());
            var UserInstance = User.Identity as System.Security.Claims.ClaimsIdentity;
            string UserName = UserInstance.FindFirst("preferred_username").Value;

            var res = ServiceClient.GetUserData(UserName, null);
            res.ProfileName = @System.Security.Claims.ClaimsPrincipal.Current.FindFirst("name").Value;
            Session["UserLoginDetails"] = res;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult ChangeUserRoleData(int userRoleId)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.ChangeUserRoleData(userData.UserId, userRoleId, GetRequestData());
            var UserInstance = User.Identity as System.Security.Claims.ClaimsIdentity;
            string UserName = UserInstance.FindFirst("preferred_username").Value;

            var res = ServiceClient.GetUserData(UserName, null);
            res.ProfileName = @System.Security.Claims.ClaimsPrincipal.Current.FindFirst("name").Value;
            Session["UserLoginDetails"] = res;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void SignIn()
        {
            if (!Request.IsAuthenticated)
            {
                HttpContext.GetOwinContext().Authentication.Challenge(
                    new AuthenticationProperties { RedirectUri = "/" },
                    OpenIdConnectAuthenticationDefaults.AuthenticationType);
            }
        }

        /// <summary>
        /// Send an OpenID Connect sign-out request.
        /// </summary>
       // [HttpPost]
       // [ValidateAntiForgeryToken]
        public ActionResult SignOut()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(
                    OpenIdConnectAuthenticationDefaults.AuthenticationType,
                    CookieAuthenticationDefaults.AuthenticationType);
            FormsAuthentication.SignOut();

            // Optionally clear the session data
            Session.Clear();
            Session.Abandon();

            // Redirect to the home page or login page
            return RedirectToAction("Index", "Home");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult NotAuthenticated()
        {
            return View();
        }

        public ActionResult ErrorNotFound()
        {
            return View();
        }

        public ActionResult ErrorInternalServerError()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        public ActionResult SortingToolError()
        {
            return View();
        }

        public ActionResult CheckSessionTimeout() => View();

        [CustAuthFilter]
        public ActionResult ForceSessionRefresh()
        {
            HttpContext.GetOwinContext()
                   .Authentication.Challenge(new AuthenticationProperties { RedirectUri = "/" },
                       OpenIdConnectAuthenticationDefaults.AuthenticationType);

            return null;
        }

        public RequestData GetRequestData()
        {
            RequestData requestData = new RequestData();
            if (Session["UserLoginDetails"] != null)
            {
                requestData.SessionSectorName = ((UserMasterResponseData)Session["UserLoginDetails"]).SectorName;
            }
            else
            {
                var UserInstance = User.Identity as System.Security.Claims.ClaimsIdentity;
                string UserName = UserInstance.FindFirst("preferred_username").Value;
                var res = ServiceClient.GetUserData(UserName, null);
                res.ProfileName = @System.Security.Claims.ClaimsPrincipal.Current.FindFirst("name").Value;
                Session["UserLoginDetails"] = res;
                requestData.SessionSectorName = res.SectorName;
            }
            return requestData;
        }
        //Krish

        [CustAuthFilter]
        public ActionResult GetUserRoleDataByUserID()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            var userRoleList = ServiceClient.GetUserRoleDataByUserID(userData.UserId, GetRequestData());
            return Json(userRoleList, JsonRequestBehavior.AllowGet);
        }
    }

   
}