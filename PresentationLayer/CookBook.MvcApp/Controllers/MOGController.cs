﻿using ClosedXML.Excel;
using CookBook.Aspects.Factory;
using CookBook.Data;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CookBook.Data.MasterData;
using System.Data.OleDb;
using Newtonsoft.Json;

namespace CookBook.MvcApp.Controllers
{
    public class MOGController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            //return View(GetRequestData());

            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult InheritMOG()
        {
            //return View(GetRequestData());

            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        public ActionResult GetMOGDataList()
        {
            var mogList = ServiceClient.GetMOGDataList(GetRequestData(), null, 0);
            return Json(mogList, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetMOGUOMDataList()
        {
            var mogList = ServiceClient.GetMOGUOMDataList(GetRequestData());
            return Json(mogList, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult GetInheritedMOGDataList()
        {
            var mogList = ServiceClient.GetInheritedMOGDataList(GetRequestData());
            return Json(mogList, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetSiteMOGDataList(string siteCode)
        {
            var mogList = ServiceClient.GetMOGDataList(GetRequestData(), siteCode, 0);
            return Json(mogList, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetRegionMOGDataList(int regionID)
        {
            var mogList = ServiceClient.GetMOGDataList(GetRequestData(), null,regionID);
            return Json(mogList, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveMOGData(MOGData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
                model.IsActive = true;
                model.Status = (int)StatusEnum.Pending;
            }
            else
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }
            var result = ServiceClient.SaveMOGData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveMOGDataList(MOGData[] model)
        {
            var result = "false";
            if (model != null)
            {
                result = ServiceClient.SaveMOGDataList(model,GetRequestData());
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetMOGImpactedDataList(int mogID)
        {
            var impactedData = new MOGImpactedData();
            if (Convert.ToBoolean(ConfigurationManager.AppSettings[Constants.ISRIENABLED]))
            {
                impactedData = ServiceClient.GetMOGImpactedDataList(mogID, 0, false,GetRequestData());
            }
            return Json(impactedData, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SendMOGEmail(MOGData model)
        {
            var mogList = ServiceClient.SendMOGEmail(model,GetRequestData());
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SendNewMOGEmail(MOGData model)
        {
            var mogList = ServiceClient.SendNewMOGEmail(model, GetRequestData());
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult NationalMOG()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());

        }


        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult MOGProcessMapping()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());

        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult MOGAPLMapping()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());

        }

        [CustAuthFilter]
        public ActionResult GetNationalMOGDataList()
        {
            var mogList = ServiceClient.GetNationalMOGDataList(GetRequestData());
            var jsonResult = Json(mogList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        [HttpGet]
        public ActionResult DownloadExcTemplate()
        {
            var mogList = ServiceClient.GetNOTMappedMOGDataList(GetRequestData());
            DataTable dt = new DataTable();
            dt.Columns.Add("MOGCode");
            dt.Columns.Add("Name");
            dt.Columns.Add("Alias");
            dt.Columns.Add("ArticleNumber");
            foreach (MOGNotMapped p in mogList)
            {
                dt.Rows.Add(p.MOGCode, p.Name, p.Alias,p.ArticalNumber);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                dt.TableName="MOGNOTMAPPED.xlsx";
                wb.Worksheets.Add(dt);
               
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "MOG-APLUploadTemplate.xlsx");
                }
            }

           
        }

        [CustAuthFilter]
        [HttpPost]
        public string UploadExcelsheetMOG()
        {
            string Res = string.Empty;
            DataTable dt = new DataTable();
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                var fileext = Path.GetExtension(file.FileName);
                string contentType = string.Empty;

                var fileExtension = Path.GetExtension(file.FileName).ToLowerInvariant();
                var allowedExtensions = new[] { ".xlsx" };

                if (!allowedExtensions.Contains(fileExtension))
                {
                    return "Invalid file type. Only XLSX files are allowed.";
                }

                // Optionally, check the MIME type
                if (fileext == ".xlsx")
                {
                    contentType = "image/xlsx";
                }
              
                var allowedMimeTypes = new[] { "image/xlsx" };

                if (!allowedMimeTypes.Contains(contentType))
                {
                    return "Invalid file type. Only XLSX files are allowed.";
                }

                // List<ProductModel> _lstProductMaster = new List<ProductModel>();
                string filePath = string.Empty;

                if (Request.Files != null)
                {
                    string path = Path.Combine(Server.MapPath("~/Content/Upload/"), Path.GetFileName(file.FileName));
                    //Saving the file
                    file.SaveAs(path);

                    using (XLWorkbook workbook = new XLWorkbook(path))
                    {
                        IXLWorksheet worksheet = workbook.Worksheet(1);
                        bool FirstRow = true;
                        //Range for reading the cells based on the last cell used.
                        string readRange = "1:1";
                        foreach (IXLRow row in worksheet.RowsUsed())
                        {

                            if (FirstRow)
                            {

                                readRange = string.Format("{0}:{1}", 1, row.LastCellUsed().Address.ColumnNumber);
                                foreach (IXLCell cell in row.Cells(readRange))
                                {
                                    dt.Columns.Add(cell.Value.ToString());
                                }
                                FirstRow = false;
                            }
                            else
                            {

                                dt.Rows.Add();
                                int cellIndex = 0;

                                foreach (IXLCell cell in row.Cells(readRange))
                                {
                                    dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                                    cellIndex++;
                                }
                            }
                        }

                    }

                    dt.TableName = "MOG";
                    MemoryStream str = new MemoryStream();
                    dt.WriteXml(str, true);
                    str.Seek(0, SeekOrigin.Begin);
                    StreamReader sr = new StreamReader(str);
                    string xmlstr;
                    xmlstr = sr.ReadToEnd();

                    string UserName = string.Empty;
                    var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
                    if (UserInstance.IsAuthenticated)
                    {
                        UserName = UserInstance.FindFirst("preferred_username").Value;
                    }
                    var res = ServiceClient.GetUserData(UserName, null);
                    string username = res.UserId.ToString();
                    Res = ServiceClient.UpdateMOGLIST(GetRequestData(), xmlstr, username);

                }


            }
            return Res;

        }

        [CustAuthFilter]
        public ActionResult GetMOGAPLHISTORY()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                 UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString(); 
            
            var regionRecipeList = ServiceClient.GetMOGAPLHISTORY(GetRequestData(),username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;

            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetMOGAPLHISTORYDetails()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();
            //  DETAILS
            var regionRecipeList_det = ServiceClient.GetMOGAPLHISTORYDet(GetRequestData(), username);
            var jsonResult = Json(regionRecipeList_det, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }

        [CustAuthFilter]
        public ActionResult GetALLHISTORYDetails()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();
            //  DETAILS
            var regionRecipeList_det = ServiceClient.GetALLHISTORYDetails(GetRequestData(), username);
            var jsonResult = Json(regionRecipeList_det, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }

        [CustAuthFilter]
        public ActionResult GetshowGetALLHISTORYBATCHWISE(string BATCHNUMBER)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();
            //  DETAILS
            var regionRecipeList_det = ServiceClient.GetshowGetALLHISTORYBATCHWISE(GetRequestData(), username, BATCHNUMBER);
            var jsonResult = Json(regionRecipeList_det, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }

        [CustAuthFilter]
        public ActionResult GetAPLMasterDataListForMOG(string mogCode)
        {
            var aplList = ServiceClient.GetAPLMasterDataListCommon(null, mogCode,GetRequestData());
            return Json(aplList, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetAPLMasterDataList()
        {
            var aplList = ServiceClient.GetAPLMasterDataListCommon(null, null,GetRequestData());
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [CustAuthFilter]
        public ActionResult GetMOGAPLImpactedDataList(int mogID)
        {
            var impactedData = new MOGAPLImpactedData();
            if (Convert.ToBoolean(ConfigurationManager.AppSettings[Constants.ISRIENABLED]))
            {
                impactedData = ServiceClient.GetMOGAPLImpactedDataList(mogID,null,30,GetRequestData());
            }
            return Json(impactedData, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult ExpiryCategoryMapping()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());

        }

        [CustAuthFilter]
        public ActionResult GetExpiryProductDataList()
        {
            var list = ServiceClient.GetExpiryProductDataList(GetRequestData());
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult SaveExpiryProductData(GetExpiryProductData model)
        {
            var result = "false";
            if (model != null)
            {
                result = ServiceClient.SaveExpiryProductData(model, GetRequestData());
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //Krish
        [CustAuthFilter]
        public ActionResult GetExpiryProductDataOnlyExpiryNameList()
        {
            var list = ServiceClient.GetExpiryProductDataOnlyExpiryNameList(GetRequestData());
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult ExportLabelPrintingData(LabelPrintingData[] labelPrintingData)
        {
            //var itemList = new List<LabelPrintingData>();
            //itemList = labelPrintingData.ToList();
            //itemList.Add(new LabelPrintingData() { 
            //    ProductName = "65 Marination",
            //    ScannedDate = "5/7/2022",
            //    ScannedTime = "15:58:19",
            //    PermitDay = "3 Days",
            //    PermitHour = "",
            //    ExpiryDate = "8/7/2022",
            //    ExpiryTime = "03:56:00",
            //    LabelCount = 1,
            //});
            FileContentResult rObj;
            using (XLWorkbook wb = new XLWorkbook())
            {
                IXLWorksheet workSheet = wb.AddWorksheet("Sheet1");
                workSheet.ShowGridLines = true;
                int rowCount = 1;

                DataTable dataTableMenuRequisitionDetails = GetLabelPrintingDetailsTable(labelPrintingData.ToList());

                wb.Worksheet(1).Cell(rowCount, 1).InsertTable(dataTableMenuRequisitionDetails);
                int tableRowCount = dataTableMenuRequisitionDetails.Rows.Count;
                rowCount = rowCount + 2;

                workSheet.Rows().AdjustToContents();
                workSheet.Columns().AdjustToContents();
                workSheet.Columns().Style.Alignment.Vertical = XLAlignmentVerticalValues.Justify;
                workSheet.Columns().CellsUsed().SetDataType(XLDataType.Text);
                foreach (var item in workSheet.Tables)
                {
                    item.SetDataType(XLDataType.Text);
                    item.ShowAutoFilter = false;
                }

                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                     var bytesdata = File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ExpiryCategoryData.xlsx");
                    rObj = bytesdata;
                }
            }
            var jsonResult = Json(rObj, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
            // return Json(rObj, JsonRequestBehavior.AllowGet);
        }
        //Krish 18-10-2022
        [CustAuthFilter]
        public ActionResult SaveProcessTypeMogData(List<string> processTypes,string mogCode,string rangeTypeCode,int inputStdDuration)
        {
            var result = ServiceClient.SaveProcessTypeMogData(GetRequestData(), processTypes.ToArray(), mogCode, rangeTypeCode, inputStdDuration);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult GetProcessTypeMogData(string mogCode)
        {
            var result = ServiceClient.GetProcessTypeMogData(GetRequestData(),  mogCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveAllergenMogData(List<string> contains, List<string> maycontains, string mogCode)
        {
           // var result = ServiceClient.SaveProcessTypeMogData(GetRequestData(), processTypes.ToArray(), mogCode, rangeTypeCode, inputStdDuration);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        private DataTable GetLabelPrintingDetailsTable(IList<LabelPrintingData> labelPrintingData)
        {
            DataTable objDataHeaderTable = new DataTable("Sheet1");

            objDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Product Name", DataType = typeof(string) });
            objDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Scanned Date", DataType = typeof(string) });
            objDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Scanned Time", DataType = typeof(string) });
            objDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Permit Day", DataType = typeof(string) });
            objDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Permit Hours", DataType = typeof(string) });
            objDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Expiry Date", DataType = typeof(string) });
            objDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Expiry Time", DataType = typeof(string) });
            objDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Label Count", DataType = typeof(int) });
           

            foreach (LabelPrintingData obj in labelPrintingData)
            {
                DataRow drObj = objDataHeaderTable.NewRow();
                drObj["Product Name"] = obj?.ProductName;
                drObj["Scanned Date"] = obj?.ScannedDate;
                drObj["Scanned Time"] = obj?.ScannedTime;
                drObj["Permit Day"] = obj?.PermitDay;
                drObj["Permit Hours"] = obj?.PermitHour;
                drObj["Expiry Date"] = obj?.ExpiryDate;
                drObj["Expiry Time"] = obj?.ExpiryTime;
                drObj["Label Count"] = obj?.LabelCount;
                objDataHeaderTable.Rows.Add(drObj);
            }
            return objDataHeaderTable;
        }

    }
}