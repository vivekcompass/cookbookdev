﻿using System;
using System.Web.Mvc;
using System.Web.UI;
using CookBook.Data.MasterData;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.Models;

namespace CookBook.MvcApp.Controllers
{
    public class DishController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        public ActionResult DishTemperatureMapping()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        public ActionResult RegionalRecipeMap()
        {
            // return View(GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult SiteRecipeMap()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetDishDataList()
        {
            var dishList = ServiceClient.GetDishDataList(null, null, GetRequestData());
            var result = Json(dishList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        public ActionResult GetRegionDishDataList(int RegionID, string SubSectorCode)
        {
            var dishList = ServiceClient.GetRegionDishDataList(RegionID, null, null, SubSectorCode, GetRequestData());
            var result = Json(dishList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        public ActionResult GetSiteDishDataList(string siteCode)
        {
            var dishList = ServiceClient.GetSiteDishDataList(siteCode, null, null, GetRequestData());
            var result = Json(dishList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [CustAuthFilter]
        public ActionResult GetCuisineDataList()
        {
            var dishList = ServiceClient.GetCuisineDataList(GetRequestData());
            var result = Json(dishList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [CustAuthFilter]
        public ActionResult GetDishRecipeMappingDataList(string DishCode)
        {
            var dishRecipeMappingList = ServiceClient.GetDishRecipeMappingDataList(DishCode, GetRequestData());
            var jsonResult = Json(dishRecipeMappingList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetRegionDishRecipeMappingDataList(int RegionID, string DishCode, string SubSectorCode)
        {
            var regionDishRecipeMappingList = ServiceClient.GetRegionDishRecipeMappingDataList(RegionID, DishCode, SubSectorCode, GetRequestData());
            var jsonResult = Json(regionDishRecipeMappingList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteDishRecipeMappingDataList(string SiteCode, string DishCode)
        {


            var siteDishRecipeMappingList = ServiceClient.GetSiteDishRecipeMappingDataList(SiteCode, DishCode, GetRequestData());
            var jsonResult = Json(siteDishRecipeMappingList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveDishData(DishData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            if (model.ID == 0 || model.ID == null)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
                model.Status = (int)StatusEnum.Pending;
            }
            else
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }

            var result = ServiceClient.SaveDishData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult SaveDishRecipeMappingList(DishRecipeMappingData[] model)
        {

            var result = ServiceClient.SaveDishRecipeMappingList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveRegionDishRecipeMappingList(RegionDishRecipeMappingData[] model)
        {


            var result = ServiceClient.SaveRegionDishRecipeMappingList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveSiteDishRecipeMappingList(SiteDishRecipeMappingData[] model)
        {


            var result = ServiceClient.SaveSiteDishRecipeMappingList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveDishDataList(DishData[] model)
        {
            var result = ServiceClient.SaveDishDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetDishCategorySiblingDataList()
        {
            var dishList = ServiceClient.GetDishCategorySiblingDataList(GetRequestData());
            var result = Json(dishList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        public ActionResult GetRecipeDishDataByDish(string dishCode)
        {
            var recipeList = ServiceClient.GetRecipeDishDataByDish(dishCode, GetRequestData());
            var result = Json(recipeList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        //Krish 28-07-2022
        [CustAuthFilter]
        public ActionResult GetSectorDishDataList(string dishCode = null, string condition = null, bool isAddEdit = false, bool onLoad = false)
        {
            var requestData = GetRequestData();
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            requestData.DishCode = dishCode;
            requestData.SessionUserRoleId = userData.UserRoleId;
            //requestData.RegionID = regionID;
            //requestData.ItemCode = itemCode;

            var dataList = ServiceClient.GetSectorDishData(requestData, condition, isAddEdit, onLoad);
            var jsonResult = Json(dataList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult SaveRangeTypeDishData(string dishCode, string rangeTypeCode)
        {
            var result = ServiceClient.SaveRangeTypeDishData(dishCode, rangeTypeCode, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
