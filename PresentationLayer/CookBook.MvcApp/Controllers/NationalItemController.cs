﻿using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.Models;
using Google.Apis.Upload;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp.Controllers
{
    public class NationalItemController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult UnitPrice()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult SectorMenu()
        {
            return View(GetRequestData());
        }
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult SiteMenu()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult RegionMenu()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetDishDataList()
        {
            var dishList = ServiceClient.GetDishDataList(null, null, GetRequestData());
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteDishDataList(string siteCode)
        {
            var dishList = ServiceClient.GetSiteDishDataList(siteCode, null, null, GetRequestData());
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetUnitPriceDataList(string siteCode)
        {
            var dishList = ServiceClient.GetSiteUnitPrice(siteCode, GetRequestData());
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteItemPriceHistory(string siteCode, string itemCode)
        {
            var dishList = ServiceClient.GetSiteItemPriceHistory(siteCode, itemCode, GetRequestData());
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetRegionDishMasterData(int regionID)
        {
            var dishList = ServiceClient.GetRegionDishMasterData(regionID, null, null, GetRequestData());
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetItemDataList()
        {
            var itemList = ServiceClient.NationalGetItemDataList("", null, null, ((UserMasterResponseData)Session["UserLoginDetails"]).SectorName, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetDishImpactedItemList(string dishCode)
        {
            var itemList = ServiceClient.GetDishImpactedItemList(dishCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetItemDishMappingDataList(string itemID)
        {
            var itemList = ServiceClient.GetItemDishMappingDataList(itemID, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetItemDishCategoryMappingDataList(string itemCode)
        {
            var itemList = ServiceClient.GetItemDishCategoryMappingDataList(itemCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetNationalItemDishCategoryMappingDataList(string itemCode,string sectorcode)
        {
            var itemList = ServiceClient.GetNationalItemDishCategoryMappingDataList(itemCode,  sectorcode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetRegionDishCategoryMappingDataList(string regionID, string itemCode)
        {

            var itemList = ServiceClient.GetRegionDishCategoryMappingDataList(regionID, itemCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteDishCategoryMappingDataList(string siteCode, string itemCode)
        {
            var itemList = ServiceClient.GetSiteDishCategoryMappingDataList(siteCode, itemCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionItemDishMappingDataList(string itemID, string regionID)
        {
            var itemList = ServiceClient.GetRegionItemDishMappingDataList(itemID, regionID, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteItemDayPartMappingDataList(string itemCode, string siteCode)
        {
            var itemList = ServiceClient.GetSiteItemDayPartMappingDataList(itemCode, siteCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteDayPartMappingDataList(string siteCode)
        {
            var itemList = ServiceClient.GetSiteDayPartMappingDataList(siteCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetFoodProgramDayPartMappingDataList(string foodCode)
        {
            var itemList = ServiceClient.GetFoodProgramDayPartMappingDataList(foodCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteItemDishMappingDataList(string itemCode, string siteCode)
        {
            var itemList = ServiceClient.GetSiteItemDishMappingDataList(itemCode, siteCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetMasterRegionItemDishMappingDataList(string regionID)
        {
            var itemList = ServiceClient.GetMasterRegionItemDishMappingDataList(regionID, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetRegionItemInheritanceMappingDataList(string regionID)
        {
            var itemList = ServiceClient.GetRegionItemInheritanceMappingDataList(regionID, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteItemInheritanceMappingDataList(string SiteCode)
        {
            var itemList = ServiceClient.GetSiteItemInheritanceMappingDataList(SiteCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetMasterSiteItemDishMappingDataList(string siteCode)
        {
            var itemList = ServiceClient.GetMasterSiteItemDishMappingDataList(siteCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetDishCategoryList()
        {
            var dishCategoryList = ServiceClient.GetDishCategoryDataList(GetRequestData());
            var jsonResult = Json(dishCategoryList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetReasonList()
        {
            var reasonList = ServiceClient.GetReasonDataList(GetRequestData());
            var jsonResult = Json(reasonList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSubSectorMasterList(bool isAllSubSector)
        {
            var subsectorList = ServiceClient.GetSubSectorDataList(GetRequestData());
            if (!isAllSubSector)
            {
                if (subsectorList != null && subsectorList.Count() > 0)
                {
                    subsectorList = subsectorList.Where(s => s.SubSectorCode != "SSR-001").ToArray();
                }
            }
            var jsonResult = Json(subsectorList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteMasterList()
        {
            var siteList = ServiceClient.GetSiteMasterDataList(GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionMasterList()
        {
            var regionList = ServiceClient.GetRegionMasterDataList(GetRequestData());
            var jsonResult = Json(regionList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionMasterListByUserId()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (userData.IsSectorUser)
            {
                var regionList = ServiceClient.GetRegionMasterDataList(GetRequestData());
                var jsonResult = Json(regionList, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            else
            {
                var regionList = ServiceClient.GetRegionsDetailsByUserIdDataList(GetRequestData());
                var jsonResult = Json(regionList, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }

        }

        [CustAuthFilter]
        public ActionResult GetUniqueMappedItemCodesFromItem()
        {
            var siteList = ServiceClient.GetUniqueMappedItemCodesFromItem(GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetUniqueMappedItemCodesFromRegion(string RegionID)
        {
            var siteList = ServiceClient.GetUniqueMappedItemCodesFromRegion(RegionID, GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionInheritedData(string RegionID, string SectorID)
        {
            var siteList = ServiceClient.GetRegionInheritedData(RegionID, SectorID, GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveItemData(ItemData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (model.ID > 0)
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }
            else
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
                model.IsActive = true;
                var status = (int)StatusEnum.Pending;
                model.Status = status.ToString();
            }

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                var fileext = Path.GetExtension(file.FileName);
                model.ImageName = model.ItemCode + fileext;
            }
            var result = ServiceClient.SaveItemData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveItemDataList(ItemData[] model)
        {
            var result = ServiceClient.SaveItemDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveItemDishMappingDataList(ItemDishMappingData[] model)
        {
            var result = ServiceClient.SaveItemDishMappingDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveRegionItemDishMappingDataList(RegionItemDishMappingData[] model)
        {
            var isCostUpdate = false;
            var requestData = GetRequestData();
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.SaveRegionItemDishMappingDataList(model, GetRequestData());

            return Json(new { result = result, isCostUpdate = isCostUpdate }, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveSiteItemDishMappingDataList(SiteItemDishMappingData[] model)
        {
            var isCostUpdate = false;
            var requestData = GetRequestData();
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.SaveSiteItemDishMappingDataList(model, GetRequestData());

            return Json(new { result = result, isCostUpdate = isCostUpdate }, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveSiteItemDayPartMappingDataList(SiteItemDayPartMappingData[] model)
        {


            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.SaveSiteItemDayPartMappingDataList(model, GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveSiteDayPartMappingDataList(SiteDayPartMappingData[] model)
        {


            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.SaveSiteDayPartMappingDataList(model, false,GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveFoodProgramDayPartMappingDataList(FoodProgramDayPartMappingData[] model)
        {


            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.SaveFoodProgramDayPartMappingDataList(model, GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveItemDishCategoryMappingDataList(ItemDishCategoryMappingData[] model)
        {
            var result = ServiceClient.SaveItemDishCategoryMappingDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveRegionDishCategoryMappingDataList(RegionDishCategoryMappingData[] model)
        {
            var result = ServiceClient.SaveRegionDishCategoryMappingDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveSectorToRegionInheritance(RegionItemInheritanceMappingData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.CreatedBy = userData.UserId;
            var requestData = GetRequestData();
            var result = ServiceClient.SaveSectorToRegionInheritance(model, requestData);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public async Task<string> UpdateCostAtRegion(RegionItemInheritanceMappingData model, RequestData requestData)
        {
            return ServiceClient.UpdateCostSectorToRegionInheritance(model, requestData);
        }


        [CustAuthFilter]
        public ActionResult SaveRegionToSiteInheritance(SiteItemInheritanceMappingData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.CreatedBy = userData.UserId;
            var requestData = GetRequestData();
            var result = ServiceClient.SaveRegionToSiteInheritance(model, requestData);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveToSiteInheritance(SiteItemInheritanceMappingData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.ModifiedBy = userData.UserId;
            model.ModifiedOn = DateTime.Now;
            var requestData = GetRequestData();
            var result = ServiceClient.SaveToSiteInheritance(model, requestData);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveToSiteInheritanceDataList(SiteItemInheritanceMappingData[] model)
        {
            var requestData = GetRequestData();
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            foreach (var m in model)
            {
                m.ModifiedBy = userData.UserId;
                m.ModifiedOn = DateTime.Now;
            }
            var result = ServiceClient.SaveToSiteInheritanceDataList(model, requestData);

            return Json(result, JsonRequestBehavior.AllowGet);
        }



        public async Task<string> UpdateCostAtSite(SiteItemInheritanceMappingData model, RequestData requestData)
        {
            return ServiceClient.UpdateCostRegionToSiteInheritance(model, requestData);
        }

        [CustAuthFilter]
        public ActionResult SaveSiteDishCategoryMappingDataList(SiteDishCategoryMappingData[] model)
        {
            var result = ServiceClient.SaveSiteDishCategoryMappingDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [HttpPost]
        public JsonResult UploadFile()
        {
            IList<string> filePathList = new List<string>();
            if (Request.Files.Count > 0)
            {
                string itemCode = Request.Form[0].ToString().Split(',')[0];
                string folderName = Request.Form[1].ToString().Split(',')[0];
                string folderPath = folderName;

                string bucketName = "compass-shield-production";
                string jsonFilePath = Server.MapPath("~/Content/JSON/GCPSettings.json");
                JObject settings = JObject.Parse(System.IO.File.ReadAllText(jsonFilePath));

                string jsonSettings = settings.ToString();
                string projectid = Convert.ToString(settings["project_id"]);
                var credentials = Google.Apis.Auth.OAuth2.GoogleCredential.FromJson(settings.ToString());

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    var fileName = Path.GetFileName(file.FileName);
                    var fileext = Path.GetExtension(file.FileName);
                    var directoryPath = Path.Combine(Server.MapPath("~/" + folderPath + "/ "));
                    bool exists = System.IO.Directory.Exists(directoryPath);
                    string filePath = Path.Combine(Server.MapPath("~/" + folderPath + "/ "), itemCode + fileext);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(directoryPath);
                    file.SaveAs(filePath);
                    string contentType = string.Empty;
                    string extension = Path.GetExtension(filePath);

                    if (extension == ".png")
                    {
                        contentType = "image/png";
                    }
                    else if (extension == ".jpeg")
                    {
                        contentType = "image/jpeg";
                    }
                    else if (extension == ".jpg")
                    {
                        contentType = "image/jpg";
                    }

                    var newObject = new Google.Apis.Storage.v1.Data.Object
                    {
                        Bucket = bucketName,
                        Name = "/" + folderPath + "/" + System.IO.Path.GetFileNameWithoutExtension(filePath),
                        ContentType = contentType
                    };

                    try
                    {
                        bool IsSuccess = false;
                        var prj = new Progress<Google.Apis.Upload.IUploadProgress>();
                        using (var storageClient = Google.Cloud.Storage.V1.StorageClient.Create(credentials))
                        {
                            prj.ProgressChanged += (e, c) =>
                            {
                                if (c.Status == UploadStatus.Completed)
                                {
                                    IsSuccess = true;
                                }
                            };
                            using (var fileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Open))
                            {
                                storageClient.UploadObject(
                                           newObject,
                                           fileStream, null, progress: prj);
                                IsSuccess = true;
                            }

                            if (IsSuccess)
                            {
                                filePathList.Add(newObject.Bucket + "/" + newObject.Name);
                                //mssg = true;
                                //pendingdet.Add(filename);
                                //directoryPath
                                System.IO.DirectoryInfo di = new DirectoryInfo(directoryPath);

                                //foreach (FileInfo deleteFile in di.GetFiles())
                                //{
                                //    deleteFile.Delete(GetRequestData());
                                //}
                            }
                            prj.ProgressChanged -= (e, c) => { };
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.TargetSite.MetadataToken == 100663833)
                        {
                            //pendingdet.Add(filename);
                            //mssg = true;
                            //AppLogger.LogCriticalLogs("UploadGCbackupdata =>  " + ex.Data + "," + ex.Message);
                        }
                        else
                        {
                            //AppLogger.LogCriticalLogs("UploadGCbackupdata =>  " + ex.Message);
                        }
                    }
                }

                return new JsonResult()
                {
                    Data = filePathList,
                    MaxJsonLength = Convert.ToInt32(ConfigurationManager.AppSettings["MaxJsonLength"]),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Json(new { message = "" });
            }
        }


    }
}