﻿using System;
using System.Web.Mvc;
using CookBook.Data.MasterData;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.ServiceApp;

namespace CookBook.MvcApp.Controllers
{
    public class CafeController : BaseController
    {
        private CookBookService.ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            //return View();
            return PartialView();
        }

        [CustAuthFilter]
        public ActionResult GetCafeDataList()
        {
            var cafeList = ServiceClient.GetCafeDataList(GetRequestData());
            var jsonResult = Json(cafeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveCafeData(CafeData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }
            var result = ServiceClient.SaveCafeData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult ChangeStatus(CafeData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.ModifiedBy = userData.UserId;
            model.ModifiedOn = DateTime.Now;
            var result = ServiceClient.SaveCafeData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveCafeDataList(CafeData[] model)
        {
            var result = ServiceClient.SaveCafeDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}