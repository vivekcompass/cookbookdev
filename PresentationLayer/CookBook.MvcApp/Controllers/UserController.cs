﻿using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using HSEQ.Business.Contract;
using HSEQ.Data.User;

namespace HSEQ.MvcApp.Controllers
{
    //[Authorize]
    public class UserController : BaseController
    {
        [Dependency]
        public IUser UserManager { get; set; }
        private IList<UserData> userList = new List<UserData>();

        [AllowAnonymous]
        // GET: User
        public ActionResult Index()
        {
            userList = UserManager.GetUsers();
            return View(userList);
        }

        // GET: User
        //[CustomAuthentication]
        public ActionResult AddModifyUserDetail(int userID)
        {
            UserData userData = UserManager.GetUser(userID);
            return View(userData);
        }

        //public PartialViewResult UserAddressDetail()
        //{
        //    UserAddressData userAddressData = new UserAddressData();

        //    return PartialView("~/Views/User/_UserAddressDetail.cshtml", userAddressData);
        //}

        //public PartialViewResult UserMasterDetail()
        //{
        //    UserData userData = new UserData();

        //    return PartialView("~/Views/User/_UserMasterDetail.cshtml", userData);
        //}

        public ActionResult SaveUserDetail(UserData userData)
        {
            bool isSuccess = UserManager.SaveUser(userData);
            return Json(isSuccess, JsonRequestBehavior.AllowGet);
        }
    }
}