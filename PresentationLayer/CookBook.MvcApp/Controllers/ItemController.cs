﻿using ClosedXML.Excel;
using CookBook.Aspects.Factory;
using CookBook.Data;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CookBook.Data.MasterData;
using Newtonsoft.Json.Linq;
using Google.Apis.Upload;
using Microsoft.AspNet.Identity;
using CookBook.Aspects.Utils;
using System.Web.UI;
using System.Threading.Tasks;
using System.Reflection;
using System.Web.UI.WebControls;

namespace CookBook.MvcApp.Controllers
{
    public class ItemController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult UnitPrice()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult ServiceTypeClosingTime()
        {
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult SectorMenu()
        {
            return View(GetRequestData());
        }
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult SiteMenu()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult RegionMenu()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetDishDataList(string SiteCode)
        {
            var requestData = GetRequestData();
            requestData.SiteCode = SiteCode;
            var dishList = ServiceClient.GetDishDataList(null, null, requestData);
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteDishDataList(string siteCode)
        {
            var dishList = ServiceClient.GetSiteDishDataList(siteCode, null, null,GetRequestData());
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetUnitPriceDataList(string siteCode)
        {
            var dishList = ServiceClient.GetSiteUnitPrice(siteCode, GetRequestData());
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteDayPartClossingMappingDataList(string siteCode)
        {
            var dishList = ServiceClient.GetSiteDayPartMapping(siteCode, GetRequestData());
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteItemPriceHistory(string siteCode,string itemCode)
        {
            var dishList = ServiceClient.GetSiteItemPriceHistory(siteCode,itemCode, GetRequestData());
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetRegionDishMasterData(int regionID)
        {
            var dishList = ServiceClient.GetRegionDishMasterData(regionID, null, null,GetRequestData());
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetItemDataList(string SiteCode)
        {
            var requestData = GetRequestData();
            requestData.SiteCode = SiteCode;
            var itemList = ServiceClient.GetItemDataList("", null, null, ((UserMasterResponseData)Session["UserLoginDetails"]).SectorName,requestData);
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetDishImpactedItemList(string dishCode)
        {
            var itemList = ServiceClient.GetDishImpactedItemList(dishCode,GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetItemDishMappingDataList(string itemID)
        {
            var itemList = ServiceClient.GetItemDishMappingDataList(itemID,GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetItemDishCategoryMappingDataList(string itemCode)
        {
            var itemList = ServiceClient.GetItemDishCategoryMappingDataList(itemCode,GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetRegionDishCategoryMappingDataList(string regionID,string itemCode)
        {
           
            var itemList = ServiceClient.GetRegionDishCategoryMappingDataList(regionID, itemCode,GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteDishCategoryMappingDataList(string siteCode, string itemCode)
        {
            var itemList = ServiceClient.GetSiteDishCategoryMappingDataList(siteCode, itemCode,GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionItemDishMappingDataList(string itemID, string regionID, string subSectorCode)
        {
            RequestData requestData = GetRequestData();
            requestData.SubSectorCode = subSectorCode;
            var itemList = ServiceClient.GetRegionItemDishMappingDataList(itemID, regionID, requestData);
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteItemDayPartMappingDataList(string itemCode, string siteCode)
        {
            var itemList = ServiceClient.GetSiteItemDayPartMappingDataList(itemCode, siteCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteDayPartMappingDataList( string siteCode)
        {
            var itemList = ServiceClient.GetSiteDayPartMappingDataList( siteCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetFoodProgramDayPartMappingDataList(string foodCode)
        {
            var itemList = ServiceClient.GetFoodProgramDayPartMappingDataList(foodCode, GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteItemDishMappingDataList(string itemCode, string siteCode)
        {
            var itemList = ServiceClient.GetSiteItemDishMappingDataList(itemCode, siteCode,GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetMasterRegionItemDishMappingDataList(string regionID)
        {
            var itemList = ServiceClient.GetMasterRegionItemDishMappingDataList(regionID,GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetRegionItemInheritanceMappingDataList(string regionID, string SiteCode)
        {
            var requestData = GetRequestData();
            requestData.SiteCode = SiteCode;
            var itemList = ServiceClient.GetRegionItemInheritanceMappingDataList(regionID, requestData);
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteItemInheritanceMappingDataList(string SiteCode)
        {
            var itemList = ServiceClient.GetSiteItemInheritanceMappingDataList(SiteCode,GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetMasterSiteItemDishMappingDataList(string siteCode)
        {
            var itemList = ServiceClient.GetMasterSiteItemDishMappingDataList(siteCode,GetRequestData());
            var jsonResult = Json(itemList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetDishCategoryList(string SiteCode)
        {
            var requestData = GetRequestData();
            requestData.SiteCode = SiteCode;
            var dishCategoryList = ServiceClient.GetDishCategoryDataList(requestData);
            var jsonResult = Json(dishCategoryList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }


        [CustAuthFilter]
        public ActionResult GetReasonList()
        {
            var reasonList = ServiceClient.GetReasonDataList(GetRequestData());
            var jsonResult = Json(reasonList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSubSectorMasterList(bool isAllSubSector)
        {
            var subsectorList = ServiceClient.GetSubSectorDataList(GetRequestData());
            if (!isAllSubSector)
            {
                if (subsectorList != null && subsectorList.Count() > 0)
                {
                    subsectorList = subsectorList.Where(s => s.SubSectorCode != "SSR-001").ToArray();
                }
            }
            var jsonResult = Json(subsectorList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteMasterList()
        {
            var siteList = ServiceClient.GetSiteMasterDataList(GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionMasterList()
        {
            var regionList = ServiceClient.GetRegionMasterDataList(GetRequestData());
            var jsonResult = Json(regionList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionMasterListByUserId()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (userData.IsSectorUser)
            {
                var regionList = ServiceClient.GetRegionMasterDataList(GetRequestData());
                var jsonResult = Json(regionList, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            else
            {
                var regionList = ServiceClient.GetRegionsDetailsByUserIdDataList(GetRequestData());
                var jsonResult = Json(regionList, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            
        }

        [CustAuthFilter]
        public ActionResult GetUniqueMappedItemCodesFromItem()
        {
            var siteList = ServiceClient.GetUniqueMappedItemCodesFromItem(GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetUniqueMappedItemCodesFromRegion(string RegionID, string SiteCode)
        {
            {
                var requestData = GetRequestData();
                requestData.SiteCode = SiteCode;
                var siteList = ServiceClient.GetUniqueMappedItemCodesFromRegion(RegionID, requestData);
                var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
        }

        [CustAuthFilter]
        public ActionResult GetRegionInheritedData(string RegionID, string SectorID )
        {
            var siteList = ServiceClient.GetRegionInheritedData(RegionID,SectorID,GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveItemData(ItemData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if(model.ID >0)
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }
            else
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
                model.IsActive = true;
                var status = (int)StatusEnum.Pending;
                model.Status = status.ToString();
            }
           
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                var fileext = Path.GetExtension(file.FileName);
                model.ImageName = model.ItemCode + fileext;
            }
            var result = ServiceClient.SaveItemData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveItemDataList(ItemData[] model)
        {
            var result = ServiceClient.SaveItemDataList(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveItemDishMappingDataList(ItemDishMappingData[] model)
        {
            var result = ServiceClient.SaveItemDishMappingDataList(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveRegionItemDishMappingDataList(RegionItemDishMappingData[] model, string subSectorCode)
        {
            var isCostUpdate = false;
            var requestData = GetRequestData();
            requestData.SubSectorCode = model[0].SubSectorCode;
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.SaveRegionItemDishMappingDataList(model, requestData);
            
            return Json(new { result = result, isCostUpdate = isCostUpdate }, JsonRequestBehavior.AllowGet);
        }

        //add by gaurav
        [CustAuthFilter]
        public ActionResult UpdateRegionDishCategoryInactiveUpdate(RegionDishCategoryMappingData[] model)
        {
          
            var requestData = GetRequestData();
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            string result = ServiceClient.mydishcateinactiveupdate(model, GetRequestData());

            return Json(new { result = result}, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult SaveSiteItemDishMappingDataList(SiteItemDishMappingData[] model)
        {
            var isCostUpdate = false;
            var requestData = GetRequestData();
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.SaveSiteItemDishMappingDataList(model,GetRequestData());
            
            return Json(new { result = result, isCostUpdate = isCostUpdate }, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveSiteItemDayPartMappingDataList(SiteItemDayPartMappingData[] model)
        {
           
           
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.SaveSiteItemDayPartMappingDataList(model, GetRequestData());

            return Json( result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveSiteDayPartMappingDataList(SiteDayPartMappingData[] model,bool isClosingTime)
        {


            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.SaveSiteDayPartMappingDataList(model, isClosingTime,GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveFoodProgramDayPartMappingDataList(FoodProgramDayPartMappingData[] model)
        {


            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var result = ServiceClient.SaveFoodProgramDayPartMappingDataList(model, GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveItemDishCategoryMappingDataList(ItemDishCategoryMappingData[] model)
        {
            var result = ServiceClient.SaveItemDishCategoryMappingDataList(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveRegionDishCategoryMappingDataList(RegionDishCategoryMappingData[] model)
        {
            var result = ServiceClient.SaveRegionDishCategoryMappingDataList(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveSectorToRegionInheritance(RegionItemInheritanceMappingData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.CreatedBy = userData.UserId;
            var requestData = GetRequestData();
            var result = ServiceClient.SaveSectorToRegionInheritance(model,requestData);
            return Json(result, JsonRequestBehavior.AllowGet);
            
        }

        [CustAuthFilter]
        public ActionResult SaveMOGNationalToSectorInheritance(SaveMOGNationalToSectorInheritanceData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.CreatedBy = userData.UserId;
            var requestData = GetRequestData();
            var result = ServiceClient.SaveMOGNationalToSectorInheritance(model, requestData);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        public async Task<string>  UpdateCostAtRegion(RegionItemInheritanceMappingData model, RequestData requestData)
        {
            return ServiceClient.UpdateCostSectorToRegionInheritance(model,requestData);
        }


        [CustAuthFilter]
        public ActionResult SaveRegionToSiteInheritance(SiteItemInheritanceMappingData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.CreatedBy = userData.UserId;
            var requestData = GetRequestData();
            var result = ServiceClient.SaveRegionToSiteInheritance(model, requestData);            
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveToSiteInheritance(SiteItemInheritanceMappingData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.ModifiedBy = userData.UserId;
            model.ModifiedOn = DateTime.Now;
            var requestData = GetRequestData();
            var result = ServiceClient.SaveToSiteInheritance(model, requestData);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveToSiteInheritanceDataList(SiteItemInheritanceMappingData[] model)
        {
            var requestData = GetRequestData();
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            foreach (var m in model)
            {
                m.ModifiedBy = userData.UserId;
                m.ModifiedOn = DateTime.Now;
            }
            var result = ServiceClient.SaveToSiteInheritanceDataList(model, requestData);

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public async Task<string> UpdateCostAtSite(SiteItemInheritanceMappingData model, RequestData requestData)
        {
            return ServiceClient.UpdateCostRegionToSiteInheritance(model, requestData);
        }

        [CustAuthFilter]
        public ActionResult SaveSiteDishCategoryMappingDataList(SiteDishCategoryMappingData[] model)
        {
            var result = ServiceClient.SaveSiteDishCategoryMappingDataList(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [HttpPost]
        public JsonResult UploadFile()
        {
            IList<string> filePathList = new List<string>();
            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    var fileext = Path.GetExtension(file.FileName);
                    string contentType = string.Empty;
                   

                    var fileExtension = Path.GetExtension(file.FileName).ToLowerInvariant();
                    var allowedExtensions = new[] { ".jpeg", ".jpg", ".png" };

                    if (!allowedExtensions.Contains(fileExtension))
                    {
                        return Json(new { fileupload = false, message = "Invalid file type. Only JPEG, JPG, and PNG files are allowed." });
                    }

                    // Optionally, check the MIME type
                    if (fileext == ".png")
                    {
                        contentType = "image/png";
                    }
                    else if (fileext == ".jpeg")
                    {
                        contentType = "image/jpeg";
                    }
                    else if (fileext == ".jpg")
                    {
                        contentType = "image/jpg";
                    }
                    var allowedMimeTypes = new[] { "image/jpeg", "image/png", "image/jpg" };

                    if (!allowedMimeTypes.Contains(contentType))
                    {
                        return Json(new { fileupload = false, message = "Invalid file type. Only JPEG and PNG files are allowed." });
                    }
                }

                string itemCode = Request.Form[0].ToString().Split(',')[0];
                string folderName = Request.Form[1].ToString().Split(',')[0];
                string folderPath = folderName;

                string bucketName = "compass-shield-production";
                string jsonFilePath = Server.MapPath("~/Content/JSON/GCPSettings.json");
                JObject settings = JObject.Parse(System.IO.File.ReadAllText(jsonFilePath));

                string jsonSettings = settings.ToString();
                string projectid = Convert.ToString(settings["project_id"]);
                var credentials = Google.Apis.Auth.OAuth2.GoogleCredential.FromJson(settings.ToString());

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    var fileName = Path.GetFileName(file.FileName);
                    var fileext = Path.GetExtension(file.FileName);
                    var directoryPath = Path.Combine(Server.MapPath("~/" + folderPath + "/ "));
                    bool exists = System.IO.Directory.Exists(directoryPath);
                    string filePath = Path.Combine(Server.MapPath("~/" + folderPath + "/ "), itemCode + fileext);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(directoryPath);
                    file.SaveAs(filePath);
                    string contentType = string.Empty;
                    string extension = Path.GetExtension(filePath);

                    if (extension == ".png")
                    {
                        contentType = "image/png";
                    }
                    else if (extension == ".jpeg")
                    {
                        contentType = "image/jpeg";
                    }
                    else if (extension == ".jpg")
                    {
                        contentType = "image/jpg";
                    }
                    
                    var newObject = new Google.Apis.Storage.v1.Data.Object
                    {
                        Bucket = bucketName,
                        Name = "/" + folderPath + "/" + System.IO.Path.GetFileNameWithoutExtension(filePath),
                        ContentType = contentType
                    };

                    try
                    {
                        bool IsSuccess = false;
                        var prj = new Progress<Google.Apis.Upload.IUploadProgress>();
                        using (var storageClient = Google.Cloud.Storage.V1.StorageClient.Create(credentials))
                        {
                            prj.ProgressChanged += (e, c) =>
                            {
                                if (c.Status == UploadStatus.Completed)
                                {
                                    IsSuccess = true;
                                }
                            };
                            using (var fileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Open))
                            {
                                storageClient.UploadObject(
                                           newObject,
                                           fileStream, null, progress: prj);
                                IsSuccess = true;
                            }

                            if (IsSuccess)
                            {
                                filePathList.Add(newObject.Bucket + "/" + newObject.Name);
                                System.IO.DirectoryInfo di = new DirectoryInfo(directoryPath);
                            }
                            prj.ProgressChanged -= (e, c) => { };
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.TargetSite.MetadataToken == 100663833)
                        {
                        }
                        else
                        {
                        }
                    }
                }

                return new JsonResult()
                {
                    Data = filePathList,
                    MaxJsonLength = Convert.ToInt32(ConfigurationManager.AppSettings["MaxJsonLength"]),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Json(new { message = "" });
            }
        }


        [CustAuthFilter]
        public ActionResult ExportItemData()
        {
            var itemList = ServiceClient.GetItemExportDataList(GetRequestData());
            FileContentResult rObj;
            using (XLWorkbook wb = new XLWorkbook())
            {
                IXLWorksheet workSheet = wb.AddWorksheet("ITILite Data");
                workSheet.ShowGridLines = false;
                int rowCount = 1;

                DataTable dataTableMenuRequisitionDetails = GetDetailsTable(itemList.ToList());

                wb.Worksheet(1).Cell(rowCount, 1).InsertTable(dataTableMenuRequisitionDetails);
                int tableRowCount = dataTableMenuRequisitionDetails.Rows.Count;
                rowCount = rowCount + 2;

                workSheet.Rows().AdjustToContents();
                workSheet.Columns().AdjustToContents();
                workSheet.Columns().Style.Alignment.Vertical = XLAlignmentVerticalValues.Justify;
                foreach (var item in workSheet.Tables)
                {
                    item.ShowAutoFilter = false;
                }

                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    // var bytesdata = File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ItemData.xls");
                    var bytesdata = File(stream.ToArray(), "application/vnd.ms-excel", "ItemData.xls");
                    rObj = bytesdata;
                }
            }
            var jsonResult = Json(rObj, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
            // return Json(rObj, JsonRequestBehavior.AllowGet);
        }

        //public void ExportItemData()
        //{
        //    JavaScriptSerializer obj = new JavaScriptSerializer();
        //    var itemList = ServiceClient.GetItemExportDataList(GetRequestData());

        //    var grid = new GridView();
        //    grid.DataSource = itemList.Select(e => new
        //    {
        //        ItemName = e.ItemName,
        //        ItemCode = e.ItemCode

        //    });
        //    grid.DataBind();

        //    Response.ClearContent();
        //    Response.Buffer = true;
        //    Response.AddHeader("content-disposition", "attachment; filename=RegionalReport.xls");
        //    Response.ContentType = "application/ms-excel";

        //    Response.Charset = "";
        //    StringWriter sw = new StringWriter();
        //    HtmlTextWriter htw = new HtmlTextWriter(sw);

        //    grid.RenderControl(htw);

        //    Response.Output.Write(sw.ToString());
        //    Response.Flush();
        //    Response.End();
        //    //return Json(true, JsonRequestBehavior.AllowGet);
        //}

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        private DataTable GetDetailsTable(IList<ItemExportData> itemData)
        {
            DataTable itiLiteDataHeaderTable = new DataTable("ItemDetails");

            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "ItemCode", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Item", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Diet Category", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Visual Cateogry", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Food Program", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Dish Category", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Dish", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Dish Type", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "HSN Code", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "CGST", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "SGST", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "CESS", DataType = typeof(string) });
           
            foreach (ItemExportData item in itemData)
            {
                DataRow drItem = itiLiteDataHeaderTable.NewRow();
                drItem["ItemCode"] = item.ItemCode;
                drItem["Item"] = item.ItemName;
                drItem["Diet Category"] = item.DietCategory;
                drItem["Visual Cateogry"] = item.VisualCateogry;
                drItem["Food Program"] = item.FoodProgram;
                drItem["Dish Category"] = item.DishCategory;
                drItem["Dish"] = item.DishName;
                drItem["HSN Code"] = item.HSNCode;
                drItem["CGST"] = item.CGST;
                drItem["SGST"] = item.SGST;
                drItem["CESS"] = item.CESS;
                itiLiteDataHeaderTable.Rows.Add(drItem);
            }
            return itiLiteDataHeaderTable;
        }

        [CustAuthFilter]
        public ActionResult GetInheritChangesSectorToRegion(ItemInheritChangeRequestData model)
        {
            var changeList = ServiceClient.GetInheritChangesSectorToRegion(model,  GetRequestData());
            var jsonResult = Json(changeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }


        [CustAuthFilter]
        public ActionResult GetInheritChangesRegionToSite(ItemInheritChangeRequestData model)
        {
            var changeList = ServiceClient.GetInheritChangesRegionToSite(model, GetRequestData());
            var jsonResult = Json(changeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult SaveChangesSectorToRegion(InheritanceChangesData[] model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var requestData = GetRequestData();
            var result = ServiceClient.SaveChangesSectorToRegion(model, requestData);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveChangesRegionToSite(InheritanceChangesData[] model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var requestData = GetRequestData();
            var result = ServiceClient.SaveChangesRegionToSite(model, requestData);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetSiteItemDropDownDataData(string siteCode, int regionID,string itemCode)
        {
            var requestData = GetRequestData();
            requestData.SiteCode = siteCode;
            requestData.RegionID = regionID;
            requestData.ItemCode = itemCode;

            var dataList = ServiceClient.GetSiteItemDropDownDataData(requestData);
            var jsonResult = Json(dataList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        //Krish 28-07-2022
        [CustAuthFilter]
        public ActionResult GetSectorItemDataList(string siteCode=null,bool isAddEdit=false, bool onLoad=false)
        {
            var requestData = GetRequestData();
            requestData.SiteCode = siteCode;
            //requestData.RegionID = regionID;
            //requestData.ItemCode = itemCode;

            var dataList = ServiceClient.GetSectorItemData(requestData,isAddEdit,onLoad);
            var jsonResult = Json(dataList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        //Krish 30-07-2022
        [CustAuthFilter]
        public ActionResult GetRegionItemDataList(bool isSectorUser,int regionId=0,bool gridOnly=false)
        {
            var dataList = ServiceClient.GetRegionItemData(GetRequestData(), isSectorUser, regionId,gridOnly);
            var jsonResult = Json(dataList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
    }
}