﻿using ClosedXML.Excel;
using CookBook.Aspects.Factory;
using CookBook.Data;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CookBook.Data.MasterData;
using CookBook.ServiceApp;
using System.Net;
using Newtonsoft.Json;
using CookBook.Aspects.Utils;
using CookBook.Aspects.Constants;
using Rotativa;
using static CookBook.MvcApp.Filters.CustAuthFilter;

namespace CookBook.MvcApp.Controllers
{
    public class ColorController : BaseController
    {
        private CookBookService.ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return PartialView("Index");
        }

        [CustAuthFilter]
        public ActionResult GetColorDataList()
        {

            var colorList = ServiceClient.GetColorDataList( GetRequestData());
            var jsonResult = Json(colorList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }


        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveColorData(ColorData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }
            var result = ServiceClient.SaveColorData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        public ActionResult GeneratePDF()
        {
            var pdf = new ViewAsPdf("SamplePDF");
            var pdfContent = pdf.BuildFile(ControllerContext);

            // Specify the path where you want to save the PDF
            string filePath = Server.MapPath("~/pdfs/example.pdf");

            // Save the PDF content to the specified path
            System.IO.File.WriteAllBytes(filePath, pdfContent);

            // Return the path to the JavaScript file
            ViewBag.PdfFilePath = filePath;
            return Content(filePath);
        }

        [CustAuthFilter]
        public ActionResult DownloadPDF()
        {
            // Specify the path to the PDF file
            string filePath = Server.MapPath("~/pdfs/example.pdf");

            // Check if the file exists
            if (!System.IO.File.Exists(filePath))
            {
                return HttpNotFound(); // or return a custom error view
            }

            // Return the file as a response
            return File(filePath, "application/pdf", "example.pdf");
        }

    }
}