﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Reflection;
using System.Web.Mvc;
using ClosedXML.Excel;
using CookBook.Data.MasterData;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.Models;

namespace CookBook.MvcApp.Controllers
{
    public class RecipeController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult ReadonlyRecipe()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult RegionalConfig()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult SiteConfig()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetRecipeDataList(string condition, string DishCode)
        {
            var RecipeList = ServiceClient.GetRecipeDataList(condition, DishCode, GetRequestData());
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetItemDishRecipeNutritionData(string SiteCode)
        {
            var RecipeList = ServiceClient.GetItemDishRecipeNutritionData(SiteCode, GetRequestData());
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionRecipeDataList(int RegionID, string condition, string SubSectorCode)
        {
            var requestData = GetRequestData();
            requestData.SubSectorCode = SubSectorCode;
            var regionRecipeList = ServiceClient.GetRegionRecipeDataList(RegionID, condition, requestData);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }
        [CustAuthFilter]
        public ActionResult GetSiteRecipeDataList(string SiteCode, string condition)
        {
            var regionRecipeList = ServiceClient.GetSiteRecipeDataList(SiteCode, condition, GetRequestData());
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        //Krish
        [CustAuthFilter]
        public ActionResult GetSiteDishRecipeDataList(string SiteCode, string dishCode, string condition)
        {
            var regionRecipeList = ServiceClient.GetSiteDishRecipeDataList(SiteCode, dishCode, condition, GetRequestData());
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult UpdateSiteRecipeAllergenData(string recipeCode, string siteCode)
        {
            var recipeAllergens = ServiceClient.SaveSiteRecipeAllergenData(recipeCode, siteCode, GetRequestData());
            return Json(recipeAllergens, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetBaseRecipesByRecipeID(string recipeCode)
        {
            var requestData =  GetRequestData();
            requestData.RecipeCode = recipeCode;
            var RecipeList = ServiceClient.GetBaseRecipesByRecipeID(0, requestData);
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionBaseRecipesByRecipeID(int regionID, string recipeCode, string subSectorCode)
        {
            var requestData = GetRequestData();
            requestData.SubSectorCode = subSectorCode;
            requestData.RecipeCode = recipeCode;
            var RecipeList = ServiceClient.GetRegionBaseRecipesByRecipeID(regionID, 0, requestData);
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteBaseRecipesByRecipeID(string SiteCode, int recipeID, string recipeCode, int regionID)
        {
            var requestData = GetRequestData();
            requestData.RegionID = regionID;
            var RecipeList = ServiceClient.GetSiteBaseRecipesByRecipeID(recipeCode, SiteCode, recipeID, requestData);
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetMOGsByRecipeID(string recipeCode)
        {
            var requestData = GetRequestData();
            requestData.RecipeCode = recipeCode;
            var RecipeList = ServiceClient.GetMOGsByRecipeID(0, requestData);
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionMOGsByRecipeID(int regionID, string recipeCode, string subSectorCode)
        {
            var requestData = GetRequestData();
            requestData.SubSectorCode = subSectorCode;
            requestData.RecipeCode = recipeCode;
            var RecipeList = ServiceClient.GetRegionMOGsByRecipeID(regionID, 0, requestData);
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode, int regionID)
        {
            var requestData = GetRequestData();
            requestData.RegionID = regionID;
            var RecipeList = ServiceClient.GetSiteMOGsByRecipeID(recipeCode, SiteCode, recipeID, requestData);
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }


        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveRecipeData(RecipeData model, RecipeMOGMappingData[] baseRecipes, RecipeMOGMappingData[] mogs)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (model.ID == null)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
                // model.Status = (int)StatusEnum.Pending;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
            }
            model.CostAsOfDate = DateTime.Now;
            var result = ServiceClient.SaveRecipeData(model, baseRecipes, mogs, GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveRegionRecipeData(RegionRecipeData model, RegionRecipeMOGMappingData[] baseRecipes, RegionRecipeMOGMappingData[] mogs)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
            }
            //  model.CostAsOfDate = DateTime.Now;//Sir
            var result = ServiceClient.SaveRegionRecipeData(model, baseRecipes, mogs, GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveSiteRecipeData(SiteRecipeData model, SiteRecipeMOGMappingData[] baseRecipes, SiteRecipeMOGMappingData[] mogs)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
            }
            // model.CostAsOfDate = DateTime.Now;//Sir
            var result = ServiceClient.SaveSiteRecipeData(model, baseRecipes, mogs, GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult SaveRecipeNutrientMappingData(RecipeData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
            }
            var result = ServiceClient.SaveRecipeNutrientMappingData(model, GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult SaveRecipeDataList(RecipeData[] model)
        {
            var result = ServiceClient.SaveRecipeDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult SaveRegionRecipeDataList(RegionRecipeData[] model)
        {
            var result = ServiceClient.SaveRegionRecipeDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveSiteRecipeDataList(SiteRecipeData[] model)
        {
            var result = ServiceClient.SaveSiteRecipeDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetRecipeImpactedDataList(int recipeId, bool isBaseRecipe)
        {
            var impactedData = new MOGImpactedData();
            if (Convert.ToBoolean(ConfigurationManager.AppSettings[Core.Constants.ISRIENABLED]))
            {
                impactedData = ServiceClient.GetMOGImpactedDataList(0, recipeId, isBaseRecipe, GetRequestData());
            }
            return Json(impactedData, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SendRecipeEmail(RecipeData model)
        {
            var mogList = ServiceClient.SendRecipeEmail(model, GetRequestData());
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        //Krish
        [CustAuthFilter]
        public ActionResult GetRecipePrintingData(string recipeCode,string siteCode,int regionId)
        {

           var result = ServiceClient.GetRecipePrintingData(recipeCode, siteCode, regionId, GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //Krish
        [CustAuthFilter]
        public ActionResult SaveRecipeCopy(RecipeCopyData[] model, string[] recipes, string sourceSite)
        {
            var result = ServiceClient.SaveRecipeCopy(model,recipes,sourceSite, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //Krish 03-08-2022
        [CustAuthFilter]
        public ActionResult GetSectorRecipeDataList(bool isAddEdit = false,bool onLoad=false)
        {           
            var dataList = ServiceClient.GetSectorRecipeData(GetRequestData(), isAddEdit,onLoad);
            var jsonResult = Json(dataList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }


        [CustAuthFilter]
        public ActionResult ExportRecipeData(string recipeCode, decimal recipeYield, string recipeName)
        {
            FileContentResult rObj;

            using (XLWorkbook wb = new XLWorkbook())
            {
                int workSheetCount = 1;
               
                var rIngData = ServiceClient.GetRecipePrintingData(recipeCode, null, 0, GetRequestData());
                var requestData = GetRequestData();
                requestData.RecipeCode = recipeCode;
                var mogRecipeList = ServiceClient.GetMOGsByRecipeID(0, requestData);
                var brRecipeList = ServiceClient.GetBaseRecipesByRecipeID(0, requestData);
                if (mogRecipeList.Length > 0 || brRecipeList.Length > 0)
                {
                    IXLWorksheet workSheet = wb.AddWorksheet("List of MOGs and Base Recipes");
                    workSheet.ShowGridLines = false;
                    int rowCount = 1;

                    var recipeHading = string.Empty;
                    recipeHading = recipeCode + " " + recipeName + "| " + (!string.IsNullOrWhiteSpace(rIngData.DishName) ? rIngData.DishName : "") + "| " + recipeYield + " Kg";
                    IXLCell cellSheetHeading = workSheet.Cell(rowCount, 1);
                    cellSheetHeading.SetValue(recipeHading);
                    cellSheetHeading.Style.Font.FontSize = 11;
                    cellSheetHeading.Style.Font.SetBold();
                    cellSheetHeading.Style.Font.FontColor = XLColor.Black;
                    cellSheetHeading.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    cellSheetHeading.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    workSheet.Range("A" + rowCount + ":E" + rowCount + "").Row(1).Merge();

                    var recipeHading2 = string.Empty;
                    recipeHading2 = "Recipe - Compass India Food Services";
                    rowCount = 2;
                    cellSheetHeading = workSheet.Cell(rowCount, 1);
                    cellSheetHeading.SetValue(recipeHading2);
                    cellSheetHeading.Style.Font.FontSize = 11;
                    cellSheetHeading.Style.Font.SetBold();
                    cellSheetHeading.Style.Font.FontColor = XLColor.Black;
                    cellSheetHeading.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    workSheet.Range("A" + rowCount + ":E" + rowCount + "").Row(1).Merge();

                    rowCount = 4;

                    DataTable rMrIngTbl = GetRecipeMOGsDetailsTable(mogRecipeList,recipeYield);
                    wb.Worksheet(workSheetCount).Cell(rowCount, 1).InsertTable(rMrIngTbl);
                    int tableRowCount = rMrIngTbl.Rows.Count;
                    rowCount = rowCount + tableRowCount + 2;

                    DataTable rBRMrIngTbl = GetBaseRecipeMOGDetailsTable(brRecipeList,recipeYield);
                    wb.Worksheet(workSheetCount).Cell(rowCount, 1).InsertTable(rBRMrIngTbl);
                    rowCount = rowCount+ rBRMrIngTbl.Rows.Count + 2;

                    workSheet.Rows().AdjustToContents();
                    workSheet.Columns().AdjustToContents();
                    workSheet.Columns().Style.Alignment.Vertical = XLAlignmentVerticalValues.Justify;
                    foreach (var item in workSheet.Tables)
                    {
                        item.ShowAutoFilter = false;
                    }

                   
                }

                workSheetCount = workSheetCount + 1;
                
                if (rIngData.Mogs.Count > 0)
                {
                    IXLWorksheet workSheet = wb.AddWorksheet("List of MOGs");
                    workSheet.ShowGridLines = false;
                    int rowCount = 1;

                    var recipeHading = string.Empty;
                    recipeHading = recipeCode + " " + recipeName + "| " + (!string.IsNullOrWhiteSpace(rIngData.DishName)?rIngData.DishName:"") + "| " + recipeYield + " Kg";
                    IXLCell cellSheetHeading = workSheet.Cell(rowCount, 1);
                    cellSheetHeading.SetValue(recipeHading);
                    cellSheetHeading.Style.Font.FontSize = 11;
                    cellSheetHeading.Style.Font.SetBold();
                    cellSheetHeading.Style.Font.FontColor = XLColor.Black;
                    cellSheetHeading.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    cellSheetHeading.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    workSheet.Range("A" + rowCount + ":E" + rowCount + "").Row(1).Merge();

                    var recipeHading2 = string.Empty;
                    recipeHading2 = "Recipe - Compass India Food Services";
                    rowCount = 2;
                    cellSheetHeading = workSheet.Cell(rowCount, 1);
                    cellSheetHeading.SetValue(recipeHading2);
                    cellSheetHeading.Style.Font.FontSize = 11;
                    cellSheetHeading.Style.Font.SetBold();
                    cellSheetHeading.Style.Font.FontColor = XLColor.Black;
                    cellSheetHeading.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    workSheet.Range("A" + rowCount + ":D" + rowCount + "").Row(1).Merge();

                    rowCount = 3;

                    DataTable rIngTbl = GetRecipeMOGDetailsTable(rIngData, recipeYield);

                    wb.Worksheet(workSheetCount).Cell(rowCount + 1, 1).InsertTable(rIngTbl);
                    rowCount = rowCount + rIngTbl.Rows.Count + 2;

                    rowCount = rowCount + 2;
                    workSheet.Rows().AdjustToContents();
                    workSheet.Columns().AdjustToContents();
                    workSheet.Columns().Style.Alignment.Vertical = XLAlignmentVerticalValues.Justify;
                    foreach (var item in workSheet.Tables)
                    {
                        item.ShowAutoFilter = false;
                    }
                    workSheetCount = workSheetCount + 1;
                }

                if (wb.Worksheets.Count > 0)
                {
                    using (MemoryStream stream = new MemoryStream())
                    {

                        wb.SaveAs(stream);

                        var bytesdata = File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", ""+recipeName+".xlsx");
                        rObj = bytesdata;
                    }
                    return Json(rObj, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(string.Empty);
        }

       


        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        private DataTable GetRecipeMOGDetailsTable(RecipePrintingData data, decimal yield)
        {
            DataTable dataHeaderTable = new DataTable("RecipeMOGDetails");

            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "MOG Code", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "MOG Name", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "UOM", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "CBK Qty", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Validation Qty", DataType = typeof(string) });

            foreach (RecipePrintingMOGListData item in data.Mogs)
            {
                DataRow drItem = dataHeaderTable.NewRow();
                drItem["MOG Code"] = item.MOGCode;
                drItem["MOG Name"] = item.MOGName;
                drItem["UOM"] = item.UOMName;
                drItem["CBK Qty"] = item.MOGQty.HasValue ? Math.Round((item.MOGQty.Value / 10) * yield, 3) : item.MOGQty;
                dataHeaderTable.Rows.Add(drItem);
            }
            return dataHeaderTable;
        }

        private DataTable GetBaseRecipeMOGDetailsTable(RecipeMOGMappingData[] data, decimal yield)
        {
            DataTable dataHeaderTable = new DataTable("BaseRecipeDetails");

            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Base Recipe Code", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Base Recipe", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "UOM", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "CBK Qty", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Validation Qty", DataType = typeof(string) });

            foreach (RecipeMOGMappingData item in data)
            {
                DataRow drItem = dataHeaderTable.NewRow();
                drItem["Base Recipe Code"] = item.BaseRecipeCode;
                drItem["Base Recipe"] = item.BaseRecipeName;
                drItem["UOM"] = item.UOMName;
                drItem["CBK Qty"] = item.Quantity.HasValue ? Math.Round((item.Quantity.Value / 10) * yield, 3) : item.Quantity;

                dataHeaderTable.Rows.Add(drItem);
            }
            return dataHeaderTable;
        }

        private DataTable GetRecipeMOGsDetailsTable(RecipeMOGMappingData[] data, decimal yield)
        {
            DataTable dataHeaderTable = new DataTable("MOGDetails");

            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "MOG Code", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "MOG Name", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "UOM", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "CBK Qty", DataType = typeof(string) });
            dataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Validation Qty", DataType = typeof(string) });

            foreach (RecipeMOGMappingData item in data)
            {
                DataRow drItem = dataHeaderTable.NewRow();
                drItem["MOG Code"] = item.MOGCode;
                drItem["MOG Name"] = item.MOGName;
                drItem["CBK Qty"] = item.Quantity.HasValue ? Math.Round((item.Quantity.Value / 10) * yield, 3) : item.Quantity;
                drItem["UOM"] = item.UOMName;
                dataHeaderTable.Rows.Add(drItem);
            }
            return dataHeaderTable;
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult NationalBaseRecipe()
        {
            return PartialView(GetRequestData());
        }
    }
}