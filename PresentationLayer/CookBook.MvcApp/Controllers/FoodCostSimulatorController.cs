﻿using CookBook.Data.MasterData;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp.Controllers
{
    public class FoodCostSimulatorController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetSimulationDataList()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            
            var aplList = ServiceClient.GetSimulationDataListWithPaging(GetRequestData(),userData);
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        public ActionResult GetSimulationItemList(int siteId,int regionId,List<int> daypartId,bool checkSiteItem = false)
        {
            var aplList = ServiceClient.GetItemsForSimulation(GetRequestData(),siteId,regionId,daypartId.ToArray(), checkSiteItem);
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        public ActionResult GetDayPartBySiteList(int siteId)
        {
            var aplList = ServiceClient.GetDayPartBySiteList(GetRequestData(), siteId);
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        public ActionResult GetSimulationRegionItemDishCategories(int itemId,int siteId)
        {
            var aplList = ServiceClient.GetSimulationRegionItemDishCategories(GetRequestData(), siteId, 0,itemId);
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        public ActionResult UpdateAndFetchSiteDishRecipeData(string simCode,string SiteCode,string DishCode)
        {
            var aplList = ServiceClient.UpdateAndFetchSiteDishRecipeData(GetRequestData(), simCode, SiteCode, DishCode);
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        public ActionResult UpdateAndFetchSiteRecipeData(string simCode, string SiteCode, string DishCode)
        {
            var aplList = ServiceClient.UpdateAndFetchSiteRecipeData(GetRequestData(), simCode, SiteCode, DishCode);
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [CustAuthFilter]
        public ActionResult SaveSimulationData(List<SimulationBoardItemsData> model,int simId,int noOfDays,int siteId,int regionId,string status,bool saveOnly,string simName)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var dataModel = new SimulationBoardData();
            if (simId == 0)
            {
                dataModel.CreatedBy = userData.UserId;
                dataModel.CreatedOn = DateTime.Now;
            }
            else
            {
                dataModel.ID = simId;
                dataModel.ModifiedBy = userData.UserId;
                dataModel.ModifiedOn = DateTime.Now;
            }
            dataModel.RegionID = regionId;
            dataModel.SiteID = siteId;
            dataModel.NoOfDays = noOfDays;
            dataModel.ItemsData = model;
            dataModel.SimulationName = simName;
            dataModel.Status = status;
            dataModel.saveOnly = saveOnly;

            var result = ServiceClient.SaveSimulationBoard(dataModel, GetRequestData(),null);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveStatusSimulationData(FoodCostSimulationData model)
        {
            var result = ServiceClient.SaveSimulationBoard(null, GetRequestData(), model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult GetSimulationBoardData(int simId)
        {
            var aplList = ServiceClient.GetSimulationBoard(simId, GetRequestData());
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        public ActionResult SaveFoodCostSimSiteRecipeData(RecipeData rmodel, SiteRecipeData model, SiteRecipeMOGMappingData[] baseRecipes, SiteRecipeMOGMappingData[] mogs,string simCode)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (rmodel!=null && rmodel.ID == null)
            {
                rmodel.CreatedBy = userData.UserId;
                rmodel.CreatedOn = DateTime.Now;
                rmodel.ModifiedOn = DateTime.Now;
                rmodel.ModifiedBy = userData.UserId;
                // model.Status = (int)StatusEnum.Pending;
            }
            else if(rmodel!=null)
            {
                rmodel.ModifiedOn = DateTime.Now;
                rmodel.ModifiedBy = userData.UserId;
            }
            if (rmodel != null)
                rmodel.CostAsOfDate = DateTime.Now;
            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
            }

            var aplList = ServiceClient.SaveFoodCostSimSiteRecipeData(rmodel,model,baseRecipes,mogs, simCode, GetRequestData());
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        public ActionResult GetAPLMasterDataListForMOG_Site_FCS(string mogCode, string SiteCode,string simuCode)
        {
            var aplList = ServiceClient.GetFCSAPLMasterSiteDataList(null, mogCode, SiteCode, simuCode, GetRequestData());
            return Json(aplList, JsonRequestBehavior.AllowGet);
        }
        
            [CustAuthFilter]
        public ActionResult SaveSimAPLHistory(FoodCostSimAPLHistoryData data)
        {           

            var result = ServiceClient.SaveFCSAPLHistory(data, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult GetSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode,string simCode)
        {
            var RecipeList = ServiceClient.GetFCSSiteMOGsByRecipeID(SiteCode,recipeID, recipeCode,simCode, GetRequestData());
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetFcsDishDataList(string siteCode,int itemId=0)
        {
            //itemId = 0;
            var dishList = ServiceClient.GetFcsDishDataList(GetRequestData(), siteCode,itemId);
            var jsonResult = Json(dishList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        //[CustAuthFilter]
        //public ActionResult GetItemsDishCategories(List<int> itemId)
        //{
        //    var aplList = ServiceClient.GetItemsDishCategories(GetRequestData(),  itemId);
        //    var result = Json(aplList, JsonRequestBehavior.AllowGet);
        //    result.MaxJsonLength = int.MaxValue;
        //    return result;
        //}
    }
}