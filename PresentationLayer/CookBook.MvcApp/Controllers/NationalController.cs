﻿using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp.Controllers
{
    public class NationalController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetSiteDataList()
        {
            var siteList = ServiceClient.NationalGetSiteMasterDataList(GetRequestData());
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        
        

    }
}