﻿using System;
using System.Web.Mvc;
using CookBook.Data.MasterData;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;

namespace CookBook.MvcApp.Controllers
{
    public class SettingController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
      
     
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return View(GetRequestData());
        }
      
        [CustAuthFilter]
        public ActionResult GetApplicationSettingDataList()
        {
            var asList = ServiceClient.GetApplicationSettingDataList(GetRequestData());
            var jsonResult = Json(asList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

      
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveApplicationSettingData(ApplicationSettingData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }
            var result = ServiceClient.SaveApplicationSettingData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult ChangeStatus(ApplicationSettingData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.ModifiedBy = userData.UserId;
            model.ModifiedOn = DateTime.Now;
            var result = ServiceClient.SaveApplicationSettingData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }


    



    }
}