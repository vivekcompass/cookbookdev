﻿using ClosedXML.Excel;
using CookBook.Aspects.Factory;
using CookBook.Data;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CookBook.Data.MasterData;
using System.Data.OleDb;
using Kendo.Mvc.UI;

namespace CookBook.MvcApp.Controllers
{
    public class NationalAPLController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            // return View();
            return PartialView();
        }

        [CustAuthFilter]
        
        public ActionResult GetAPLMasterDataList([DataSourceRequest] DataSourceRequest request)
        {
            var aplList = ServiceClient.GetAPLMasterDataListPageing(GetRequestData(), request.Page);
            //var result = Json(new { total = recCount, data = aplList }, JsonRequestBehavior.AllowGet);
            var result = Json(new { total = 100000, aplList }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [CustAuthFilter]
        public ActionResult GetAPLMasterDataListPageing(int pageindex)
        {
           
            var aplList = ServiceClient.GetAPLMasterDataListPageing(GetRequestData(), pageindex);
            //var result = Json(new { total = recCount, data = aplList }, JsonRequestBehavior.AllowGet);
            var result = Json(new { total = 100000, aplList }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;

        }

        [CustAuthFilter]
        [HttpGet]
        public ActionResult DownloadExcTemplateAPL()
        {
            var grdReport = new System.Web.UI.WebControls.GridView();
            grdReport.DataSource = ServiceClient.GetNationalAPLExcelList(GetRequestData());
            grdReport.DataBind();
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
            grdReport.RenderControl(htw);
           
            byte[] bindata = System.Text.Encoding.ASCII.GetBytes(sw.ToString());
            //Response.Flush();
            //Response.End();
            return File(bindata, "application/ms-excel", "NationalAPlMaster.xlsx");



           

        }
      

        [CustAuthFilter]
        public ActionResult GetAPLMasterDataListForMOG(string mogCode)
        {
            var aplList = ServiceClient.GetAPLMasterDataList(null, mogCode, GetRequestData());
            return Json(aplList, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult GetAPLMasterDataListForMOG_Region(string mogCode, int Region_ID)
        {
            var aplList = ServiceClient.GetAPLMasterRegionDataList(null, mogCode, Region_ID, GetRequestData());
            return Json(aplList, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetAPLMasterDataListForMOG_Site(string mogCode, string SiteCode)
        {
            var aplList = ServiceClient.GetAPLMasterSiteDataList(null, mogCode, SiteCode, GetRequestData());
            return Json(aplList, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveAPLMasterData(APLMasterData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.ModifiedBy = userData.UserId;
            model.ModifiedDate = DateTime.Now;
            var result = ServiceClient.SaveAPLMasterData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveAPLMasterDataList(APLMasterData[] model)
        {
            var result = ServiceClient.SaveAPLMasterDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [HttpGet]
        public ActionResult DownloadExcTemplate()
        {
            var mogList = ServiceClient.GetNOTMappedMOGDataListAPL(GetRequestData());
            DataTable dt = new DataTable();
            dt.Columns.Add("ArticleNumber");
            dt.Columns.Add("ArticleDescription");
            dt.Columns.Add("MOGCode");
            foreach (APLMOGDATA p in mogList)
            {
                dt.Rows.Add(p.ArticleNumber, p.ArticleDescription, p.MOGCode);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                dt.TableName = "ArticleMapped.xlsx";
                wb.Worksheets.Add(dt);

                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ArticleMapped.xlsx");
                }
            }


        }

        [CustAuthFilter]
        [HttpPost]
        public string UploadExcelsheet()
        {
            string Res = string.Empty;
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                // List<ProductModel> _lstProductMaster = new List<ProductModel>();
                string filePath = string.Empty;
                if (Request.Files != null)
                {
                    string path = Server.MapPath("~/Content/Upload/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    filePath = path + Path.GetFileName("MOGNOTMAPPEDAPL-" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss-ff") + Path.GetExtension(file.FileName));
                    string extension = Path.GetExtension("MOGNOTMAPPEDAPL-" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss-ff") + Path.GetExtension(file.FileName));
                    file.SaveAs(filePath);

                    string conString = string.Empty;
                    switch (extension)
                    {
                        case ".xls": //Excel 97-03.
                            conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                            break;
                        case ".xlsx": //Excel 07 and above.
                            conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                            break;
                    }
                    conString = string.Format(conString, filePath);

                    using (OleDbConnection connExcel = new OleDbConnection(conString))
                    {
                        using (OleDbCommand cmdExcel = new OleDbCommand())
                        {
                            using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                            {
                                DataTable dt = new DataTable();
                                cmdExcel.Connection = connExcel;

                                //Get the name of First Sheet.
                                connExcel.Open();
                                DataTable dtExcelSchema;
                                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                connExcel.Close();

                                //Read Data from First Sheet.
                                connExcel.Open();
                                cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                                odaExcel.SelectCommand = cmdExcel;
                                odaExcel.Fill(dt);

                                dt.TableName = "APLMOG";
                                MemoryStream str = new MemoryStream();
                                dt.WriteXml(str, true);
                                str.Seek(0, SeekOrigin.Begin);
                                StreamReader sr = new StreamReader(str);
                                string xmlstr;
                                xmlstr = sr.ReadToEnd();
                                connExcel.Close();
                                string UserName = string.Empty;
                                var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
                                if (UserInstance.IsAuthenticated)
                                {
                                    UserName = UserInstance.FindFirst("preferred_username").Value;
                                }
                                var res = ServiceClient.GetUserData(UserName, null);
                                string username = res.UserId.ToString(); ;
                                Res = ServiceClient.UpdateMOGLISTAPL(GetRequestData(), xmlstr, username);

                            }
                        }
                    }

                }
            }
            return Res;

        }

        [CustAuthFilter]
        public ActionResult GetMOGAPLHISTORYDetailsAPL()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();
            //  DETAILS
            var regionRecipeList_det = ServiceClient.GetMOGAPLHISTORYDetAPL(GetRequestData(), username);
            var jsonResult = Json(regionRecipeList_det, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }

        [CustAuthFilter]
        public ActionResult GetMOGAPLHISTORYAPL()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();

            var regionRecipeList = ServiceClient.GetMOGAPLHISTORYAPL(GetRequestData(), username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;

            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetALLHISTORYDetailsAPL()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();
            //  DETAILS
            var regionRecipeList_det = ServiceClient.GetALLHISTORYDetailsAPL(GetRequestData(), username);
            var jsonResult = Json(regionRecipeList_det, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }
        [CustAuthFilter]
        public ActionResult GetshowGetALLHISTORYBATCHWISEAPL(string BATCHNUMBER)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();
            //  DETAILS
            var regionRecipeList_det = ServiceClient.GetshowGetALLHISTORYBATCHWISEAPL(GetRequestData(), username, BATCHNUMBER);
            var jsonResult = Json(regionRecipeList_det, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }
    }
}