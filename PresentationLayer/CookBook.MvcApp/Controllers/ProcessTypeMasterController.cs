﻿using CookBook.Data.MasterData;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp.Controllers
{   
    public class ProcessTypeMasterController : BaseController
    {
        private CookBookService.ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        public ActionResult GetProcessTypeDataList()
        {
            var aplList = ServiceClient.GetProcessTypeMasterData(GetRequestData());
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveProcessTypeData(ProcessTypeMasterData model)
        {
            var result = ServiceClient.SaveProcessTypeMasterData(GetRequestData(), model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult GetProcessMappingDataList()
        {
            var aplList = ServiceClient.GetProcessMappingDataList(GetRequestData());
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        
    }
}