﻿using CookBook.Data.MasterData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp.Controllers
{
    public class ManagePermissionsController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetPermissionsMasterList()
        {
            var mogList = ServiceClient.GetPermissionDataList(GetRequestData());
            return Json(mogList, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult SavePermissionInfo(PermissionMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.SavePermissionInfo(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        public ActionResult UpdatePermissionInfo(PermissionMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.UpdatePermissionInfo(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        public ActionResult UpdatePermissionInactiveActive(PermissionMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.UpdatePermissionInactiveActive(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }
    }
}