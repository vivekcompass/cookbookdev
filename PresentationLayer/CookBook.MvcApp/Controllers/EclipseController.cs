﻿using System.Web.Mvc;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.Data.MasterData;
using System;

namespace CookBook.MvcApp.Controllers
{
    public class EclipseController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

       
        [CustAuthFilter]
        public ActionResult UserCapringMapping()
        {
            return View();
        }
        [CustAuthFilter]
        public ActionResult GetCapringMasterDataList()
        {
            var capList = ServiceClient.GetCapringMasterDataList(GetRequestData());
            var jsonResult = Json(capList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetUserMasterDataList()
        {
            var UserMasterList = ServiceClient.GetUserMasterDataList(GetRequestData());
            var jsonResult = Json(UserMasterList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
    }
}