﻿using CookBook.Data.MasterData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace CookBook.MvcApp.Controllers
{
    public class ManageFunctionController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetFunctionMasterList()
        {
            var mogList = ServiceClient.GetFunctionDataList(GetRequestData());
            return Json(mogList, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveFunctionLevelInfo(FunctionMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.SaveFunctionLevelInfo(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        public ActionResult UpdateFunctionLevelInfo(FunctionMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.UpdateFunctionLevelInfo(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }


        [CustAuthFilter]
        public ActionResult UpdatefunctionInactiveActive(FunctionMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.UpdatefunctionInactiveActive(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }
    }
}