﻿using CookBook.Data.MasterData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CookBook.MvcApp.Controllers
{
    public class HealthcareMasterController : BaseController
    {
        // GET: HealthTagMaster
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();
        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult HealthTagMaster()
        {
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult HealthTagMasterList()
        {
            var regionRecipeList = ServiceClient.GetHealthTagDataList(GetRequestData());
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
           
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveHealthTag(HealthTagMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var  regionRecipeList = ServiceClient.SaveHealthTag(GetRequestData(),model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
           
            return Json(jsonResult, JsonRequestBehavior.AllowGet);
            
        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult UpdateHealthTag(HealthTagMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.UpdateHealthTag(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult LifeStyleTagMaster()
        {
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult LifeStyleTagMasterList()
        {
            var regionRecipeList = ServiceClient.GetLifeStyleTagDataList(GetRequestData());
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveLifeStyleTag(LifeStyleTagMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.SaveLifeStyleTag(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult UpdateLifeStyleTag(LifeStyleTagMasterData model)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            int username = res.UserId;
            var regionRecipeList = ServiceClient.UpdateLifeStyleTag(GetRequestData(), model, username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);

            return Json(jsonResult, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult NutritionMaster()
        {
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        public ActionResult GetDietTypeMasterList()
        {
            var regionRecipeList = ServiceClient.GetDietTypeDataList(GetRequestData());
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }

        [CustAuthFilter]
        public ActionResult GetPlanningTagMasterList()
        {
            var regionRecipeList = ServiceClient.GetPlanningTag(GetRequestData());
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }

        [CustAuthFilter]
        public ActionResult GetTextureMasterList()
        {
            var regionRecipeList = ServiceClient.GetTextureDataList(GetRequestData());
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }

        [CustAuthFilter]
        public ActionResult TextureMaster()
        {
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult DietTypeMaster()
        {
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult PlanningTagMaster()
        {
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult NutrientUOMMaster()
        {
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult NutrientTypeMaster()
        {
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult CommonMaster()
        {
            return PartialView(GetRequestData());
        }


        [CustAuthFilter]
        public ActionResult CommonMasterList(CommonMasterData model)
        {
            var data = ServiceClient.GetCommonMasterDataList(GetRequestData(),model);
            var jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveCommonMaster(CommonMasterData model)
        {
            var result = ServiceClient.SaveCommonMasterData(GetRequestData(), model);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SavePlanningTagMaster(PlanningTagData model)
        {
            var result = ServiceClient.SavePlanningTagData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);

        }
    }
}