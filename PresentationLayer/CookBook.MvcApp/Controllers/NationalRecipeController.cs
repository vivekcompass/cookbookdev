﻿using System;
using System.Configuration;
using System.Web.Mvc;
using CookBook.Aspects.Factory;
using CookBook.Data.MasterData;
using CookBook.Data.UserData;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.Models;


namespace CookBook.MvcApp.Controllers
{
    public class NationalRecipeController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult RegionalConfig()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult SiteConfig()
        {
            //return View(GetRequestData());
            return PartialView(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult GetRecipeDataList(string condition, string DishCode)
        {
            var RecipeList = ServiceClient.GetNationalRecipeDataList(condition, DishCode, GetRequestData());
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetRegionRecipeDataList(int RegionID, string condition, string SubSectorCode)
        {
            var requestData = GetRequestData();
            requestData.SubSectorCode = SubSectorCode;
            var regionRecipeList = ServiceClient.GetRegionRecipeDataList(RegionID, condition, requestData);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }
        [CustAuthFilter]
        public ActionResult GetSiteRecipeDataList(string SiteCode, string condition)
        {
            var regionRecipeList = ServiceClient.GetSiteRecipeDataList(SiteCode, condition, GetRequestData());
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetBaseRecipesByRecipeID(int recipeID)
        {
            var RecipeList = ServiceClient.GetBaseRecipesByRecipeID(recipeID, GetRequestData());
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionBaseRecipesByRecipeID(int regionID, int recipeID, string subSectorCode)
        {
            var requestData = GetRequestData();
            requestData.SubSectorCode = subSectorCode;
            var RecipeList = ServiceClient.GetRegionBaseRecipesByRecipeID(regionID, recipeID, requestData);
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetSiteBaseRecipesByRecipeID(string SiteCode, int recipeID, string recipeCode)
        {
            var RecipeList = ServiceClient.GetSiteBaseRecipesByRecipeID(recipeCode, SiteCode, recipeID, GetRequestData());
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetMOGsByRecipeID(int recipeID)
        {

            var RecipeList = ServiceClient.GetMOGsByRecipeID(recipeID, GetRequestData());
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetRegionMOGsByRecipeID(int regionID, int recipeID, string subSectorCode)
        {
            var requestData = GetRequestData();
            requestData.SubSectorCode = subSectorCode;
            var RecipeList = ServiceClient.GetRegionMOGsByRecipeID(regionID, recipeID, requestData);
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode)
        {
            var RecipeList = ServiceClient.GetSiteMOGsByRecipeID(recipeCode, SiteCode, recipeID, GetRequestData());
            var jsonResult = Json(RecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }


        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveRecipeData(RecipeData model, RecipeMOGMappingData[] baseRecipes, RecipeMOGMappingData[] mogs)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (model.ID == null)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
                // model.Status = (int)StatusEnum.Pending;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
            }
            model.CostAsOfDate = DateTime.Now;
            var result = ServiceClient.SaveRecipeData(model, baseRecipes, mogs, GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveRegionRecipeData(RegionRecipeData model, RegionRecipeMOGMappingData[] baseRecipes, RegionRecipeMOGMappingData[] mogs)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
            }
            //  model.CostAsOfDate = DateTime.Now;//Sir
            var result = ServiceClient.SaveRegionRecipeData(model, baseRecipes, mogs, GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveSiteRecipeData(SiteRecipeData model, SiteRecipeMOGMappingData[] baseRecipes, SiteRecipeMOGMappingData[] mogs)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = userData.UserId;
            }
            // model.CostAsOfDate = DateTime.Now;//Sir
            var result = ServiceClient.SaveSiteRecipeData(model, baseRecipes, mogs, GetRequestData());

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult SaveRecipeDataList(RecipeData[] model)
        {
            var result = ServiceClient.SaveRecipeDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult SaveRegionRecipeDataList(RegionRecipeData[] model)
        {
            var result = ServiceClient.SaveRegionRecipeDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveSiteRecipeDataList(SiteRecipeData[] model)
        {
            var result = ServiceClient.SaveSiteRecipeDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetRecipeImpactedDataList(int recipeId, bool isBaseRecipe)
        {
            var impactedData = new MOGImpactedData();
            if (Convert.ToBoolean(ConfigurationManager.AppSettings[Core.Constants.ISRIENABLED]))
            {
                impactedData = ServiceClient.GetMOGImpactedDataList(0, recipeId, isBaseRecipe, GetRequestData());
            }
            return Json(impactedData, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SendRecipeEmail(RecipeData model)
        {
            var mogList = ServiceClient.SendRecipeEmail(model, GetRequestData());
            return Json(true, JsonRequestBehavior.AllowGet);
        }


    }
}