﻿using ClosedXML.Excel;
using CookBook.Aspects.Factory;
using CookBook.Data;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CookBook.Data.MasterData;
using System.Data.OleDb;
using static CookBook.MvcApp.Filters.CustAuthFilter;

namespace CookBook.MvcApp.Controllers
{
    public class APLMasterController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        [HandleError]
        public ActionResult Index()
        {
           // return View();
            return PartialView();
        }

        [CustAuthFilter]
        public ActionResult GetAPLMasterDataList()
        {
            var aplList =   ServiceClient.GetAPLMasterDataList(null,null,GetRequestData());
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [CustAuthFilter]
        public ActionResult GetAPLMasterDataListForSector()
        {
            var aplList = ServiceClient.GetAPLMasterDataListForSector(null, null, GetRequestData());
            var result = Json(aplList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [CustAuthFilter]
        public ActionResult GetAPLMasterDataListForMOG(string mogCode)
        {
            var aplList = ServiceClient.GetAPLMasterDataListForSector(null, mogCode, GetRequestData());
            return Json(aplList, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult GetAPLMasterDataListForMOG_Region(string mogCode, int Region_ID)
        {
            var aplList = ServiceClient.GetAPLMasterRegionDataList(null, mogCode,Region_ID, GetRequestData());
            return Json(aplList, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult GetAPLMasterDataListForMOG_Site(string mogCode, string SiteCode)
        {
            var aplList = ServiceClient.GetAPLMasterSiteDataList(null, mogCode, SiteCode, GetRequestData());
            return Json(aplList, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult SaveAPLMasterData(APLMasterData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.ModifiedBy = userData.UserId;
            model.ModifiedDate = DateTime.Now;
            var result = ServiceClient.SaveAPLMasterData(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveAPLMasterDataList(APLMasterData[] model)
        {
            var result = ServiceClient.SaveAPLMasterDataList(model, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        [HttpGet]
        public ActionResult DownloadExcTemplate()
        {
            var mogList = ServiceClient.GetNOTMappedMOGDataListAPL(GetRequestData());
            DataTable dt = new DataTable();
            dt.Columns.Add("ArticleNumber");
            dt.Columns.Add("ArticleDescription");           
            dt.Columns.Add("MOGCode");
            foreach (APLMOGDATA p in mogList)
            {
                dt.Rows.Add(p.ArticleNumber, p.ArticleDescription, p.MOGCode);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                dt.TableName = "ArticleMapped.xlsx";
                wb.Worksheets.Add(dt);

                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ArticleMapped.xlsx");
                }
            }


        }

        [CustAuthFilter]
        [HttpPost]
        public string UploadExcelsheet()
        {
            string Res = string.Empty;
            DataTable dt = new DataTable();
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                var fileext = Path.GetExtension(file.FileName);
                string contentType = string.Empty;

                var fileExtension = Path.GetExtension(file.FileName).ToLowerInvariant();
                var allowedExtensions = new[] { ".xlsx" };

                if (!allowedExtensions.Contains(fileExtension))
                {
                    return "Invalid file type. Only XLSX files are allowed.";
                }

                // Optionally, check the MIME type
                if (fileext == ".xlsx")
                {
                    contentType = "image/xlsx";
                }

                var allowedMimeTypes = new[] { "image/xlsx" };

                if (!allowedMimeTypes.Contains(contentType))
                {
                    return "Invalid file type. Only XLSX files are allowed.";
                }
                string filePath = string.Empty;
                if (Request.Files != null)
                {
                    string path = Path.Combine(Server.MapPath("~/Content/Upload/"), Path.GetFileName(file.FileName));
                    //Saving the file
                    file.SaveAs(path);

                    using (XLWorkbook workbook = new XLWorkbook(path))
                    {
                        IXLWorksheet worksheet = workbook.Worksheet(1);
                        bool FirstRow = true;
                        //Range for reading the cells based on the last cell used.
                        string readRange = "1:1";
                        foreach (IXLRow row in worksheet.RowsUsed())
                        {

                            if (FirstRow)
                            {

                                readRange = string.Format("{0}:{1}", 1, row.LastCellUsed().Address.ColumnNumber);
                                foreach (IXLCell cell in row.Cells(readRange))
                                {
                                    dt.Columns.Add(cell.Value.ToString());
                                }
                                FirstRow = false;
                            }
                            else
                            {

                                dt.Rows.Add();
                                int cellIndex = 0;

                                foreach (IXLCell cell in row.Cells(readRange))
                                {
                                    dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                                    cellIndex++;
                                }
                            }
                        }

                    }
                    dt.TableName = "APLMOG";
                                MemoryStream str = new MemoryStream();
                                dt.WriteXml(str, true);
                                str.Seek(0, SeekOrigin.Begin);
                                StreamReader sr = new StreamReader(str);
                                string xmlstr;
                                xmlstr = sr.ReadToEnd();
                               
                                string UserName = string.Empty;
                                var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
                                if (UserInstance.IsAuthenticated)
                                {
                                    UserName = UserInstance.FindFirst("preferred_username").Value;
                                }
                                var res = ServiceClient.GetUserData(UserName, null);
                                string username = res.UserId.ToString();;
                                Res = ServiceClient.UpdateMOGLISTAPL(GetRequestData(), xmlstr, username);

                            }
                        
                    

                
            }
            return Res;

        }

        [CustAuthFilter]
        public ActionResult GetMOGAPLHISTORYDetailsAPL()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();
            //  DETAILS
            var regionRecipeList_det = ServiceClient.GetMOGAPLHISTORYDetAPL(GetRequestData(), username);
            var jsonResult = Json(regionRecipeList_det, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }

        [CustAuthFilter]
        public ActionResult GetMOGAPLHISTORYAPL()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();

            var regionRecipeList = ServiceClient.GetMOGAPLHISTORYAPL(GetRequestData(), username);
            var jsonResult = Json(regionRecipeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;

            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetALLHISTORYDetailsAPL()
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString(); 
            //  DETAILS
            var regionRecipeList_det = ServiceClient.GetALLHISTORYDetailsAPL(GetRequestData(), username);
            var jsonResult = Json(regionRecipeList_det, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }
        [CustAuthFilter]
        public ActionResult GetshowGetALLHISTORYBATCHWISEAPL(string BATCHNUMBER)
        {
            string UserName = string.Empty;
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                UserName = UserInstance.FindFirst("preferred_username").Value;
            }
            var res = ServiceClient.GetUserData(UserName, null);
            string username = res.UserId.ToString();
            //  DETAILS
            var regionRecipeList_det = ServiceClient.GetshowGetALLHISTORYBATCHWISEAPL(GetRequestData(), username, BATCHNUMBER);
            var jsonResult = Json(regionRecipeList_det, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;

        }
        //Krish 11-10-2022
        [CustAuthFilter]
        public ActionResult SaveRangeTypeAPLData(string articleNumber, string rangeTypeCode)
        {
            var result = ServiceClient.SaveRangeTypeAPLData(articleNumber, rangeTypeCode, GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}