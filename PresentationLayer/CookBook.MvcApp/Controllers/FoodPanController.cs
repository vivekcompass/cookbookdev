﻿using ClosedXML.Excel;
using CookBook.Aspects.Factory;
using CookBook.Data;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.MvcApp.Core;
using CookBook.MvcApp.Filters;
using CookBook.MvcApp.CookBookService;
using CookBook.MvcApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CookBook.Data.MasterData;

namespace CookBook.MvcApp.Controllers
{
    public class FoodPanController : BaseController
    {
        private ICookBookService ServiceClient = ServiceHelper.GetCookBookService();

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult Index()
        {
            return View(GetRequestData());
        }
        [CustAuthFilter]
        public ActionResult SiteFoodPan()
        {
            return View(GetRequestData());
        }

        //[CustAuthFilter]
        //public ActionResult GetDishDataListForFood(string siteCode)
        //{
        //    var aplList = ServiceClient.GetSiteDishFoodMappingDataList(siteCode,GetRequestData());
        //    var jsonResult = Json(aplList, JsonRequestBehavior.AllowGet);
        //    jsonResult.MaxJsonLength = Int32.MaxValue;
        //    return jsonResult;
        //}

        //[CustAuthFilter]
        //public ActionResult GetDishCategoryDataListForFood(string siteCode)
        //{
        //    var aplList = ServiceClient.GetSiteDishCategoryFoodMappingDataList(siteCode,GetRequestData());
        //    var jsonResult = Json(aplList, JsonRequestBehavior.AllowGet);
        //    jsonResult.MaxJsonLength = Int32.MaxValue;
        //    return jsonResult;
        //}



        //[CustAuthFilter]
        //public ActionResult SaveDishFoodPanData(SiteDishFoodPanMappingData model)
        //{
        //    UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
        //    model.ModifiedBy = userData.UserId;
        //    model.ModifiedOn = DateTime.Now;
        //    var result = ServiceClient.SaveSiteDishFoodPanMappingData(model,GetRequestData());
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //[CustAuthFilter]
        //public ActionResult SaveFoodPanDataList(SiteDishFoodPanMappingData[] model)
        //{
        //    var result = ServiceClient.SaveSiteDishFoodPanMappingDataList(model,GetRequestData());
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        //[CustAuthFilter]
        //public ActionResult SaveFoodPanCategoryDataList(SiteDishCategoryFoodPanMappingData[] model)
        //{
        //    var result = ServiceClient.SaveSiteDishCategoryFoodPanMappingDataList(model,GetRequestData());
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        [CustAuthFilter]
        [LogRequestDetailsAttribute]
        public ActionResult FoodPan()
        {
            return View(GetRequestData());
        }


        [CustAuthFilter]
        public ActionResult GetDishDataList()
        {
            var dishList = ServiceClient.GetDishDataList(null, null,GetRequestData());
            var result = Json(dishList, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }

        [CustAuthFilter]
        public ActionResult GetDishRecipeMappingDataList(string DishCode)
        {
            var dishRecipeMappingList = ServiceClient.GetDishRecipeMappingDataList(DishCode,GetRequestData());
            var jsonResult = Json(dishRecipeMappingList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        [ValidateXSS]
        public ActionResult SaveDishData(DishData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }
            var result = ServiceClient.SaveDishData(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult SaveDishRecipeMappingList(DishRecipeMappingData[] model)
        {

            var result = ServiceClient.SaveDishRecipeMappingList(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveDishDataList(DishData[] model)
        {
            var result = ServiceClient.SaveDishDataList(model,GetRequestData());
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}