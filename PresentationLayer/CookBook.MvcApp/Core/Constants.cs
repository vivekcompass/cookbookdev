﻿namespace CookBook.MvcApp.Core
{
    public class Constants
    {
        public const string COOKBOOKSERVICE_ENDPOINTKEY = "CookBookServiceEndPoint";
        public const string ApplicationUrl = "ApplicationUrl";
        public const string COOKBOOKSERVICE_ENDPOINT = "Web";
        public const string TEST_USER = "TestUser";
        public const string ProductionPlanFolder = "ProductionPlanFolder";
        public const string ProductionPlanExcelFile = "ProductionPlanExcelFile";
        public const string ProductionPlanSheetName = "ProductionPlanSheetName";
        public const string ISRIENABLED = "IsRIEnabled";
    }
}