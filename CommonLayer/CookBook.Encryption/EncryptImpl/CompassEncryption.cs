﻿using CookBook.Encryption.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Encryption.EncryptImpl
{
    [Export(typeof(ICompassEncryption))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CompassEncryption : ICompassEncryption
    {
        #region Private Variables

        private const string KeyValue = "8080808080808080";
        private const string IVValue = "9090909090909090";

        #endregion

        #region Public Methods

        /// <summary>
        /// this overloaded method will encrypt the Data based on encryptionKey
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string EncryptData(string plainText, String keyParameter = "")
        {
            var key = Encoding.UTF8.GetBytes(KeyValue);
            var iv = Encoding.UTF8.GetBytes(IVValue);
            byte[] encrypted;
            // Create a RijndaelManaged object
            // with the specified key and IV.
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return Convert.ToBase64String(encrypted);
        }

        /// <summary>
        /// this overloaded method will decrypt the data based on decryptionKey
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string DecryptData(string cipherText, String key = "")
        {
            byte[] cipherTextByte = Convert.FromBase64String(cipherText.ToString());
            byte[] KeyByte = Encoding.UTF8.GetBytes(KeyValue);
            byte[] IV = Encoding.UTF8.GetBytes(IVValue);
            if (cipherTextByte == null || cipherTextByte.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (KeyByte == null || KeyByte.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an RijndaelManaged object 
            // with the specified key and IV. 
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = KeyByte;
                rijAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherTextByte))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream 
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;

        }

        #endregion
    }
}
