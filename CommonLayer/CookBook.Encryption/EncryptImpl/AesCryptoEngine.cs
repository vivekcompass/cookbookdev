﻿using CookBook.Aspects.Constants;
using CookBook.Encryption.Interfaces;
using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CookBook.Encryption.EncryptImpl
{
    [Export(typeof(IAesCryptoEngine))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AesCryptoEngine : IAesCryptoEngine
    {
        #region Public Methods

        /// <summary>
        /// this overloaded method will encrypt the Data based on encryptionKey
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string EncryptData(string plainText, String key = "")
        {
            string encryptionKey = ApplicationGlobalConstants.AES_ENCRYPTDECRYPT_KEY;

            // Convert our plaintext into a byte array.
            // Let us assume that plaintext contains UTF8-encoded characters.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Use the password to generate pseudo-random bytes for the encryption
            // key. Specify the size of the key in bytes (instead of bits).
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] keyBytes = new byte[16];

            byte[] tempKey = encoding.GetBytes(encryptionKey);

            for (int i = 0; i < keyBytes.Length; i++)
            {
                if (i < tempKey.Length)
                {
                    keyBytes[i] = tempKey[i];
                }
                else
                {
                    keyBytes[i] = 0;
                }
            }

            // Create uninitialized Rijndael encryption object.
            RijndaelManaged symmetricKey = new RijndaelManaged();

            // It is reasonable to set encryption mode to Cipher Block Chaining
            // (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.ECB;

            // Generate encryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key 
            // bytes.

            //ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes,initVectorBytes);
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, null);

            // Define memory stream which will be used to hold encrypted data.
            MemoryStream memoryStream = new MemoryStream();

            // Define cryptographic stream (always use Write mode for encryption).
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                         encryptor,
                                                         CryptoStreamMode.Write);
            // Start encrypting.
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

            // Finish encrypting.
            cryptoStream.FlushFinalBlock();

            // Convert our encrypted data from a memory stream into a byte array.
            byte[] cipherTextBytes = memoryStream.ToArray();

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert encrypted data into a base64-encoded string.
            string cipherText = Convert.ToBase64String(cipherTextBytes);

            // Return encrypted string.
            return cipherText;
        }

        /// <summary>
        /// this overloaded method will decrypt the data based on decryptionKey
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string DecryptData(string cipherText, String key = "")
        {
            string decryptionKey = ApplicationGlobalConstants.AES_ENCRYPTDECRYPT_KEY;
            // Convert our ciphertext into a byte array.
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

            // Use the password to generate pseudo-random bytes for the encryption
            // key. Specify the size of the key in bytes (instead of bits).
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] keyBytes = new byte[16];

            byte[] tempKey = encoding.GetBytes(decryptionKey);

            for (int i = 0; i < keyBytes.Length; i++)
            {
                if (i < tempKey.Length)
                {
                    keyBytes[i] = tempKey[i];
                }
                else
                {
                    keyBytes[i] = 0;
                }
            }

            // Create uninitialized Rijndael encryption object.
            RijndaelManaged symmetricKey = new RijndaelManaged();

            // It is reasonable to set encryption mode to Cipher Block Chaining
            // (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.ECB;

            // Generate decryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key 
            // bytes.

            //ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes,initVectorBytes);
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, null);

            // Define memory stream which will be used to hold encrypted data.
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

            // Define cryptographic stream (always use Read mode for encryption).
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                          decryptor,
                                                          CryptoStreamMode.Read);


            // Since at this point we don't know what the size of decrypted data
            // will be, allocate the buffer long enough to hold ciphertext;
            // plaintext is never longer than ciphertext.
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            // Start decrypting.
            int decryptedByteCount = cryptoStream.Read(plainTextBytes,
                                                       0,
                                                       plainTextBytes.Length);

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert decrypted data into a string. 
            // Let us assume that the original plaintext string was UTF8-encoded.
            string plainText = Encoding.UTF8.GetString(plainTextBytes,
                                                       0,
                                                       decryptedByteCount);

            // Return decrypted string.   
            return plainText;
        }

        #endregion
    }
}
