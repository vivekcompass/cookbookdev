﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Encryption.Interfaces
{
    public interface ISha256CryptoEngine
    {
        /// <summary>
        /// this overloaded method will encrypt the Data based on encryptionKey
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        string EncryptData(string plainText, String key = "");
    }
}
