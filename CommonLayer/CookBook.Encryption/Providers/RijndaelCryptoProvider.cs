﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Encryption.Providers
{
    internal class RijndaelCryptoProvider : SymmetricEncryptTransformer
    {
        internal override ICryptoTransform GetCryptoServiceProvider(byte[] bytesKey, byte[] initVector)
        {
            ICryptoTransform cryptoTransform = null;
            using (Rijndael rijndael = new RijndaelManaged())
            {
                rijndael.Mode = CipherMode.CBC;
                // See if a key was provided
                if (null == bytesKey)
                {
                    encKey = rijndael.Key;
                }
                else
                {
                    rijndael.Key = bytesKey;
                    encKey = rijndael.Key;
                }

                // See if the client provided an initialization vector
                if (null == vector)
                { // Have the algorithm create one
                    vector = rijndael.IV;
                }
                else
                { //No, give it to the algorithm
                    rijndael.IV = vector;
                }
                cryptoTransform = rijndael.CreateEncryptor();
            }
            return cryptoTransform;
        }

        internal override ICryptoTransform GetDecryptoServiceProvider(byte[] bytesKey, byte[] initVector)
        {
            ICryptoTransform cryptoTransform = null;
            using (Rijndael rijndael = new RijndaelManaged())
            {
                rijndael.Mode = CipherMode.CBC;
                rijndael.Key = bytesKey;
                rijndael.IV = initVector;

                cryptoTransform = rijndael.CreateDecryptor();
            }
            return cryptoTransform;
        }
    }
}
