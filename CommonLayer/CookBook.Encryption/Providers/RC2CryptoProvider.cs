﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Encryption.Providers
{
    internal class RC2CryptoProvider : SymmetricEncryptTransformer
    {

        internal override ICryptoTransform GetCryptoServiceProvider(byte[] bytesKey, byte[] initVector)
        {
            ICryptoTransform cryptoTransform = null;
            using (RC2 rc2 = new RC2CryptoServiceProvider())
            {

                rc2.Mode = CipherMode.CBC;
                // See if a key was provided
                if (null == bytesKey)
                {
                    encKey = rc2.Key;
                }
                else
                {
                    rc2.Key = bytesKey;
                    encKey = rc2.Key;
                }

                // See if the client provided an initialization vector
                if (null == vector)
                { // Have the algorithm create one
                    vector = rc2.IV;
                }
                else
                { //No, give it to the algorithm
                    rc2.IV = vector;
                }
                cryptoTransform = rc2.CreateEncryptor();
            }
            return cryptoTransform;
        }

        internal override ICryptoTransform GetDecryptoServiceProvider(byte[] bytesKey, byte[] initVector)
        {
            ICryptoTransform cryptoTransform = null;
            using (RC2 rc2 = new RC2CryptoServiceProvider())
            {
                rc2.Mode = CipherMode.CBC;
                rc2.Key = bytesKey;
                rc2.IV = initVector;

                cryptoTransform = rc2.CreateEncryptor();
            }
            return cryptoTransform;
        }
    }
}
