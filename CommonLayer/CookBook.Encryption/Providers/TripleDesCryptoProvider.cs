﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Encryption.Providers
{
    internal class TripleDesCryptoProvider : SymmetricEncryptTransformer
    {

        internal override ICryptoTransform GetCryptoServiceProvider(byte[] bytesKey, byte[] initVector)
        {
            ICryptoTransform cryptoTransform = null;
            using (TripleDES des3 = new TripleDESCryptoServiceProvider())
            {

                des3.Mode = CipherMode.CBC;
                // See if a key was provided
                if (null == bytesKey)
                {
                    encKey = des3.Key;
                }
                else
                {
                    des3.Key = bytesKey;
                    encKey = des3.Key;
                }

                // See if the client provided an initialization vector
                if (null == vector)
                { // Have the algorithm create one
                    vector = des3.IV;
                }
                else
                { //No, give it to the algorithm
                    des3.IV = vector;
                }
                cryptoTransform = des3.CreateEncryptor();
            }
            return cryptoTransform;
        }

        internal override ICryptoTransform GetDecryptoServiceProvider(byte[] bytesKey, byte[] initVector)
        {
            ICryptoTransform cryptoTransform = null;
            using (TripleDES des3 = new TripleDESCryptoServiceProvider())
            {
                des3.Mode = CipherMode.CBC;
                des3.Key = bytesKey;
                des3.IV = initVector;

                cryptoTransform = des3.CreateEncryptor();
            }
            return cryptoTransform;
        }
    }
}
