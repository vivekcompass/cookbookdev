﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Encryption
{
    internal abstract class SymmetricEncryptTransformer : IDisposable
    {

        protected byte[] vector;
        protected byte[] encKey;

        internal abstract ICryptoTransform GetCryptoServiceProvider(byte[] bytesKey, byte[] initVector);
        internal abstract ICryptoTransform GetDecryptoServiceProvider(byte[] bytesKey, byte[] initVector);

        public byte[] IV
        {
            //get { return initVec; }
            set { vector = value; }
        }

        public byte[] Key
        {
            get { return encKey; }
        }

        public SymmetricEncryptTransformer()
        {

        }

        public byte[] Encrypt(byte[] bytesData, byte[] bytesKey, byte[] initVec)
        {
            byte[] streamArray = null;
            MemoryStream memStreamEncryptedData = null;
            CryptoStream encStream = null;
            ICryptoTransform transform = null;
            //Set up the stream that will hold the encrypted data.
            try
            {
                using (memStreamEncryptedData = new MemoryStream())
                {
                    this.IV = initVec;
                    transform = this.GetCryptoServiceProvider(bytesKey, initVec);
                    encStream = new CryptoStream(memStreamEncryptedData, transform, CryptoStreamMode.Write);
                    //Encrypt the data, write it to the memory stream.
                    encStream.Write(bytesData, 0, bytesData.Length);
                    //Set the IV and key for the client to retrieve
                    encKey = this.Key;
                    encStream.FlushFinalBlock();
                    streamArray = memStreamEncryptedData.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error while writing encrypted data to the stream: \n" + ex.Message);
            }
            //finally
            //{
            //    //if (memStreamEncryptedData != null)
            //    //{
            //    //    memStreamEncryptedData.Dispose();
            //    //    memStreamEncryptedData = null;
            //    //}
            //    //if (encStream != null)
            //    //{
            //    //    encStream.Dispose();
            //    //    encStream = null;
            //    //}
            //    //if (transform != null)
            //    //{
            //    //    transform.Dispose();
            //    //    transform = null;
            //    //}
            //}



            //Send the data back.
            return streamArray;
        }//end Encrypt

        public byte[] Decrypt(byte[] bytesData, byte[] bytesKey, byte[] initVec)
        {
            byte[] streamArray = null;
            MemoryStream memStreamEncryptedData = null;
            CryptoStream decStream = null;
            ICryptoTransform transform = null;
            //Set up the stream that will hold the encrypted data.
            try
            {

                this.IV = initVec;
                transform = this.GetDecryptoServiceProvider(bytesKey, initVec);
                decStream = new CryptoStream(memStreamEncryptedData, transform, CryptoStreamMode.Write);

                //Encrypt the data, write it to the memory stream.
                decStream.Write(bytesData, 0, bytesData.Length);
                //decStream.FlushFinalBlock();
                streamArray = memStreamEncryptedData.ToArray();
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error while writing encrypted data to the stream: \n" + ex.Message);
            }
            finally
            {
                //if (memStreamEncryptedData != null)
                //{
                //    memStreamEncryptedData.Dispose();
                //    memStreamEncryptedData = null;
                //}
                if (decStream != null)
                {
                    decStream.Dispose();
                    decStream = null;
                }
                //if (transform != null)
                //{
                //    transform.Dispose();
                //    transform = null;
                //}
            }




            //Send the data back.
            return streamArray;
        }//end Decrypt


        public void Dispose()
        {
            vector = null;
            encKey = null;
        }
    }
}
