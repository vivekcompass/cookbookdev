﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace CookBook.Aspects.Wrappers
{
    public class CustomExceptionLogHandler : IExceptionHandler
    {
        private readonly string logCategory;
        private readonly int eventId;
        private readonly TraceEventType severity;
        private readonly string defaultTitle;
        private readonly Type formatterType;
        private readonly int minimumPriority;
        private readonly LogWriter logWriter;
        private readonly string queuePath = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingExceptionHandler"/> class with the log category, the event ID, the <see cref="TraceEventType"/>,
        /// the title, minimum priority, the formatter type, and the <see cref="LogWriter"/>.
        /// </summary>
        /// <param name="logCategory">The default category</param>
        /// <param name="eventId">An event id.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="title">The log title.</param>
        /// <param name="priority">The minimum priority.</param>
        /// <param name="formatterType">The type <see cref="ExceptionFormatter"/> type.</param>
        /// <param name="writer">The <see cref="LogWriter"/> to use.</param>
        /// <remarks>
        /// The type specified for the <paramref name="formatterType"/> attribute must have a public constructor with
        /// parameters of type <see name="TextWriter"/>, <see cref="Exception"/> and <see cref="Guid"/>.
        /// </remarks>
        public CustomExceptionLogHandler(
            string logCategory,
            int eventId,
            TraceEventType severity,
            string title,
            int priority,
            Type formatterType,
            LogWriter writer)
        {
            this.queuePath = string.Empty;
            this.logCategory = logCategory;
            this.eventId = eventId;
            this.severity = severity;
            this.defaultTitle = title;
            this.minimumPriority = priority;
            this.formatterType = formatterType;
            this.logWriter = writer;
        }

        /// <summary>
        /// <para>Handles the specified <see cref="Exception"/> object by formatting it and writing to the configured log.</para>
        /// </summary>
        /// <param name="exception"><para>The exception to handle.</para></param>        
        /// <param name="handlingInstanceId">
        /// <para>The unique ID attached to the handling chain for this handling instance.</para>
        /// </param>
        /// <returns><para>Modified exception to pass to the next handler in the chain.</para></returns>
        public Exception HandleException(Exception exception, Guid handlingInstanceId)
        {
            WriteToLog(CreateMessage(exception, handlingInstanceId), exception.Data);
            return exception;
        }

        /// <summary>
        /// Writes the specified log message using 
        /// the Logging Application Block's <see cref="Logger.Write(LogEntry)"/>
        /// method.
        /// </summary>
        /// <param name="logMessage">The message to write to the log.</param>
        /// <param name="exceptionData">The exception's data.</param>
        protected virtual void WriteToLog(string logMessage, IDictionary exceptionData)
        {
            LogEntry entry = new LogEntry(
                logMessage,
                logCategory,
                minimumPriority,
                eventId,
                severity,
                defaultTitle,
                null);
            if (logCategory == "MSMQ")
            {
                using (MessageQueue msgQ = new MessageQueue(queuePath))
                {
                    msgQ.Send(logMessage);
                }
            }
            foreach (DictionaryEntry dataEntry in exceptionData)
            {
                if (dataEntry.Key is string)
                {
                    entry.ExtendedProperties.Add(dataEntry.Key as string, dataEntry.Value);
                }
            }

            this.logWriter.Write(entry);
        }

        /// <summary>
        /// Creates an instance of the <see cref="StringWriter"/>
        /// class using its default constructor.
        /// </summary>
        /// <returns>A newly created <see cref="StringWriter"/></returns>
        protected virtual StringWriter CreateStringWriter()
        {
            return new StringWriter(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Creates an <see cref="ExceptionFormatter"/>
        /// object based on the configured ExceptionFormatter
        /// type name.
        /// </summary>
        /// <param name="writer">The stream to write to.</param>
        /// <param name="exception">The <see cref="Exception"/> to pass into the formatter.</param>
        /// <param name="handlingInstanceID">The id of the handling chain.</param>
        /// <returns>A newly created <see cref="ExceptionFormatter"/></returns>
        protected virtual Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.ExceptionFormatter CreateFormatter(
            StringWriter writer,
            Exception exception,
            Guid handlingInstanceID)
        {
            ConstructorInfo constructor = GetFormatterConstructor();
            return (Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.ExceptionFormatter)constructor.Invoke(new object[] { writer, exception, handlingInstanceID });
        }

        private ConstructorInfo GetFormatterConstructor()
        {
            Type[] types = new Type[] { typeof(TextWriter), typeof(Exception), typeof(Guid) };
            ConstructorInfo constructor = formatterType.GetConstructor(types);
            if (constructor == null)
            {
                throw new Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.ExceptionHandlingException(
                    string.Format(CultureInfo.CurrentCulture, "The configured exception formatter '{0}' must expose a public constructor that takes a TextWriter object, an Exception object and a GUID instance as parameters.", formatterType.AssemblyQualifiedName));
            }
            return constructor;
        }

        private string CreateMessage(Exception exception, Guid handlingInstanceID)
        {
            StringWriter writer = null;
            StringBuilder stringBuilder = null;
            try
            {
                writer = CreateStringWriter();
                Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.ExceptionFormatter formatter = CreateFormatter(writer, exception, handlingInstanceID);
                formatter.Format();
                stringBuilder = writer.GetStringBuilder();

            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
            }

            return stringBuilder.ToString();
        }
    }
}
