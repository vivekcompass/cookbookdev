﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Aspects.Wrappers
{
    internal class ExceptionHandlingLoggingDesigntime
    {
        public static class ValidatorTypes
        {
            public const string LogFormatterValidator = "Microsoft.Practices.EnterpriseLibrary.Configuration.Design.ViewModel.BlockSpecifics.Logging.LogFormatterValidator, Microsoft.Practices.EnterpriseLibrary.Configuration.DesignTime";
        }
    }
}
