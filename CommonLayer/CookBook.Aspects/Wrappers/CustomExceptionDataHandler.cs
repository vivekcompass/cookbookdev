﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design.Validation;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;

namespace CookBook.Aspects.Wrappers
{
    public class CustomExceptionDataHandler : ExceptionHandlerData
    {
        private const string LOGCATEGORY = "logCategory";
        private const string EVENTID = "eventId";
        private const string SEVERITY = "severity";
        private const string TITLE = "title";
        private const string FORMATTERTYPE = "formatterType";
        private const string PRIORITY = "priority";
        private const string USEDEFAULTLOGGER = "useDefaultLogger";
        private static readonly AssemblyQualifiedTypeNameConverter typeConverter = new AssemblyQualifiedTypeNameConverter();

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomExceptionDataHandler" /> class
        /// </summary>
        public CustomExceptionDataHandler()
            : base(typeof(LoggingExceptionHandler))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingExceptionHandlerData"/> class.
        /// </summary>
        /// <param name="name">
        /// The name of the handler.
        /// </param>
        /// <param name="logCategory">
        /// The default log category.
        /// </param>
        /// <param name="eventId">
        /// The default eventID.
        /// </param>
        /// <param name="severity">
        /// The default severity.
        /// </param>
        /// <param name="title">
        /// The default title.
        /// </param>
        /// <param name="formatterType">
        /// The formatter type.
        /// </param>
        /// <param name="priority">
        /// The minimum value for messages to be processed.  Messages with a priority below the minimum are dropped immediately on the client.
        /// </param>
        public CustomExceptionDataHandler(string name,
                                           string logCategory,
                                           int eventId,
                                           TraceEventType severity,
                                           string title,
                                           Type formatterType,
                                           int priority)
            : this(name, logCategory, eventId, severity, title, typeConverter.ConvertToString(formatterType), priority)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingExceptionHandlerData"/> class.
        /// </summary>
        /// <param name="name">
        /// The name of the handler.
        /// </param>
        /// <param name="logCategory">
        /// The default log category.
        /// </param>
        /// <param name="eventId">
        /// The default eventID.
        /// </param>
        /// <param name="severity">
        /// The default severity.
        /// </param>
        /// <param name="title">
        /// The default title.
        /// </param>
        /// <param name="formatterTypeName">
        /// The formatter fully qualified assembly type name.
        /// </param>
        /// <param name="priority">
        /// The minimum value for messages to be processed.  Messages with a priority below the minimum are dropped immediately on the client.
        /// </param>
        public CustomExceptionDataHandler(string name,
                                           string logCategory,
                                           int eventId,
                                           TraceEventType severity,
                                           string title,
                                           string formatterTypeName,
                                           int priority)
            : base(name, typeof(LoggingExceptionHandler))
        {
            LogCategory = logCategory;
            EventId = eventId;
            Severity = severity;
            Title = title;
            FormatterTypeName = formatterTypeName;
            Priority = priority;
        }

        /// <summary>
        /// Gets or sets the default log category.
        /// </summary>
        [ConfigurationProperty(LOGCATEGORY, IsRequired = true)]
        [Reference(typeof(NamedElementCollection<TraceSourceData>), typeof(TraceSourceData))]
        [ResourceDescription(typeof(ExceptionResources), "LoggingExceptionHandlerDataLogCategoryDescription")]
        [ResourceDisplayName(typeof(ExceptionResources), "LoggingExceptionHandlerDataLogCategoryDisplayName")]
        public string LogCategory
        {
            get { return (string)this[LOGCATEGORY]; }
            set { this[LOGCATEGORY] = value; }
        }

        /// <summary>
        /// Gets or sets the default event ID.
        /// </summary>
        [ConfigurationProperty(EVENTID, IsRequired = true, DefaultValue = 100)]
        [ResourceDescription(typeof(ExceptionResources), "LoggingExceptionHandlerDataEventIdDescription")]
        [ResourceDisplayName(typeof(ExceptionResources), "LoggingExceptionHandlerDataEventIdDisplayName")]
        public int EventId
        {
            get { return (int)this[EVENTID]; }
            set { this[EVENTID] = value; }
        }

        /// <summary>
        /// Gets or sets the default severity.
        /// </summary>
        [ConfigurationProperty(SEVERITY, IsRequired = true, DefaultValue = TraceEventType.Error)]
        [ResourceDescription(typeof(ExceptionResources), "LoggingExceptionHandlerDataSeverityDescription")]
        [ResourceDisplayName(typeof(ExceptionResources), "LoggingExceptionHandlerDataSeverityDisplayName")]
        public TraceEventType Severity
        {
            get { return (TraceEventType)this[SEVERITY]; }
            set { this[SEVERITY] = value; }
        }

        /// <summary>
        ///  Gets or sets the default title.
        /// </summary>
        [ConfigurationProperty(TITLE, IsRequired = true, DefaultValue = "Enterprise Library Exception Handling")]
        [ResourceDescription(typeof(ExceptionResources), "LoggingExceptionHandlerDataTitleDescription")]
        [ResourceDisplayName(typeof(ExceptionResources), "LoggingExceptionHandlerDataTitleDisplayName")]
        public string Title
        {
            get { return (string)this[TITLE]; }
            set { this[TITLE] = value; }
        }

        /// <summary>
        /// Gets or sets the formatter type.
        /// </summary>
        public Type FormatterType
        {
            get { return (Type)typeConverter.ConvertFrom(FormatterTypeName); }
            set { FormatterTypeName = typeConverter.ConvertToString(value); }
        }

        /// <summary>
        /// Gets or sets the formatter fully qualified assembly type name.
        /// </summary>
        /// <value>
        /// The formatter fully qualified assembly type name
        /// </value>
        [ConfigurationProperty(FORMATTERTYPE, IsRequired = true)]
        [ResourceDescription(typeof(ExceptionResources), "LoggingExceptionHandlerDataFormatterTypeNameDescription")]
        [ResourceDisplayName(typeof(ExceptionResources), "LoggingExceptionHandlerDataFormatterTypeNameDisplayName")]
        [Editor(CommonDesignTime.EditorTypes.TypeSelector, CommonDesignTime.EditorTypes.UITypeEditor)]
        [BaseType(typeof(Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.ExceptionFormatter))]
        [DesigntimeDefaultAttribute("Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling")]
        [Validation(ExceptionHandlingLoggingDesigntime.ValidatorTypes.LogFormatterValidator)]
        public string FormatterTypeName
        {
            get { return (string)this[FORMATTERTYPE]; }
            set { this[FORMATTERTYPE] = value; }
        }

        /// <summary>
        /// Gets or sets the minimum value for messages to be processed.  Messages with a priority
        /// below the minimum are dropped immediately on the client.
        /// </summary>
        [ResourceDescription(typeof(ExceptionResources), "LoggingExceptionHandlerDataPriorityDescription")]
        [ResourceDisplayName(typeof(ExceptionResources), "LoggingExceptionHandlerDataPriorityDisplayName")]
        [ConfigurationProperty(PRIORITY, IsRequired = true)]
        public int Priority
        {
            get { return (int)this[PRIORITY]; }
            set { this[PRIORITY] = value; }
        }

        /// <summary>
        /// Gets or sets the default logger to be used.
        /// </summary>
        [ConfigurationProperty(USEDEFAULTLOGGER, IsRequired = false, DefaultValue = false)]
        [Obsolete("Behavior is limited to UseDefaultLogger = true")]
        [Browsable(false)]
        public bool UseDefaultLogger
        {
            get { return (bool)this[USEDEFAULTLOGGER]; }
            set { this[USEDEFAULTLOGGER] = value; }
        }

        /// <summary>
        /// Builds the exception handler represented by this configuration object.
        /// </summary>
        /// <returns>An <see cref="IExceptionHandler"/>.</returns>
        public override IExceptionHandler BuildExceptionHandler()
        {
            Type exceptionFormatterType;
            try
            {
                exceptionFormatterType = this.FormatterType;
            }
            catch (ArgumentException e)
            {
                throw new ConfigurationErrorsException("The formatter type is not set or does not represent a type.", e, this.ElementInformation.Source, this.ElementInformation.LineNumber);
            }

            if (exceptionFormatterType == null)
            {
                throw new ConfigurationErrorsException("The formatter type is not set or does not represent a type.", this.ElementInformation.Source, this.ElementInformation.LineNumber);
            }

            if (!exceptionFormatterType.IsSubclassOf(typeof(Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.ExceptionFormatter)))
            {
                throw new ConfigurationErrorsException("The type specified for the formatter is not a formatter type.", this.ElementInformation.Source, this.ElementInformation.LineNumber);
            }

            return new CustomExceptionLogHandler(this.LogCategory, this.EventId, this.Severity, this.Title, this.Priority, exceptionFormatterType, Logger.Writer);
        }

    }
}
