﻿using System;
using System.IO;
using System.Web;
using System.Configuration;
using System.Net;
using CookBook.Aspects.Factory;

namespace CookBook.Aspects.Utils
{
    public class FileTransferUtility
    {
        public static byte[] DownloadFile(string fileName)
        {
            string userName = ConfigurationManager.AppSettings["fileServerUserCode"];
            string password = ConfigurationManager.AppSettings["fileServerPassword"];
            string domainName = ConfigurationManager.AppSettings["fileServerDomain"];
            string downloadNetwork = ConfigurationManager.AppSettings["fileServerNetwork"];

            byte[] bytes;
            MemoryStream msinput = new MemoryStream();
            NetworkCredential NCredentials = new NetworkCredential(userName, password, domainName);

            using (NetworkConnection NCR = new NetworkConnection(downloadNetwork, NCredentials))
            {
                string path = downloadNetwork + @"\" + fileName;
                LogTraceFactory.WriteLogWithCategory("The Download path" + " " + path, Aspects.Constants.LogTraceCategoryNames.Important);
                FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
                bytes = new byte[file.Length];
                file.Read(bytes, 0, (int)file.Length);
                return bytes;
            }
        }

        public static void UploadFile(string userCode, string fileName, MemoryStream msinput)
        {
            string userName = ConfigurationManager.AppSettings["fileServerUserCode"];
            string password = ConfigurationManager.AppSettings["fileServerPassword"];
            string domainName = ConfigurationManager.AppSettings["fileServerDomain"];
            string downloadNetwork = ConfigurationManager.AppSettings["fileServerNetwork"];

            LogTraceFactory.WriteLogWithCategory(userName + " " + password + " " + domainName + " " + downloadNetwork + " " + "Entered Upload File" , Aspects.Constants.LogTraceCategoryNames.Important);
            NetworkCredential NCredentials = new NetworkCredential(userName, password, domainName);
            using (NetworkConnection NCR = new NetworkConnection(downloadNetwork, NCredentials))
            {
                string path = CheckDestinationFolder(userCode, downloadNetwork);
                path += @"\" + fileName;
                LogTraceFactory.WriteLogWithCategory("The Upload path" + " " + path, Aspects.Constants.LogTraceCategoryNames.Important);
                FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write);
                msinput.WriteTo(file);
                file.Close();
                msinput.Close();
            }

        }

        private static string CheckDestinationFolder(string userCode, string path)
        {
            string mainFolderPath = path + @"\" + userCode;
            bool exists = System.IO.Directory.Exists(mainFolderPath);
            if (!exists)
                System.IO.Directory.CreateDirectory(mainFolderPath);
            return mainFolderPath;
        }

        public static string SaveFile(string userCode, HttpPostedFileBase file, string type = "")
        {
            string FileNameWithOutExtension = Path.GetFileNameWithoutExtension(file.FileName);
            string FileExtension = Path.GetExtension(file.FileName).ToLower();

            string NewFileName = GetFileName(FileNameWithOutExtension, FileExtension, type);

            return userCode + '/' + NewFileName;
        }

        private static string GetFileName(string FileNameWithOutExtension, string FileExtension, string type = "")
        {
            string RandomNo = DateTime.Now.Ticks.ToString();
            string NewFileName = "";
            if (type.Split('_').Length > 1) { NewFileName = type + "_" + RandomNo.ToString(); }
            else { NewFileName = FileNameWithOutExtension.Trim().Replace(" ", "-").Replace("&", "and").Replace("?", "-").Replace("%", "-").Replace(".", "-").Replace("'", "-").Replace("#", "-").Replace("\\", "-").Replace("/", "-") + "_" + RandomNo.ToString(); }
            NewFileName = NewFileName.ToLower() + FileExtension;
            return NewFileName;
        }

        public bool isImageExist(string path)
        {
            bool isExist;
            string userName = ConfigurationManager.AppSettings["fileServerUserCode"];
            string password = ConfigurationManager.AppSettings["fileServerPassword"];
            string domainName = ConfigurationManager.AppSettings["fileServerDomain"];
            string downloadNetwork = ConfigurationManager.AppSettings["fileServerNetwork"];
            NetworkCredential NCredentials = new NetworkCredential(userName, password, domainName);
            using (NetworkConnection NCR = new NetworkConnection(downloadNetwork, NCredentials))
            {
                if (System.IO.File.Exists(path))
                {
                    isExist = true;
                }
                else
                {
                    isExist = false; 
                }
            }
            return isExist;
        } 
        
    }
}
