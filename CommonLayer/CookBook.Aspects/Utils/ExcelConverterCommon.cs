﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using CookBook.Data.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace CookBook.Dealer.MvcApp.Helper
{
    public static class ExcelConverterCommon
    {
        /// <summary>
        /// Provides excel file in byte format.
        /// </summary>
        /// <param name="dt">DataTable representation for excel data</param>
        /// <returns></returns>
        public static void ExportListUsingEPPlus(DataTable dt, string fileName, string fileTitle, CookBook.Data.Export.ExportData exportData, Dictionary<string, string> searchCriteria = null, bool isHeader = false)
        {
            Byte[] bytes;

            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                using (var memoryStream = new MemoryStream())
                {
                    int row = 0;
                    excelPackage.Workbook.Properties.Author = "System";
                    excelPackage.Workbook.Properties.Created = DateTime.Now;
                    int columns = dt.Columns.Count;
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Sheet 1");
                    if (exportData != null)
                    {
                        if (isHeader)
                        {
                            string applicationLogoPath = ConfigurationManager.AppSettings["applicationLogoPath"];
                            string imagepath = HttpContext.Current.Server.MapPath(applicationLogoPath);
                            var logo = new Bitmap(Image.FromFile(imagepath), new Size(100, 25));
                            var picture = worksheet.Drawings.AddPicture(string.Empty, logo);
                            picture.SetPosition(25, 15);
                            row++;
                            worksheet.Cells[row, 1].Value = "Downloaded Date: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                            worksheet.Cells[row, 1, row, columns].Merge = true;
                            worksheet.Cells[row, 1, row, columns].Style.Font.Bold = true;
                            worksheet.Cells[row, 1, row, columns].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            worksheet.Cells[row, 1, row, columns].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.White);
                            row++;
                            worksheet.Cells[row, 1].Value = "Panasonic India Private Limited";
                            worksheet.Cells[row, 1, row, columns].Merge = true;
                            worksheet.Cells[row, 1, row, columns].Style.Font.Bold = true;
                            worksheet.Cells[row, 1, row, columns].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Cells[row, 1, row, columns].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.White);

                            row++;
                            worksheet.Cells[row, 1, row, columns].Merge = true;
                            worksheet.Cells[row, 1, row, columns].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.White);
                            row++;
                            worksheet.Cells[row, 1].Value = fileTitle;
                            worksheet.Cells[row, 1, row, columns].Merge = true;
                            worksheet.Cells[row, 1, row, columns].Style.Font.Bold = true;
                            worksheet.Cells[row, 1, row, columns].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Cells[row, 1, row, columns].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.White);
                            row++;
                            worksheet.Cells[row, 1, row, columns].Merge = true;
                            worksheet.Cells[row, 1, row, columns].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.White);
                            if (!string.IsNullOrEmpty(exportData.FromDate))
                            {
                                row++;
                                worksheet.Cells[row, 1].Value = "Period: " + exportData.FromDate + " TO " + exportData.ToDate;
                                worksheet.Cells[row, 1, row, columns].Merge = true;
                                worksheet.Cells[row, 1, row, columns].Style.Font.Bold = true;
                                worksheet.Cells[row, 1, row, columns].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                worksheet.Cells[row, 1, row, columns].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.White);
                            }


                            row++;
                            worksheet.Cells[row, 1].Value = "Currency           :";
                            worksheet.Cells[row, 1].Style.Font.Bold = true;
                            worksheet.Cells[row, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.White);

                            worksheet.Cells[row, 2].Value = "INR(Indian Rupee)";
                            worksheet.Cells[row, 2, row, columns].Merge = true;
                            worksheet.Cells[row, 2, row, columns].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            worksheet.Cells[row, 2, row, columns].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.White);
                            row += 2;

                            worksheet.Cells[row, 1].Value = "Search Criteria :";
                            worksheet.Cells[row, 1].Style.Font.Bold = true;
                            row++;
                        }
                    }
                    row++;
                    if (dt.Rows.Count == 0)
                    {
                        dt.Rows.Add(dt.NewRow());
                        worksheet.Cells[row, 1].LoadFromDataTable(dt, true);
                        dt.Rows.RemoveAt(0);
                    }
                    else
                    {
                        worksheet.Cells[row, 1].LoadFromDataTable(dt, true);
                    }

                    worksheet.Cells.AutoFitColumns();
                    var headerCell = worksheet.Cells[row, 1, row, columns];
                    headerCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    var headerFont = headerCell.Style.Font;
                    headerFont.SetFromFont(new System.Drawing.Font("Times New Roman", 12));
                    headerFont.Bold = true;
                    var headerFill = headerCell.Style.Fill;
                    headerFill.PatternType = ExcelFillStyle.Solid;
                    headerFill.BackgroundColor.SetColor(System.Drawing.Color.SkyBlue);
                    var parentCell = worksheet.Cells[row, 1, row, columns];
                    parentCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    for (int j = 1; j <= columns; j++)
                    {
                        worksheet.Column(j).Width = 20;
                    }

                    if (exportData != null && !string.IsNullOrEmpty(exportData.ColumnsToHorizontalAlign) && dt.Rows.Count > 0)
                    {
                        string intFormat = Convert.ToString(ConfigurationManager.AppSettings["IntFormat"]);
                        string decimalFormat = Convert.ToString(ConfigurationManager.AppSettings["DecimalFormat"]);
                        //worksheet.View.FreezePanes(row + 1, 1);
                        List<string> colArray = exportData.ColumnsToHorizontalAlign.Split(',').ToList();
                        foreach (var col in colArray)
                        {
                            int n = Convert.ToInt32(col);
                            n = n + 1;
                            int d = 0;
                            string columnLetters = string.Empty;
                            do
                            {
                                d = (n - 1) % 26;
                                columnLetters += (char)(65 + d);
                                n = (n - d) / 26;
                            }
                            while (n > 0);
                            if (columnLetters.Length > 1)
                            {
                                char[] letters = columnLetters.ToArray();
                                Array.Reverse(letters);
                                columnLetters = new String(letters);
                            }

                            var cells = worksheet.Cells[columnLetters + Convert.ToString(row + 1) + ":" + columnLetters + worksheet.Dimension.End.Row];
                            cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            Type colType = dt.Columns[Convert.ToInt32(col)].DataType;
                            bool isIntType = colType == typeof(Int32);
                            bool isLongType = colType == typeof(Int64);
                            bool isDecimalType = colType == typeof(decimal);
                            foreach (var cell in cells)
                            {
                                if (isIntType)
                                {
                                    int val = Convert.ToInt32(cell.Value);
                                    cell.Value = val;
                                    if (val != 0)
                                        cell.Style.Numberformat.Format = intFormat;
                                }
                                else if (isLongType)
                                {
                                    long val = Convert.ToInt64(cell.Value);
                                    cell.Value = val;
                                    if (val != 0)
                                        cell.Style.Numberformat.Format = intFormat;
                                }
                                else if (isDecimalType)
                                {
                                    decimal val = Convert.ToDecimal(cell.Value);
                                    cell.Value = val;
                                    if (val != 0)
                                        cell.Style.Numberformat.Format = decimalFormat;
                                }
                            }
                        }
                    }
                    excelPackage.SaveAs(memoryStream);
                    bytes = memoryStream.ToArray();
                }
            }
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=" + fileName + ".xlsx");
            HttpContext.Current.Response.BinaryWrite(bytes);
            HttpContext.Current.Response.End();
        }

    }
}