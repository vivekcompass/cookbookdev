﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.Practices.Unity.InterceptionExtension;
using CookBook.Aspects.Constants;
using CookBook.Aspects.Factory;

namespace CookBook.Aspects.Behaviors
{
    public class CustomPolicyInjectionBehavior : ICallHandler
    {

        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            if (getNext() == null)
            {
                throw new ArgumentNullException("getNext");
            }
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }
            IMethodReturn methodReturn = getNext().Invoke(input, getNext);
            if (methodReturn.Exception != null && !methodReturn.Exception.StackTrace.Contains("Microsoft.Practices.EnterpriseLibrary.ExceptionHandling"))
            {
                ExceptionFactory.AppExceptionManager.HandleException(methodReturn.Exception, ExceptionPolicyNames.AssistingAdministrators.ToString());
            }
            return methodReturn;
        }

        public int Order
        {
            get;
            set;
        }
    }
}
