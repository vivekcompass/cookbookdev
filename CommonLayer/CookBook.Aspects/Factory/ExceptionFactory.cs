﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace CookBook.Aspects.Factory
{
    public static class ExceptionFactory
    {
        //private variable for exception manager instance
        private static ExceptionManager exceptionManager;

        /// <summary>
        /// Property to get set exception manager instance
        /// </summary>
        public static ExceptionManager AppExceptionManager
        {
            get { return exceptionManager; }
        }

        public static InvalidOperationException Create(string format, params object[] args)
        {
            return new InvalidOperationException(string.Format(format, args));
        }

        public static InvalidOperationException Create(Exception innerException, string format, params object[] args)
        {
            return new InvalidOperationException(string.Format(format, args), innerException);
        }

        public static TException Create<TException>(string format, params object[] args)
            where TException : Exception
        {
            return (TException)Activator.CreateInstance(typeof(TException), string.Format(format, args));
        }

        public static TException Create<TException>(Exception innerException, string format, params object[] args)
            where TException : Exception
        {
            return (TException)Activator.CreateInstance(typeof(TException), string.Format(format, args), innerException);
        }

        /// <summary>
        /// Method to initialize the enterprise library exception handler instance under the Aop
        /// </summary>
        public static void InitializeExceptionAopFramework()
        {
            try
            {
                using (IConfigurationSource config = ConfigurationSourceFactory.Create())
                {
                    ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
                    exceptionManager = factory.CreateManager();
                    ExceptionPolicy.SetExceptionManager(exceptionManager);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
