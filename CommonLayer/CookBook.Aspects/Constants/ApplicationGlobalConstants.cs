﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Aspects.Constants
{
    public class ApplicationGlobalConstants
    {
        /// <summary>
        /// This constant has define to use the encryption & decryption of AES key.
        /// </summary>
        public const string AES_ENCRYPTDECRYPT_KEY = "LcNZtYA7tLPbqUBH";

        /// <summary>
        /// This constant has define to use the encryption & decryption of Normal key.
        /// </summary>
        public const string NORMAL_ENCRYPTDECRYPT_KEY = "!#$a54?3";
    }
}
