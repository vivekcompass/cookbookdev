﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Aspects.Constants
{
    /// <summary>
    /// Enum to define category names for logging and tracing in application
    /// </summary>
    public enum LogTraceCategoryNames
    {
        General,
        Tracing,
        DiskFiles,
        Important,
        BlockedByFilter,

    }
}
