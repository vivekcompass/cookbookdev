﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Aspects.Constants
{
    public enum GeographicLocations
    {
        [Description("National")]
        National,
        [Description("Region")]
        Region,
        [Description("State")]
        State,
        [Description("SubState")]
        SubState,
        [Description("Site")]
        Site,
    }

    public enum Regions
    {
        [Description("NORTH")]
        North,
        [Description("SOUTH")]
        South,
        [Description("EAST")]
        East,
        [Description("WEST")]
        West
    }
}
