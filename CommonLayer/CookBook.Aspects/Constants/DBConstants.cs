﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Aspects.Constants
{
    public class DBConstants
    {
        public const string Physical = "Physical";
        public const string Biological = "Biological";
        public const string All = "All";
    }
}
