﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Aspects.Constants
{
    public enum  ExceptionPolicyNames
    {
        AssistingAdministrators,
        ExceptionShielding,
        LoggingAndReplacingException,
    }

    public enum ListStatus
    {
        Add,
        Delete,
        Modify,
    }
}
