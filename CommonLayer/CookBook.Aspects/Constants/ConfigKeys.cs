﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Aspects.Constants
{
    public enum ConfigKeys
    {
        LoginSessionHeaderKey,
        ServiceHeaderUsername,
        IsServiceUsageLogEnabled,
        ServiceURL,
        MethodLevelLoggingEnabled,
        APLServiceRepeatDateOfEveryMonth,
        APLServiceRepeatHourOfEveryMonth,
        APLServiceRepeatMinuteOfEveryMonth,
        FacebookFeedServiceRunningTimeHour,
        APLRunningTimeMinuteImm,
        APLRunningTimeMinute,
        APLRunningTimeHour,
        APLRunningTimeDay
    }
}
