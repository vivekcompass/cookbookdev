﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace CookBook.Ioc.Wcf
{
    public class UnityInstanceContextExtension : IExtension<InstanceContext>
    {
        private IUnityContainer ChildContainer { get; set; }


        public IUnityContainer GetChildContainer(IUnityContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            return ChildContainer ?? (ChildContainer = container.CreateChildContainer());
        }

        public void DisposeOfChildContainer()
        {
            if (ChildContainer != null)
            {
                ChildContainer.Dispose();
            }
        }

        public void Attach(InstanceContext owner)
        {
            //Does not require any implementation
        }

        public void Detach(InstanceContext owner)
        {
            //Does not require any implementation
        }
    }
}
