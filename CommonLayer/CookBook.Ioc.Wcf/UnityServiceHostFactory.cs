﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using CookBook.Ioc;

namespace CookBook.Ioc.Wcf
{
    public class UnityServiceHostFactory : ServiceHostFactory
    {
        protected IUnityContainer Container { get; set; }

        public UnityServiceHostFactory()
        {
            this.Container = new IocManager().Container;
        }

        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            return new UnityServiceHost(this.Container, serviceType, baseAddresses);
        }
    }
}