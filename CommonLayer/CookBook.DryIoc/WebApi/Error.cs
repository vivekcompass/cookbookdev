﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReferredDryIoc = DryIoc;

namespace CookBook.DryIoc.WebApi
{
    /// <summary>Possible web exceptions.</summary>
    public static class Error
    {
#pragma warning disable 1591 // "Missing XML-comment"
        public static readonly int
            RequestMessageDoesnotReferenceDryiocDependencyScope = ReferredDryIoc.Error.Of(
                "Expecting request message dependency scope to be of type {1} but found: {0}.");
#pragma warning restore 1591
    }
}
