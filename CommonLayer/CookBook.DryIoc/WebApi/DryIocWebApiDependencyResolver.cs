﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;
using DryIoc;

namespace CookBook.DryIoc.WebApi
{
    public sealed class  DryIocWebApiDependencyResolver : IDependencyResolver
    {
        /// <summary>Original DryIoc container.</summary>
        public readonly IContainer Container;

        private readonly Func<Type, bool> throwExceptionIfUnresolved;

        /// <summary>Creates dependency resolver.</summary>
        /// <param name="container">Container.</param>
        /// <param name="throwIfUnresolved">(optional) Instructs DryIoc to throw exception
        /// for unresolved type instead of fallback to default Resolver.</param>
        internal DryIocWebApiDependencyResolver(IContainer container, Func<Type, bool> throwIfUnresolved = null)
        {
            Container = container;
            throwExceptionIfUnresolved = throwIfUnresolved;
        }

        /// <summary>Disposes container.</summary>
        public void Dispose()
        {
            if (Container != null)
            {
                Container.Dispose();
            }
        }

        /// <summary>Retrieves a service from the scope or null if unable to resolve service.</summary>
        /// <returns>The retrieved service.</returns> <param name="serviceType">The service to be retrieved.</param>
        public object GetService(Type serviceType)
        {
            var ifUnresolvedReturnDefault = throwExceptionIfUnresolved == null || !throwExceptionIfUnresolved(serviceType);
            return Container.Resolve(serviceType, ifUnresolvedReturnDefault);
        }

        /// <summary>Retrieves a collection of services from the scope or empty collection.</summary>
        /// <returns>The retrieved collection of services.</returns>
        /// <param name="serviceType">The collection of services to be retrieved.</param>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return Container.ResolveMany<object>(serviceType);
        }

        /// <summary>Opens scope from underlying container.</summary>
        /// <returns>Opened scope wrapped in dependency scope.</returns>
        public IDependencyScope BeginScope()
        {
            var scope = Container.OpenScope(Reuse.WebRequestScopeName);
            return new DryIocWebApiDependencyScope(scope, throwExceptionIfUnresolved);
        }
    }
}
