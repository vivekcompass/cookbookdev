﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using DryIoc;
using System.Web.Http;

namespace CookBook.DryIoc.WebApi
{
    public class DryIocControllerFactory : IControllerFactory
    {
        private IContainer iocContainer;
        public DryIocControllerFactory(IContainer container)
        {
            iocContainer = container;
        }

        public IController CreateController(RequestContext requestContext, string controllerName)
        {
            return iocContainer.Resolve<IController>(controllerName, IfUnresolved.ReturnDefault);
        }

        public SessionStateBehavior GetControllerSessionBehavior(RequestContext requestContext, string controllerName)
        {
            throw new NotImplementedException();
        }
      
        public void ReleaseController(IController controller)
        {
            var disposable = controller as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }
        }
    }
}
