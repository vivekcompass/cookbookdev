﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DryIoc;
using Microsoft.Practices.ServiceLocation;

namespace CookBook.DryIoc
{
    public static class DryIocMvcExtensions
    {
        public static IDryIocManager WithMvc(this IDryIocManager container, IEnumerable<Assembly> controllerAssemblies = null)
        {
            if(container != null)
            {
                var iocContainer = ServiceLocator.Current.GetInstance<IContainer>();
                if (iocContainer != null)
                {
                    iocContainer.WithMvc(controllerAssemblies);
                }
            }
            return container;
        }
    }
}
