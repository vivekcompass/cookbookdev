﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DryIoc
{
    public enum HostType
    {
        Webforms,
        Mvc,
        WebApi,
        Wcf,
        WindowService,
        Console,
        Assembly,
        None,
    }
}
