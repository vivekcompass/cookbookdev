﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.DryIoc.Attributes
{
    /// <summary>
    /// A strategy that is used at build plan execution time
    /// to resolve a dependent value.
    /// </summary>
    public interface IDependencyResolverPolicy 
    {
        /// <summary>
        /// Get the value for a dependency.
        /// </summary>
        /// <returns>The value for the dependency.</returns>
        object Resolve();
    }
}
