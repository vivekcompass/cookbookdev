﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DryIoc;

namespace CookBook.DryIoc
{
    public interface IDryIocManager
    {
        T Resolve<T>();

        T Resolve<T>(string keyName);

        object Resolve(Type type, string keyName);

        void ExplicitRegister<T>(string keyName);
     
    }
}
