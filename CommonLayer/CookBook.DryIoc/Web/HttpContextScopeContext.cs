﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DryIoc;

namespace CookBook.DryIoc.Web
{
    /// <summary>Stores current scope in <see cref="HttpContext.Items"/>.</summary>
    /// <remarks>Stateless context, so could be created multiple times and used from different places without side-effects.</remarks>
    //public sealed class HttpContextScopeContext : IScopeContext, IDisposable
    //{
    //    private readonly string currentScopeEntryKey;
        

    //    /// <summary>Fixed root scope name for the context.</summary>
    //    public static readonly string ScopeContextName = typeof(HttpContextScopeContext).FullName;

    //    /// <summary>Provides default context items dictionary using <see cref="HttpContext.Current"/>.
    //    /// Could be overridden with any key-value dictionary where <see cref="HttpContext"/> is not available, e.g. in tests.</summary>
    //    public static Func<IDictionary> GetContextItems = () =>
    //        HttpContext.Current.ThrowIfNull(Error.Of("No HttpContext is available.")).Items;

    //    /// <summary>Creates the context optionally with arbitrary/test items storage.</summary>
    //    /// <param name="getContextItems">(optional) Arbitrary/test items storage.</param>
    //    public HttpContextScopeContext(Func<IDictionary> getContextItems = null)
    //    {
    //        currentScopeEntryKey = RootScopeName;
    //        getContextItems = getContextItems ?? GetContextItems;
    //    }

        

    //    /// <summary>Returns fixed name.</summary>
    //    public string RootScopeName { get { return ScopeContextName; } }

    //    /// <summary>Returns current ambient scope stored in item storage.</summary> <returns>Current scope or null if there is no.</returns>
    //    public IScope GetCurrentOrDefault()
    //    {
    //        if (getContextItems == null)
    //        {
    //            return null;
    //        }
    //        return getContextItems()[currentScopeEntryKey] as IScope;
    //    }

    //    /// <summary>Sets the new scope as current using existing current as input.</summary>
    //    /// <param name="setCurrentScope">Delegate to get new scope.</param>
    //    /// <returns>New current scope.</returns>
    //    public IScope SetCurrent(SetCurrentScopeHandler setCurrentScope)
    //    {
    //        var newCurrentScope = setCurrentScope.ThrowIfNull()(GetCurrentOrDefault());
    //        getContextItems()[currentScopeEntryKey] = newCurrentScope;
    //        return newCurrentScope;
    //    }

    //    /// <summary>Nothing to dispose.</summary>
    //    public void Dispose() { }

    //    private readonly Func<IDictionary> getContextItems;
    //}

    public sealed class HttpContextScopeContext : IScopeContext
    {
        /// <summary>Fixed root scope name for the context.</summary>
        public static readonly string ROOT_SCOPE_NAME = typeof(HttpContextScopeContext).FullName;

        /// <summary>Provides default context items dictionary using <see cref="HttpContext.Current"/>.
        /// Could be overridden with any key-value dictionary where <see cref="HttpContext"/> is not available, e.g. in tests.</summary>
        public static Func<IDictionary> GetContextItems = () => HttpContext.Current.ThrowIfNull().Items;

        /// <summary>Returns fixed <see cref="ROOT_SCOPE_NAME"/>.</summary>
        public string RootScopeName { get { return ROOT_SCOPE_NAME; } }

        /// <summary>Creates the context optionally with arbitrary/test items storage.</summary>
        /// <param name="getContextItems">(optional) Arbitrary/test items storage.</param>
        public HttpContextScopeContext(Func<IDictionary> getContextItems = null)
        {
            sessionGetContextItems = getContextItems ?? GetContextItems;
        }

        /// <summary>Returns current ambient scope stored in item storage.</summary> <returns>Current scope or null if there is no.</returns>
        public IScope GetCurrentOrDefault()
        {
            return sessionGetContextItems()[RootScopeName] as IScope;
        }

        /// <summary>Sets the new scope as current using existing current as input.</summary>
        /// <param name="setCurrentScope">Delegate to get new scope.</param>
        /// <returns>Return new current scope.</returns>
        public IScope SetCurrent(SetCurrentScopeHandler setCurrentScope)
        {
            var newCurrentScope = setCurrentScope.ThrowIfNull()(GetCurrentOrDefault());
            sessionGetContextItems()[ROOT_SCOPE_NAME] = newCurrentScope;
            return newCurrentScope;
        }

        private readonly Func<IDictionary> sessionGetContextItems;


        public void Dispose()
        {
            //No objects to dispose
        }
    }
}
