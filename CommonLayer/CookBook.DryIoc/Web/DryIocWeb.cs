﻿using System;
using System.Collections;
using System.Diagnostics;
using DryIoc;

[assembly: System.Web.PreApplicationStartMethod(typeof(CookBook.DryIoc.Web.DryIocHttpModuleInitializer), "Initialize")]

namespace CookBook.DryIoc.Web
{
    /// <summary>Extension to get container with ambient <see cref="HttpContext.Current"/> scope context.</summary>
    public static class DryIocWeb
    {
        /// <summary>Creates new container from original with HttpContext or arbitrary/test context <paramref name="getContextItems"/>.</summary>
        /// <param name="container">Original container with some rules and registrations.</param>
        /// <param name="getContextItems">(optional) Arbitrary or test context to use instead of <see cref="HttpContext.Current"/>.</param>
        /// <returns>New container with the same rules and registrations/cache but with new ambient context.</returns>
        public static IContainer WithHttpContextScopeContext(this IContainer container, Func<IDictionary> getContextItems = null)
        {
            HttpContextScopeContext context = null;
            try
            {
                context = new HttpContextScopeContext(getContextItems);
                return container.ThrowIfNull().With(scopeContext: context);
            }
            catch(Exception ex)
            {
                if(context != null)
                {
                    context.Dispose();
                }
                Debug.Write(ex.Message);
                throw;
            }
        }
    }
}
