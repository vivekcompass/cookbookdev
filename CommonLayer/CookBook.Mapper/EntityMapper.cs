﻿using CookBook.Data.UserData;
using CookBook.Data.MasterData;
using CookBook.DataAdapter.Factory;
using System.ComponentModel.Composition;
using CookBook.DataAdapter.Factory.Common;
using CookBook.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace CookBook.Mapper
{
    [Export(typeof(IEntityMapper))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class EntityMapper : IEntityMapper
    {
        private AutoMapper.IMapper mapper = null;
        private AutoMapper.MapperConfiguration config = null;

        private bool isMapperActivated;

        public EntityMapper()
        {
            InitializeMapper();
        }

        /// <summary>
        /// Method to initialize the mapping library. Here all object mappings need to be defined for first time so that later 
        /// in down layers we can call Map API to get the mapped output object
        /// </summary>
        private void InitializeMapper()
        {
            config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CookBook.DataAdapter.Factory.Common.UserMaster, UserMasterData>().ReverseMap();
                cfg.CreateMap<Allergen, AllergenData>().ReverseMap();
                cfg.CreateMap<SiteType, SiteTypeData>().ReverseMap();

                cfg.CreateMap<UniqueAPLMaster, APLMasterData>().ReverseMap();
                cfg.CreateMap<procGetAPLMasterData_Result, APLMasterData>().ReverseMap();
                cfg.CreateMap<procGetAPLMasterDataCommon_Result, APLMasterData>().ReverseMap();
                cfg.CreateMap<procGetAPLMasterDataForSite_Result, APLMasterData>().ReverseMap();
                cfg.CreateMap<procGetAPLMasterDataForRegion_Result, APLMasterData>().ReverseMap();
                cfg.CreateMap<ConceptType1, ConceptType1Data>().ReverseMap();
                cfg.CreateMap<ConceptType2, ConceptType2Data>().ReverseMap();
                cfg.CreateMap<ConceptType3, ConceptType3Data>().ReverseMap();
                cfg.CreateMap<ConceptType4, ConceptType4Data>().ReverseMap();
                cfg.CreateMap<ApplicationSetting, ApplicationSettingData>().ReverseMap();
                cfg.CreateMap<ConceptType5, ConceptType5Data>().ReverseMap();
                cfg.CreateMap<DayPart, DayPartData>().ReverseMap();
                cfg.CreateMap<DietCategory, DietCategoryData>().ReverseMap();
                cfg.CreateMap<Reason, ReasonData>().ReverseMap();
                cfg.CreateMap<SubSectorMaster, SubSectorData>().ReverseMap();
                cfg.CreateMap<procGetRegionInheritedData_Result, RegionInheritedData>().ReverseMap();
                cfg.CreateMap<DishCategory, DishCategoryData>().ReverseMap();
                cfg.CreateMap<procGetDishCategoryList_Result, DishCategoryData>().ReverseMap();
                cfg.CreateMap<procGetDishCategorySiblingList_Result, DishCategorySiblingMapping>().ReverseMap();
                cfg.CreateMap<procGetDishCategorySiblingList_Result, DishCategorySiblingData>().ReverseMap();
                cfg.CreateMap<DishRecipeMapping, DishRecipeMappingData>().ReverseMap();
                cfg.CreateMap<RegionDishRecipeMapping, RegionDishRecipeMappingData>().ReverseMap();
                cfg.CreateMap<SiteDishRecipeMapping, SiteDishRecipeMappingData>().ReverseMap();
                cfg.CreateMap<SiteDishCategoryContainerMappingData, procGetSiteDishCategoryContainerMappingData_Result>().ReverseMap();
                cfg.CreateMap<SiteDishCategoryContainerMappingData, SiteDishCategoryContainerMapping>().ReverseMap();
                cfg.CreateMap<procGetSiteItemPriceHistory_Result, PriceHistoryData>().ReverseMap();
                cfg.CreateMap<procGetMogNotMappedList_Result, MOGNotMapped>().ReverseMap();
                cfg.CreateMap<procGetSiteUnitPrice_Result, UnitPriceData>().ReverseMap();
                cfg.CreateMap<procGetSiteDayPartMapping_Result, SiteDayPartMappingData>()
                    .ForMember(dest => dest.ModifiedOnString, source => source.MapFrom(m => m.ModifiedOn != null ? m.ModifiedOn.Value.ToString("dd-MMM-yyyy") : ""));
                cfg.CreateMap<procGetDishImpactedItemList_Result, SiteDayPartMappingData>().ReverseMap();

                cfg.CreateMap<Dish, DishData>().ReverseMap();
                cfg.CreateMap<DishSubCategory, DishSubCategoryData>().ReverseMap();
                cfg.CreateMap<DishType, DishTypeData>().ReverseMap();
                cfg.CreateMap<FoodProgram, FoodProgramData>().ReverseMap();
                cfg.CreateMap<ItemDishMapping, ItemDishMappingData>().ReverseMap();
                cfg.CreateMap<ItemDishCategoryMapping, ItemDishCategoryMappingData>().ReverseMap();
                cfg.CreateMap<RegionItemDishMapping, RegionItemDishMappingData>().ReverseMap();
                cfg.CreateMap<SiteItemDishMapping, SiteItemDishMappingData>().ReverseMap();
                cfg.CreateMap<Item, ItemData>().ReverseMap();
                cfg.CreateMap<ItemType1, ItemType1Data>().ReverseMap();
                cfg.CreateMap<ItemType2, ItemType2Data>().ReverseMap();
                cfg.CreateMap<MOG, MOGData>().ReverseMap();
                cfg.CreateMap<procGetInheritMOGList_Result, MOGInheritedData>().ReverseMap();
                cfg.CreateMap<ProcGetInheritChangesSectorToRegion_Result, InheritanceChangesData>().ReverseMap();
                cfg.CreateMap<ProcGetInheritChangesRegionToSite_Result, InheritanceChangesData>().ReverseMap();
                cfg.CreateMap<RecipeMOGMapping, RecipeMOGMappingData>().ReverseMap();
                cfg.CreateMap<Recipe, RecipeData>().ReverseMap();
                cfg.CreateMap<procGetRecipeList_Result, RecipeData>()
                 .ForMember(dest => dest.PublishedDateAsString, source => source.MapFrom(m => m.PublishedDate != null ? m.PublishedDate.Value.ToString("dd-MMM-yyyy") : ""));
                //.ForMember(dest => dest.IsElementoryString, source => source.MapFrom(m => m.IsElementory.HasValue  m.IsElementory.Value ==1 ? "Elementory" : "Non Elementory"));
                cfg.CreateMap<RegionRecipeMOGMapping, RegionRecipeMOGMappingData>().ReverseMap();
                cfg.CreateMap<SiteRecipeMOGMapping, SiteRecipeMOGMappingData>().ReverseMap();
                cfg.CreateMap<RegionRecipe, RegionRecipeData>().ReverseMap();
                cfg.CreateMap<SiteRecipe, SiteRecipeData>().ReverseMap();
                cfg.CreateMap<Serveware, ServewareData>().ReverseMap();
                cfg.CreateMap<ServingTemperature, ServingTemperatureData>().ReverseMap();
                cfg.CreateMap<SiteMaster, SiteMasterData>().ReverseMap();
                cfg.CreateMap<RegionMaster, RegionMasterData>().ReverseMap();
                cfg.CreateMap<Cafe, CafeData>().ReverseMap();
                cfg.CreateMap<PlanningTag, PlanningTagData>().ReverseMap();
                cfg.CreateMap<Container, ContainerData>().ReverseMap();
                cfg.CreateMap<ContainerType, ContainerTypeData>().ReverseMap();
                cfg.CreateMap<procGetContainerType_Result, ContainerTypeData>().ReverseMap();
                cfg.CreateMap<SiteProfile1, SiteProfile1Data>().ReverseMap();
                cfg.CreateMap<SiteProfile2, SiteProfile2Data>().ReverseMap();
                cfg.CreateMap<SiteProfile3, SiteProfile3Data>().ReverseMap();
                cfg.CreateMap<UOM, UOMData>().ReverseMap();
                cfg.CreateMap<UOMModuleMaster, UOMModuleData>().ReverseMap();
                cfg.CreateMap<UOMModuleMapping, UOMModuleMappingData>().ReverseMap();
                cfg.CreateMap<procGetUOMModuleMappingList_Result, UOMModuleMappingData>().ReverseMap();
                cfg.CreateMap<procGetUOMModuleMappingGridData_Result, GetUOMModuleMappingGridData>().ReverseMap();
                cfg.CreateMap<ReasonType, ReasonTypeData>().ReverseMap();
                cfg.CreateMap<RevenueType, RevenueTypeData>().ReverseMap();
                cfg.CreateMap<CookBook.DataAdapter.Factory.Common.UserMaster, UserMasterData>().ReverseMap();
                cfg.CreateMap<VisualCategory, VisualCategoryData>().ReverseMap();
                cfg.CreateMap<procGetUserDataByUsername_Result, UserMasterResponseData>().ReverseMap();
                cfg.CreateMap<procGetSiteList_Result, SiteMasterData>().ReverseMap();
                cfg.CreateMap<procGetCafeList_Result, CafeData>().ReverseMap();
                cfg.CreateMap<procGetCPUList_Result, CPUData>().ReverseMap();
                cfg.CreateMap<procGetDKList_Result, DKData>().ReverseMap();
                cfg.CreateMap<procGetItemListExport_Result, ItemExportData>().ReverseMap();
                cfg.CreateMap<procGetItemList_Result, ItemData>()
                    .ForMember(dest => dest.PublishedDateAsString, source => source.MapFrom(m => m.PublishedDate != null ? m.PublishedDate.Value.ToString("dd-MMM-yyyy") : ""));
                cfg.CreateMap<procGetDishImpactedItemList_Result, ItemData>().ReverseMap();
                cfg.CreateMap<ItemData, Item>()
                            .ForMember(dest => dest.Name, source => source.MapFrom(m => m.ItemName));
                cfg.CreateMap<procGetDishList_Result, DishData>().ReverseMap();
                cfg.CreateMap<procGetRegionDishMasterData_Result, DishData>().ReverseMap();
                cfg.CreateMap<procGetSiteDishList_Result, DishData>().ReverseMap();
                cfg.CreateMap<procGetRegionDishList_Result, DishData>().ReverseMap();
                cfg.CreateMap<procGetSiteMOGList_Result, MOGData>()
                 .ForMember(dest => dest.CreatedOnAsString, source => source.MapFrom(m => m.CreatedOn != null ? m.CreatedOn.Value.ToString("dd-MMM-yyyy") : ""))
                 .ForMember(dest => dest.ModifiedOnAsString, source => source.MapFrom(m => m.PublishedDate != null ? m.PublishedDate.Value.ToString("dd-MMM-yyyy") : ""));
                cfg.CreateMap<procGetRegionMOGList_Result, MOGData>()
                .ForMember(dest => dest.CreatedOnAsString, source => source.MapFrom(m => m.CreatedOn != null ? m.CreatedOn.Value.ToString("dd-MMM-yyyy") : ""))
                .ForMember(dest => dest.ModifiedOnAsString, source => source.MapFrom(m => m.PublishedDate != null ? m.PublishedDate.Value.ToString("dd-MMM-yyyy") : ""));
                cfg.CreateMap<procGetMOGList_Result, MOGData>()
                 .ForMember(dest => dest.CreatedOnAsString, source => source.MapFrom(m => m.CreatedOn != null ? m.CreatedOn.Value.ToString("dd-MMM-yyyy") : ""))
                 .ForMember(dest => dest.ModifiedOnAsString, source => source.MapFrom(m => m.PublishedDate != null ? m.PublishedDate.Value.ToString("dd-MMM-yyyy") : ""));

                cfg.CreateMap<procGetNationalMOGList_Result, NationalMOGData>().ReverseMap();
                //.ForMember(dest => dest.IsElementoryString, source => source.MapFrom(m => m.IsElementoryMOG != null && m.IsElementoryMOG.Value ? "Elementory" : "Non Elementory"));



                cfg.CreateMap<NationalMOG, NationalMOGData>().ReverseMap();
                cfg.CreateMap<NationalMOG, MOGData>().ReverseMap();
                cfg.CreateMap<ProcGetExpiryProductData_Result, GetExpiryProductData>().ReverseMap();

                cfg.CreateMap<procGetRegionRecipeList_Result, RegionRecipeData>().ReverseMap();
                cfg.CreateMap<procGetSiteRecipeList_Result, SiteRecipeData>().ReverseMap();
                cfg.CreateMap<procGetMOGsByRecipeID_Result, RecipeMOGMappingData>().ReverseMap();
                cfg.CreateMap<procGetSiteMOGsByRecipeID_Result, RecipeMOGMappingData>().ReverseMap();
                cfg.CreateMap<procGetSiteBaseRecipesByRecipeID_Result, RecipeMOGMappingData>().ReverseMap();
                cfg.CreateMap<procGetRegionMOGsByRecipeID_Result, RecipeMOGMappingData>().ReverseMap();
                cfg.CreateMap<procGetRegionBaseRecipesByRecipeID_Result, RecipeMOGMappingData>().ReverseMap();
                cfg.CreateMap<procGetBaseRecipesByRecipeID_Result, RecipeMOGMappingData>().ReverseMap();
                cfg.CreateMap<procGetSectorDataByUserID_Result, SectorData>().ReverseMap();
                cfg.CreateMap<procGetFoodProgramList_Result, FoodProgramData>().ReverseMap();
                cfg.CreateMap<RegionDishCategoryMapping, RegionDishCategoryMappingData>().ReverseMap();
                cfg.CreateMap<SiteDishCategoryMapping, SiteDishCategoryMappingData>().ReverseMap();
                cfg.CreateMap<RegionItemInheritanceMapping, RegionItemInheritanceMappingData>().ReverseMap();
                cfg.CreateMap<procGetRegionItemList_Result, RegionItemInheritanceMappingData>().ReverseMap();
                cfg.CreateMap<SiteItemInheritanceMapping, SiteItemInheritanceMappingData>().ReverseMap();
                cfg.CreateMap<procGetSiteItemInheritanceMappingData_Result, SiteItemInheritanceMappingData>().ReverseMap();
                cfg.CreateMap<Color, ColorData>().ReverseMap();
                cfg.CreateMap<procGetSiteDishContainerMappingData_Result, SiteDishContainerMappingData>().ReverseMap();

                cfg.CreateMap<SiteItemDayPartMapping, SiteItemDayPartMappingData>().ReverseMap();
                cfg.CreateMap<SiteDayPartMapping, SiteDayPartMappingData>().ReverseMap();
                cfg.CreateMap<FoodProgramDayPartMapping, FoodProgramDayPartMappingData>().ReverseMap();

                cfg.CreateMap<SiteDishContainerMapping, SiteDishContainerMappingData>().ReverseMap();
                cfg.CreateMap<procGetMOGImpactedBaseRecipes_Result, MOGImpactedBaseRecipesData>().ReverseMap();
                cfg.CreateMap<procGetMOGImpactedFinalRecipes_Result, MOGImpactedFinalRecipesData>().ReverseMap();
                cfg.CreateMap<procGetMOGImpactedDishes_Result, MOGImpactedDishesData>().ReverseMap();
                cfg.CreateMap<procGetMOGImpactedItems_Result, MOGImpactedItemsData>().ReverseMap();
                cfg.CreateMap<procGetMOGImpactedSites_Result, MOGImpactedSiteData>().ReverseMap();
                cfg.CreateMap<procGetMOGImpactedRegion_Result, MOGImpactedRegionData>().ReverseMap();
                cfg.CreateMap<procGetUserModules_Result, ModuleData>().ReverseMap();
                cfg.CreateMap<procGetUserModulePermission_Result, UserPermissionData>().ReverseMap();
                cfg.CreateMap<procGetMappedDishRecipeList_Result, DishRecipeMapData>().ReverseMap();
                cfg.CreateMap<CostSimulation, CostSimulationData>().ReverseMap();
                cfg.CreateMap<procGetSimulatedMappedRecipeMOGListSite_Result, SimulatedMappedRecipeMOGListSiteData>().ReverseMap();
                cfg.CreateMap<procGetSimulatedMappedRecipeMOGListSector_Result, SimulatedMappedRecipeMOGListSectorData>().ReverseMap();
                cfg.CreateMap<procGetSimulatedMappedRecipeMOGListNational_Result, SimulatedMappedRecipeMOGListNationalData>().ReverseMap();
                cfg.CreateMap<CapringMaster, CapringMasterData>().ReverseMap();
                cfg.CreateMap<procGetMOGAPLImpactedDisheSector_Result, MOGAPLImpactedDishesData>().ReverseMap();
                cfg.CreateMap<procGetMOGAPLImpactedDishRegion_Result, MOGAPLImpactedDishesRegionData>().ReverseMap();
                cfg.CreateMap<procGetMOGAPLImpactedDishSites_Result, MOGAPLImpactedDishesSiteData>().ReverseMap();
                cfg.CreateMap<procGetRegionsDetailsByUserId_Result, RegionsDetailsByUserIdData>().ReverseMap();
                cfg.CreateMap<procGetSiteDetailsByUserId_Result, SiteDetailsByUserIdData>().ReverseMap();
                cfg.CreateMap<procGetUserRBFAMatrixByUserID_Result, UserRBFAMatrixData>().ReverseMap();
                cfg.CreateMap<CuisineMaster, CuisineMasterData>().ReverseMap();
                cfg.CreateMap<HealthTagMaster, HealthTagMasterData>().ReverseMap();
                cfg.CreateMap<LifeStyleTagMaster, LifeStyleTagMasterData>().ReverseMap();
                cfg.CreateMap<ExpiryCategoryMaster, ExpiryCategoryMasterData>().ReverseMap();
                cfg.CreateMap<procGetExpiryCategoryMasterData_Result, ExpiryCategoryMasterData>().ReverseMap();
                cfg.CreateMap<ShelfLifeUOMMaster, ShelfLifeUOMMasterData>().ReverseMap();
                cfg.CreateMap<procGetShelfLifeUOMMasterData_Result, ShelfLifeUOMMasterData>().ReverseMap();

                cfg.CreateMap<ProcGetMOGBATCHDETAILS_Result, MOGAPLMAPPEDHISTORY>().ReverseMap();

                cfg.CreateMap<procshowMOGAPLUPLOADDETAILS_Result, MOGAPLMAPPEDHISTORY>().ReverseMap();


                cfg.CreateMap<procshowGetALLHISTORY_Result, MOGAPLMAPPEDHISTORY>().ReverseMap();

                cfg.CreateMap<procshowGetALLHISTORYBATCHWISE_Result, MOGAPLMAPPEDHISTORY>().ReverseMap();

                cfg.CreateMap<procGetAPLMasterMOGCode_Result, APLMOGDATA>().ReverseMap();

                cfg.CreateMap<procshowMOGAPLUPLOADDETAILSAPL_Result, APLMAPPEDHISTORYDLIST>().ReverseMap();
                cfg.CreateMap<ProcGetMOGBATCHDETAILSAPL_Result, APLMAPPEDHISTORYDLIST>().ReverseMap();
                cfg.CreateMap<procshowGetALLHISTORYAPL_Result, APLMAPPEDHISTORYDLIST>().ReverseMap();
                cfg.CreateMap<procshowGetALLHISTORYBATCHWISEAPL_Result, APLMAPPEDHISTORYDLIST>().ReverseMap();
                cfg.CreateMap<procCuisineMasterList_Result, CuisineMaster>().ReverseMap();
                cfg.CreateMap<procshowregionmaster_Result, RegionMasterData>().ReverseMap();
                cfg.CreateMap<NationalprocGetSiteList_Result, SiteMasterData>().ReverseMap();
                cfg.CreateMap<NationalprocGetAPLMasterData_Result, APLMasterData>().ReverseMap();
                cfg.CreateMap<procGetAPLMasterDataforSector_Result, APLMasterData>().ReverseMap();

                cfg.CreateMap<ProcShowNationalDishCategory_Result, NationalDishCategoryData>().ReverseMap();
                cfg.CreateMap<ProcShowNationalDishes_Result, NationalDishData>().ReverseMap();

                cfg.CreateMap<procNationalGetItemList_Result, NationalItemData>().ReverseMap();

                cfg.CreateMap<procNationalGetAPLMasterData_Result, APLMasterData>().ReverseMap();

                cfg.CreateMap<procGetSectorMasterData_Result, ManageSectorData>().ReverseMap();

                cfg.CreateMap<procGetLevelMasterData_Result, LevelMasterData>().ReverseMap();

                cfg.CreateMap<procGetFunctionalityData_Result, FunctionMasterData>().ReverseMap();

                cfg.CreateMap<procGetPermissionData_Result, PermissionMasterData>().ReverseMap();
                cfg.CreateMap<procGetAPLsPageWiseDataList_Result, APLMasterData>().ReverseMap();

                cfg.CreateMap<procGetSectorMasterData_Result, ManageSectorData>().ReverseMap();

                cfg.CreateMap<procGetNationalAPLMaster_Result, APLMasterData>().ReverseMap();
                cfg.CreateMap<procGetnationalRecipeList_Result, NationalRecipeData>().ReverseMap();

                cfg.CreateMap<procGetCapringSectorMaster_Result, ManageSectorData>().ReverseMap();

                cfg.CreateMap<procGetCapringLevelMaster_Result, LevelMasterData>().ReverseMap();
                cfg.CreateMap<procGetCapringFunctionMaster_Result, FunctionMasterData>().ReverseMap();
                cfg.CreateMap<procGetCapringPermissionMaster_Result, PermissionMasterData>().ReverseMap();
                cfg.CreateMap<procGetCapringMasterData_Result, CapringMastre>().ReverseMap();
                cfg.CreateMap<NutritionElementType, NutritionElementTypeData>().ReverseMap();
                cfg.CreateMap<procGetNutritionMasterData_Result, NutritionElementMasterData>().ReverseMap();
                cfg.CreateMap<NutritionElementMaster, NutritionElementMasterData>().ReverseMap();
                cfg.CreateMap<DietType, DietTypeData>().ReverseMap();
                cfg.CreateMap<Texture, TextureData>().ReverseMap();
                cfg.CreateMap<NutritionElementType, CommonMasterData>().ReverseMap();
                cfg.CreateMap<NutrientElementUOM, CommonMasterData>().ReverseMap();
                cfg.CreateMap<ItemSectorMaster, CommonMasterData>().ReverseMap();
                cfg.CreateMap<MenuType2, CommonMasterData>().ReverseMap();
                cfg.CreateMap<MenuType1, CommonMasterData>().ReverseMap();


                //Krish
                cfg.CreateMap<FoodCostSimulation, FoodCostSimulationData>().ReverseMap();
                cfg.CreateMap<FoodCostSimulationBoard, FoodCostSimulationBoardData>().ReverseMap();
                cfg.CreateMap<procGetSiteByRegionList_Result, SiteMasterData>().ReverseMap();
                cfg.CreateMap<procGetSiteDishRecipeList_Result, SiteRecipeData>().ReverseMap();
                cfg.CreateMap<procUpdateGetFCSSiteDishRecipeList_Result, SiteRecipeData>().ReverseMap();
                cfg.CreateMap<procUpdateGetFCSSiteRecipeList_Result, SiteRecipeData>().ReverseMap();
                cfg.CreateMap<SiteRecipeMOGMappingData, FoodCostSimSiteRecipeMOGMapping>().ReverseMap();
                cfg.CreateMap<SiteRecipeData, FoodCostSimSiteRecipe>().ReverseMap();
                cfg.CreateMap<RecipeData, FoodCostSimRecipe>().ReverseMap();
                cfg.CreateMap<procGetFCSSiteRecipeData_Result, SiteRecipeData>().ReverseMap();
                cfg.CreateMap<procGetFCSSiteMOGsByRecipeID_Result, RecipeMOGMappingData>().ReverseMap();
                cfg.CreateMap<FoodCostSimAPLHistory, FoodCostSimAPLHistoryData>().ReverseMap();
                cfg.CreateMap<procGetFCSAPLMasterDataForSite_Result, APLMasterData>().ReverseMap();
                cfg.CreateMap<FoodCostSimSiteDish, DishData>().ReverseMap();
                cfg.CreateMap<FoodCostSimAPLHistory, FoodCostSimAPLHistoryData>().ReverseMap();
                //cfg.CreateMap<SiteRecipeData, RecipeData>().ReverseMap();
                //Krish
                cfg.CreateMap<procGetRecipeMOGNLevelFOrPrinting_Result, RecipePrintingMOGListData>().ReverseMap();

                cfg.CreateMap<procGetMOGUOMDetails_Result, MOGUOMData>().ReverseMap();
                cfg.CreateMap<procGetItemDishRecipeNutritionData_Result, ItemDishRecipeNutritionData>().ReverseMap();

                //Krish
                cfg.CreateMap<procGetRoleDataByUserID_Result, UserRoleData>().ReverseMap();
                //Krish
                cfg.CreateMap<procGetPatientMasterBATCHHEADER_Result, PatientMasterHISTORY>().ReverseMap();
                cfg.CreateMap<procGetPatientMasterBATCHDETAILS_Result, PatientMasterHISTORY>().ReverseMap();
                cfg.CreateMap<procGetPatientMaster_Result, PatientMasterData>().ReverseMap();
                cfg.CreateMap<procshowGetALLPatientMasterHISTORYBATCHWISE_Result, PatientMasterHistoryBatchWise>().ReverseMap();
                cfg.CreateMap<procshowGetALLHISTORYPatientMaster_Result, PatientMasterHistoryAllData>().ReverseMap();
                //Bed
                cfg.CreateMap<procGetBedMasterBATCHHEADER_Result, BedMasterHISTORY>().ReverseMap();
                cfg.CreateMap<procGetBedMasterBATCHDETAILS_Result, BedMasterHISTORY>().ReverseMap();
                cfg.CreateMap<procGetBedMaster_Result, BedMasterData>().ReverseMap();
                cfg.CreateMap<procshowGetALLBedMasterHISTORYBATCHWISE_Result, BedMasterHistoryBatchWise>().ReverseMap();
                cfg.CreateMap<procshowGetALLHISTORYBedMaster_Result, BedMasterHistoryAllData>().ReverseMap();
                //Dietician
                cfg.CreateMap<procGetDieticianMasterBATCHHEADER_Result, DieticianMasterHISTORY>().ReverseMap();
                cfg.CreateMap<procGetDieticianMasterBATCHDETAILS_Result, DieticianMasterHISTORY>().ReverseMap();
                cfg.CreateMap<procGetDieticianMaster_Result, DieticianMasterData>().ReverseMap();
                cfg.CreateMap<procshowGetALLDieticianMasterHISTORYBATCHWISE_Result, DieticianMasterHistoryBatchWise>().ReverseMap();
                cfg.CreateMap<procshowGetALLHISTORYDieticianMaster_Result, DieticianMasterHistoryAllData>().ReverseMap();
                //Krish 06-08-2022
                cfg.CreateMap<procGetMOGNLevelForSplitScreen_Result, RecipeMOGMappingData >().ReverseMap();
                //Krish
                cfg.CreateMap<procGetRangeTypeMaster_Result, RangeTypeMasterData>().ReverseMap();
                cfg.CreateMap<RangeType, RangeTypeMasterData>().ReverseMap();
                cfg.CreateMap<procGetProcessTypeMaster_Result, ProcessTypeMasterData>().ReverseMap();
                cfg.CreateMap<ProcessType, ProcessTypeMasterData>().ReverseMap();
                cfg.CreateMap<MogProcessTypeMapping, MogProcessTypeMappingData>().ReverseMap();
                //Krish 25-10-2022
                //cfg.CreateMap<procGetSectorDishWithRangeType_Result, DishData>().ReverseMap();
            });
            mapper = config.CreateMapper();
            isMapperActivated = true;
        }

        public AutoMapper.MapperConfiguration GetConfigurations()
        {
            return config;
        }

        /// <summary>
        /// Overload method to get the destination object from source object
        /// </summary>
        /// <typeparam name="T">Type of Destination Object</typeparam>
        /// <param name="source">Source Object instance</param>
        /// <returns>returns the detination object instance</returns>
        public T Map<T>(object source)
        {
            return mapper.Map<T>(source);
        }

        /// <summary>
        /// Overload method to map the source and destination object
        /// </summary>
        /// <typeparam name="TSource">Source Object Type</typeparam>
        /// <typeparam name="TTarget">Destination Object Type</typeparam>
        /// <param name="source">Source object instance</param>
        /// <param name="target">Destination Object Instance</param>
        /// <returns>returns the destination object instance</returns>
        public TTarget Map<TSource, TTarget>(TSource source, TTarget target)
        {
            return mapper.Map<TSource, TTarget>(source, target);

        }

        /// <summary>
        /// Method to determine whetner mapping library is ready for function or not
        /// </summary>
        /// <returns>returns boolean status</returns>
        public bool IsActivated()
        {
            return isMapperActivated;
        }

       
    }
}
