﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HSEQ.Data.User;

namespace HSEQ.Mapper.Converter
{
    public class UserAddressConverter: AutoMapper.ITypeConverter<UserData, UserAddressData>
    {
        public UserAddressData Convert(UserData source, UserAddressData destination, AutoMapper.ResolutionContext context)
        {
            if (source != null)
            {
                UserAddressData address = new UserAddressData { Address1 = "House No", Address2 = "Sector", Country = "India",
                    City = "Gurgaon", IsActive = true, AddressTypeID = 1, PinCode = "122001", State = "Haryana",
                    UserAddressID = 1, UserID = 1, Remarks = string.Format("{0} {1} {2}", 
                    source.FirstName, source.LastName, source.MobileNo) };

                return address;
            }
            return null;
        }
    }
}
