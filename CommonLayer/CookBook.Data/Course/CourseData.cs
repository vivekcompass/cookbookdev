﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HSEQ.Data.Course
{
    [DataContract]
    public class CourseData
    {
        [DataMember]
        public int CourseID { get; set; }
        [Required]
        [DataMember]
        public string CourseCode { get; set; }
        [DataMember]
        public decimal Course_Version { get; set; }
        [Required]
        [DataMember]
        public string CourseName { get; set; }
        [DataMember]
        public string Category { get; set; }
        [DataMember]
        public string CourseDetails { get; set; }
        [DataMember]
        public string Active { get; set; }

        public Nullable<System.DateTime> Active_From_Date { get; set; }
        public Nullable<System.DateTime> Expiry_Date { get; set; }
        public Nullable<decimal> CourseRating { get; set; }
        public Nullable<int> No_of_Registered_Users { get; set; }
        public string Encore_Course_Code { get; set; }
        public string Is_Online_CW_Available { get; set; }
        public string Is_Online_Assessment_Available { get; set; }
        public string Forum_ID { get; set; }
        public string Duration { get; set; }
        public string Duration_UOM { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public Nullable<System.DateTime> Createdon { get; set; }
        public Nullable<System.DateTime> Changedon { get; set; }
        public string Createdby { get; set; }
        public string Changedby { get; set; }
        public string Is_CCS_Opnly_Avlbl { get; set; }
        public string Is_BuddyNet_Opnly_Avlbl { get; set; }
        public string Is_Assmnt_Opnly_Avlbl { get; set; }
        public string Is_Crs_Opnly_Avlbl { get; set; }
        public string crs_img_url { get; set; }
        public string Course_metadata { get; set; }
        public string CourseGroup { get; set; }
        public string is_ondemand_avbl { get; set; }
        public Nullable<int> popularity_rank { get; set; }
        public string language { get; set; }
        public string signup_rqrd { get; set; }
        public Nullable<System.DateTime> crs_lnch_dt { get; set; }
        public string Is_Upcoming { get; set; }
        public Nullable<int> CourseFormatID { get; set; }
        public Nullable<int> CourseLevelID { get; set; }
        public Nullable<decimal> PartnerID { get; set; }
        public string PartnerType { get; set; }
        public string publisher { get; set; }
        public string author { get; set; }
        public string courseowner { get; set; }
        public string instructor { get; set; }
        public string Faculty_Code { get; set; }
        public string bulanguage { get; set; }
        public string profileTypeID { get; set; }
    }
}
