﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HSEQ.Data.Course
{
    [DataContract]
    public class CourseKeywordMasterData
    {
        [DataMember]
        public string CourseCode { get; set; }
        [DataMember]
        public string Keyword { get; set; }
        [DataMember]
        public string active { get; set; }
    }
}
