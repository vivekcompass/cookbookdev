﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.Common
{
    /// <summary>
    /// Class to define service response properties
    /// </summary>
    [DataContract]
    public class ResponseBase
    {
        private bool isSuccess;
        private string message;

        /// <summary>
        /// True, If bussiness logic return successful response
        /// </summary>
        [DataMember]
        public bool IsSuccess
        {
            get { return isSuccess; }
            set { isSuccess = value; }
        }

        /// <summary>
        /// Response message will be either for success or unsuccess
        /// </summary>
        [DataMember]
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

    }
}
