﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.Common
{
    [DataContract]
    public class ServiceFaultData
    {
        /// <summary>
        /// Property to get set fault message of service method execution
        /// </summary>
        [DataMember]
        public string Message
        {
            get;
            set;
        }

        /// <summary>
        /// Property to get set fault code of service method execution
        /// </summary>
        [DataMember]
        public string FaultCode
        {
            get;
            set;
        }
    }
}
