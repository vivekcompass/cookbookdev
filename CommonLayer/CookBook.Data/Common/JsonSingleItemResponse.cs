﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.Common
{
    public class JsonSingleItemResponse<T> : ResponseBase
    {
        #region [Private Member]

        private T singleResult;

        #endregion

        #region Properties


        [DataMember]
        public T SingleResult
        {
            get { return singleResult; }
            set { singleResult = value; }
        }


        #endregion

    }
}
