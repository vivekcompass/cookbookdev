﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.Export
{
    public class ExportData
    {
        private string _toDate = string.Empty;
        private string _fromDate = string.Empty;
        private string _tpName = string.Empty;

        public string CustomerAddress { get; set; }
        public string UserCode { get; set; }
        public string TPName
        {
            get
            {
                return this._tpName;
            }
            set
            {
                if (value != null)
                {
                    this._tpName = value;
                }
            }
        }
        public string FromDate
        {
            get
            {
                return this._fromDate;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    this._fromDate = string.Empty;
                else
                {
                    DateTime dateTime;
                    if (DateTime.TryParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
                    {
                        this._fromDate = dateTime.ToString("dd.MM.yyyy");
                    }
                    else
                    {
                        this._fromDate = Convert.ToDateTime(value).ToString("dd.MM.yyyy");
                    }
                }
            }
        }
        public string ToDate
        {
            get
            {
                return this._toDate;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    this._toDate = string.Empty;
                else
                {
                    DateTime dateTime;
                    if (DateTime.TryParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
                    {
                        this._toDate = dateTime.ToString("dd.MM.yyyy");
                    }
                    else
                    {
                        this._toDate = Convert.ToDateTime(value).ToString("dd.MM.yyyy");
                    }
                }
            }
        }

        public string ColumnsToHorizontalAlign { get; set; }
    }
}
