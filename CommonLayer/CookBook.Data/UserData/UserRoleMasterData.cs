﻿using System;

namespace CookBook.Data.UserData
{
    public class UserRoleMasterData
    {
        public int UserRoleId { get; set; }

        public string UserRoleName { get; set; }

        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
