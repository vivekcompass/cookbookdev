﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class NutritionDishRecipeMogMapping
    {
        public string DishCode { get; set; }
        public string DishName { get; set; }
        public string RecipeCode { get; set; }
        public string RecipeName { get; set; }
        public int RecipeID { get; set; }
        public decimal? Quantity { get; set; }
        public string RecipeType { get; set; }
        public string Instructions { get; set; }
        public string Allergens { get; set; }
        public string NGAllergens { get; set; }
        //Krish 06-08-2022
        public List<RecipeMOGMappingData> RecipeMOG { get; set; }
        //Krish 07-08-2022
        public bool? IsDefault { get; set; }

    }
}
