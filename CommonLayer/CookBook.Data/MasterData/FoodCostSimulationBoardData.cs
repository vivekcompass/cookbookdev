﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{    
    public partial class FoodCostSimulationBoardData
    {
        public long ID { get; set; }
        public long SimulationID { get; set; }
        public Nullable<int> DayNumber { get; set; }
        public Nullable<int> ItemID { get; set; }
        public Nullable<int> DayPartID { get; set; }
        public Nullable<int> DishCategoryID { get; set; }
        public Nullable<int> DishID { get; set; }
        public Nullable<int> Pax { get; set; }
        public string Gm { get; set; }
        public string Cost { get; set; }
        public Nullable<decimal> LunchAvgCost { get; set; }
        public Nullable<decimal> TotalMenuCost { get; set; }

        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string DayPartCode { get; set; }
        public string DayPartName { get; set; }
        public string DishName { get; set; }
        public string DishCategoryName { get; set; }
        public string DishCategoryCode { get; set; }
        public decimal? DishCost { get; set; }
        public string DishCode { get; set; }

        public string ServedPortion { get; set; }
    }
    public class SimulationBoardData
    {
        public int ID { get; set; }
        public string SimulationName { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int RegionID { get; set; }
        public int? SiteID { get; set; }
        public int NoOfDays { get; set; }
        public int DayPartID { get; set; }
        public List<SimulationBoardItemsData> ItemsData { get; set; }
        public List<FoodCostSimulationBoardData> FoodCostSimulationBoard { get; set; }
        public string SimCode { get; set; }
        public string Status { get; set; }
        public string Version { get; set; }
        public string SiteCode { get; set; }
        public bool saveOnly { get; set; }
        public List<SimulationBoardDS> DeletedDishes { get; set; }
    }
    public class SimulationBoardItemsData
    {
        public string code { get; set; }
        public string dpcode { get; set; }
        public string text { get; set; }
        public string value { get; set; }
        public List<SimulationBoardDC> DishCategories { get; set; }
        public string TotalAvgCost { get; set; }
                
    }
    public class SimulationBoardDC
    {
        public string DishCategoryCode { get; set; }
        public int DishCategoryID { get; set; }
        public string DishCategoryName { get; set; }
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public List<SimulationBoardDS> Dishes { get; set; }
    }
    public class SimulationBoardDS
    {
        public int DishID { get; set; }
        public string DishName { get; set; }
        public decimal? CostPerKG { get; set; }
        public string DishCode { get; set; }
        public List<SimulationBoardValues> dataO { get; set; }
    }
    public class SimulationBoardValues
    {
        public int index { get; set; }
        public string paxTxt { get; set; }
        public string grmTxt { get; set; }
        public string costTxt { get; set; }
        public int boradId { get; set; }
    }
}
