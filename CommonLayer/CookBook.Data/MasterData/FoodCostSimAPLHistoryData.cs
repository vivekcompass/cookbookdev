﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class FoodCostSimAPLHistoryData
    {
        public int ID { get; set; }
        public string PreviousArticleNumber { get; set; }
        public string CurrentArticleNumber { get; set; }
        public decimal PreviousCost { get; set; }
        public decimal CurrentCost { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public string SimulationCode { get; set; }
        public Nullable<int> RegionID { get; set; }
        public Nullable<int> SiteID { get; set; }
        public string RecipeCode { get; set; }
        public string DishCode { get; set; }
        public string ItemCode { get; set; }
        public string DayPartCode { get; set; }
        public int ItemID { get; set; }
        public string MOGCode { get; set; }
        public string SiteCode { get; set; }
        public string SelectedMOGSiteCode { get; set; }

        public bool? IsDelete { get; set; }        
    }
}
