using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class SiteDayPartMappingData
    {
        public int ID { get; set; }
        public string SiteCode { get; set; }
        public string DayPartCode { get; set; }
        public string DayPart { get; set; }
        public string CreatedByName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedOnString { get; set; }
        public string ClosingStatusLevel { get; set; }
        public Nullable<System.DateTime> ClosingTime { get; set; }
        public Nullable<int> ClosingStatus { get; set; }
        public bool IsChangedByUser { get; set; }
    }
}
