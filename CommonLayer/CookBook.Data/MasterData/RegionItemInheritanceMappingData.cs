using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class RegionItemInheritanceMappingData
    {
        public int ID { get; set; }
        public int Region_ID { get; set; }
        public int Item_ID { get; set; }
        public string ItemCode { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> Status { get; set; }
        public string SubSectorCode { get; set; }
        public string RecipeMapStatus { get; set; }

    }
}
