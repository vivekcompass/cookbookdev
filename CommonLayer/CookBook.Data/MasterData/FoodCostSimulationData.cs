﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{    
    public partial class FoodCostSimulationData
    {
        public int ID { get; set; }
        public string SimulationCode { get; set; }
        public int RegionID { get; set; }
        public Nullable<int> SiteID { get; set; }
        //public Nullable<int> DayPartID { get; set; }
        public string Name { get; set; }
        public int NoOfDays { get; set; }
        public string Status { get; set; }
        public string Version { get; set; }
        public System.DateTime CreateOn { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ItemIDs { get; set; }        
    }

    public class SimulationList
    {
        public int ID { get; set; }
        public string SimulationCode { get; set; }      
        public string Name { get; set; }
        public string Status { get; set; }
        public string Version { get; set; }
        public string CreateOn { get; set; }
        public bool IsActive { get; set; }       
    }
    public class SimulationItemsData
    {
        public int ItemID { get; set; }
        public int DayPartID { get; set; }
        public string ItemCode { get; set; }
        public string SiteCode { get; set; }
        public string DayPartCode { get; set; }
        public string DayPartName { get; set; }        
        public int RegionID { get; set; }
        public string ItemName { get; set; }
        public List<SimulationRegionItemDishCategoryData> DishCategoryList { get; set; }
    }
    public class SimulationRegionItemDishCategoryData
    {
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public string DishCategoryName { get; set; }
        public int DishCategoryID { get; set; }
        public string DishCategoryCode { get; set; }

        public List<SimulationItemDishData> Dishes { get; set; }
    }
    public class SimulationItemDishData
    {
        public string DishName { get; set; }
        public int DishID { get; set; }
        public decimal? CostPerKG { get; set; }
        public string DishCode { get; set; }
        public decimal? ServedPortion { get; set; }
        public string UOM { get; set; }
    }
    //public class ItemsMultiSelectModel
    //{
    //    public int value { get; set; }
    //    public string text { get; set; }
    //    public string MyProperty { get; set; }
    //}
}
