using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class SiteItemInheritanceData
    {
        public Nullable<int> ID { get; set; }

        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public Nullable<int> VisualCategory_ID { get; set; }
        public string VisualCategoryCode { get; set; }
        public string VisualCategoryName { get; set; }
        public Nullable<int> FoodProgram_ID { get; set; }
        public string FoodProgramCode { get; set; }
        public string FoodProgramName { get; set; }
        public Nullable<int> DietCategory_ID { get; set; }
        public string DietCategoryCode { get; set; }
        public string DietCategoryName { get; set; }
        public Nullable<int> ConceptType2_ID { get; set; }
        public string ConceptType2Code { get; set; }
        public string ConceptType2Name { get; set; }
        public Nullable<int> ItemType1_ID { get; set; }
        public string ItemType1Code { get; set; }
        public string ItemType1Name { get; set; }
        public Nullable<int> ItemType2_ID { get; set; }
        public string ItemType2Code { get; set; }
        public string ItemType2Name { get; set; }
        public string ApplicableTo { get; set; }
        public int Status { get; set; }
        public Nullable<System.DateTime> PublishedDate { get; set; }
        public string SubSectorCode { get; set; }
        public string SubSectorName { get; set; }
        public string DayPartNames { get; set; }
        public string RecipeMapStatus { get; set; }
        public string ItemSectorCode { get; set; }
        public string ItemSectorName { get; set; }
        public Nullable<decimal> ItemPrice { get; set; }
        public string MenuLevel { get; set; }
        public int IsInherited { get; set; }
        public string SiteCode { get; set; }
        public Nullable<int> Site_ID { get; set; }
        public int Item_ID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    }
}
