﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class BedMasterData
    {
        public int ID { get; set; }
        public string BedNumber { get; set; }
        public string BedCode { get; set; }
        public string Occupied { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }       
    }
    public partial class BedMasterHISTORY
    {
        public int IDNO { get; set; }
        public string BATCHNUMBER { get; set; }
        public string BATCHSTATUS { get; set; }
        public Nullable<System.DateTime> UPLOADON { get; set; }
        public string UPLOADBY { get; set; }
        public string BedCode { get; set; }
        public string Occupied { get; set; }
        public string BedNumber { get; set; }

        //11-07-2022        
        public string FLAG { get; set; }
        public Nullable<int> TOTALRECORDS { get; set; }
        public Nullable<int> FAILED { get; set; }
    }
    public partial class BedMasterHistoryBatchWise
    {
        public string BATCHNUMBER { get; set; }
        public string FLAG { get; set; }
        public string CreationTime { get; set; }
        public string BedCode { get; set; }
        public string BedNumber { get; set; }
        public string Occupied { get; set; }
    }
    public partial class BedMasterHistoryAllData {
        public string BATCHNUMBER { get; set; }
        public string FLAG { get; set; }
        public string CreationTime { get; set; }
        public Nullable<int> TOTALRECORDS { get; set; }
        public Nullable<int> FAILED { get; set; }
        public string UPLOADBY { get; set; }
    }
}
