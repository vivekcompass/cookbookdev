﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class UserRoleData
    {
        public int UserRoleId { get; set; }
        public string UserRoleName { get; set; }
    }
}
