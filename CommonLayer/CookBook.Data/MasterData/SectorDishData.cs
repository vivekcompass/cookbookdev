﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class SectorDishData
    {
        public DietCategoryData[] DietCategoryData { get; set; }
        public ApplicationSettingData[] ApplicationSettingData { get; set; }

        public CuisineMasterData[] CuisineMasterData { get; set; }
        public SubSectorData[] SubSectorData { get; set; }
        public HealthTagMasterData[] HealthTagMasterData { get; set; }
        public LifeStyleTagMasterData[] LifeStyleTagMasterData { get; set; }
        public DietTypeData[] DietTypeData { get; set; }

        public ColorData[] ColorData { get; set; }
        public DishCategoryData[] DishCategoryData { get; set; }
        public UOMModuleMappingData[] UOMModuleMappingData { get; set; }
        public DishTypeData[] DishTypeData { get; set; }
        public TextureData[] TextureData { get; set; }
        public ServingTemperatureData[] ServingTemperatureData { get; set; }

        public RecipeData[] RecipeData { get; set; }
        public DishRecipeMappingData[] DishRecipeMappingData { get; set; }
        public DishData[] DishData { get; set; }
    }
}
