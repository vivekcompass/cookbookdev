﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
   public partial class ManageSectorData
    {
        public int id { get; set; }
        public string SectorNumber { get; set; }
        public string SectorName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string SectorCode { get; set; }
    }


    public partial class LevelMasterData
    {
        public int id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }

    public partial class FunctionMasterData
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }

    public partial class PermissionMasterData
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string typ { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }

    public class CapringMastre
    {
        public int ID { get; set; }
        public string CAPRINGCODE { get; set; }
        public string Description { get; set; }
        public Nullable<int> SectorCode { get; set; }
        public string SectorName { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        public string FunctionCode { get; set; }
        public string FunctionName { get; set; }
        public string PermissionCode { get; set; }
        public string PermissionName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public string SCODE { get; set; }
        public string Name { get; set; }
        public string typ { get; set; }

    }
}


