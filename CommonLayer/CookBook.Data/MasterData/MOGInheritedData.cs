﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
   public  class MOGInheritedData
    {
        public int ID { get; set; }
        public string MOGCode { get; set; }
        public string Name { get; set; }
        public Nullable<decimal> Yield { get; set; }
        public Nullable<int> UOM_ID { get; set; }
        public string UOMCode { get; set; }
        public string UOMName { get; set; }
        public Nullable<int> NationalUOM_ID { get; set; }
        public string NationalUOMCode { get; set; }
        public string NationalUOMName { get; set; }
        public Nullable<decimal> CostPerUOM { get; set; }
        public Nullable<decimal> CostPerKG { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Alias { get; set; }
        public int Status { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> PublishedDate { get; set; }
        public Nullable<bool> SaveAsDraft { get; set; }
        public string MenuLevel { get; set; }
        public int IsInheritedMOG { get; set; }
    }
}
