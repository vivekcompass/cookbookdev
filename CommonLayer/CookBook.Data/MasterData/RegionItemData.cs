﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class RegionItemData
    {
        public SubSectorData[] SubSectorData { get; set; }
        public RegionsDetailsByUserIdData[] RegionsDetailsByUserIdData { get; set; }
        public RegionMasterData[] RegionMasterData { get; set; }
        public UOMModuleMappingData[] UOMModuleMappingData { get; set; }
        public DishCategoryData[] DishCategoryData { get; set; }
        public FoodProgramData[] FoodProgramData { get; set; }
        public ItemData[] ItemData { get; set; }
        public DietCategoryData[] DietCategoryData { get; set; }

        //On GO
        public DishData[] RegionDishMasterData { get; set; }
        public DishData[] DishData { get; set; }
        public RegionItemInheritanceMappingData[] RegionItemInheritanceMappingData { get; set; }
        public string[] UniqueMappedItemCodesFromRegion { get; set; }
    }
}
