using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class SiteItemInheritanceMappingData
    {
        public int ID { get; set; }
        public int Site_ID { get; set; }
        public string SiteCode { get; set; }
        public int Item_ID { get; set; }
        public string ItemCode { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public int Status { get; set; }
        public Nullable<decimal> ItemPrice { get; set; }
        public Nullable<int> PriceStatus { get; set; }
        public Nullable<decimal> MaxMealCount { get; set; }
        public string DayPartNames { get; set; }
        public string RecipeMapStatus { get; set; }
        public string CreatedSector { get; set; }


    }
}
