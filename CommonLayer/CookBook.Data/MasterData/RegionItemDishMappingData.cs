using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class RegionItemDishMappingData
    {
        public int ID { get; set; }
        public int Item_ID { get; set; }
        public int Dish_ID { get; set; }
        public Nullable<decimal> ContractPortion { get; set; }
        public Nullable<decimal> StandardPortion { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ItemCode { get; set; }
        public string DishCode { get; set; }
        public Nullable<int> Region_ID { get; set; }
        public string UOMCode { get; set; }
        public Nullable<decimal> WeightPerUOM { get; set; }
        public int Status { get; set; }
        public string SubSectorCode { get; set; }

    }
}
