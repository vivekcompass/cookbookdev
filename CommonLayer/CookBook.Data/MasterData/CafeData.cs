//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CookBook.Data.MasterData
{
    using System;
    using System.Collections.Generic;
    
    public partial class CafeData
    {
        public int ID { get; set; }

        public string CafeCode { get;  set; }
        public string CafeName { get; set; }
        public string CafeDescription { get; set; }
        public Nullable<int> Site_ID { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
        public string SQCafe_ID { get; set; }
        public string SQSecureCode { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
