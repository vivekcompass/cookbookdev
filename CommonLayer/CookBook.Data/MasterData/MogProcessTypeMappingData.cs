﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class MogProcessTypeMappingData
    {
        public int ID { get; set; }
        public string MOGCode { get; set; }
        public string ProcessTypeCode { get; set; }
        public string RangeTypeCode { get; set; }
        public Nullable<int> StandardDuration { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
