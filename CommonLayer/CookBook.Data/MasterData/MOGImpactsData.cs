namespace CookBook.Data.MasterData
{
    using System;
    using System.Collections.Generic;
    
    public partial class MOGImpactedData
    {
        public bool IsMOGImpacted { get; set; }
        public int TotalImapactedBaseRecipes { get; set; }
        public int TotalImapactedFinalRecipes { get; set; }
        public int TotalImapactedDishes { get; set; }
        public int TotalImapactedItems { get; set; }
        public int TotalImapactedSites { get; set; }
        public int TotalImapactedRegions { get; set; }
        public List<MOGImpactedBaseRecipesData> MOGImpactedBaseRecipesData { get; set; }
        public List<MOGImpactedFinalRecipesData> MOGImpactedFinalRecipesData { get; set; }
        public List<MOGImpactedDishesData> MOGImpactedDishesData { get; set; }
        public List<MOGImpactedItemsData> MOGImpactedItemsData { get; set; }
        public List<MOGImpactedSiteData> MOGImpactedSitesData { get; set; }
        public List<MOGImpactedRegionData> MOGImpactedRegionsData { get; set; }
    }

    public partial class MOGImpactedBaseRecipesData
    {
        public string BaseRecipeCode { get; set; }
        public string BaseRecipeName { get; set; }
        public string ImpactedFinalRecipeCode { get; set; }
        public string ImpactedFinalRecipeName { get; set; }
    }

    public partial class MOGImpactedFinalRecipesData
    {
        public string FinalRecipeCode { get; set; }
        public string FinalRecipeName { get; set; }
        public string ImpactedDishName { get; set; }
        public string ImpactedDishCode { get; set; }
    }

    public partial class MOGImpactedDishesData
    {
        public string DishCode { get; set; }
        public string DishName { get; set; }
        public string DishType { get; set; }
        public string ImpactedItemName { get; set; }
        public string ImpactedItemCode { get; set; }
    }

    public partial class MOGImpactedItemsData
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ImpactedSiteName { get; set; }
        public string ImpactedSiteCode { get; set; }
    }
    
    public partial class MOGImpactedRegionData
    {
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
    }

    public partial class MOGImpactedSiteData
    {
        public string SiteName { get; set; }
        public string SiteCode { get; set; }
        public string RegionName { get; set; }
    }
}
