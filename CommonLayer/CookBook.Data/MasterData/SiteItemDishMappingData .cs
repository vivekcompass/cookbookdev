using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class SiteItemDishMappingData
    {
        public int ID { get; set; }
        public int Site_ID { get; set; }
        public int Item_ID { get; set; }
        public int Dish_ID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string SiteCode { get; set; }
        public string ItemCode { get; set; }
        public string DishCode { get; set; }
        public string DishName { get; set; }
        public Nullable<decimal> ServedPortion { get; set; }
        public Nullable<decimal> StandardPortion { get; set; }
        public Nullable<decimal> ContractPortion { get; set; }
        public Nullable<int> Reason_ID { get; set; }
        public Nullable<decimal> StandardWeightPerUOM { get; set; }
        public string StandardUOMCode { get; set; }
        public Nullable<decimal> ServedWeightPerUOM { get; set; }
        public string ServedUOMCode { get; set; }
        public Nullable<decimal> ContractWeightPerUOM { get; set; }
        public string ContractUOMCode { get; set; }
        public string ReasonCode { get; set; }
        public int Status { get; set; }
    }
}
