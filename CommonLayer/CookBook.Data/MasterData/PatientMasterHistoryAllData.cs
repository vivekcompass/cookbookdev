﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class PatientMasterHistoryAllData
    {
        public string BATCHNUMBER { get; set; }
        public string FLAG { get; set; }
        public string CreationTime { get; set; }
        public Nullable<int> TOTALRECORDS { get; set; }
        public Nullable<int> FAILED { get; set; }
        public string UPLOADBY { get; set; }
    }
}
