﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class RecipePrintingData
    {
        public string  RecipeName { get; set; }
        public string DishName { get; set; }
        public decimal? Yield { get; set; }
        public List<RecipePrintingMOGListData> Mogs { get; set; }
    }
    public partial class RecipePrintingMOGListData
    {
        public string MOGCode { get; set; }
        public string MOGName { get; set; }
        public string UOMName { get; set; }
        public Nullable<decimal> MOGQty { get; set; }
    }
}
