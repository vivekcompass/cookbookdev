﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class RecipeCopyData
    {
        public string SiteCode { get; set; }
        public List<SiteRecipeData> Recipes { get; set; }
    }
}
