namespace CookBook.Data.MasterData
{
    using System;
    using System.Collections.Generic;
    
    public partial class MOGAPLImpactedData
    {
        public bool IsMOGAPLImpacted { get; set; }
        public int TotalImapactedDishes { get; set; }
        public int TotalImapactedSites { get; set; }
        public int TotalImapactedRegions { get; set; }
        public string PreviousMappedAPL { get; set; }
        public string CurrentMappedAPL { get; set; }
        public List<MOGAPLImpactedDishesData> MOGAPLImpactedDishesData { get; set; }
        public List<MOGAPLImpactedDishesRegionData> MOGAPLImpactedDishesRegionData { get; set; }
        public List<MOGAPLImpactedDishesSiteData> MOGAPLImpactedDishesSiteData { get; set; }

    }

    public class MOGAPLImpactedDishesData
    {
        public string DishCode { get; set; }
        public string DishName { get; set; }
        public string DishType { get; set; }
        public string PreviousCost { get; set; }
        public string CurrentCost { get; set; }
    }

    public class MOGAPLImpactedDishesRegionData
    {
        public string RegionName { get; set; }
        public string DishCode { get; set; }
        public string DishName { get; set; }
        public string DishType { get; set; }
        public string PreviousCost { get; set; }
        public string CurrentCost { get; set; }
    }

    public class MOGAPLImpactedDishesSiteData
    {
        public string SiteName { get; set; }
        public string SiteCode { get; set; }
        public string RegionName { get; set; }
        public string DishCode { get; set; }
        public string DishName { get; set; }
        public string DishType { get; set; }
        public string PreviousCost { get; set; }
        public string CurrentCost { get; set; }
    }

}
