﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class PatientMasterData
    {
        public int ID { get; set; }
        public string PatientName { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string PatientID { get; set; }
    }
    public partial class PatientMasterHISTORY
    {
        public string PatientID { get; set; }
        public string PatientName { get; set; }
        public string BATCHSTATUS { get; set; }
        public string BATCHNUMBER { get; set; }

        public string FLAG { get; set; }
        public string TOTALRECORDS { get; set; }
        public string FAILED { get; set; }

        public string CreationTime { get; set; }
        public string UPLOADBY { get; set; }
    }
    public partial class PatientMasterHistoryBatchWise
    {
        public string BATCHNUMBER { get; set; }
        public string FLAG { get; set; }
        public string CreationTime { get; set; }
        public string PatientID { get; set; }
        public string PatientName { get; set; }
    }
}
