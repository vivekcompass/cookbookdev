//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CookBook.Data.MasterData
{
    using System;
    using System.Collections.Generic;
    
    public partial class SiteDishCategoryMappingData
    {
        public int ID { get; set; }
        public int Site_ID { get; set; }
        public string SiteCode { get; set; }
        public int Item_ID { get; set; }
        public int DishCategory_ID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ItemCode { get; set; }
        public string DishCategoryCode { get; set; }
        public string DishCategoryName { get; set; }
        public Nullable<decimal> StandardPortion { get; set; }
        public string UOMCode { get; set; }
        public Nullable<decimal> WeightPerUOM { get; set; }
    }
}
