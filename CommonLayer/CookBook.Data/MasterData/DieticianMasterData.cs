﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class DieticianMasterData
    {
        public string DieticianID { get; set; }
        public int ID { get; set; }        
        public string DieticianName { get; set; }        
        public string PhoneNumber { get; set; }
        public string DieticianCode { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        
       
    }
    public partial class DieticianMasterHISTORY
    {
        public int IDNO { get; set; }
        public string BATCHNUMBER { get; set; }
        public string BATCHSTATUS { get; set; }
        public Nullable<System.DateTime> UPLOADON { get; set; }
        public string UPLOADBY { get; set; }
        public string DieticianCode { get; set; }
        public string DieticianName { get; set; }
        public string PhoneNumber { get; set; }
        public string DieticianID { get; set; }
        //11-07-2022        
        public string FLAG { get; set; }
        public Nullable<int> TOTALRECORDS { get; set; }
        public Nullable<int> FAILED { get; set; }
    }
    public partial class DieticianMasterHistoryBatchWise
    {
        public string BATCHNUMBER { get; set; }
        public string FLAG { get; set; }
        public string CreationTime { get; set; }
        public string DieticianCode { get; set; }
        public string DieticianID { get; set; }
        public string DieticianName { get; set; }
        public string PhoneNumber { get; set; }
    }
    public partial class DieticianMasterHistoryAllData
    {
        public string BATCHNUMBER { get; set; }
        public string FLAG { get; set; }
        public string CreationTime { get; set; }
        public Nullable<int> TOTALRECORDS { get; set; }
        public Nullable<int> FAILED { get; set; }
        public string UPLOADBY { get; set; }
    }
}
