﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class SectorRecipeData
    {
        public MOGData[] MOGData { get; set; }
        public DishData[] DishData { get; set; }

        public SubSectorData[] SubSectorData { get; set; }
        public NutritionElementMasterData[] NutritionElementMasterData { get; set; }
        public UOMModuleMappingData[] UOMModuleMappingData { get; set; }
        public RecipeData[] RecipeData { get; set; }

        public ApplicationSettingData[] ApplicationSettingData { get; set; }

        public UOMData[] UOMData { get; set; }
    }
}
