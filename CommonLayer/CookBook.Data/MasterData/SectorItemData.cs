﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.MasterData
{
    public partial class SectorItemData
    {
        public DietCategoryData[] DietCategoryData { get; set; }
        public SubSectorData[] SubSectorData { get; set; }
        public FoodProgramData[] FoodProgramData { get; set; }
        public CommonMasterData[] ItemSectorMasterData { get; set; }
        public CommonMasterData[] MenuType2MasterData { get; set; }
        public TextureData[] TextureData { get; set; }
        public DayPartData[] DayPartData { get; set; }
        public UOMModuleMappingData[] UOMModuleMappingData { get; set; }
        public DishCategoryData[] DishCategoryData { get; set; }
        public DishData[] DishData { get; set; }
        public ItemData[] ItemData { get; set; }

        //AddEdit
        public VisualCategoryData[] VisualCategoryData { get; set; }
        public ServewareData[] ServewareData { get; set; }
        public ItemType1Data[] ItemType1Data { get; set; }
        public ItemType2Data[] ItemType2Data { get; set; }
        public ConceptType1Data[] ConceptType1Data { get; set; }
        public ConceptType2Data[] ConceptType2Data { get; set; }
        public ConceptType3Data[] ConceptType3Data { get; set; }
        public ConceptType4Data[] ConceptType4Data { get; set; }
        public ConceptType5Data[] ConceptType5Data { get; set; }
    }
}
