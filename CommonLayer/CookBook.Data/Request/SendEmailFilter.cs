﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.Request
{
    public class SendEmailFilter
    {
        public int MenuRequestId { get; set; }

        public int SiteCafeId { get; set; }

        public string Period { get; set; }

        public string SiteName { get; set; }

        public string LoggedInUsername { get; set; }
    }
}
