﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Data.Request
{
    public class RequestData
    {
        public string SessionSectorName { get; set; }
        public string SessionSectorNumber { get; set; }
        public int SessionUserId { get; set; }
        public string SubSectorCode { get; set; }
        public int RegionID { get; set; }
        public string SiteCode { get; set; }
        public string ItemCode { get; set; }
        public string RecipeCode { get; set; }

        //Krish
        public string DishCode { get; set; }
        public int SessionUserRoleId { get; set; }
    }
}
