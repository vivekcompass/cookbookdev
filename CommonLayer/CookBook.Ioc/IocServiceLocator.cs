﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookBook.Ioc
{
    public class IocServiceLocator : ServiceLocatorImplBase
    {
        /// <summary>Exposes underlying Container for direct operation.</summary>
        public readonly IUnityContainer Container;

    /// <summary>Initializes a new instance of the <see cref="DryIocServiceLocator" /> class</summary>
    /// <summary>Creates new locator as adapter for provided container.</summary>
    /// <param name="container">Container to use/adapt.</param>
    public IocServiceLocator(IUnityContainer container)
    {
        if (container == null)
        {
            throw new ArgumentNullException("container");
        }
        Container = container;
    }

    /// <summary>Resolves service from container. Throws if unable to resolve.</summary>
    /// <param name="serviceType">Service type to resolve.</param>
    /// <param name="key">(optional) Service key to resolve.</param>
    /// <returns>Resolved service object.</returns>
    protected override object DoGetInstance(Type serviceType, string key)
    {
        if (serviceType == null)
        {
            throw new ArgumentNullException("serviceType");
        }
        return Container.Resolve(serviceType, key);
    }

    /// <summary>Returns enumerable which when enumerated! resolves all default and named 
    /// implementations/registrations of requested service type. 
    /// If no services resolved when enumerable accessed, no exception is thrown - enumerable is empty.</summary>
    /// <param name="serviceType">Service type to resolve.</param>
    /// <returns>Returns enumerable which will return resolved service objects.</returns>
    protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
    {
        if (serviceType == null)
        {
            throw new ArgumentNullException("serviceType");
        }
        return Container.ResolveAll<object>();
    }
}
}
