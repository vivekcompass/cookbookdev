﻿using Microsoft.Practices.Unity;

namespace CookBook.Ioc
{
    public interface IIocManager
    {
        T Resolve<T>();
        void BuildUp<T>(T existingObject);
        IUnityContainer Container { get; set; }
    }
}
