﻿using CookBook.Business.Contract;
using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.Data.MasterData;
using CookBook.ServiceApp.Core;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Microsoft.Practices.Unity;
using System.ComponentModel.Composition;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using System;

namespace CookBook.ServiceApp
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    [ExceptionShielding("ServiceExceptionPolicy")]
    [Export(typeof(ICookBookService))]
    public partial class CookBookService : ServiceBase, ICookBookService
    {
        [Dependency]
        public IUser UserManager { get; set; }

        [Dependency]

        public IEmailManager EmailManager { get; set; }

        [Dependency]
        public IMasterDataManager MasterDataManager { get; set; }

        [Dependency]
        public IItemDataManager ItemDataManager { get; set; }

        [Dependency]
        public IDishDataManager DishDataManager { get; set; }

        [Dependency]
        public IMOGDataManager MOGDataManager { get; set; }

        [Dependency]
        public IRecipeDataManager RecipeDataManager { get; set; }

        [Dependency]
        public ICostSimulatorManager CostSimulatorManager { get; set; }

        //Krish
        [Dependency]
        public IFoodCostSimulatorManager FoodCostSimulatorManager { get; set; }
        //Krish
        [Dependency]
        public IPatientMasterDataManager PatientMasterDataManager { get; set; }
        [Dependency]
        public IBedMasterDataManager BedMasterDataManager { get; set; }

        [Dependency]
        public IDieticianMasterDataManager DieticianMasterDataManager { get; set; }
        
        public UserMasterResponseData GetUserData(string userName, RequestData request)
        {
            return UserManager.GetUserData(userName, request);
        }
        public IList<SiteTypeData> GetSiteTypeDataList(RequestData request)
        {
            return MasterDataManager.GetSiteTypeDataList(request);
        }

        public IList<AllergenData> GetAllergenDataList(RequestData request)
        {
            return MasterDataManager.GetAllergenDataList(request);
        }
        public string UpdateMOGLIST(RequestData request, string xml, string username)
        {
            return MOGDataManager.UpdateMOGLIST(request, xml, username);

        }
        public string UpdateMOGLISTAPL(RequestData request, string xml, string username)
        {
            return MasterDataManager.UpdateMOGLISTAPL(request, xml, username);

        }
        public IList<APLMasterData> GetAPLMasterDataList(string articleNumber, string mogCode, RequestData request)
        {
            return MasterDataManager.GetAPLMasterDataList(articleNumber, mogCode, request);
        }

        public IList<APLMasterData> GetAPLMasterDataListForSector(string articleNumber, string mogCode, RequestData request)
        {
            return MasterDataManager.GetAPLMasterDataListForSector(articleNumber, mogCode, request);
        }


        public IList<APLMasterData> NationalGetAPLMasterDataList(RequestData request)
        {
            return MasterDataManager.NationalGetAPLMasterDataList(request);
        }

        public IList<APLMasterData> GetAPLMasterDataListPageing(RequestData request, int pageindex)
        {
            return MasterDataManager.GetAPLMasterDataListPageing(request, pageindex);
        }

        public IList<APLMasterData> GetAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, RequestData request)
        {
            return MasterDataManager.GetAPLMasterSiteDataList(articleNumber, mogCode, SiteCode, request);
        }

        public IList<APLMasterData> GetAPLMasterRegionDataList(string articleNumber, string mogCode, int Region_ID, RequestData request)
        {
            return MasterDataManager.GetAPLMasterRegionDataList(articleNumber, mogCode, Region_ID, request);
        }

        public IList<ConceptType1Data> GetConceptType1DataList(RequestData request)
        {
            return MasterDataManager.GetConceptType1DataList(request);
        }
        public IList<ConceptType2Data> GetConceptType2DataList(RequestData request)
        {
            return MasterDataManager.GetConceptType2DataList(request);
        }
        public IList<ConceptType3Data> GetConceptType3DataList(RequestData request)
        {
            return MasterDataManager.GetConceptType3DataList(request);
        }
        public IList<ConceptType4Data> GetConceptType4DataList(RequestData request)
        {
            return MasterDataManager.GetConceptType4DataList(request);
        }
        public IList<ConceptType5Data> GetConceptType5DataList(RequestData request)
        {
            return MasterDataManager.GetConceptType5DataList(request);
        }
        public IList<DayPartData> GetDayPartDataList(RequestData request)
        {
            return MasterDataManager.GetDayPartDataList(request);
        }
        public IList<RevenueTypeData> GetRevenueTypeDataList(RequestData request)
        {
            return MasterDataManager.GetRevenueTypeDataList(request);
        }
        public IList<DietCategoryData> GetDietCategoryDataList(RequestData request)
        {
            return MasterDataManager.GetDietCategoryDataList(request);
        }
        public IList<ReasonData> GetReasonDataList(RequestData request)
        {
            return MasterDataManager.GetReasonDataList(request);
        }

        public IList<SubSectorData> GetSubSectorDataList(RequestData request)
        {
            return MasterDataManager.GetSubSectorDataList(request);
        }
        public IList<DishCategoryData> GetDishCategoryDataList(RequestData request)
        {
            return MasterDataManager.GetDishCategoryDataList(request);
        }

       

        public IList<NationalDishCategoryData> NationalGetDishCategoryDataList(RequestData request)
        {
            return MasterDataManager.NationalGetDishCategoryDataList(request);
        }

        public IList<NationalDishData> NationalGetDishDataList(RequestData request)
        {
            return MasterDataManager.NationalGetDishDataList(request);
        }
        public IList<DishSubCategoryData> GetDishSubCategoryDataList(RequestData request)
        {
            return MasterDataManager.GetDishSubCategoryDataList(request);
        }
        public IList<DishTypeData> GetDishTypeDataList(RequestData request)
        {
            return MasterDataManager.GetDishTypeDataList(request);
        }
        public IList<FoodProgramData> GetFoodProgramDataList(RequestData request)
        {
            return MasterDataManager.GetFoodProgramDataList(request);
        }
        public IList<ContainerData> GetContainerDataList(RequestData request)
        {
            return MasterDataManager.GetContainerDataList(request);
        }
        public IList<ItemType1Data> GetItemType1DataList(RequestData request)
        {
            return MasterDataManager.GetItemType1DataList(request);
        }
        public IList<ItemType2Data> GetItemType2DataList(RequestData request)
        {
            return MasterDataManager.GetItemType2DataList(request);
        }

        public IList<ServewareData> GetServewareDataList(RequestData request)
        {
            return MasterDataManager.GetServewareDataList(request);
        }

        public IList<ServingTemperatureData> GetServingTemperatureDataList(RequestData request)
        {
            return MasterDataManager.GetServingTemperatureDataList(request);
        }
        public IList<SiteProfile1Data> GetSiteProfile1DataList(RequestData request)
        {
            return MasterDataManager.GetSiteProfile1DataList(request);
        }
        public IList<SiteProfile2Data> GetSiteProfile2DataList(RequestData request)
        {
            return MasterDataManager.GetSiteProfile2DataList(request);
        }
        public IList<SiteProfile3Data> GetSiteProfile3DataList(RequestData request)
        {
            return MasterDataManager.GetSiteProfile3DataList(request);
        }
        public IList<UOMData> GetUOMDataList(RequestData request)
        {
            return MasterDataManager.GetUOMDataList(request);
        }
        public IList<UOMModuleMappingData> GetUOMModuleMappingDataList(RequestData request)
        {
            return MasterDataManager.GetUOMModuleMappingDataList(request);
        }

        public IList<GetUOMModuleMappingGridData> procGetUOMModuleMappingGridData(RequestData request)
        {
            return MasterDataManager.procGetUOMModuleMappingGridData(request);
        }


        public IList<ReasonTypeData> GetReasonTypeDataList(RequestData request)
        {
            return MasterDataManager.GetReasonTypeDataList(request);
        }
        public IList<VisualCategoryData> GetVisualCategoryDataList(RequestData request)
        {
            return MasterDataManager.GetVisualCategoryDataList(request);
        }

        public IList<ColorData> GetColorDataList(RequestData request)
        {
            return MasterDataManager.GetColorDataList(request);
        }

        public IList<CPUData> GetCPUDataList(RequestData request)
        {
            return MasterDataManager.GetCPUDataList(request);
        }

        public IList<DKData> GetDKDataList(RequestData request)
        {
            return MasterDataManager.GetDKDataList(request);
        }

        public IList<CafeData> GetCafeDataList(RequestData request)
        {
            return MasterDataManager.GetCafeDataList(request);
        }

        public IList<CapringMasterData> GetCapringMasterDataList(RequestData request)
        {
            return MasterDataManager.GetCapringMasterDataList(request);
        }
        public IList<ContainerTypeData> GetContainerTypeDataList(RequestData request)
        {
            return MasterDataManager.GetContainerTypeDataList(request);
        }
        public IList<ApplicationSettingData> GetApplicationSettingDataList(RequestData request)
        {
            return MasterDataManager.GetApplicationSettingDataList(request);
        }
        public IList<ItemData> GetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, string sectorName, RequestData request)
        {
            return ItemDataManager.GetItemDataList(itemCode, foodProgramID, dietCategoryID, sectorName, request);
        }
        public IList<NationalItemData> NationalGetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, string sectorName, RequestData request)
        {
            return ItemDataManager.NationalGetItemDataList(itemCode, foodProgramID, dietCategoryID, sectorName, request);
        }
        public IList<ItemData> GetDishImpactedItemList(string dishCode, RequestData request)
        {
            return ItemDataManager.GetDishImpactedItemList(dishCode, request);
        }
        public IList<UnitPriceData> GetSiteUnitPrice(string siteCode, RequestData request)
        {
            return ItemDataManager.GetSiteUnitPrice(siteCode, request);
        }

        public IList<SiteDayPartMappingData> GetSiteDayPartMapping(string siteCode, RequestData request)
        {
            return ItemDataManager.GetSiteDayPartMapping(siteCode, request);
        }
        public IList<PriceHistoryData> GetSiteItemPriceHistory(string siteCode, string itemCode, RequestData request)
        {
            return ItemDataManager.GetSiteItemPriceHistory(siteCode, itemCode, request);
        }
        public IList<RegionInheritedData> GetRegionInheritedData(string RegionID, string SectorID, RequestData request)
        {
            return ItemDataManager.GetRegionInheritedData(RegionID, SectorID, request);
        }

        public IList<DishData> GetDishDataList(int? foodProgramID, int? dietCategoryID, RequestData request)
        {
            return DishDataManager.GetDishDataList(foodProgramID, dietCategoryID, request);
        }
        public IList<DishData> GetSiteDishDataList(string siteCode, int? foodProgramID, int? dietCategoryID, RequestData request)
        {
            return DishDataManager.GetSiteDishDataList(siteCode, foodProgramID, dietCategoryID, request);
        }

        public IList<DishData> GetRegionDishMasterData(int regionID, int? foodProgramID, int? dietCategoryID, RequestData request)
        {
            return DishDataManager.GetRegionDishMasterData(regionID, foodProgramID, dietCategoryID, request);
        }
        public IList<DishData> GetRegionDishDataList(int RegionID, int? foodProgramID, int? dietCategoryID, string subSectorCode, RequestData request)
        {
            return DishDataManager.GetRegionDishDataList(RegionID, foodProgramID, dietCategoryID, subSectorCode, request);
        }
        public IList<MOGData> GetMOGDataList(RequestData request, string siteCode, int regionID)
        {
            return MOGDataManager.GetMOGDataList(request, siteCode, regionID);
        }

        public IList<MOGInheritedData> GetInheritedMOGDataList(RequestData request)
        {
            return MOGDataManager.GetInheritedMOGDataList(request);
        }

        public IList<NationalMOGData> GetNationalMOGDataList(RequestData request)
        {
            return MOGDataManager.GetNationalMOGDataList(request);
        }

        public List<ManageSectorData> GetSectorDataList(RequestData request)
        {
            return UserManager.GetSectorDataList(request);
        }

        public string SaveManageSectorInfo(RequestData request, ManageSectorData model, int usercode)
        {
            return UserManager.SaveManageSectorInfo(request, model, usercode);
        }


        public string SaveManageLevelInfo(RequestData request, LevelMasterData model, int usercode)
        {
            return UserManager.SaveManageLevelInfo(request, model, usercode);
        }

        public string SaveFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode)
        {
            return UserManager.SaveFunctionLevelInfo(request, model, usercode);
        }

        public string SavePermissionInfo(RequestData request, PermissionMasterData model, int usercode)
        {
            return UserManager.SavePermissionInfo(request, model, usercode);
        }

        public string UpdatePermissionInfo(RequestData request, PermissionMasterData model, int usercode)
        {
            return UserManager.UpdatePermissionInfo(request, model, usercode);
        }

        public string UpdateFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode)
        {
            return UserManager.UpdateFunctionLevelInfo(request, model, usercode);
        }

        public string UpdatefunctionInactiveActive(RequestData request, FunctionMasterData model, int usercode)
        {
            return UserManager.UpdatefunctionInactiveActive(request, model, usercode);
        }

        public string UpdatePermissionInactiveActive(RequestData request, PermissionMasterData model, int usercode)
        {
            return UserManager.UpdatePermissionInactiveActive(request, model, usercode);
        }

        public string UpdateManageLevelInfo(RequestData request, LevelMasterData model, int usercode)
        {
            return UserManager.UpdateManageLevelInfo(request, model, usercode);
        }


        public string UpdateManageSectorInfo(RequestData request, ManageSectorData model, int usercode)
        {
            return UserManager.UpdateManageSectorInfo(request, model, usercode);
        }

        public string UpdateInactiveActive(RequestData request, ManageSectorData model, int usercode)
        {
            return UserManager.UpdateInactiveActive(request, model, usercode);
        }

        public string UpdateLevelInactiveActive(RequestData request, LevelMasterData model, int usercode)
        {
            return UserManager.UpdateLevelInactiveActive(request, model, usercode);
        }

        public List<LevelMasterData> GetLevelDataList(RequestData request)
        {
            return UserManager.GetLevelDataList(request);
        }
        public List<FunctionMasterData> GetFunctionDataList(RequestData request)
        {
            return UserManager.GetFunctionDataList(request);
        }
        public List<PermissionMasterData> GetPermissionDataList(RequestData request)
        {
            return UserManager.GetPermissionDataList(request);
        }
        public IList<MOGNotMapped> GetNOTMappedMOGDataList(RequestData request)
        {
            return MOGDataManager.GetNOTMappedMOGDataList(request);
        }
        public IList<APLMOGDATA> GetNOTMappedMOGDataListAPL(RequestData request)
        {
            return MasterDataManager.GetNOTMappedMOGDataListAPL(request);
        }
        public List<MOGAPLMAPPEDHISTORY> GetMOGAPLHISTORY(RequestData request, string username)
        {
            return MOGDataManager.GetMOGAPLHISTORY(request, username);
        }


        public List<APLMAPPEDHISTORYDLIST> GetMOGAPLHISTORYAPL(RequestData request, string username)
        {
            return MasterDataManager.GetMOGAPLHISTORYAPL(request, username);
        }

        public List<APLMAPPEDHISTORYDLIST> GetshowGetALLHISTORYBATCHWISEAPL(RequestData request, string username, string batchno)
        {
            return MasterDataManager.GetshowGetALLHISTORYBATCHWISEAPL(request, username, batchno);
        }


        public List<MOGAPLMAPPEDHISTORY> GetMOGAPLHISTORYDet(RequestData request, string username)
        {
            return MOGDataManager.GetMOGAPLHISTORYDet(request, username);
        }

        public List<APLMAPPEDHISTORYDLIST> GetMOGAPLHISTORYDetAPL(RequestData request, string username)
        {
            return MasterDataManager.GetMOGAPLHISTORYDetAPL(request, username);
        }

        public List<MOGAPLMAPPEDHISTORY> GetshowGetALLHISTORYBATCHWISE(RequestData request, string username, string batchno)
        {
            return MOGDataManager.GetshowGetALLHISTORYBATCHWISE(request, username, batchno);
        }

        public List<MOGAPLMAPPEDHISTORY> GetALLHISTORYDetails(RequestData request, string username)
        {
            return MOGDataManager.GetALLHISTORYDetails(request, username);
        }

        public List<APLMAPPEDHISTORYDLIST> GetALLHISTORYDetailsAPL(RequestData request, string username)
        {
            return MasterDataManager.GetALLHISTORYDetailsAPL(request, username);
        }

        public IList<APLMasterData> GetAPLMasterDataListCommon(string articleNumber, string mogCode, RequestData request)
        {
            return MOGDataManager.GetAPLMasterDataListCommon(articleNumber, mogCode, request);
        }

        public MOGImpactedData GetMOGImpactedDataList(int mogID, int recipeID, bool isBaseRecipe, RequestData request)
        {
            return MOGDataManager.GetMOGImpactedDataList(mogID, recipeID, isBaseRecipe, request);
        }

        public IList<SiteMasterData> GetSiteMasterList(RequestData request)
        {
            return ItemDataManager.GetSiteMasterList(request);
        }
        public IList<RecipeData> GetRecipeDataList(string condition, string DishCode, RequestData request)
        {
            return RecipeDataManager.GetRecipeDataList(condition, DishCode, request);
        }
        public IList<NationalRecipeData> GetNationalRecipeDataList(string condition, string DishCode, RequestData request)
        {
            return RecipeDataManager.GetNationalRecipeDataList(condition, DishCode, request);
        }
        public IList<RegionRecipeData> GetRegionRecipeDataList(int RegionID, string condition, RequestData request)
        {
            return RecipeDataManager.GetRegionRecipeDataList(RegionID, condition, request);
        }
        public IList<SiteRecipeData> GetSiteRecipeDataList(string SiteCode, string condition, RequestData request)
        {
            return RecipeDataManager.GetSiteRecipeDataList(SiteCode, condition, request);
        }
        //Krish
        public IList<SiteRecipeData> GetSiteDishRecipeDataList(string SiteCode, string dishCode, string condition, RequestData request)
        {
            return null; //RecipeDataManager.GetSiteDishRecipeDataList(SiteCode, dishCode, condition, request);
        }
        public IList<RecipeMOGMappingData> GetBaseRecipesByRecipeID(int recipeID, RequestData request)
        {
            return RecipeDataManager.GetBaseRecipesByRecipeID(recipeID, request);
        }

        public IList<RecipeMOGMappingData> GetRegionBaseRecipesByRecipeID(int regionID, int recipeID, RequestData request)
        {
            return RecipeDataManager.GetRegionBaseRecipesByRecipeID(regionID, recipeID, request);
        }

        public IList<RecipeMOGMappingData> GetSiteBaseRecipesByRecipeID(string recipeCode, string siteCode, int recipeID, RequestData request)
        {
            return RecipeDataManager.GetSiteBaseRecipesByRecipeID(siteCode, recipeID, recipeCode, request);
        }

        public IList<RecipeMOGMappingData> GetMOGsByRecipeID(int recipeID, RequestData request)
        {
            return RecipeDataManager.GetMOGsByRecipeID(recipeID, request);
        }

        public IList<RecipeMOGMappingData> GetRegionMOGsByRecipeID(int regionID, int recipeID, RequestData request)
        {
            return RecipeDataManager.GetRegionMOGsByRecipeID(regionID, recipeID, request);
        }
        public IList<RecipeMOGMappingData> GetSiteMOGsByRecipeID(string recipeCode, string siteCode, int recipeID, RequestData request)
        {
            return RecipeDataManager.GetSiteMOGsByRecipeID(siteCode, recipeID, recipeCode, request);
        }
        public IList<SectorData> GetSectorDataByUserID(int userID, RequestData request)
        {
            return UserManager.GetSectorDataByUserID(userID, request);
        }

        public string ChangeSectorData(int UserID, string sectorNumber, RequestData request)
        {
            return UserManager.ChangeSectorData(UserID, sectorNumber, request);
        }

        public string ChangeUserRoleData(int UserID, int userRoleID, RequestData requestData)
        {
            return UserManager.ChangeUserRoleData(UserID, userRoleID, requestData);
        }

        public string SaveFoodProgramData(FoodProgramData model, RequestData request)
        {
            return MasterDataManager.SaveFoodProgramData(model, request);
        }

        public string SaveContainerData(ContainerData model, RequestData request)
        {
            return MasterDataManager.SaveContainerData(model, request);
        }

        public string SaveVisualCategoryData(VisualCategoryData model, RequestData request)
        {
            return MasterDataManager.SaveVisualCategoryData(model, request);
        }

        public string SaveColorData(ColorData model, RequestData request)
        {
            return MasterDataManager.SaveColorData(model, request);
        }

        public string SaveDishCategoryData(DishCategoryData model, RequestData request)
        {
            return MasterDataManager.SaveDishCategoryData(model, request);
        }

        public string SaveDishSubCategoryData(DishSubCategoryData model, RequestData request)
        {
            return MasterDataManager.SaveDishSubCategoryData(model, request);
        }

        public string SaveConceptType1Data(ConceptType1Data model, RequestData request)
        {
            return MasterDataManager.SaveConceptType1Data(model, request);
        }

        public string SaveConceptType2Data(ConceptType2Data model, RequestData request)
        {
            return MasterDataManager.SaveConceptType2Data(model, request);
        }

        public string SaveConceptType3Data(ConceptType3Data model, RequestData request)
        {
            return MasterDataManager.SaveConceptType3Data(model, request);
        }

        public string SaveConceptType4Data(ConceptType4Data model, RequestData request)
        {
            return MasterDataManager.SaveConceptType4Data(model, request);
        }

        public string SaveConceptType5Data(ConceptType5Data model, RequestData request)
        {
            return MasterDataManager.SaveConceptType5Data(model, request);
        }

        public string SaveSiteProfile1Data(SiteProfile1Data model, RequestData request)
        {
            return MasterDataManager.SaveSiteProfile1Data(model, request);
        }

        public string SaveSiteProfile2Data(SiteProfile2Data model, RequestData request)
        {
            return MasterDataManager.SaveSiteProfile2Data(model, request);
        }

        public string SaveSiteProfile3Data(SiteProfile3Data model, RequestData request)
        {
            return MasterDataManager.SaveSiteProfile3Data(model, request);
        }

        public string SaveItemType1Data(ItemType1Data model, RequestData request)
        {
            return MasterDataManager.SaveItemType1Data(model, request);
        }

        public string SaveItemType2Data(ItemType2Data model, RequestData request)
        {
            return MasterDataManager.SaveItemType2Data(model, request);
        }

        public string SaveDietCategoryData(DietCategoryData model, RequestData request)
        {
            return MasterDataManager.SaveDietCategoryData(model, request);
        }
        public string SaveReasonData(ReasonData model, RequestData request)
        {
            return MasterDataManager.SaveReasonData(model, request);
        }
        public string SaveDayPartData(DayPartData model, RequestData request)
        {
            return MasterDataManager.SaveDayPartData(model, request);
        }
        public string SaveRevenueTypeData(RevenueTypeData model, RequestData request)
        {
            return MasterDataManager.SaveRevenueTypeData(model, request);
        }

        public string SaveServewareData(ServewareData model, RequestData request)
        {
            return MasterDataManager.SaveServewareData(model, request);
        }
        public string SaveSiteTypeData(SiteTypeData model, RequestData request)
        {
            return MasterDataManager.SaveSiteTypeData(model, request);
        }
        public string SaveAllergenData(AllergenData model, RequestData request)
        {
            return MasterDataManager.SaveAllergenData(model, request);
        }

        public string SaveAPLMasterData(APLMasterData model, RequestData request)
        {
            return MasterDataManager.SaveAPLMasterData(model, request);
        }

        public string SaveReasonTypeData(ReasonTypeData model, RequestData request)
        {
            return MasterDataManager.SaveReasonTypeData(model, request);
        }

        public string SaveAPLMasterDataList(IList<APLMasterData> model, RequestData request)
        {
            return MasterDataManager.SaveAPLMasterDataList(model, request);
        }

        public string SaveUOMData(UOMData model, RequestData request)
        {
            return MasterDataManager.SaveUOMData(model, request);
        }

        public string SaveUOMModuleMasterData(UOMModuleData model, RequestData request)
        {
            return MasterDataManager.SaveUOMModuleData(model, request);
        }

        public string SaveUOMModuleMappingData(UOMModuleMappingData model, RequestData request)
        {
            return MasterDataManager.SaveUOMModuleMappingData(model, request);
        }

        public string SaveServingTemperatureData(ServingTemperatureData model, RequestData request)
        {
            return MasterDataManager.SaveServingTemperatureData(model, request);
        }

        public string SaveSiteData(SiteMasterData model, RequestData request)
        {
            return MasterDataManager.SaveSiteData(model, request);
        }

        public string SaveSiteDataList(IList<SiteMasterData> model, RequestData request)
        {
            return MasterDataManager.SaveSiteDataList(model, request);
        }

        public string SaveCafeData(CafeData model, RequestData request)
        {
            return MasterDataManager.SaveCafeData(model, request);
        }

        public string SaveContainerTypeData(ContainerTypeData model, RequestData request)
        {
            return MasterDataManager.SaveContainerTypeData(model, request);
        }
        public string SaveApplicationSettingData(ApplicationSettingData model, RequestData request)
        {
            return MasterDataManager.SaveApplicationSettingData(model, request);
        }

        public string SaveCafeDataList(IList<CafeData> model, RequestData request)
        {
            return MasterDataManager.SaveCafeDataList(model, request);
        }
        public string SaveContainerTypeDataList(IList<ContainerTypeData> model, RequestData request)
        {
            return MasterDataManager.SaveContainerTypeDataList(model, request);
        }
        public IList<SiteMasterData> GetSiteMasterDataList(RequestData request)
        {
            return MasterDataManager.GetSiteMasterDataList(request);
        }
        //Krish
        public IList<SiteMasterData> GetSiteByRegionDataList(int regionId, RequestData request)
        {
            return MasterDataManager.GetSiteByRegionDataList(regionId, request);
        }
        public IList<SiteMasterData> NationalGetSiteMasterDataList(RequestData request)
        {
            return MasterDataManager.NationalGetSiteMasterDataList(request);
        }
        public IList<RegionMasterData> GetRegionMasterDataList(RequestData request)
        {
            return MasterDataManager.GetRegionMasterDataList(request);
        }

        public string SaveItemData(ItemData model, RequestData request)
        {
            return ItemDataManager.SaveItemData(model, request);
        }

        public string SaveItemDataList(IList<ItemData> model, RequestData request)
        {
            return ItemDataManager.SaveItemDataList(model, request);
        }
        public string SaveItemDishMappingDataList(IList<ItemDishMappingData> model, RequestData request)
        {
            return ItemDataManager.SaveItemDishMappingDataList(model, request);
        }

        public string SaveDishData(DishData model, RequestData request)
        {
            return DishDataManager.SaveDishData(model, request);
        }

        public string SaveDishDataList(IList<DishData> model, RequestData request)
        {
            return DishDataManager.SaveDishDataList(model, request);
        }

        public string SaveMOGData(MOGData model, RequestData request)
        {
            return MOGDataManager.SaveMOGData(model, request);
        }

        public string SaveMOGDataList(IList<MOGData> model, RequestData request)
        {
            return MOGDataManager.SaveMOGDataList(model, request);
        }

        public string SaveRecipeData(RecipeData model, RecipeMOGMappingData[] baseRecipes, RecipeMOGMappingData[] mogs, RequestData request)
        {
            return RecipeDataManager.SaveRecipeData(model, baseRecipes, mogs, request);
        }

        public string SaveRegionRecipeData(RegionRecipeData model, RegionRecipeMOGMappingData[] baseRecipes, RegionRecipeMOGMappingData[] mogs, RequestData request)
        {
            return RecipeDataManager.SaveRegionRecipeData(model, baseRecipes, mogs, request);
        }

        public string SaveSiteRecipeData(SiteRecipeData model, SiteRecipeMOGMappingData[] baseRecipes, SiteRecipeMOGMappingData[] mogs, RequestData request)
        {
            return RecipeDataManager.SaveSiteRecipeData(model, baseRecipes, mogs, request);
        }
        public string SaveRecipeDataList(IList<RecipeData> model, RequestData request)
        {
            return RecipeDataManager.SaveRecipeDataList(model, request);
        }
        public string SaveRegionRecipeDataList(IList<RegionRecipeData> model, RequestData request)
        {
            return RecipeDataManager.SaveRegionRecipeDataList(model, request);
        }
        public string SaveSiteRecipeDataList(IList<SiteRecipeData> model, RequestData request)
        {
            return RecipeDataManager.SaveSiteRecipeDataList(model, request);
        }
        public bool SaveReminderEmail(string webSiteUrl, SendEmailFilter request, RequestData requestData)
        {
            return EmailManager.SaveReminderEmail(webSiteUrl, request, requestData);
        }
        public bool SendExceptionEmail(RequestData request)
        {
            EmailManager.SendExceptionEmail(request);
            return true;
        }

        public bool SendMOGEmail(MOGData model, RequestData request)
        {
            EmailManager.SendMOGEmail(model, request);
            return true;
        }

        public bool SendNewMOGEmail(MOGData model, RequestData request)
        {
            EmailManager.SendNewMOGEmail(model, request);
            return true;
        }

        public bool SendRecipeEmail(RecipeData model, RequestData request)
        {
            EmailManager.SendRecipeEmail(model, request);
            return true;
        }


        public IList<DishCategoryData> GetDishCategory(RequestData request)
        {
            return MasterDataManager.GetDishCategoryDataList(request);
        }

        public IList<ItemDishMappingData> GetItemDishMappingDataList(string itemCode, RequestData request) => ItemDataManager.GetItemDishMappingDataList(itemCode, request);

        public IList<ItemDishCategoryMappingData> GetItemDishCategoryMappingDataList(string itemCode, RequestData request) => ItemDataManager.GetItemDishCategoryMappingDataList(itemCode, request);

        public IList<RegionItemDishMappingData> GetMasterRegionItemDishMappingDataList(string regionID, RequestData request)
        {
            return ItemDataManager.GetMasterRegionItemDishMappingDataList(regionID, request);
        }

        public IList<RegionItemDishMappingData> GetRegionItemDishMappingDataList(string itemCode, string regionID, RequestData request)
        {
            return ItemDataManager.GetRegionItemDishMappingDataList(itemCode, regionID, request);
        }

        public IList<SiteItemDishMappingData> GetMasterSiteItemDishMappingDataList(string siteCode, RequestData request)
        {
            return ItemDataManager.GetMasterSiteItemDishMappingDataList(siteCode, request);
        }

        public IList<SiteItemDishMappingData> GetSiteItemDishMappingDataList(string itemCode, string siteCode, RequestData request)
        {
            return ItemDataManager.GetSiteItemDishMappingDataList(itemCode, siteCode, request);
        }
        public IList<FoodBookNutritionData> GetFoodBookNutritionDataList(String sectorCode)
        {
            return ItemDataManager.GetFoodBookNutritionDataList(sectorCode);
        }
        public IList<SiteItemDayPartMappingData> GetSiteItemDayPartMappingDataList(string itemCode, string siteCode, RequestData request)
        {
            return ItemDataManager.GetSiteItemDayPartMappingDataList(itemCode, siteCode, request);
        }
        public IList<SiteDayPartMappingData> GetSiteDayPartMappingDataList(string siteCode, RequestData request)
        {
            return ItemDataManager.GetSiteDayPartMappingDataList(siteCode, request);
        }
        public IList<FoodProgramDayPartMappingData> GetFoodProgramDayPartMappingDataList(string foodCode, RequestData request)
        {
            return ItemDataManager.GetFoodProgramDayPartMappingDataList(foodCode, request);
        }
        public string SaveRegionItemDishMappingDataList(IList<RegionItemDishMappingData> model, RequestData request)
        {
            return ItemDataManager.SaveRegionItemDishMappingDataList(model, request);
        }



        public string SaveSiteItemDishMappingDataList(IList<SiteItemDishMappingData> model, RequestData request)
        {
            return ItemDataManager.SaveSiteItemDishMappingDataList(model, request);
        }

        public string SaveSiteItemDayPartMappingDataList(IList<SiteItemDayPartMappingData> model, RequestData request)
        {
            return ItemDataManager.SaveSiteItemDayPartMappingDataList(model, request);
        }
        public string SaveSiteDayPartMappingDataList(IList<SiteDayPartMappingData> model, bool isClosingTime, RequestData request)
        {
            return ItemDataManager.SaveSiteDayPartMappingDataList(model, isClosingTime, request);
        }
        public string SaveFoodProgramDayPartMappingDataList(IList<FoodProgramDayPartMappingData> model, RequestData request)
        {
            return ItemDataManager.SaveFoodProgramDayPartMappingDataList(model, request);
        }

        public string SaveItemDishCategoryMappingDataList(IList<ItemDishCategoryMappingData> model, RequestData request)
        {
            return ItemDataManager.SaveItemDishCategoryMappingDataList(model, request);
        }

        IList<ItemDishCategoryMappingData> ICookBookService.GetItemDishCategoryMappingDataList(string itemCode, RequestData request)
        {
            return ItemDataManager.GetItemDishCategoryMappingDataList(itemCode, request);
        }

        public string SaveRegionDishCategoryMappingDataList(IList<RegionDishCategoryMappingData> model, RequestData request)
        {
            return ItemDataManager.SaveRegionDishCategoryMappingDataList(model, request);
        }

        public string SaveSiteDishCategoryMappingDataList(IList<SiteDishCategoryMappingData> model, RequestData request)
        {
            return ItemDataManager.SaveSiteDishCategoryMappingDataList(model, request);
        }

        public IList<RegionDishCategoryMappingData> GetRegionDishCategoryMappingDataList(string regionID, string itemCode, RequestData request)
        {
            return ItemDataManager.GetRegionDishCategoryMappingDataList(regionID, itemCode, request);
        }

        public IList<SiteDishCategoryMappingData> GetSiteDishCategoryMappingDataList(string siteCode, string itemCode, RequestData request)
        {
            return ItemDataManager.GetSiteDishCategoryMappingDataList(siteCode, itemCode, request);
        }

        public IList<RegionItemInheritanceMappingData> GetRegionItemInheritanceMappingDataList(string regionID, RequestData request)
        {
            return ItemDataManager.GetRegionItemInheritanceMappingDataList(regionID, request);
        }
        public IList<SiteItemInheritanceMappingData> GetSiteItemInheritanceMappingDataList(string SiteID, RequestData request)
        {
            return ItemDataManager.GetSiteItemInheritanceMappingDataList(SiteID, request);
        }

        public string SaveSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData request)
        {
            return ItemDataManager.SaveSectorToRegionInheritance(model, request);
        }

        public string SaveMOGNationalToSectorInheritance(SaveMOGNationalToSectorInheritanceData model, RequestData request)
        {
            return ItemDataManager.SaveMOGNationalToSectorInheritance(model, request);
        }

        public string UpdateCostSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData request)
        {
            return ItemDataManager.UpdateCostSectorToRegionInheritance(model, request);
        }
        public string SaveRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData request)
        {
            return ItemDataManager.SaveRegionToSiteInheritance(model, request);
        }
        public string SaveToSiteInheritance(SiteItemInheritanceMappingData model, RequestData request)
        {
            return ItemDataManager.SaveToSiteInheritance(model, request);
        }
        public string SaveToSiteInheritanceDataList(IList<SiteItemInheritanceMappingData> model, RequestData request)
        {
            return ItemDataManager.SaveToSiteInheritanceDataList(model, request);
        }
        public string UpdateCostRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData request)
        {
            return ItemDataManager.UpdateCostRegionToSiteInheritance(model, request);
        }
        public IList<string> GetUniqueMappedItemCodesFromItem(RequestData request)
        {
            return ItemDataManager.GetUniqueMappedItemCodesFromItem(request);
        }

        public IList<string> GetUniqueMappedItemCodesFromRegion(string RegionID, RequestData request)
        {
            return ItemDataManager.GetUniqueMappedItemCodesFromRegion(RegionID, request);
        }

        public IList<DishRecipeMappingData> GetDishRecipeMappingDataList(string DishCode, RequestData request)
        {
            return DishDataManager.GetDishRecipeMappingDataList(DishCode, request);
        }
        public IList<RegionDishRecipeMappingData> GetRegionDishRecipeMappingDataList(int RegionID, string DishCode, string SubSectorCode, RequestData request)
        {
            return DishDataManager.GetRegionDishRecipeMappingDataList(RegionID, DishCode, SubSectorCode, request);
        }
        public IList<SiteDishRecipeMappingData> GetSiteDishRecipeMappingDataList(string SiteCode, string DishCode, RequestData request)
        {
            return DishDataManager.GetSiteDishRecipeMappingDataList(SiteCode, DishCode, request);
        }


        public string SaveDishRecipeMappingList(IList<DishRecipeMappingData> model, RequestData request)
        {
            return DishDataManager.SaveDishRecipeMappingList(model, request);
        }

        public string SaveRegionDishRecipeMappingList(IList<RegionDishRecipeMappingData> model, RequestData request)
        {
            return DishDataManager.SaveRegionDishRecipeMappingList(model, request);
        }
        public string SaveSiteDishRecipeMappingList(IList<SiteDishRecipeMappingData> model, RequestData request)
        {
            return DishDataManager.SaveSiteDishRecipeMappingList(model, request);
        }

        public IList<SiteDishContainerMappingData> GetSiteDishContainerMappingDataList(string SiteCode, RequestData request)
        {
            return MasterDataManager.GetSiteDishContainerMappingDataList(SiteCode, request);
        }
        public IList<SiteDishCategoryContainerMappingData> GetSiteDishCategoryContainerMappingDataList(string SiteCode, RequestData request)
        {
            return MasterDataManager.GetSiteDishCategoryContainerMappingDataList(SiteCode, request);
        }

        public string SaveSiteDishCategoryContainerMappingDataList(IList<SiteDishCategoryContainerMappingData> model, RequestData request)
        {
            return MasterDataManager.SaveSiteDishCategoryContainerMappingDataList(model, request);
        }
        public string SaveSiteDishContainerMappingDataList(IList<SiteDishContainerMappingData> model, RequestData request)
        {
            return MasterDataManager.SaveSiteDishContainerMappingDataList(model, request);
        }
        public string SaveSiteDishContainerMappingData(SiteDishContainerMappingData model, RequestData request)
        {
            return MasterDataManager.SaveSiteDishContainerMappingData(model, request);
        }

        public IList<ModuleData> GetModulesByUserID(int userID, RequestData request)
        {
            return UserManager.GetModulesByUserID(userID, request);
        }
        public IList<UserPermissionData> GetUserPermissionsByModuleCode(int userID, string moduleCode, RequestData request)
        {
            return UserManager.GetUserPermissionsByModuleCode(userID, moduleCode, request);
        }
        public IList<UserMasterData> GetUserMasterDataList(RequestData request)
        {
            return UserManager.GetUserMasterDataList(request);
        }


        public IList<DishRecipeMapData> GetRecipeMappingDataList(string DishCode, RequestData request)
        {
            return CostSimulatorManager.GetRecipeMappingDataList(DishCode, request);
        }
        public IList<SimulatedMappedRecipeMOGListSiteData> GetSimulatedMappedRecipeMOGListSiteDataList(int RecipeID, int SiteId, RequestData request)
        {
            return CostSimulatorManager.GetSimulatedMappedRecipeMOGListSiteDataList(RecipeID, SiteId, request);
        }
        public IList<SimulatedMappedRecipeMOGListSectorData> GetSimulatedMappedRecipeMOGListSectorDataList(int RecipeID, int RegionId, RequestData request)
        {
            return CostSimulatorManager.GetSimulatedMappedRecipeMOGListSectorDataList(RecipeID, RegionId, request);
        }
        public IList<SimulatedMappedRecipeMOGListNationalData> GetSimulatedMappedRecipeMOGListNationalDataList(int RecipeID, RequestData request)
        {
            return CostSimulatorManager.GetSimulatedMappedRecipeMOGListNationalDataList(RecipeID, request);
        }

        public MOGAPLImpactedData GetMOGAPLImpactedDataList(int mogID, string aplList, int sectorID, RequestData request)
        {
            return MOGDataManager.GetMOGAPLImpactedDataList(mogID, aplList, sectorID, request);
        }

        public IList<RegionsDetailsByUserIdData> GetRegionsDetailsByUserIdDataList(RequestData request)
        {
            return MasterDataManager.GetRegionsDetailsByUserIdDataList(request);
        }

        public IList<SiteDetailsByUserIdData> GetSiteDetailsByUserIdDataList(RequestData request)
        {
            return MasterDataManager.GetSiteDetailsByUserIdDataList(request);
        }

        public IList<UserRBFAMatrixData> GetUserRBFAMatrixDataByUserId(RequestData request)
        {
            return MasterDataManager.GetUserRBFAMatrixDataByUserId(request);
        }

        public IList<CuisineMasterData> GetCuisineDataList(RequestData requestData)
        {
            return MasterDataManager.GetCuisineDataList(requestData);
        }

        public string SaveCuisine(RequestData requestData, CuisineMasterData model, int username)
        {
            return MasterDataManager.SaveCuisine(requestData, model, username);
        }

        public string UpdateCuisine(RequestData requestData, CuisineMasterData model, int username)
        {
            return MasterDataManager.UpdateCuisine(requestData, model, username);
        }

        public IList<LifeStyleTagMasterData> GetLifeStyleTagDataList(RequestData requestData)
        {
            return MasterDataManager.GetLifeStyleTagDataList(requestData);
        }

        public string SaveLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username)
        {
            return MasterDataManager.SaveLifeStyleTag(requestData, model, username);
        }

        public string UpdateLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username)
        {
            return MasterDataManager.UpdateLifeStyleTag(requestData, model, username);
        }

        public IList<HealthTagMasterData> GetHealthTagDataList(RequestData requestData)
        {
            return MasterDataManager.GetHealthTagDataList(requestData);
        }

        public string SaveHealthTag(RequestData requestData, HealthTagMasterData model, int username)
        {
            return MasterDataManager.SaveHealthTag(requestData, model, username);
        }

        public string UpdateHealthTag(RequestData requestData, HealthTagMasterData model, int username)
        {
            return MasterDataManager.UpdateHealthTag(requestData, model, username);
        }


        public string mydishcateinactiveupdate(IList<RegionDishCategoryMappingData> model, RequestData request)
        {
            return ItemDataManager.mydishcateinactiveupdate(model, request).ToString();
        }

        public IList<APLMasterData> NationalGetAPLMasterDataList(string articleNumber, string mogCode, RequestData requestData)
        {
            throw new NotImplementedException();
        }

        public IList<APLMasterData> GetNationalAPLExcelList(RequestData requestData)
        {
            throw new NotImplementedException();
        }

        public List<ManageSectorData> GetSectorListInfo(RequestData requestData)
        {
            return UserManager.GetSectorListInfo(requestData);
        }

        public List<LevelMasterData> GetLevelMasterData(RequestData requestData)
        {
            return UserManager.GetLevelMasterData(requestData);
        }

        public List<FunctionMasterData> GetFunctionMasterData(RequestData requestData)
        {
            return UserManager.GetFunctionMasterData(requestData);
        }

        public List<PermissionMasterData> GetPermissionMasterData(RequestData requestData)
        {
            return UserManager.GetPermissionMasterData(requestData);
        }

        public string SaveCapringInfo(RequestData request, CapringMastre model, int usercode)
        {
            return UserManager.SaveCapringInfo(request, model, usercode);
        }

        public string UpdateCapringInfo(RequestData request, CapringMastre model, int usercode)
        {
            return UserManager.UpdateCapringInfo(request, model, usercode);
        }

        public string UpdateCapringDescriptionInfo(RequestData request, CapringMastre model, int usercode)
        {
            return UserManager.UpdateCapringDescriptionInfo(request, model, usercode);
        }

        public string UpdateCapringInactiveActive(RequestData request, CapringMastre model, int usercode)
        {
            return UserManager.UpdateCapringInactiveActive(request, model, usercode);
        }

        public List<CapringMastre> GetCapringMasterList(RequestData requestData)
        {
            return UserManager.GetCapringMasterList(requestData);
        }

        public IList<ItemExportData> GetItemExportDataList(RequestData requestData)
        {
            return ItemDataManager.GetItemExportDataList(requestData);
        }

        public IList<ItemDishCategoryMappingData> GetNationalItemDishCategoryMappingDataList(string itemCode, string sectorcode, RequestData requestData)
        {
            return ItemDataManager.GetItemDishCategoryMappingDataList(itemCode, requestData);
        }

        public string SaveSiteRecipeAllergenData(string recipeCode, string siteCode, RequestData requestData)
        {
            throw new NotImplementedException();
        }

        public IList<UOMModuleData> GetUOMModuleDataList(RequestData requestData)
        {
            return MasterDataManager.GetUOMModuleDataList(requestData);
        }

        public IList<InheritanceChangesData> GetInheritChangesSectorToRegion(ItemInheritChangeRequestData itemInheritChangeRequestData, RequestData requestData)
        {
            return ItemDataManager.GetInheritChangesSectorToRegion(itemInheritChangeRequestData, requestData);
        }

        public IList<InheritanceChangesData> GetInheritChangesRegionToSite(ItemInheritChangeRequestData itemInheritChangeRequestData, RequestData requestData)
        {
            return ItemDataManager.GetInheritChangesRegionToSite(itemInheritChangeRequestData, requestData);
        }

        public string SaveChangesSectorToRegion(List<InheritanceChangesData> model, RequestData requestData)
        {
            return ItemDataManager.SaveChangesSectorToRegion(model, requestData);
        }

        public string SaveChangesRegionToSite(List<InheritanceChangesData> model, RequestData requestData)
        {
            return ItemDataManager.SaveChangesRegionToSite(model, requestData);
        }

        public IList<ExpiryCategoryMasterData> GetExpiryCategoryDataList(RequestData requestData)
        {
            return MasterDataManager.GetExpiryCategoryDataList(requestData);
        }

        public IList<ShelfLifeUOMMasterData> GetShelfLifeUOMDataList(RequestData requestData)
        {
            return MasterDataManager.GetShelfLifeUOMDataList(requestData);
        }

        public string SaveExpiryCategory(RequestData requestData, ExpiryCategoryMasterData model)
        {
            return MasterDataManager.SaveExpiryCategory(requestData, model);
        }

        public IList<GetExpiryProductData> GetExpiryProductDataList(RequestData requestData)
        {
            return MOGDataManager.GetExpiryProductDataList(requestData);
        }

        public string SaveExpiryProductData(GetExpiryProductData model, RequestData requestData)
        {
            return MOGDataManager.SaveExpiryProductData(model, requestData);
        }

        public string SaveNutritionMaster(RequestData requestData, NutritionElementMasterData model)
        {
            return MasterDataManager.SaveNutritionMaster(requestData, model);
        }

        public IList<NutritionElementMasterData> GetNutritionMasterDataList(RequestData requestData)
        {
            return MasterDataManager.GetNutritionMasterDataList(requestData);
        }

        public IList<NutritionElementTypeData> GetNutritionElementTypeDataList(RequestData requestData)
        {
            return MasterDataManager.GetNutritionElementTypeDataList(requestData);
        }

        public string SaveRecipeNutrientMappingData(RecipeData model, RequestData requestData)
        {
            return RecipeDataManager.SaveRecipeNutrientMappingData(model, requestData);
        }

        public IList<DietTypeData> GetDietTypeDataList(RequestData requestData)
        {
            return MasterDataManager.GetDietTypeDataList(requestData);
        }

        public IList<TextureData> GetTextureDataList(RequestData requestData)
        {
            return MasterDataManager.GetTextureDataList(requestData);
        }

        public string SaveCommonMasterData(RequestData requestData, CommonMasterData model)
        {
            return MasterDataManager.SaveCommonMaster(requestData, model);
        }

        public IList<CommonMasterData> GetCommonMasterDataList(RequestData requestData, CommonMasterData model)
        {
            return MasterDataManager.GetCommonMasterDataList(requestData, model);
        }

        //IList<ItemData> ICookBookService.NationalGetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, string sectorName, RequestData requestData)
        //{
        //    throw new NotImplementedException();
        //}

        //Krish
        //public IList<SimulationList> GetSimulationDataListWithPaging(RequestData request, int pageindex, int totalRecordsPerPage)
        //{
        //    return FoodCostSimulatorManager.GetSimulationDataListWithPaging(request, pageindex,totalRecordsPerPage);
        //}
        public List<FoodCostSimulationData> GetSimulationDataListWithPaging(RequestData requestData, UserMasterResponseData userData)
        {
            return FoodCostSimulatorManager.GetSimulationDataListWithPaging(requestData, userData);
        }
        public IList<SimulationItemsData> GetItemsForSimulation(RequestData requestData, int siteId, int regionId, List<int> daypartId, bool checksiteItem)
        {
            return FoodCostSimulatorManager.GetItemsForSimulation(requestData, siteId, regionId, daypartId,checksiteItem);
        }
        public IList<DayPartData> GetDayPartBySiteList(RequestData requestData, int siteId)
        {
            return FoodCostSimulatorManager.GetDayPartBySiteList(requestData, siteId);
        }
        public IList<SimulationRegionItemDishCategoryData> GetSimulationRegionItemDishCategories(RequestData requestData, int siteId, int regionId, int itemId)
        {
            return FoodCostSimulatorManager.GetSimulationRegionItemDishCategories(requestData, siteId, regionId, itemId);
        }
        public string SaveSimulationBoard(SimulationBoardData model, RequestData requestData, FoodCostSimulationData simStatusUpdate)
        {
            return FoodCostSimulatorManager.SaveSimulationBoard(model, requestData, simStatusUpdate);
        }
        public SimulationBoardData GetSimulationBoard(int simId, RequestData requestData)
        {
            return FoodCostSimulatorManager.GetSimulationBoard(simId, requestData);
        }
        public IList<SiteRecipeData> UpdateAndFetchSiteDishRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode)
        { return FoodCostSimulatorManager.UpdateAndFetchSiteDishRecipeData(requestData, simCode, SiteCode, DishCode); }
        public IList<SiteRecipeData> UpdateAndFetchSiteRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode)
        { return FoodCostSimulatorManager.UpdateAndFetchSiteRecipeData(requestData, simCode, SiteCode,DishCode); }
        public string SaveFoodCostSimSiteRecipeData(RecipeData modelr, SiteRecipeData model, IList<SiteRecipeMOGMappingData> baseRecipes, IList<SiteRecipeMOGMappingData> mogs, string simCode, RequestData requestData)
        {
            return FoodCostSimulatorManager.SaveFoodCostSimSiteRecipeData(modelr, model, baseRecipes, mogs, simCode, requestData);
        }
        public IList<RecipeMOGMappingData> GetFCSSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode, string simCode, RequestData requestData)
        { return FoodCostSimulatorManager.GetSiteMOGsByRecipeID(SiteCode, recipeID, recipeCode, simCode, requestData); }
        public string SaveFCSAPLHistory(FoodCostSimAPLHistoryData entity, RequestData requestData)
        {
            return FoodCostSimulatorManager.SaveFCSAPLHistory(entity, requestData);
        }
        public IList<APLMasterData> GetFCSAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, string simCode, RequestData request)
        {
            return FoodCostSimulatorManager.GetFCSAPLMasterSiteDataList(articleNumber, mogCode, SiteCode, simCode, request);
        }
        public IList<DishData> GetFcsDishDataList(RequestData requestData, string siteCode,int itemId)
        {
            return FoodCostSimulatorManager.GetDishDataList(requestData, siteCode, itemId);
        }


        public IList<MOGUOMData> GetMOGUOMDataList(RequestData requestData)
        {
            return MasterDataManager.GetMOGUOMDataList(requestData);
        }

        public IList<ItemDishRecipeNutritionData> GetItemDishRecipeNutritionData(string SiteCode, RequestData requestData)
        {
            return RecipeDataManager.GetItemDishRecipeNutritionData(SiteCode, requestData);
        }
        //Krish
        public RecipePrintingData GetRecipePrintingData(string recipeCode, string siteCode, int regId, RequestData requestData)
        {
            return RecipeDataManager.GetRecipePrintingData(recipeCode, siteCode, regId, requestData);
        }

        public bool SendGOVOOffSiteItemAutoInheritEmail(ItemData model, RequestData requestData)
        {
          //  EmailManager.SendGOVOOffSiteItemAutoInheritEmail(model, requestData);
            return true;
        }

        public IList<DishCategorySiblingData> GetDishCategorySiblingDataList(RequestData requestData)
        {
            return MasterDataManager.GetDishCategorySiblingDataList(requestData);
        }
        //Krish
        public string SaveRecipeCopy(List<RecipeCopyData> model, List<string> recipes, string sourceSite, RequestData requestData)
        {
            return RecipeDataManager.SaveRecipeCopy(model, recipes, sourceSite, requestData);
        }
        public IList<GetExpiryProductData> GetExpiryProductDataOnlyExpiryNameList(RequestData requestData)
        {
            return MOGDataManager.GetExpiryProductDataOnlyExpiryNameList(requestData);
        }
        public IList<UserRoleData> GetUserRoleDataByUserID(int userID, RequestData request)
        {
            return UserManager.GetUserRoleDataByUserID(userID, request);
        }
        //Krish
        public IList<NutritionDishRecipeMogMapping> GetRecipeDishDataByDish(string dishCode, RequestData request)
        {
            return RecipeDataManager.GetRecipeDishDataByDish(dishCode, request);
        }

        public string SavePlanningTagData(PlanningTagData model, RequestData requestData)
        {
            return DishDataManager.SavePlanningTagData(model, requestData);
        }

        public IList<PlanningTagData> GetPlanningTag(RequestData requestData)
        {
            return DishDataManager.GetPlanningTag(requestData);
        }
        //Krish
        public List<PatientMasterData> GetPatientMaster(RequestData requestData)
        {
            return PatientMasterDataManager.GetPatientMaster(requestData);
        }

        public List<PatientMasterHISTORY> GetPatientMasterHISTORY(RequestData requestData, string username)
        {
            return PatientMasterDataManager.GetPatientMasterHISTORY(requestData,username);
        }
        public List<PatientMasterHISTORY> GetPatientMasterHISTORYDet(RequestData requestData, string username)
        {
            return PatientMasterDataManager.GetPatientMasterHISTORYDet(requestData,username);
        }
        public List<PatientMasterHistoryBatchWise> GetshowGetALLPatientMasterHISTORYBATCHWISE(RequestData requestData, string batchno)
        {
            return PatientMasterDataManager.GetshowGetALLPatientMasterHISTORYBATCHWISE(requestData,  batchno);
        }
        public string AddUpdatePatientMasterList(RequestData request, string xml, string username)
        {
            return PatientMasterDataManager.AddUpdatePatientMasterList(request, xml, username).ToString();
        }
        public string SavePatientMasterStatus(RequestData request, int id, int username)
        {
            return PatientMasterDataManager.SavePatientMasterStatus(request, id, username).ToString();
        }
        public List<PatientMasterHistoryAllData> GetALLHISTORYDetailsPatientMaster(RequestData request)
        {
            return PatientMasterDataManager.GetALLHISTORYDetailsPatientMaster(request);
        }
        //Bed
        public List<BedMasterData> GetBedMaster(RequestData requestData)
        {
            return BedMasterDataManager.GetBedMaster(requestData);
        }

        public List<BedMasterHISTORY> GetBedMasterHISTORY(RequestData requestData, string username)
        {
            return BedMasterDataManager.GetBedMasterHISTORY(requestData, username);
        }
        public List<BedMasterHISTORY> GetBedMasterHISTORYDet(RequestData requestData, string username)
        {
            return BedMasterDataManager.GetBedMasterHISTORYDet(requestData, username);
        }
        public List<BedMasterHistoryBatchWise> GetshowGetALLBedMasterHISTORYBATCHWISE(RequestData requestData,  string batchno)
        {
            return BedMasterDataManager.GetshowGetALLBedMasterHISTORYBATCHWISE(requestData,  batchno);
        }
        public string AddUpdateBedMasterList(RequestData request, string xml, string username)
        {
            return BedMasterDataManager.AddUpdateBedMasterList(request, xml, username).ToString();
        }
        public string SaveBedMasterStatus(RequestData request, int id, int username)
        {
            return BedMasterDataManager.SaveBedMasterStatus(request, id, username).ToString();
        }
        public List<BedMasterHistoryAllData> GetALLHISTORYDetailsBedMaster(RequestData request)
        {
            return BedMasterDataManager.GetALLHISTORYDetailsBedMaster(request);
        }
        //Dietician
        public List<DieticianMasterData> GetDieticianMaster(RequestData requestData)
        {
            return DieticianMasterDataManager.GetDieticianMaster(requestData);
        }

        public List<DieticianMasterHISTORY> GetDieticianMasterHISTORY(RequestData requestData, string username)
        {
            return DieticianMasterDataManager.GetDieticianMasterHISTORY(requestData, username);
        }
        public List<DieticianMasterHISTORY> GetDieticianMasterHISTORYDet(RequestData requestData, string username)
        {
            return DieticianMasterDataManager.GetDieticianMasterHISTORYDet(requestData, username);
        }
        public List<DieticianMasterHistoryBatchWise> GetshowGetALLDieticianMasterHISTORYBATCHWISE(RequestData requestData,  string batchno)
        {
            return DieticianMasterDataManager.GetshowGetALLDieticianMasterHISTORYBATCHWISE(requestData,  batchno);
        }
        public string AddUpdateDieticianMasterList(RequestData request, string xml, string username)
        {
            return DieticianMasterDataManager.AddUpdateDieticianMasterList(request, xml, username).ToString();
        }
        public string SaveDieticianMasterStatus(RequestData request, int id, int username)
        {
            return DieticianMasterDataManager.SaveDieticianMasterStatus(request, id, username).ToString();
        }
        public List<DieticianMasterHistoryAllData> GetALLHISTORYDetailsDieticianMaster(RequestData request)
        {
            return DieticianMasterDataManager.GetALLHISTORYDetailsDieticianMaster(request);
        }

        public SiteItemDropDownData GetSiteItemDropDownDataData(RequestData requestData)
        {
            return ItemDataManager.GetSiteItemDropDownDataData(requestData);
        }
        //Krish 28-07-2022
        public SectorItemData GetSectorItemData(RequestData requestData, bool isAddEdit, bool onLoad)
        {
            return ItemDataManager.GetSectorItemData(requestData,isAddEdit,onLoad);
        }
        //Krish 30-07-2022
        public RegionItemData GetRegionItemData(RequestData requestData, bool isSectorUser, int regionId = 0, bool gridOnly = false)
        {
            return ItemDataManager.GetRegionItemData(requestData, isSectorUser,regionId,gridOnly);
        }
        //Krish 02-08-2022
        public SectorDishData GetSectorDishData(RequestData requestData, string condition, bool isAddEdit, bool onLoad)
        {
            return DishDataManager.GetSectorDishData(requestData,condition, isAddEdit, onLoad);
        }
        public SectorRecipeData GetSectorRecipeData(RequestData requestData, bool isAddEdit, bool onLoad)
        {
            return RecipeDataManager.GetSectorRecipeData(requestData, isAddEdit,onLoad);
        }
        //Krish
        public IList<RangeTypeMasterData> GetRangeTypeMasterData(RequestData requestData)
        {
            return MasterDataManager.GetRangeTypeMasterDataList(requestData);
        }
        public string SaveRangeTypeMasterData(RequestData requestData, RangeTypeMasterData model)
        {
            return MasterDataManager.SaveRangeTypeMaster(requestData, model);
        }
        public string SaveRangeTypeDishData(string dishCode, string rangeTypeCode, RequestData requestData)
        {
            return DishDataManager.SaveRangeTypeDishData(dishCode, rangeTypeCode, requestData);
        }
        public string SaveRangeTypeAPLData(string articleNumber, string rangeTypeCode, RequestData requestData)
        {
            return MasterDataManager.SaveRangeTypeAPLData(articleNumber, rangeTypeCode, requestData);
        }
        //Krish 13-10-2022
        public IList<ProcessTypeMasterData> GetProcessTypeMasterData(RequestData requestData)
        {
            return MasterDataManager.GetProcessTypeMasterDataList(requestData);
        }
        public string SaveProcessTypeMasterData(RequestData requestData, ProcessTypeMasterData model)
        {
            return MasterDataManager.SaveProcessTypeMaster(requestData, model);
        }
        public string SaveProcessTypeMogData(RequestData requestData, List<string> processTypes, string mogCode, string rangeTypeCode, int inputStdDuration)
        {
            return MasterDataManager.SaveProcessTypeMogData(requestData, processTypes, mogCode, rangeTypeCode, inputStdDuration);
        }
        public IList<MogProcessTypeMappingData> GetProcessTypeMogData(RequestData requestData, string mogCode)
        {
            return MasterDataManager.GetProcessTypeMogData(requestData, mogCode);
        }
        public IList<MogProcessTypeMappingData> GetProcessMappingDataList(RequestData requestData)
        {
            return MasterDataManager.GetProcessMappingDataList(requestData);
        }

        public string SaveHISPatientProfileMaster(RequestData request, List<HISPatientProfileData> hisPatientProfileData)
        {
            return DieticianMasterDataManager.SaveHISPatientProfileMaster(request, hisPatientProfileData).ToString();
        }

        public string SaveHISUserData(RequestData request, List<HISUserData> hisUserData)
        {
            return DieticianMasterDataManager.SaveHISUserData(request, hisUserData).ToString();
        }

        public string SaveHISBedMasterData(RequestData request, List<HISBedMasterData> hisBedMasterData)
        {
            return DieticianMasterDataManager.SaveHISBedMasterData(request, hisBedMasterData).ToString();
        }
    }
}