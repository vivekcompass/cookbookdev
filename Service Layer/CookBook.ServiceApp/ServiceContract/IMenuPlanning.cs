﻿using KitchenPlanning.Data;
using KitchenPlanning.Data.Common;
//using KitchenPlanning.Data.SiteSelectData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace KitchenPlanning.ServiceApp
{
    [ServiceContract]
    public interface IMenuPlanning
    {
        /// <summary>
        ///Test service
        /// </summary>
        /// <param name="parameter1">parameter1</param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetMenuPlanning")]
        IList<MenuPlanningData> GetMenuPlanning(MenuPlanningFilter filterData);

        /// <summary>
        ///Test service
        /// </summary>
        /// <param name="parameter1">parameter1</param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllSiteMasterDataByUserId")]
        IList<SiteMasterData> GetAllSiteMasterDataByUserId(int userId);

        /// <summary>
        ///Test service
        /// </summary>
        /// <param name="parameter1">parameter1</param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDishMasterData")]
        IList<SiteMasterData> GetDishMasterData();
    }
}