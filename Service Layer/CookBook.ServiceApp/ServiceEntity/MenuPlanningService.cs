﻿using KitchenPlanning.Data.Common;
using KitchenPlanning.Data;
using KitchenPlanning.ServiceApp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using KitchenPlanning.Business.Contract;
using KitchenPlanning.Data.MasterData;
//using KitchenPlanning.Data.SiteSelectData;

namespace KitchenPlanning.ServiceApp
{
    public partial class KitchenPlanningService : ServiceBase, IKitchenPlanningService
    {
        [Dependency]
        public IMenuPlanningManager MenuPlanningManager { get; set; }

        [Dependency]
        public IMasterDataManager MasterDataManager { get; set; }

        public IList<MenuPlanningData> GetMenuPlanning(MenuPlanningFilter filterData)
        {
            IList<MenuPlanningData> result = new List<MenuPlanningData>();
            result = MenuPlanningManager.GetMenuPlanning(filterData);
            return result;
        }

        public IList<SiteMasterData> GetAllSiteMasterDataByUserId(int userId)
        {
            return MasterDataManager.GetAllSiteMasterDataByUserId(userId);
        }

        public IList<DishMasterData> GetDishMasterData()
        {
            return MasterDataManager.GetDishMasterData();
        }
    }
}