﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CookBook.Ioc;
using CookBook.Aspects.Factory;

namespace CookBook.ServiceApp
{
    public class ServiceBootstrapper
    {
        public static IIocManager IocEngine { get; private set; }
        public static void Initialize()
        {
            IocEngine = new IocManager(DiscoveryStrategy.SearchBaseDirectory);
            LogTraceFactory.InitializeLoggingService();
            ExceptionFactory.InitializeExceptionAopFramework();
        }
    }
}