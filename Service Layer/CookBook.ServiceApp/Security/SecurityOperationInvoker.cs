﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Web;
using CookBook.Aspects.Constants;
using CookBook.Aspects.Utils;

namespace CookBook.ServiceApp.Security
{
    /// <summary>
    /// Class to invoke service methods if provided token values matches
    /// </summary>
    public class SecurityOperationInvoker : IOperationInvoker
    {
        #region Private Variables

        private IOperationInvoker InnerOperationInvoker { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor to initialize class variables
        /// </summary>
        /// <param name="operationInvoker"></param>
        public SecurityOperationInvoker(IOperationInvoker operationInvoker)
        {
            this.InnerOperationInvoker = operationInvoker;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Method to allocate input for the Invokable operation context requests
        /// </summary>
        /// <returns></returns>
        public Object[] AllocateInputs()
        {
            return InnerOperationInvoker.AllocateInputs();
        }

        /// <summary>
        /// Method to invoke Service Request
        /// </summary>
        /// <param name="instance">instance of the operation context request</param>
        /// <param name="inputs">input patameters array</param>
        /// <param name="outputs">output parameter arrays</param>
        /// <returns>returns execution context</returns>
        public Object Invoke(Object instance, Object[] inputs, out Object[] outputs)
        {
            //+ authorization
            outputs = null;
            var props = OperationContext.Current.IncomingMessageProperties;
            
            var request = props[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
            string sessionTokenKey = XmlTextSerializer.GetAppSettings(ConfigKeys.LoginSessionHeaderKey);
            string sessionTokenValue = request.Headers[sessionTokenKey];
            string userAgentTokenKey = XmlTextSerializer.GetAppSettings(ConfigKeys.ServiceHeaderUsername);
            string userID = request.Headers[userAgentTokenKey];
            string serviceUsageFlag = XmlTextSerializer.GetAppSettings(ConfigKeys.IsServiceUsageLogEnabled);
            if (serviceUsageFlag == "1")
            {
                //Not required yet
                //RemoteEndpointMessageProperty endpoint = props[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                //Add service usage details into log/trace files
            }
            if (!String.IsNullOrEmpty(sessionTokenValue) && !String.IsNullOrEmpty(userID))
            {
                // Call method to get the Login Session Token authorization from database or other source.
                bool isValid = true;//as per the logic implemented
                if (isValid)
                {
                    //Also we can develop custom logic for more validations and authorization
                    return InnerOperationInvoker.Invoke(instance, inputs, out outputs);
                }
                else
                {
                    // Returns exception for missing API Credentials.
                    throw new System.Security.SecurityException("ApiAccessDenied");
                }
            }
            // Returns exception for missing API Credentials.
            throw new System.Security.SecurityException("CredentialsNotFound");
        }

        /// <summary>
        /// Method which fires at the time of Request invoke action begins
        /// </summary>
        /// <param name="instance">operation context instance</param>
        /// <param name="inputs">input parameter array</param>
        /// <param name="callback">call back method</param>
        /// <param name="state">request state</param>
        /// <returns>returns result status</returns>
        public IAsyncResult InvokeBegin(Object instance, Object[] inputs, AsyncCallback callback, Object state)
        {
            return InnerOperationInvoker.InvokeBegin(instance, inputs, callback, state);
        }

        /// <summary>
        /// Method which fires at the time of Request invoke action ends
        /// </summary>
        /// <param name="instance">operation context instance</param>
        /// <param name="outputs">output parameter array</param>
        /// <param name="result">result obtained</param>
        /// <returns>returns object</returns>
        public Object InvokeEnd(Object instance, out Object[] outputs, IAsyncResult result)
        {
            return InnerOperationInvoker.InvokeEnd(instance, out outputs, result);
        }

        /// <summary>
        /// Property to get Synchronous state boolean response
        /// </summary>
        public Boolean IsSynchronous
        {
            get { return InnerOperationInvoker.IsSynchronous; }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Method to remove unwanted slashes from string input
        /// </summary>
        /// <param name="inputText">input text</param>
        /// <returns>returns formatted string</returns>
        private string RemoveUnwantedCharacters(string inputText)
        {
            return inputText.Replace("\"", string.Empty);
        }

        #endregion
    }
}