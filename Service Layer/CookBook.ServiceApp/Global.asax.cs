﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace CookBook.ServiceApp
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            ServiceBootstrapper.Initialize();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            //Session Not required yet
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //Not required yet
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            //Not required yet
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //Not required yet
        }

        protected void Session_End(object sender, EventArgs e)
        {
            //Not required yet
        }

        protected void Application_End(object sender, EventArgs e)
        {
            //Not required yet
        }
    }
}