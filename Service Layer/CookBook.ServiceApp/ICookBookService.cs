﻿using CookBook.Data.Request;
using CookBook.Data.UserData;
using CookBook.Data.MasterData;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;
using System;

namespace CookBook.ServiceApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change 
    //the interface name "ICourseService" in both code and config file together.
    [ServiceContract]
    public interface ICookBookService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetUserData")]
        UserMasterResponseData GetUserData(string userName, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetMOGAPLHISTORY")]
        List<MOGAPLMAPPEDHISTORY> GetMOGAPLHISTORY(RequestData requestData, string username);




        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetMOGAPLHISTORYAPL")]
        List<APLMAPPEDHISTORYDLIST> GetMOGAPLHISTORYAPL(RequestData requestData, string username);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetALLHISTORYDetailsAPL")]
        List<APLMAPPEDHISTORYDLIST> GetALLHISTORYDetailsAPL(RequestData requestData, string username);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetshowGetALLHISTORYBATCHWISEAPL")]
        List<APLMAPPEDHISTORYDLIST> GetshowGetALLHISTORYBATCHWISEAPL(RequestData requestData, string username, string batchno);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetALLHISTORYDetails")]
        List<MOGAPLMAPPEDHISTORY> GetALLHISTORYDetails(RequestData requestData, string username);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetMOGAPLHISTORYDet")]
        List<MOGAPLMAPPEDHISTORY> GetMOGAPLHISTORYDet(RequestData requestData, string username);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetMOGAPLHISTORYDetAPL")]
        List<APLMAPPEDHISTORYDLIST> GetMOGAPLHISTORYDetAPL(RequestData requestData, string username);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetshowGetALLHISTORYBATCHWISE")]
        List<MOGAPLMAPPEDHISTORY> GetshowGetALLHISTORYBATCHWISE(RequestData requestData, string username, string batchno);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetModulesByUserID")]
        IList<ModuleData> GetModulesByUserID(int userID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetUserPermissionsByModuleCode")]
        IList<UserPermissionData> GetUserPermissionsByModuleCode(int userID, string moduleCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetUserMasterDataList")]
        IList<UserMasterData> GetUserMasterDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteTypeDataList")]
        IList<SiteTypeData> GetSiteTypeDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetAllergenDataList")]
        IList<AllergenData> GetAllergenDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetAPLMasterDataList")]
        IList<APLMasterData> GetAPLMasterDataList(string articleNumber, string mogCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetAPLMasterDataListForSector")]
        IList<APLMasterData> GetAPLMasterDataListForSector(string articleNumber, string mogCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/NationalGetAPLMasterDataList")]
        IList<APLMasterData> NationalGetAPLMasterDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetAPLMasterDataListPageing")]
        IList<APLMasterData> GetAPLMasterDataListPageing(RequestData requestData, int pageindex);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetNationalAPLExcelList")]
        IList<APLMasterData> GetNationalAPLExcelList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetAPLMasterDataListCommon")]
        IList<APLMasterData> GetAPLMasterDataListCommon(string articleNumber, string mogCode, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetAPLMasterSiteDataList")]
        IList<APLMasterData> GetAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetAPLMasterRegionDataList")]
        IList<APLMasterData> GetAPLMasterRegionDataList(string articleNumber, string mogCode, int Region_ID, RequestData requestData);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteDishContainerMappingDataList")]
        IList<SiteDishContainerMappingData> GetSiteDishContainerMappingDataList(string SiteCode, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteDishCategoryContainerMappingDataList")]
        IList<SiteDishCategoryContainerMappingData> GetSiteDishCategoryContainerMappingDataList(string SiteCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetConceptType1DataList")]
        IList<ConceptType1Data> GetConceptType1DataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetConceptType2DataList")]
        IList<ConceptType2Data> GetConceptType2DataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetConceptType3DataList")]
        IList<ConceptType3Data> GetConceptType3DataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetConceptType4DataList")]
        IList<ConceptType4Data> GetConceptType4DataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetConceptType5DataList")]
        IList<ConceptType5Data> GetConceptType5DataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDayPartDataList")]
        IList<DayPartData> GetDayPartDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetCuisineDataList")]
        IList<CuisineMasterData> GetCuisineDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveCuisine")]
        string SaveCuisine(RequestData requestData, CuisineMasterData model, int username);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateCuisine")]
        string UpdateCuisine(RequestData requestData, CuisineMasterData model, int username);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetHealthTagDataList")]
        IList<HealthTagMasterData> GetHealthTagDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveHealthTag")]
        string SaveHealthTag(RequestData requestData, HealthTagMasterData model, int username);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateHealthTag")]
        string UpdateHealthTag(RequestData requestData, HealthTagMasterData model, int username);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetLifeStyleTagDataList")]
        IList<LifeStyleTagMasterData> GetLifeStyleTagDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveLifeStyleTag")]
        string SaveLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateLifeStyleTag")]
        string UpdateLifeStyleTag(RequestData requestData, LifeStyleTagMasterData model, int username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRevenueTypeDataList")]
        IList<RevenueTypeData> GetRevenueTypeDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDietCategoryDataList")]
        IList<DietCategoryData> GetDietCategoryDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetReasonDataList")]
        IList<ReasonData> GetReasonDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSubSectorDataList")]
        IList<SubSectorData> GetSubSectorDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDishCategoryDataList")]
        IList<DishCategoryData> GetDishCategoryDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDishCategorySiblingDataList")]
        IList<DishCategorySiblingData> GetDishCategorySiblingDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/NationalGetDishCategoryDataList")]
        IList<NationalDishCategoryData> NationalGetDishCategoryDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/NationalGetDishDataList")]
        IList<NationalDishData> NationalGetDishDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDishTypeDataList")]
        IList<DishTypeData> GetDishTypeDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDishSubCategoryDataList")]
        IList<DishSubCategoryData> GetDishSubCategoryDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetFoodProgramDataList")]
        IList<FoodProgramData> GetFoodProgramDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetContainerDataList")]
        IList<ContainerData> GetContainerDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetItemType1DataList")]
        IList<ItemType1Data> GetItemType1DataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetItemType2DataList")]
        IList<ItemType2Data> GetItemType2DataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetMOGDataList")]
        IList<MOGData> GetMOGDataList(RequestData requestData, string siteCode, int regionID);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetInheritedMOGDataList")]
        IList<MOGInheritedData> GetInheritedMOGDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetNationalMOGDataList")]
        IList<NationalMOGData> GetNationalMOGDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSectorDataList")]
        List<ManageSectorData> GetSectorDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSectorListInfo")]
        List<ManageSectorData> GetSectorListInfo(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetLevelMasterData")]
        List<LevelMasterData> GetLevelMasterData(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetFunctionMasterData")]
        List<FunctionMasterData> GetFunctionMasterData(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetPermissionMasterData")]
        List<PermissionMasterData> GetPermissionMasterData(RequestData requestData);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveManageSectorInfo")]
        string SaveManageSectorInfo(RequestData request, ManageSectorData model, int usercode);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveManageLevelInfo")]
        string SaveManageLevelInfo(RequestData request, LevelMasterData model, int usercode);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveFunctionLevelInfo")]
        string SaveFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SavePermissionInfo")]
        string SavePermissionInfo(RequestData request, PermissionMasterData model, int usercode);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveCapringInfo")]
        string SaveCapringInfo(RequestData request, CapringMastre model, int usercode);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateCapringInfo")]
        string UpdateCapringInfo(RequestData request, CapringMastre model, int usercode);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateCapringDescriptionInfo")]
        string UpdateCapringDescriptionInfo(RequestData request, CapringMastre model, int usercode);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdatePermissionInfo")]
        string UpdatePermissionInfo(RequestData request, PermissionMasterData model, int usercode);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateFunctionLevelInfo")]
        string UpdateFunctionLevelInfo(RequestData request, FunctionMasterData model, int usercode);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdatefunctionInactiveActive")]
        string UpdatefunctionInactiveActive(RequestData request, FunctionMasterData model, int usercode);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdatePermissionInactiveActive")]
        string UpdatePermissionInactiveActive(RequestData request, PermissionMasterData model, int usercode);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateManageLevelInfo")]
        string UpdateManageLevelInfo(RequestData request, LevelMasterData model, int usercode);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateManageSectorInfo")]
        string UpdateManageSectorInfo(RequestData request, ManageSectorData model, int usercode);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateInactiveActive")]
        string UpdateInactiveActive(RequestData request, ManageSectorData model, int usercode);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateLevelInactiveActive")]
        string UpdateLevelInactiveActive(RequestData request, LevelMasterData model, int usercode);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateCapringInactiveActive")]
        string UpdateCapringInactiveActive(RequestData request, CapringMastre model, int usercode);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetLevelDataList")]
        List<LevelMasterData> GetLevelDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetCapringMasterList")]
        List<CapringMastre> GetCapringMasterList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetFunctionDataList")]
        List<FunctionMasterData> GetFunctionDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetPermissionDataList")]
        List<PermissionMasterData> GetPermissionDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetNOTMappedMOGDataList")]
        IList<MOGNotMapped> GetNOTMappedMOGDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetNOTMappedMOGDataListAPL")]
        IList<APLMOGDATA> GetNOTMappedMOGDataListAPL(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateMOGLIST")]
        string UpdateMOGLIST(RequestData requestData, string xml, string username);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateMOGLISTAPL")]
        string UpdateMOGLISTAPL(RequestData requestData, string xml, string username);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetMOGImpactedDataList")]
        MOGImpactedData GetMOGImpactedDataList(int mogID, int recipeID, bool isBaseRecipe, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetMOGAPLImpactedDataList")]
        MOGAPLImpactedData GetMOGAPLImpactedDataList(int mogID, string aplList, int sectorID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetServewareDataList")]
        IList<ServewareData> GetServewareDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetServingTemperatureDataList")]
        IList<ServingTemperatureData> GetServingTemperatureDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteProfile1DataList")]
        IList<SiteProfile1Data> GetSiteProfile1DataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteProfile2DataList")]
        IList<SiteProfile2Data> GetSiteProfile2DataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteProfile3DataList")]
        IList<SiteProfile3Data> GetSiteProfile3DataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetUOMDataList")]
        IList<UOMData> GetUOMDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetUOMModuleMappingDataList")]
        IList<UOMModuleMappingData> GetUOMModuleMappingDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/procGetUOMModuleMappingGridData")]
        IList<GetUOMModuleMappingGridData> procGetUOMModuleMappingGridData(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetUOMModuleDataList")]
        IList<UOMModuleData> GetUOMModuleDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetReasonTypeDataList")]
        IList<ReasonTypeData> GetReasonTypeDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetVisualCategoryDataList")]
        IList<VisualCategoryData> GetVisualCategoryDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetColorDataList")]
        IList<ColorData> GetColorDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteMasterDataList")]
        IList<SiteMasterData> GetSiteMasterDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/NationalGetSiteMasterDataList")]
        IList<SiteMasterData> NationalGetSiteMasterDataList(RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionMasterDataList")]
        IList<RegionMasterData> GetRegionMasterDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetCafeDataList")]
        IList<CafeData> GetCafeDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetCapringMasterDataList")]
        IList<CapringMasterData> GetCapringMasterDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetContainerTypeDataList")]
        IList<ContainerTypeData> GetContainerTypeDataList(RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetApplicationSettingDataList")]
        IList<ApplicationSettingData> GetApplicationSettingDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetCPUDataList")]
        IList<CPUData> GetCPUDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDKDataList")]
        IList<DKData> GetDKDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetItemDataList")]
        IList<ItemData> GetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, string sectorName, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetItemExportDataList")]
        IList<ItemExportData> GetItemExportDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/NationalGetItemDataList")]
        IList<NationalItemData> NationalGetItemDataList(string itemCode, int? foodProgramID, int? dietCategoryID, string sectorName, RequestData requestData);




        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDishImpactedItemList")]
        IList<ItemData> GetDishImpactedItemList(string dishCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteUnitPrice")]
        IList<UnitPriceData> GetSiteUnitPrice(string siteCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteDayPartMapping")]
        IList<SiteDayPartMappingData> GetSiteDayPartMapping(string siteCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteItemPriceHistory")]
        IList<PriceHistoryData> GetSiteItemPriceHistory(string siteCode, string itemCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionInheritedData")]
        IList<RegionInheritedData> GetRegionInheritedData(string RegionID, string SectorID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDishDataList")]
        IList<DishData> GetDishDataList(int? foodProgramID, int? dietCategoryID, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteDishDataList")]
        IList<DishData> GetSiteDishDataList(string siteCode, int? foodProgramID, int? dietCategoryID, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionDishMasterData")]
        IList<DishData> GetRegionDishMasterData(int regionID, int? foodProgramID, int? dietCategoryID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionDishDataList")]
        IList<DishData> GetRegionDishDataList(int RegiionID, int? foodProgramID, int? dietCategoryID, string subSectorCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetItemDishMappingDataList")]
        IList<ItemDishMappingData> GetItemDishMappingDataList(string itemCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetUniqueMappedItemCodesFromItem")]
        IList<string> GetUniqueMappedItemCodesFromItem(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetUniqueMappedItemCodesFromRegion")]
        IList<string> GetUniqueMappedItemCodesFromRegion(string RegionID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDishRecipeMappingDataList")]
        IList<DishRecipeMappingData> GetDishRecipeMappingDataList(string DishCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionDishRecipeMappingDataList")]
        IList<RegionDishRecipeMappingData> GetRegionDishRecipeMappingDataList(int RegionID, string DishCode, string SubSectorCode, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteDishRecipeMappingDataList")]
        IList<SiteDishRecipeMappingData> GetSiteDishRecipeMappingDataList(string SiteCode, string DishCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetItemDishCategoryMappingDataList")]
        IList<ItemDishCategoryMappingData> GetItemDishCategoryMappingDataList(string itemCode, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetNationalItemDishCategoryMappingDataList")]
        IList<ItemDishCategoryMappingData> GetNationalItemDishCategoryMappingDataList(string itemCode, string sectorcode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionDishCategoryMappingDataList")]
        IList<RegionDishCategoryMappingData> GetRegionDishCategoryMappingDataList(string regionID, string itemCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteDishCategoryMappingDataList")]
        IList<SiteDishCategoryMappingData> GetSiteDishCategoryMappingDataList(string siteCode, string itemCode, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDishCategory")]
        IList<DishCategoryData> GetDishCategory(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRecipeDataList")]
        IList<RecipeData> GetRecipeDataList(string condition, string DishCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetItemDishRecipeNutritionData")]
        IList<ItemDishRecipeNutritionData> GetItemDishRecipeNutritionData(string SiteCode, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetNationalRecipeDataList")]
        IList<NationalRecipeData> GetNationalRecipeDataList(string condition, string DishCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionRecipeDataList")]
        IList<RegionRecipeData> GetRegionRecipeDataList(int RegionID, string condition, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteRecipeDataList")]
        IList<SiteRecipeData> GetSiteRecipeDataList(string SiteCode, string condition, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteMasterList")]
        IList<SiteMasterData> GetSiteMasterList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetBaseRecipesByRecipeID")]
        IList<RecipeMOGMappingData> GetBaseRecipesByRecipeID(int recipeID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionBaseRecipesByRecipeID")]
        IList<RecipeMOGMappingData> GetRegionBaseRecipesByRecipeID(int regionID, int recipeID, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteBaseRecipesByRecipeID")]
        IList<RecipeMOGMappingData> GetSiteBaseRecipesByRecipeID(string recipeCode, string SiteCode, int recipeID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetMOGsByRecipeID")]
        IList<RecipeMOGMappingData> GetMOGsByRecipeID(int recipeID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionMOGsByRecipeID")]
        IList<RecipeMOGMappingData> GetRegionMOGsByRecipeID(int regionID, int recipeID, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteMOGsByRecipeID")]
        IList<RecipeMOGMappingData> GetSiteMOGsByRecipeID(string recipeCode, string SiteCode, int recipeID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSectorDataByUserID")]
        IList<SectorData> GetSectorDataByUserID(int userID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetMasterRegionItemDishMappingDataList")]
        IList<RegionItemDishMappingData> GetMasterRegionItemDishMappingDataList(string regionID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionItemDishMappingDataList")]
        IList<RegionItemDishMappingData> GetRegionItemDishMappingDataList(string itemCode, string regionID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetMasterSiteItemDishMappingDataList")]
        IList<SiteItemDishMappingData> GetMasterSiteItemDishMappingDataList(string siteCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteItemDishMappingDataList")]
        IList<SiteItemDishMappingData> GetSiteItemDishMappingDataList(string itemCode, string siteCode, RequestData requestData);
        
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetFoodBookNutritionDataList")]
        IList<FoodBookNutritionData> GetFoodBookNutritionDataList(string sectorCode);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteItemDayPartMappingDataList")]
        IList<SiteItemDayPartMappingData> GetSiteItemDayPartMappingDataList(string itemCode, string siteCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteDayPartMappingDataList")]
        IList<SiteDayPartMappingData> GetSiteDayPartMappingDataList(string siteCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetFoodProgramDayPartMappingDataList")]
        IList<FoodProgramDayPartMappingData> GetFoodProgramDayPartMappingDataList(string foodCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionItemInheritanceMappingDataList")]
        IList<RegionItemInheritanceMappingData> GetRegionItemInheritanceMappingDataList(string regionID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteItemInheritanceMappingDataList")]
        IList<SiteItemInheritanceMappingData> GetSiteItemInheritanceMappingDataList(string SiteCode, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/ChangeSectorData")]
        string ChangeSectorData(int UserID, string sectorNumber, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/ChangeUserRoleData")]
        string ChangeUserRoleData(int UserID, int userRoleID, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveFoodProgramData")]
        string SaveFoodProgramData(FoodProgramData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveContainerData")]
        string SaveContainerData(ContainerData model, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveVisualCategoryData")]
        string SaveVisualCategoryData(VisualCategoryData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveColorData")]
        string SaveColorData(ColorData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveDishCategoryData")]
        string SaveDishCategoryData(DishCategoryData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveDishSubCategoryData")]
        string SaveDishSubCategoryData(DishSubCategoryData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveConceptType1Data")]
        string SaveConceptType1Data(ConceptType1Data model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveConceptType2Data")]
        string SaveConceptType2Data(ConceptType2Data model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveDishRecipeMappingList")]
        string SaveDishRecipeMappingList(IList<DishRecipeMappingData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveRegionDishRecipeMappingList")]
        string SaveRegionDishRecipeMappingList(IList<RegionDishRecipeMappingData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteDishRecipeMappingList")]
        string SaveSiteDishRecipeMappingList(IList<SiteDishRecipeMappingData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveConceptType3Data")]
        string SaveConceptType3Data(ConceptType3Data model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveConceptType4Data")]
        string SaveConceptType4Data(ConceptType4Data model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveConceptType5Data")]
        string SaveConceptType5Data(ConceptType5Data model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteProfile1Data")]
        string SaveSiteProfile1Data(SiteProfile1Data model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteProfile2Data")]
        string SaveSiteProfile2Data(SiteProfile2Data model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteProfile3Data")]
        string SaveSiteProfile3Data(SiteProfile3Data model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveItemType1Data")]
        string SaveItemType1Data(ItemType1Data model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveItemType2Data")]
        string SaveItemType2Data(ItemType2Data model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveDietCategoryData")]
        string SaveDietCategoryData(DietCategoryData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveDayPartData")]
        string SaveDayPartData(DayPartData model, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveReasonData")]
        string SaveReasonData(ReasonData model, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveRevenueTypeData")]
        string SaveRevenueTypeData(RevenueTypeData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveServewareData")]
        string SaveServewareData(ServewareData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteTypeData")]
        string SaveSiteTypeData(SiteTypeData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveAllergenData")]
        string SaveAllergenData(AllergenData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveAPLMasterData")]
        string SaveAPLMasterData(APLMasterData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveAPLMasterDataList")]
        string SaveAPLMasterDataList(IList<APLMasterData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveReasonTypeData")]
        string SaveReasonTypeData(ReasonTypeData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveUOMData")]
        string SaveUOMData(UOMData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveUOMModuleMasterData")]
        string SaveUOMModuleMasterData(UOMModuleData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveUOMModuleMappingData")]
        string SaveUOMModuleMappingData(UOMModuleMappingData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveServingTemperatureData")]
        string SaveServingTemperatureData(ServingTemperatureData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteData")]
        string SaveSiteData(SiteMasterData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteDataList")]
        string SaveSiteDataList(IList<SiteMasterData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveCafeData")]
        string SaveCafeData(CafeData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveContainerTypeData")]
        string SaveContainerTypeData(ContainerTypeData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveApplicationSettingData")]
        string SaveApplicationSettingData(ApplicationSettingData model, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveCafeDataList")]
        string SaveCafeDataList(IList<CafeData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveContainerTypeDataList")]
        string SaveContainerTypeDataList(IList<ContainerTypeData> model, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveItemData")]
        string SaveItemData(ItemData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveItemDataList")]
        string SaveItemDataList(IList<ItemData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveItemDishMappingDataList")]
        string SaveItemDishMappingDataList(IList<ItemDishMappingData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveRegionItemDishMappingDataList")]
        string SaveRegionItemDishMappingDataList(IList<RegionItemDishMappingData> model, RequestData requestData);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "mydishcateinactiveupdate")]
        string mydishcateinactiveupdate(IList<RegionDishCategoryMappingData> model, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteItemDishMappingDataList")]
        string SaveSiteItemDishMappingDataList(IList<SiteItemDishMappingData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteItemDayPartMappingDataList")]
        string SaveSiteItemDayPartMappingDataList(IList<SiteItemDayPartMappingData> model, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteDayPartMappingDataList")]
        string SaveSiteDayPartMappingDataList(IList<SiteDayPartMappingData> model, bool isClosingTime, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveFoodProgramDayPartMappingDataList")]
        string SaveFoodProgramDayPartMappingDataList(IList<FoodProgramDayPartMappingData> model, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveItemDishCategoryMappingDataList")]
        string SaveItemDishCategoryMappingDataList(IList<ItemDishCategoryMappingData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveRegionDishCategoryMappingDataList")]
        string SaveRegionDishCategoryMappingDataList(IList<RegionDishCategoryMappingData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteDishCategoryMappingDataList")]
        string SaveSiteDishCategoryMappingDataList(IList<SiteDishCategoryMappingData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveDishData")]
        string SaveDishData(DishData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveDishDataList")]
        string SaveDishDataList(IList<DishData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SavePlanningTagData")]
        string SavePlanningTagData(PlanningTagData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveMOGData")]
        string SaveMOGData(MOGData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveMOGDataList")]
        string SaveMOGDataList(IList<MOGData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveRecipeData")]
        string SaveRecipeData(RecipeData model, RecipeMOGMappingData[] baseRecipes, RecipeMOGMappingData[] mogs, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveRegionRecipeData")]
        string SaveRegionRecipeData(RegionRecipeData model, RegionRecipeMOGMappingData[] baseRecipes, RegionRecipeMOGMappingData[] mogs, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteRecipeData")]
        string SaveSiteRecipeData(SiteRecipeData model, SiteRecipeMOGMappingData[] baseRecipes, SiteRecipeMOGMappingData[] mogs, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveRecipeNutrientMappingData")]
        string SaveRecipeNutrientMappingData(RecipeData model, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveRecipeDataList")]
        string SaveRecipeDataList(IList<RecipeData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteRecipeDataList")]
        string SaveSiteRecipeDataList(IList<SiteRecipeData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteRecipeAllergenDataList")]
        string SaveSiteRecipeAllergenData(string recipeCode, string siteCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveRegionRecipeDataList")]
        string SaveRegionRecipeDataList(IList<RegionRecipeData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSectorToRegionInheritance")]
        string SaveSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveMOGNationalToSectorInheritance")]
        string SaveMOGNationalToSectorInheritance(SaveMOGNationalToSectorInheritanceData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateCostSectorToRegionInheritance")]
        string UpdateCostSectorToRegionInheritance(RegionItemInheritanceMappingData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveRegionToSiteInheritance")]
        string SaveRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveToSiteInheritance")]
        string SaveToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveToSiteInheritanceDataList")]
        string SaveToSiteInheritanceDataList(IList<SiteItemInheritanceMappingData> model, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateCostRegionToSiteInheritance")]
        string UpdateCostRegionToSiteInheritance(SiteItemInheritanceMappingData model, RequestData requestData);


        /// <summary>
        ///Test service
        /// </summary>
        /// <param name="parameter1">parameter1</param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveReminderEmail")]
        bool SaveReminderEmail(string webSiteUrl, SendEmailFilter request, RequestData requestData);

        /// <summary>
        ///Test service
        /// </summary>
        /// <param name="parameter1">parameter1</param>
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendExceptionEmail")]
        bool SendExceptionEmail(RequestData requestData);

        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendRecipeEmail")]
        bool SendRecipeEmail(RecipeData model, RequestData requestData);

        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendMOGEmail")]
        bool SendMOGEmail(MOGData model, RequestData requestData);

        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendGOVOOffSiteItemAutoInheritEmail")]
        bool SendGOVOOffSiteItemAutoInheritEmail(ItemData model, RequestData requestData);

        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendNewMOGEmail")]
        bool SendNewMOGEmail(MOGData model, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteDishContainerMappingData")]
        string SaveSiteDishContainerMappingData(SiteDishContainerMappingData model, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteDishContainerMappingDataList")]
        string SaveSiteDishContainerMappingDataList(IList<SiteDishContainerMappingData> model, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveSiteDishCategoryContainerMappingDataList")]
        string SaveSiteDishCategoryContainerMappingDataList(IList<SiteDishCategoryContainerMappingData> model, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRecipeMappingDataList")]
        IList<DishRecipeMapData> GetRecipeMappingDataList(string SiteCode, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetPlanningTag")]
        IList<PlanningTagData> GetPlanningTag(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSimulatedMappedRecipeMOGListSiteDataList")]
        IList<SimulatedMappedRecipeMOGListSiteData> GetSimulatedMappedRecipeMOGListSiteDataList(int RecipeID, int SiteId, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSimulatedMappedRecipeMOGListSectorDataList")]
        IList<SimulatedMappedRecipeMOGListSectorData> GetSimulatedMappedRecipeMOGListSectorDataList(int RecipeID, int RegionId, RequestData requestData);[OperationContract]

        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSimulatedMappedRecipeMOGListNationalDataList")]
        IList<SimulatedMappedRecipeMOGListNationalData> GetSimulatedMappedRecipeMOGListNationalDataList(int RecipeID, RequestData requestData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionsDetailsByUserIdDataList")]
        IList<RegionsDetailsByUserIdData> GetRegionsDetailsByUserIdDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteDetailsByUserIdDataList")]
        IList<SiteDetailsByUserIdData> GetSiteDetailsByUserIdDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetUserRBFAMatrixDataByUserId")]
        IList<UserRBFAMatrixData> GetUserRBFAMatrixDataByUserId(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetInheritChangesSectorToRegion")]
        IList<InheritanceChangesData> GetInheritChangesSectorToRegion(ItemInheritChangeRequestData itemInheritChangeRequestData, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetInheritChangesRegionToSite")]
        IList<InheritanceChangesData> GetInheritChangesRegionToSite(ItemInheritChangeRequestData itemInheritChangeRequestData, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveChangesSectorToRegion")]
        string SaveChangesSectorToRegion(List<InheritanceChangesData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveChangesRegionToSite")]
        string SaveChangesRegionToSite(List<InheritanceChangesData> model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetExpiryCategoryDataList")]
        IList<ExpiryCategoryMasterData> GetExpiryCategoryDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetShelfLifeUOMDataList")]
        IList<ShelfLifeUOMMasterData> GetShelfLifeUOMDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveExpiryCategory")]
        string SaveExpiryCategory(RequestData requestData, ExpiryCategoryMasterData model);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetExpiryProductDataList")]
        IList<GetExpiryProductData> GetExpiryProductDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveExpiryProductData")]
        string SaveExpiryProductData(GetExpiryProductData model, RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SaveNutritionMaster")]
        string SaveNutritionMaster(RequestData requestData, NutritionElementMasterData model);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetNutritionMasterDataList")]
        IList<NutritionElementMasterData> GetNutritionMasterDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetNutritionElementTypeDataList")]
        IList<NutritionElementTypeData> GetNutritionElementTypeDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDietTypeDataList")]
        IList<DietTypeData> GetDietTypeDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetTextureDataList")]
        IList<TextureData> GetTextureDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetCommonMasterDataList")]
        IList<CommonMasterData> GetCommonMasterDataList(RequestData requestData, CommonMasterData model);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveCommonMasterData")]
        string SaveCommonMasterData(RequestData requestData, CommonMasterData model);

        //Krish
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSimulationDataListWithPaging")]
        List<FoodCostSimulationData> GetSimulationDataListWithPaging(RequestData requestData, UserMasterResponseData userData);
        //Krish
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteDishRecipeDataList")]
        IList<SiteRecipeData> GetSiteDishRecipeDataList(string SiteCode, string dishCode, string condition, RequestData request);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetItemsForSimulation")]
        IList<SimulationItemsData> GetItemsForSimulation(RequestData requestData, int siteId, int regionId, List<int> daypartId, bool checksiteItem);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDayPartBySiteList")]
        IList<DayPartData> GetDayPartBySiteList(RequestData requestData, int siteId);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSimulationRegionItemDishCategories")]
        IList<SimulationRegionItemDishCategoryData> GetSimulationRegionItemDishCategories(RequestData requestData, int siteId, int regionId, int itemId);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteByRegionDataList")]
        IList<SiteMasterData> GetSiteByRegionDataList(int regionId, RequestData request);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSimulationBoard")]
        SimulationBoardData GetSimulationBoard(int simId, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveSimulationBoard")]
        string SaveSimulationBoard(SimulationBoardData model, RequestData requestData, FoodCostSimulationData simStatusUpdate);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateAndFetchSiteDishRecipeData")]
        IList<SiteRecipeData> UpdateAndFetchSiteDishRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/UpdateAndFetchSiteRecipeData")]
        IList<SiteRecipeData> UpdateAndFetchSiteRecipeData(RequestData requestData, string simCode, string SiteCode, string DishCode);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveFoodCostSimSiteRecipeData")]
        string SaveFoodCostSimSiteRecipeData(RecipeData modelr, SiteRecipeData model, IList<SiteRecipeMOGMappingData> baseRecipes, IList<SiteRecipeMOGMappingData> mogs, string simCode, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetFCSSiteMOGsByRecipeID")]
        IList<RecipeMOGMappingData> GetFCSSiteMOGsByRecipeID(string SiteCode, int recipeID, string recipeCode, string simCode, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveFCSAPLHistory")]
        string SaveFCSAPLHistory(FoodCostSimAPLHistoryData entity, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetFCSAPLMasterSiteDataList")]
        IList<APLMasterData> GetFCSAPLMasterSiteDataList(string articleNumber, string mogCode, string SiteCode, string simCode, RequestData request);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetFcsDishDataList")]
        IList<DishData> GetFcsDishDataList(RequestData requestData, string siteCode, int itemId);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetMOGUOMDataList")]
        IList<MOGUOMData> GetMOGUOMDataList(RequestData requestData);

        //Krish
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRecipePrintingData")]
        RecipePrintingData GetRecipePrintingData(string recipeCode, string siteCode, int regId, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveRecipeCopy")]
        string SaveRecipeCopy(List<RecipeCopyData> model, List<string> recipes, string sourceSite, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetExpiryProductDataOnlyExpiryNameList")]
        IList<GetExpiryProductData> GetExpiryProductDataOnlyExpiryNameList(RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetUserRoleDataByUserID")]
        IList<UserRoleData> GetUserRoleDataByUserID(int userID, RequestData request);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRecipeDishDataByDish")]
        IList<NutritionDishRecipeMogMapping> GetRecipeDishDataByDish(string dishCode, RequestData request);
        [OperationContract]
        //Krish
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetPatientMaster")]
        List<PatientMasterData> GetPatientMaster(RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetPatientMasterHISTORY")]        
        List<PatientMasterHISTORY> GetPatientMasterHISTORY(RequestData requestData, string username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetPatientMasterHISTORYDet")]
        List<PatientMasterHISTORY> GetPatientMasterHISTORYDet(RequestData requestData, string username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetshowGetALLPatientMasterHISTORYBATCHWISE")]
        List<PatientMasterHistoryBatchWise> GetshowGetALLPatientMasterHISTORYBATCHWISE(RequestData requestData, string batchno);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/AddUpdatePatientMasterList")]
        string AddUpdatePatientMasterList(RequestData request, string xml, string username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SavePatientMasterStatus")]
        string SavePatientMasterStatus(RequestData request, int id, int username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetALLHISTORYDetailsPatientMaster")]
        List<PatientMasterHistoryAllData> GetALLHISTORYDetailsPatientMaster(RequestData request);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSiteItemDropDownDataData")]
        SiteItemDropDownData GetSiteItemDropDownDataData( RequestData request);
        //Bed
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetBedMaster")]
        List<BedMasterData> GetBedMaster(RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetBedMasterHISTORY")]
        List<BedMasterHISTORY> GetBedMasterHISTORY(RequestData requestData, string username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetBedMasterHISTORYDet")]
        List<BedMasterHISTORY> GetBedMasterHISTORYDet(RequestData requestData, string username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetshowGetALLBedMasterHISTORYBATCHWISE")]
        List<BedMasterHistoryBatchWise> GetshowGetALLBedMasterHISTORYBATCHWISE(RequestData requestData,  string batchno);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/AddUpdateBedMasterList")]
        string AddUpdateBedMasterList(RequestData request, string xml, string username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveBedMasterStatus")]
        string SaveBedMasterStatus(RequestData request, int id, int username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetALLHISTORYDetailsBedMaster")]
        List<BedMasterHistoryAllData> GetALLHISTORYDetailsBedMaster(RequestData request);

        //Dietician
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDieticianMaster")]
        List<DieticianMasterData> GetDieticianMaster(RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDieticianMasterHISTORY")]
        List<DieticianMasterHISTORY> GetDieticianMasterHISTORY(RequestData requestData, string username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDieticianMasterHISTORYDet")]
        List<DieticianMasterHISTORY> GetDieticianMasterHISTORYDet(RequestData requestData, string username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetshowGetALLDieticianMasterHISTORYBATCHWISE")]
        List<DieticianMasterHistoryBatchWise> GetshowGetALLDieticianMasterHISTORYBATCHWISE(RequestData requestData,  string batchno);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/AddUpdateDieticianMasterList")]
        string AddUpdateDieticianMasterList(RequestData request, string xml, string username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveDieticianMasterStatus")]
        string SaveDieticianMasterStatus(RequestData request, int id, int username);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetALLHISTORYDetailsDieticianMaster")]
        List<DieticianMasterHistoryAllData> GetALLHISTORYDetailsDieticianMaster(RequestData request);
        //Krish 28-07-2022
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSectorItemData")]
        SectorItemData GetSectorItemData(RequestData requestData, bool isAddEdit, bool onLoad);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRegionItemData")]
        RegionItemData GetRegionItemData(RequestData requestData, bool isSectorUser, int regionId = 0, bool gridOnly = false);
        //Krish 02-08-2022
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSectorDishData")]
        SectorDishData GetSectorDishData(RequestData requestData, string condition, bool isAddEdit, bool onLoad);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetSectorRecipeData")]
        SectorRecipeData GetSectorRecipeData(RequestData requestData, bool isAddEdit, bool onLoad);
        //Krish
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetRangeTypeMasterData")]
        IList<RangeTypeMasterData> GetRangeTypeMasterData(RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveRangeTypeMasterData")]
        string SaveRangeTypeMasterData(RequestData requestData, RangeTypeMasterData model);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveRangeTypeDishData")]
        string SaveRangeTypeDishData(string dishCode, string rangeTypeCode, RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveRangeTypeAPLData")]
        string SaveRangeTypeAPLData(string articleNumber, string rangeTypeCode, RequestData requestData);
        //Krish 13-10-2022
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetProcessTypeMasterData")]
        IList<ProcessTypeMasterData> GetProcessTypeMasterData(RequestData requestData);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveProcessTypeMasterData")]
        string SaveProcessTypeMasterData(RequestData requestData, ProcessTypeMasterData model);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveProcessTypeMogData")]
        string SaveProcessTypeMogData(RequestData requestData, List<string> processTypes, string mogCode, string rangeTypeCode, int inputStdDuration);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetProcessTypeMogData")]
        IList<MogProcessTypeMappingData> GetProcessTypeMogData(RequestData requestData, string mogCode);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetProcessMappingDataList")]
        IList<MogProcessTypeMappingData> GetProcessMappingDataList(RequestData requestData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveHISPatientProfileMaster")]
        string SaveHISPatientProfileMaster(RequestData request, List<HISPatientProfileData> hisPatientProfileData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveHISUserData")]
        string SaveHISUserData(RequestData request, List<HISUserData> hisUserData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SaveHISBedMasterData")]
        string SaveHISBedMasterData(RequestData request, List<HISBedMasterData> hisBedMasterData);

    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        private bool boolValue = true;
        private string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
